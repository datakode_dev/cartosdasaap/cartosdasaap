<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index')->name('home');
Route::get('/charte', function () {
    return view('charte');
});

Auth::routes();

Route::prefix('profil')->group(function () {
    Route::get('/edit', 'ProfilController@edit')->name('profil.edit');
    Route::get('/', 'ProfilController@show')->name('profil.show');
    Route::put('/update', 'ProfilController@update')->name('profil.update');
});

Route::get('/p', 'ProxyController@get')->name('http.get');

Route::get('/permissions', 'PermissionController@index')->name('permissions.index');
Route::get('/permissions/create', 'PermissionController@create')->name('permissions.create');
Route::post('/permissions', 'PermissionController@store')->name('permissions.store');
Route::get('/permissions/dataTable', 'PermissionController@indexDatatable')->name('permissions.indexDatatable');

Route::get('/permissions/manage', 'PermissionController@manage')->name('permissions.manage');
Route::get('/permissions/{id}/edit_accounts', 'PermissionController@edit_accounts')->name('permissions.edit_accounts');
Route::post('/permissions/{id}', 'PermissionController@storeAccountsUser')->name('permission.store_accounts');

Route::get('/permissions/usersDatatable', 'PermissionController@userDatatable')->name('permissions.userDatatable');

Route::prefix('accounts')->group(function () {
    Route::get('', 'AccountController@index')->name('accounts.index');
    Route::get('/create', 'AccountController@create')->name('accounts.create');
    Route::post('', 'AccountController@store')->name('accounts.store');
    Route::get('/dataTable', 'AccountController@indexDatatable')->name('accounts.indexDatatable');

    Route::prefix('{account_id}')->group(function () {
        Route::get('/', 'AccountController@home')->name('accounts.home');
        Route::get('/edit', 'AccountController@edit')->name('accounts.edit');
        Route::post('/update', 'AccountController@update')->name('accounts.update');

        Route::post('/save_custom_icon', 'AccountController@saveCustomIcon')->name('accounts.saveCustomIcon');
        Route::get('/custom_icon', 'AccountController@customIcon')->name('accounts.customIcon');
        Route::get('/deleteIconSvg/{ico_id}', 'AccountController@deleteIconSvg')->name('accounts.deleteIconSvg');

        Route::prefix('members')->group(function () {
            Route::get('/create', 'MemberController@create')->name('members.create');
            Route::post('/', 'MemberController@store')->name('members.store');
            Route::get('/', 'MemberController@index')->name('members.index');
            Route::get('/dataTable', 'MemberController@indexDatatable')->name('members.indexDatatable');

            Route::prefix('{member_id}')->group(function () {
                Route::get('/', 'MemberController@show')->name('members.show');
                Route::get('/edit', 'MemberController@edit')->name('members.edit');
                Route::put('/update', 'MemberController@update')->name('members.update');
                Route::get('/', 'MemberController@show')->name('members.show');
                Route::put('/archivate', 'MemberController@archivate')->name('members.archivate');
                Route::put('/unarchivate', 'MemberController@unarchivate')->name('members.unarchivate');
                Route::get('/delete', 'MemberController@delete')->name('members.delete');
            });
        });

        Route::prefix('roles')->group(function () {
            Route::get('/create', 'RoleController@create')->name('roles.create');
            Route::post('/', 'RoleController@store')->name('roles.store');
            Route::get('/', 'RoleController@index')->name('roles.index');
            Route::get('/dataTable', 'RoleController@indexDatatable')->name('roles.indexDatatable');

            Route::prefix('{role_id}')->group(function () {
                Route::get('/', 'RoleController@show')->name('roles.show');
                Route::get('/edit', 'RoleController@edit')->name('roles.edit');
                Route::put('/update', 'RoleController@update')->name('roles.update');
                Route::get('/', 'RoleController@show')->name('roles.show');
                Route::put('/archivate', 'RoleController@archivate')->name('roles.archivate');
                Route::put('/unarchivate', 'RoleController@unarchivate')->name('roles.unarchivate');
                Route::delete('/delete', 'RoleController@delete')->name('roles.delete');
            });
        });

        Route::prefix('ico_color')->group(function () {
            Route::get('/create', 'IcoColorController@create')->name('ico_color.create');
            Route::post('', 'IcoColorController@store')->name('ico_color.store');
            Route::get('', 'IcoColorController@index')->name('ico_color.index');
            Route::get('/dataTable', 'IcoColorController@indexDatatable')->name('ico_color.indexDatatable');


            Route::prefix('{ico_color_id}')->group(function () {
                Route::get('/', 'IcoColorController@show')->name('ico_color.show');
                Route::get('/edit', 'IcoColorController@edit')->name('ico_color.edit');
                Route::put('/update', 'IcoColorController@update')->name('ico_color.update');
                Route::get('/delete', 'IcoColorController@delete')->name('ico_color.delete');
            });
        });

        Route::prefix('isochrone_context')->group(function () {
            Route::get('/create', 'IsochroneContextController@create')->name('isochrone_contexts.create');
            Route::post('/', 'IsochroneContextController@store')->name('isochrone_contexts.store');
            Route::get('/', 'IsochroneContextController@index')->name('isochrone_contexts.index');
            Route::get('/dataTable', 'IsochroneContextController@indexDatatable')->name('isochrone_contexts.indexDatatable');

            Route::prefix('{isochrone_context_id}')->group(function () {
                Route::get('/', 'IsochroneContextController@show')->name('isochrone_contexts.show');
                Route::get('/edit', 'IsochroneContextController@edit')->name('isochrone_contexts.edit');
                Route::put('/update', 'IsochroneContextController@update')->name('isochrone_contexts.update');
                Route::get('/update_calcul', 'IsochroneContextController@updateCalcul')->name('isochrone_contexts.updateCalcul');
                Route::put('/duplicate', 'IsochroneContextController@duplicate')->name('isochrone_contexts.duplicate');
                Route::put('/archivate', 'IsochroneContextController@archivate')->name('isochrone_contexts.archivate');
                Route::put('/unarchivate', 'IsochroneContextController@unarchivate')->name('isochrone_contexts.unarchivate');
                Route::delete('/delete', 'IsochroneContextController@delete')->name('isochrone_contexts.delete');
            });
        });

        Route::prefix('layers')->group(function () {
            Route::get('/create', 'LayerController@create')->name('layers.create');
            Route::post('/', 'LayerController@store')->name('layers.store');
            Route::get('/', 'LayerController@index')->name('layers.index');
            Route::get('/dataTable', 'LayerController@indexDatatable')->name('layers.indexDatatable');

            Route::prefix('{layer_id}')->group(function () {
                Route::get('/', 'LayerController@show')->name('layers.show');
                Route::get('/edit', 'LayerController@edit')->name('layers.edit');
                Route::put('/update', 'LayerController@update')->name('layers.update');
                Route::put('/duplicate', 'LayerController@duplicate')->name('layers.duplicate');
                Route::put('/archivate', 'LayerController@archivate')->name('layers.archivate');
                Route::put('/unarchivate', 'LayerController@unarchivate')->name('layers.unarchivate');
                Route::delete('/delete', 'LayerController@delete')->name('layers.delete');
                Route::get('/download', 'LayerController@download')->name('layers.download');
            });
        });

        Route::prefix('tb')->group(function () {
            Route::get('/create', 'TbController@create')->name('tb.create');
            Route::post('/', 'TbController@store')->name('tb.store');
            Route::get('/', 'TbController@index')->name('tb.index');
            Route::get('/dataTable', 'TbController@indexDatatable')->name('tb.indexDatatable');

            Route::prefix('{tb_id}')->group(function () {
                Route::get('/', 'TbController@show')->name('tb.show');
                Route::get('/edit', 'TbController@edit')->name('tb.edit');
                Route::put('/update', 'TbController@update')->name('tb.update');
                Route::put('/duplicate', 'TbController@duplicate')->name('tb.duplicate');
                Route::put('/archivate', 'TbController@archivate')->name('tb.archivate');
                Route::put('/unarchivate', 'TbController@unarchivate')->name('tb.unarchivate');
                Route::delete('/delete', 'TbController@delete')->name('tb.delete');
            });
        });

        // Research_contexts
        Route::prefix('research_contexts')->group(function () {
            Route::get('/create', 'ResearchContextsController@create')->name('research_contexts.create');
            Route::post('/', 'ResearchContextsController@store')->name('research_contexts.store');
            Route::get('/', 'ResearchContextsController@index')->name('research_contexts.index');
            Route::get('/dataTable', 'ResearchContextsController@indexDatatable')->name('research_contexts.indexDatatable');

            Route::prefix('{research_contexts_id}')->group(function () {
                Route::get('/', 'ResearchContextsController@show')->name('research_contexts.show');

                Route::get('/filter', 'ResearchContextsController@filter')->name('research_contexts.filter');

                Route::get('/edit', 'ResearchContextsController@edit')->name('research_contexts.edit');
                Route::put('/update', 'ResearchContextsController@update')->name('research_contexts.update');
                Route::put('/duplicate', 'ResearchContextsController@duplicate')->name('research_contexts.duplicate');

                Route::put('/archivate', 'ResearchContextsController@archivate')->name('research_contexts.archivate');
                Route::put('/unarchivate', 'ResearchContextsController@archivate')->name('research_contexts.unarchivate');
                Route::delete('/delete', 'ResearchContextsController@delete')->name('research_contexts.delete');

            });
        });

        Route::prefix('services')->group(function () {
            Route::prefix('peis')->group(function () {
                Route::get('/create', 'ServicePeiController@create')->name('services_pei.create');
                Route::post('/', 'ServicePeiController@store')->name('services_pei.store');
                Route::get('/', 'ServicePeiController@index')->name('services_pei.index');
                Route::get('/dataTable', 'ServicePeiController@indexDatatable')->name('services_pei.indexDatatable');

                Route::prefix('{peis_id}')->group(function () {
                    Route::get('/edit', 'ServicePeiController@edit')->name('services_pei.edit');
                    Route::put('/update', 'ServicePeiController@update')->name('services_pei.update');
                    Route::get('/', 'ServicePeiController@show')->name('services_pei.show');
                    Route::get('/dataTable', 'ServicePeiController@showDatatable')->name('services_pei.showDatatable');
                    Route::put('/duplicate', 'ServicePeiController@duplicate')->name('services_pei.duplicate');
                    Route::put('/archivate', 'ServicePeiController@archivate')->name('services_pei.archivate');
                    Route::put('/unarchivate', 'ServicePeiController@unarchivate')->name('services_pei.unarchivate');
                    Route::delete('/delete', 'ServicePeiController@delete')->name('services_pei.delete');

                    Route::get('/edit_peis', 'ServicePeiController@edit_peis')->name('services_pei.edit_peis');
                    Route::put('/update_peis', 'ServicePeiController@update_peis')->name('services_pei.update_peis');

                    Route::prefix('pei')->group(function () {
                        Route::prefix('{pei_id}')->group(function () {
                            Route::get('/edit', 'ServicePeiController@edit_point')->name('services_pei.edit_point');
                            Route::put('/update', 'ServicePeiController@update_point')->name('services_pei.update_point');
                        });
                    });
                });
            });
        });

        Route::prefix('ign_keys')->group(function () {
            Route::get('/create', 'IgnKeyController@create')->name('ign_keys.create');
            Route::post('/', 'IgnKeyController@store')->name('ign_keys.store');
            Route::get('/', 'IgnKeyController@index')->name('ign_keys.index');
            Route::get('/dataTable', 'IgnKeyController@indexDatatable')->name('ign_keys.indexDatatable');

            Route::prefix('{ign_keys_id}')->group(function () {
                Route::get('/', 'IgnKeyController@show')->name('ign_keys.show');
                Route::get('/edit', 'IgnKeyController@edit')->name('ign_keys.edit');
                Route::put('/update', 'IgnKeyController@update')->name('ign_keys.update');
                Route::get('/delete', 'IgnKeyController@delete')->name('ign_keys.delete');
            });
        });
    });
});

Route::prefix('tests')->group(function () {
    Route::get('/mail', 'TestController@mail')->name('tests.mail');
    Route::get('/show', 'TestController@show')->name('tests.show');
    Route::get('/embed', 'TestController@embed')->name('tests.embed');
    Route::get('/dataTable', 'TestController@indexDatatable')->name('layers.indexDatatable');
});
