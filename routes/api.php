<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/limit', 'API\GeoController@getLimit')->name('API.geo.limite')->middleware('cors');
Route::get('/insee/{zone_id}/{insee}/{dist_around_limit?}', 'API\GeoController@geoJson')->name('API.geo.geoJson')->middleware('cors');
Route::get('/sirene', 'API\SireneController@index')->name('API.geo.convert')->middleware('local_only');
Route::get('/sirene/limit/{limit}/{buffer?}', 'API\SireneController@getLimitAsArray')->name('API.geo.getLimitAsArray')->middleware('cors');

Route::get('/limit/name/{limit}', 'API\GeoController@getLimitName')->name('API.geo.limiteName')->middleware('cors');

Route::get('/accounts/{account_id}/layers_insee/{id}', 'API\GeoController@getPossibleInseeForContext')->name('API.geo.possibleInsee')->middleware('cors');
Route::get('/accounts/{account_id}/layers/{id}', 'API\GeoController@getLayerClipFromContextMoveStep')->name('API.geo.layerClip')->middleware('cors');
Route::post('/accounts/{account_id}/layers/{id}/collision', 'API\LayerController@collision')->name('API.geo.collision')->middleware('cors');
Route::get('/accounts/{account_id}/boards/layers/{id}', 'API\GeoController@getBoardLayerClipFromLayerId')->name('API.geo.boardLayerClipFromLayerId')->middleware('cors');

Route::get('/boards/{id}', 'API\BoardController@getBoard')->name('API.geo.getBoard')->middleware('cors');
Route::get('/boards/{id}/points', 'API\BoardController@getBoardServicePoints')->name('API.geo.getBoardServicePoints')->middleware('cors');


Route::get('/accounts/{account_id}/layers_insee_board/{id}', 'API\GeoController@getPossibleInseeForBoard')->name('API.geo.possibleInseeForBoard')->middleware('cors');
Route::get('/contexts/{id}', 'API\ContextController@getContext')->name('API.getContext')->middleware('cors');
Route::get('/contexts/{id}/services', 'API\ContextController@getContextServices')->name('API.getContextServices')->middleware('cors');
Route::get('/contexts/{contextId}/services/{id}/movetypes', 'API\ContextController@getContextServiceMoveTypes')->name('API.getContextServiceMoveTypes')->middleware('cors');
Route::get('/contexts/{contextId}/services/{serviceId}/movetypes/{id}/movesteps', 'API\ContextController@getContextServiceMoveTypeSteps')->name('API.getContextServiceMoveTypeSteps')->middleware('cors');

Route::get('/services/{id}', 'API\ServiceController@getServiceWithPoints')->name('API.getPointsForService')->middleware('cors');

Route::get('/researchcontexts/{id}', 'API\ResearchContextController@getContext')->name('API.getResearchContext')->middleware('cors');
Route::get('/researchcontexts/{serviceId}/{contextId}', 'API\ResearchContextController@getServiceWithPointsForLimit')->name('API.getServiceWithPointsForLimit')->middleware('cors');

Route::post('/accounts/{account_id}/research_contexts/{research_contexts_id}/download_pdf','API\ResearchContextController@downloadPdf')->name('API.downloadPdf')->middleware('cors');

Route::get('/accounts/home/{id}', 'API\HomeController@getHomeIgnData')->name('API.getHomeIgnData')->middleware('cors');