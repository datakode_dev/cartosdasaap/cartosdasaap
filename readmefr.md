## Installation

-`composer install`

-`npm install`

-`cp .env.example .env`

- Editer le fichier .env 

-`php artisan key:generate`

- Installer postgis (`brew install postgis` sur macOs )

- Activer postgis avec `CREATE EXTENSION postgis;` dans psql

-`psql cartosdasaap -c "CREATE EXTENSION postgis";`

- Créer un nouveau compte avec le même mail que celui utilisé pour la connexion, sans remplir les autres champs. Cela génèrera un accès au compte en tant qu'utilisateur. 
- Import CSV files to database (details to come)



## Table des types de clé
- 1 : Login/Pass

- 2 : Référés 



## Files d'attente

Il y a deux files d'attentes pour chaque compte.`default_{account_id}` et `high_{account_id}`Les fichiers de configuration de supervisor sont créés automatiquement lors de la création d'un nouveau compte.
Pour installer supervisor sur votre serveur, veuillez consulter [la documentation du superviseur] (http://supervisord.org/installing.html).
Si vous avez déjà des comptes créés lors de l'installation de supervisor, vous pouvez exécuter la commande suivante pour créer les fichiers de configuration supervisor: `php artisan work: update`
Vous devrez créer un lien symbolique entre votre fichier de configuration de supervisor et le dossier dans lequel ils sont stockés, par exemple `ln -s /etc/supervisor/conf.d/var/www/carto/storage/app/.supervisord` où `/var/www/carto/` est votre répertoire d'installation. Vous devrez peut-être supprimer le dossier `/etc/supervisor/conf.d` avant de créer le lien symbolique.
Ajoutez la variable suivante à votre fichier `.env`: `SUPERVISOR_USER = www-data` où `www-data` est l'utilisateur sous lequel votre serveur Web est exécuté.
La commande `supervisorctl restart all` permet de relancer les services (calculs de services, contextes) sur le serveur.
La commande `php artisan queue:work --queue=high_2` (2 représente le numéro de compte) permet de relancer les calculs des services pour un compte.
La commande `php artisan queue:work --queue=default_2` (2 représente le numéro de compte) permet de relancer les calculs des contextes pour un compte.
Pour charger un departement en local : `php artisan geoapi:get_departement 09` (09 représente le numero de département ici l ariège).
