<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class ProfilTest extends BaseTestCase
{
    /**
     * Check profil.edit run with code 200
     *
     * @return void
     */
    public function testEdit()
    {
        $response = $this->actingAs($this->getAdmin())
            ->get(route('profil.edit'));
        if ($response->exception) {
            dump($response->exception);
        }
        $response->assertStatus(200);
    }
    /**
     * Check profil.show run with code 200
     *
     * @return void
     */
    public function testShow()
    {
        $response = $this->actingAs($this->getAdmin())
            ->get(route('profil.show'));
        $response->assertStatus(200);
    }
}
