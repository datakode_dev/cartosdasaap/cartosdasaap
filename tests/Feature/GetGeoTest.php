<?php

namespace Tests\Feature;

use Tests\Feature\BaseFeature as BaseTestCase;

class GetGeoTest extends BaseTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);

        /* try {
    $this->artisan('geoapi:get_departement', [
    'insee' => "2A",
    ]);
    $this->assertTrue(true);
    } catch (Exception $e) {
    $this->assertTrue(false);
    }*/
    }

    public function testGetLayerClip()
    {
        $this->markTestSkipped('Not working if the account is not created');
        $url = route('API.geo.layerClip', [
            'account_id' => 1,
            'id' => '13',
        ]);
        $response = $this->actingAs($this->getAdmin())
            ->get($url);
        $response->assertSuccessful();
    }
}
