<?php

namespace Tests\Feature\API;

use Tests\TestCase;

class GeoTest extends TestCase
{
    /**
     * Check API.geo.limite run with code 200
     *
     * @return void
     */
    public function testLimite()
    {
        $this->markTestSkipped('Returns a 500 error');

        $response = $this->get('api/limit');
        $response->assertStatus(200);
    }
}
