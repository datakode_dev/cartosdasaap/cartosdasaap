<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class RoleTest extends BaseTestCase
{
    /**
     * Check roles.index run with code 200
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestSkipped('Not working if the account is not created');
        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/roles');
        $response->assertStatus(200);
    }

    /**
     * Check roles.show run with code 200
     *
     * @return void
     */
    public function testShow()
    {
        $this->markTestSkipped('Not working if the account is not created');
        $response = $this->actingAs($this->getAdmin())
            ->get('accounts/1/roles/1');
        $response->assertStatus(200);
    }

    /**
     * Check roles.edit run with code 200
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('accounts/1/roles/1/edit');
        $response->assertStatus(200);
    }
}
