<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class IcoColorTest extends BaseTestCase
{
    /**
     * Check ico_color.index run with code 200
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/ico_color');
        $response->assertStatus(200);
    }

    /**
     * Check ico_color.dataTable run with code 200
     *
     * @return void
     */
    public function testDataTable()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/ico_color/dataTable');
        $response->assertStatus(200);
    }
}
