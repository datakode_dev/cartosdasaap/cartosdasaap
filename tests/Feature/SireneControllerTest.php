<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;

class SireneControllerTest extends TestCase
{
    public function testSireneConversionReturnsGeoJson()
    {
        $this->markTestSkipped();
        $res = $this->get(route('API.geo.convert', [
            'limit' => '81005.1',
            'nodes[0]' => '02',
        ]));

        if ($res->getStatusCode() !== 200) {
            dump($res->exception);
        }
        $res->assertSuccessful();
        $data = $res->json();

        


        // TODO get the total number of results from another source
        $this->assertEquals(4, count($data['features']));
    }
    
    public function testSireneConversionReturnsGeoJsonWithBuffer()
    {
        $this->markTestSkipped();
        $res = $this->get(route('API.geo.convert', [
            'limit' => '81005.1',
            'buffer' => 10,
            'nodes[0]' => '02',
        ]));
        $u = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=economicref-france-sirene-v3&facet=libellecommuneetablissement&geofilter.polygon=(43.342236343004,2.4811532851415),(43.344495025163,2.4720856855197),(43.352497859919,2.4554795021565),(43.355927859919,2.4499045021565),(43.365704492073,2.4368350626871),(43.377678501285,2.4257438099309),(43.391456960021,2.416994703738),(43.393346184876,2.4162305794728),(43.396981119499,2.4139128606555),(43.414916551689,2.4072655114274),(43.433851038741,2.404554021121),(43.437221038741,2.404433021121),(43.454062357541,2.4054086056416),(43.47042662708,2.4095064668546),(43.485740187925,2.4165829517398),(43.499466213875,2.42638998981),(43.505156118902,2.4323417506054),(43.511186211755,2.4366217863432),(43.511534705987,2.4369839170555),(43.522412336656,2.4423030561597),(43.536434633611,2.4529788737817),(43.548110475655,2.4661801088131),(43.556993141,2.4814016773976),(43.562742775771,2.4980611970123),(43.565139396937,2.5155212686267),(43.564091308936,2.5331138638115),(43.563239308936,2.5384798638115),(43.562633045435,2.5408013602781),(43.56235640675,2.5475056483983),(43.558556033165,2.5638791073398),(43.551780538818,2.5792617492926),(43.542265732821,2.5931182085778),(43.530342761187,2.6049662356083),(43.521186032295,2.61116928358),(43.514727808304,2.6163844153416),(43.512116808304,2.6180324153416),(43.503926765171,2.622139713706),(43.500206638859,2.6246901053432),(43.484832363532,2.6314509213776),(43.483844809612,2.6316796723642),(43.482725677103,2.6324594161542),(43.482693455466,2.6324734836058),(43.482581584601,2.6325953897674),(43.468027047062,2.6431694035936),(43.451661887685,2.6506410760931),(43.434138543591,2.6547125307604),(43.416155625766,2.6552214490934),(43.39843006723,2.6521475418176),(43.381668540743,2.6456133577675),(43.366539285571,2.6358793981779),(43.353645466488,2.6233337311643),(43.34350112714,2.6084765204385),(43.34120512714,2.6042645204385),(43.334284557434,2.5879136156884),(43.330704804607,2.5705230509793),(43.330604881064,2.552768153444),(43.333801956525,2.5363001376728),(43.333266364295,2.5273609387132),(43.333591364295,2.5163699387132),(43.335405959599,2.5008493163096),(43.339874916827,2.4858756428388),(43.342236343004,2.4811532851415)&refine.activiteprincipaleunitelegale=02.10Z&refine.activiteprincipaleunitelegale=02.20Z&refine.activiteprincipaleunitelegale=02.30Z&refine.activiteprincipaleunitelegale=02.40Z&disjunctive.activiteprincipaleunitelegale=true&rows=100&start=0&fields=adresseetablissement,codepostaletablissement,libellecommuneetablissement,denominationunitelegale,prenom1unitelegale,nomunitelegale,denominationusuelleetablissement,geolocetablissement';

        $totalReq = json_decode(@file_get_contents($u));

        $this->assertEquals(true, isset($totalReq->nhits));

        if ($res->getStatusCode() !== 200) {
            dump($res->exception);
        }
        $res->assertSuccessful();
        $data = $res->json();

        // TODO get the total number of results from another source
        $this->assertEquals($totalReq->nhits, count($data['features']));
    }

    public function testSireneConversionWithNoActivityReturnsEmptyGeoJson()
    {
        $this->markTestSkipped();
        $res = $this->get(route('API.geo.convert', [
            'limit' => '81005.1',
        ]));

        if ($res->getStatusCode() !== 200) {
            dump($res);
        }
        $res->assertSuccessful();
        $data = $res->json();

        $this->assertEquals(0, count($data['features']));
    }
}
