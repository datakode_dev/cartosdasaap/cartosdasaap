<?php
namespace Tests\Feature;

use App\Models\Account;
use App\Models\Member;
use App\Models\User;
use Tests\TestCase;

class BaseFeature extends TestCase
{
    protected $admin;

    public function getAdmin()
    {
        if (!$this->admin) {
            $this->admin = User::where('email', 'admin@datakode.fr')->first();
        }

        return $this->admin;
    }

    public function getAccounts()
    {
        $member = Member::where('user_id', $this->getAdmin()->id)->get()->map(function ($item) {
            return $item->account_id;
        })->toArray();

        return Account::whereIn('id', $member)->get();
    }
}
