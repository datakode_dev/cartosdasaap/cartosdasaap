<?php

namespace Tests\Feature;

use App\Models\User;

class AccountTest extends BaseFeature
{
    public function testCreate()
    {
        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/create');
        $response->assertStatus(200);
    }
    public function testIndex()
    {
        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts');
        $response->assertStatus(200);
    }
    public function testHome()
    {
        $this->markTestSkipped('Not working if the account is not created');
        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1');
        $response->assertStatus(200);
    }
}
