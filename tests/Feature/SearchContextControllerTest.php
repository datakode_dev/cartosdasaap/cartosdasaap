<?php

namespace Tests\Feature;

use App\Models\IgnKey;
use App\Models\ResearchContext;
use App\Models\ServicePei;
use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class SearchContextControllerTest extends BaseTestCase
{
    /**
     * Test Get Search Context
     *
     * @return void
     */
    public function testSearchControllerShouldThrow404IfContextDoesNotBelongToAccount()
    {
        $account = $this->getAccounts()->first();

        $response = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => 1,
            ]));

        $response->assertStatus(404);
    }

    /**
     * Test POST Search Context
     *
     * @return void
     */
    public function testSearchControllerShouldRedirectToIndexAfterCreate()
    {
        $account = $this->getAccounts()->first();

        $response = $this->actingAs($this->getAdmin())
            ->post(route('research_contexts.store', [
                "account_id" => $account->id,
            ]), [
                '_token' => csrf_token(),
                'name' => "testestes",
                "limit" => "81005.1",
                "ign_key_id" => (string) IgnKey::where('account_id', $account->id)->first()->id,
                'services' => [(string) ServicePei::where('account_id', $account->id)->first()->id],
                'dist_around_limit' => 0,
                'dist_city' => false,
                'dist_route' => false,
                'dist_selection' => false,
                'dist_legend' => false,
            ]);

        $response->assertRedirect(route('research_contexts.index', [
            "account_id" => $account->id,
        ]));

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => ResearchContext::where('account_id', $account->id)->orderBy('id', 'desc')->first()->id,
            ]));

        $contextResponse->assertSee("testestes");
    }


    /**
     * Test Update Search Context
     *
     * @depends testSearchControllerShouldRedirectToIndexAfterCreate
     * @return void
     */
    public function testUpdateSearchController()
    {
        $account = $this->getAccounts()->first();

        $ctx = ResearchContext::where('account_id', $account->id)->orderBy('id', 'desc')->first();

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertSee("testestes");

        $response = $this->actingAs($this->getAdmin())
            ->put(route('research_contexts.update', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]), [
                //'_token' => csrf_token(),
                'name' => "name is changed",
                "limit" => "81005.1",
                "ign_key_id" => (string) IgnKey::where('account_id', $account->id)->first()->id,
                'services' => [(string) ServicePei::where('account_id', $account->id)->first()->id],
                'dist_around_limit' => 0,
                'dist_city' => false,
                'dist_route' => false,
                'dist_selection' => false,
                'dist_legend' => false,
            ]);

        $response->assertRedirect(route('research_contexts.index', [
            "account_id" => $account->id,
        ]));

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertDontSee("testestes");
        $contextResponse->assertSee("name is changed");
    }

    /*
    /**
     * Test Archive Search Context
     *
     * @depends testSearchControllerShouldRedirectToIndexAfterCreate
     * @return void

    public function testArchiveSearchController()
    {
        $account = $this->getAccounts()->first();

        $ctx = ResearchContext::where('account_id', $account->id)->orderBy('id', 'desc')->first();

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertDontSee("Ce contexte de recherche est désactivé");

        $response = $this->actingAs($this->getAdmin())
            ->put(route('research_contexts.archivate', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $response->assertRedirect(route('research_contexts.index', [
            "account_id" => $account->id,
        ]));

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertSee("Ce contexte de recherche est désactivé");
    }
    */

    /**
     * Test Restore Search Context
     *
     * @depends testArchiveSearchController
     * @return void
     */
    public function testRestoreSearchController()
    {
        $account = $this->getAccounts()->first();

        $ctx = ResearchContext::where('account_id', $account->id)->orderBy('id', 'desc')->first();

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertSee("Ce contexte de recherche est désactivé");

        $response = $this->actingAs($this->getAdmin())
            ->put(route('research_contexts.unarchivate', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $response->assertRedirect(route('research_contexts.index', [
            "account_id" => $account->id,
        ]));

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertDontSee("Ce contexte de recherche est désactivé");
    }


    /**
     * Test Delete Search Context
     *
     * @depends testRestoreSearchController
     * @return void
     */
    public function testDeleteSearchController()
    {
        $account = $this->getAccounts()->first();

        $ctx = ResearchContext::where('account_id', $account->id)->orderBy('id', 'desc')->first();

        $this->actingAs($this->getAdmin())
            ->put(route('research_contexts.archivate', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertSee("Ce contexte de recherche est désactivé");



        $response = $this->actingAs($this->getAdmin())
            ->delete(route('research_contexts.delete', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $response->assertRedirect(route('research_contexts.index', [
            "account_id" => $account->id,
        ]));

        $contextResponse = $this->actingAs($this->getAdmin())
            ->get(route('research_contexts.show', [
                "account_id" => $account->id,
                "research_contexts_id" => $ctx->id,
            ]));

        $contextResponse->assertStatus(404);
    }
}
