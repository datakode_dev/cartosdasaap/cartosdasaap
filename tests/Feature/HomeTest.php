<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class HomeTest extends BaseTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomeAdmin()
    {
        $response = $this->actingAs($this->getAdmin())
            ->get('/');

        $response->assertStatus(200);
    }

    public function testHomeFail()
    {
        $response = $this->actingAs($this->getAdmin())
            ->get('/test');
        $response->assertStatus(404);
    }
}
