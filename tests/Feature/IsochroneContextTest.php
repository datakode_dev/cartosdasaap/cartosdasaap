<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class IsochroneContextTest extends BaseTestCase
{
    /**
     * Check isochrone_contexts.index run with code 200
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/isochrone_context');
        $response->assertStatus(200);
    }

    /**
     * Check isochrone_contexts.dataTable run with code 200
     *
     * @return void
     */
    public function testDataTable()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/isochrone_context/dataTable');
        $response->assertStatus(200);
    }

    /**
     * Check isochrone_contexts.create run with code 200
     *
     * @return void
     */
    public function testCreate()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/isochrone_context');
        $response->assertStatus(200);
    }

    /**
     * Check isochrone_contexts.show run with code 200
     *
     * @return void
     */
    public function testShow()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/isochrone_context/1');
        $response->assertStatus(200);
    }
}
