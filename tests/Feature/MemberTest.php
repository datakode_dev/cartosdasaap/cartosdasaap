<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\Feature\BaseFeature as BaseTestCase;

class MemberTest extends BaseTestCase
{
    /**
     * Check members.index run with code 200
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestSkipped('Not working if the account is not created');
        $response = $this->actingAs($this->getAdmin())
            ->get('/accounts/1/members');
        $response->assertStatus(200);
    }

    /**
     * Check members.show run with code 200
     *
     * @return void
     */
    public function testShow()
    {
        $this->markTestSkipped('Not working if the account is not created');
        $response = $this->actingAs($this->getAdmin())
            ->get('accounts/1/members/1');
        $response->assertStatus(200);
    }

    /**
     * Check members.edit run with code 200
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestSkipped('Not working if the account is not created');

        $response = $this->actingAs($this->getAdmin())
            ->get('accounts/1/members/1/edit');
        $response->assertStatus(200);
    }
}
