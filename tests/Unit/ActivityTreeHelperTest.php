<?php

namespace Tests\Unit;

use App\Helpers\ActivityTreeHelper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ActivityTreeHelperTest extends TestCase
{
    /**
     * Check that the helper returns all the top nodes when the trees are the same
     *
     * @return void
     */
    public function testGetCommonNodeShouldReturnAllTopNodesOnSameTrees()
    {
        $origin = [
            [
                "code" => 'A',
                "libelle" => "[A] Transports et entreposage",
                "children" => [
                    [
                        "code" => '1',
                        "libelle" => "[1] aaaa",
                        "children" => [
                            [
                                "code" => '01',
                                "libelle" => "[01] aaaa",
                            ]
                        ]
                    ],
                    [
                        "libelle" => "[2] aaaa",
                        "code" => '2',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "libelle" => "[B] Transports et entreposage",
                "children" => [
                    [
                        "code" => '3',
                        "libelle" => "[3] aaaa",
                        "children" => [
                            [
                                "libelle" => "[02] aaaa",
                                "code" => '02',
                            ],
                            [
                                "code" => '03',
                                "libelle" => "[03] aaaa",
                            ]
                        ]
                    ],
                    [
                        "code" => '4',
                        "libelle" => "[4] aaaa",
                    ],
                ],
            ],
        ];
        $tree = $origin;
        $res = ActivityTreeHelper::getMainCommonNode($origin, $tree);
        $this->assertEquals(['A', 'B'], $res);
    }

    /**
     * Check that the helper returns the full top node when one top node is fully selected
     *
     * @return void
     */
    public function testGetCommonNodeShouldReturnOneTopNodesOnDifferentTrees()
    {
        $origin = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                            ]
                        ]
                    ],
                    [
                        "code" => '2',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '02',
                            ],
                            [
                                "code" => '03',
                            ]
                        ]
                    ],
                    [
                        "code" => '4',
                    ],
                ],
            ],
            [
                "code" => 'C',
                "children" => [
                    [
                        "code" => '4',
                        "children" => [
                            [
                                "code" => '05',
                            ],
                            [
                                "code" => '06',
                            ]
                        ]
                    ],
                    [
                        "code" => '07',
                    ],
                ],
            ],
        ];
        $tree = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                            ]
                        ]
                    ],
                    [
                        "code" => '2',
                    ],
                ],
            ],
        ];
        $res = ActivityTreeHelper::getMainCommonNode($origin, $tree);
        $this->assertEquals(['A'], $res);
    }


    /**
     * Check that the helper returns the full top node and one child
     * node when the second side is partially selected
     *
     * @return void
     */
    public function testGetCommonNodeShouldReturnOneTopNodeAndSubNode()
    {
        $origin = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                            ]
                        ]
                    ],
                    [
                        "code" => '2',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '02',
                            ],
                            [
                                "code" => '03',
                            ]
                        ]
                    ],
                    [
                        "code" => '4',
                    ],
                ],
            ],
            [
                "code" => 'C',
                "children" => [
                    [
                        "code" => '4',
                        "children" => [
                            [
                                "code" => '02',
                            ],
                            [
                                "code" => '03',
                            ]
                        ]
                    ],
                    [
                        "code" => '5',
                    ],
                ],
            ],
        ];
        $tree = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                            ]
                        ]
                    ],
                    [
                        "code" => '2',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '02',
                            ],
                            [
                                "code" => '03',
                            ]
                        ]
                    ],
                ],
            ],
        ];
        $res = ActivityTreeHelper::getMainCommonNode($origin, $tree);

        $this->assertEquals(['A', '3'], $res);
    }


    /**
     * Check that the helper returns the full top node and sub child when only
     * one is selected in one of the main branches
     *
     * @return void
     */
    public function testGetCommonNodeShouldReturnOneTopNodeAndOneSubNode()
    {
        $origin = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                            ]
                        ]
                    ],
                    [
                        "code" => '2',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '02',
                            ],
                            [
                                "code" => '03',
                            ]
                        ]
                    ],
                    [
                        "code" => '4',
                    ],
                ],
            ],
        ];

        $tree = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                            ]
                        ]
                    ],
                    [
                        "code" => '2',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '02',
                            ]
                        ]
                    ],
                ],
            ],
        ];
        $res = ActivityTreeHelper::getMainCommonNode($origin, $tree);
        $this->assertEquals(['A', '02'], $res);
    }

    public function testGetDeepNodesReturnsOnlyDeepNodes()
    {
        $origin = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                                "children" => [
                                    [
                                        "code" => '011',
                                        "children" => [
                                            [
                                                "code" => '0111',
                                            ],
                                            [
                                                "code" => '0112',
                                            ],
                                            [
                                                "code" => '0113',
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "code" => '02',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '04',
                            ],
                            [
                                "code" => '05',
                            ]
                        ]
                    ],
                    [
                        "code" => '4',
                    ],
                ],
            ],
            [
                "code" => 'C',
                "children" => [
                    [
                        "code" => '8',
                        "children" => [
                            [
                                "code" => '081',
                            ],
                            [
                                "code" => '082',
                            ]
                        ]
                    ],
                    [
                        "code" => '9',
                    ],
                ],
            ],
        ];

        $tree = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                                "children" => [
                                    [
                                        "code" => '011',
                                        "children" => [
                                            [
                                                "code" => '0111',
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "code" => '02',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '04',
                            ]
                        ]
                    ],
                ],
            ],
            [
                "code" => 'C',
                "children" => [
                    [
                        "code" => '8',
                        "children" => [
                            [
                                "code" => '081',
                            ],
                            [
                                "code" => '082',
                            ]
                        ]
                    ],
                    [
                        "code" => '9',
                    ],
                ],
            ],
        ];
        $res = ActivityTreeHelper::getMainCommonNode($origin, $tree);
        $this->assertEquals(['0111', '02', '04', 'C'], $res);
    }

    public function testGetDeepNodesReturnsOnlyDeepNodesWithoutFirst()
    {
        $origin = [
            [
                "code" => 'A',
                "children" => [
                    [
                        "code" => '1',
                        "children" => [
                            [
                                "code" => '01',
                                "children" => [
                                    [
                                        "code" => '011',
                                        "children" => [
                                            [
                                                "code" => '0111',
                                            ],
                                            [
                                                "code" => '0112',
                                            ],
                                            [
                                                "code" => '0113',
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "code" => '02',
                    ],
                ],
            ],
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => '3',
                        "children" => [
                            [
                                "code" => '04',
                            ],
                            [
                                "code" => '05',
                            ]
                        ]
                    ],
                    [
                        "code" => '4',
                    ],
                ],
            ],
            [
                "code" => 'C',
                "children" => [
                    [
                        "code" => '8',
                        "children" => [
                            [
                                "code" => '081',
                            ],
                            [
                                "code" => '082',
                            ]
                        ]
                    ],
                    [
                        "code" => '9',
                    ],
                ],
            ],
        ];

        $tree = [
            [
                "code" => 'B',
                "children" => [
                    [
                        "code" => 3,
                        "children" => [
                            [
                                "code" => '04',
                            ]
                        ]
                    ],
                ],
            ],
            [
                "code" => 'C',
                "children" => [
                    [
                        "code" => '8',
                        "children" => [
                            [
                                "code" => '081',
                            ],
                            [
                                "code" => '082',
                            ]
                        ]
                    ],
                    [
                        "code" => '9',
                    ],
                ],
            ],
        ];
        $res = ActivityTreeHelper::getMainCommonNode($origin, $tree);
        $this->assertEquals(['04', 'C'], $res);
    }

    public function testRequestFormatter()
    {
        $requestFormat = [
            "A" => [
                "01" => "01",
            ],
            "B" => [
                "02" => [
                    "022" => "022",
                    "023" => "023",
                ],
            ],
        ];

        $expectedFormat = [
            [
                "code" => "A",
                "children" => [
                    [
                        "code" => "01",
                    ],
                ],
            ],
            [
                "code" => "B",
                "children" => [
                    [
                        "code" => "02",
                        "children" => [
                            [
                                "code" => "022",
                            ],
                            [
                                "code" => "023",
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $formatted = ActivityTreeHelper::mapFromRequest($requestFormat);

        $this->assertEquals($expectedFormat, $formatted);
    }
}
