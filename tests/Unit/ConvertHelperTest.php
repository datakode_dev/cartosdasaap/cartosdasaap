<?php

namespace Tests\Unit;

use App\Helpers\ConvertHelper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConvertHelperTest extends TestCase
{
    protected $csv;
    protected $csvWithCommas;

    public function setUp()
    {
        parent::setUp();
        $this->csv = 'fiche_dispositif_type;fiche_structure;fiche_dispositif_lieu;fiche_adresse;fiche_acces_transport;fiche_acces_PMR;fiche_tel;fiche_mail;fiche_site;fiche_horaires;fiche_public;Adresse;Code postal;Commune;N°Obs
Accès au wifi - 1;Commune de BREUIL-LA-REORTE;secrétariat de mairie;93 LA CRIGNOLEE<br>17700 BREUIL-LA-REORTE;;Oui;05.46.68.91.72;mairie@breuillareorte.fr;non;Mardi : après midi<br>Mercredi : matin<br>Vendredi : après midi;Tout public;93 LA CRIGNOLEE;17700;BREUIL-LA-REORTE;1
Accès au wifi - 6;DIAGONALES /Net SOLIDAIRE;Net SOLIDAIRE Villeneuve-les-Salines;30 BIS AVENUE BILLAUD VARENNE<br>17000 LA ROCHELLE;Ligne 2 Arrêt 14 juillet;Oui;05.16.59.00.60;contact@netsolidaire.org;https://netsolidaire.org;Mardi : après midi<br>Jeudi : matin;Moins de 26 ans<br>Plus de 60 ans<br>Bénéficiaires du RSA<br>Personnes de nationalité étrangère<br>Personnes en recherche d’emploi;30 BIS AVENUE BILLAUD VARENNE;17000;LA ROCHELLE;6
Accès au wifi - 7;DIAGONALES /Net SOLIDAIRE;Net SOLIDAIRE  Mireuil;10 BIS AVENUE DES GRANDES VARENNES<br>17000 LA ROCHELLE;Ligne 2 Arrêt Dâme Hilaire;Oui;05.46.07.94.15;contact@netsolidaire.org;https://netsolidaire.org;Mardi : après midi<br>Jeudi : matin;Moins de 26 ans<br>Plus de 60 ans<br>Bénéficiaires du RSA<br>Personnes de nationalité étrangère<br>Personnes en recherche d’emploi;10 BIS AVENUE DES GRANDES VARENNES;17000;LA ROCHELLE;7
Accès au wifi - 11;ESPACE MOSAÏQUE;ESPACE MOSAÏQUE;27 RUE DE BENON<br>17170 COURCON;;Oui;05.46.01.94.39;cs.courcon@wanadoo.fr;www.espacemosaique.fr;Lundi : matin, après midi<br>Mardi : matin, après midi<br>Mercredi : matin, après midi<br>Jeudi : matin, après midi<br>Vendredi : matin, après midi;Tout public;27 RUE DE BENON;17170;COURCON;11
Accès au wifi - 15;Centre socioculturel "Les 4 Vents";Centre social;2 RUE DES HERONS<br>17140 LAGORD;arret illico et les cygognes;Oui;05.46.67.15.73;centre-lagord@wanadoo.fr;www.les4vents.centres-sociaux.fr;Lundi : après midi<br>Mardi : matin, après midi<br>Mercredi : matin, après midi<br>Jeudi : matin, après midi<br>Vendredi : matin;Tout public;2 RUE DES HERONS;17140;LAGORD;15
Accès au wifi - 16;Commune de SAINTES;Centre communal ou intercommunal d’Action Sociale (CCAS ou CIAS);1 ESPLANADE DU 6E RÉGIMENT D\'INFANTERIE<br>17100 SAINTES;Ligne B arret de Bus Abbaye;Oui;05.46.98.24.10;acceuil-mds@ville-saintes.fr;https://www.ville-saintes.fr/;Lundi : 24/24 h<br>Mardi : 24/24 h<br>Mercredi : 24/24 h<br>Jeudi : 24/24 h<br>Vendredi : 24/24 h<br>Samedi : 24/24 h<br>Dimanche : 24/24 h;Tout public;1 ESPLANADE DU 6E RÉGIMENT D\'INFANTERIE;17100;SAINTES;16';

        $this->csvWithCommas = 'fiche_dispositif_type;fiche_structure;fiche_dispositif_lieu;fiche_adresse;fiche_acces_transport;fiche_acces_PMR;fiche_tel;fiche_mail;fiche_site;fiche_horaires;fiche_public;Adresse;Code postal;Commune;N°Obs
Accès au wifi, - 1;Commune de BREUIL-LA-REORTE;secrétariat de mairie;93 LA CRIGNOLEE<br>17700 BREUIL-LA-REORTE;;Oui;05.46.68.91.72;mairie@breuillareorte.fr;non;Mardi : après midi<br>Mercredi : matin<br>Vendredi : après midi;Tout public;93 LA CRIGNOLEE;17700;BREUIL-LA-REORTE;1
Accès au wifi - 6;DIAGONALES /Net, SOLIDAIRE;Net SOLIDAIRE Villeneuve-les-Salines;30 BIS AVENUE BILLAUD VARENNE<br>17000 LA ROCHELLE;Ligne 2 Arrêt 14 juillet;Oui;05.16.59.00.60;contact@netsolidaire.org;https://netsolidaire.org;Mardi : après midi<br>Jeudi : matin;Moins de 26 ans<br>Plus de 60 ans<br>Bénéficiaires du RSA<br>Personnes de nationalité étrangère<br>Personnes en recherche d’emploi;30 BIS AVENUE BILLAUD VARENNE;17000;LA ROCHELLE;6
Accès au wifi - 7;DIAGONALES /Net SOLIDAIRE;Net SOLIDAIRE  Mireuil;10 BIS AVENUE DES GRANDES VARENNES<br>17000 LA ROCHELLE;Ligne 2 Arrêt Dâme Hilaire;Oui;05.46.07.94.15;contact@netsolidaire.org;https://netsolidaire.org;Mardi : après midi<br>Jeudi : matin;Moins de 26 ans, Plus de 60 ans<br>Bénéficiaires du RSA<br>Personnes de nationalité étrangère<br>Personnes en recherche, d’emploi;10 BIS AVENUE DES GRANDES VARENNES;17000;LA ROCHELLE;7
Accès au wifi - 11;ESPACE MOSAÏQUE;ESPACE MOSAÏQUE;27 RUE DE BENON<br>17170 COURCON;;Oui;05.46.01.94.39;cs.courcon@wanadoo.fr;www.espacemosaique.fr;Lundi : matin, après midi<br>Mardi : matin, après midi<br>Mercredi : matin, après midi<br>Jeudi : matin, après midi<br>Vendredi : matin, après midi;Tout public;27 RUE DE BENON;17170;COURCON;11
Accès au wifi - 15;Centre socioculturel "Les 4 Vents";Centre social;2 RUE DES HERONS<br>17140 LAGORD;arret illico et les cygognes;Oui;05.46.67.15.73;centre-lagord@wanadoo.fr;www.les4vents.centres-sociaux.fr;Lundi : après midi<br>Mardi : matin, après midi<br>Mercredi : matin, après midi<br>Jeudi : matin, après midi<br>Vendredi : matin;Tout public;2 RUE DES HERONS;17140;LAGORD;15
Accès au wifi - 16;Commune de SAINTES;Centre communal ou intercommunal d’Action Sociale (CCAS ou CIAS);1 ESPLANADE DU 6E RÉGIMENT D\'INFANTERIE<br>17100 SAINTES;Ligne B arret de Bus Abbaye;Oui;05.46.98.24.10;acceuil-mds@ville-saintes.fr;https://www.ville-saintes.fr/;Lundi : 24/24 h<br>Mardi : 24/24 h<br>Mercredi : 24/24 h<br>Jeudi : 24/24 h<br>Vendredi : 24/24 h<br>Samedi : 24/24 h<br>Dimanche : 24/24 h;Tout public;"1 ESPLANADE DU 6E RÉGIMENT; D\'INFANTERIE";17100;SAINTES;16';
    }

    /**
     * Test basic CSV conversion
     *
     * @return void
     */
    public function testBasicConversion()
    {
        $res = ConvertHelper::csvToJson($this->csv);

        $this->assertTrue(is_array($res));
        $this->assertEquals(6, count($res));
        $this->assertEquals('fiche_dispositif_type', array_keys($res[5])[0]);
        $this->assertEquals('Accès au wifi - 16', $res[5]['fiche_dispositif_type']);
    }

    /**
     * Test basic CSV conversion With commas in values
     *
     * @return void
     */
    public function testBasicConversionWithComma()
    {
        $res = ConvertHelper::csvToJson($this->csvWithCommas);

        $this->assertTrue(is_array($res));
        $this->assertEquals(6, count($res));
        $this->assertEquals('fiche_dispositif_type', array_keys($res[5])[0]);
        $this->assertEquals('Accès au wifi - 16', $res[5]['fiche_dispositif_type']);
        $this->assertEquals('Accès au wifi, - 1', $res[0]['fiche_dispositif_type']);
    }

    /**
     * Test basic CSV conversion For file with issues
     *
     * @return void
     */
    public function testBasicConversionWithIssues()
    {
        $res = ConvertHelper::csvToJson(file_get_contents('tests/fixtures/MSAP-utf8.csv'));

        $this->assertTrue(is_array($res));
        $this->assertEquals(1277, count($res));
        $this->assertEquals('Présentation', array_keys($res[29])[1]);
        $this->assertEquals('Handicap mental, Handicap moteur', $res[29]['Accessibilité']);
        $this->assertEquals('Oui', $res[0]['Agence départementale d\'information sur le logement (ADIL)']);

        foreach($res as $i => $r) {
            $this->assertEquals(array_keys($res[0]), array_keys($r));
        }
    }
}
