let mix = require('laravel-mix');
const exec = require('child_process').exec;
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
   'resources/assets/js/app.js',
   'resources/assets/js/inputs/select2/inputAjax.js'
], 'public/js');

mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('resources/assets/sass/embed.scss', 'public/css');

mix.sass('resources/assets/sass/font.scss', 'public/css');

mix.then(() => {
    exec('postcss ./public/css/embed.css -o ./public/css/embed.css', (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return;
        }
    });
});

mix.js([
   'resources/assets/js/controllers/accounts/data_table.js',
], 'public/js/account.js');

mix.js([
   'resources/assets/js/controllers/accounts/home.js',
], 'public/js/account_home.js');

mix.js([
   'resources/assets/js/controllers/members/data_table.js',
], 'public/js/member.js');

mix.js([
   'resources/assets/js/controllers/permissions/data_table.js',
], 'public/js/permission.js');

mix.js([
   'resources/assets/js/controllers/roles/data_table.js',
], 'public/js/role.js');

mix.js([
   'resources/assets/js/embed/embed.js',
], 'public/js/embed.js');

mix.js([
   'resources/assets/js/embed/embed-service.js',
], 'public/js/embed-service.js');


mix.js([
   'resources/assets/js/controllers/tb/show.js',
], 'public/js/board_show.js');

mix.js([
   'resources/assets/js/embed/embed-board.js',
], 'public/js/embed-board.js');



mix.js([
   'resources/assets/js/controllers/ico_colors/data_table.js',
], 'public/js/ico_color.js');

mix.js([
   'resources/assets/js/controllers/services_pei/hummingbird-treeview.js',
   'resources/assets/js/controllers/services_pei/services_pei.js',
   'resources/assets/js/controllers/services_pei/data_table.js',
], 'public/js/services_pei.js');

mix.js([
   'resources/assets/js/controllers/layers/data_table.js',
   'resources/assets/js/controllers/layers/map.js',
], 'public/js/layers.js');

mix.js([
   'resources/assets/js/controllers/isochrone_contexts/duplicate_input.js',
   'resources/assets/js/controllers/isochrone_contexts/data_table.js',
   'resources/assets/js/controllers/isochrone_contexts/limite_map.js',
   'resources/assets/js/controllers/isochrone_contexts/layersshow.js',
], 'public/js/isochrone_contexts.js');



mix.js([
   'resources/assets/js/controllers/table_research_contexts/data_table.js',
], 'public/js/table_research_contexts.js');


mix.js([
   'resources/assets/js/controllers/tb/data_table.js',
], 'public/js/tb.js');


mix.js([
   'resources/assets/js/controllers/services_pei/peis_show.js',
   'resources/assets/js/controllers/services_pei/show_data_table.js',
], 'public/js/peis_show.js');

mix.js([
   'resources/assets/js/controllers/services_pei/peis_edit.js',
], 'public/js/peis_edit.js');

mix.js([
   'resources/assets/js/controllers/services_pei/pei_edit.js',
], 'public/js/pei_edit.js');

mix.js([
   'resources/assets/js/controllers/tests/test.js',
], 'public/js/test.js');

mix.js([
   'resources/assets/js/controllers/ign_keys/data_table.js',
], 'public/js/ign_keys.js');

mix.js([
   'resources/assets/js/controllers/ign_keys/create_ign_key.js',
], 'public/js/create_ign_key.js');

mix.js([
   'resources/assets/js/controllers/table_research_contexts/show.js',
], 'public/js/research_context.js');

mix.js([
   'resources/assets/js/embed/embed-search.js',
], 'public/js/embed-search.js');

mix.js([
   'resources/assets/js/embed/workers/search-points.js',
], 'public/js/search-worker.js');

mix.copyDirectory('node_modules/fontawesome-iconpicker', 'public/vendor/fontawesome-iconpicker');
mix.copyDirectory('node_modules/tarteaucitronjs', 'public/vendor/tarteaucitronjs');
mix.copyDirectory('node_modules/geoportal-extensions-leaflet/dist', 'public/vendor/geoportal');
