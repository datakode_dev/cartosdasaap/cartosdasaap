<?php

namespace App\Rules;

use http\Env\Request;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PasswordConfirmation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     * Cette regle va permettre de s assurer que le mdp utilisateur du formulaire est bon
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //je recupere le mot de passe de l'utilisateur
        $hashedPassword = auth()->user()->password;

        $allow = false;
        //je compare le mot de passe du formulaire avec celui enregistré dans la base
        if (Hash::check(request()->actualPassword, $hashedPassword)) {
            // si c est le meme je met la variable allow à true
            $allow = true;
        }
        //je retourne la valeur true si c est le meme mdp, false sinon
        return $allow;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
