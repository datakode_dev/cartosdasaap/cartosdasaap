<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class MemberEmail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $parameters = request()->route()->parameters;
        $member = DB::table('members')
            ->join('users', 'users.id', '=', 'members.user_id')
            ->select('members.id')
            ->where('users.email', $value)
            ->where('members.account_id', $parameters['account_id'])
            ->first();
        $allow = false;
        if ($member == null) {
            $allow = true;
        }
        return $allow;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.member_email');
    }
}
