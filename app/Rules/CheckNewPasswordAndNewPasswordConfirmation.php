<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckNewPasswordAndNewPasswordConfirmation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     * Cette regle va permettre de s assurer que le nouveau mdp et  le nouveau mdp confirmation sont identiques
     *
     * @param  mixed $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $newPassword = request()->newPassword;
        $newPasswordConfirmation = request()->newPasswordConfirmation;
        $allow = false;
        // si les 2 mdp sont identiques, je retourne true
        if ($newPassword == $newPasswordConfirmation) {
            $allow = true;
        }
        return $allow;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
