<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IgnKeyLoginPass implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     * @param mixed $attribute
     * @param mixed $value
     */
    public function passes($attribute, $value)
    {
        $key_type = request()->key_type;
        $allow = true;
        if (($key_type == "1") and (request()->ign_login == null)) {
            $allow = false;
        }
        return $allow;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Le champ Login doit être rempli.';
    }
}
