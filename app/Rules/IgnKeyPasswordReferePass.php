<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IgnKeyPasswordReferePass implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     * @param mixed $attribute
     * @param mixed $value
     */
    public function passes($attribute, $value)
    {
        $key_type = request()->key_type;
        $allow = true;
        if (($key_type == "2") and (request()->ign_password <> null)) {
            $allow = false;
        }
        return $allow;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Le champ Password doit être vide si le type de clé est référer.';
    }
}
