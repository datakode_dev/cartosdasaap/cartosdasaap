<?php
namespace App\Repositories;

use App\Models\IsoContextMoveStep;
use App\Models\IsoContextMoveTypes;
use App\Models\IsoContextServices;
use Illuminate\Support\Facades\DB;

class ContextRepository
{
    public static function deleteContextOLDData($context, $context_service_ids, $context_move_type_ids, $context_move_step_ids)
    {
        $account_id = $context->account_id;
        $context_services = IsoContextServices::where('iso_context_id', $context->id)->get();
        foreach ($context_services as $key => $context_service) {
            $context_move_types = IsoContextMoveTypes::where('iso_context_service_id', $context_service->id)->get();
            foreach ($context_move_types as $key => $context_move_type) {
                $context_move_steps = IsoContextMoveStep::where('iso_context_move_type_id', $context_move_type->id)->get();
                foreach ($context_move_steps as $key => $context_move_step) {
                    if (!in_array($context_move_step->id, $context_move_step_ids)) {
                        DB::table('account_' . $account_id . '.isos')->where('context_step_move_id', $context_move_step->id)->delete();
                        $layers_collect = DB::table('account_' . $account_id . '.layers')->where('context_step_move_id', $context_move_step->id)->select('id')->get();
                        $layers_id = [];
                        foreach ($layers_collect as $layer) {
                            $layers_id[] = $layer->id;
                        }

                        DB::table('account_' . $account_id . '.layers')->where('context_step_move_id', $context_move_step->id)->update(['context_step_move_id' => null]);
                        if (!empty($layers_id)) {
                            DB::table('account_' . $account_id . '.layer_clips')->whereIn('layer_id', [$layers_id])->delete();
                            DB::table('account_' . $account_id . '.territoire_props')->whereIn('layer_id', [$layers_id])->delete();
                            DB::table('account_' . $account_id . '.props')->whereIn('layer_id', [$layers_id])->delete();
                        }

                        $context_move_step->delete();
                    }
                }
                if (!in_array($context_move_type->id, $context_move_type_ids)) {
                    $context_move_type->delete();
                }
            }
            if (!in_array($context_service->id, $context_service_ids)) {
                $context_service->delete();
            }
        }
    }

    public static function deleteContextData($context)
    {
        $account_id = $context->account_id;
        $context_services = IsoContextServices::where('iso_context_id', $context->id)->get();
        foreach ($context_services as $key => $context_service) {
            $context_move_types = IsoContextMoveTypes::where('iso_context_service_id', $context_service->id)->get();
            foreach ($context_move_types as $key => $context_move_type) {
                $context_move_steps = IsoContextMoveStep::where('iso_context_move_type_id', $context_move_type->id)->get();
                foreach ($context_move_steps as $key => $context_move_step) {
                    DB::table('account_' . $account_id . '.isos')->where('context_step_move_id', $context_move_step->id)->delete();
                    $layers_collect = DB::table('account_' . $account_id . '.layers')->where('context_step_move_id', $context_move_step->id)->select('id')->get();
                    $layers_id = [];
                    foreach ($layers_collect as $layer) {
                        $layers_id[] = $layer->id;
                    }

                    DB::table('account_' . $account_id . '.layers')->where('context_step_move_id', $context_move_step->id)->update(['context_step_move_id' => null]);
                    if (!empty($layers_id)) {
                        DB::table('account_' . $account_id . '.layer_clips')->whereIn('layer_id', [$layers_id])->delete();
                        DB::table('account_' . $account_id . '.territoire_props')->whereIn('layer_id', [$layers_id])->delete();
                        DB::table('account_' . $account_id . '.props')->whereIn('layer_id', [$layers_id])->delete();
                    }

                    $context_move_step->delete();
                }
                $context_move_type->delete();
            }
            $context_service->delete();
        }
    }
    //cette methode va creer un objet avec le nom, la couleur et l icone de chaque marqueur pour un account
    public static function getAllStepByContextId($context_id, $account_id)
    {
        return DB::table('iso_context_move_step')

            ->join('iso_context_move_types', 'iso_context_move_step.iso_context_move_type_id', '=', 'iso_context_move_types.id')
            ->join('move_types', 'iso_context_move_types.move_type_id', '=', 'move_types.id')
            ->join('iso_context_services', 'iso_context_move_types.iso_context_service_id', '=', 'iso_context_services.id')
            ->join('service_peis', 'iso_context_services.service_pei_id', '=', 'service_peis.id')
            ->join('iso_contexts', 'iso_context_services.iso_context_id', '=', 'iso_contexts.id')

            ->where('iso_contexts.id', $context_id)
            ->where('iso_contexts.account_id', $account_id)
            ->select('iso_context_move_step.id', 'iso_context_move_step.status', 'service_peis.name as service_name', 'iso_contexts.name', 'iso_context_move_step.value', 'move_types.name as move_type_name')
            ->get()->sortBy('value');
    }
}
