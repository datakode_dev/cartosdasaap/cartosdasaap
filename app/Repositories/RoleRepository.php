<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class RoleRepository
{

    public static function getRoleAttibutsByAccountId($account_id)
    {
        $query = "SELECT roles.id,roles.name,roles.active, COUNT(members.id) as nb_member
        FROM roles
        LEFT JOIN members ON roles.id = members.role_id
        WHERE roles.account_id = " . $account_id . " GROUP BY roles.id";

        $roles = DB::connection()->select($query);
        return $roles;
    }

    //cette methode va creer un objet avec le nom de chaque permission pour un role défini
    public static function getRoleAndPermissionByRoleId($role_id)
    {
        return DB::table('roles_permissions')
            ->join('permissions', 'permissions.id', '=', 'roles_permissions.permission_id')
            ->select('roles_permissions.role_id', 'permissions.name')
            ->where('roles_permissions.role_id', $role_id)
            ->get();
    }

    public static function getRolesIdByAccountCannotDisabled($account_id)
    {
        $result = [];
        $collect = DB::table('roles_permissions')
            ->join('permissions', 'permissions.id', '=', 'roles_permissions.permission_id')
            ->join('roles', 'roles.id', '=', 'roles_permissions.role_id')
            ->select('roles.id')
            ->where('permissions.slug', 'not_disabled')
            ->where('roles.account_id', $account_id)
            ->get();
        foreach ($collect as $element) {
            $result[] = $element->id;
        }
        return $result;
    }

    public static function removeAndRemovePermissions($role)
    {
        DB::table("roles_permissions")->where('role_id', $role->id)->delete();
        DB::table("roles")->where('id', $role->id)->delete();
    }
}
