<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 12/06/2019
 * Time: 20:44
 */

namespace App\Repositories;

use App\Helpers\GeoHelper;
use Illuminate\Support\Facades\DB;

class InseeRepository
{
    public function getFromInseeAndZoneId($insee, $zoneId)
    {
        $tableAndInsee = $this->getTableNameAndInseeFieldForZone($zoneId);
        if (!$tableAndInsee) {
            return null;
        }
        $table = $tableAndInsee['table'];
        $field = $tableAndInsee['field'];
        $insee = DB::table($table)->where($field, $insee)->first();
        //dd($insee);
        return [
            'insee' => $insee->{$field},
            'name' => isset($insee->name) ? $insee->name : $insee->nom_complet,
            'zone_id' => $zoneId,
        ];
    }

    public function getTableNameAndInseeFieldForZone($zoneId)
    {
        $zone = DB::table('geo_data.zones')->where('id', (int)$zoneId)->first();
        switch ($zone->name) {
            case 'Commune':
                return ['field' => 'insee', 'table' => 'geo_data.communes'];
                break;
            case 'EPCI':
                return ['field' => 'siren_epci', 'table' => 'geo_data.epcis'];
                break;
            case 'Département':
                return ['field' => 'insee', 'table' => 'geo_data.departements'];
                break;
            case 'Région':
                return ['field' => 'insee', 'table' => 'geo_data.regions'];
                break;
            default:
                return null;
        }
    }

    public function getNameFromZoneAndInseeLimit($insee_limit, $zone_id)
    {
        $line_zone = DB::table('geo_data.zones')->where('id', (int)$zone_id)->first();
        $zone = $line_zone->name;
        switch ($zone) {
            case 'Commune':
                $line = DB::table('geo_data.communes')->where('insee', $insee_limit)->first();
                $name = $line->name;
                return $name;
                break;
            case 'EPCI':
                $line = DB::table('geo_data.epcis')->where('siren_epci', $insee_limit)->first();
                $name = $line->nom_complet;
                return $name;
                break;
            case 'Département':
                $line = DB::table('geo_data.departements')->where('insee', $insee_limit)->first();
                $name = $line->name;
                return $name;
                break;
            case 'Région':
                $line = DB::table('geo_data.regions')->where('insee', $insee_limit)->first();
                $name = $line->name;
                return $name;
                break;
            default:
                return null;
        }
    }
}
