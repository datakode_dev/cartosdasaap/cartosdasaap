<?php
namespace App\Repositories;

use App\Helpers\GeoHelper;
use Illuminate\Support\Facades\DB;

class LayerRepository
{
    //cette methode va creer un objet avec le nom, la couleur et l icone de chaque marqueur pour un account
    public static function getLayerAttibutsByAccountId($account_id)
    {
        $query = "SELECT id,name,context_step_move_id,active FROM account_" . $account_id . ".layers";

        $layers = DB::connection()->select($query);
        return $layers;
    }

    public static function getLayerByIdAndAccountId($id, $account_id)
    {
        $query = "SELECT id,name,context_step_move_id,active FROM account_" . $account_id . ".layers WHERE id=" . $id . " LIMIT 1";

        $layers = DB::connection()->select($query);

        if (isset($layers[0])) {
            return $layers[0];
        }
        return null;
    }

    public static function getLayerGEOByIdAndAccountId($id, $account_id, $geojson = true)
    {
        $query = "SELECT id,name,ST_AsText(geom) as geo,context_step_move_id,active FROM account_" . $account_id . ".layers WHERE id=" . $id . " LIMIT 1";

        $layers = DB::connection()->select($query);

        if (isset($layers[0])) {
            $layer = $layers[0];
            if ($layer->geo != null && $geojson) {
                $result = [
                    "type" => "Feature",
                    "properties" => (object) [],
                    "geometry" => json_decode(GeoHelper::wkt2json($layer->geo)),
                ];
                $layer->geo = json_encode($result);
            }
            return $layer;
        }
        return null;
    }

    public static function archivate($layer, $account_id)
    {
        DB::table("account_" . $account_id . ".layers")
            ->where('id', $layer->id)
            ->update(['active' => false]);
    }

    public static function unarchivate($layer, $account_id)
    {
        DB::table("account_" . $account_id . ".layers")
            ->where('id', $layer->id)
            ->update(['active' => true]);
    }

    public static function removeAndRemovechild($layer, $account_id)
    {
        DB::table("account_" . $account_id . ".layer_clips")->where('layer_id', $layer->id)->delete();
        DB::table("account_" . $account_id . ".territoire_props")->where('layer_id', $layer->id)->delete();
        DB::table("account_" . $account_id . ".props")->where('layer_id', $layer->id)->delete();
        DB::table("account_" . $account_id . ".layers")->where('id', $layer->id)->delete();
    }
}
