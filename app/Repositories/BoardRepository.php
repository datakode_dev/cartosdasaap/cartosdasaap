<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 12/06/2019
 * Time: 21:38
 */

namespace App\Repositories;

use App\Models\Tb;
use Illuminate\Support\Facades\DB;

class BoardRepository
{
    public function deleteBoardData($id, $layers_id = null)
    {
        $board = Tb::find($id);
        if (!$board) {
            return;
        }
        if ($layers_id === null) {
            $layers = DB::table('board_layers')->where('board_id', $board->id)->get();
        } else {
            $layers = DB::table('board_layers')->whereIn('id', $layers_id)->get();
        }
        $schema = 'account_' . $board->account_id;

        foreach ($layers as $layer) {
            DB::table($schema . '.board_layer_clips')->where('board_layer_id', $layer->id)->delete();
            DB::table($schema . '.board_territoire_props')->where('board_layer_id', $layer->id)->delete();
            DB::table($schema . '.board_props')->where('board_layer_id', $layer->id)->delete();
        }

        if ($layers_id === null) {
            DB::table('board_layers')->where('board_id', $board->id)->delete();
        } else {
            DB::table('board_layers')->whereIn('id', $layers_id)->delete();
        }

    }
}
