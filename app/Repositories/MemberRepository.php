<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class MemberRepository
{
    public static function getAccountsByUserId(Int $id)
    {
        return DB::table('members')
            ->join('accounts', 'accounts.id', '=', 'members.account_id')
            ->select('accounts.id', 'accounts.name')
            ->where('user_id', $id)
            ->where('members.active', 1)
            ->where('accounts.active', 1)
            ->get();
    }

    public static function getMembersAndRoleByAccountId($account_id)
    {
        return DB::table('members')
            ->join('roles', 'roles.id', '=', 'members.role_id')
            ->join('users', 'users.id', '=', 'members.user_id')
            ->select('members.id', 'members.active', 'users.lastname', 'users.firstname', 'roles.name', 'roles.id as role_id')
            ->where('members.account_id', $account_id)
            ->where('users.lastname', '!=', 'Anonyme')
            ->get();
    }

    //cette methode va me creer un objet avec l id, l acount_id, le nom, le prenom, l email et le role d'un membre defini
    public static function getMemberAndRoleById($id)
    {
        return DB::table('members')
            ->join('roles', 'roles.id', '=', 'members.role_id')
            ->join('users', 'users.id', '=', 'members.user_id')
            ->select('members.id', 'members.active', 'members.account_id', 'users.lastname', 'users.firstname', 'users.email', 'roles.name')
            ->where('members.id', $id)
            ->first();
    }
}
