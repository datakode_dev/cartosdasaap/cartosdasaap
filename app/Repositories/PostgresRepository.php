<?php
namespace App\Repositories;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PostgresRepository
{
    public static function createAccountSchema($id)
    {
        //CREATE SCHEMA schema_name
        $name = 'account_' . $id;
        $sql1 = 'CREATE SCHEMA ' . $name;
        $pdo = DB::connection()->getPdo(); // get pdo from laravel database
        $query = $pdo->prepare($sql1); // prepare raw sql
        $succ = $query->execute(); // execute raw sql

        self::createPeisTable($name);
        self::createIsosTable($name);
        self::createTmpIsoTable($name);
        self::createLayersTable($name);
        self::createPropsTable($name);
        self::createLayerClipsTable($name);
        self::createTerritoirePropsTable($name);
        self::createBoardPropsTable($name);
        self::createBoardLayerClipsTable($name);
        self::createBoardTerritoirePropsTable($name);
    }

    public static function createBoardTerritoirePropsTable($name)
    {
        Schema::create($name . '.board_territoire_props', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('board_layer_id');
            $table->string('insee');
            $table->unsignedInteger('zone_id');
            $table->unsignedInteger('board_prop_id')->nullable();
            $table->foreign('board_prop_id')->references('id')->on($name . '.board_props');
            $table->timestamps();
        });
    }

    public static function createBoardLayerClipsTable($name)
    {
        Schema::create($name . '.board_layer_clips', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('board_layer_id');
            $table->string('insee');
            $table->unsignedInteger('zone_id');
            $table->unsignedInteger('board_prop_id')->nullable();
            $table->foreign('board_prop_id')->references('id')->on($name . '.board_props');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.board_layer_clips ADD "geom" geometry(Geometry,4326) NULL;');
    }

    public static function createBoardPropsTable($name)
    {
        Schema::create($name . '.board_props', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('board_layer_id');
            $table->json('props');
            $table->timestamps();
        });
    }

    public static function createTerritoirePropsTable($name)
    {
        Schema::create($name . '.territoire_props', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('layer_id');
            $table->string('insee');
            $table->unsignedInteger('zone_id');
            $table->unsignedInteger('prop_id')->nullable();
            $table->foreign('prop_id')->references('id')->on($name . '.props');
            $table->timestamps();
        });
    }

    public static function createPropsTable($name)
    {
        Schema::create($name . '.props', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('layer_id')->nullable();
            $table->foreign('layer_id')->references('id')->on($name . '.layers');
            $table->json('props');
            $table->timestamps();
        });
    }

    public static function createLayersTable($name)
    {
        Schema::create($name . '.layers', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->string('name');
            $table->string('path')->nullable();
            $table->unsignedInteger('context_step_move_id')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.layers ADD "geom" geometry(Geometry,4326) NULL;');
    }

    public static function createLayerClipsTable($name)
    {
        Schema::create($name . '.layer_clips', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('layer_id')->nullable();
            $table->foreign('layer_id')->references('id')->on($name . '.layers');
            $table->string('insee');
            $table->unsignedInteger('zone_id');
            $table->unsignedInteger('prop_id')->nullable();
            $table->foreign('prop_id')->references('id')->on($name . '.props');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.layer_clips ADD "geom" geometry(Geometry,4326) NULL;');
    }

    public static function createPeisTable($name)
    {
        Schema::create($name . '.peis', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('service_pei_id');
            $table->text('prop');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE ' . $name . '.peis ADD "geom" geometry(Geometry,4326) NULL;');
    }

    public static function createIsosTable($name)
    {
        Schema::create($name . '.isos', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->unsignedInteger('context_step_move_id');
            $table->unsignedInteger('pei_id')->nullable();
            $table->foreign('pei_id')->references('id')->on($name . '.peis');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.isos ADD "geom" geometry(Geometry,4326) NULL;');
    }

    public static function createTmpIsoTable($name)
    {
        Schema::create($name . '.tmp_iso', function (Blueprint $table) use ($name) {
            $table->increments('id');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.tmp_iso ADD "geom" geometry(Geometry,4326) NULL;');
    }

    public static function getColNameFromTable(String $table_name)
    {
        $query = "select column_name
        from information_schema.columns
        where table_name = '" . $table_name . "'
        AND column_name NOT IN ('id','idinspire','idk','geom')";
        return DB::connection()->select($query);
    }
}
