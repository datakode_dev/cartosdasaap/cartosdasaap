<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class IgnKeyRepository
{
    public static function getIgnKeysByAccountId(Int $account_id)
    {
        return DB::table('ign_keys')
            ->join('key_types', 'key_types.id', '=', 'ign_keys.key_types_id')
            ->where('account_id', $account_id)
            ->select('ign_keys.id', 'ign_keys.name', 'ign_keys.end_date', 'key_types.name as key_type_name')
            ->get();
    }

    public static function getIgnKeysById(Int $id)
    {
        return DB::table('ign_keys')
            ->join('key_types', 'key_types.id', '=', 'ign_keys.key_types_id')
            ->where('ign_keys.id', $id)
            ->select(
                'ign_keys.id',
                'ign_keys.name',
                'ign_keys.key',
                'ign_keys.max_count',
                'ign_keys.start_date',
                'ign_keys.end_date',
                'ign_keys.login',
                'ign_keys.password',
                'key_types.name as key_type_name'
            )
            ->first();
    }
}
