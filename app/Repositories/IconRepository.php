<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class IconRepository
{
    //cette methode va creer un objet avec le nom, la couleur et l icone de chaque marqueur pour un account
    public static function getIconAttibutsByAccountId($account_id)
    {
        return DB::table('icos_colors')
            ->join('colors', 'colors.id', '=', 'icos_colors.color_id')
            ->join('icos', 'icos.id', '=', 'icos_colors.ico_id')
            ->where('icos_colors.is_real', true)
            ->where('icos_colors.account_id', $account_id)
            ->select('icos_colors.id', 'icos_colors.account_id', 'icos_colors.name', 'colors.name as color_name', 'icos.name as icon_name')
            ->get();
    }
}
