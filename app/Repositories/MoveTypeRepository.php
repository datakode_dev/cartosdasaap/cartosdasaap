<?php
namespace App\Repositories;

use App\Models\MoveType;

class MoveTypeRepository
{
    public static function getAllToSelect()
    {
        $move_type_models = MoveType::all();

        $move_types = [];
        foreach ($move_type_models as $key => $move_type_model) {
            $move_type = (object) [];

            $move_type->value = $move_type_model->id;
            $move_type->name = $move_type_model->name;
            $move_types[] = $move_type;
        }
        return collect($move_types);
    }
}
