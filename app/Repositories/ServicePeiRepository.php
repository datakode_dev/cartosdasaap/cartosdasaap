<?php
namespace App\Repositories;

use App\Models\ServicePei;
use Illuminate\Support\Facades\DB;

class ServicePeiRepository
{
    //cette methode va creer un objet avec le nom, la couleur et l icone de chaque marqueur pour un account
    public static function getServicePeiAttibutsByServicePeiId($service_pei_id, $account_id)
    {
        return DB::table('service_peis')
            ->leftjoin('icos_colors', 'icos_colors.id', '=', 'service_peis.ico_color_id')
            ->leftjoin('icos', 'icos.id', '=', 'icos_colors.ico_id')
            ->leftjoin('colors', 'colors.id', '=', 'icos_colors.color_id')
            ->where('service_peis.id', $service_pei_id)
            ->where('service_peis.account_id', $account_id)
            ->select(
                'service_peis.id',
                'service_peis.info',
                'service_peis.day_between_refresh',
                'service_peis.account_id',
                'service_peis.source_url',
                'service_peis.ico_color_id',
                'service_peis.name',
                'service_peis.path',
                'service_peis.geojson_type',
                'service_peis.geometry',
                'service_peis.outline as color_outline_name',
                'service_peis.fill as color_fill_name',
                'icos.path as icon_path',
                'icos.is_svg as icon_svg',
                'icos.name as icon_name',
                'colors.name as color_name',
                'icos_colors.name as ico_colors_name',
                'service_peis.display_fields',
                'service_peis.display_selection',
                'service_peis.display_recherche'
            )
            ->first();
    }

    public static function getServicePeiToTable($account_id)
    {
        $query = "SELECT service_peis.id,service_peis.name,service_peis.info,service_peis.active ,service_peis.updated_at,service_peis.status, count(peis.id), concat(lastname,' ',firstname) as updated_by
        FROM public.service_peis as service_peis
        LEFT JOIN account_" . $account_id . ".peis as peis ON peis.service_pei_id = service_peis.id
        LEFT JOIN public.users ON users.id = service_peis.updated_by
        WHERE service_peis.account_id = " . $account_id . "
        GROUP BY service_peis.id, users.firstname, users.lastname";

        return DB::connection()->select($query);
    }

    public static function getAllFromAccountToSelect($account_id, $active = 1)
    {
        $service_peis_models = ServicePei::where('account_id', $account_id)
            ->where('active', $active)
            ->whereIn('geometry',['Point','MultiPoint'])
            ->get();

        $service_peis = [];
        foreach ($service_peis_models as $key => $service_pei_model) {
            $service_pei = (object) [];

            $service_pei->value = $service_pei_model->id;
            $service_pei->name = $service_pei_model->name;
            $service_peis[] = $service_pei;
        }

        return collect($service_peis);
    }
}
