<?php
namespace App\Repositories;

use App\Models\IsoType;

class IsoTypeRepository
{
    public static function getAllToSelect()
    {
        $iso_type_models = IsoType::all();

        $iso_types = [];
        foreach ($iso_type_models as $key => $iso_type_model) {
            $iso_type = (object) [];

            $iso_type->value = $iso_type_model->id;
            $iso_type->name = $iso_type_model->name;
            $iso_types[] = $iso_type;
        }

        return collect($iso_types);
    }
}
