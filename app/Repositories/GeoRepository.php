<?php
namespace App\Repositories;

use App\Models\GeoData\Batiment;
use App\Models\GeoData\CarroPop;
use geoPHP;

class GeoRepository
{
    public static function getProperties($wkt)
    {
        $geoObj = geoPHP::load($wkt, 'wkt');
        $population_2010 = CarroPop::getPolyPopulation($geoObj,2010);
        $population_2015 = CarroPop::getPolyPopulation($geoObj,2015);

        $batiments = Batiment::getPolyCount($geoObj);
        $res = [];

        if (isset($population_2010)){
            foreach ($population_2010 as $k => $v) {
                $res['indindicators.insee.' . $k] = $v;
            }
        }

        if (isset($population_2015)) {
            foreach ($population_2015 as $k => $v) {
                $res['indindicators.insee_2015.' . $k] = $v;
            }
        }

        foreach ($batiments as $k => $v) {
            $res['indindicators.batiments.' . $k] = $v;
        }

        return $res;
    }
}
