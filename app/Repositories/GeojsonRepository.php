<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Array_;

class GeojsonRepository
{
    /**
     * Cette methode va retourner un tableau avec les coordonnées d'un point
     *
     * @param $file
     * @param $index
     * @return array
     */
    public static function getCoordinatesOfElementByIndex($file, $index)
    {
        $x_coordinate = $file['features'][$index]['geometry']['coordinates'][0];
        $y_coordinate = $file['features'][$index]['geometry']['coordinates'][1];
        $coordinates = [$x_coordinate, $y_coordinate];
        return $coordinates;
    }



    /**
     * Cette methode va permettre de convertir un fichier csv en fichier Json. Il sera stocké dans accounts/1/servicePei/
     *
     * @param $file
     * @param $service_pei
     * @return mixed
     */
    public static function convertCsvToGeoJson($file, $service_pei)
    {
        $name_file = substr($file, 14);
        $content_file = Storage::get($file);
        $data = str_getcsv($content_file, "\n");
        //je transforme le fichier csv en tableau ligne par ligne
        $array_addresses = array_map('str_getcsv', $data);
        //je supprime tous les titres de la premiere ligne
        array_shift($array_addresses);

        //chaque ligne correspond à une adresse, je vais donc parcourir chaque ligne et le convertir au format souhaité
        $addresses = [];
        foreach ($array_addresses as $line_address) {
            $line = explode(";", $line_address[0]);
            $address = $line[2].' '.$line[3].' '.$line[4];
            $name = $line[1];
            $identifier = $line[0];

            //je remplace les espaces par des + pour l ajouter à la commande de l'api
            $validAddress = str_replace(" ", "+", $address);

            //je fais un appel à l'api pour recupere les coordonnes de cette adresse
            $url = 'https://api-adresse.data.gouv.fr/search/?q='.$validAddress;
            $json = file_get_contents($url);
            $value = json_decode($json);

            if ($value) {
                $validated['latitude'] = $value->features[0]->geometry->coordinates[1];
                $validated['longitude'] = $value->features[0]->geometry->coordinates[0];
            }

            //j enregistre l adresse à la suite des precedentes adresses dans un tableau avec le format geojson
            $addresses [] = ['type'=>'Feature', 'properties'=>['identifier enterprise'=>$identifier, 'name'=>$name, 'address'=>$address], 'geometry'=>['type'=>'Point', 'coordinates'=>[$validated['longitude'], $validated['latitude']]
            ]];
        }
        //dd($addresses);
        $addresses_collection = ['type'=>'FeatureCollection', 'features'=>$addresses];
        $addresses_collection_json = json_encode($addresses_collection);

        $path = 'accounts/1/servicePei'.$name_file;
        //j enregistre le fichier convertit au format geojson
        Storage::disk('local')->put($path, $addresses_collection_json);
        //je retourne le nom de fichier pour l enregistrer dans la table service_peis
        return $path;
    }
}
