<?php

namespace App\Jobs;

use App\Helpers\ConvertHelper;
use App\Mail\PeiGeocodeError;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ProcessConvertCSVtoGeoJson implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;
    protected $service_pei;
    protected $user;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param mixed $service_pei
     * @param mixed $user
     * @return void
     */
    public function __construct($service_pei, $user)
    {
        $this->service_pei = $service_pei;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->service_pei->path;
        $account_id = $this->service_pei->account_id;
        $contents = null;
        $name_file = substr($path, 15);

        if (isset($path)) {
            $content = Storage::get($path);

            $data = ConvertHelper::csvToJson($content);

            //chaque ligne correspond à une adresse, je vais donc parcourir chaque ligne et le convertir au format souhaité
            $addresses = [];
            $errors = [];
            foreach ($data as $line) {
                $line_upper = array_change_key_case( $line ,CASE_UPPER);
                //si les coordonnees wgs84 existent, je les recuperent et ajoute l'adresse de ce point.
                if (isset($line_upper["X"]) && ($line_upper["X"] != "") && isset($line_upper["Y"]) && ($line_upper["Y"]!= "")){
                    $addresses[] = ['type' => 'Feature', 'properties' => [$line], 'geometry' => ['type' => 'Point', 'coordinates' => [$line_upper["X"], $line_upper["Y"]],
                    ]];
                }
                elseif (isset($line_upper["ADRESSE"]) && isset($line_upper["CODE POSTAL"]) && isset($line_upper["COMMUNE"])) {
                    //si le code postal ne contient que 4 chiffres, je lui ajoute le 0 devant.
                    if (strlen($line_upper["CODE POSTAL"]) === 4) {
                        $line_upper["CODE POSTAL"] = '0' . $line_upper["CODE POSTAL"];
                    }
                    $address_error = "L'adresse : " . $line_upper["ADRESSE"] . ' ' . $line_upper["COMMUNE"] . ' ' . $line_upper["CODE POSTAL"];
                    $address = urlencode($line_upper["ADRESSE"]) . ' ' . urlencode($line_upper["COMMUNE"]) . '&postcode=' . $line_upper["CODE POSTAL"];
                    $length_all_address = strlen($address);
                    //limite max de ce champ à 200 caractères (api data gouv) pour l adresse complète
                    if ($length_all_address > 200) {
                        $address = urlencode($line_upper["COMMUNE"]) . '&postcode=' . $line_upper["CODE POSTAL"];
                        // limite max à 200 sinon on laisse que le code postal
                        if (strlen($address) > 200) {
                            $address = '&postcode=' . $line_upper["CODE POSTAL"];
                        }
                    }

                    if (strlen($address) < 200) {
                        //je remplace les espaces par des + pour l ajouter à la commande de l'api
                        $validAddress = str_replace(" ", "+", $address);

                        //je fais un appel à l'api pour recupere les coordonnes de cette adresse
                        $url = 'https://api-adresse.data.gouv.fr/search/?q=' . $validAddress;

                        $json = file_get_contents($url);
                        $value = json_decode($json);

                        if ($value) {
                            if (!empty($value->features)) {
                                $validated['latitude'] = $value->features[0]->geometry->coordinates[1];
                                $validated['longitude'] = $value->features[0]->geometry->coordinates[0];
                                foreach ($value->features[0]->properties as $key => $property) {
                                    $line["ban." . $key] = $property;
                                }
                                //j enregistre l adresse à la suite des precedentes adresses dans un tableau avec le format geojson
                                $addresses[] = ['type' => 'Feature', 'properties' => [$line], 'geometry' => ['type' => 'Point', 'coordinates' => [$validated['longitude'], $validated['latitude']],
                                ]];
                            } else {
                                $errors[] = $address_error . " n'a pas pu être géocodée";
                            }
                        } else {
                            $errors[] = $address_error . " n'a pas pu être géocodée";
                        }
                    }
                }
            }
            if ($errors !== []) {
                Mail::to($this->user->email)->send(new PeiGeocodeError($this->service_pei, $errors));
            }
            $addresses_collection = ['type' => 'FeatureCollection', 'features' => $addresses];
            $addresses_collection_json = json_encode($addresses_collection);

            $path = 'accounts/' . $account_id . '/servicePei/' . $name_file;
            //j enregistre le fichier convertit au format geojson
            Storage::disk('local')->put($path, $addresses_collection_json);

            // je modifie les attributs du service_pei
            $this->service_pei->path = $path;
            $this->service_pei->save();
            ProcessServicePEI::dispatch($this->service_pei)->onQueue('high_' . $this->service_pei->account_id);
        }
    }
}
