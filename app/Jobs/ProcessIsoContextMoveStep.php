<?php

namespace App\Jobs;

use App\Jobs\ProcessBoardLayer;
use App\Mail\ProcessEnd;
use App\Models\BoardLayer;
use App\Models\GeoData\Batiment;
use App\Models\GeoData\CarroPop;
use App\Models\IsochroneContext;
use App\Models\IsoContextMoveStep;
use App\Models\IsoContextMoveTypes;
use App\Models\IsoContextServices;
use App\Models\IsoType;
use App\Models\MoveType;
use App\Models\ServicePei;
use App\Models\TmpIso;
use Carbon\Carbon;
use geoPHP;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Zttp\Zttp;

class ProcessIsoContextMoveStep implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $iso_context_move_step;
    /**
     * Valeur de la zone de l'iso (min or m)
     *
     * @var int
     */
    public $deleteWhenMissingModels = true;
    protected $value;
    protected $iso_context_move_type;
    protected $iso_context_service;
    protected $iso_context;
    protected $user;
    public $tries = 5;

    protected $isoType;
    protected $moveType;
    /**
     * Schema Postgres
     *
     * @var string
     */
    protected $schema;

    /**
     * Create a new job instance.
     *
     * @param null|mixed $user
     * @return void
     */
    public function __construct(IsoContextMoveStep $iso_context_move_step, $user = null)
    {
        $this->iso_context_move_step = $iso_context_move_step;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle( /*LoggerInterface $logger*/)
    {
        $this->value = $this->iso_context_move_step->value;

        $this->iso_context_move_step->status = 2;

        $this->iso_context_move_step->save();
        $iso_context_move_type_id = $this->iso_context_move_step->iso_context_move_type_id;

        // IsoContextMoveType est une table de liaison entre move_type (pieton / voiture) et une table de lien entre contexte et service.
        //enregistré dans $this->iso_context_move_type
        $this->getIsoContextMoveType($iso_context_move_type_id);
        // IsoContextService est la table de lien entre service et contexte
        // enregistré dans $this->iso_context_move_type
        $this->getIsoContextService($this->iso_context_move_type->iso_context_service_id);
        // Ca c'est le service
        //enregistré dans $this->service_pei
        $this->getServicePei($this->iso_context_service->service_pei_id);
        // Le contexte lui-même, dans $this->iso_context
        $this->getIsochroneContext($this->iso_context_service->iso_context_id);
        // statut démarré
        $this->iso_context->status = IsochroneContext::STATUS_STARTED;
        $this->iso_context->save();
        // Le type de contexte : isochrone ou isodistance
        $iso_type_id = $this->iso_context->iso_type_id;

        // Le compte du service / contexte
        $this->account_id = $this->service_pei->account_id;

        // Quel schema utiliser
        $this->schema = 'account_' . $this->account_id;

        // Le type de mouvement (piéton / voiture
        $move_type_id = $this->iso_context_move_type->move_type_id;
        // On récupère encore le type d'iso (distance ou temps) et le type de mouvement (piéton ou voiture)
        $this->isoType = IsoType::find($iso_type_id);
        $this->moveType = MoveType::find($move_type_id);
        $load_full = $this->iso_context->load_full;

        $table_name = 'peis';

        // On récupère les points du service
        $query = 'SELECT id,ST_AsText(geom) FROM ' . $this->schema . '.' . $table_name . ' WHERE service_pei_id = ' . $this->service_pei->id;

        $peis = DB::connection()->select($query);

        // C'est ça qui appelle l'api IGN et renvoie une union des isochrones ?
        $fullWkt = $this->getAndUnionIsoPeis($peis);
        if ($fullWkt == "") {
            $this->fail(new \Exception('Empty geo', 10254));
        }

        // Met à jour ou créé la couche avec les points du service ?
        $this->layer_id = $this->insertOrUpdateLayer($fullWkt, $this->iso_context_move_step->id);

        $board_layers = BoardLayer::where('layer_id', $this->layer_id)->get();
        // On lance les calculs pour tous les contextes de recherche qui dépendent de ce service.
        foreach ($board_layers as $key => $board_layer) {
            ProcessBoardLayer::dispatch($board_layer->id)->onQueue('default_' . $this->account_id);
        }
        // On récupère à nouveau la géométrie de la couche qu'on vient d'insérer ?
        $query = 'SELECT id,ST_AsText(geom) FROM ' . $this->schema . '.layers WHERE id = ' . $this->layer_id;
        $layers = DB::connection()->select($query);
        $fullWkt = $layers[0]->st_astext;

        $limit = $this->getLimit();

        $decoupe_limit = $this->decoupe($fullWkt, $limit->st_astext);

        switch ($limit->zone_id) {
            case 1:
                // Si la limite est une commune

                $this->storeIso($limit->insee, $decoupe_limit, 1);
                $this->storeTerriProps($limit->insee, $limit->st_astext, 1);
                break;

            case 2:
                // Si la limite est un EPCI
                $this->storeTerriProps($limit->insee, $limit->st_astext, 2);
                $this->storeIso($limit->insee, $decoupe_limit, 2);
                if ($load_full) {
                    $this->storeIsoCommunesEPCI($limit->insee, $decoupe_limit);
                }
                break;
            case 3:
                // Si la limite est un département
                $this->storeTerriProps($limit->insee, $limit->st_astext, 3);
                $this->storeIso($limit->insee, $decoupe_limit, 3);
                if ($load_full) {
                    $this->storeIsoCommunes($limit->insee, $decoupe_limit);
                    $this->processEpcis($limit->insee, $fullWkt);
                }
                break;

            case 4:
                // Si la limite est une région
                $this->storeTerriProps($limit->insee, $limit->st_astext, 4);
                $this->storeIso($limit->insee, $decoupe_limit, 4);
                if ($load_full) {
                    $this->storeIsoDepartements($limit->insee, $decoupe_limit);
                }
                break;

            default:
                # code...
                break;
        }
        // on change à nouveau le statut, surement "terminé" ??
        $this->iso_context_move_step->status = 3;
        $this->iso_context_move_step->save();

        $query = "SELECT iso_context_move_step.* FROM iso_context_move_step
        JOIN iso_context_move_types ON iso_context_move_step.iso_context_move_type_id=iso_context_move_types.id
        JOIN iso_context_services ON iso_context_move_types.iso_context_service_id=iso_context_services.id
        WHERE iso_context_services.iso_context_id = " . $this->iso_context->id; //iso_contexts

        $iso_context_move_steps = DB::connection()->select($query);
        $status = IsochroneContext::STATUS_DONE;
        $error = false;
        foreach ($iso_context_move_steps as $iso_context_move_step) {
            if ($iso_context_move_step->status !== IsochroneContext::STATUS_DONE) {
                $status = IsochroneContext::STATUS_STARTED;
            }
            if ($iso_context_move_step->status == IsochroneContext::STATUS_ERROR) {
                $error = true;
            }
        }
        if ($error) {
            $this->iso_context->status = IsochroneContext::STATUS_ERROR;
        } else {
            if ($status == IsochroneContext::STATUS_DONE && $this->user != null) {
                $url = action('IsochroneContextController@show', [$this->iso_context->account_id, $this->iso_context->id]);
                Mail::to($this->user->email)->send(new ProcessEnd('Contexte', $this->iso_context->name, $url));
            }
            $this->iso_context->status = $status;
        }
        $this->iso_context->save();
    }

    public function processEpcis($insee, $wkt)
    {
        $query = "SELECT siren_epci FROM geo_data.epcis WHERE dep_epci = '" . $insee . "'";
        $sirens_epci = DB::connection()->select($query);

        foreach ($sirens_epci as $key => $siren_epci) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.epcis WHERE siren_epci= '" . $siren_epci->siren_epci . "'";
            $limit = DB::connection()->select($query);
            $limit = $limit[0];
            $this->storeTerriProps($siren_epci->siren_epci, $limit->st_astext, 2);
            $decoupe = $this->decoupe($wkt, $limit->st_astext);
            $this->storeIso($siren_epci->siren_epci, $decoupe, 2);
        }
    }

    public function storeIso($insee_dep, $wkt, $zone_id)
    {
        $properties = json_encode($this->getProperties($wkt));

        $query = "SELECT id,prop_id FROM " . $this->schema . ".layer_clips WHERE layer_id=" . $this->layer_id . " AND zone_id=" . $zone_id . " AND insee='" . $insee_dep . "'";
        $layer_clips = DB::connection()->select($query);
        if (empty($layer_clips)) {
            $prop = $this->storeProp($properties);
            $this->storeLayerClips($insee_dep, $prop->id, $wkt, $zone_id);
        } else {
            $prop = $this->storeProp($properties);
            $this->updateLayerClips($layer_clips[0]->id, $prop->id, $wkt);
            $this->deleteProp($layer_clips[0]->prop_id);
        }
    }

    public function storeTerriProps($insee, $wkt, $zone_id)
    {
        $properties = json_encode($this->getProperties($wkt));

        $query = "SELECT id,prop_id FROM " . $this->schema . ".territoire_props WHERE layer_id=" . $this->layer_id . " AND zone_id=" . $zone_id . " AND insee='" . $insee . "'";
        $ter_data = DB::connection()->select($query);
        if (empty($ter_data)) {
            $prop = $this->storeProp($properties);
            $this->storeTerritoireProps($insee, $prop->id, $zone_id);
        } else {
            $prop = $this->storeProp($properties);
            $this->updateTerritoireProps($ter_data[0]->id, $prop->id);
            $this->deleteProp($ter_data[0]->prop_id);
        }
    }

    public function storeTerritoireProps($insee_dep, $prop_id, $zone_id)
    {
        $query = "INSERT INTO " . $this->schema . ".territoire_props(
            layer_id, zone_id,insee, prop_id)
            VALUES ( " . $this->layer_id . "," . $zone_id . ",'" . $insee_dep . "'," . $prop_id . ")";

        DB::connection()->select($query);
    }

    public function updateTerritoireProps($ter_data_id, $prop_id)
    {
        $query = "UPDATE " . $this->schema . ".territoire_props SET  prop_id = " . $prop_id . " WHERE id=" . $ter_data_id;
        DB::connection()->select($query);
    }

    public function deleteProp($prop_id)
    {
        $query = "DELETE FROM " . $this->schema . ".props WHERE id=" . $prop_id;
        DB::connection()->select($query);
    }

    public function updateLayerClips($layer_clip_id, $prop_id, $wkt)
    {
        $query = "UPDATE " . $this->schema . ".layer_clips SET geom = ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326), prop_id = " . $prop_id . " WHERE id=" . $layer_clip_id;
        DB::connection()->select($query);
    }

    public function storeLayerClips($insee_dep, $prop_id, $wkt, $zone_id)
    {
        $query = "INSERT INTO " . $this->schema . ".layer_clips(
            layer_id, zone_id,insee, prop_id, geom)
            VALUES ( " . $this->layer_id . "," . $zone_id . ",'" . $insee_dep . "'," . $prop_id . ",ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326))";

        DB::connection()->select($query);
    }

    public function storeProp($properties)
    {
        $query = "INSERT INTO " . $this->schema . ".props (props,layer_id)
                VALUES ('" . $properties . "'," . $this->layer_id . ")";
        DB::connection()->select($query);
        $query = "SELECT id FROM " . $this->schema . ".props ORDER BY id DESC LIMIT 1";
        return DB::connection()->select($query)[0];
    }

    public function storeIsoDepartements($insee_dep, $wkt)
    {
        $query = "SELECT insee FROM geo_data.departements WHERE insee_region = '" . $insee_dep . "'";
        $departements_insee = DB::connection()->select($query);

        foreach ($departements_insee as $key => $departement_insee) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.departements WHERE insee= '" . $departement_insee->insee . "'";
            $limite = DB::connection()->select($query);
            $this->storeTerriProps($departement_insee->insee, $limite[0]->st_astext, 3);
            $decoupe = $this->decoupe($wkt, $limite[0]->st_astext);
            $this->storeIso($departement_insee->insee, $decoupe, 3);
            $this->storeIsoCommunes($departement_insee->insee, $decoupe);
            $this->processEpcis($departement_insee->insee, $wkt);
        }
    }

    public function storeIsoCommunesEPCI($siren_epci, $wkt)
    {
        $query = "SELECT insee_commune as insee FROM geo_data.epci_communes  WHERE epci_communes.siren_epci = '" . $siren_epci . "'";

        $communes_insee = DB::connection()->select($query);

        foreach ($communes_insee as $key => $commune_insee) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee= '" . $commune_insee->insee . "'";
            $limite = DB::connection()->select($query);
            $this->storeTerriProps($commune_insee->insee, $limite[0]->st_astext, 1);
            $decoupe = $this->decoupe($wkt, $limite[0]->st_astext);
            $this->storeIso($commune_insee->insee, $decoupe, 1);
        }
    }

    public function storeIsoCommunes($insee_dep, $wkt)
    {
        $query = "SELECT insee FROM geo_data.communes WHERE insee_departement = '" . $insee_dep . "'";
        $communes_insee = DB::connection()->select($query);

        foreach ($communes_insee as $key => $commune_insee) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee= '" . $commune_insee->insee . "'";
            $limite = DB::connection()->select($query);
            $this->storeTerriProps($commune_insee->insee, $limite[0]->st_astext, 1);
            $decoupe = $this->decoupe($wkt, $limite[0]->st_astext);
            $this->storeIso($commune_insee->insee, $decoupe, 1);
        }
    }

    public function insertOrUpdateLayer($wkt, $iso_context_move_step_id)
    {
        $date = Carbon::now();
        $query = "SELECT id FROM " . $this->schema . ".layers WHERE context_step_move_id = " . $iso_context_move_step_id;

        $layer = DB::connection()->select($query);

        $unit = 'm';
        $move = 'Voiture';
        if ($this->isoType->name === 'Isochrone') {
            $unit = 'min';
        }

        $move = $this->moveType->name;
        if ($move == "Piéton") {
            $move = "Pieton";
        }

        if (empty($layer)) {
            DB::table($this->schema . '.layers')->insert(
                [
                    'name' => $this->service_pei->name . " à " . $this->value . " " . $unit . " (" . $move . ")",
                    'context_step_move_id' => $iso_context_move_step_id,
                    'active' => 1,
                    'created_at' => $date,
                ]
            );

            $query = "SELECT id FROM " . $this->schema . ".layers WHERE context_step_move_id = " . $iso_context_move_step_id;

            $layer = DB::connection()->select($query);
        } else {
            DB::table($this->schema . '.layers')
                ->where('id', $layer[0]->id)
                ->update(['name' => $this->service_pei->name . " à " . $this->value . " " . $unit . " (" . $move . ")"]);
        }
        $wkt = str_replace('-0 ', '0 ', $wkt);
        $query = "UPDATE " . $this->schema . ".layers SET geom = ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326), updated_at = '" . $date . "' WHERE id=" . $layer[0]->id;
        DB::connection()->select($query);

        return $layer[0]->id;
    }

    /**
     * Découpe l'iso par rapport à un territoire donné
     *
     * @param string $obj_wkt
     * @param string $territoire_wkt
     * @return string
     */
    public function decoupe($obj_wkt, $territoire_wkt)
    {
        $query = "SELECT ST_AsText(ST_Intersection(ST_MakeValid('" . $obj_wkt . "'::geometry), ST_MakeValid('" . $territoire_wkt . "'::geometry))) as geom";

        $time_start = microtime(true);

        $res = \DB::connection('pgsql')->select(\DB::raw($query))[0];

        return $res->geom;
    }

    /**
     * Récupère les valeurs Insee et bâtiment pour une zone donner
     *
     * @param string $wkt
     * @return array
     */
    public function getProperties($wkt)
    {
        $geoObj = geoPHP::load($wkt, 'wkt');

        $population_2015 = CarroPop::getPolyPopulation($geoObj, 2015);
        $population_2010 = CarroPop::getPolyPopulation($geoObj, 2010);

        $batiments = Batiment::getPolyCount($geoObj);
        $res = [];

        if (isset($population_2010)) {
            foreach ($population_2010 as $k => $v) {
                $res['indindicators.insee.' . $k] = $v;
            }
        }

        if (isset($population_2015)) {
            foreach ($population_2015 as $k => $v) {
                $res['indindicators.insee_2015.' . $k] = $v;
            }
        }

        foreach ($batiments as $k => $v) {
            $res['indindicators.batiments.' . $k] = $v;
        }
        return $res;
    }

    /**
     * Récupère le wkt de la limite du contexte
     *
     * @return object
     */
    public function getLimit()
    {
        switch ($this->iso_context->zone_id) {
            case 1:
                $query = "SELECT id,insee,ST_AsText(geom), 1 as zone_id FROM geo_data.communes WHERE insee = '" . $this->iso_context->insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;
            case 2:
                $query = "SELECT id,siren_epci as insee,ST_AsText(geom), 2 as zone_id FROM geo_data.epcis WHERE siren_epci = '" . $this->iso_context->insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;

            case 3:
                $query = "SELECT id,insee,ST_AsText(geom), 3 as zone_id FROM geo_data.departements WHERE insee = '" . $this->iso_context->insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;
            case 4:
                $query = "SELECT id,insee,ST_AsText(geom), 4 as zone_id FROM geo_data.regions WHERE insee = '" . $this->iso_context->insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;

            default:
                # code...
                break;
        }
    }
    /**
     * Récupère les isos des Pei et effectue la fusion
     *
     * @param array $peis
     * @return string
     */
    public function getAndUnionIsoPeis($peis)
    {
        $fullWkt = "";
        $iso_context_move_step_id = $this->iso_context_move_step->id;
        $move_type_id = $this->iso_context_move_type->move_type_id;
        $iso_type_id = $this->iso_context->iso_type_id;
        $max = 100;
        $pei_nb = 0;
        $tmp_ids = [];
        foreach ($peis as $key => $pei) {
            $pei_nb++;
            $wkt = $this->getWktFromDB($pei->id, $iso_context_move_step_id);
            if ($wkt == "" || $wkt == "POLYGON EMPTY") {
                $this->getRemoveIsoFromDB($pei->id, $iso_context_move_step_id);
                $wkt = $this->callAndGetWKT($pei->st_astext, $this->value, $pei->id);
                $this->storeWktToIso($wkt, $pei->id, $iso_context_move_step_id);
            }
            if ($fullWkt == "") {
                $fullWkt = $wkt;
            } else {
                $query = "SELECT ST_AsText(ST_Union(ST_MakeValid(ST_GeomFromText('" . $fullWkt . "')),ST_MakeValid(ST_GeomFromText('" . $wkt . "'))) );";

                $fullWkt = DB::connection()->select($query)[0]->st_astext;
            }

            if ($pei_nb == $max) {
                $tmp_iso = new TmpIso();
                $tmp_iso->setSchema($this->schema);
                $tmp_iso->save();
                $query = "UPDATE " . $this->schema . ".tmp_iso SET geom = ST_SetSRID(ST_GeomFromText('" . $fullWkt . "'),4326) WHERE id=" . $tmp_iso->id;
                DB::connection()->select($query);
                $tmp_ids[] = $tmp_iso->id;
                $fullWkt = "";
                $pei_nb = 0;
            }
        }

        if ($fullWkt !== "") {
            $tmp_iso = new TmpIso();
            $tmp_iso->setSchema($this->schema);
            $tmp_iso->save();
            $query = "UPDATE " . $this->schema . ".tmp_iso SET geom = ST_SetSRID(ST_GeomFromText('" . $fullWkt . "'),4326) WHERE id=" . $tmp_iso->id;
            DB::connection()->select($query);
            $tmp_ids[] = $tmp_iso->id;
            $fullWkt = "";
            $pei_nb = 0;
        }

        if (count($tmp_ids) === 0) {
            return null;
        }

        $query = "SELECT ST_AsText(ST_Union(ST_MakeValid(geom))) FROM " . $this->schema . ".tmp_iso  WHERE id IN (" . implode(',', $tmp_ids) . ")";
        var_dump($query);
        $fullWkt = DB::connection()->select($query)[0]->st_astext;

        DB::table($this->schema . ".tmp_iso")->delete();
        return $fullWkt;
    }

    /**
     * Récupère l'iso d'un Pei de la DB
     *
     * @param int $pei_id
     * @param int $iso_context_move_type_id
     * @param mixed $iso_context_move_step_id
     * @return  string|array
     */
    public function getWktFromDB($pei_id, $iso_context_move_step_id)
    {
        $query = 'SELECT ST_AsText(geom) FROM ' . $this->schema . '.isos  ';
        $query .= 'WHERE context_step_move_id =' . $iso_context_move_step_id . ' AND pei_id=' . $pei_id;

        $result = DB::connection()->select($query);

        if (!empty($result)) {
            return $result[0]->st_astext;
        }
        return "";
    }

    public function getRemoveIsoFromDB($pei_id, $iso_context_move_step_id)
    {
        $query = 'DELETE  FROM ' . $this->schema . '.isos  ';
        $query .= 'WHERE context_step_move_id =' . $iso_context_move_step_id . ' AND pei_id=' . $pei_id;

        $result = DB::connection()->select($query);
    }
    /**
     * Enregistre l'iso dans la DB
     *
     * @param string $wkt
     * @param int $pei_id
     * @param int $iso_context_move_type_id
     */
    public function storeWktToIso($wkt, $pei_id, $iso_context_move_type_id)
    {
        $date = Carbon::now();

        $query = 'INSERT INTO ' . $this->schema . '.isos  (context_step_move_id,pei_id, geom,created_at,updated_at) VALUES ';
        $query .= "( '" . $iso_context_move_type_id . "','" . $pei_id . "' ,ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326),'" . $date . "','" . $date . "' )";

        DB::connection()->select($query);
    }

    public function getIsosWKTFromDB($pei_id, $value, $iso_type_id, $move_type_id)
    {
        $query = "SELECT ST_AsText(isos.geom) FROM " . $this->schema . ".isos AS isos
        JOIN public.iso_context_move_step AS context_move_step ON isos.context_step_move_id = context_move_step.id
        JOIN public.iso_context_move_types AS iso_context_move_types ON context_move_step.iso_context_move_type_id = iso_context_move_types.id
        JOIN public.iso_context_services AS iso_context_services ON iso_context_move_types.iso_context_service_id = iso_context_services.id
        JOIN public.iso_contexts AS iso_contexts ON  iso_context_services.iso_context_id =iso_contexts.id
        WHERE isos.pei_id = " . $pei_id . "
        AND iso_contexts.iso_type_id = " . $iso_type_id . "
        AND iso_context_move_types.move_type_id = " . $move_type_id . "
        AND context_move_step.value = " . $value . "LIMIT 1";

        $result = DB::connection()->select($query);
        if (isset($result[0])) {
            return $result[0]->st_astext;
        }
        return '';
    }

    /**
     * Appele l'IGN pour calculer l'iso
     *
     * @param string $wkt WKT du pei
     * @param int $iso_type_id
     * @param int $move_type_id
     * @param int $value
     * @param mixed $pei_id
     * @return string
     */
    public function callAndGetWKT($wkt, $value, $pei_id)
    {
        $method = "Distance";
        $request_val = "distance=" . $value;

        if ($this->isoType->name === 'Isochrone') {
            $method = "Time";
            $request_val = "time=";
            $request_val .= $value * 60;
        }

        $graphName = $this->moveType->name;
        if ($graphName == "Piéton") {
            $graphName = "Pieton";
        }

        $result_wkt = $this->getIsosWKTFromDB($pei_id, $value, $this->isoType->id, $this->moveType->id);

        if ($result_wkt != "") {
            return $result_wkt;
        }

        $key = env("IGN_KEY");
        $wkt = str_replace("POINT(", "", $wkt);
        $wkt = str_replace(")", "", $wkt);
        $lat_lng = explode(" ", $wkt);
        $url = "http://wxs.ign.fr/" . $key . "/isochrone/isochrone.json?location=" . $lat_lng[0] . ',' . $lat_lng[1] . "&method=" . $method . "&graphName=" . $graphName . "&smoothing=true&" . $request_val;
        var_dump($url);

        $json = Zttp::withBasicAuth(env("IGN_LOGIN"), env("IGN_PASS"))->get($url)->body();

        $json = json_decode($json, true);
        if (isset($json['wktGeometry'])) {
            return $json['wktGeometry'];
        }
        return "POLYGON EMPTY";
        //$this->fail(new \Exception('Could not get data from IGN', 10254));
    }

    public function getIsoContextMoveType($iso_context_move_type_id)
    {
        $this->iso_context_move_type = IsoContextMoveTypes::find($iso_context_move_type_id);
    }

    public function getIsoContextService($iso_context_service_id)
    {
        $this->iso_context_service = IsoContextServices::find($iso_context_service_id);
    }
    public function getServicePei($service_pei_id)
    {
        $this->service_pei = ServicePei::find($service_pei_id);
    }

    public function getIsochroneContext($iso_context_id)
    {
        $this->iso_context = IsochroneContext::find($iso_context_id);
    }
}
