<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ProcessCloneServicePei implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $old_service_id;
    public $new_service_id;
    public $account_id;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param mixed $old_service_id
     * @param mixed $new_service_id
     * @param mixed $account_id
     * @return void
     */
    public function __construct($old_service_id, $new_service_id, $account_id)
    {
        $this->new_service_id = $new_service_id;
        $this->old_service_id = $old_service_id;
        $this->account_id = $account_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $peis = DB::select('SELECT * FROM account_' . $this->account_id . '.peis WHERE service_pei_id =' . $this->old_service_id);
        foreach ($peis as $pei) {
            $query = 'INSERT INTO account_' . $this->account_id . '.peis  (service_pei_id,prop, geom,created_at,updated_at) VALUES ';
            $query .= "( '" . $this->new_service_id . "','" . $pei->prop . "' ,'" . $pei->geom . "','" . $pei->created_at . "','" . $pei->updated_at . "' )";
            DB::connection()->select($query);
        }
    }
}
