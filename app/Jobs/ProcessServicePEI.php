<?php

namespace App\Jobs;

use App\Helpers\GeoHelper;
use App\Jobs\ProcessBoardLayer;
use App\Jobs\ProcessIsoContextMoveStep;
use App\Models\BoardLayer;
use App\Models\IsoContextMoveStep;
use App\Models\ServicePei;
use App\Models\Tb;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProcessServicePEI implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;
    protected $service_pei;
    public $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ServicePei $service_pei)
    {
        $this->service_pei = $service_pei;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->service_pei->status = ServicePei::STATUS_STARTED;
        $this->service_pei->save();
        $date = Carbon::now();
        $path = $this->service_pei->path;
        $account_id = $this->service_pei->account_id;
        $name = 'account_' . $account_id;
        $service_pei_id = $this->service_pei->id;
        $contents = null;
        $table_name = 'peis';

        if ($path && Storage::exists($path)) {
            $contents = Storage::get($path);
        } else {
            //je controle que le fichier issu de l'url est lisible

            if ($contents = @file_get_contents($this->service_pei->source_url)) {
                //je recupere les donnees de l'url au format json et la convertit en une variable php
                $check_file = json_decode($contents);
                //je verifie que les donnees sont issues d'un geojson
                if (!isset($check_file->type)) {
                    $this->fail(new \Exception('Not a geojson file', 10254));
                }
            } else {
                $this->fail(new \Exception('Could not get geojson from server', 10253));
            }
        }

        if ($contents) {
            $data = json_decode($contents);
            // je parcours les points du fichier afin de les rajouter ou de les updater
            if ($data->features){
                foreach ($data->features as $key => $feature) {
                    $geometry = $feature->geometry;
                    $properties = $feature->properties;
                    $wkt = GeoHelper::json2wkt($geometry);
                    if (is_array($properties)) {
                        if (isset($properties[0])) {
                            $properties = json_encode($properties[0], JSON_HEX_APOS);
                        } else {
                            $properties = "{}";
                        }
                    } else {
                        $properties = json_encode($properties, JSON_HEX_APOS);
                    }

                    //je vérifie si le point existe dans la table en tenant compte du service_pei_id
                    $check = "SELECT id FROM " . $name . "." . $table_name . " WHERE " . $table_name . ".prop LIKE'" . $properties . "' AND " . $table_name . ".geom = ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326) AND " . $table_name . ".service_pei_id = " . $service_pei_id;

                    $exist = DB::connection()->select($check);

                    //si le point n'existe pas, je le rajoute dans la table
                    if (empty($exist)) {
                        $query = 'INSERT INTO ' . $name . '.' . $table_name . " (service_pei_id,prop, geom,created_at,updated_at) VALUES ";
                        $query .= "( '" . $service_pei_id . "','" . $properties . "' ,ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326),'" . $date . "','" . $date . "' )";
                        DB::connection()->select($query);
                    } else {
                        $update = "UPDATE " . $name . "." . $table_name . " SET updated_at = '" . $date . "' WHERE id = " . $exist[0]->id;
                        DB::connection()->select($update);
                    }
                }
                $this->service_pei->status = ServicePei::STATUS_DONE;
                $this->service_pei->save();
            } else {
                $this->service_pei->status = ServicePei::STATUS_ERROR;
                $this->service_pei->save();
            }

        } else {
            $this->service_pei->status = ServicePei::STATUS_ERROR;
            $this->service_pei->save();
        }
        $query = "SELECT id FROM " . $name . "." . $table_name . " WHERE updated_at < '" . $date . "' AND service_pei_id = " . $service_pei_id;
        $db_peis_ids = DB::connection()->select($query);
        $peis_id = [];
        foreach ($db_peis_ids as $key => $db_pei_id) {
            $peis_id[] = $db_pei_id->id;
        }
        if ($peis_id !== []) {
            $peis_id = implode(',', $peis_id);
            $query = "DELETE FROM " . $name . ".isos WHERE pei_id IN (" . $peis_id . ")";
            DB::connection()->select($query);
        }

        //je supprime tous les points qui n'ont pas la date updaté dans la table peis. cela veut dire qu ils n'existent plus
        $delete_points_from_table = "DELETE FROM " . $name . "." . $table_name . " WHERE updated_at < '" . $date . "' AND service_pei_id = " . $service_pei_id;
        DB::connection()->select($delete_points_from_table);

        $query = "SELECT iso_context_move_step.id FROM public.iso_context_move_step
         JOIN iso_context_move_types ON iso_context_move_step.iso_context_move_type_id=iso_context_move_types.id
         JOIN iso_context_services ON iso_context_move_types.iso_context_service_id=iso_context_services.id
         WHERE iso_context_services.service_pei_id = " . $service_pei_id;
        $iso_context_move_steps_id = DB::connection()->select($query);

        $tbs = Tb::where('account_id', $account_id)->get();
        foreach ($tbs as $tb) {
            foreach (json_decode($tb->indicators) as $indicator) {
                if ($indicator == 'table.' . $service_pei_id) {
                    $board_layers = BoardLayer::where('board_id', $tb->id)->get();
                    foreach ($board_layers as $key => $board_layer) {
                        ProcessBoardLayer::dispatch($board_layer->id)->onQueue('default_' . $account_id);
                    }
                }
            }
        }
        foreach ($iso_context_move_steps_id as $iso_context_move_step_id) {
            $iso_context_move_step = IsoContextMoveStep::find($iso_context_move_step_id->id);
            ProcessIsoContextMoveStep::dispatch($iso_context_move_step)->onQueue('default_' . $account_id);
        }
    }
}
