<?php

namespace App\Jobs;

use App\Mail\ProcessEnd;
use App\Models\BoardLayer;
use App\Models\GeoData\Batiment;
use App\Models\GeoData\CarroPop;
use App\Models\Tb;
use geoPHP;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProcessBoardLayer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;
    protected $board_layer_id;
    public $tries = 5;
    protected $board_layer;
    protected $board;
    protected $schema;
    protected $user;
    protected $indicators;

    /**
     * Create a new job instance.
     *
     * @param mixed $board_layer_id
     * @param null|mixed $user
     * @return void
     */
    public function __construct($board_layer_id, $user = null)
    {
        $this->board_layer_id = $board_layer_id;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->board_layer = BoardLayer::find($this->board_layer_id);
        if ($this->board_layer === null) {
            return;
        }
        $this->board_layer->status = BoardLayer::STATUS_STARTED;
        $this->board_layer->save();

        $this->board = Tb::find($this->board_layer->board_id);

        $this->board->status = Tb::STATUS_STARTED;
        $this->board->save();

        $this->indicators = json_decode($this->board->indicators);
        $this->schema = 'account_' . $this->board->account_id;
        $load_full = $this->board->load_full;
        $query = "SELECT id,ST_AsText(geom) FROM " . $this->schema . ".layers WHERE id = " . $this->board_layer->layer_id;

        $layer = DB::connection()->select($query);
        $layer = $layer[0];
        $limit = $this->getLimit($this->board->zone_id, $this->board->insee_limit);
        $layer_decoupe = $this->decoupe($layer->st_astext, $limit->st_astext);

        //$this->getProperties($layer_decoupe);
        //$this->getProperties($limit);
        switch ($limit->zone_id) {
            case 1:
                $this->storeLayerResult($limit->insee, $layer_decoupe, 1);
                $this->storeTerriResult($limit->insee, $limit->st_astext, 1);
                break;

            case 2:
                $this->storeTerriResult($limit->insee, $limit->st_astext, 2);
                $this->storeLayerResult($limit->insee, $layer_decoupe, 2);
                if ($load_full) {
                    $this->processCommunesEPCI($limit->insee, $layer_decoupe);
                }

                break;
            case 3:
                $this->storeTerriResult($limit->insee, $limit->st_astext, 3);

                $this->storeLayerResult($limit->insee, $layer_decoupe, 3);

                if ($load_full) {
                    $this->processCommunes($limit->insee, $layer_decoupe);
                    $this->processEpcis($limit->insee, $layer_decoupe);
                }

                break;

            case 4:
                $this->storeTerriResult($limit->insee, $limit->st_astext, 4);
                $this->storeLayerResult($limit->insee, $layer_decoupe, 4);
                if ($load_full) {
                    $this->processDepartements($limit->insee, $layer_decoupe);
                }

                break;

            default:
                # code...
                break;
        }
        $this->board_layer->status = BoardLayer::STATUS_DONE;
        $this->board_layer->save();
        $board_layers = BoardLayer::where('board_id', $this->board->id)->get();

        $status = Tb::STATUS_DONE;
        $error = false;
        foreach ($board_layers as $board_layer) {
            if ($board_layer->status !== BoardLayer::STATUS_DONE) {
                $status = Tb::STATUS_STARTED;
            }
            if ($board_layer->status == BoardLayer::STATUS_ERROR) {
                $error = true;
            }
        }
        if ($error) {
            $this->board->status = Tb::STATUS_ERROR;
        } else {
            if ($status == Tb::STATUS_DONE && $this->user != null) {
                $url = action('TbController@show', [$this->board->account_id, $this->board->id]);
                Mail::to($this->user->email)->send(new ProcessEnd('Tableau de bord', $this->board->name, $url));
            }
            $this->board->status = $status;
        }
        $this->board->save();
    }

    public function processEpcis($insee, $wkt)
    {
        $query = "SELECT siren_epci FROM geo_data.epcis WHERE dep_epci = '" . $insee . "'";
        $sirens_epci = DB::connection()->select($query);

        foreach ($sirens_epci as $key => $siren_epci) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.epcis WHERE siren_epci= '" . $siren_epci->siren_epci . "'";
            $limit = DB::connection()->select($query);
            $limit = $limit[0];
            $this->storeTerriResult($siren_epci->siren_epci, $limit->st_astext, 2);
            $decoupe = $this->decoupe($wkt, $limit->st_astext);
            $this->storeLayerResult($siren_epci->siren_epci, $decoupe, 2);
        }
    }

    public function processCommunes($insee, $wkt)
    {
        $query = "SELECT insee FROM geo_data.communes WHERE insee_departement = '" . $insee . "'";
        $communes_insee = DB::connection()->select($query);

        foreach ($communes_insee as $key => $commune_insee) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee= '" . $commune_insee->insee . "'";
            $limit = DB::connection()->select($query);
            $limit = $limit[0];
            $this->storeTerriResult($commune_insee->insee, $limit->st_astext, 1);
            $decoupe = $this->decoupe($wkt, $limit->st_astext);
            $this->storeLayerResult($commune_insee->insee, $decoupe, 1);
        }
    }

    public function processCommunesEPCI($siren_epci, $wkt)
    {
        $query = "SELECT insee_commune as insee FROM geo_data.epci_communes  WHERE epci_communes.siren_epci = '" . $siren_epci . "'";

        $communes_insee = DB::connection()->select($query);

        foreach ($communes_insee as $key => $commune_insee) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee= '" . $commune_insee->insee . "'";
            $limit = DB::connection()->select($query);

            $limit = $limit[0];
            $this->storeTerriResult($commune_insee->insee, $limit->st_astext, 1);
            $decoupe = $this->decoupe($wkt, $limit->st_astext);
            $this->storeLayerResult($commune_insee->insee, $decoupe, 1);
        }
    }

    public function processDepartements($insee, $wkt)
    {
        $query = "SELECT insee FROM geo_data.departements WHERE insee_region = '" . $insee . "'";
        $departements_insee = DB::connection()->select($query);

        foreach ($departements_insee as $key => $departement_insee) {
            $query = "SELECT ST_AsText(geom) FROM geo_data.departements WHERE insee= '" . $departement_insee->insee . "'";
            $limit = DB::connection()->select($query);

            $limit = $limit[0];
            $this->storeTerriResult($departement_insee->insee, $limit->st_astext, 3);
            $decoupe = $this->decoupe($wkt, $limit->st_astext);
            $this->storeLayerResult($departement_insee->insee, $decoupe, 3);

            $this->storeIsoCommunes($departement_insee->insee, $decoupe);
            $this->processEpcis($departement_insee->insee, $wkt);
        }
    }

    /**
     * Territoire
     * @param mixed $insee
     * @param mixed $wkt
     * @param mixed $zone_id
     */

    public function storeTerriResult($insee, $wkt, $zone_id)
    {
        $properties = json_encode($this->getProperties($wkt));

        $query = "SELECT id,board_prop_id FROM " . $this->schema . ".board_territoire_props WHERE board_layer_id=" . $this->board_layer_id . " AND zone_id=" . $zone_id . " AND insee='" . $insee . "'";
        $ter_data = DB::connection()->select($query);
        if (empty($ter_data)) {
            $prop = $this->storeProp($properties);
            $this->storeTerritoireProps($insee, $prop->id, $zone_id);
        } else {
            $prop = $this->storeProp($properties);
            $this->updateTerritoireProps($ter_data[0]->id, $prop->id);
            $this->deleteProp($ter_data[0]->board_prop_id);
        }
    }

    public function storeTerritoireProps($insee, $board_prop_id, $zone_id)
    {
        $query = "INSERT INTO " . $this->schema . ".board_territoire_props(
            board_layer_id, zone_id,insee, board_prop_id)
            VALUES ( " . $this->board_layer_id . "," . $zone_id . ",'" . $insee . "'," . $board_prop_id . ")";

        DB::connection()->select($query);
    }

    public function updateTerritoireProps($ter_data_id, $board_prop_id)
    {
        $query = "UPDATE " . $this->schema . ".board_territoire_props SET  board_prop_id = " . $board_prop_id . " WHERE id=" . $ter_data_id;
        DB::connection()->select($query);
    }

    /**
     *
     * @param mixed $insee
     * @param mixed $wkt
     * @param mixed $zone_id
     */

    public function storeLayerResult($insee, $wkt, $zone_id)
    {
        $properties = json_encode($this->getProperties($wkt));

        $query = "SELECT id,board_prop_id FROM " . $this->schema . ".board_layer_clips WHERE board_layer_id=" . $this->board_layer_id . " AND zone_id=" . $zone_id . " AND insee='" . $insee . "'";
        $board_layer_clips = DB::connection()->select($query);

        if (empty($board_layer_clips)) {
            $prop = $this->storeProp($properties);
            $this->storeBoardLayerClip($insee, $prop->id, $wkt, $zone_id);
        } else {
            $prop = $this->storeProp($properties);
            $this->updateBoardLayerClip($board_layer_clips[0]->id, $prop->id, $wkt);
            $this->deleteProp($board_layer_clips[0]->board_prop_id);
        }
    }

    public function updateBoardLayerClip($id, $board_prop_id, $wkt)
    {
        $query = "UPDATE " . $this->schema . ".board_layer_clips SET geom = ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326), board_prop_id = " . $board_prop_id . " WHERE id=" . $id;
        DB::connection()->select($query);
    }

    public function storeBoardLayerClip($insee, $board_prop_id, $wkt, $zone_id)
    {
        $query = "INSERT INTO " . $this->schema . ".board_layer_clips(
            board_layer_id, zone_id,insee, board_prop_id, geom)
            VALUES ( " . $this->board_layer_id . "," . $zone_id . ",'" . $insee . "'," . $board_prop_id . ",ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326))";

        DB::connection()->select($query);
    }

    public function deleteProp($board_prop_id)
    {
        $query = "DELETE FROM " . $this->schema . ".board_props WHERE id=" . $board_prop_id;
        DB::connection()->select($query);
    }

    public function storeProp($properties)
    {
        $query = "INSERT INTO " . $this->schema . ".board_props (props,board_layer_id)
                VALUES ('" . $properties . "'," . $this->board_layer_id . ")";
        DB::connection()->select($query);
        $query = "SELECT id FROM " . $this->schema . ".board_props ORDER BY id DESC LIMIT 1";
        return DB::connection()->select($query)[0];
    }

    public function getProperties($wkt)
    {
        $geoObj = geoPHP::load($wkt, 'wkt');
        $indicators_group = [];
        foreach ($this->indicators as $indicator) {
            $indicator_ar = explode('.', $indicator);
            if (!isset($indicators_group[$indicator_ar[0]])) {
                $indicators_group[$indicator_ar[0]] = [];
            }
            if (!isset($indicators_group[$indicator_ar[0]][$indicator_ar[1]])) {
                $indicators_group[$indicator_ar[0]][$indicator_ar[1]] = [];
            }
            if (isset($indicator_ar[2])) {
                $indicators_group[$indicator_ar[0]][$indicator_ar[1]][] = $indicator_ar[2];
            }
        }
        $res = [];
        foreach ($indicators_group as $key => $indicator_group) {
            switch ($key) {
                case 'indindicators':
                    foreach ($indicator_group as $key_b => $indicators) {
                        switch ($key_b) {
                            case 'insee':
                                $population = CarroPop::getPolyPopulationAttribute($geoObj, $indicators, 2010);
                                foreach ($population as $k => $v) {
                                    $res['indindicators.insee.' . $k] = $v;
                                }
                                break;
                            case 'insee_2015':
                                $population = CarroPop::getPolyPopulationAttribute($geoObj, $indicators, 2015);
                                foreach ($population as $k => $v) {
                                    $res['indindicators.insee_2015.' . $k] = $v;
                                }
                                break;
                            case 'batiments':
                                $batiments = Batiment::getPolyCount($geoObj);

                                if (isset($batiments['nb'])) {
                                    $res["indindicators.batiments.nb"] = $batiments['nb'];
                                } else {
                                    $res["indindicators.batiments.nb"] = 0;
                                }
                                break;
                            default:
                                # code...
                                break;
                        }
                    }

                    break;
                case 'table':
                    $wkt = $geoObj->out('wkt');
                    foreach ($indicator_group as $service_pei_id => $value) {
                        $query = "SELECT count(id) FROM " . $this->schema . ".peis WHERE service_pei_id = " . $service_pei_id . " AND ST_Contains(ST_SetSRID(ST_GeomFromText('" . $wkt . "'), 4326), geom)";
                        $result = DB::connection()->select($query);
                        if (isset($result[0])) {
                            $res['table.' . $service_pei_id] = $result[0]->count;
                        } else {
                            $res['table.' . $service_pei_id] = 0;
                        }
                    }

                    break;

                default:
                    # code...
                    break;
            }
        }

        return $res;
    }

    /////

    public function decoupe($layer_wkt, $territoire_wkt)
    {
        $query = "SELECT ST_AsText(ST_Intersection(ST_MakeValid('" . $layer_wkt . "'::geometry), ST_MakeValid('" . $territoire_wkt . "'::geometry))) as geom";

        $time_start = microtime(true);

        $res = \DB::connection('pgsql')->select(\DB::raw($query))[0];

        return $res->geom;
    }

    public function getLimit($zone_id, $insee_limit)
    {
        switch ($zone_id) {
            case 1:
                $query = "SELECT id,insee,ST_AsText(geom), 1 as zone_id FROM geo_data.communes WHERE insee = '" . $insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;
            case 2:
                $query = "SELECT id,siren_epci as insee,ST_AsText(geom), 2 as zone_id FROM geo_data.epcis WHERE siren_epci = '" . $insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;

            case 3:
                $query = "SELECT id,insee,ST_AsText(geom), 3 as zone_id FROM geo_data.departements WHERE insee = '" . $insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;
            case 4:
                $query = "SELECT id,insee,ST_AsText(geom), 4 as zone_id FROM geo_data.regions WHERE insee = '" . $insee_limit . "'";
                return DB::connection()->select($query)[0];
                break;

            default:
                # code...
                break;
        }
    }
}
