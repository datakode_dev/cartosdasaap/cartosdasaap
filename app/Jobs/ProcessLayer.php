<?php

namespace App\Jobs;

use App\Helpers\GeoHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProcessLayer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $deleteWhenMissingModels = true;
    protected $layer_id;
    protected $account_id;
    protected $table_name;
    protected $wkt = null;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param mixed $layer_id
     * @param mixed $account_id
     * @return void
     */
    public function __construct($layer_id, $account_id)
    {
        $this->layer_id = $layer_id;
        $this->account_id = $account_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = Carbon::now();
        $this->table_name = 'account_' . $this->account_id . '.layers';
        $query = "SELECT path FROM " . $this->table_name . " WHERE id = " . $this->layer_id;

        $layer = DB::connection()->select($query);
        $path = $layer[0]->path;
        $exists = Storage::exists($path);
        if ($exists) {
            $geojson = Storage::get($path);
            $features = json_decode($geojson)->features;
            foreach ($features as $key => $feature) {
                $geometry = $feature->geometry;
                $wkt = GeoHelper::json2wkt($geometry);
                if ($this->wkt == null) {
                    $this->wkt = $wkt;
                } else {
                    $query = "SELECT ST_AsText(ST_Union(ST_MakeValid(ST_GeomFromText('" . $this->wkt . "')),ST_MakeValid(ST_GeomFromText('" . $wkt . "'))) );";

                    $this->wkt = DB::connection()->select($query)[0]->st_astext;
                }
            }
            $query = "UPDATE " . $this->table_name . " SET updated_at='" . $date . "' ,geom = ST_SetSRID(ST_GeomFromText('" . $this->wkt . "'),4326) WHERE id=" . $this->layer_id;
            DB::connection()->select($query);
        }
    }
}
