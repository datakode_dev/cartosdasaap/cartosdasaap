<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PeiGeocodeError extends Mailable
{
    use Queueable, SerializesModels;

    public $errors;
    public $service_pei;
    /**
     * Create a new message instance.
     *
     * @param mixed $service_pei
     * @param mixed $errors
     * @return void
     */
    public function __construct($service_pei, $errors)
    {
        $this->errors = $errors;
        $this->service_pei = $service_pei;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->service_pei->name . ": Des points n'ont pas pu être géocodés")->view('mails.service_peis.geocode_error')->with([
            'errors' => $this->errors,
            'service_pei' => $this->service_pei,
        ]);
    }
}
