<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProcessEnd extends Mailable
{
    use Queueable, SerializesModels;

    public $type;
    public $name;
    public $url;
    /**
     * Create a new message instance.
     *
     * @param mixed $type
     * @param mixed $name
     * @param mixed $url
     * @return void
     */
    public function __construct($type, $name, $url)
    {
        $this->type = $type;
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Votre " . $this->type . " " . $this->name . " est maintenant disponible")
            ->view('mails.process.end')
            ->with([
                'type' => $this->type,
                'name' => $this->name,
                'url' => $this->url,
            ]);
    }
}
