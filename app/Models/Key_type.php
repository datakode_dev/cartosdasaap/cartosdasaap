<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Key_type extends Model
{
    protected $table = 'key_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
