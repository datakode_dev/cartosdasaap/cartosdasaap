<?php

namespace App\Models;

use App\Mail\MailResetPasswordToken;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute()
    {
        return $this->lastname . " " . $this->firstname;
    }

    /**
     * Send a password reset email to the user
     * @param mixed $token
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->email)->send(new MailResetPasswordToken($token));
    }
}
