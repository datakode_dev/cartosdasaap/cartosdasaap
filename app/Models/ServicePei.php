<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicePei extends Model
{
    const STATUS_NOT_STARTED = 1;
    const STATUS_STARTED = 2;
    const STATUS_DONE = 3;
    const STATUS_ERROR = 4;


    protected $table = 'service_peis';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'source_url', 'is_simulation', 'day_between_refresh', 'account_id', //'ico_color_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
