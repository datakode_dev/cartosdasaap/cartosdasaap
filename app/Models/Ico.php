<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ico extends Model
{
    protected $table = 'icos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
