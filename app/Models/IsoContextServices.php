<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsoContextServices extends Model
{
    protected $table = 'iso_context_services';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getIsoContextMoveTypes()
    {
        if (!$this->id) {
            return collect();
        }

        return IsoContextMoveTypes::where('iso_context_service_id', $this->id)->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getMoveTypes()
    {
        if (!$this->id) {
            return collect();
        }

        $s = $this->getIsoContextMoveTypes()->map(function ($i) {
            return $i->move_type_id;
        })->toArray();

        return MoveType::whereIn('id', $s)->get();
    }
}
