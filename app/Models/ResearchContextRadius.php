<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchContextRadius extends Model
{
    protected $table = 'research_context_radius';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
