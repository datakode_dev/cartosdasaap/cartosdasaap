<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchContext extends Model
{
    protected $table = 'research_contexts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function getResearchContextServices()
    {
        if (!$this->id) {
            return collect();
        }

        return ResearchContextService::where('research_context_id', $this->id)->get();
    }

    public function getResearchContextRadiuses()
    {
        if (!$this->id) {
            return collect();
        }

        return ResearchContextRadius::where('research_context_id', $this->id)->get();
    }

    public function getServices()
    {
        if (!$this->id) {
            return collect();
        }

        $s = $this->getResearchContextServices()->map(function ($i) {
            return $i->service_pei_id;
        })->toArray();

        return ServicePei::whereIn('id', $s)->where('active', true)->get();
    }
}
