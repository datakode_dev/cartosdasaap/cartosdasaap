<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmpIso extends Model
{
    protected $table = 'tmp_iso';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function setSchema($schema)
    {
        $this->table = $schema . '.' . $this->table;
    }
}
