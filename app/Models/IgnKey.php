<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IgnKey extends Model
{
    protected $table = 'ign_keys';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'key', 'login', 'password', 'count', 'max_count', 'end_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
