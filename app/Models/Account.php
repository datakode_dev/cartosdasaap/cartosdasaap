<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function members()
    {
        return $this->hasMany('App\Models\Member');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\Role');
    }

    public function service_peis()
    {
        return $this->hasMany('App\Models\ServicePei');
    }

    public function userIsInAccount($user_id)
    {
        $members = $this->members;
        if ($members->count() == 0) {
            $result = false;
        } else {
            $filtered = $members->filter(function ($value, $key) use ($user_id) {
                return $value->user_id == $user_id && $value->active;
            });
            if ($filtered->count() > 0) {
                $result = true;
            } else {
                $result = false;
            }
        }
        return $result;
    }
}
