<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsoContextMoveStep extends Model
{
    const STATUS_NOT_STARTED = 1;
    const STATUS_STARTED = 2;
    const STATUS_DONE = 3;
    const STATUS_ERROR = 4;
    
    protected $table = 'iso_context_move_step';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
