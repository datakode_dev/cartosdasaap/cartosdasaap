<?php

namespace App\Models\GeoData;

use DB;

class CarroPop
{
    public static function getPolyPopulationAttribute($area, $indicators, $years)
    {
        $wkt = $area->out('wkt');

        $time_start = microtime(true);
        $rquery = "";

        if ($years === 2010) {
            if (in_array('nbcar_car', $indicators)) {
                $rquery .= 'count(*) as nbcar_car';
            }

            if (in_array('ind_c', $indicators)) {
                if ($rquery != '') {
                    $rquery .= ', ';
                }
                $rquery .= 'sum(ind_c::float) as ind_c';
            }

            $indicators = array_diff($indicators, array("nbcar_car", "ind_c"));

            $fields = $indicators;

            foreach ($fields as $k => $field) {
                if ($rquery != '') {
                    $rquery .= ', ';
                }
                $rquery .= 'sum(' . $field . '::float/ind_r::float*ind_c::float) as ' . $field;
            }

            $data = Population::inArea($wkt)->select(DB::raw($rquery))->first()->toArray();
        } else {
            $fields = $indicators;

            foreach ($fields as $k => $field) {
                if ($rquery != '') {
                    $rquery .= ', ';
                }
                //$rquery .= 'sum(' . $field . '::float/ind_r::float*ind_c::float) as ' . $field;
                $rquery .= 'sum(' . $field . '::float) as ' . $field;
            }

            $data = Population2015::inArea($wkt)->select(DB::raw($rquery))->first()->toArray();
        }

        $res = [];
        foreach ($data as $k => $field) {
            $res[$k] = $field + 0;
        }

        $queries = DB::getQueryLog();
        $last_query = end($queries);

        return $res;
    }

    public static function getPolyPopulation($area,$year)
    {
        $wkt = $area->out('wkt');

        $time_start = microtime(true);

        /* $data= Population::inArea($wkt)->sum('car_m_ind_c');
        $value= round($data);*/
        $rquery='';

        if ($year === 2010) {
            $fields = [
                'ind_c',
                'nbcar_car',
                'men', 'men_surf', 'men_occ5', 'men_coll', 'men_5ind', 'men_1ind', 'men_prop', 'men_basr',
                'ind_r',
                'ind_age1', 'ind_age2', 'ind_age3', 'ind_age4', 'ind_age5', 'ind_age6', 'ind_age7', 'ind_age8', 'ind_srf',
            ];

            foreach ($fields as $k => $field) {
                if ($rquery != '') {
                    $rquery .= ', ';
                }
                $rquery .= 'sum(' . $field . '::float/ind_r::float*ind_c::float) as ' . $field;
            }

            $data = Population::inArea($wkt)->select(DB::raw($rquery))->first()->toArray();

        } else {
            $fields = [
                //'idinspire','id_carr1km','i_est_cr','id_carr_n','groupe','depcom','i_pauv','id_car2010',
                'ind','men','men_pauv',
                'men_1ind','men_5ind','men_prop','men_fmp','ind_snv','men_surf','men_coll','men_mais','log_av45','log_45_70',
                'log_70_90','log_ap90','log_inc','log_soc','ind_0_3','ind_4_5','ind_6_10','ind_11_17','ind_18_24','ind_25_39',
                'ind_40_54','ind_55_64','ind_65_79','ind_80p','ind_inc'
                //,'i_est_1km'
            ];
            foreach ($fields as $k => $field) {
                if ($rquery != '') {
                    $rquery .= ', ';
                }
                $rquery .= 'sum(' . $field . '::float) as ' . $field;
            }

            $data = Population2015::inArea($wkt)->select(DB::raw($rquery))->first()->toArray();
        }

        $res = [];
        foreach ($data as $k => $field) {
            $res[$k] = $field;
        }

        $queries = DB::getQueryLog();
        $last_query = end($queries);

        return $res;
    }
}
