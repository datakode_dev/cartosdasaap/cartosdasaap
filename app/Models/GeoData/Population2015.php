<?php
namespace App\Models\GeoData;

use Illuminate\Database\Eloquent\Model;

class Population2015 extends Model
{
    protected $table = 'geo_data.new_insee_caropop';

    public static function inArea($poly, $format = 'wkt')
    {
        $r = "ST_Intersects(ST_SetSRID(ST_Buffer(ST_GeomFromText('" . $poly . "'), 0), 4326), geom)";
        return self::whereRaw($r);
    }
}
