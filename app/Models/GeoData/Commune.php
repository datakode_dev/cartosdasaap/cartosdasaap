<?php

namespace App\Models\GeoData;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $table = 'geo_data.communes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
