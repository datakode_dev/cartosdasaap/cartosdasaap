<?php
namespace App\Models\GeoData;

use DB;
use Illuminate\Database\Eloquent\Model;

class Batiment extends Model
{
    protected $table = 'geo_data.pci_batiment';

    public static function inArea($poly, $format = 'wkt')
    {
        $r = "ST_Intersects(ST_SetSRID(ST_Buffer(ST_GeomFromText('" . $poly . "'),0), 4326), geom)";
        return self::whereRaw($r);
    }

    public static function getPolyCount($area)
    {
        $wkt = $area->out('wkt');

        $time_start = microtime(true);

        $rquery = 'count(*) as nb';

        $data = self::inArea($wkt)->select(DB::raw($rquery))->first()->toArray();

        //dd(self::inArea($wkt)->select(DB::raw($rquery))->toSql());
        $count = $data['nb'];

        $queries = DB::getQueryLog();
        $last_query = end($queries);


        return $data;
    }
}
