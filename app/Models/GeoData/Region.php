<?php

namespace App\Models\GeoData;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'geo_data.regions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
