<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsoContextMoveTypes extends Model
{
    protected $table = 'iso_context_move_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function getIsoContextMoveSteps()
    {
        return IsoContextMoveStep::where('iso_context_move_type_id', $this->id)->get();
    }
}
