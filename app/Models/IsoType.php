<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsoType extends Model
{
    protected $table = 'iso_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
