<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceMember extends Model
{
    protected $table = 'service_members';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_id',
        'member_id',
        'account_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
