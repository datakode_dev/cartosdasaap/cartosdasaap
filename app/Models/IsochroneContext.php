<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsochroneContext extends Model
{
    const STATUS_NOT_STARTED = 1;
    const STATUS_STARTED = 2;
    const STATUS_DONE = 3;
    const STATUS_ERROR = 4;

    protected $table = 'iso_contexts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function getServices()
    {
        if (!$this->id) {
            return collect();
        }

        $s = $this->getIsoContextServices()->map(function ($i) {
            return $i->service_pei_id;
        })->toArray();

        return ServicePei::whereIn('id', $s)->get();
    }

    public function getIsoContextServices()
    {
        if (!$this->id) {
            return collect();
        }

        return IsoContextServices::where('iso_context_id', $this->id)->get();
    }
}
