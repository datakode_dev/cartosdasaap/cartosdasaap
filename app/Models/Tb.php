<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tb extends Model
{
    const STATUS_NOT_STARTED = 1;
    const STATUS_STARTED = 2;
    const STATUS_DONE = 3;
    const STATUS_ERROR = 4;
    protected $table = 'boards';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'insee_limit', 'zone_id', 'indicators',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function getLayers()
    {
        if ($this->account_id === null || $this->id === null) {
            return [];
        }
        $boardLayers = BoardLayer::where('board_id', $this->id)->get()->map(function ($bl) {
            return $bl->layer_id;
        });
        $layers = new Layer();
        $layers->setTableName($this->account_id);
        return $layers->whereIn('id', $boardLayers)->get();
    }
}
