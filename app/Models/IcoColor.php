<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IcoColor extends Model
{
    protected $table = 'icos_colors';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'ico_id',
        'color_id',
        'account_id',
        'is_real',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
