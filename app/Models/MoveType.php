<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoveType extends Model
{
    protected $table = 'move_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
