<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Member;
use Bluora\LaravelGitInfo\Facade as Git;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Debugbar;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data;
    protected $current_user;
    protected $current_member;
    protected $current_role;
    protected $current_account;
    protected $current_permissions;
    protected $breadcrumb;
    public $tabindex = 1;

    /**
     * Initialise les principales variables de l'application
     *
     * @return void
     */
    public function init()
    {
        Bugsnag::setAppVersion(Git::version());
        Debugbar::startMeasure('init', 'Controller@init');
        $parameters = request()->route()->parameters;
        $this->current_user = auth()->user();

        $this->data = [];
        $this->data["current_user"] = $this->current_user;

        if (isset($parameters['account_id'])) {
            $this->current_account = Account::find($parameters['account_id']);
            if (isset($this->current_account)) {
                $this->current_member = Member::where('account_id', $parameters['account_id'])
                    ->where('user_id', $this->current_user->id)
                    ->where('active', 1)
                    ->first();
                if (isset($this->current_member)) {
                    $this->current_role = $this->current_member->role;
                    $this->current_permissions = $this->current_role->permissions;
                    $this->current_account->url = action('AccountController@home', $this->current_account);
                    $this->data["current_account"] = $this->current_account;
                    $this->data["current_member"] = $this->current_member;
                    $this->data["current_role"] = $this->current_role;
                    $this->breadcrumb = collect([]);
                    $active = false;
                    if (request()->route()->action['as'] == "accounts.home") {
                        $active = true;
                    }
                    $this->breadcrumb[] = (object) ["link" => route('accounts.home', [$this->current_account->id]), "name" => 'Accueil', "active" => $active];
                    $this->addBugsnagDataArray('account', [
                        'id' => $this->current_account->id,
                        'name' => $this->current_account->name,
                    ]);

                } else {
                    abort(404, "Page inaccessible");
                }
            } else {
                abort(404, "Page inaccessible");
            }

        }

        $this->buildMenu();
        Debugbar::stopMeasure('init');
    }
    /**
     * Ajoute un tableau au log bugsnag
     *
     * @param string $name
     * @param array $values
     * @return void
     */
    public function addBugsnagDataArray($name, $values)
    {
        Bugsnag::registerCallback(function ($report) use ($name, $values) {
            $report->setMetaData([
                $name => [
                    $values,
                ],
            ]);
        });
    }
    /**
     * Ajoute un élément au log bugsnag
     *
     * @param string $name
     * @param string $value
     * @return void
     */
    public function addBugsnagData($name, $value)
    {
        Bugsnag::registerCallback(function ($report) {
            $report->setMetaData([
                $name => $value,
            ]);
        });
    }

    /**
     * Vérifie une permission et redirige sur une 403 ci l'utilisateur n'en dispose pas
     *
     * @param String $permission_slug Slug de la permission
     * @return boolean
     */
    public function hasPermissionOrAbord(String $permission_slug)
    {
        if (!$this->hasPermission($permission_slug)) {
            abort(403, 'Unauthorized action.');
        }
    }
    /**
     * Vérifie ci l'utilisateur dispose d'une permission
     *
     * @param String $permission_slug Slug de la permission
     * @return boolean
     */
    public function hasPermission(String $permission_slug)
    {
        if (($this->data['current_user']->is_superadmin) === true) {
            return true;
        }
        $permission = $this->current_permissions->where('slug', $permission_slug)->first();
        $result = false;
        if ($permission !== null) {
            $result = true;
        }
        return $result;
    }
    /**
     * Gestion de l'affichage de la page
     *
     * @return void
     */
    public function render()
    {
        if (isset($this->breadcrumb)) {
            $this->data['breadcrumb'] = $this->breadcrumb;
        }
        $this->data['tabindex'] = $this->tabindex;
        $view = "controllers." . Route::currentRouteName();
        foreach ($this->data as $key => $object) {
            \Debugbar::addMessage($object, $key);
        }
        return view($view, $this->data);
    }
    /**
     * Construit le menu
     *
     * @return void
     */
    public function buildMenu()
    {
        $menu = config('adminlte.menu');

        if (isset($this->current_account)) {
            $menu = $this->accountMenu();
        } else {
            if ($this->current_user->is_superadmin) {
                $menu = [
                    "Menu",
                    [
                        'text' => 'ACCOUNT',
                        'icon' => 'share',
                        'tabindex' => $this->tabindex++,
                        'submenu' =>
                        [
                            [
                                'text' => 'Create Account',
                                'url' => action('AccountController@create'),
                                'icon' => 'user',
                                'tabindex' => $this->tabindex++,
                            ],
                            [
                                'text' => 'List Accounts',
                                'url' => action('AccountController@index'),
                                'icon' => 'lock',
                                'tabindex' => $this->tabindex++,
                            ],
                        ],
                    ],
                    [
                        'text' => 'Permissions',
                        'icon' => 'lock',
                        'tabindex' => $this->tabindex++,
                        'submenu' =>
                        [
                            [
                                'text' => 'Ajouter une permission',
                                'url' => action('PermissionController@create'),
                                'icon' => 'user',
                                'tabindex' => $this->tabindex++,
                            ],
                            [
                                'text' => 'Liste des permissions',
                                'url' => action('PermissionController@index'),
                                'icon' => 'lock',
                                'tabindex' => $this->tabindex++,
                            ],
                            [
                                'text' => 'Gérer les utilisateurs',
                                'url' => action('PermissionController@manage'),
                                'icon' => 'cog',
                                'tabindex' => $this->tabindex++,
                            ],
                        ],
                    ],
                    [
                        'text' => 'Test',
                        'icon' => 'share',
                        'tabindex' => $this->tabindex++,
                        'submenu' =>
                        [
                            [
                                'text' => 'Test envois de mail',
                                'url' => action('TestController@mail'),
                                'icon' => 'user',
                                'tabindex' => $this->tabindex++,
                            ],
                        ],
                    ],
                ];
            }
        }
        config(['adminlte.menu' => $menu]);
    }
    /**
     * Construit le menu d'un compte
     *
     * @return void
     */
    public function accountMenu()
    {
        $menu = [];
        $menu = $this->addMenuService($menu);
        $menu = $this->addMenuContext($menu);
        //$menu = $this->addMenuTB($menu);

        $can_edit = $this->hasPermission("can_edit");
        if ($can_edit == true) {
            $menu = $this->addMenuParams($menu);
        }
        return $menu;
    }
    /**
     * Construit le menu des tableaux de bord
     *
     * @param array $menu
     * @return array
     */
    public function addMenuTB($menu)
    {
        $can_edit = $this->hasPermission("can_edit");
        if ($can_edit == true) {
            $menu[] = [
                'text' => 'Tableaux de bord',
                'icon' => 'tachometer',
                'tabindex' => $this->tabindex++,
                'submenu' =>
                [
                    [
                        'text' => 'Tous les tableaux de bord',
                        'url' => action('TbController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Créer un tableau de bord',
                        'url' => action('TbController@create', [$this->current_account->id]),
                        'icon' => 'pencil-square-o',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Gestions des couches',
                        'url' => action('LayerController@index', [$this->current_account->id]),
                        'icon' => 'cogs',
                        'tabindex' => $this->tabindex++,
                    ],
                ],
            ];
        } else {
            $menu[] = [
                'text' => 'Tableaux de bord',
                'icon' => 'tachometer',
                'tabindex' => $this->tabindex++,
                'submenu' =>
                [
                    [
                        'text' => 'Tous les tableaux de bord',
                        'url' => action('TbController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Gestions des couches',
                        'url' => action('LayerController@index', [$this->current_account->id]),
                        'icon' => 'cogs',
                        'tabindex' => $this->tabindex++,
                    ],
                ],
            ];
        }
        return $menu;
    }
    /**
     * Construit le menu des paramètres
     *
     * @param array $menu
     * @return array
     */
    public function addMenuParams($menu)
    {
        $menu[] = [
            'text' => 'Paramètres',
            'icon' => 'cog',
            'tabindex' => $this->tabindex++,
            'submenu' =>
            [
                [
                    'text' => 'Utilisateurs',
                    'url' => action('MemberController@index', [$this->current_account->id]),
                    'icon' => 'users',
                    'tabindex' => $this->tabindex++,
                ],
                [
                    'text' => 'Rôles',
                    'url' => action('RoleController@index', [$this->current_account->id]),
                    'icon' => 'user-circle-o',
                    'tabindex' => $this->tabindex++,
                ],
                /*[
                    'text' => 'Marqueurs',
                    'url' => action('IcoColorController@index', [$this->current_account->id]),
                    'icon' => 'paint-brush',
                    'tabindex' => $this->tabindex++,
                ],*/
                [
                    'text' => 'Liste des clés IGN',
                    'url' => action('IgnKeyController@index', [$this->current_account->id]),
                    'icon' => 'key',
                    'tabindex' => $this->tabindex++,
                ],

            ],

        ];
        return $menu;
    }
    /**
     * Construit le menu des contexts
     *
     * @param array $menu
     * @return array
     */
    public function addMenuContext($menu)
    {
        $can_edit = $this->hasPermission("can_edit");
        if ($can_edit == true) {
            $menu[] = [
                'text' => 'Contextes',
                'icon' => 'share-alt',
                'tabindex' => $this->tabindex++,
                'submenu' =>
                [
                    [
                        'text' => 'Tous les contextes',
                        'url' => action('IsochroneContextController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Nouveau contexte',
                        'url' => action('IsochroneContextController@create', [$this->current_account->id]),
                        'icon' => 'pencil-square-o',
                        'tabindex' => $this->tabindex++,
                    ],

                    [
                        'text' => 'Tous les contextes de recherche',
                        'url' => action('ResearchContextsController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Nouveau contexte de recherche',
                        'url' => action('ResearchContextsController@create', [$this->current_account->id]),
                        'icon' => 'pencil-square-o',
                        'tabindex' => $this->tabindex++,
                    ],
                ],
            ];
        } else {
            $menu[] = [
                'text' => 'Contextes',
                'icon' => 'share-alt',
                'tabindex' => $this->tabindex++,
                'submenu' =>
                [
                    [
                        'text' => 'Tous les contextes',
                        'url' => action('IsochroneContextController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Tous les contextes de recherche',
                        'url' => action('ResearchContextsController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],

                ],
            ];
        }

        return $menu;
    }

    /**
     * Construit le menu des services
     *
     * @param array $menu
     * @return array
     */
    public function addMenuService($menu)
    {
        $can_edit = $this->hasPermission("can_edit");
        if ($can_edit == true) {
            $menu[] = [
                'text' => 'Services',
                'icon' => 'upload',
                'tabindex' => $this->tabindex++,
                'submenu' =>
                [
                    [
                        'text' => 'Tous les services',
                        'url' => action('ServicePeiController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                    [
                        'text' => 'Nouveau service',
                        'url' => action('ServicePeiController@create', [$this->current_account->id]),
                        'icon' => 'pencil-square-o',
                        'tabindex' => $this->tabindex++,
                    ],
                ],
            ];
        } else {
            $menu[] = [
                'text' => 'Services',
                'icon' => 'upload',
                'tabindex' => $this->tabindex++,
                'submenu' =>
                [
                    [
                        'text' => 'Tous les services',
                        'url' => action('ServicePeiController@index', [$this->current_account->id]),
                        'icon' => 'list',
                        'tabindex' => $this->tabindex++,
                    ],
                ],
            ];
        }

        return $menu;
    }
}
