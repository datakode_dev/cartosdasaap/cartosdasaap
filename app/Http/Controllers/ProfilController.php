<?php

namespace App\Http\Controllers;

use App\Helpers\HtmlHelper;
use App\Http\Requests\UpdateProfil;
use App\Models\User;
use Debugbar;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Cette methode va permettre d'editer les informations de l'utilisateur.
     *
     * @return void
     */
    public function edit()
    {
        $this->init();

        return $this->render();
    }

    /**
     * Methode qui va mettre à jour les informations de l utilisateur
     *
     * @param UpdateProfil $request
     * @return void
     */
    public function update(UpdateProfil $request)
    {
        Debugbar::startMeasure('Controller', 'ProfilController@update');
        $this->init();
        $validated = $request->validated();

        // je recupere les nouvelles informations de l'utilisateur
        $firstName = $validated["firstName"];
        $lastName = $validated["lastName"];
        $email = $validated["user_email"];
        $newPassword = $validated["newPassword"];
        //$newPasswordConfirmation = $validated["newPasswordConfirmation"];

        //je recupere l identifiant de l'utilisateur
        $id = $this->current_user->id;

        //je mets à jour les infos de l'utilisateurs
        User::where('id', $id)
            ->update([
                'firstname' => $firstName,
                'lastname' => $lastName,
                'email' => $email]);

        // je modifie le mdp de l utilisateur s'il n est pas nul
        if ($newPassword) {
            User::where('id', $id)
                ->update(['password' => bcrypt($newPassword)]);
        }

        Debugbar::stopMeasure('Controller');
        return redirect(action('ProfilController@show'));
    }

    /**
     * Cette methode va afficher un membre avec son nom, prénom, mail et son rôle
     *
     * @return void
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'ProfilController@show');
        $this->init();

        // je recupere l image en fonction de son adresse mail
        $avatar = HtmlHelper::getGravatarUrl($this->current_user->email);

        $this->data["avatar"] = $avatar;

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }
}
