<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIsochroneContext;
use App\Http\Requests\UpdateIsochroneContext;
use App\Jobs\ProcessIsoContextMoveStep;
use App\Models\IsochroneContext;
use App\Models\IsoContextMoveStep;
use App\Models\IsoContextMoveTypes;
use App\Models\IsoContextServices;
use App\Models\IsoType;
use App\Models\ServicePei;
use App\Repositories\ContextRepository;
use App\Repositories\InseeRepository;
use App\Repositories\IsoTypeRepository;
use App\Repositories\MoveTypeRepository;
use App\Repositories\PostgresRepository;
use App\Repositories\ServicePeiRepository;
use Debugbar;

class IsochroneContextController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des contextes
     *
     * @return void
     */
    public function index()
    {
        $this->init();
        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes', "active" => true];
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->data['url_data_table'] = action('IsochroneContextController@indexDatatable', [$this->current_account->id]);
        return $this->render();
    }

    /**
     * Renvoie les contextes de la liste
     *
     * @return void
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("can_edit");
        $iso_contexts = IsochroneContext::where('account_id', $this->current_account->id)->get();
        $iso_contexts_array = [];
        $token = csrf_token();
        foreach ($iso_contexts as $context) {
            if ($can_edit === true) {
                $links = [
                    'token' => $token,
                    'edit' => action('IsochroneContextController@edit', [$this->current_account->id, $context->id]),
                    'show' => action('IsochroneContextController@show', [$this->current_account->id, $context->id]),
                    'duplicate' => route('isochrone_contexts.duplicate', [$this->current_account->id, $context->id]),
                ];

                if ($context->active) {
                    $links["archivate"] = action('IsochroneContextController@archivate', [$this->current_account->id, $context->id]);
                } else {
                    $links["unarchivate"] = action('IsochroneContextController@unarchivate', [$this->current_account->id, $context->id]);
                    $links["delete"] = action('IsochroneContextController@delete', [$this->current_account->id, $context->id]);
                }
            } else {
                $links = [
                    'show' => action('IsochroneContextController@show', [$this->current_account->id, $context->id]),
                ];
            }

            switch ($context->status) {
                case IsochroneContext::STATUS_NOT_STARTED:
                    $context->status = ["code" => IsochroneContext::STATUS_NOT_STARTED, "label" => "Non démarré"];
                    break;
                case IsochroneContext::STATUS_STARTED:
                    $context->status = ["code" => IsochroneContext::STATUS_STARTED, "label" => "En cours de calcul"];
                    break;
                case IsochroneContext::STATUS_ERROR:
                    $context->status = ["code" => IsochroneContext::STATUS_ERROR, "label" => "Erreur de calcul"];
                    break;
                case IsochroneContext::STATUS_DONE:
                    $context->status = ["code" => IsochroneContext::STATUS_DONE, "label" => "Terminé"];
                    break;
                default:
                    $context->status = ["code" => $context->status];
                    break;
            }

            $context->links = $links;

            //affichage du territoire
            $test = new InseeRepository();
            $context->limit = $test->getNameFromZoneAndInseeLimit($context->insee_limit, $context->zone_id);
            //Affichage du type de mesure
            $type = IsoType::where('id', $context->iso_type_id)->first();
            $context->iso_type = $type->name;
            //Affichage des services associés à ce contexte
            $services_names = [];
            $services = IsoContextServices::where('iso_context_id', $context->id)->get();
            if ($services) {
                foreach ($services as $service) {
                    $service_name = ServicePei::where('id', $service->service_pei_id)->first();
                    $services_names[] = $service_name->name;
                }
            }
            $context->service = $services_names;

            $iso_contexts_array[] = $context;
        }
        $data = [];
        $data["data"] = $iso_contexts;
        return $data;
    }

    /**
     * Page de création d'un contexte
     *
     * @return void
     */
    public function create()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.create', [$this->current_account->id]), "name" => 'Créer un contexte', "active" => true];

        $this->data['move_types'] = MoveTypeRepository::getAllToSelect();
        $this->data['iso_types'] = IsoTypeRepository::getAllToSelect();

        $this->data['service_peis'] = ServicePeiRepository::getAllFromAccountToSelect($this->current_account->id);

        $this->data['indicators'] = $this->getIndicators();

        return $this->render();
    }

    /**
     * Duplication d'un contexte
     *
     * @param integer $account_id
     * @param integer $id
     * @return void
     */
    public function duplicate($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        /**
         * @var IsochroneContext $context
         */
        $context = IsochroneContext::find((int) $id);

        if (!$context || $context->account_id !== (int) $account_id || $this->current_account->id !== (int) $account_id) {
            abort(404, 'Context not found');
        }

        $newContext = $context->replicate();
        $newContext->status = IsochroneContext::STATUS_NOT_STARTED;
        $newContext->save();

        foreach ($context->getIsoContextServices() as $service) {
            /**
             * @var IsoContextServices $service
             */
            $ctxService = new IsoContextServices();
            $ctxService->iso_context_id = $newContext->id;

            $ctxService->service_pei_id = $service->service_pei_id;
            $ctxService->save();
            foreach ($service->getIsoContextMoveTypes() as $mt) {
                /**
                 * @var IsoContextMoveTypes $mt
                 */
                $newMoveType = new IsoContextMoveTypes;
                $newMoveType->move_type_id = $mt->move_type_id;
                $newMoveType->iso_context_service_id = $ctxService->id;
                $newMoveType->save();
                foreach ($mt->getIsoContextMoveSteps() as $ms) {
                    $newMoveStep = new IsoContextMoveStep;
                    $newMoveStep->iso_context_move_type_id = $newMoveType->id;
                    $newMoveStep->value = $ms->value;
                    $newMoveStep->save();
                }
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $newContext->name . " dupliqué avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Page d'un contexte
     *
     * @return void
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'IsochroneContextController@show');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");

        $parameters = request()->route()->parameters;
        $this->data["context"] = IsochroneContext::where('account_id', $this->current_account->id)->where('id', $parameters['isochrone_context_id'])->first();
        if ($this->data["context"] == null) {
            abort(404, 'Context not found');
        }

        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.show', [$this->current_account->id, $this->data["context"]->id]), "name" => 'Contexte : ' . $this->data["context"]->name, "active" => true];

        $steps = ContextRepository::getAllStepByContextId($this->data["context"]->id, $this->current_account->id);

        $this->data["steps"] = $steps->groupBy('service_name')->map(function ($ar) {
            return collect($ar)->groupBy('move_type_name');
        });

        $this->data["limit_url"] = route('API.geo.geoJson', [$this->data["context"]->zone_id, $this->data["context"]->insee_limit]);

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Enregistrement d'un contexte
     *
     * @param StoreIsochroneContext $request
     * @return void
     */
    public function store(StoreIsochroneContext $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();
        $iso_context = new IsochroneContext;
        $iso_context->name = $validated['name'];
        $iso_context->is_simulation = false;
        $iso_context->active = true;
        if (isset($validated['load_full'])) {
            $iso_context->load_full = true;
        } else {
            $iso_context->load_full = false;
        }
        $iso_context->iso_type_id = $validated['iso_type'];
        $iso_context->insee_limit = explode('.', $validated['limit'])[0];
        $iso_context->zone_id = explode('.', $validated['limit'])[1];
        $iso_context->account_id = $this->current_account->id;
        $iso_context->indicators = json_encode($validated['indicators']);

        $iso_context->referrers = null;

        if (isset($validated['referrers']) && count($validated['referrers']) > 0) {
            $iso_context->referrers = join(',', $validated['referrers']);
        }

        $iso_context->save();

        foreach ($validated['services'] as $key => $service) {
            $iso_context_services = new IsoContextServices;
            $iso_context_services->iso_context_id = $iso_context->id;

            $iso_context_services->service_pei_id = $service;
            $iso_context_services->save();

            foreach ($validated['move_types'] as $key => $move_type) {
                $iso_context_move_type = new IsoContextMoveTypes;
                $iso_context_move_type->move_type_id = $move_type;
                $iso_context_move_type->iso_context_service_id = $iso_context_services->id;
                $iso_context_move_type->save();

                foreach ($validated['steps'] as $key => $step) {
                    if ($step != null) {
                        $iso_context_move_step = new IsoContextMoveStep;
                        $iso_context_move_step->iso_context_move_type_id = $iso_context_move_type->id;
                        $iso_context_move_step->value = $step;
                        $iso_context_move_step->save();
                        ProcessIsoContextMoveStep::dispatch($iso_context_move_step, $this->current_user)->onQueue('default_' . $this->current_account->id);
                    }
                }
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $iso_context->name . " enregistré avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Édition d'un contexte
     *
     * @param InseeRepository $inseeRepository
     * @return void
     */
    public function edit(InseeRepository $inseeRepository)
    {
        //TODO refacto + get step and move type
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $context = IsochroneContext::where('account_id', $this->current_account->id)->where('id', $parameters['isochrone_context_id'])->first();

        if (!$context) {
            abort(404);
        }

        $this->data["context"] = $context;
        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('isochrone_contexts.edit', [$this->current_account->id, $context->id]), "name" => "Edition d'un contexte", "active" => true];

        $this->data['move_types'] = MoveTypeRepository::getAllToSelect();
        $this->data['iso_types'] = IsoTypeRepository::getAllToSelect();
        $this->data['service_peis'] = ServicePeiRepository::getAllFromAccountToSelect($this->current_account->id);

        $iso_type = IsoType::where('id', $context->iso_type_id)
            ->first();
        $selected_iso_type = [];
        $selected_iso_type[] = $iso_type->id;
        $this->data['selected_iso_type'] = $selected_iso_type;

        $used_services = IsoContextServices::where("iso_context_id", $context->id)->select('id', 'service_pei_id')->get();
        $context_service_ids = [];
        $this->data['service_peis_ids'] = [];
        foreach ($used_services as $key => $used_service) {
            $this->data['service_peis_ids'][] = $used_service->service_pei_id;
            $context_service_ids[] = $used_service->id;
        }

        $context_move_types = IsoContextMoveTypes::whereIn('iso_context_service_id', $context_service_ids)->get();
        $selected_move_type_id = [];
        $this->data['selected_move_types'] = [];
        foreach ($context_move_types as $key => $context_move_type) {
            $this->data['selected_move_types'][] = $context_move_type->move_type_id;
            $selected_move_type_id[] = $context_move_type->id;
        }

        $context_step_moves = IsoContextMoveStep::whereIn('iso_context_move_type_id', $selected_move_type_id)->get();
        $this->data['selected_step_moves'] = [];
        foreach ($context_step_moves as $key => $context_step_move) {
            $this->data['selected_step_moves'][] = $context_step_move->value;
        }
        $this->data['selected_step_moves'] = array_unique($this->data['selected_step_moves']);
        $this->data['selected_move_types'] = array_unique($this->data['selected_move_types']);
        $this->data['indicators'] = $this->getIndicators();

        $insee = $inseeRepository->getFromInseeAndZoneId($context->insee_limit, $context->zone_id);
        $this->data['current_limit'] = [
            'value' => $insee['insee'] . '.' . $insee['zone_id'],
            'text' => $insee['name'],
        ];

        return $this->render();
    }

    /**
     * Mise à jour d'un contexte
     *
     * @param UpdateIsochroneContext $request
     * @return void
     */
    public function update(UpdateIsochroneContext $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $validated = $request->validated();
        $context = IsochroneContext::where('account_id', $this->current_account->id)->where('id', $parameters['isochrone_context_id'])->first();

        $context->name = $validated['name'];
        $context->is_simulation = false;
        $context->active = true;
        if (isset($validated['load_full'])) {
            $context->load_full = true;
        } else {
            $context->load_full = false;
        }
        $context->insee_limit = explode('.', $validated['limit'])[0];
        $context->zone_id = explode('.', $validated['limit'])[1];
        $context->indicators = json_encode($validated['indicators']);

        $context->referrers = null;

        if (isset($validated['referrers']) && count($validated['referrers']) > 0) {
            $context->referrers = join(',', $validated['referrers']);
        }

        $context->save();

        $context_service_ids = [];
        $context_move_type_ids = [];
        $context_move_step_ids = [];

        foreach ($validated['services'] as $key => $service) {
            $context_service = IsoContextServices::where('iso_context_id', $context->id)->where('service_pei_id', $service)->first();
            if ($context_service == null) {
                $context_service = new IsoContextServices;
            }

            $context_service->iso_context_id = $context->id;
            $context_service->service_pei_id = $service;
            $context_service->save();
            $context_service_ids[] = $context_service->id;
            foreach ($validated['move_types'] as $key => $move_type) {
                $context_move_type = IsoContextMoveTypes::where('move_type_id', $move_type)->where('iso_context_service_id', $context_service->id)->first();
                if ($context_move_type == null) {
                    $context_move_type = new IsoContextMoveTypes;
                }
                $context_move_type->move_type_id = $move_type;
                $context_move_type->iso_context_service_id = $context_service->id;
                $context_move_type->save();
                $context_move_type_ids[] = $context_move_type->id;
                foreach ($validated['steps'] as $key => $step) {
                    if ($step != null) {
                        $context_move_step = IsoContextMoveStep::where('value', $step)->where('iso_context_move_type_id', $context_move_type->id)->first();
                        if ($context_move_step == null) {
                            $context_move_step = new IsoContextMoveStep;
                            $context_move_step->iso_context_move_type_id = $context_move_type->id;
                            $context_move_step->value = $step;
                            $context_move_step->save();
                            ProcessIsoContextMoveStep::dispatch($context_move_step, $this->current_user)->onQueue('default_' . $this->current_account->id);
                            $context->status = IsochroneContext::STATUS_NOT_STARTED;
                        }
                        $context_move_step_ids[] = $context_move_step->id;

                    }
                }
            }
        }
        $context->save();
        ContextRepository::deleteContextOLDData($context, $context_service_ids, $context_move_type_ids, $context_move_step_ids);

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $context->name . " enregistré avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Relance les calculs d'un contexte
     *
     * @return void
     */
    public function updateCalcul()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        $context = IsochroneContext::where('account_id', $this->current_account->id)->where('id', $parameters['isochrone_context_id'])->first();
        $context->status = IsochroneContext::STATUS_NOT_STARTED;
        $context->save();

        $context_service_ids = IsoContextServices::where('iso_context_id', $parameters['isochrone_context_id'])->get();

        foreach ($context_service_ids as $context_service_id) {
            $context_move_type_ids = IsoContextMoveTypes::where('iso_context_service_id', $context_service_id->id)->get();
            foreach ($context_move_type_ids as $context_move_type_id) {
                $context_move_step_ids = IsoContextMoveStep::where('iso_context_move_type_id', $context_move_type_id->id)->get();
                foreach ($context_move_step_ids as $context_move_step_id) {
                    ProcessIsoContextMoveStep::dispatch($context_move_step_id, $this->current_user)->onQueue('default_' . $this->current_account->id);
                }
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $context->name . " enregistré avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Archive un contexte
     *
     * @return void
     */
    public function archivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $context = IsochroneContext::where('account_id', $this->current_account->id)->where('active', true)->where('id', $parameters['isochrone_context_id'])->first();
        $context->active = false;
        $context->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $context->name . " archivé avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Désarchive un contexte
     *
     * @return void
     */
    public function unarchivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $context = IsochroneContext::where('account_id', $this->current_account->id)->where('active', false)->where('id', $parameters['isochrone_context_id'])->first();
        $context->active = true;
        $context->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $context->name . " déarchivé avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Supprime un contexte
     *
     * @return void
     */
    public function delete()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $context = IsochroneContext::where('account_id', $this->current_account->id)->where('active', false)->where('id', $parameters['isochrone_context_id'])->first();

        ContextRepository::deleteContextData($context);
        $context->delete();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context " . $context->name . " supprimer avec succès.");

        return redirect(action('IsochroneContextController@index', [$this->current_account->id]));
    }

    /**
     * Récupère la liste des indicateurs
     *
     * @return collection
     */
    private function getIndicators()
    {
        $indicators = PostgresRepository::getColNameFromTable('insee_caropop');
        $indicators_format = [];
        foreach ($indicators as $key => $indicator) {
            if ('indindicators.insee.' . $indicator->column_name !== trans('indindicators.insee.' . $indicator->column_name)) {
                $indicator_format = (object) [];
                $indicator_format->value = 'indindicators.insee.' . $indicator->column_name;
                $indicator_format->name = trans('indindicators.insee.' . $indicator->column_name);
                $indicators_format[] = $indicator_format;
            }
        }

        $indicators = PostgresRepository::getColNameFromTable('new_insee_caropop');
        foreach ($indicators as $key => $indicator) {
            if ('indindicators.insee_2015.' . $indicator->column_name !== trans('indindicators.insee_2015.' . $indicator->column_name)) {
                $indicator_format = (object) [];
                $indicator_format->value = 'indindicators.insee_2015.' . $indicator->column_name;
                $indicator_format->name = trans('indindicators.insee_2015.' . $indicator->column_name);
                $indicators_format[] = $indicator_format;
            }
        }

        $indicator_format = (object) [];
        $indicator_format->value = 'indindicators.batiments.nb';
        $indicator_format->name = trans('indindicators.batiments.nb');
        $indicators_format[] = $indicator_format;

        $total = collect($indicators_format);

        return $total;
    }
}
