<?php

namespace App\Http\Controllers;

use App\Helpers\ActivityTreeHelper;
use App\Helpers\GeoHelper;
use App\Http\Requests\StoreServicePEI;
use App\Http\Requests\UpdateServicePei;
use App\Http\Requests\UpdateServicePeis;
use App\Http\Requests\UpdateServicePoint;
use App\Jobs\ProcessCloneServicePei;
use App\Jobs\ProcessConvertCSVtoGeoJson;
use App\Jobs\ProcessIsoContextMoveStep;
use App\Jobs\ProcessServicePEI;
use App\Models\Color;
use App\Models\Ico;
use App\Models\IcoColor;
use App\Models\IsoContextMoveStep;
use App\Models\IsoContextMoveTypes;
use App\Models\IsoContextServices;
use App\Models\Member;
use App\Models\Role;
use App\Models\ServiceMember;
use App\Models\ServicePei;
use App\Repositories\GeojsonRepository;
use App\Repositories\IconRepository;
use App\Repositories\ServicePeiRepository;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ServicePeiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des services
     */
    public function index()
    {
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->data['url_data_table'] = action('ServicePeiController@indexDatatable', [$this->current_account->id]);

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('services_pei.index', [$this->current_account->id]), "name" => 'Liste des services', "active" => true];

        return $this->render();
    }

    /**
     * Renvoie la liste des services
     *
     * @return array
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("can_edit");
        $service_peis = ServicePeiRepository::getServicePeiToTable($this->current_account['id']);

        if (!$can_edit){
            $member = Member::where('user_id', $this->current_user->id)
                ->where('account_id',$this->current_account->id)
                ->first();

            // Je récupere les service dont l'utilisateur à les accès.
            $AccessByUserServices = ServiceMember::where('account_id',$this->current_account['id'])
                ->where('all_members', true)
                ->orWhere('member_id',$member->id)
                ->select('service_id')
                ->get();

            //je crée un tableau avec les identifiants de chaque service
            $servicesacces = [];
            foreach($AccessByUserServices as $service){
                $servicesacces[] = $service->service_id;
            }
        }

        $token = csrf_token();
        $services = [];
        foreach ($service_peis as $key => $service_pei) {
            if ($can_edit === true || in_array($service_pei->id, $servicesacces)) {
                $links = [
                    'token' => $token,
                    'edit' => action('ServicePeiController@edit', [$this->current_account->id, $service_pei->id]),
                    'duplicate' => action('ServicePeiController@duplicate', [$this->current_account->id, $service_pei->id]),
                    'show' => action('ServicePeiController@show', [$this->current_account->id, $service_pei->id]),
                ];
                if ($service_pei->active) {
                    $links["archivate"] = action('ServicePeiController@archivate', [$this->current_account->id, $service_pei->id]);
                } else {
                    $links["unarchivate"] = action('ServicePeiController@unarchivate', [$this->current_account->id, $service_pei->id]);
                    $links["delete"] = action('ServicePeiController@delete', [$this->current_account->id, $service_pei->id]);
                }
            } else {
                $links = [
                    'show' => action('ServicePeiController@show', [$this->current_account->id, $service_pei->id]),
                ];
            }

            $service_pei->links = $links;
            switch ($service_pei->status) {
                case ServicePei::STATUS_NOT_STARTED:
                    $service_pei->status = ["code" => ServicePei::STATUS_NOT_STARTED, "label" => "Non démarré"];
                    break;
                case ServicePei::STATUS_STARTED:
                    $service_pei->status = ["code" => ServicePei::STATUS_STARTED, "label" => "En cours de calcul"];
                    break;
                case ServicePei::STATUS_ERROR:
                    $service_pei->status = ["code" => ServicePei::STATUS_ERROR, "label" => "Erreur de calcul"];
                    break;
                case ServicePei::STATUS_DONE:
                    $service_pei->status = ["code" => ServicePei::STATUS_DONE, "label" => "Terminé"];
                    break;
                default:
                    $service_pei->status = ["code" => $service_pei->status];
                    break;
            }
            $services[$key] = $service_pei;
        }

        $data = [];
        $data["data"] = $services;
        return $data;
    }

    /**
     * Affiche un service
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'ServicePeiController@show');
        $this->init();
        $can_edit = $this->hasPermission("can_edit");
        $parameters = request()->route()->parameters;
        $service_id = $parameters['peis_id'];
        $this->data['url_data_table'] = action('ServicePeiController@showDatatable', [$this->current_account->id, $parameters['peis_id']]);

        $member = Member::where('user_id', $this->current_user->id)
            ->where('account_id',$this->current_account->id)
            ->first();

        // Je regarde si le membre a acces à ce service.
        $this->service_id =$service_id;
        $this->member_id = $member;
        $AccessByUserServices = DB::table('service_members')
            ->where(function($query){
                $query->where('account_id',$this->current_account->id)
                ->where('service_id', $this->service_id)
                ->where('all_members', true);
            })
            ->orWhere(function($query){
                $query->where('account_id',$this->current_account->id)
                    ->where('service_id', $this->service_id)
                    ->where('member_id', $this->member_id->id);
            })
            ->select('service_id')
            ->get();

        // si l utilisateur à des droits sur ces services, il peut les modifier
        $edit_service = false;
        if ($can_edit || count($AccessByUserServices) !== 0){
            $edit_service = true;
        }

        $this->data['edit_service'] = $edit_service;

        //Je recupere le nom, l icone et la couleur de ce service
        $service_pei = ServicePeiRepository::getServicePeiAttibutsByServicePeiId($service_id, $this->current_account['id']);

        //je recupere tous les marqueurs de la table peis
        $name = 'account_' . $parameters['account_id'];
        $table_name = 'peis';
        $markers = DB::select('SELECT id, prop, ST_AsText(geom) as geom FROM ' . $name . '.' . $table_name . ' WHERE service_pei_id =' . $service_id);

        //je controle l'existence d'un score adresse sur les marqueurs.
        $marker_adresse_prop = DB::select("SELECT * FROM " . $name . "." . $table_name . " WHERE service_pei_id =" . $service_id . " AND (upper(prop) LIKE '%SCORE%')");

        //Si les marqueurs ont une adresse dans les proprietes, je cree une datatable des points
        if (!empty($marker_adresse_prop)) {
            $this->data['url_data_table'] = action('ServicePeiController@showDatatable', [$this->current_account->id, $parameters['peis_id']]);
        } else {
            $this->data['url_data_table'] = false;
        }

        $this->data['service_pei'] = collect($service_pei);
        $this->data['json_addresses'] = collect($markers)->map(function ($m) {
            $m->geom = json_decode(GeoHelper::wkt2json($m->geom));
            $m->prop = json_decode($m->prop);
            return $m;
        })->toJson();

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('services_pei.index', [$this->current_account->id]), "name" => 'Liste des services', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('services_pei.show', [$this->current_account->id, $parameters['peis_id']]), "name" => "Affichage d'un service", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Renvoi un tableau de la liste des points d'un service
     *
     * @return array
     */
    public function showDatatable()
    {
        $this->init();
        $parameters = request()->route()->parameters;
        //je recupere tous les marqueurs de la table peis
        $name = 'account_' . $parameters['account_id'];
        $service_id = $parameters['peis_id'];
        $table_name = 'peis';
        $markers = DB::select('SELECT id, prop, ST_AsText(geom) as geom FROM ' . $name . '.' . $table_name . ' WHERE service_pei_id =' . $service_id);
        $token = csrf_token();
        $markers_array = [];
        foreach ($markers as $key => $marker) {
            //gestion des points en fonction des proprietés
            if ($marker->prop != '{}') {
                if (json_decode($marker->prop) != null) {
                    $test = array_change_key_case(get_object_vars(json_decode($marker->prop)),CASE_UPPER);
                    //Cas d'une adresse existante
                    $marker->name = "";
                    if (isset($test['ADRESSE']) && ($test['ADRESSE'] != "")) {
                            $marker->adresse = $test['ADRESSE'] . '.' . $test['CODE POSTAL'] . '.' . $test['COMMUNE'];
                            $marker->adresse_ban = $test['BAN.LABEL'];
                            $marker->score = floor($test['BAN.SCORE'] * 100);
                    } else {
                        /*
                        // Cas d'un point geolocalisé issu d'un fichier .csv
                        if (isset($test['ADRESSE']) && ($test['ADRESSE'] == "")){
                            $marker->adresse = 'Point Geojson issus du fichier CSV';
                            if (isset($test['BAN.LABEL'])) {
                                $marker->adresse_ban = $test['BAN.LABEL'];
                            } else {
                                $marker->adresse_ban = '';
                            }
                            $marker->score = 100;
                        }
                        //si c est un point sirene, alors je mets la precision à 100 et je remplis le champ adresse
                        if (isset($test['ADDRESS'])) {
                            $marker->adresse = $test['ADDRESS'];
                            //si le marqueur sirene a été modifié manuellementje rempli le champ adresse ban
                            $marker->name = isset($test['NAME']) ? $test['NAME'] : "";

                            if (isset($test['BAN.LABEL'])) {
                                $marker->adresse_ban = $test['BAN.LABEL'];
                            } else {
                                $marker->adresse_ban = '';
                            }
                            $marker->score = 100;
                        }
                        */

                        // Si il n y a pas de calculs d'adresse, je n affiche pas le tableau
                        $data["data"] = [];
                        return $data;
                    }
                } else {
                    $marker->adresse = 'Point ajouté manuellement';
                    $marker->adresse_ban = '';
                    $marker->score = 100;
                }
            } else {
                $marker->adresse = 'Point ajouté manuellement';
                $marker->adresse_ban = '';
                $marker->score = 100;
            }

            $links = [
                'token' => $token,
                'edit' => action('ServicePeiController@edit_point', [$this->current_account->id, $service_id, $marker->id]),
            ];
            $marker->links = $links;

            $markers_array[$key] = $marker;
        }
        $data = [];
        $data["data"] = $markers_array;

        return $data;
    }

    /**
     * Création d'un service
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function create(Request $request)
    {
        Debugbar::startMeasure('Controller', 'ServicePeiController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        //Handle post max size exceeded case
        if ($request->query('error')) {
            Session::flash('message', $request->query('error'));
            Session::flash('msg_type', 'danger');
            $q = http_build_query($request->except("error"));

            return redirect()->to(url()->current() . ($q ? '?' . $q : ''));
        }
        $this->init();
        $this->data['can_edit'] = true;
        $this->data["activity_tree"] = config('siren.activity_tree');

        //Je récupère tous les membres de ce compte
        $AllMembers = Member::where('members.account_id',$this->current_account->id)
            ->where('members.active','=',true)
            ->with('role')
            ->with('role.permissions')
            ->with('user')
            ->get();

        // Je ne selectionne pas le membre connecté ni ceux qui ont la permission de creer et modifier les services
        $Availablemembers = [];
        foreach ($AllMembers as $member){
            if (($member->user['lastname'] !== 'Anonyme') && ($member->user['id'] !== $this->current_user->id)) {
                $can_edit_services = false;
                $permissions = $member->role->permissions;
                foreach ($permissions as $permission) {
                    if ($permission->slug = 'can_edit') {
                        $can_edit_services = true;
                    }
                }
                if (!$can_edit_services) {
                    $addmember = (object) [];
                    $addmember->value = $member->id;
                    $addmember->name = $member->user['lastname'] . " " . $member->user['firstname'];
                    $addmember->user_id = $member->user['id'];
                    $Availablemembers[] = $addmember;
                }
            }
        }
        $members = $Availablemembers;
        $this->data["members"] = $members;

        //j appelle ce QB qui va recuperer l id, le nom, le nom de la couleur et l'icone de chaque ensemble
        $icons = IconRepository::getIconAttibutsByAccountId($this->current_account->id);
        $this->data["icons_json"] = json_encode($icons);

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('services_pei.index', [$this->current_account->id]), "name" => 'Liste des services', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('services_pei.create', [$this->current_account->id]), "name" => "Créer un service", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Enregistrement d'un service
     *
     * @param StoreServicePEI $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(StoreServicePEI $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();
        $format_geojson = false;

        //Si l'utilisateur a chargé un fichier, je vérifie le format du fichier geojson ou csv, je redirige l'utilisateur
        //vers le formulaire si une erreur est trouvée. $data contient success_format et le nom du fichier
        if (isset($validated['fileToUpload']) && $validated['fileToUpload']) {
            $data = $this->CheckFileFormat($validated);
            if ($data['success_format'] == false) {
                \Session::flash('msg_type', 'error');
                \Session::flash('message', $data['error_comment']);
                return redirect(action('ServicePeiController@create', [$this->current_account->id]));
            }

            $file = $validated['fileToUpload'];
            $file_name = $file->getClientOriginalName();
            $format_geojson = Str::endsWith($file_name, '.geojson');
            $file_geojson = json_decode(file_get_contents($file), true);
            $geometry = $file_geojson['features'][0]['geometry']['type'];
        }

        //si le fichier n est pas geojson et la geometrie differente de polygon ou line
        if (!($format_geojson && $geometry && ($geometry ==='Polygon' || $geometry === 'Linestring'))) {
            if (isset($validated['color']) && $validated['color']) {
                $color = Color::firstOrCreate(["name" => $validated['color']]);
            }

            if (isset($validated['ico']) && $validated['ico']) {
                $ico = Ico::firstOrCreate(['name' => $validated['ico']]);
            }

            if (isset($ico) && isset($color)) {
                $ico_color = IcoColor::firstOrCreate([
                    'ico_id' => $ico->id,
                    'color_id' => $color->id,
                    'name' => $validated['ico'],
                    'account_id' => $this->current_account->id,
                ]);
            }
        }

        $refresh = isset($validated['day_between_refresh_url']) ? $validated['day_between_refresh_url'] : 5;

        if (isset($data['path'])) {
            $url = "";
        } else {
            $url = $validated['source_url'];
            $format_geojson = Str::endsWith($url, '.geojson');
            if ($format_geojson){
                $data['geojson_type'] = true;
            }
        }

        if (isset($validated['activity']) && isset($validated['limit']) && (isset($validated['buffer']) || $validated['buffer'] === null)) {
            // Create a url to insert into service depending on the activities required by the user
            // This url will be called by ProcessServicePEI and the controller is API\SireneController

            $url = $this->createUrlFromActivities(
                $validated['activity'],
                $validated['limit'],
                $validated['buffer']
            );
            $refresh = $validated['day_between_refresh_siren'];
        }

        // Then insert into $service_pei->source_url
        $service_pei = new ServicePei();
        $service_pei->name = $validated['name'];
        $service_pei->status = ServicePei::STATUS_NOT_STARTED;
        $service_pei->info = $validated['info'];
        $service_pei->source_url = $url;
        $service_pei->day_between_refresh = $refresh;
        $service_pei->ico_color_id = isset($ico_color->id) ? $ico_color->id : null;
        $service_pei->account_id = $this->current_account->id;

        $service_pei->geometry = isset($geometry) ? $geometry : 'Point';
        $service_pei->outline = $validated['colorOutline'];
        $service_pei->fill = $validated['colorFill'];

        // Path cannot be null apparently?
        $service_pei->path = isset($data['path']) ? $data['path'] : '';
        $service_pei->geojson_type = isset($data['geojson_type']) ? $data['geojson_type'] : false;
        $service_pei->active = true;
        $service_pei->updated_by = $this->current_user->id;
        $service_pei->save();

        $this->mapFieldNames($validated, $service_pei);

        if ($service_pei->path and (strstr($service_pei->path, '/csv'))) {
            ProcessConvertCSVtoGeoJson::dispatch($service_pei, $this->current_user)->onQueue('high_' . $this->current_account->id);
        } else {
            ProcessServicePEI::dispatch($service_pei)->onQueue('high_' . $this->current_account->id);
        }

        // Acces des utilisateurs pour ce service
        if ($validated['access'] === 'select_members') {
            $member = Member::where('user_id', $this->current_user->id)
                ->where('account_id',$this->current_account->id)
                ->get();
            $this::addMembersAccess($member, $service_pei);
            if(isset($validated['members_access'])){
                $this::addMembersAccess($validated['members_access'], $service_pei);
            }
        } else {
            if ($validated['access'] === 'none'){
                $member = Member::where('user_id', $this->current_user->id)
                    ->where('account_id',$this->current_account->id)
                    ->get();
                $this::addMembersAccess($member, $service_pei);
            } else {
                $service_member = new ServiceMember();
                $service_member->service_id = $service_pei->id;
                $service_member->member_id = null;
                $service_member->account_id = $this->current_account->id;
                $service_member->all_members = true;
                $service_member->save();
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Service ' . $service_pei->name . ' enregistré avec succès.');

        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }


    /**
     * Edition d'un service
     */
    public function edit()
    {
        Debugbar::startMeasure('Controller', 'ServicePeiController@edit');

        $this->init();
        $parameters = request()->route()->parameters;
        $can_edit = $this->hasPermission("can_edit");
        $this->data["can_edit"] = $can_edit;
        $this->userEdit($parameters['peis_id']);

        $this->data["activity_tree"] = config('siren.activity_tree');
        //Je recupere les attributs de ce service

        $service_pei = ServicePeiRepository::getServicePeiAttibutsByServicePeiId($parameters['peis_id'], $this->current_account['id']);
        $this->data['service_pei'] = $service_pei;

        $firstField = DB::table('account_' . $this->current_account['id'] . '.peis')->where('service_pei_id', $service_pei->id)->first();

        $this->data['available_fields'] = (object)[];

        if ($firstField && isset($firstField->prop)) {
            $this->data['available_fields'] = $firstField->prop;
        }

        //j appelle ce QB qui va recuperer l id, le nom, le nom de la couleur et l'icone de chaque ensemble
        $icons = IconRepository::getIconAttibutsByAccountId($this->current_account->id);
        $this->data["icons_json"] = json_encode($icons);

        //Je récupère le membre associé à l utilisateur connecté
        $userMember = Member::where('user_id',$this->current_user->id)
            ->where('account_id',$this->current_account->id)
            ->first();
        
        // Récuperation de tous les membres de ce service
        $members = ServiceMember::where('service_id',$service_pei->id)
            ->get();

        $this->data["access"] = "";
        $members_service = [];
        foreach ($members as $member){
            if ($member->all_members === true){
                $this->data["access"] = "all";
            }
            else {
                if (count($members) === 1){
                    $this->data["access"] = "none";
                }
                if ( $member->member_id !== $userMember->id){
                    $members_service[] = $member->member_id;
                }
            }
        }

        $this->data["members_service"] = $members_service;

        //Je récupère tous les membres de ce compte
        $AllMembers = Member::where('members.account_id',$this->current_account->id)
            ->where('members.active','=',true)
            ->with('role')
            ->with('role.permissions')
            ->with('user')
            ->get();

        // Je ne selectionne pas le membre connecté ni ceux qui ont la permission de creer et modifier les services
        $Availablemembers = [];
        foreach ($AllMembers as $member){
            if (($member->user['lastname'] !== 'Anonyme') && ($member->user['id'] !== $this->current_user->id)) {
                $can_edit_services = false;
                $permissions = $member->role->permissions;
                foreach ($permissions as $permission) {
                    if ($permission->slug = 'can_edit') {
                        $can_edit_services = true;
                    }
                }
                if (!$can_edit_services) {
                    $addmember = (object) [];
                    $addmember->value = $member->id;
                    $addmember->name = $member->user['lastname'] . " " . $member->user['firstname'];
                    $addmember->user_id = $member->user['id'];
                    $Availablemembers[] = $addmember;
                }
            }
        }
        $members = $Availablemembers;
        $this->data["members"] = $members;

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('services_pei.index', [$this->current_account->id]), "name" => 'Liste des services', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('services_pei.edit', [$this->current_account->id, $parameters['peis_id']]), "name" => "Edition d'un service", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Mise à jour d'un service
     *
     * @param UpdateServicePei $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(UpdateServicePei $request)
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $this->userEdit($parameters['peis_id']);
        $format_geojson = false;

        //Handle post max size exceeded case
        if ($request->query('error')) {
            Session::flash('message', $request->query('error'));
            Session::flash('msg_type', 'danger');
            $q = http_build_query($request->except("error"));
            return redirect()->to(url()->current() . ($q ? '?' . $q : ''));
        }

        $parameters = request()->route()->parameters;
        $validated = $request->validated();

        //Si l'utilisateur a chargé un fichier, je vérifie le format du fichier geojson ou csv, je redirige l'utilisateur
        //vers le formulaire si une erreur est trouvée. $data contient success_format et le nom du fichier
        if (isset($validated['fileToUpload']) && $validated['fileToUpload']) {
            $data = $this->CheckFileFormat($validated);
            if ($data['success_format'] == false) {
                \Session::flash('message', 'error');
                \Session::flash('message', "Le fichier n'est pas compatible ou contient des erreurs");
                return redirect(action('ServicePeiController@edit', [$this->current_account->id, $parameters['peis_id']]));
            }

            $file = $validated['fileToUpload'];
            $file_name = $file->getClientOriginalName();
            if (Str::endsWith($file_name, '.geojson') || Str::endsWith($file_name, '.json')){
                $format_geojson = true;
            }
            $file_geojson = json_decode(file_get_contents($file), true);
            $geometry = $file_geojson['features'][0]['geometry']['type'];
        }

        if (!($format_geojson && $geometry && ($geometry ==='Polygon' || $geometry === 'Linestring'))) {
            if (isset($validated['color']) && $validated['color']) {
                $color = Color::firstOrCreate(["name" => $validated['color']]);
            }

            if (isset($validated['ico']) && $validated['ico']) {
                $ico = Ico::firstOrCreate([
                    'name' => $validated['ico'],
                    'account_id' => $this->current_account->id
                ]);
            }

            if (isset($ico) && isset($color)) {
                $ico_color = IcoColor::firstOrCreate([
                    'ico_id' => $ico->id,
                    'color_id' => $color->id,
                    'name' => $validated['ico'],
                    'account_id' => $this->current_account->id,
                ]);
            }
        }

        $url = $validated['source_url'];
        $refresh = isset($validated['day_between_refresh_url']) ? $validated['day_between_refresh_url'] : 5;

        if ($validated['source_url'] == null) {
            $url = "";
        }
        if (!isset($data['path'])) {
            $data['path'] = "";
        }

        if (isset($validated['activity']) && isset($validated['limit']) && (isset($validated['buffer']) || $validated['buffer'] === null)) {
            // Create a url to insert into service depending on the activities required by the user
            // This url will be called by ProcessServicePEI and the controller is API\SireneController
            $url = $this->createUrlFromActivities(
                $validated['activity'],
                $validated['limit'],
                $validated['buffer']
            );

            $refresh = $validated['day_between_refresh_siren'];
        }

        $service_pei = ServicePei::where('id', $parameters['peis_id'])
            ->first();

        $old = clone $service_pei;
        $service_pei->name = $validated['name'];
        $service_pei->info = $validated['info'];
        $service_pei->updated_by = $this->current_user->id;
        $service_pei->ico_color_id = isset($ico_color->id) ? $ico_color->id : null;

        $service_pei->geometry = isset($geometry) ? $geometry : 'Point';
        $service_pei->outline = $validated['colorOutline'];
        $service_pei->fill = $validated['colorFill'];

        if ($url != "" || $data['path'] != "") {
            $service_pei->source_url = $url;
            $service_pei->path = $data['path'];
        }
        if ($service_pei->source_url != "" || $service_pei->path != "") {
            $service_pei->day_between_refresh = $refresh;
        }
        $service_pei->save();

        if (
            ($url != "" && $url != $old->source_url)
            ||
            ($data['path'] != "" && isset($validated['path']) != $old->path)
        ) {
            if ($service_pei->path and (strstr($service_pei->path, '/csv'))) {
                ProcessConvertCSVtoGeoJson::dispatch($service_pei, $this->current_user)->onQueue('high_' . $this->current_account->id);
            } else {
                ProcessServicePEI::dispatch($service_pei)->onQueue('high_' . $this->current_account->id);
            }
            $service_pei->status = ServicePei::STATUS_NOT_STARTED;
            $service_pei->save();
        }

        $this->mapFieldNames($validated, $service_pei);

        // Acces des utilisateurs pour ce service
        if (isset($validated['access'])){
            //Suppression des membres de ce service avant d'enregistrer les nouveaux
            ServiceMember::where('service_id',$service_pei->id)
                ->delete();

            if ($validated['access'] === 'select_members') {
                $member = Member::where('user_id', $this->current_user->id)
                    ->where('account_id',$this->current_account->id)
                    ->get();
                $this::addMembersAccess($member, $service_pei);
                if(isset($validated['members_access'])){
                    $this::addMembersAccess($validated['members_access'], $service_pei);
                }
            } else {
                if ($validated['access'] === 'none') {
                    $member = Member::where('user_id', $this->current_user->id)
                        ->where('account_id',$this->current_account->id)
                        ->get();
                    $this::addMembersAccess($member, $service_pei);
                }
                else {
                    $service_member = new ServiceMember();
                    $service_member->service_id = $service_pei->id;
                    $service_member->member_id = null;
                    $service_member->account_id = $this->current_account->id;
                    $service_member->all_members = true;
                    $service_member->save();
                }
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Service ' . $validated['name'] . ' enregistré avec succès.');

        Debugbar::stopMeasure('Controller');
        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }

    /**
     * Duplication d'un service
     *
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function duplicate($account_id, $id)
    {
        $this->init();
        $this->userEdit($id);

        $service = ServicePei::find($id);
        $newService = $service->replicate();
        $newService->name = $service->name . " au " . Carbon::now()->format('d/m/Y');
        $newService->day_between_refresh = null;
        $newService->save();

        $serviceMembers = ServiceMember::where('service_id',$id)
            ->where('account_id', $account_id)
            ->get();

        foreach($serviceMembers as $serviceMember){
            $newServiceMembers = $serviceMember->replicate();
            $newServiceMembers->service_id = $newService->id;
            $newServiceMembers->save();
        }

        ProcessCloneServicePei::dispatch($service->id, $newService->id, $account_id)->onQueue('high_' . $this->current_account->id);
        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Service ' . $newService->name . ' copié avec succès.');

        Debugbar::stopMeasure('Controller');
        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }

    /**
     *  Edition des points d'un service sur une carte
     */
    public function edit_peis()
    {
        Debugbar::startMeasure('Controller', 'ServicePeiController@edit');

        $this->init();
        $parameters = request()->route()->parameters;
        $service_id = $parameters['peis_id'];
        $this->userEdit($parameters['peis_id']);

        //Je recupere le nom, l icone et la couleur de ce service
        $service_pei = ServicePeiRepository::getServicePeiAttibutsByServicePeiId($parameters['peis_id'], $this->current_account['id']);

        $icon = substr($service_pei->icon_name, 6);

        //je recupere tous les marqueurs de la table peis
        $name = 'account_' . $parameters['account_id'];
        $table_name = 'peis';
        $markers = DB::select('SELECT id, ST_AsText(geom) as geom,prop FROM ' . $name . '.' . $table_name . ' WHERE service_pei_id =' . $service_id);

        //je transfere dans un tableau l'id et la position de chaque marqueur
        $markers_array = [];
        foreach ($markers as $marker) {
            $geometry = $marker->geom;
            array_push($markers_array, [$marker->id, json_decode(GeoHelper::wkt2json($geometry)), $icon, $service_pei->color_name, $marker->prop]);
        }

        $this->data['service_pei'] = collect($service_pei);
        $this->data['json_addresses'] = json_encode($markers_array);

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('services_pei.index', [$this->current_account->id]), "name" => 'Liste des services', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('services_pei.edit', [$this->current_account->id, $parameters['peis_id']]), "name" => "Edition d'un service", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     *  Mise à jour des points sur la carte pour un service
     */
    public function update_peis(UpdateServicePeis $request)
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $validated = $request->validated();
        $this->userEdit($parameters['peis_id']);

        //je recupere les nouveaux marqueurs avec leurs positions respectives
        $markers = json_decode($validated['markers_update']);
        // j ajoute chaque marqueur dans la table peis avec leurs attributs respectifs
        $date = Carbon::now();
        $name = 'account_' . $parameters['account_id'];
        $table_name = 'peis';
        $service_pei_id = $parameters['peis_id'];

        //nom de la table a modifier et l id à modifier
        $table = $name . '.' . $table_name;
        $id = $table . ".id";
        // 3 situations : marqueurs intacts(on ne fait rien), marqueurs inactifs(on update), nouveaux marqueurs(on ajoute)
        foreach ($markers as $value) {
            //cas d un nouveau marqueur : on les ajoute un par un dans la table peis
            if ((is_null($value[0])) and ($value[3] === true)) {
                $geometry = '{"type": "Point","coordinates": [' . $value[2] . ',' . $value[1] . ']}';
                //dd($geometry);
                $wkt = GeoHelper::json2wkt($geometry);
                $query = 'INSERT INTO ' . $name . '.' . $table_name . " (service_pei_id,prop,created_at,updated_at,geom) VALUES ";
                $query .= "('" . $service_pei_id . "','{}','" . $date . "','" . $date . "',ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326))";
                //dd($query);
                DB::connection()->select($query);
            }

            // Cas d'un marqueur devenu inactif = suppression du marqueur dans la table
            elseif ((isset($value[0])) && (!$value[3])) {
                DB::table('account_' . $this->current_account->id . '.isos')->where('pei_id', $value[0])->delete();
                $query = 'DELETE FROM ' . $table . ' WHERE id =' . $value[0];
                DB::connection()->select($query);
            }

            //Cas d'un marqueur qui a été déplacé
            elseif ((isset($value[0])) and ($value[3] === true) and ($value[4] === true)) {
                //je modifie les proprietes du point : point modifié manuellement.
                $marker_to_update = DB::table('account_' . $this->current_account->id . '.' . $table_name)->where('id', $value[0])->first();
                if (isset($marker_to_update->prop) and ($marker_to_update->prop != '{}')) {
                    $test = get_object_vars(json_decode($marker_to_update->prop));
                    $test['ban.label'] = "Point modifié manuellement";
                    $marker_to_update->prop = json_encode($test, JSON_HEX_APOS);
                    $query = "UPDATE " . $name . "." . $table_name . " SET prop = '" . $marker_to_update->prop . "' WHERE " . $id . "=" . $value[0];
                    DB::connection()->select($query);
                }

                //je modifie les coordonnées du point
                $geometry = '{"type": "Point","coordinates": [' . $value[2] . ',' . $value[1] . ']}';
                $wkt = GeoHelper::json2wkt($geometry);
                DB::table('account_' . $this->current_account->id . '.isos')->where('pei_id', $value[0])->delete();
                $query = 'UPDATE ' . $name . '.' . $table_name . " SET updated_at = '$date', geom = ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326) WHERE " . $id . '=' . "'$value[0]'";
                //dd($query);
                DB::connection()->select($query);
            };
        }

        $context_services = IsoContextServices::where('service_pei_id', $service_pei_id)->get();

        foreach ($context_services as $key => $context_service) {
            $context_move_types = IsoContextMoveTypes::where('iso_context_service_id', $context_service->id)->get();

            foreach ($context_move_types as $key => $context_move_type) {
                $context_move_steps = IsoContextMoveStep::where('iso_context_move_type_id', $context_move_type->id)->get();
                foreach ($context_move_steps as $key => $context_move_step) {
                    ProcessIsoContextMoveStep::dispatch($context_move_step, $this->current_user)->onQueue('default_' . $this->current_account->id);
                }
            }
        }

        Debugbar::stopMeasure('Controller');
        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }

    /**
     *  Edition d'un point d'un service sur une carte
     */
    public function edit_point()
    {
        Debugbar::startMeasure('Controller', 'ServicePeiController@edit_point');

        $this->init();
        $parameters = request()->route()->parameters;
        $this->userEdit($parameters['peis_id']);

        $marker_id = $parameters['pei_id'];

        //je recupere le marqueur choisi
        $name = 'account_' . $parameters['account_id'];
        $table_name = 'peis';
        $marker = DB::select('SELECT id, ST_AsText(geom) as geom, prop FROM ' . $name . '.' . $table_name . ' WHERE id =' . $marker_id);

        $geometry = $marker[0]->geom;
        $marker[0]->coordonnees = json_decode(GeoHelper::wkt2json($geometry))->coordinates;

        $this->data['json_marker'] = json_encode($marker[0]);
        $this->data['pei'] = $marker[0];
        $this->data['marker_id'] = $marker_id;
        $this->data['service_id'] = $parameters['peis_id'];

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('services_pei.index', [$this->current_account->id]), "name" => 'Liste des services', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('services_pei.show', [$this->current_account->id, $parameters['peis_id']]), "name" => "Affichage d'un service", "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('services_pei.edit_point', [$this->current_account->id, $parameters['pei_id'], $marker_id]), "name" => "Edition d'un point", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Mise à jour des points pour un service
     *
     * @param UpdateServicePoint $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update_point(UpdateServicePoint $request)
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $validated = $request->validated();
        $this->userEdit($parameters['peis_id']);

        //je recupere le marqueur avec ses nouvelles positions
        $marker = json_decode($validated['marker_update']);

        $date = Carbon::now();
        $name = 'account_' . $parameters['account_id'];
        $table_name = 'peis';
        //nom de la table a modifier et l id à modifier
        $table = $name . '.' . $table_name;
        $id = $table . ".id";

        // je modifie l adresse ban pour avertir que le point a été modifié manuellement
        $marker_to_update = DB::table('account_' . $this->current_account->id . '.' . $table_name)->where('id', $marker[0])->first();
        if (isset($marker_to_update->prop) and ($marker_to_update->prop != '{}')) {
            //dd($marker_to_update->prop);
            $test = get_object_vars(json_decode($marker_to_update->prop));
            //dd($test);
            $test['ban.label'] = "Point modifié manuellement";
            $marker_to_update->prop = json_encode($test, JSON_HEX_APOS);
            //dd($marker_to_update->prop);

            $query = "UPDATE " . $name . "." . $table_name . " SET prop = '" . $marker_to_update->prop . "' WHERE " . $id . "=" . $marker[0];
            DB::connection()->select($query);
        }

        $geometry = '{"type": "Point","coordinates": [' . $marker[2] . ',' . $marker[1] . ']}';
        //dd($geometry);
        $wkt = GeoHelper::json2wkt($geometry);
        DB::table('account_' . $this->current_account->id . '.isos')->where('pei_id', $marker[0]);
        $query = 'UPDATE ' . $name . '.' . $table_name . " SET updated_at = '$date', geom = ST_SetSRID(ST_GeomFromText('" . $wkt . "'),4326) WHERE " . $id . '=' . "'$marker[0]'";
        //dd($query);
        DB::connection()->select($query);

        return redirect(action('ServicePeiController@show', [$this->current_account->id, $parameters['peis_id']]));
    }

    /***********************************************************************************************************************
     *****************************************   Private methods                *********************************************
     ***********************************************************************************************************************/

    /**
     * Cette methode va controler le format du fichier (csv ou geojson)
     * Pour un format geojson, il faire un controle aleatoire du contenu pour verifier la cohérence des données
     * Pour un fichier CSV, il va transformer le fichier en fichier geojson avec ajout des coordonnees
     *
     * @param $validated
     * @throws \Exception
     * @return array
     */
    private function CheckFileFormat($validated)
    {
        //tableaux qui contiendra la valeur successformat et le nom du fichier
        $data = [];
        //$success format definit si le fichier est bon ou pas, je le definit a false par defaut
        $success_format = false;
        $data['success_format'] = $success_format;
        //je recupere le fichier afin de le controler
        $file = $validated['fileToUpload'];
        //je recupere le nom du fichier
        $file_name = $file->getClientOriginalName();
        //je verifie l extension du fichier
        $format_geojson = Str::endsWith($file_name, '.geojson');
        $format_json = Str::endsWith($file_name, '.json');
        $format_csv = Str::endsWith($file_name, '.csv');
        $format_kml = Str::endsWith($file_name, '.kml');
        $format_gpx = Str::endsWith($file_name, '.gpx');
        $path = null;
        $data['geojson_type'] = false;

        //si le format est geojson ou json
        if ($format_geojson || $format_json) {
            //je recupere le contenu du fichier
            $file_geojson = json_decode(file_get_contents($file), true);
            //je compte le nombre d elements du fichier
            $nb_elements = count($file_geojson['features']);
            $geometry = $file_geojson['features'][0]['geometry']['type'];

            //Si la geometrie du geojson est line, polygon, multiline ou multipolygon je valide le format car il a été controlé coté front
            if ($geometry === 'LineString' || $geometry === 'Polygon' || $geometry === 'MultiLineString' || $geometry === 'MultiPolygon'){
                $success_format = true;
            }

            if ($geometry === 'Point' || $geometry === 'MultiPoint'){
                //je fais une boucle pour faire 10 tests d'elements dans le fichier de maniere aléatoire
                for ($i = 1; $i <= 10; $i++) {
                    $random_element = random_int(0, $nb_elements - 1);
                    //Je controle que les coordonnées existent et sont cohérentes
                    if (isset($file_geojson['features'][$random_element]['geometry']['coordinates'][0])
                        and isset($file_geojson['features'][$random_element]['geometry']['coordinates'][1])) {
                        $coordinates = GeojsonRepository::getCoordinatesOfElementByIndex($file_geojson, $random_element);
                        if (abs($coordinates[0]) < 180 and abs($coordinates[1]) < 90) {
                            $success_format = true;
                        } else {
                            $success_format = false;
                            $data['success_format'] = $success_format;
                            $data['error_comment'] = 'Le fichier ne peut être chargé car il contient des erreurs. Les coordonnées ne respectent pas le format WGS84';
                            break;
                        }
                    } else {
                        $success_format = false;
                        $data['success_format'] = $success_format;
                        $data['error_comment'] = "Le fichier ne peut être chargé car il contient des erreurs. Des coordonnées sont manquantes sur un ou plusieurs points";
                        break;
                    }
                }
            }

            //Si le fichier est conforme, je l enregistre
            if ($success_format === true) {
                $path = $validated['fileToUpload']->store('accounts/' . $this->current_account->id . '/servicePei');
            }

            //je retourne la valeur success format et le nom du fichier dans un tableau
            $data['geojson_type'] = true;
            $data['success_format'] = $success_format;
            $data['path'] = $path;
            return $data;
        }

        //si le contenu est du type .csv
        if ($format_csv) {
            $file = $validated['fileToUpload']->store('accounts/' . $this->current_account->id . '/csv');

            $path = $file;

            $content = Storage::get($path);
            $array = array_map("str_getcsv", explode("\n", $content));
            $json = json_decode(json_encode($array));
            $header = explode(';', strtoupper($json[0][0]));

            if (in_array("ADRESSE", $header) && in_array("CODE POSTAL", $header) && in_array("COMMUNE", $header)) {
                $success_format = true;
            } else {
                $success_format = false;
                $data['error_comment'] = "Le fichier .csv contient des erreurs. Les champs Adresse, Code postal et Commune sont obligatoires.";
            }

            $data['success_format'] = $success_format;
            $data['path'] = $path;
            return $data;
        }

        //si le contenu est du type .kml
        if ($format_kml) {
            $file = $validated['fileToUpload']->store('accounts/' . $this->current_account->id . '/kml');

            $path = $file;

            $content = Storage::get($path);
            $points_array = explode('<Placemark>', $content);
            unset($points_array[0]);

            if (!empty($points_array)) {
                //je recherhe tous les points avec ses propriétes(nom, description,coordonnées)
                foreach ($points_array as $point_array) {
                    $name = substr((explode('name>', $point_array))[1], 0, -2);
                    $prop = substr((explode('description>', $point_array))[1], 0, -2);
                    $coordinates = substr((explode('coordinates>', $point_array))[1], 0, -2);
                    $coordinates = preg_split('/,/', $coordinates);

                    //Vérification des valeurs pour que les coordonnées existent et respectent les limites
                    if (!empty($coordinates) && (abs($coordinates[0]) < 180) && (abs($coordinates[1])) < 90) {
                        //j enregistre l adresse à la suite des precedentes adresses dans un tableau avec le format geojson
                        $addresses[] = ['type' => 'Feature', 'properties' => ['name' => $name, 'properties' => $prop], 'geometry' => ['type' => 'Point', 'coordinates' => [$coordinates[0], $coordinates[1]]]];
                    } else {
                        $data['success_format'] = false;
                        $data['error_comment'] = "Le fichier ne peut être chargé car il contient des erreurs. Des coordonnées de points sont manquantes ou ne respectent pas le format WGS84";
                        return $data;
                    }
                }
                $addresses_collection = ['type' => 'FeatureCollection', 'features' => $addresses];
                $addresses_collection_json = json_encode($addresses_collection);

                //j enregistre le fichier convertit au format geojson
                Storage::disk('local')->put($path, $addresses_collection_json);
                $success_format = true;
                $data['success_format'] = $success_format;
                $data['path'] = $path;
                return $data;
            } else {
                $data['success_format'] = false;
                $data['error_comment'] = "Le fichier ne peut être chargé car il contient des erreurs. Il n'y a pas de points dans ce fichier";
                return $data;
            }
        }

        //si le contenu est du type .gpx
        if ($format_gpx) {
            $file = $validated['fileToUpload']->store('accounts/' . $this->current_account->id . '/gpx');
            $path = $file;

            $content = Storage::get($path);
            $points_array = explode('<wpt ', $content);
            unset($points_array[0]);

            if (!empty($points_array)) {
                //je recherhe tous les points avec ses propriétes(nom, description,coordonnées)
                foreach ($points_array as $point_array) {
                    $name = substr((explode('name>', $point_array))[1], 0, -2);
                    $type = substr((explode('type>', $point_array))[1], 0, -2);
                    $coordinates = [];
                    $coordinates[0] = explode('"', $point_array)[1];
                    $coordinates[1] = explode('"', $point_array)[3];
                    //Vérification des valeurs pour que les coordonnées existent et respectent les limites
                    if (!empty($coordinates) && (abs($coordinates[1]) < 180) && (abs($coordinates[0])) < 90) {
                        //j enregistre l adresse à la suite des precedentes adresses dans un tableau avec le format geojson
                        $addresses[] = ['type' => 'Feature', 'properties' => ['name' => $name, 'type' => $type], 'geometry' => ['type' => 'Point', 'coordinates' => [$coordinates[1], $coordinates[0]]]];
                    } else {
                        $data['success_format'] = false;
                        $data['error_comment'] = "Le fichier ne peut être chargé car il contient des erreurs. Des coordonnées de points sont manquantes ou ne respectent pas le format WGS84";
                        return $data;
                    }
                }
                $addresses_collection = ['type' => 'FeatureCollection', 'features' => $addresses];
                $addresses_collection_json = json_encode($addresses_collection);

                //j enregistre le fichier convertit au format geojson
                Storage::disk('local')->put($path, $addresses_collection_json);
                $success_format = true;
                $data['success_format'] = $success_format;
                $data['path'] = $path;
                return $data;
            } else {
                $data['success_format'] = false;
                $data['error_comment'] = "Le fichier ne peut être chargé car il contient des erreurs. Il n'y a pas de points dans ce fichier";
                return $data;
            }
        }

        // si le format du fichier ne correspond pas aux choix précedent, je renvoi un message d'erreur
        $data['success_format'] = false;
        $data['error_comment'] = 'Le format du fichier est incorrect.';
        return $data;
    }

    /**
     * Archivage d'un service
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archivate()
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $this->userEdit($parameters['peis_id']);

        $service_pei = ServicePei::where('account_id', $this->current_account->id)->where('active', true)->where('id', $parameters['peis_id'])->first();

        $service_pei->active = false;
        $service_pei->updated_by = $this->current_user->id;
        $service_pei->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Service " . $service_pei->name . " archivé avec succès.");

        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }

    /**
     * Désarchivage d'un service
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function unarchivate()
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $this->userEdit($parameters['peis_id']);

        $service_pei = ServicePei::where('account_id', $this->current_account->id)->where('active', false)->where('id', $parameters['peis_id'])->first();

        $service_pei->active = true;
        $service_pei->updated_by = $this->current_user->id;;
        $service_pei->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Service " . $service_pei->name . " déarchivé avec succès.");

        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }

    /**
     * Suppression d'un service
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete()
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $this->userEdit($parameters['peis_id']);
        $service_pei = ServicePei::where('account_id', $this->current_account->id)->where('active', false)->where('id', $parameters['peis_id'])->first();

        $iso_context_services = IsoContextServices::where('service_pei_id', $service_pei->id)->get();

        $serviceMembers = ServiceMember::where('service_id',$service_pei->id)
            ->delete();

        foreach ($iso_context_services as $key => $iso_context_service) {
            $iso_context_move_types = IsoContextMoveTypes::where('iso_context_service_id', $iso_context_service->id)->get();
            foreach ($iso_context_move_types as $key => $iso_context_move_type) {
                $iso_context_move_steps = IsoContextMoveStep::where('iso_context_move_type_id', $iso_context_move_type->id)->get();
                foreach ($iso_context_move_steps as $key => $iso_context_move_step) {
                    DB::table('account_' . $this->current_account->id . '.isos')->where('context_step_move_id', $iso_context_move_step->id)->delete();
                    DB::table('account_' . $this->current_account->id . '.layers')->where('context_step_move_id', $iso_context_move_step->id)->update(['context_step_move_id' => null]);
                    $iso_context_move_step->delete();
                }
                $iso_context_move_type->delete();
            }
            $iso_context_service->delete();
        }

        DB::table('account_' . $this->current_account->id . '.peis')->where('service_pei_id', $service_pei->id)->delete();
        DB::table('research_context_services')->where('service_pei_id', $service_pei->id)->delete();

        $service_pei->delete();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Service " . $service_pei->name . " supprimer avec succès.");

        return redirect(action('ServicePeiController@index', [$this->current_account->id]));
    }

    /**
     * Creation d'une url depuis l'activité
     *
     * @param $requestActivities
     * @param $limit
     * @param $buffer
     * @return string
     */
    private function createUrlFromActivities($requestActivities, $limit, $buffer)
    {
        if ($buffer === null) {
            $buffer = 0;
        }
        $activityTree = config('siren.activity_tree');

        //convert validity post data to activity tree format

        $activities = ActivityTreeHelper::mapFromRequest($requestActivities);
        $mainCommonNodes = ActivityTreeHelper::getMainCommonNode($activityTree, $activities);

        $params = [
            'limit' => $limit,
            'buffer' => $buffer,
        ];

        foreach ($mainCommonNodes as $k => $node) {
            $params['nodes[' . $k . ']'] = $node;
        }

        return route('API.geo.convert', $params);
    }

    /**
     * @param array $validated
     * @param ServicePei $servicePei
     *
     * @return ServicePei
     */
    private function mapFieldNames($validated, ServicePei $servicePei)
    {
        if (isset($validated['field-names'])&&isset($validated['field-values'])){
            $names = $validated['field-names'];
            $values = $validated['field-values'];

            $final_selection = [];
            $final_infobulle = [];
            $final_recherche = [];
            //dd($names, $values, $validated['selection-values']);
            // Cette fonction permet de trier entre les champs infobulle et selection
            if (isset($names) && isset($values)) {
                foreach ($names as $key => $name) {
                    if (isset($name)) {
                        if (isset($validated['selection-values']) && isset($validated['selection-values'][$key]) && isset($values[$key])) {
                            $final_selection[$name] = $values[$key];
                        }
                        if (isset($validated['infobulle-values']) && isset($validated['infobulle-values'][$key]) && isset($values[$key])) {
                            $final_infobulle[$name] = $values[$key];
                        }
                        if (isset($validated['recherche-values']) && isset($validated['recherche-values'][$key]) && isset($values[$key])) {
                            $final_recherche[$name] = $values[$key];
                        }
                    };
                };

                if ($final_selection === []) {
                    $servicePei->display_selection = null;
                }
                else {
                    $servicePei->display_selection = json_encode($final_selection);
                };

                if ($final_infobulle === []) {
                    $servicePei->display_fields = null;
                }
                else {
                    $servicePei->display_fields = json_encode($final_infobulle);
                };

                if ($final_recherche === []) {
                    $servicePei->display_recherche = null;
                }
                else {
                    $servicePei->display_recherche = json_encode($final_recherche);
                };
                $servicePei->save();
            }
        }

        return $servicePei;
    }

    private function addMembersAccess($members,$service_pei){
        foreach ($members as $member){
            $service_member = new ServiceMember();
            $service_member->service_id = $service_pei->id;
            if(isset($member->id)){
                $service_member->member_id = $member->id;
            }
            else {
                $service_member->member_id = $member;
            }
            $service_member->account_id = $this->current_account->id;;
            $service_member->save();
        }
    }

    private function userEdit($service_id){
        $can_edit = $this->hasPermission("can_edit");
        $member = Member::where('user_id', $this->current_user->id)
            ->where('account_id',$this->current_account->id)
            ->first();
        $this->member_id = $member;
        $this->service_id = $service_id;

        // Je regarde si le membre a acces à ce service.
        $AccessByUserServices = DB::table('service_members')
            ->where(function($query){
                $query->where('account_id',$this->current_account->id)
                    ->where('service_id', $this->service_id)
                    ->where('all_members', true);
            })
            ->orWhere(function($query){
                $query->where('account_id',$this->current_account->id)
                    ->where('service_id', $this->service_id)
                    ->where('member_id', $this->member_id->id);
            })
            ->select('service_id')
            ->get();

        if (!($can_edit || count($AccessByUserServices) !== 0)){
            abort(403, 'Unauthorized action.');
        }
    }
}
