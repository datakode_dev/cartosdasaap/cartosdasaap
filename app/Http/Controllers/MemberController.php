<?php

namespace App\Http\Controllers;

use App\Helpers\HtmlHelper;
use App\Http\Requests\StoreMember;
use App\Http\Requests\UpdateMember;
use App\Interfaces\Controllers\MemberInterface;
use App\Mail\CreateMember;
use App\Mail\JoinMember;
use App\Models\Member;
use App\Models\ServiceMember;
use App\Models\User;
use App\Repositories\MemberRepository;
use App\Repositories\RoleRepository;
use Debugbar;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Request;

class MemberController extends Controller implements MemberInterface
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des membres
     *
     * @return void
     */
    public function index()
    {
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("members_all");
        $this->breadcrumb[] = (object) ["link" => route('members.index', [$this->current_account->id]), "name" => 'Liste des utilisateurs', "active" => true];
        $this->data['url_data_table'] = action('MemberController@indexDatatable', [$this->current_account->id]);
        return $this->render();
    }

    /**
     * Renvoie les membres de la liste
     *
     * @return void
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("members_all");
        $can_not_delete = RoleRepository::getRolesIdByAccountCannotDisabled($this->current_account->id);
        $members = MemberRepository::getMembersAndRoleByAccountId($this->current_account->id);
        $token = csrf_token();

        foreach ($members as $key => $member) {
            if ($can_edit === true) {
                $member->links = [
                    'token' => $token,
                    'edit' => action('MemberController@edit', [$this->current_account->id, $member->id]),
                    'show' => action('MemberController@show', [$this->current_account->id, $member->id]),
                ];
                // si ce membre à la permission "not disabled", il ne pourra pas etre archiver ou desarchiver, ni supprimé.
                if (!in_array($member->role_id, $can_not_delete)) {
                    if ($member->active) {
                        $member->links["archivate"] = action('MemberController@archivate', [$this->current_account->id, $member->id]);
                    } else {
                        $member->links["unarchivate"] = action('MemberController@unarchivate', [$this->current_account->id, $member->id]);
                    }
                    $member->links["delete"] = action('MemberController@delete', [$this->current_account->id, $member->id]);
                }
            } else {
                $member->links = [
                    'show' => action('MemberController@show', [$this->current_account->id, $member->id]),
                ];
            }

            $members[$key] = $member;
        }

        $data = [];
        $data["data"] = $members;
        return $data;
    }

    /**
     * Création d'un membre
     *
     * @return void
     */
    public function create()
    {
        Debugbar::startMeasure('Controller', 'MemberController@create');

        $this->init();
        $this->hasPermissionOrAbord("members_all");

        $roles_models = $this->current_account->roles;
        $roles = [];
        foreach ($roles_models as $key => $role_model) {
            $role = (object) [];

            $role->value = $role_model->id;
            $role->name = $role_model->name;
            $roles[] = $role;
        }

        $this->data['roles'] = collect($roles);
        $this->breadcrumb[] = (object) ["link" => route('members.index', [$this->current_account->id]), "name" => 'Liste des utilisateurs', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('members.create', [$this->current_account->id]), "name" => "Ajouter un utilisateur", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Enregistrement d'un membre
     *
     * @param StoreMember $request
     * @return void
     */
    public function store(StoreMember $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("members_all");

        $validated = $request->validated();

        if ($validated['email'] != null) {
            $user = User::where("email", $validated['email'])->first();

            if (!$user) {
                $user = new User;

                $user->lastname = $validated['lastname'];
                $user->firstname = $validated['firstname'];
                $user->email = $validated['email'];
                $user->password = Str::random(255);

                $user->save();
                Mail::to($user->email)->send(new CreateMember());
            } else {
                Mail::to($user->email)->send(new JoinMember());
            }
        } else {
            $user = $this->current_user;
        }

        $member = new Member;
        $member->user_id = $user->id;
        $member->account_id = $this->current_account->id;
        $member->role_id = $validated['role_id'];
        $member->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Membre " . $this->current_account->name . " enregistré avec succès.");

        return redirect(action('MemberController@index', [$this->current_account->id]));
    }

    /**
     * Page d'édition d'un membre
     *
     * @return void
     */
    public function edit()
    {
        $this->init();
        $this->hasPermissionOrAbord("members_all");
        $parameters = request()->route()->parameters;

        //on recupere le membre de cet account definit par l'utilisateur
        $member = Member::where('account_id', $parameters['account_id'])
            ->where('id', $parameters['member_id'])
            ->first();
        //dd($member);

        // A partir de ce membre, je vais chercher ses carateristiques utilisateurs (nom, prenom,mail,...)
        $user = User::where('id', $member->user_id)
            ->first();

        //je stocke le role et les permissions de ce role dans $this->data
        $this->data["user"] = $user;
        $this->data["member"] = $member;

        //je liste tous les roles que peut avoir ce membre
        $roles_models = $this->current_account->roles;
        $roles = [];
        foreach ($roles_models as $key => $role_model) {
            $role = (object) [];

            $role->value = $role_model->id;
            $role->name = $role_model->name;
            $roles[] = $role;
        }
        $this->data['roles'] = collect($roles);
        $this->breadcrumb[] = (object) ["link" => route('members.index', [$this->current_account->id]), "name" => 'Liste des utilisateurs', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('members.edit', [$this->current_account->id, $member->id]), "name" => "Edition d'un utilisateur", "active" => true];

        return $this->render();
    }

    /**
     * Mise à jour d'un membre
     *
     * @param UpdateMember $request
     * @return void
     */
    public function update(UpdateMember $request)
    {
        Debugbar::startMeasure('Controller', 'MemberController@update');

        $this->init();
        $this->hasPermissionOrAbord("members_all");
        $parameters = request()->route()->parameters;

        // je recupere les donnees du formulaire remplies par l utilisateur
        $validated = $request->validated();
        //dd($validated);
        $firstName = $validated["firstName"];
        $lastName = $validated["lastName"];
        $user_email = $validated["user_email"];
        $role_id = $validated["role_id"];

        //je mets le role du membre à jour
        Member::where('id', $parameters['member_id'])
            ->update(['role_id' => $role_id]);

        //je recupere les caracteristiques du membre
        $member = Member::where('id', $parameters['member_id'])
            ->first();

        //je recupere son user_id
        $user_id = $member->user_id;
        $user = $member->user;

        $user->firstname = $firstName;
        $user->lastname = $lastName;
        $user->email = $user_email;

        $user->save();
        Debugbar::stopMeasure('Controller');

        return redirect(action('MemberController@index', [$this->current_account->id]));
    }

    /**
     * Page d'un membre
     *
     * @return void
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'MemberController@show');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("members_all");
        $parameters = request()->route()->parameters;

        $member = MemberRepository::getMemberAndRoleById($parameters['member_id']);

        // je recupere l image en fonction de son adresse mail
        $member_image = HtmlHelper::getGravatarUrl($member->email);

        $this->data["member_image"] = $member_image;
        $this->data["member"] = $member;

        $this->breadcrumb[] = (object) ["link" => route('members.index', [$this->current_account->id]), "name" => 'Liste des utilisateurs', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('members.show', [$this->current_account->id, $member->id]), "name" => "Affichage d'un utilisateur", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Archive un membre
     *
     * @param [type] $request
     * @return void
     */
    public function archivate($request)
    {
        $this->init();
        $this->hasPermissionOrAbord("members_all");
        $parameters = request()->route()->parameters;

        //je recupere le membre actif dans la table members
        $member = Member::where('account_id', $this->current_account->id)
            ->where('active', true)
            ->where('id', $parameters['member_id'])
            ->first();
        $member->active = false;
        $member->save();

        //je recupere le nom de l'utilisateur dans la table users pour l afficher dans le message flash
        $user = User::where('id', $member->user_id)
            ->first();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "L'utilisateur " . $user->firstname . " " . $user->lastname . " est inactif.");

        return redirect(action('MemberController@index', [$this->current_account->id]));
    }

    /**
     * Désarchive un membre
     *
     * @param [type] $request
     * @return void
     */
    public function unarchivate($request)
    {
        $this->init();
        $this->hasPermissionOrAbord("members_all");
        $parameters = request()->route()->parameters;

        $member = Member::where('account_id', $this->current_account->id)
            ->where('active', false)
            ->where('id', $parameters['member_id'])
            ->first();

        $member->active = true;
        $member->save();

        //je recupere le nom de l'utilisateur dans la table users pour l afficher dans le message flash
        $user = User::where('id', $member->user_id)
            ->first();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "L'utilisateur " . $user->firstname . " " . $user->lastname . " est actif.");

        return redirect(action('MemberController@index', [$this->current_account->id]));
    }

    public function delete()
    {
        $this->init();
        $this->hasPermissionOrAbord("members_all");
        $parameters = request()->route()->parameters;

        $member = Member::where('account_id', $this->current_account->id)
            ->where('id', $parameters['member_id'])
            ->first();
        $member->active = false;
        $member->save();
        $userId = $member->user_id;

        //Je supprime tous les services auquel ce membre est associé
        ServiceMember::where('member_id', $member->id)
            ->delete();

        //je recupere le nom de l'utilisateur dans la table users pour l afficher dans le message flash
        $user = User::where('id', $userId)
            ->first();
        $user->firstname = "";
        $user->lastname = "Anonyme";
        $user->email = substr(sha1(time()), 0, 12);
        $user->password = substr(sha1(time()), 0, 12);
        $user->is_superadmin = false;
        $user->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "L'utilisateur a été supprimé.");

        return redirect(action('MemberController@index', [$this->current_account->id]));
    }
}
