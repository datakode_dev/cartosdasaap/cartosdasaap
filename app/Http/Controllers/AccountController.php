<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAccount;
use App\Http\Requests\UpdateAccount;
use App\Models\Account;
use App\Models\Member;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\ServicePei;
use App\Models\User;
use App\Models\Ico;
use App\Models\Color;
use App\Models\IcoColor;
use App\Repositories\PostgresRepository;
use Debugbar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Page d'accueil d'un compte
     *
     * @param Request $request
     * @return view
     */
    public function home()
    {
        Debugbar::startMeasure('Controller', 'AccountController@home');
        //Todo construct
        $this->middleware('account_member');
        $this->init();
        $current_account = $this->current_account->id;
        $account = 'account_' . $current_account;

        //Mesure de l'espace utilisé pour ce compte sur le serveur
        $query = "SELECT schema_name, pg_size_pretty(sum(table_size)::bigint) as disk_space
            FROM ( SELECT pg_catalog.pg_namespace.nspname as schema_name, pg_relation_size(pg_catalog.pg_class.oid) as table_size
                FROM pg_catalog.pg_class JOIN pg_catalog.pg_namespace ON relnamespace = pg_catalog.pg_namespace.oid) t
            WHERE schema_name LIKE '" . $account . "'
            GROUP BY schema_name
            ORDER BY schema_name";

        //Calcul du pourcentage de la capacité utilisée de la bdd par rapport à la capacité maximale définie ici 5000Kb.
        $db_size = DB::connection()->select($query);
        $db_size_value = explode(' ', $db_size[0]->disk_space);
        $total_disk_space = "5000";
        $capacity_use_percent = ($db_size_value[0] * 100) / $total_disk_space;

        $this->data["capacity_use"] = $capacity_use_percent;
        $this->data["db_size"] = $db_size_value[0];
        $this->data["total_space"] = $total_disk_space;

        //Récuperation du total du nombre de services et des services terminés
        $query = "SELECT COUNT(*) FROM public.service_peis WHERE account_id = " . $current_account;
        $all_services = DB::connection()->select($query);

        $query = "SELECT COUNT(*) FROM public.service_peis WHERE account_id = " . $current_account . "and status = 3";
        $services_ok = DB::connection()->select($query);

        $services_in_progress = $all_services[0]->count - $services_ok[0]->count;
        $this->data["services_ok"] = $services_ok[0]->count;
        $this->data["services_in_progress"] = $services_in_progress;

        //Récuperation de tous les contextes et de tous les contextes ok
        $query = "SELECT COUNT(*) FROM public.iso_contexts WHERE account_id = " . $current_account;
        $all_contextes = DB::connection()->select($query);

        $query = "SELECT COUNT(*) FROM public.iso_contexts WHERE account_id = " . $current_account . "and status = 3";
        $contextes_ok = DB::connection()->select($query);

        $contextes_in_progress = $all_contextes[0]->count - $contextes_ok[0]->count;
        $this->data["contextes_ok"] = $contextes_ok[0]->count;
        $this->data["contextes_in_progress"] = $contextes_in_progress;

        //Récuperation de tous les tableaux de bord et des tableaux de bords terminés
        $query = "SELECT COUNT(*) FROM public.boards WHERE account_id = " . $current_account;
        $all_boards = DB::connection()->select($query);

        $query = "SELECT COUNT(*) FROM public.boards WHERE account_id = " . $current_account . "and status = 3";
        $boards_ok = DB::connection()->select($query);
        
        $boards_in_progress =  $all_boards[0]->count - $boards_ok[0]->count;
        $this->data["boards_ok"] = $boards_ok[0]->count;
        $this->data["boards_in_progress"] = $boards_in_progress;

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Liste des comptes
     *
     * @return view
     */
    public function index()
    {
        $this->middleware('super_admin');

        $this->init();

        $this->data['url_data_table'] = action('AccountController@indexDatatable');
        return $this->render();
    }

    /**
     * Liste des comptes pour datatable
     *
     * @return json Liste des comptes pour datatables
     */
    public function indexDatatable()
    {
        $this->middleware('super_admin');
        $this->init();
        $accounts = Account::All();

        $accounts_array = [];
        //je vais parcourir toutes les icones et remplir les champs icone et links
        foreach ($accounts as $key => $account) {
            $account->links = ['edit' => action('AccountController@edit', [$account->id])];
            $accounts_array[$key] = $account;
        }
        $data = [];
        $data["data"] = $accounts_array;
        return $data;
    }
    /**
     * Page de création des comptes
     *
     * @return view
     */
    public function create()
    {
        $this->middleware('super_admin');
        $this->init();

        $this->data["current_user"] = $this->current_user;
        return $this->render();
    }

    /**
     * Page d édition des comptes
     *
     * @return view
     */
    public function edit()
    {
        $this->middleware('super_admin');
        $this->init();
        $parameters = request()->route()->parameters;
        $account_id = $parameters['account_id'];
        $account = Account::find($account_id);

        $this->data["account"] = $account;
        $this->data["current_user"] = $this->current_user;
        $file = null;
        if (Storage::url($account->logo_path) !== '/storage/'){
            $file = Storage::url($account->logo_path);
        }
        $this->data["file"] = $file;
        return $this->render();
    }

    public function update(UpdateAccount $request)
    {
        $this->middleware('super_admin');
        $this->init();

        $parameters = request()->route()->parameters;
        $account_id = $parameters['account_id'];
        $account = Account::find($account_id);

        $validated = $request->validated();

        $account->name = $validated['name'];
        if ($request->file('change_logo') !== null){
            $path = $request->file('change_logo')->store('logos', 'public');
            $account->logo_path = $path;
        }
        $account->save();

        $this->data["current_user"] = $this->current_user;
        return redirect(action('AccountController@index'));
    }

    /**
     * Ajoute le compte
     *
     * @param StoreAccount $request
     * @return redirect
     */
    public function store(StoreAccount $request)
    {
        $this->middleware('super_admin');
        $this->init();

        $validated = $request->validated();
        $permissions = Permission::All();

        $account = new Account;

        $account->name = $validated['name'];

        if ($request->file('logo') !== null){
            $path = $request->file('logo')->store('logos', 'public');
            $account->logo_path = $path;
        }

        $account->save();

        if ($validated['admin_email'] != null) {
            $user = User::where("email", $validated['admin_email'])->first();

            if (!$user) {
                $user = new User;

                $user->lastname = $validated['admin_lastname'];
                $user->firstname = $validated['admin_firstname'];
                $user->email = $validated['admin_email'];
                $user->password = Str::random(255);

                $user->save();
                //Send mail
            }
        } else {
            $user = $this->current_user;
        }

        $role = new Role;
        $role->name = "Admin";
        $role->account_id = $account->id;
        $role->save();

        foreach ($permissions as $key => $permission) {
            $role_permission = new RolePermission;
            $role_permission->permission_id = $permission->id;
            $role_permission->role_id = $role->id;
            $role_permission->save();
        }

        $member = new Member;
        $member->user_id = $user->id;
        $member->account_id = $account->id;
        $member->role_id = $role->id;
        $member->save();

        PostgresRepository::createAccountSchema($account->id);

        // Create supervisor jobs files
        Artisan::call('jobs:update');

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Compte " . $account->name . " enregistré avec succès.");

        return redirect(action('AccountController@index'));
    }

    public function saveCustomIcon(Request $request)
    {
        $name = $request->input('name');
        $parameters = request()->route()->parameters;
        $account_id = $parameters['account_id'];
        $svgname = Str::random(5).$request->file('svg')->getClientOriginalName();

        if ($request->file('svg')){
            $path = $request->file('svg')->storeAs('accounts/' .$account_id. '/icons', $svgname,'public');

            $ico = new Ico;
            $ico->name = $name;
            $ico->path = $path;
            $ico->is_svg = true;
            $ico->account_id = $account_id;
            $ico->save();
        }
        return $ico;
    }
    public function customIcon(Request $request)
    {
        $parameters = request()->route()->parameters;
        $account_id = $parameters['account_id'];
        return DB::table('icos')
            ->where('icos.is_svg', true)
            ->where('icos.account_id', $account_id)
            ->select('icos.id', 'icos.path', 'icos.name as name')
            ->get();
    }

    public function deleteIconSvg(Request $request){
        $parameters = request()->route()->parameters;
        $account_id = $parameters['account_id'];
        $ico_id = $parameters['ico_id'];

        $account_services = DB::table('service_peis')
            ->join('icos_colors', 'icos_colors.id', '=', 'service_peis.ico_color_id')
            ->join('icos', 'icos_colors.ico_id', '=', 'icos.id')
            ->where('icos.id',$ico_id)
            ->where('service_peis.account_id',$account_id)
            ->select('service_peis.id as service_id', 'icos.id as ico_id')
            ->first();

        //Si je n ai aucun service associé à cette icone, je peux supprimer l icone et les ico_colors associées
        if ($account_services === null){
            IcoColor::where('ico_id',$ico_id)
                ->delete();

            Ico::where('id', $ico_id)
                ->delete();
            return 'OK';
        } else {
            return 'KO';
        }
    }
}
