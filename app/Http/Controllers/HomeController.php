<?php

namespace App\Http\Controllers;

use App\Repositories\MemberRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->init();

        $accounts = MemberRepository::getAccountsByUserId($this->current_user->id);

        if ($accounts->count() == 1 && !$this->current_user->is_superadmin) {
            return redirect(action('AccountController@home', $accounts->first()->id));
        }
        foreach ($accounts as $account) {
            $account->url = action('AccountController@home', $account->id);
        }
        $this->data['accounts'] = $accounts;
        return $this->render();
    }
}
