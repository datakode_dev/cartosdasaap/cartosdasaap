<?php

namespace App\Http\Controllers;

use App\Repositories\MemberRepository;

use Illuminate\Http\Request;

class ProxyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get data from the supplied URL and return it
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $this->init();

        $url = $request->input('url');

        if ($url) {
            return file_get_contents($url);
        }
        return null;
    }
}
