<?php

namespace App\Http\Controllers;

use App\Helpers\GeoHelper;
use App\Mail\Test;
use App\Repositories\LayerRepository;
use Illuminate\Support\Facades\Mail;
use Debugbar;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function mail()
    {
        Mail::to("test@datakode.fr")
            ->queue(new Test());
    }

    public function embed()
    {
        $this->data = [];
        return $this->render();
    }
    //cette methode va afficher un membre avec son nom, prénom, mail et son rôle
    public function show()
    {
        Debugbar::startMeasure('Controller', 'TestController@show');
        $this->init();
        $parameters = request()->route()->parameters;

        //TODO : il faudra ajouter l id du contexte en paramètre en fonction de la route
        $this->data['url_data_table'] = action('TestController@indexDatatable');

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    public function indexDatatable()
    {
        Debugbar::startMeasure('Controller', 'TestController@indexDatatable');
        $this->init();

        //En fonction de l'id du contexte à afficher, je recupere la zone_id et l'INSEE_limit
        $id_value = '1';
        $query = "SELECT insee_limit,zone_id FROM public.iso_contexts WHERE id=" . $id_value . " LIMIT 1";
        $context = DB::connection()->select($query);
        dd($context);

        $id_value = '1';
        $query = "SELECT id,insee,name,ST_AsText(geom) as geom FROM geo_data" . ".departements WHERE id=" . $id_value . " LIMIT 1";
        $datas = DB::connection()->select($query);

        $surface = $datas[0]->geom;
        $data = GeoHelper::wkt2json($surface);

        Debugbar::stopMeasure('Controller');
        return $data;
    }
}
