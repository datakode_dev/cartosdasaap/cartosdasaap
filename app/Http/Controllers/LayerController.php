<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLayer;
use App\Http\Requests\UpdateLayer;
use App\Interfaces\Controllers\LayerInterface;
use App\Jobs\ProcessLayer;
use App\Models\Layer;
use App\Repositories\LayerRepository;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class LayerController extends Controller implements LayerInterface
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des couches
     *
     * @return void
     */
    public function index()
    {
        Debugbar::startMeasure('Controller', 'LayerController@index');
        $this->middleware('account_member');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->data['url_data_table'] = action('LayerController@indexDatatable', [$this->current_account->id]);

        //ajout du fil d ariane
        $this->breadcrumb[] = (object) ["link" => route('layers.index', [$this->current_account->id]), "name" => 'Liste des couches', "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Renvoie les couches de la liste
     *
     * @return void
     */
    public function indexDatatable()
    {
        Debugbar::startMeasure('Controller', 'LayerController@index');
        $this->init();
        $can_edit = $this->hasPermission("can_edit");

        $layers = LayerRepository::getLayerAttibutsByAccountId($this->current_account->id);

        $layer_array = [];
        $token = csrf_token();
        foreach ($layers as $layer) {
            $layer->links = [];
            if ($can_edit === true) {
                if ($layer->context_step_move_id == null) {
                    $layer->links['edit'] = action('LayerController@edit', [$this->current_account->id, $layer->id]);
                    $layer->links['duplicate'] = action('LayerController@duplicate', [$this->current_account->id, $layer->id]);
                    $layer->links['token'] = $token;

                    if ($layer->active) {
                        $layer->links["archivate"] = action('LayerController@archivate', [$this->current_account->id, $layer->id]);
                    } else {
                        $layer->links["unarchivate"] = action('LayerController@unarchivate', [$this->current_account->id, $layer->id]);
                        $layer->links["delete"] = action('LayerController@delete', [$this->current_account->id, $layer->id]);
                    }
                }
                $layer->links["download"] = action('LayerController@download', [$this->current_account->id, $layer->id]);
                $layer->links['show'] = action('LayerController@show', [$this->current_account->id, $layer->id]);
            } else {
                $layer->links['show'] = action('LayerController@show', [$this->current_account->id, $layer->id]);
            }

            $layer_array[] = $layer;
        }
        $data = [];
        $data["data"] = $layer_array;
        Debugbar::stopMeasure('Controller');
        return $data;
    }

    /**
     * Téléchargement d'une couche
     *
     * @return file
     */
    public function download()
    {
        $this->init();
        $parameters = request()->route()->parameters;
        $layer = LayerRepository::getLayerGEOByIdAndAccountId($parameters['layer_id'], $this->current_account->id);

        if (!$layer) {
            abort(404);
        }

        $headers = array(
            'Content-Type: application/json',
        );
        $data = [
            "type" => "FeatureCollection",
            "name" => $layer->name,
            "crs" => [
                "type" => "name",
                "properties" => [
                    "name" => "urn:ogc:def:crs:OGC:1.3:CRS84",
                ],
            ],
            "features" => [json_decode($layer->geo)],
        ];
        file_put_contents(sys_get_temp_dir() . '/download.geojson', json_encode($data));

        return Response::download(sys_get_temp_dir() . '/download.geojson', Str::slug($layer->name) . '.geojson', $headers);
    }

    /**
     * Création d'une couche
     *
     * @return void
     */
    public function create()
    {
        Debugbar::startMeasure('Controller', 'LayerController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $this->breadcrumb[] = (object) ["link" => route('layers.index', [$this->current_account->id]), "name" => 'Liste des couches', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('layers.create', [$this->current_account->id]), "name" => "Créer une couche", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Enregistrement d'une couche
     *
     * @param StoreLayer $request
     * @return void
     */
    public function store(StoreLayer $request)
    {
        $parameters = request()->route()->parameters;
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();
        $path = $validated['fileToUpload']->store('accounts/1/layers');
        $layer = new Layer();
        $layer->setTableName($this->current_account->id);
        $layer->name = $validated['name'];
        $layer->active = true;
        $layer->path = $path;
        $layer->save();
        ProcessLayer::dispatch($layer->id, $this->current_account->id)->onQueue('default_' . $this->current_account->id);

        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Couche ' . $layer->name . ' enregistré avec succès.');

        return redirect(action('LayerController@index', [$this->current_account->id]));
    }

    /**
     * Page d'une couche
     *
     * @return void
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'LayerController@show');
        $this->init();

        $parameters = request()->route()->parameters;
        $this->breadcrumb[] = (object) ["link" => route('layers.index', [$this->current_account->id]), "name" => 'Liste des couches', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('layers.show', [$this->current_account->id, $parameters['layer_id']]), "name" => "Affichage d'une couche", "active" => true];

        $parameters = request()->route()->parameters;
        $this->data['layer'] = LayerRepository::getLayerGEOByIdAndAccountId($parameters['layer_id'], $this->current_account->id);

        if ($this->data['layer'] == null) {
            abort(404, 'Layer not found');
        }
        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Duplication d'une couche
     *
     * @param integer $account_id
     * @param integer $id
     * @return void
     */
    public function duplicate($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $layer_old = LayerRepository::getLayerGEOByIdAndAccountId($id, $this->current_account->id, false);

        $layer = new Layer();
        $layer->setTableName($this->current_account->id);
        $layer->name = $layer_old->name . " au " . Carbon::now()->format('d/m/Y');
        $layer->active = true;
        $layer->save();

        $query = "UPDATE account_" . $this->current_account->id . ".layers SET geom = ST_SetSRID(ST_GeomFromText('" . $layer_old->geo . "'),4326) WHERE id=" . $layer->id;

        DB::connection()->select($query);

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Couche " . $layer->name . " dupliqué avec succès.");

        return redirect(action('LayerController@edit', [$this->current_account->id, $layer->id]));
    }

    /**
     * Édition d'une couche
     *
     * @return void
     */
    public function edit()
    {
        Debugbar::startMeasure('Controller', 'LayerController@show');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $parameters = request()->route()->parameters;
        $this->breadcrumb[] = (object) ["link" => route('layers.index', [$this->current_account->id]), "name" => 'Liste des couches', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('layers.edit', [$this->current_account->id, $parameters['layer_id']]), "name" => "Edition d'une couche", "active" => true];

        $parameters = request()->route()->parameters;
        $this->data['layer'] = LayerRepository::getLayerGEOByIdAndAccountId($parameters['layer_id'], $this->current_account->id);

        if ($this->data['layer'] == null) {
            abort(404, 'Layer not found');
        }
        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Mise à jour d'une couche
     *
     * @param integer $account_id
     * @param integer $id
     * @param UpdateLayer $request
     * @return void
     */
    public function update($account_id, $id, UpdateLayer $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();
        $layer = new Layer();
        $layer->setTableName($this->current_account->id);
        $layer = $layer->find($id);
        $layer->setTableName($this->current_account->id);
        $layer->name = $validated['name'];
        $layer->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Couche ' . $layer->name . ' modifié avec succès.');

        return redirect(action('LayerController@index', [$this->current_account->id]));
    }

    /**
     * Archive une couche
     *
     * @return void
     */
    public function archivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $layer = LayerRepository::getLayerByIdAndAccountId($parameters['layer_id'], $this->current_account->id);
        if ($layer == null) {
            abort(404, 'Layer not found');
        }
        LayerRepository::archivate($layer, $this->current_account->id);

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Couche " . $layer->name . " archivé avec succès.");

        return redirect(action('LayerController@index', [$this->current_account->id]));
    }

    /**
     * Désarchive une couche
     *
     * @return void
     */
    public function unarchivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $layer = LayerRepository::getLayerByIdAndAccountId($parameters['layer_id'], $this->current_account->id);
        if ($layer == null) {
            abort(404, 'Layer not found');
        }
        LayerRepository::unarchivate($layer, $this->current_account->id);

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Couche " . $layer->name . " déarchivé avec succès.");

        return redirect(action('LayerController@index', [$this->current_account->id]));
    }

    /**
     * Supprime une couche
     *
     * @return void
     */
    public function delete()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $layer = LayerRepository::getLayerByIdAndAccountId($parameters['layer_id'], $this->current_account->id);
        if ($layer == null) {
            abort(404, 'Layer not found');
        }
        LayerRepository::removeAndRemovechild($layer, $this->current_account->id);

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Couche " . $layer->name . " supprimer avec succès.");

        return redirect(action('LayerController@index', [$this->current_account->id]));
    }
}
