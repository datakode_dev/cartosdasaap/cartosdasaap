<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreResearchContext;
use App\Models\IgnKey;
use App\Models\ResearchContext;
use App\Models\ResearchContextRadius;
use App\Models\ResearchContextService;
use App\Repositories\IgnKeyRepository;
use App\Repositories\InseeRepository;

class ResearchContextsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des contextes de recherche
     *
     */
    public function index()
    {
        $this->init();
        $ign_keys = IgnKeyRepository::getIgnKeysByAccountId($this->current_account->id);
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->data['has_ign_key'] = !$ign_keys->isEmpty();
        $this->breadcrumb[] = (object) ["link" => route('research_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes de recherche', "active" => true];
        $this->data['url_data_table'] = action('ResearchContextsController@indexDatatable', [$this->current_account->id]);
        return $this->render();
    }

    /**
     * Renvoi les contextes de recherche
     *
     * @return array
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("can_edit");
        $contexts = ResearchContext::where('account_id', $this->current_account->id)->get();
        $contexts_array = [];
        $token = csrf_token();
        foreach ($contexts as $context) {
            if ($can_edit === true) {
                $links = [
                    'token' => $token,
                    'edit' => action('ResearchContextsController@edit', [$this->current_account->id, $context->id]),
                    'show' => action('ResearchContextsController@show', [$this->current_account->id, $context->id]),
                    'duplicate' => action('ResearchContextsController@duplicate', [$this->current_account->id, $context->id]),
                ];

                if ($context->active) {
                    $links["archivate"] = route('research_contexts.unarchivate', [$this->current_account->id, $context->id]);
                } else {
                    $links["unarchivate"] = route('research_contexts.unarchivate', [$this->current_account->id, $context->id]);
                    $links["delete"] = action('ResearchContextsController@delete', [$this->current_account->id, $context->id]);
                }
            } else {
                $links = [
                    'show' => action('ResearchContextsController@show', [$this->current_account->id, $context->id]),
                ];
            }

            $context->links = $links;
            $contexts_array[] = $context;
        }

        return ["data" => $contexts_array];
    }

    /**
     * Création de contexte de recherche
     */

    public function create()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $this->breadcrumb[] = (object) ["link" => route('research_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes de recherche', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('research_contexts.create', [$this->current_account->id]), "name" => 'Créer un contexte de recherche', "active" => true];

        $service_peis_models = $this->current_account->service_peis;

        $service_peis = [];
        foreach ($service_peis_models as $key => $service_pei_model) {
            $service_pei = (object) [];

            $service_pei->value = $service_pei_model->id;
            $service_pei->name = $service_pei_model->name;
            $service_peis[] = $service_pei;
        }

        $this->data['service_peis'] = collect($service_peis);

        $ign_keys_models = IgnKeyRepository::getIgnKeysByAccountId($this->current_account->id);
        $ign_keys = [];
        foreach ($ign_keys_models as $ign_key) {
            $ign_key_re = (object) [];

            $ign_key_re->value = $ign_key->id;
            $ign_key_re->name = $ign_key->name;
            $ign_keys[] = $ign_key_re;
        }
        $this->data['ign_keys'] = collect($ign_keys);

        return $this->render();
    }

    /**
     * Edition d'un contexte de recherche
     *
     * @param $account_id
     * @param $id
     */
    public function edit($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $this->breadcrumb[] = (object) ["link" => route('research_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes de recherche', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('research_contexts.create', [$this->current_account->id]), "name" => 'Créer un contexte de recherche', "active" => true];

        $context = $this->getContext($account_id, $id);
        $this->data['research_context'] = $context;

        $this->data['current_services'] = $context->getResearchContextServices()->map(function ($s) {
            return $s->service_pei_id;
        })->all();
        $inseeRepository = new InseeRepository();
        $insee = $inseeRepository->getFromInseeAndZoneId($context->insee_limit, $context->zone_id);

        $this->data['current_limit'] = [
            'value' => $insee['insee'] . '.' . $insee['zone_id'],
            'text' => $insee['name'],
        ];
        $service_peis_models = $this->current_account->service_peis;

        $service_peis = [];
        foreach ($service_peis_models as $key => $service_pei_model) {
            $service_pei = (object) [];

            $service_pei->value = $service_pei_model->id;
            $service_pei->name = $service_pei_model->name;
            $service_peis[] = $service_pei;
        }

        $this->data['service_peis'] = collect($service_peis);

        $ign_keys_models = IgnKeyRepository::getIgnKeysByAccountId($this->current_account->id);
        $ign_keys = [];
        foreach ($ign_keys_models as $ign_key) {
            $ign_key_re = (object) [];

            $ign_key_re->value = $ign_key->id;
            $ign_key_re->name = $ign_key->name;
            $ign_keys[] = $ign_key_re;
        }
        $this->data['ign_keys'] = collect($ign_keys);

        return $this->render();
    }

    /**
     * Archiver le contexte de recherche
     *
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archivate($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        /**
         * @var ResearchContext $context
         */
        $context = $this->getContext($account_id, $id);

        $context->active = !$context->active;
        $context->save();
        return redirect(action('ResearchContextsController@index', [$this->current_account->id]));
    }

    /**
     * Dupliquer le contexte de recherche
     *
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function duplicate($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        /**
         * @var ResearchContext $context
         */
        $context = $this->getContext($account_id, $id);

        $newContext = $context->replicate();
        $newContext->save();

        foreach ($context->getResearchContextServices() as $rcs) {
            $newRCS = new ResearchContextService();
            $newRCS->service_pei_id = $rcs->service_pei_id;
            $newRCS->research_context_id = $newContext->id;
            $newRCS->save();
        }

        foreach ($context->getResearchContextRadiuses() as $radius) {
            $newRadius = new ResearchContextRadius();
            $newRadius->research_context_id = $newContext->id;
            $newRadius->value = $radius->value;
            $newRadius->save();
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context de recherche " . $newContext->name . " dupliqué avec succès.");

        return redirect(action('ResearchContextsController@index', [$this->current_account->id]));
    }

    /**
     * Supprimer le contexte de recherche
     *
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $research_context = $this->getContext($account_id, $id);
        ResearchContextService::where('research_context_id', $research_context->id)->delete();
        $research_context->delete();
        return redirect(action('ResearchContextsController@index', [$this->current_account->id]));
    }

    /**
     * Enregistrement d'un contexte de recherche
     *
     * @param StoreResearchContext $request
     * @param $account_id
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreResearchContext $request, $account_id, $id = null)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();
        $research_context = new ResearchContext();

        if ($id !== null) {
            $research_context = $this->getContext($account_id, $id);
        }

        $research_context->name = $validated['name'];
        $research_context->active = true;
        $research_context->insee_limit = explode('.', $validated['limit'])[0];
        $research_context->zone_id = explode('.', $validated['limit'])[1];
        $research_context->account_id = $this->current_account->id;
        $research_context->ign_key_id = $validated['ign_key_id'];
        $research_context->referrers = null;
        $research_context->dist_around_limit = $validated['dist_around_limit'];
        $research_context->display_city = isset($validated['display_city']) ? 1 : 0;
        $research_context->display_route = isset($validated['display_route']) ? 1 : 0;
        $research_context->display_selection = isset($validated['display_selection']) ? 1 : 0;
        $research_context->display_legend = isset($validated['display_legend']) ? 1 : 0;

        if (isset($validated['referrers']) && count($validated['referrers']) > 0) {
            $research_context->referrers = join(',', $validated['referrers']);
        }

        $research_context->save();
        if (isset($validated['services'])) {
            //Delete existing context services
            ResearchContextService::where('research_context_id', $research_context->id)->delete();
            foreach ($validated['services'] as $service) {
                $research_context_service = new ResearchContextService;
                $research_context_service->research_context_id = $research_context->id;
                $research_context_service->service_pei_id = $service;
                $research_context_service->save();
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context de recherche " . $research_context->name . " enregistré avec succès.");

        return redirect(action('ResearchContextsController@index', [$this->current_account->id]));
    }

    /**
     * Mise à jour du contexte de recherche
     *
     * @param StoreResearchContext $request
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreResearchContext $request, $account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();

        $research_context = $this->getContext($account_id, $id);

        $research_context->name = $validated['name'];
        $research_context->active = true;
        $research_context->insee_limit = explode('.', $validated['limit'])[0];
        $research_context->zone_id = explode('.', $validated['limit'])[1];
        $research_context->account_id = $this->current_account->id;
        $research_context->ign_key_id = $validated['ign_key_id'];
        $research_context->referrers = null;
        $research_context->dist_around_limit = $validated['dist_around_limit'];
        $research_context->display_city = isset($validated['display_city']) ? 1 : 0;
        $research_context->display_route = isset($validated['display_route']) ? 1 : 0;
        $research_context->display_selection = isset($validated['display_selection']) ? 1 : 0;
        $research_context->display_legend = isset($validated['display_legend']) ? 1 : 0;

        if (isset($validated['referrers']) && count($validated['referrers']) > 0) {
            $research_context->referrers = join(',', $validated['referrers']);
        }

        $research_context->save();
        if (isset($validated['services'])) {
            //Delete existing context services
            ResearchContextService::where('research_context_id', $research_context->id)->delete();
            foreach ($validated['services'] as $service) {
                $research_context_service = new ResearchContextService;
                $research_context_service->research_context_id = $research_context->id;
                $research_context_service->service_pei_id = $service;
                $research_context_service->save();
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Context de recherche " . $research_context->name . " enregistré avec succès.");

        return redirect(action('ResearchContextsController@index', [$this->current_account->id]));
    }

    /**
     * Affichage du contexte de recherche
     *
     * @param $account_id
     * @param $id
     */
    public function show($account_id, $id)
    {
        $context = $this->getContext($account_id, $id);

        $this->breadcrumb[] = (object) ["link" => route('research_contexts.index', [$this->current_account->id]), "name" => 'Liste des contextes de recherche', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('research_contexts.show', [$this->current_account->id, $id]), "name" => 'Contexte de recherche ' . $context->name, "active" => true];

        $key = IgnKey::find($context->ign_key_id);

        $this->data['services'] = $context->getServices()->map(function ($s) {
            $r = new \stdClass();
            $r->value = $s->id;
            $r->name = $s->name;
            return $r;
        });
        $this->data['context'] = $context;
        $this->data['key'] = $key;

        return $this->render();
    }

    /**
     * JSON contexte de recherche
     *
     * @param $account_id
     * @param $id
     */
    public function filter($account_id, $id)
    {
        $context = ResearchContext::where('id', $id)->first();
        return json_encode($context);
    }

    /**
     * Récuperer contexte de recherche
     *
     * @param $account_id
     * @param $id
     * @return ResearchContext
     */
    protected function getContext($account_id, $id)
    {
        $this->init();

        /**
         * @var ResearchContext $context
         */
        $context = ResearchContext::find((int) $id);

        if (!$context || $context->account_id !== (int) $account_id || $this->current_account->id !== (int) $account_id) {
            abort(404, 'Ce contexte n\'existe pas');
        }
        return $context;
    }
}
