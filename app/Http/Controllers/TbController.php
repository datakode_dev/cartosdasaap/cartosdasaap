<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTb;
use App\Jobs\ProcessBoardLayer;
use App\Models\BoardLayer;
use App\Models\Tb;
use App\Repositories\BoardRepository;
use App\Repositories\InseeRepository;
use App\Repositories\LayerRepository;
use App\Repositories\PostgresRepository;
use App\Repositories\ServicePeiRepository;
use Illuminate\Support\Facades\DB;

class TbController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Création d'un tableau de bord
     */
    public function create()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $this->breadcrumb[] = (object) ["link" => route('tb.index', [$this->current_account->id]), "name" => 'Liste des tableaux de bord', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('tb.create', [$this->current_account->id]), "name" => "Créer un tableau de bord", "active" => true];

        $this->data['layers'] = $this->getLayers();

        $this->data['indicators'] = $this->getIndicators();

        return $this->render();
    }

    /**
     * Enregistrement d'un tableau de bord
     *
     * @param BoardRepository $boardRepository
     * @param StoreTb $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BoardRepository $boardRepository, StoreTb $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();

        $bord = new Tb;
        if (isset($validated['id']) && $validated['id'] !== null) {
            $bord = Tb::find((int) $validated['id']);
        }

        $bord->insee_limit = explode('.', $validated['limit'])[0];
        $bord->zone_id = explode('.', $validated['limit'])[1];
        $bord->indicators = json_encode($validated['indicators']);
        $bord->account_id = $this->current_account->id;
        $bord->name = $validated['name'];
        if (isset($validated['load_full'])) {
            $bord->load_full = true;
        } else {
            $bord->load_full = false;
        }
        $bord->active = 1;

        $bord->referrers = null;

        if (isset($validated['referrers']) && count($validated['referrers']) > 0) {
            $bord->referrers = join(',', $validated['referrers']);
        }

        $bord->save();
        $board_layers_id = [];
        foreach ($validated['layers'] as $key => $layer_id) {
            if ($layer_id != null) {
                $board_layer = BoardLayer::where('board_id', $bord->id)->where('layer_id', $layer_id)->first();
                if ($board_layer == null) {
                    $board_layer = new BoardLayer;
                    $board_layer->layer_id = $layer_id;
                    $board_layer->board_id = $bord->id;
                    $board_layer->save();
                    ProcessBoardLayer::dispatch($board_layer->id, $this->current_user)->onQueue('default_' . $this->current_account->id);
                }
                $board_layers_id[] = $board_layer->id;

            }
        }
        $board_layers = BoardLayer::whereNotIn('id', $board_layers_id)->where('board_id', $bord->id)->get();
        $board_layers_id_to_remove = [];

        foreach ($board_layers as $board_layer) {
            $board_layers_id_to_remove[] = $board_layer->id;
        }

        $boardRepository->deleteBoardData($bord->id, $board_layers_id_to_remove);
        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Tableau de bord ' . $bord->name . ' enregistré avec succès.');
        return redirect(action('TbController@index', [$this->current_account->id]));
    }

    /**
     * Edition d'un tableau de bord
     *
     * @param InseeRepository $inseeRepository
     * @param $accountId
     * @param $id
     */
    public function edit(InseeRepository $inseeRepository, $accountId, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        $this->breadcrumb[] = (object) ["link" => route('tb.index', [$this->current_account->id]), "name" => 'Liste des tableaux de bord', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('tb.edit', [$this->current_account->id, $parameters['tb_id']]), "name" => "Edition d'un tableau de bord", "active" => true];

        /**
         * @var Tb $board
         */
        $board = Tb::find((int) $id);

        if (!$board) {
            abort(404, 'Ce tableau de bord n\'existe pas');
        }

        $this->data['board'] = $board;
        $insee = $inseeRepository->getFromInseeAndZoneId($board->insee_limit, $board->zone_id);
        $this->data['current_limit'] = [
            'value' => $insee['insee'] . '.' . $insee['zone_id'],
            'text' => $insee['name'],
        ];

        $this->data['current_layers'] = $board->getLayers()->map(function ($l) {
            return $l->id;
        })->all();

        $this->data['current_indicators'] = json_decode($board->indicators);

        $this->data['layers'] = $this->getLayers();
        $this->data['indicators'] = $this->getIndicators();

        return $this->render();
    }

    /**
     * Duplication d'un tableau de bord
     *
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function duplicate($account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $tb = Tb::find((int) $id);

        if (!$tb || $tb->account_id !== (int) $account_id || $this->current_account->id !== (int) $account_id) {
            abort(404, 'Context not found');
        }

        $newTb = $tb->replicate();
        $newTb->save();

        foreach (BoardLayer::where('board_id', $tb->id)->get() as $board_layer) {
            $new_board_layer = new BoardLayer;
            $new_board_layer->layer_id = $board_layer->layer_id;
            $new_board_layer->board_id = $newTb->id;
            $new_board_layer->save();
        }
        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Tableau de bord " . $newTb->name . " dupliqué avec succès.");

        return redirect(action('TbController@edit', [$this->current_account->id, $newTb->id]));
    }

    /**
     * Affichage d'un tableau de bord
     *
     * @param InseeRepository $inseeRepository
     * @param $account_id
     * @param $id
     */
    public function show(InseeRepository $inseeRepository, $account_id, $id)
    {
        $this->init();
        $parameters = request()->route()->parameters;

        $this->breadcrumb[] = (object) ["link" => route('tb.index', [$this->current_account->id]), "name" => 'Liste des tableaux de bord', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('tb.show', [$this->current_account->id, $parameters['tb_id']]), "name" => "Affichage d'un tableau de bord", "active" => true];

        /**
         * @var Tb $board
         */
        $board = Tb::find((int) $id);
        $limit = $inseeRepository->getFromInseeAndZoneId($board->insee_limit, $board->zone_id);
        $board->limit_name = $limit['name'];
        if (!$board) {
            abort(404, 'Ce tableau de bord n\'existe pas');
        }

        $this->data['board'] = $board;
        $this->data['layers'] = $board->getLayers();
        return $this->render();
    }

    /**
     * Suppression d'un tableau de bord
     *
     * @param BoardRepository $boardRepository
     * @param $account_id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(BoardRepository $boardRepository, $account_id, $id)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        /**
         * @var Tb $board
         */
        $board = Tb::find((int) $id);

        if (!$board) {
            abort(404, 'Ce tableau de bord n\'existe pas');
        }
        if ($board->active !== false) {
            abort(400, 'Ce tableau de bord est encore actif');
        }
        $boardRepository->deleteBoardData($board->id);
        $board->delete();
        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Tableau de bord ' . $board->name . ' supprimé avec succès.');
        return redirect(action('TbController@index', [$this->current_account->id]));
    }

    /**
     * Liste des des tableaux de bord
     */
    public function index()
    {
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->breadcrumb[] = (object) ["link" => route('tb.index', [$this->current_account->id]), "name" => 'Liste des tableaux de bord', "active" => true];
        $this->data['url_data_table'] = action('TbController@indexDatatable', [$this->current_account->id]);
        return $this->render();
    }

    /**
     * Retourne la liste des tableaux de bord
     *
     * @return array
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("can_edit");
        $tbs = Tb::where('account_id', $this->current_account->id)->get();
        $tbs_array = [];
        $token = csrf_token();
        foreach ($tbs as $tb) {
            if ($can_edit === true) {
                $links = [
                    'token' => $token,
                    'edit' => action('TbController@edit', [$this->current_account->id, $tb->id]),
                    'duplicate' => action('TbController@duplicate', [$this->current_account->id, $tb->id]),
                    'show' => action('TbController@show', [$this->current_account->id, $tb->id]),
                ];
                if ($tb->active) {
                    $links["archivate"] = action('TbController@archivate', [$this->current_account->id, $tb->id]);
                } else {
                    $links["unarchivate"] = action('TbController@unarchivate', [$this->current_account->id, $tb->id]);
                    $links["delete"] = action('TbController@delete', [$this->current_account->id, $tb->id]);
                }
            } else {
                $links = [
                    'show' => action('TbController@show', [$this->current_account->id, $tb->id]),
                ];
            }
            $tb->links = $links;

            switch ($tb->status) {
                case Tb::STATUS_NOT_STARTED:
                    $tb->status = ["code" => Tb::STATUS_NOT_STARTED, "label" => "Non démarré"];
                    break;
                case Tb::STATUS_STARTED:
                    $tb->status = ["code" => Tb::STATUS_STARTED, "label" => "En cours de calcul"];
                    break;
                case Tb::STATUS_ERROR:
                    $tb->status = ["code" => Tb::STATUS_ERROR, "label" => "Erreur de calcul"];
                    break;
                case Tb::STATUS_DONE:
                    $tb->status = ["code" => Tb::STATUS_DONE, "label" => "Terminé"];
                    break;
                default:
                    $tb->status = ["code" => $tb->status];
                    break;
            }

            //affichage du territoire
            $test = new InseeRepository();
            $tb->limit = $test->getNameFromZoneAndInseeLimit($tb->insee_limit, $tb->zone_id);

            //Affichage des couches associées au tableau de bord
            $layers_names = [];
            $blayers = BoardLayer::where('board_id', $tb->id)->get();

            if ($blayers) {
                foreach ($blayers as $blayer) {
                    $names = DB::select('SELECT name FROM account_' . $this->current_account->id . '.layers WHERE id =' . $blayer->layer_id);

                    foreach ($names as $name) {
                        $layers_names[] = $name->name;
                    }
                }
            }
            $tb->layers = $layers_names;

            $tbs_array[] = $tb;
        }
        $data = [];
        $data["data"] = $tbs_array;
        return $data;
    }

    /**
     * Archivage d'un tableau de bord
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        $tb = Tb::where('account_id', $this->current_account->id)->where('active', true)->where('id', $parameters['tb_id'])->first();
        $tb->active = false;
        $tb->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Tableau de bord " . $tb->name . " archivé avec succès.");

        return redirect(action('TbController@index', [$this->current_account->id]));
    }

    /**
     * Désarchivage d'un tableau de bord
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function unarchivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        $tb = Tb::where('account_id', $this->current_account->id)->where('active', false)->where('id', $parameters['tb_id'])->first();
        $tb->active = true;
        $tb->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Tableau de bord " . $tb->name . " déarchivé avec succès.");

        return redirect(action('TbController@index', [$this->current_account->id]));
    }

    /**
     * Récuperation des couches pour ce compte
     *
     * @return \Illuminate\Support\Collection
     */
    private function getLayers()
    {
        $layers_models = LayerRepository::getLayerAttibutsByAccountId($this->current_account->id);

        $layers = [];

        foreach ($layers_models as $layers_model) {
            $layer = (object) [];

            $layer->value = $layers_model->id;
            $layer->name = $layers_model->name;
            $layers[] = $layer;
        }
        return collect($layers);
    }

    /**
     * Récuperation des indicateurs
     *
     * @return array|\Illuminate\Support\Collection
     */
    private function getIndicators()
    {
        $indicators = PostgresRepository::getColNameFromTable('insee_caropop');
        $indicators_format = [];
        foreach ($indicators as $key => $indicator) {
            if ('indindicators.insee.' . $indicator->column_name !== trans('indindicators.insee.' . $indicator->column_name)) {
                $indicator_format = (object) [];
                $indicator_format->value = 'indindicators.insee.' . $indicator->column_name;
                $indicator_format->name = trans('indindicators.insee.' . $indicator->column_name);
                $indicators_format[] = $indicator_format;
            }
        }

        $indicators = PostgresRepository::getColNameFromTable('new_insee_caropop');
        foreach ($indicators as $key => $indicator) {
            if ('indindicators.insee_2015.' . $indicator->column_name !== trans('indindicators.insee_2015.' . $indicator->column_name)) {
                $indicator_format = (object) [];
                $indicator_format->value = 'indindicators.insee_2015.' . $indicator->column_name;
                $indicator_format->name = trans('indindicators.insee_2015.' . $indicator->column_name);
                $indicators_format[] = $indicator_format;
            }
        }

        $indicatorsCollection = collect($indicators_format);

        $services = ServicePeiRepository::getAllFromAccountToSelect($this->current_account->id)->map(function ($service) {
            // Use 'table.{service_id} as select value
            $service->value = 'table.' . $service->value;
            return $service;
        });

        $total = $indicatorsCollection->merge($services);
        $total[] = (object) ["value" => "indindicators.batiments.nb", "name" => trans("indindicators.batiments")];

        return $total;
    }
}
