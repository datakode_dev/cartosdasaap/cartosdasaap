<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIcoColor;
use App\Http\Requests\UpdateIcoColor;
use App\Interfaces\Controllers\IcoColorInterface;
use App\Models\Color;
use App\Models\Ico;
use App\Models\IcoColor;
use App\Models\ServicePei;
use App\Repositories\IconRepository;
use Debugbar;

class IcoColorController extends Controller implements IcoColorInterface
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des marqueurs
     *
     * @return void
     */
    public function index()
    {
        Debugbar::startMeasure('Controller', 'IcoColorController@index');
        $this->middleware('account_member');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->data['url_data_table'] = action('IcoColorController@indexDatatable', [$this->current_account->id]);

        //ajout du fil d ariane
        $this->breadcrumb[] = (object) ["link" => route('ico_color.index', [$this->current_account->id]), "name" => 'Liste des marqueurs', "active" => true];
        Debugbar::startMeasure('Controller', 'IcoColorController@index');
        return $this->render();
    }
    /**
     * Renvoie les marqueurs de la liste
     *
     * @return array
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("can_edit");

        //j appelle ce QB qui va recuperer l id, le nom, le nom de la couleur et l'icone de chaque marqueur
        $icons = IconRepository::getIconAttibutsByAccountId($this->current_account->id);
        $ico_colors_array = [];
        //je vais parcourir toutes les icones et remplir les champs icone et links
        foreach ($icons as $icon) {
            $icon->icone = '<i class="' . $icon->icon_name . '" style="color:' . $icon->color_name . '"></i>';
            if ($can_edit === true) {
                $icon->links = [
                    'edit' => action('IcoColorController@edit', [$this->current_account->id, $icon->id]),
                    'show' => action('IcoColorController@show', [$this->current_account->id, $icon->id]),
                    'delete' => action('IcoColorController@delete', [$this->current_account->id, $icon->id]),
                ];
            } else {
                $icon->links = [
                    'show' => action('IcoColorController@show', [$this->current_account->id, $icon->id]),
                ];
            }
            $ico_colors_array[] = $icon;
        }
        $data = [];
        $data["data"] = $ico_colors_array;
        return $data;
    }
    /**
     * Page de création d'un marqueur
     *
     * @return void
     */
    public function create()
    {
        Debugbar::startMeasure('Controller', 'IcoColorController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('ico_color.index', [$this->current_account->id]), "name" => 'Liste des marqueurs', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('ico_color.create', [$this->current_account->id]), "name" => "Créer un marqueur", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Cette methode va me permettre d afficher un marqueur avec ses paramètres
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'IsoColorController@create');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $parameters = request()->route()->parameters;
        $ico_color_id = $parameters['ico_color_id'];

        //je recupere le marqueur dont on veut afficher les caracteristiques (nom, couleur_id et icone_id)
        $ico_color = IcoColor::where('id', $ico_color_id)
            ->where('is_real', true)
            ->first();

        $color = Color::where('id', $ico_color->color_id)
            ->first();

        $icon = Ico::where('id', $ico_color->ico_id)
            ->first();

        $this->data["ico_color"] = $ico_color;
        $this->data["color"] = $color;
        $this->data["icon"] = $icon;

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('ico_color.index', [$this->current_account->id]), "name" => 'Liste des marqueurs', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('ico_color.edit', [$this->current_account->id, $parameters['ico_color_id']]), "name" => "Affichage d'un marqueur", "active" => true];

        return $this->render();
    }

    /**
     * Cette methode va me permettre d'enregistrer un nouveau marker
     *
     * @param StoreIcoColor $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreIcoColor $request)
    {
        Debugbar::startMeasure('Controller', 'IsoColorController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $current_account = $parameters['account_id'];
        $validated = $request->validated();

        // J'enregistre la couleur du marqueur dans la table color et je recupere l'id de l'objet marker color
        $marker_color = new Color();
        $marker_color->name = $validated['marker_color'];
        $marker_color->save();
        $marker_color_id = $marker_color->id;

        // J'enregistre l'icone du marqueur dans la table icos et je recupere l'id de l'objet marker icon
        $marker_icon = new Ico();
        $marker_icon->name = $validated['marker_icon'];
        $marker_icon->save();
        $marker_icon_id = $marker_icon->id;

        //j enregistre l'id couleur et l'id icon dans la table icos_colors pour lier le marqueur avec son nom, sa couleur et son icone
        $marker_icos_color = new IcoColor();
        $marker_icos_color->name = $validated['marker_name'];
        $marker_icos_color->ico_id = $marker_icon_id;
        $marker_icos_color->color_id = $marker_color_id;
        $marker_icos_color->account_id = $current_account;
        $marker_icos_color->is_real = true;
        $marker_icos_color->save();

        Debugbar::stopMeasure('Controller');
        return redirect(action('IcoColorController@index', [$current_account]));
    }

    /**
     * Cette methode ve ma permettre d'editer un marqueur existant
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        Debugbar::startMeasure('Controller', 'IsoColorController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $ico_color_id = $parameters['ico_color_id'];

        //je recupere le marqueur dont on veut afficher les caracteristiques (nom, couleur_id et icone_id)
        $ico_color = IcoColor::where('id', $ico_color_id)
            ->where('is_real', true)
            ->first();

        $color = Color::where('id', $ico_color->color_id)
            ->first();

        $icon = Ico::where('id', $ico_color->ico_id)
            ->first();

        $this->data["ico_color"] = $ico_color;
        $this->data["color"] = $color;
        $this->data["icon"] = $icon;

        //Ajout du fil d'ariane
        $this->breadcrumb[] = (object) ["link" => route('ico_color.index', [$this->current_account->id]), "name" => 'Liste des marqueurs', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('ico_color.edit', [$this->current_account->id, $parameters['ico_color_id']]), "name" => "Edition d'un marqueur", "active" => true];

        return $this->render();
    }

    /**
     * Cette methode me permet de mettre à jour les informations d'un marqueur.
     *
     * @param UpdateIcoColor $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateIcoColor $request)
    {
        Debugbar::startMeasure('Controller', 'IsoColorController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $current_account = $parameters['account_id'];
        $ico_color_id = $parameters['ico_color_id'];
        $validated = $request->validated();

        //Je mets à jour le nom du marqueur dans la table icos_colors
        IcoColor::where('id', $ico_color_id)
            ->where('is_real', true)
            ->update(['name' => $validated['marker_name']]);

        // je recupere les attributs de l'objet ico_color avec l id que j'enregistre dans les variables $ico_id et $color_id
        $ico_color = IcoColor::where('id', $ico_color_id)
            ->where('is_real', true)
            ->first();

        $ico_id = $ico_color->ico_id;
        $color_id = $ico_color->color_id;

        //je mets à jour la couleur dans la table colors
        Color::where('id', $color_id)
            ->update(['name' => $validated['marker_color']]);

        //Je mets à jour l'icone dans la table icos
        Ico::where('id', $ico_id)
            ->update(['name' => $validated['marker_icon']]);

        Debugbar::stopMeasure('Controller');
        return redirect(action('IcoColorController@index', [$current_account]));
    }

    /**
     * Suppression d'un marqueur de la liste des marqueurs
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(){
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $current_account = $parameters['account_id'];

        IcoColor::where('id', $parameters['ico_color_id'])
            ->delete();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'Le marqueur a été supprimé.');

        Debugbar::stopMeasure('Controller');
        return redirect(action('IcoColorController@index', [$current_account]));
    }

    public function archivate($request)
    {
    }
    public function unarchivate($request)
    {
    }
}
