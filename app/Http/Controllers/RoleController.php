<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRole;
use App\Http\Requests\UpdateRole;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Repositories\RoleRepository;
use Debugbar;
use Symfony\Component\HttpFoundation\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }

    /**
     * Liste des rôles
     */
    public function index()
    {
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("roles_all");
        $this->breadcrumb[] = (object) ["link" => route('roles.index', [$this->current_account->id]), "name" => 'Liste des rôles', "active" => true];
        $this->data['url_data_table'] = action('RoleController@indexDatatable', [$this->current_account->id]);
        return $this->render();
    }

    /**
     * Renvoie les rôles de la liste
     *
     * @return array
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("roles_all");

        $roles = RoleRepository::getRoleAttibutsByAccountId($this->current_account->id);
        $token = csrf_token();

        foreach ($roles as $key => $role) {
            $role->links = [];

            if ($can_edit === true) {

                $role->links["edit"] = action('RoleController@edit', [$this->current_account->id, $role->id]);
                $role->links["show"] = action('RoleController@show', [$this->current_account->id, $role->id]);

                if ($role->nb_member == 0) {
                    $role->links['token'] = $token;
                    if ($role->active) {
                        $role->links["archivate"] = action('RoleController@archivate', [$this->current_account->id, $role->id]);
                    } else {
                        $role->links["unarchivate"] = action('RoleController@unarchivate', [$this->current_account->id, $role->id]);
                        $role->links["delete"] = action('RoleController@delete', [$this->current_account->id, $role->id]);
                    }
                }
            } else {
                $role->links["show"] = action('RoleController@show', [$this->current_account->id, $role->id]);
            }

            $roles[$key] = $role;
        }
        $data = [];
        $data["data"] = $roles;
        return $data;
    }

    /**
     * Création d'un role
     */
    public function create()
    {
        Debugbar::startMeasure('Controller', 'RoleController@create');

        $this->init();
        $this->hasPermissionOrAbord("roles_all");

        $permissions_models = Permission::All();
        $permissions = [];
        foreach ($permissions_models as $key => $permission_model) {
            $permission = (object) [];

            $permission->value = $permission_model->id;
            $permission->name = $permission_model->name;
            $permissions[] = $permission;
        }

        $this->data['permissions'] = collect($permissions);
        $this->breadcrumb[] = (object) ["link" => route('roles.index', [$this->current_account->id]), "name" => 'Liste des rôles', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('roles.create', [$this->current_account->id]), "name" => "Créer un rôle", "active" => true];
        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Enregistrement d'un rôle
     *
     * @param StoreRole $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRole $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("roles_all");
        $validated = $request->validated();

        $role = new Role;
        $role->name = $validated['name'];
        $role->account_id = $this->current_account->id;
        $role->save();

        if (isset($validated['permissions'])){
            $permissions = Permission::whereIn('id', $validated['permissions'])->get();

            foreach ($permissions as $key => $permission) {
                $role_permission = new RolePermission;
                $role_permission->permission_id = $permission->id;
                $role_permission->role_id = $role->id;
                $role_permission->save();
            }
        }

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Rôle " . $role->name . " enregistré avec succès.");

        return redirect(action('RoleController@index', [$this->current_account->id]));
    }

    /**
     * Edition d'un rôle
     */
    public function edit()
    {
        $this->init();
        $this->hasPermissionOrAbord("roles_all");
        //On va recuperer l'id de l account
        $account_id = $this->current_account->id;

        $parameters = request()->route()->parameters;

        //on recupere un role de cet account definit par l'utilisateur
        $role = Role::where('account_id', $account_id)
            ->where('id', $parameters['role_id'])
            ->first();

        // je recupere toutes les permissions attribuées à ce role
        $role_permission = RolePermission::where('role_id', $parameters['role_id'])
            ->get();

        //je stocke le role et les permissions de ce role dans $this->data
        $this->data["role_permission"] = $role_permission;
        $this->data["role"] = $role;

        $permissions_models = Permission::All();
        $permissions = [];
        foreach ($permissions_models as $key => $permission_model) {
            $permission = (object) [];

            $permission->value = $permission_model->id;
            $permission->name = $permission_model->name;
            $permissions[] = $permission;
        }

        $this->data['permissions'] = collect($permissions);
        $this->breadcrumb[] = (object) ["link" => route('roles.index', [$this->current_account->id]), "name" => 'Liste des rôles', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('roles.edit', [$this->current_account->id, $role->id]), "name" => "Edition d'un rôle ", "active" => true];
        return $this->render();
    }

    /**
     * Mise à jour d'un rôle
     *
     * @param UpdateRole $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateRole $request)
    {
        Debugbar::startMeasure('Controller', 'RoleController@update');
        $this->init();
        $this->hasPermissionOrAbord("roles_all");
        $validated = $request->validated();

        $parameters = request()->route()->parameters;

        // je modifie le nom du role dans la base de données roles
        $new_name = $validated["name"];
        Role::where('id', $parameters['role_id'])
            ->where('account_id', $parameters['account_id'])
            ->update(['name' => $new_name]);

        //Je supprime toutes les permissions pour un role défini
        $role_permissions = RolePermission::where('role_id', $parameters['role_id'])
            ->get();
        foreach ($role_permissions as $role_permission) {
            $role_permission->delete();
        }

        //Je recupère les permissions choisit par l utilisateur pour ce role
        $validated = $request;
        if (isset($validated['permissions'])){
            $permissions_array = $validated['permissions'];

            // Je parcours la liste des permission une à une pour les ajouter à la table
            foreach ($permissions_array as $permission) {
                $role_permission = new RolePermission();
                $role_permission->permission_id = $permission;
                $role_permission->role_id = $parameters['role_id'];
                $role_permission->save();
            }
        }

        Debugbar::stopMeasure('Controller');
        return redirect(action('RoleController@index', [$this->current_account->id]));
        //return $this->render();
    }

    /**
     * Affichage d'un rôle
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'MemberController@show');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("roles_all");
        $parameters = request()->route()->parameters;

        //je recupere le role dont on veut afficher les caracteristiques en fonction du role_id et de l account_id
        $role = Role::where('account_id', $parameters['account_id'])
            ->where('id', $parameters['role_id'])
            ->first();

        // j utilise cette methode pour ne recuperer que le nom de chaque permission autorisé par ce role
        $role_permissions = RoleRepository::getRoleAndPermissionByRoleId($parameters['role_id']);

        //je stocke le nom du role et ses permissions dans $this->data
        $this->data["role_permissions"] = $role_permissions;
        $this->data["role"] = $role;
        $this->breadcrumb[] = (object) ["link" => route('roles.index', [$this->current_account->id]), "name" => 'Liste des rôles', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('roles.show', [$this->current_account->id, $role->id]), "name" => "Affichage d'un rôle", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    public function archivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("roles_all");
        $parameters = request()->route()->parameters;
        $role = Role::where('account_id', $parameters['account_id'])
            ->where('id', $parameters['role_id'])
            ->first();

        if ($role == null) {
            abort(404, 'Role not found');
        }
        $role->active = false;
        $role->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Role " . $role->name . " archivé avec succès.");

        return redirect(action('RoleController@index', [$this->current_account->id]));
    }

    public function unarchivate()
    {
        $this->init();
        $this->hasPermissionOrAbord("roles_all");
        $parameters = request()->route()->parameters;
        $role = Role::where('account_id', $parameters['account_id'])
            ->where('id', $parameters['role_id'])
            ->first();

        if ($role == null) {
            abort(404, 'Role not found');
        }
        $role->active = true;
        $role->save();
        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Role " . $role->name . " déarchivé avec succès.");

        return redirect(action('RoleController@index', [$this->current_account->id]));
    }

    public function delete()
    {
        $this->init();
        $this->hasPermissionOrAbord("roles_all");
        $parameters = request()->route()->parameters;
        $role = Role::where('account_id', $parameters['account_id'])
            ->where('id', $parameters['role_id'])
            ->where('active', false)
            ->first();

        if ($role == null) {
            abort(404, 'Role not found');
        }
        if ($role->members->count() == 0) {
            RoleRepository::removeAndRemovePermissions($role);

            \Session::flash('msg_type', 'success');
            \Session::flash('message', "Role " . $role->name . " supprimer avec succès.");
        } else {
            \Session::flash('msg_type', 'warning');
            \Session::flash('message', "Le rôle " . $role->name . " ne peut pas être supprimé car a moins un membre lui est assigné");
        }

        return redirect(action('RoleController@index', [$this->current_account->id]));
    }
}
