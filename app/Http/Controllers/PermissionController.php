<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePermission;
use App\Models\Permission;
use App\Models\User;
use App\Models\Member;
use App\Models\Account;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('super_admin');
    }

    /**
     * Liste des permissions
     *
     * @return void
     */
    public function index()
    {
        $this->init();

        $this->data['url_data_table'] = action('PermissionController@indexDatatable');
        return $this->render();
    }

    /**
     * Renvoie les permissions de la liste
     *
     * @return void
     */
    public function indexDatatable()
    {
        $this->init();
        $permissions = Permission::All();
        $data = [];
        $data["data"] = $permissions;
        return $data;
    }

    /**
     * Renvoie les utilisateurs de la liste
     *
     * @return void
     */
    public function userDatatable()
    {
        $this->middleware('super_admin');
        $this->init();
        $users = User::All();

        $users_array = [];
        foreach ($users as $key => $user) {
            // $user->links = ['edit' => action('PermissionController@memberDatatable', [$user->id])];
            $user->links = 'TO CHANGE';

            $userId = $user->id;
            $result = Account::whereHas('members', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })->get();
            $user->accounts = $result;

            $users_array[$key] = $user;
        }

        $data = [];
        $data["data"] = $users_array;
        return $data;
    }


    /**
     * Page de création d'une permission
     *
     * @return void
     */
    public function create()
    {
        $this->middleware('super_admin');
        $this->init();
        return $this->render();
    }

    /**
     * Page de gestion des utilisateurs
     *
     * @return void
     */
    public function manage()
    {
        $this->middleware('super_admin');
        $this->init();
        $this->data['url_data_table'] = action('PermissionController@userDatatable');
        return $this->render();
    }

    /**
     * Enregistrement d'une permission
     *
     * @param StorePermission $request
     * @return void
     */
    public function store(StorePermission $request)
    {
        $this->middleware('super_admin');
        $this->init();

        $validated = $request->validated();

        $permission = new Permission;

        $permission->name = $validated['name'];
        $permission->slug = $validated['slug'];

        $permission->save();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', "Permission " . $permission->name . " enregistré avec succès.");

        return redirect(action('PermissionController@index'));
    }

    public function edit_accounts(){
        $this->middleware('super_admin');
        $this->init();
        $parameters = request()->route()->parameters;

        $user = User::find($parameters['id']);

        $member_accounts = Member::where('user_id',$parameters['id'])
            ->get();

        $accountsMember = [];
        foreach($member_accounts as $member_account){
            $accountsMember[] = $member_account->account_id;
        }

        $accounts = Account::All();

        $list_account = [];
        foreach($accounts as $account){
            $acc = (object) [];

            $acc->value = $account->id;
            $acc->name = $account->name;
            $list_account[] = $acc;
        }

        $this->data['user'] = $user;
        $this->data['accountsMember'] = $accountsMember;
        $this->data['accounts'] = collect($list_account);

        return $this->render();
    }



    public function storeAccountsUser($request)
    {
        $this->middleware('super_admin');
        $this->init();
        $parameters = request()->route()->parameters;
        $newaccounts = request()->input('accounts_id');

        if ($newaccounts !== null){
            //suppression des comptes de l utilisateur
            $deletemembers = Member::where('user_id', $parameters['id'])
                ->whereNotIn('account_id',$newaccounts)
                ->delete();

            //Je récupere les comptes associés actuels qui ne sont pas supprimés
            $actual_accounts = DB::table('members')
                ->where('user_id', $parameters['id'])
                ->select('account_id')
                ->get();
            $collection = collect($actual_accounts);

            //je les parcours si il y a un nouveau compte je l ajoute
            if ($newaccounts !== []){
                foreach($newaccounts as $newaccount){
                    if (!$collection->contains($newaccount)){
                        $new_member = new Member();
                        $new_member->user_id = $parameters['id'];
                        $new_member->account_id = $newaccount;
                        $new_member->role_id = 1; // todo voir avec thomas
                        $new_member->save();
                    }
                }
            }
        } else {
            $deletemembers = Member::where('user_id', $parameters['id'])
                ->delete();
        }

        return redirect(action('PermissionController@manage'));
    }
}
