<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Support\Facades\Request;

class BaseController extends Controller
{
    protected $error = false;
    protected $message = "";
    protected $properties = [];
    protected $geometry = [];
    protected $data = [];

    public function renderJson()
    {
        $result = ["data" => $this->data, "error" => $this->error, "message" => $this->message];
        return json_encode($result);
    }

    public function renderGeoJson($is_collection = false)
    {
        $result = [
            "type" => "Feature",
            "properties" => $this->properties,
            "geometry" => $this->geometry,
        ];
        return new JsonResponse($result);
    }

    protected function checkReferrer($item)
    {
        $ref = Request::server('HTTP_REFERER');
        $pass = false;
        //Always return true if no referrers are set
        if ($item->referrers === null) {
            return true;
        }
        //Always allow local requests
        if (strpos(Request::server('HTTP_REFERER'), Request::getHttpHost())) {
            return true;
        }

        $allowed = explode(',', $item->referrers);
        foreach ($allowed as $allowedRef) {
            if ($allowedRef) {
                $allowedRef = str_replace('*.', '', $allowedRef);
                if (strpos($ref, $allowedRef)) {
                    $pass = true;
                }
            }
        }
        return $pass;
    }
}
