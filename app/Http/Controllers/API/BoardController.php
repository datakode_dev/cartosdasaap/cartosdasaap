<?php

namespace App\Http\Controllers\API;

use App\Helpers\GeoHelper;
use App\Models\BoardLayer;
use App\Models\ServicePei;
use App\Models\Tb;
use geoPHP;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class BoardController extends BaseController
{
    /**
     * @param $id int Context  id
     */
    public function getBoard($id)
    {
        if (!$id) {
            abort(404);
        }

        $board = Tb::find((int) $id);

        $pass = $this->checkReferrer($board);

        if (!$board || !$pass) {
            abort(404);
        }

        $board->indicators = json_decode($board->indicators);

        $board->indicator_names = collect($board->indicators)->map(function ($indic) {
            $i = explode('.', $indic);
            if (isset($i[1]) && $id = filter_var($i[1], FILTER_VALIDATE_INT)) {
                //This is a service
                return ServicePei::find($id)->name;
            }
            return trans($indic);
        })->all();
        $schema = "account_" . $board->account_id;

        $boardLayers = BoardLayer::where('board_id', (int) $id)->get();

        $board->layers = $boardLayers->map(function ($bl) use ($schema) {
            $totalProps = DB::table($schema . '.board_territoire_props')
                ->join($schema . '.board_props', $schema . '.board_territoire_props.board_prop_id', $schema . '.board_props.id')
                ->where($schema . '.board_props.board_layer_id', $bl->id)
                ->orderBy('insee')
                ->get()
            ;

            $l = DB::table($schema . '.layers')
                ->select('layers.*')
                ->addSelect('service_peis.info as info')
                ->leftJoin('iso_context_move_step', 'iso_context_move_step.id', $schema . '.layers.context_step_move_id')
                ->leftJoin('iso_context_move_types', 'iso_context_move_types.id', 'iso_context_move_step.iso_context_move_type_id')
                ->leftJoin('iso_context_services', 'iso_context_services.id', 'iso_context_move_types.iso_context_service_id')
                ->leftJoin('service_peis', 'iso_context_services.service_pei_id', 'service_peis.id')
                ->where($schema . '.layers.id', $bl->layer_id)
                ->first();

            $query = DB::table($schema . '.board_props')
                ->join($schema . '.board_layer_clips', $schema . '.board_layer_clips.board_prop_id', $schema . '.board_props.id')
                ->join('board_layers', $schema . '.board_layer_clips.board_layer_id', 'board_layers.id')
                ->leftJoin('geo_data.communes', 'geo_data.communes.insee', '=', $schema . ".board_layer_clips.insee")
                ->leftJoin('geo_data.departements', 'geo_data.departements.insee', '=', $schema . ".board_layer_clips.insee")
                ->leftJoin('geo_data.epcis', 'geo_data.epcis.siren_epci', '=', $schema . ".board_layer_clips.insee")
                ->leftJoin('geo_data.zones', 'geo_data.zones.id', '=', $schema . ".board_layer_clips.zone_id")
                ->where($schema . '.board_props.board_layer_id', $bl->id)
                ->whereNotIn($schema . '.board_props.id', $totalProps->map(function ($p) {
                    return $p->id;
                }))
                ->orderBy('insee')
                ->select($schema . ".board_layer_clips.insee as insee")
                ->addSelect(DB::raw('COALESCE(geo_data.departements.name, geo_data.communes.name, geo_data.epcis.nom_complet) as name'))
                ->addSelect($schema . ".board_layer_clips.zone_id as zone_id")
                ->addSelect($schema . ".board_layer_clips.geom as geom")
                ->addSelect("geo_data.zones.name as zone_type")
                ->addSelect($schema . '.board_props.props as props')
                ->addSelect('board_layers.layer_id')
            ;

            if (isset($l->geom)) {
                $l->geom = json_decode(geoPHP::load($l->geom)->out('json'));
            }

            $l->props = $query->get()
                ->map(function ($p) {
                    return [
                        'props' => json_decode($p->props),
                        'insee' => $p->insee,
                        'geom' => json_decode(geoPHP::load($p->geom)->out('json')),
                        'name' => $p->name,
                    ];
                })
            ;
            $l->total_props = $totalProps->map(function ($p) {
                return [
                    'props' => json_decode($p->props),
                    'insee' => $p->insee,
                ];
            })
            ;
            return $l;
        });

        return $board;
    }

    public function getBoardServicePoints($id)
    {
        if (!$id) {
            abort(404);
        }

        $board = Tb::find((int) $id);

        if (!$board) {
            abort(404);
        }

        $schema = 'account_' . $board->account_id;

        $q = DB::table('board_layers')
            ->join($schema . '.layers', $schema . '.layers.id', 'board_layers.layer_id')
            ->join('iso_context_move_step', 'iso_context_move_step.id', $schema . '.layers.context_step_move_id')
            ->join('iso_context_move_types', 'iso_context_move_types.id', 'iso_context_move_step.iso_context_move_type_id')
            ->join('iso_context_services', 'iso_context_services.id', 'iso_context_move_types.iso_context_service_id')
            ->join('service_peis', 'service_peis.id', 'iso_context_services.service_pei_id')
            ->join('icos_colors', 'service_peis.ico_color_id', 'icos_colors.id')
            ->join('colors', 'colors.id', 'icos_colors.color_id')
            ->join('icos', 'icos.id', 'icos_colors.ico_id')
            ->join($schema . '.peis', $schema . '.peis.service_pei_id', 'iso_context_services.service_pei_id')

            ->selectRaw('ST_AsText(' . $schema . '.peis.geom) as geometry, ' . $schema . '.peis.prop as properties, icos.name as icon, colors.name as color, service_peis.display_fields as display_fields')

            ->where('board_layers.board_id', $board->id)
        ;

        $points = $q->get()->map(function ($point) {
            $point->geometry = json_decode(GeoHelper::wkt2json($point->geometry));
            $point->properties = json_decode($point->properties);
            return $point;
        });

        return new JsonResponse($points);
    }
}
