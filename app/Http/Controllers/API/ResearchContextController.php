<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 13/06/2019
 * Time: 23:34
 */

namespace App\Http\Controllers\API;

use App\Models\IgnKey;
use App\Models\ResearchContext;
use App\Models\ServicePei;
use App\Repositories\InseeRepository;
use App\Repositories\ServicePeiRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class ResearchContextController extends BaseController
{
    public function getContext($id)
    {
        $context = ResearchContext::find((int)$id);

        $pass = $this->checkReferrer($context);

        if (!$context || !$pass) {
            abort(404, "Ce contexte n'existe pas");
        }

        $context->services = $context->getServices();
        $key = IgnKey::find($context->ign_key_id);

        if ($key) {
            $context->key = $key->key;
        }

        return $context;
    }

    public function getServiceWithPointsForLimit(InseeRepository $inseeRepository, $serviceId, $searchContextId)
    {
        $context = ResearchContext::find((int)$searchContextId);
        $buffer = $context->dist_around_limit * 1000;

        if (!$context) {
            abort(404, 'Ce contexte de recherche n\'existe pas');
        }

        $zoneTable = $inseeRepository->getTableNameAndInseeFieldForZone($context->zone_id);

        $service = ServicePei::find((int)$serviceId);
        if (!$service) {
            abort(404, 'Ce service n\'existe pas');
        }
        $schema = 'account_' . $service->account_id;
        $service->iconColor = DB::table('icos_colors')
                                ->join('icos', 'icos.id', '=', 'icos_colors.ico_id')
                                ->join('colors', 'colors.id', '=', 'icos_colors.color_id')
                                ->where('icos_colors.id', $service->ico_color_id)
                                ->select('icos.name as icon', 'colors.name as color', 'icos.is_svg as icon_svg', 'icos.path as icon_path')
                                ->first()
        ;

        $limit = DB::table($zoneTable['table'])
            ->selectRaw('ST_AsText(ST_buffer(geom,'. $buffer.')) as geometry')
            ->where($zoneTable['field'], $context->insee_limit)
            ->first();

        $service->points = DB::table($schema . '.peis')
             ->selectRaw('id, prop, ST_AsGeoJSON(geom) as geom')
            ->whereRaw('service_pei_id=' . $service->id . ' AND ST_Contains(ST_GeomFromText(\'SRID=4326;' . $limit->geometry . '\'), geom)')
             ->get()
             ->map(function ($item) {
                 $item->prop = json_decode($item->prop);
                 $item->geom = json_decode($item->geom);
                 return $item;
             })
        ;
        $service->test = "ola";

        return $service;
    }

    /**
     * Exportation PDF 
     */

    public function downloadPdf(Request $request) {
        $data = request()->post();

        foreach ($data['services'] as $i=>$service) {
            if ($service['iconColor']['icon_svg'] === 'true') {
                $fullPath = public_path('/storage/' . $service['iconColor']['icon_path']);

                $data['services'][$i]['imgData'] = file_get_contents($fullPath);
            }
        }

        $pdf = PDF::loadView('pdf/pdf', ['name' => $data['context']['name'], 'services' => $data['services'], 'button' => isset($data['button']) ? $data['button'] : null]);

        return new Response($pdf->output(), 200, [
            'Content-Type' => 'application/pdf',
        ]);
    }
}
