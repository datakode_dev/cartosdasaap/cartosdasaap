<?php

namespace App\Http\Controllers\API;

use App\Helpers\ActivityTreeHelper;
use App\Helpers\GeoHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Zttp\Zttp;
use geoPHP;

class SireneController extends BaseController
{
    /**
     * Convert a sirene url and returns geoJSON
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // Get center of postcode with https://geo.api.gouv.fr/communes?codePostal=81240&fields=centre
        // TODO get limit geography
        $polygons = $this->getLimitAsArray($request->get('limit'), $request->get('buffer', 0));

        $points = [];

        foreach ($polygons as $polygon) {
            foreach ($polygon as $point) {
                // generate point, latitude goes first
                $points[] = '(' . join(',', array_reverse($point)) . ')';
            }
        }

        //Convert the polygon to the format expected by the opendatasoft API
        $points = join(',', $points);

        // Send to API : https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene&facet=rpet&facet=depet&facet=libcom&facet=epci&facet=siege&facet=libapet&facet=libtefet&facet=saisonat&facet=libnj&facet=libapen&facet=ess&facet=libtefen&facet=categorie&facet=proden&facet=libtu&facet=liborigine&facet=libtca&facet=libreg_new&facet=nom_dept&facet=section&geofilter.polygon=(lat1,lon1),(lat2,lon2),(lat3,lon3)

        $exactActivities = [];
        $activityTree = config('siren.activity_tree');
        $geo = collect();
        $nodes = $request->get('nodes');
        if (is_array($nodes)) {
            $approxActivities = [];

            foreach ($nodes as $node) {
                // If node is a letter, use all the top level below.
                if (!preg_match('/[^A-Za-z]/', $node)) {
                    $branch = ActivityTreeHelper::findBranch(config('siren.activity_tree'), $node);
                    foreach ($branch['children'] as $child) {
                        $approxActivities[] = $child['code'];
                    }
                    continue;
                }
                //Is a number
                $approxActivities[] = $node;
            }

            //Get all the exact actity NAF codes for the selected node
            foreach ($approxActivities as $activity) {
                $final = ActivityTreeHelper::getAllFinalChildren($activityTree, $activity);
                if(!is_array($final) && !is_null($final)) {
                    $final = [$final];
                }
                if (is_null($final)) {
                    $final = [];
                }
                $exactActivities = array_merge($exactActivities, $final);
            }

            $geo = $this->makeRequest($exactActivities, $points);
        }

        return new JsonResponse([
            "type" => "FeatureCollection",
            "features" => $geo,
        ]);
    }

    private function makeRequest($activities, $points)
    {
        $acts = [];
        foreach ($activities as $ac) {
            $acts[] = substr($ac, 0, 2).'.'.substr($ac, 2);
        }
        $activityQuery = join('&refine.activiteprincipaleunitelegale=', $acts);
        $businesses = new Collection();
        $rows = 100;
        $start = 0;

        $urlFormat = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=economicref-france-sirene-v3&facet=libellecommuneetablissement&geofilter.polygon=%s&refine.activiteprincipaleunitelegale=%s&disjunctive.activiteprincipaleunitelegale=true&rows=%d&start=%d&fields=adresseetablissement,codepostaletablissement,libellecommuneetablissement,denominationunitelegale,prenom1unitelegale,nomunitelegale,denominationusuelleetablissement,geolocetablissement";

        $url = sprintf($urlFormat, $points, $activityQuery, $rows, $start);

        $result = json_decode(@file_get_contents($url));

        if ($result && isset($result->records)) {
            $businesses = $businesses->concat($result->records);
        }

        $total = $result->nhits ?? 0;

        if ($total > 5000) {
            $total = 5000;
        }
        if ($result && $total > 100) {
            // Get all pages of results
            $rows = 100;
            for ($start = 100; $start <= $total; $start += $rows) {
                $page = json_decode(@file_get_contents(sprintf($urlFormat, $points, $activityQuery, $rows, $start)));

                if (isset($page->records)) {
                    // If we have something, add it to the businesses
                    $businesses = $businesses->concat($page->records);
                }
            }
        }

        return $businesses->map(function ($business) {
            $name = "";
            if (isset($business->fields->denominationunitelegale)) {
                $name = $business->fields->denominationunitelegale;
            } else if (isset($business->fields->nomunitelegale) && isset($business->fields->prenom1unitelegale)) {
                $name = $business->fields->nomunitelegale . ' ' . $business->fields->prenom1unitelegale;
            }
            return [
                "type" => "Feature",
                "properties" => [
                    "name" => $name,
                    "address" => (isset($business->fields->adresseetablissement) ? $business->fields->adresseetablissement : "") . ' ' . (isset($business->fields->codepostaletablissement) ? $business->fields->codepostaletablissement : "").' '. (isset($business->fields->libellecommuneetablissement) ? $business->fields->libellecommuneetablissement : ""),
                ],
                "geometry" => $business->geometry,
            ];
        });
    }

    public function getLimitAsArray($limit, $buffer = 0)
    {
        list($insee, $zone_id) = explode('.', $limit);

        $zone = DB::table('geo_data.zones')->where('id', (int) $zone_id)->first();
        switch ($zone->name) {
            case 'Commune':
                $table = 'geo_data.communes';
                $insee_field = 'insee';
                break;
            case 'Département':
                $table = 'geo_data.departements';
                $insee_field = 'insee';
                break;
            case 'EPCI':
                $table = 'geo_data.epcis';
                $insee_field = 'siren_epci';
                break;
            case 'Région':
                $table = 'geo_data.regions';
                $insee_field = 'insee';
                break;
        }
        if (!isset($table)) {
            return null;
        }
        $meter = (float) DB::select("select(ST_LENGTH(ST_GeomFromText('SRID=4326;LINESTRING(2.1240 42.45665099, 2.1240 42.45666)'))) as length")[0]->length;
        $size = DB::table($table)
            ->where($insee_field, $insee)
            ->selectRaw('ST_MaxDistance(geom::geometry, geom::geometry) AS maxlength')
            ->first();
        $simplifyLength = 0.001;

        if (isset($size->maxlength)) {
            $simplifyLength = (float) $size->maxlength / 100;
        }

        //ST_Buffer(ST_SimplifyPreserveTopology(ST_astext(geom), length), length * 100) as "geom"
        $builder = DB::table($table)
            ->where($insee_field, $insee)
            ->selectRaw('ST_AsText(ST_Buffer(ST_SimplifyPreserveTopology(ST_astext(geom), ?), ?)) as geom', [$simplifyLength, $meter * $buffer * 1000]);

        $limit = $builder->first();

//        $query = str_replace(array('?'), array('\'%s\''), $builder->toSql());
//        $query = vsprintf($query, $builder->getBindings());

        return geoPHP::load($limit->geom)->asArray();
    }
}
