<?php

namespace App\Http\Controllers\API;

use App\Helpers\GeoHelper;
use Illuminate\Support\Facades\DB;

class LayerController extends BaseController
{
    /**
     * @param $id int Context  id
     */
    public function collision(int $account_id, int $layer_id)
    {
        $feature = $_POST["feature"];

        $feature = json_encode(json_decode($feature));
        $wkt = GeoHelper::json2wkt($feature);

        $query = "SELECT ST_AsText(geom) as str_txt FROM account_" . $account_id . ".layers WHERE id = " . $layer_id;
        $layer = DB::connection()->select($query);
        if (empty($layer)) {
            abort(404);
        }
        $query = "SELECT ST_Intersects('" . $layer[0]->str_txt . "'::geometry, '" . $wkt . "'::geometry);";
        $result_check = DB::select(DB::raw($query))[0];
        return json_encode(["status" => true, "data" => ['check' => $result_check->st_intersects], "messages" => []]);
    }

}
