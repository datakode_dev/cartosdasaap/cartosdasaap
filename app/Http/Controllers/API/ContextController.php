<?php

namespace App\Http\Controllers\API;

use App\Helpers\GeoHelper;
use App\Models\IsochroneContext;
use App\Models\IsoContextMoveStep;
use App\Models\IsoContextMoveTypes;
use App\Models\IsoContextServices;
use App\Models\IsoType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class ContextController extends BaseController
{
    /**
     * @param $id int Context  id
     */
    public function getContext($id)
    {
        if (!$id) {
            abort(404);
        }

        $context = IsochroneContext::find((int) $id);

        $pass = $this->checkReferrer($context);

        if (!$context || !$pass) {
            abort(404);
        }
        $context->iso_type = IsoType::find($context->iso_type_id);
        $context->indicators = json_decode($context->indicators);
        $context->indicator_names = collect($context->indicators)->map(function ($indic) {
            return trans($indic);
        })->all();

        return $context;
    }
    /**
     * @param $id int Context  id
     */
    public function getContextServices($id)
    {
        if (!$id) {
            abort(404);
        }

        $ctx = IsochroneContext::find((int) $id);
        $schema = 'account_' . $ctx->account_id;
        // Assuming only ONE service_peis per service
        // TODO verify this is correct
        $services = IsoContextServices::where('iso_context_id', (int) $id)
            ->select('iso_context_services.*')
            ->addSelect('service_peis.name')
            ->addSelect('service_peis.info')
            ->addSelect('service_peis.display_fields')
            ->addSelect('icos.name as icon')
            ->addSelect('colors.name as color')
            ->join('service_peis', 'service_peis.id', '=', 'iso_context_services.service_pei_id')
            ->join('icos_colors', 'service_peis.ico_color_id', 'icos_colors.id')
            ->join('colors', 'colors.id', 'icos_colors.color_id')
            ->join('icos', 'icos.id', 'icos_colors.ico_id')
            ->get();

        $services = $services->map(function ($service) use ($schema) {
            $points = DB::table($schema . '.peis')->where('service_pei_id', $service->service_pei_id)->selectRaw('ST_AsText(geom) as geometry, prop as properties')->get();
            $service->points = $points->map(function($point) use ($service) {
                $point->geometry = json_decode(GeoHelper::wkt2json($point->geometry));
                $point->properties = json_decode($point->properties);
                $point->icon = $service->icon;
                $point->color = $service->color;
                $point->display_fields = $service->display_fields;
                return $point;
            });

            return $service;
        });

        if (!$services) {
            abort(404);
        }

        return $services;
    }

    /**
     * @param $contextId int context id
     * @param $id int Service id
     */
    public function getContextServiceMoveTypes($contextId, $id)
    {
        if (!$id) {
            abort(404);
        }

        $moveTypes = IsoContextMoveTypes::where('iso_context_service_id', (int) $id)
            ->join('move_types', 'move_types.id', '=', 'iso_context_move_types.move_type_id')
            ->select('iso_context_move_types.*')
            ->addSelect('move_types.name')
            ->get();

        if (!$moveTypes) {
            abort(404);
        }

        return $moveTypes;
    }

    /**
     * @param $contextId int context id
     * @param $serviceId int service id
     * @param $id int IsoContextMoveType id
     */
    public function getContextServiceMoveTypeSteps($contextId, $serviceId, $id)
    {
        if (!$id) {
            abort(404);
        }

        $moveSteps = IsoContextMoveStep::where('iso_context_move_type_id', (int) $id)->orderBy('value', 'asc')->get();

        if (!$moveSteps) {
            abort(404);
        }

        return $moveSteps;
    }
}
