<?php

namespace App\Http\Controllers\API;

use App\Helpers\GeoHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TerritoirePropsController extends BaseController
{
    protected $error = false;
    protected $message = "";
    protected $geometry = [];
    protected $data = [];

    public function renderJson()
    {
        //Je recupere l'identifiant pour recuperer les proprietes du territoire
        $layer_id = $layer[0]->layer_id;
        $query = "SELECT prop_id FROM account_" . $this->current_account->id . ".territoire_props WHERE layer_id='" . $layer_id . "' AND insee='" . $insee . "' AND zone_id='" .$zone_id . "'";
        $id_territory_props = DB::connection()->select($query);
        //je recupere les propriétés de ce territoire
        $query = "SELECT props FROM account_" . $this->current_account->id . ".props WHERE id='" . $id_territory_props[0]->prop_id . "'";
        $territory_props = DB::connection()->select($query);

        //je mets dans une variable les proprietes du layer et du territoire, cela va servir à remplir le graphique.
        $this->data['props'] = ([$layer_prop[0]->props, $territory_props[0]->props]);
        //dd($this->data['props']);

        //je recupere la geometrie de ce layer, on suppose qu il n'y en a qu'un seul pour l instant
        $query = "SELECT ST_AsText(geom) as geom FROM account_" . $this->current_account->id . ".layers WHERE id='" . $layer[0]->layer_id  . "'";
        $layers_geom = DB::connection()->select($query);
        $this->data["layers_geom"] = GeoHelper::wkt2json($layers_geom[0]->geom);
        //dd($this->data["layers_geom"]);



        $result = ["data" => $this->data, "error" => $this->error, "message" => $this->message];
        return json_encode($result);
    }

    public function renderGeoJson($is_collection = false)
    {
        $result = [
            "type" => "Feature",
            "properties" => [],
            "geometry" => $this->geometry,
        ];
        return json_encode($result);
    }
}
