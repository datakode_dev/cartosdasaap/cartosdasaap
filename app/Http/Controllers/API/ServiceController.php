<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 13/06/2019
 * Time: 23:34
 */

namespace App\Http\Controllers\API;

use App\Models\ServicePei;
use App\Repositories\ServicePeiRepository;
use Illuminate\Support\Facades\DB;

class ServiceController
{
    public function getServiceWithPoints($id)
    {
        $service = ServicePei::find((int)$id);
        if (!$service) {
            abort(404, 'Ce service n\'existe pas');
        }
        $schema = 'account_' . $service->account_id;
        $service->iconColor = DB::table('icos_colors')
        ->join('icos', 'icos.id', '=', 'icos_colors.ico_id')
        ->join('colors', 'colors.id', '=', 'icos_colors.color_id')
        ->where('icos_colors.id', $service->ico_color_id)
        ->select('icos.name as icon', 'colors.name as color')
        ->first()
        ;

        $service->points = DB::table($schema . '.peis')->where('service_pei_id', $service->id)
            ->select('prop')
            ->addSelect(DB::Raw('ST_AsGeoJSON(geom) as geom'))
            ->addSelect('id')
            ->get()
            ->map(function ($item) {
                $item->prop = json_decode($item->prop);
                $item->geom = json_decode($item->geom);
                return $item;
            })
            ;
        return $service;
    }
}
