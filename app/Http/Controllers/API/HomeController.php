<?php

namespace App\Http\Controllers\API;

use App\Helpers\GeoHelper;
use App\Models\Account;
use App\Models\BoardLayer;
use App\Models\ServicePei;
use App\Models\Tb;
use App\Models\IsoContextMoveStep;
use App\Models\IsoContextMoveTypes;
use App\Models\IsoContextServices;
use App\Models\Layer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use geoPHP;

class HomeController extends BaseController
{
    /**
     * @param $accountId int Account  id
     */
    public function getHomeIgnData($accountId)
    {
        if (!$accountId) {
            abort(404);
        }

        return new JsonResponse($this->getMonitors());
    }

    private function getMonitors()
    {
        //IGN uptimerobot account
        $utrAccount = '28xBxu6Q9';
        $utrMonitors = [
            '778877637' => 'map-marker',
            '778877639' => 'globe',
            '778877627' => 'male',
            '778877625' => 'car',
            '778877632' => 'male',
            '778877629' => 'car',
        ];
        $monitors = collect();
        foreach ($utrMonitors as $utrMonitor => $icon) {
            $monitorUrl = 'https://stats.uptimerobot.com/api/getMonitor/' . $utrAccount . '?m=' . $utrMonitor;

            $json = @file_get_contents($monitorUrl);

            if ($json) {
                $monitor = json_decode($json)->monitor;
                $monitor->icon = $icon;
                $monitors->push($monitor);
            }
        }

        return $monitors;
    }
}
