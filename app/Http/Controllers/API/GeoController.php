<?php

namespace App\Http\Controllers\API;

use App\Helpers\GeoHelper;
use App\Models\Account;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GeoController extends BaseController
{
    public function getLimit(Request $request)
    {
        $q = $request->input("q");

        $query = "
        SELECT
        CONCAT(insee,'.4') as id, name,'Region' As Type FROM geo_data.regions WHERE LOWER(name) LIKE LOWER('%" . $q . "%') OR insee LIKE LOWER('%" . $q . "%')
        UNION ALL
        SELECT
        CONCAT(insee,'.3') as id, name,'Departement' As Type FROM geo_data.departements WHERE LOWER(name) LIKE LOWER('%" . $q . "%') OR insee LIKE LOWER('%" . $q . "%')
        UNION ALL
        SELECT
        CONCAT(siren_epci,'.2') as id, nom_complet as name,'EPCI' As Type FROM geo_data.epcis WHERE (LOWER(nom_complet) LIKE LOWER('%" . $q . "%') OR siren_epci LIKE LOWER('%" . $q . "%')) AND geom IS NOT NULL
        UNION ALL
        SELECT
        CONCAT(insee,'.1') as id, name,'Commune' As Type FROM geo_data.communes WHERE LOWER(name) LIKE LOWER('%" . $q . "%') OR insee LIKE LOWER('%" . $q . "%')
        ";

        $result = DB::connection()->select($query);

        $this->data = $result;

        return $this->renderJson();
    }

    public function geoJson()
    {
        $parameters = request()->route()->parameters;
        switch ($parameters["zone_id"]) {
            case 1:
                $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee LIKE '" . $parameters["insee"] . "'";
                break;

            case 2:
                $query = "SELECT ST_AsText(geom) FROM geo_data.epcis WHERE siren_epci LIKE '" . $parameters["insee"] . "'";
                break;

            case 3:
                $query = "SELECT ST_AsText(geom) FROM geo_data.departements WHERE insee LIKE '" . $parameters["insee"] . "'";
                break;

            case 4:
                $query = "SELECT ST_AsText(geom) FROM geo_data.regions WHERE insee LIKE '" . $parameters["insee"] . "'";
                break;

            default:
                # code...
                break;
        }
        $result = DB::connection()->select($query);

        if (isset($result[0])) {
            $this->geometry = json_decode(GeoHelper::wkt2json($result[0]->st_astext));
        }
        return $this->renderGeoJson();
    }


    public function getBoardLayerClipFromLayerId(Request $request, $accountId, $id)
    {
        if (!$accountId) {
            abort(404);
        }

        $insee = $request->input("insee");
        $account = Account::find((int)$accountId);

        if (!$account) {
            abort(404);
        }
        $schema = "account_" . $account->id;

        $clips = DB::table($schema . ".board_layer_clips")
            ->join("board_layers", "board_layers.id", '=', $schema . ".board_layer_clips.board_layer_id")
            ->join($schema . ".board_props", $schema . ".board_props.id", '=', $schema . ".board_layer_clips.board_prop_id")
            ->where("board_layers.layer_id", (int)$id);

        if ($insee) {
            $clips->where('insee', $insee);
        }

        $clips->select($schema . ".board_layer_clips.geom as geom")
            ->addSelect($schema . ".board_layer_clips.insee as insee")
            ->addSelect($schema . ".board_props.props as props")
            ->addSelect("board_layers.layer_id as layer_id")
            ->orderBy("board_layers.layer_id", "asc");

        $res = $clips->get();

        return $res->map(function ($item) use ($schema, $id, $insee) {
            if ($item->geom) {
                $item->geom = \geoPHP::load($item->geom)->out('json');
            }
            $tp = DB::table($schema . ".board_territoire_props")
                ->join("board_layers", "board_layers.id", '=', $schema . ".board_territoire_props.board_layer_id")
                ->join($schema . ".board_props", $schema . ".board_props.id", '=', $schema . ".board_territoire_props.board_prop_id")
                ->where("board_layers.layer_id", (int)$id)
                ->select($schema . ".board_props.props as props")
            ;
            if ($insee) {
                $tp->where('insee', $insee);
            }
            $tp->orderBy("board_layers.layer_id", "asc");
            $item->territoire_props = $tp->first() ? $tp->first()->props : "{}";
            return $item;
        });
    }

    private function _geometryCollection2MultiPolygon($json)
    {
        if (is_string($json)) {
            $json = json_decode($json);
        }
        $type = $json->type;

        if ("GeometryCollection" !== $type) {
            return json_encode($json);
        }
        $coordinates = [];
        foreach ($json->geometries as $item) {
            if ("Polygon"===$item->type) {
                $coordinates[]=$item->coordinates;
            }
        }
        $type="MultiPolygon";

        return json_encode(compact('type', 'coordinates'));
    }



    /**
     * @param $accountId int account id
     * @param $id int Context move step id
     */
    public function getLayerClipFromContextMoveStep(Request $request, $accountId, $id)
    {
        if (!$accountId) {
            abort(404);
        }

        $insee = $request->input("insee");
        $account = Account::find((int) $accountId);

        if (!$account) {
            abort(404);
        }

        $schema = "account_" . $account->id;

        $clips = DB::table($schema . ".layer_clips")
            ->join($schema . ".layers", $schema . ".layers.id", '=', $schema . ".layer_clips.layer_id")
            ->join($schema . ".props", $schema . ".props.id", '=', $schema . ".layer_clips.prop_id")
            ->where($schema . ".layers.context_step_move_id", (int) $id);
        if ($insee) {
            $clips->where('insee', $insee);
        }
        $clips->select($schema . ".layer_clips.geom as geom")
            ->addSelect($schema . ".layer_clips.insee as insee")
            ->addSelect($schema . ".props.props as props")
            ->addSelect($schema . ".layers.context_step_move_id as step_id")
            ->addSelect($schema . ".layers.name as layer_name")
            ->orderBy($schema . ".layers.id", "asc");

        $res = $clips->get();

        return $res->map(function ($item) use ($schema, $id, $insee) {
            if ($item->geom) {
                $item->geom = $this->_geometryCollection2MultiPolygon(
                    \geoPHP::load($item->geom)->out('json')
                );
            }

            $tp = DB::table($schema . ".territoire_props")
                ->join($schema . ".layers", $schema . ".layers.id", '=', $schema . ".territoire_props.layer_id")
                ->join($schema . ".props", $schema . ".props.id", '=', $schema . ".territoire_props.prop_id")
                ->where($schema . ".layers.context_step_move_id", (int) $id)
                ->select($schema . ".props.props as props")
                ;
            if ($insee) {
                $tp->where('insee', $insee);
            }
            $tp->orderBy($schema . ".layers.id", "asc");
            $item->territoire_props = $tp->first() ? $tp->first()->props : "{}";
            return $item;
        });
    }

    /**
     * @param Request $request
     * @param $accountId
     * @param int $id the context ID
     * @return array
     */
    public function getPossibleInseeForContext(Request $request, $accountId, $id)
    {
        $account = Account::find((int) $accountId);

        if (!$account) {
            abort(404);
        }
        $q = $request->input("q");
        $schema = "account_" . $account->id;
        $clips = DB::table($schema . ".layer_clips")
            ->join($schema . ".layers", $schema . ".layers.id", '=', $schema . ".layer_clips.layer_id")
            ->join("iso_context_move_step", $schema . ".layers.context_step_move_id", '=', "iso_context_move_step.id")
            ->join("iso_context_move_types", "iso_context_move_types.id", '=', "iso_context_move_step.iso_context_move_type_id")
            ->join("iso_context_services", "iso_context_services.id", '=', "iso_context_move_types.iso_context_service_id")
            ->leftJoin('geo_data.communes', 'geo_data.communes.insee', '=', $schema . ".layer_clips.insee")
            ->leftJoin('geo_data.departements', 'geo_data.departements.insee', '=', $schema . ".layer_clips.insee")
            ->leftJoin('geo_data.epcis', 'geo_data.epcis.siren_epci', '=', $schema . ".layer_clips.insee")
            ->leftJoin('geo_data.zones', 'geo_data.zones.id', '=', $schema . ".layer_clips.zone_id")

            ->where("iso_context_services.iso_context_id", (int) $id);
        if ($q) {
            $clips->where(function ($query) use ($q, $schema) {
                $query->where($schema . ".layer_clips.insee", 'LIKE', '%' . $q . '%')
                    ->orWhere('geo_data.departements.name', 'ILIKE', '%' . $q . '%')
                    ->orWhere('geo_data.epcis.nom_complet', 'ILIKE', '%' . $q . '%')
                    ->orWhere('geo_data.communes.name', 'ILIKE', '%' . $q . '%');
            });
        }
        $clips->select($schema . ".layer_clips.insee as insee")
            ->addSelect(DB::raw('COALESCE(geo_data.departements.name, geo_data.communes.name, geo_data.epcis.nom_complet) as name'))
            ->addSelect($schema . ".layer_clips.zone_id as zone_id")
            ->addSelect("geo_data.zones.name as zone_type");

        $res = $clips->distinct()->get();
        return [
            'data' => $res->map(function ($r) {
                return [
                    'id' => $r->insee . '.' . $r->zone_id,
                    'name' => $r->name,
                    'type' => $r->zone_type,
                ];
            }),
        ];
    }

    /**
     * @param Request $request
     * @param $accountId
     * @param int $id the board ID
     * @return array
     */
    public function getPossibleInseeForBoard(Request $request, $accountId, $id)
    {
        $account = Account::find((int) $accountId);

        if (!$account) {
            abort(404);
        }

        $q = $request->input("q");
        $schema = "account_" . $account->id;

        $clips = DB::table($schema . ".board_layer_clips")
            ->join("board_layers", "board_layers.id", '=', $schema . ".board_layer_clips.board_layer_id")
           ->join($schema . ".layers", $schema . ".layers.id", '=', "board_layers.layer_id")
           ->leftJoin('geo_data.communes', 'geo_data.communes.insee', '=', $schema . ".board_layer_clips.insee")
           ->leftJoin('geo_data.departements', 'geo_data.departements.insee', '=', $schema . ".board_layer_clips.insee")
           ->leftJoin('geo_data.epcis', 'geo_data.epcis.siren_epci', '=', $schema . ".board_layer_clips.insee")
           ->leftJoin('geo_data.zones', 'geo_data.zones.id', '=', $schema . ".board_layer_clips.zone_id")

           ->where("board_layers.board_id", (int)$id);
        if ($q) {
            $clips->where(function ($query) use ($q, $schema) {
                $query->where($schema . ".board_layer_clips.insee", 'ILIKE', '%' . $q . '%')
                      ->orWhere('geo_data.departements.name', 'ILIKE', '%' . $q . '%')
                      ->orWhere('geo_data.epcis.nom_complet', 'ILIKE', '%' . $q . '%')
                      ->orWhere('geo_data.communes.name', 'ILIKE', '%' . $q . '%')
                ;
            });
        }
        $clips->select($schema . ".board_layer_clips.insee as insee")
              ->addSelect(DB::raw('COALESCE(geo_data.departements.name, geo_data.communes.name, geo_data.epcis.nom_complet) as name'))
              ->addSelect($schema . ".board_layer_clips.zone_id as zone_id")
              ->addSelect("geo_data.zones.name as zone_type")

        ;

        $res = $clips->distinct('insee')->get();
        return [
            'data' => $res->map(function ($r) {
                return [
                    'id' => $r->insee . '.' . $r->zone_id,
                    'name' => $r->name,
                    'type' => $r->zone_type,
                ];
            }),
        ];
    }

    public function getLimitName($limit)
    {
        list($insee, $zone_id) = explode('.', $limit);
        $zone = DB::table('geo_data.zones')->where('id', (int) $zone_id)->first();
        switch ($zone->name) {
            case 'Commune':
                $table = 'geo_data.communes';
                $insee_field = 'insee';
                $name_field = 'name';
                break;
            case 'Département':
                $table = 'geo_data.departements';
                $insee_field = 'insee';
                $name_field = 'name';
                break;
            case 'EPCI':
                $table = 'geo_data.epcis';
                $insee_field = 'siren_epci';
                $name_field = 'nom_complet';
                break;
            case 'Région':
                $table = 'geo_data.regions';
                $insee_field = 'insee';
                $name_field = 'name';
                break;
        }
        if (!isset($table)) {
            return null;
        }
        $name = DB::table($table)
            ->where($insee_field, $insee)
            ->select($name_field . ' as name')
            ->first();
        return new JsonResponse($name->name);
    }
}
