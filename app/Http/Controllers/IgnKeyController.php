<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIgnKey;
use App\Http\Requests\UpdateIgnKey;
use App\Interfaces\Controllers\IgnKeyInterface;
use App\Models\IgnKey;
use App\Models\Key_type;
use App\Models\ResearchContext;
use App\Repositories\IgnKeyRepository;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Support\Facades\Session;

class IgnKeyController extends Controller implements IgnKeyInterface
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('account_member');
    }
    /**
     * Liste des clés IGN
     *
     * @return void
     */
    public function index()
    {
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $this->breadcrumb[] = (object) ["link" => route('ign_keys.index', [$this->current_account->id]), "name" => 'Liste des clés', "active" => true];
        $this->data['url_data_table'] = action('IgnKeyController@indexDatatable', [$this->current_account->id]);
        //dd($this->data['url_data_table']);
        return $this->render();
    }
    /**
     * Renvoie les clés de la liste
     *
     * @return array
     */
    public function indexDatatable()
    {
        $this->init();
        $can_edit = $this->hasPermission("can_edit");

        $ign_keys = IgnKeyRepository::getIgnKeysByAccountId($this->current_account->id);
        foreach ($ign_keys as $key => $ign_key) {
            if ($can_edit === true) {
                $ign_key->links = [
                    'edit' => action('IgnKeyController@edit', [$this->current_account->id, $ign_key->id]),
                    'show' => action('IgnKeyController@show', [$this->current_account->id, $ign_key->id]),
                    'delete' => action('IgnKeyController@delete', [$this->current_account->id, $ign_key->id]),
                ];
            } else {
                $ign_key->links = [
                    'show' => action('IgnKeyController@show', [$this->current_account->id, $ign_key->id]),
                ];
            }
            $ign_keys[$key] = $ign_key;
        }

        $data = [];
        $data["data"] = $ign_keys;
        return $data;
    }

    /**
     * Page de création d'une clé IGN
     *
     * @return void
     */
    public function create()
    {
        Debugbar::startMeasure('Controller', 'IgnKeyController@create');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");

        $key_types = Key_type::All();
        $this->data['key_types'] = $key_types;

        $this->breadcrumb[] = (object) ["link" => route('ign_keys.index', [$this->current_account->id]), "name" => 'Liste des clés', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('ign_keys.index', [$this->current_account->id]), "name" => "Ajout d'une clé", "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Enregistrement d'une clé IGN
     *
     * @param StoreIgnKey $request
     * @return void
     */
    public function store(StoreIgnKey $request)
    {
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;
        $validated = $request->validated();

        // J'enregistre la cle ign dans la table ign_keys
        $ign_key = new IgnKey();
        $ign_key->name = $validated['name'];
        $ign_key->key_types_id = $validated['key_type'];
        $ign_key->key = $validated['key'];
        $ign_key->login = $validated['ign_login'];
        $ign_key->password = $validated['ign_password'];
        $ign_key->max_count = $validated['max_count'];
        $ign_key->start_date = Carbon::now();
        $ign_key->end_date = $validated['end_date'];
        $ign_key->account_id = $parameters['account_id'];
        $ign_key->save();

        return redirect(action('IgnKeyController@index', [$this->current_account->id]));
    }

    /**
     * Cette methode va permettre d'editer les différentes caracteristiques d'une clé
     *
     * @return void
     */
    public function edit()
    {
        Debugbar::startMeasure('Controller', 'IgnKeyController@edit');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        //Récuperation de totutes les clés
        $ign_key = IgnKeyRepository::getIgnKeysById($parameters['ign_keys_id']);
        $this->data["ign_key"] = $ign_key;

        //Récuperation des types de clé
        $key_types = Key_type::All();
        $this->data['key_types'] = $key_types;

        $this->breadcrumb[] = (object) ["link" => route('ign_keys.index', [$this->current_account->id]), "name" => 'Liste des clés', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('ign_keys.index', [$this->current_account->id]), "name" => "Edition d'une clé", "active" => true];

        DebugBar::stopMeasure('Controller');
        return $this->render();
    }

    /**
     * Methode qui va mettre à jour les caracteristiques pour cette clé
     *
     * @param UpdateIgnKey $request
     * @return void
     */
    public function update(UpdateIgnKey $request)
    {
        Debugbar::startMeasure('Controller', 'IgnKeyController@update');
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $validated = $request->validated();
        $parameters = request()->route()->parameters;

        //je met à jour la cle ign
        IgnKey::where('id', $parameters['ign_keys_id'])
            ->where('active', true)
            ->where('account_id', $this->current_account->id)
            ->update(['name' => $validated['name'],
                'key' => $validated['key'],
                'login' => $validated['ign_login'],
                'max_count' => $validated['max_count'],
                'end_date' => $validated['end_date'],
                'password' => $validated['ign_password'],
                'key_types_id' => $validated['key_type']]);

        Debugbar::stopMeasure('Controller');
        return redirect(action('IgnKeyController@index', [$this->current_account->id]));
    }

    /**
     * Cette methode va afficher un membre avec son nom, prénom, mail et son rôle
     *
     * @return void
     */
    public function show()
    {
        Debugbar::startMeasure('Controller', 'IgnKeyController@show');
        $this->init();
        $this->data['can_edit'] = $this->hasPermission("can_edit");
        $parameters = request()->route()->parameters;

        $ign_key = IgnKeyRepository::getIgnKeysById($parameters['ign_keys_id']);
        $this->data["ign_key"] = $ign_key;

        $this->breadcrumb[] = (object) ["link" => route('ign_keys.index', [$this->current_account->id]), "name" => 'Liste des clés', "active" => false];
        $this->breadcrumb[] = (object) ["link" => route('ign_keys.show', [$this->current_account->id, $ign_key->id]), "name" => "Clé " . $ign_key->name, "active" => true];

        Debugbar::stopMeasure('Controller');
        return $this->render();
    }

    public function delete(){
        $this->init();
        $this->hasPermissionOrAbord("can_edit");
        $parameters = request()->route()->parameters;

        $ign_key_use = ResearchContext::where('ign_key_id',$parameters['ign_keys_id'] )
            ->where('account_id', $parameters['account_id'])
            ->first();

        //si la clé est associée à un contexte de recherche, j'annule la suppression avec un message d'erreur
        if (isset($ign_key_use)){
            \Session::flash('msg_type', 'error');
            \Session::flash('message', 'Cette clé ne peut pas être supprimée, elle est associée à un contexte de recherche.');
            return redirect(action('IgnKeyController@index', [$this->current_account->id]));
        };

        IgnKey::where('id', $parameters['ign_keys_id'])
            ->where('account_id', $parameters['account_id'])
            ->delete();

        \Session::flash('msg_type', 'success');
        \Session::flash('message', 'La clé a été supprimée avec succès.');

        Debugbar::stopMeasure('Controller');
        return redirect(action('IgnKeyController@index', [$this->current_account->id]));
    }

    public function archivate($request)
    {
    }

    public function unarchivate($request)
    {
    }
}
