<?php

namespace App\Http\Requests;

use App\Rules\RoleNameUniqueAccount;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string', 'max:255', 'required'],
            'permissions' => 'array',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nom',
            'permissions' => 'Autorisations'
        ];
    }
}
