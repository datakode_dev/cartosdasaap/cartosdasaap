<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIsochroneContext extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255|required',
            'iso_type' => 'int|required',
            'load_full' => 'boolean',
            'move_types' => 'array|required',
            'limit' => 'string|max:255|required',
            'services' => 'array|required',
            'indicators' => 'array|max:255|required',
            'steps' => 'array|max:255|required',
            'steps.0' => 'integer|required',
            'referrers' => 'array',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nom',
            'move_types' => 'Type de déplacement',
            'limit' => 'Limite',
            'services' => 'Services',
            'indicators' => 'Indicateurs',
        ];
    }
}
