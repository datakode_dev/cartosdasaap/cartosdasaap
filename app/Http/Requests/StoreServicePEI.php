<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreServicePEI extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255|required',
            'info' => 'string|max:255|nullable',
            'fileToUpload' => 'nullable',
            'source_url' => 'nullable',
            'day_between_refresh_url' => 'nullable|int',
            'day_between_refresh_siren' => 'nullable|int',
            'color' => 'nullable|string|max:7',
            'colorFill' => 'nullable|string|max:7',
            'colorOutline' => 'nullable|string|max:7',
            'ico' => 'nullable|string|max:255',
            'limit' => 'nullable',
            'buffer' => 'nullable|int',
            'activity' => 'nullable',
            'field-names' => 'nullable|array',
            'field-values' => 'nullable|array',
            'selection-values' => 'nullable|array',
            'infobulle-values' => 'nullable|array',
            'recherche-values' => 'nullable|array',
            'access' => 'required|string',
            'members_access' => 'nullable|array',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nom',
        ];
    }
}
