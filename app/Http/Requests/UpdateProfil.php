<?php

namespace App\Http\Requests;

use App\Rules\CheckNewPasswordAndNewPasswordConfirmation;
use App\Rules\MemberEmail;
use App\Rules\PasswordConfirmation;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Nullable;

class UpdateProfil extends FormRequest
{
    /**
     * Determine if the profil is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastName' => 'string|max:255|required',
            'firstName' => 'string|max:255|required',
            'user_email' => ['email', 'max:255', 'required'],
            'actualPassword' => ['string', 'min:6', 'required', new PasswordConfirmation],
            'newPassword' => ['string', 'min:6', 'nullable', new CheckNewPasswordAndNewPasswordConfirmation()],
            'newPasswordConfirmation' => ['string', 'min:6', 'nullable', new CheckNewPasswordAndNewPasswordConfirmation()],
        ];
    }

    public function attributes()
    {
        return [
            'lastName' => 'Prénom',
            'firstName' => 'Nom',
            'user_email' => 'Email',
            'actualPassword' => 'Définir un nouveau mot de passe',
        ];
    }
}
