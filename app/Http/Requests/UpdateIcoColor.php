<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateIcoColor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'marker_name' => 'required|string|max:255',
            'marker_color' => 'required|string|max:255',
            'marker_icon' => 'required|string|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'marker_name' => 'Nom',
            'marker_icon' => 'Icône',
        ];
    }
}
