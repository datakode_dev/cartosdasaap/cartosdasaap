<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTb extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'int|nullable',
            'name' => 'string|max:255|required',
            'limit' => 'string|max:255|required',
            'load_full' => 'boolean',
            'layers' => 'array|required',
            'indicators' => 'array|required',
            'referrers' => 'array',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nom',
            'limit' => 'Limite',
            'layers' => 'Couches',
            'indicators' => 'Indicateurs',
        ];
    }
}
