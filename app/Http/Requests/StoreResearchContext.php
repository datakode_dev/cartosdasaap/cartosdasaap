<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreResearchContext extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255|required',
            'limit' => 'string|max:255|required',
            'ign_key_id' => 'string|max:255|required',
            'services' => 'array|required',
            'referrers' => 'array',
            'dist_around_limit' => 'integer|required',
            'display_city' => 'boolean',
            'display_route' => 'boolean',
            'display_selection' => 'boolean',
            'display_legend' => 'boolean',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nom',
            'limit' => 'Limite',
            'services' => 'Services',
            'referrers' => trans('form.label.referrers'),
        ];
    }
}
