<?php

namespace App\Http\Requests;

use App\Rules\MemberEmail;
use Illuminate\Foundation\Http\FormRequest;

class StoreMember extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'string|max:255|required',
            'lastname' => 'string|max:255|required',
            'email' => ['email', 'max:255', 'required', new MemberEmail],
            'role_id' => 'int|required',
        ];
    }

    public function attributes()
    {
        return [
            'firstname' => 'Prénom',
            'lastname' => 'Nom',
            'email' => 'Email',
        ];
    }
}
