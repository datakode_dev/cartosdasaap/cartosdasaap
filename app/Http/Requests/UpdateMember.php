<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMember extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'string|max:255|required',
            'lastName' => 'string|max:255|required',
            'user_email' => ['email', 'max:255', 'required'],
            'role_id' => 'int|required',
        ];
    }

    public function attributes()
    {
        return [
            'firstName' => 'Prénom',
            'lastName' => 'Nom',
            'user_email' => 'Email',
        ];
    }
}
