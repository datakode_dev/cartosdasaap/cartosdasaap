<?php

namespace App\Http\Requests;

use App\Rules\IgnKeyLoginPass;
use App\Rules\IgnKeyLoginReferePass;
use App\Rules\IgnKeyPasswordPass;
use App\Rules\IgnKeyPasswordReferePass;
use Illuminate\Foundation\Http\FormRequest;

class UpdateIgnKey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|nullable',
            'key_type' => 'required|string',
            'key' => 'required|string|max:255|nullable',
            'ign_login' => [new IgnKeyLoginPass(), new IgnKeyLoginReferePass()],
            'ign_password' => [new IgnKeyPasswordPass(), new IgnKeyPasswordReferePass()],
            'max_count' => 'required|integer',
            'end_date' => 'required|date',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nom',
            'key' => 'Clé IGN',
            'max_count' => 'Nombre de Connexions autorisées',
            'end_date' => 'Date de fin',
        ];
    }
}
