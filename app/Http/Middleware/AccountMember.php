<?php

namespace App\Http\Middleware;

use App\Models\Account;
use Closure;
use Illuminate\Support\Facades\Auth;

class AccountMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($request->account_id) {
            $account = Account::where('id', $request->account_id)->first();
            $is_in_account = $account->userIsInAccount($user->id);
            if ($is_in_account) {
                return $next($request);
            }
            return redirect('home');
        }
    }
}
