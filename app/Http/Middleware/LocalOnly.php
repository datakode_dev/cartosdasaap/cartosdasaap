<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LocalOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->server('SERVER_ADDR') === null || $request->server('SERVER_ADDR') === $request->server('REMOTE_ADDR')) {
            return $next($request);
        }
        return abort(401);
    }
}
