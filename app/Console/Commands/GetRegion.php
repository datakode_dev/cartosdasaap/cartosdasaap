<?php

namespace App\Console\Commands;

use App\Models\GeoData\Departement;
use App\Models\GeoData\Region;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Zttp\Zttp;

class GetRegion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geoapi:get_region {insee}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get region from data gouv";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $insee = $this->argument('insee');

            $region = Region::where('insee', $insee)->first();
            if ($region) {
                $this->info("Region deja chargée");
                $this->getDepartementAndProperties($region);
                return;
            }

            $region = new Region();
            $response = Zttp::get('https://geo.api.gouv.fr/regions/' . $insee);
            $result = $response->json();
            if (empty($result)) {
                $this->info("Not define");
                return;
            }
            $region->name = $result['nom'];
            $region->insee = str_replace(' ', '', $result['code']);

            $region->save();

            $this->info("Region chargée");
            $this->getDepartementAndProperties($region);
        } catch (\Exception $e) {
            $this->error("An error occurred");
        }
    }
    /**
     * Get communes Wkt
     *
     * @param [string] $insee
     * @return string
     */
    public function getWktDepartement($insee)
    {
        $query = "SELECT ST_AsText(geom) FROM geo_data.departements WHERE insee = '" . $insee . "'";

        return DB::connection()->select($query)[0]->st_astext;
    }
    /**
     * Get All Communes in departement and build departement geom
     *
     * @param Region $region
     * @return void
     */
    public function getDepartementAndProperties(Region $region)
    {
        $insee = str_replace(' ', '', $region->insee);
        $response = Zttp::get('https://geo.api.gouv.fr/regions/' . $insee . '/departements');
        $result = $response->json();
        $dep_wkt = "";
        foreach ($result as $key => $departement_api) {
            $this->info($departement_api['nom']);
            $this->call('geoapi:get_departement', [
                'insee' => $departement_api['code'],
            ]);
            $departement = Departement::where('insee', $departement_api['code'])->first();

            if ($dep_wkt == "") {
                $dep_wkt = $this->getWktDepartement($departement_api['code']);
            } else {
                $co_wkt = $this->getWktDepartement($departement_api['code']);

                $query = "SELECT ST_AsText(ST_Union(ST_GeomFromText('" . $dep_wkt . "'),ST_GeomFromText('" . $co_wkt . "')) );";
                $dep_wkt = DB::connection()->select($query)[0]->st_astext;
            }
        }

        $query = "UPDATE geo_data.regions SET geom = ST_GeomFromText('" . $dep_wkt . "') WHERE id = " . $region->id;

        DB::connection()->select($query);

        $this->info("Region ok");
    }
}
