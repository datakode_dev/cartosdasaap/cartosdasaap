<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\ServicePei;


class CopyDisplayFieldsToSelectionColumnsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy:displayfields';

    /**vagrant ssh
     * The console command description.
     *
     * @var string
     */
    protected $description = 'copy display fields columns to selection columns ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $services = ServicePei::all();

        foreach ($services as $service) {
            if ($service->display_selection === null){
                $service->display_selection = $service->display_fields;
            }
            if ($service->display_recherche === null){
                $service->display_recherche = $service->display_fields;
            }
            $service->save();
        }

    }
}
