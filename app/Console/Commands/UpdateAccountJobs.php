<?php

namespace App\Console\Commands;

use App\Models\Account;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class UpdateAccountJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create or update supervisor jobs for accounts";

    const QUEUES = ['default', 'high'];
    const CONFIG = <<<EOT
[program:account-ACCOUNT-QUEUE-ENV]
process_name=%(program_name)s_%(process_num)02d
command=php DIR/artisan queue:work --timeout=0 --queue=QUEUE_ACCOUNT
autostart=true
autorestart=true
numprocs=1
user=USERNAME
redirect_stderr=true
stdout_logfile=DIR/storage/app/.supervisord/logs/QUEUE_ACCOUNT.log
EOT;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createDirectories();

        $accounts = Account::get();
        $new = [];

        foreach ($accounts as $account) {
            $this->line('');
            $this->info('Checking account ' . $account->id);

            foreach (self::QUEUES as $queue) {
                if (Storage::exists('.supervisord/jobs-account-' . env('APP_ENV') . '-' . $queue . '-' . $account->id . '.conf')) {
                    $this->info('Supervisord config file already exists');
                } else {
                    $this->createConfigFile($account->id, $queue);
                }
            }
        }

        exec('supervisorctl reread');
        exec('supervisorctl update all');

        //Start new jobs
        foreach ($new as $app) {
            exec('supervisorctl start ' . $app);
        }

        return true;
    }

    /**
     * Creates a config file for a specified account id and queue
     *
     * @param $accountId
     * @param $queue
     */
    private function createConfigFile($accountId, $queue)
    {
        $conf = str_replace([
            'DIR',
            'ACCOUNT',
            'QUEUE',
            'USERNAME',
            'ENV',
        ], [
            base_path(),
            $accountId,
            $queue,
            getenv('SUPERVISOR_USER'),
            env('APP_ENV'),
        ], self::CONFIG);
        $new[] = 'account-' . $accountId . '-' . $queue;
        Storage::put('.supervisord/jobs-account-' . env('APP_ENV') . '-' . $queue . '-' . $accountId . '.conf', $conf);
        $this->info('Created config file for account ' . $accountId);
    }

    /**
     * Creates the required directories if they do not exist
     */
    private function createDirectories()
    {
        if (!Storage::exists('.supervisord')) {
            $this->info('Creating .supervisord directory.');
            Storage::makeDirectory('.supervisord');
        }

        //Create log directories
        if (!Storage::exists('.supervisord/logs')) {
            $this->info('Creating .supervisord logs directory.');
            Storage::makeDirectory('.supervisord/logs');
        }
    }
}
