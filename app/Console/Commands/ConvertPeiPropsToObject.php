<?php

namespace App\Console\Commands;

use App\Models\Account;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ConvertPeiPropsToObject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:pei_prop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert pei prop from array to object';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now();
        $accounts = Account::all();

        foreach ($accounts as $account) {
            $this->info("Account " . $account->name);

            $query = 'SELECT id,prop FROM account_' . $account->id . '.peis';
            $peis = DB::connection()->select($query);
            $bar = $this->output->createProgressBar(count($peis));

            $bar->start();
            foreach ($peis as $pei) {
                $prop = json_decode($pei->prop);
                if (is_array($prop)) {
                    $prop = $prop[0];
                    $update = "UPDATE account_" . $account->id . ".peis SET prop = '" . json_encode($prop, JSON_HEX_APOS) . "' WHERE id = " . $pei->id;
                    DB::connection()->select($update);
                }
                $bar->advance();
            }
            $bar->finish();
            $this->info("");
        }
    }
}
