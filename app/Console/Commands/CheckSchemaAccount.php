<?php

namespace App\Console\Commands;

use App\Repositories\PostgresRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckSchemaAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_schema:account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Vérifier l'intégrité du schéma";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = "SELECT schema_name FROM information_schema.schemata WHERE schema_name = 'account_test'";

        $schemas = DB::connection()->select($query);
        if (empty($schemas)) {
            PostgresRepository::createAccountSchema('test');
        }

        $query = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE 'account_%'";
        $schemas = DB::connection()->select($query);

        $query = "SELECT * FROM pg_catalog.pg_tables WHERE schemaname LIKE 'account_test'";
        $tables_patern = DB::connection()->select($query);

        $table_names = [];
        $tables_cols_names = [];
        foreach ($tables_patern as $key => $table_patern) {
            $table_names[] = $table_patern->tablename;
            $tables_cols_names[$table_patern->tablename] = [];
            $query = "SELECT * FROM information_schema.columns WHERE table_schema = 'account_test' AND table_name = '" . $table_patern->tablename . "'";
            $cols_patern = DB::connection()->select($query);
            foreach ($cols_patern as $key_b => $col_patern) {
                $tables_cols_names[$table_patern->tablename][] = $col_patern->column_name;
            }
        }

        foreach ($schemas as $key => $schema) {
            if ($schema->schema_name !== 'account_test') {
                $query = "SELECT * FROM pg_catalog.pg_tables WHERE schemaname LIKE '" . $schema->schema_name . "'";
                $tables = DB::connection()->select($query);

                $this->info("Schema name : " . $schema->schema_name);
                $current_table_names = [];
                $current_tables_cols_names = [];
                foreach ($tables as $key_a => $table) {
                    $current_tables_cols_names[$table->tablename] = [];
                    $this->info("Table name : " . $table->tablename);
                    $current_table_names[] = $table->tablename;
                    $query = "SELECT * FROM information_schema.columns WHERE table_schema = '" . $table->schemaname . "' AND table_name = '" . $table->tablename . "'";
                    $cols = DB::connection()->select($query);

                    foreach ($cols as $key => $col) {
                        $current_tables_cols_names[$table->tablename][] = $col->column_name;
                    }
                }
                if (!empty(array_diff($table_names, $current_table_names))) {
                    $this->info("copy table");
                    foreach (array_diff($table_names, $current_table_names) as $key => $table) {
                        $func_name = 'create' . $this->camelize($table) . 'Table';
                        if ($this->confirm('Do you create table ' . $table . '?')) {
                            PostgresRepository::$func_name($schema->schema_name);
                        }
                    }
                }
                foreach ($tables_cols_names as $key_a => $table_col_names) {
                    if (isset($current_tables_cols_names[$key_a])) {
                        $diff_add = array_diff($tables_cols_names[$key_a], $current_tables_cols_names[$key_a]);
                        $diff_rm = array_diff($current_tables_cols_names[$key_a], $tables_cols_names[$key_a]);
                        if (!empty($diff_add)) {
                            foreach ($diff_add as $key_b => $col) {
                                $query = "SELECT * FROM information_schema.columns WHERE table_schema = 'account_test' AND table_name = '" . $key_a . "' AND column_name ='" . $col . "'";
                                $col_ori = DB::connection()->select($query);
                                if ($this->confirm('Do you add column ' . $col . ' to ' . $schema->schema_name . '.' . $key_a . '?')) {
                                    if ($col_ori[0]->column_name === 'geom') {
                                        DB::statement('ALTER TABLE ' . $schema->schema_name . '.' . $key_a . ' ADD "geom" geometry(Geometry,4326) NULL;');
                                    } else {
                                        $query = "ALTER TABLE " . $schema->schema_name . "." . $key_a . " ADD " . $col_ori[0]->column_name . " " . $col_ori[0]->data_type;
                                        DB::connection()->select($query);
                                    }
                                }
                            }
                        }
                        if (!empty($diff_rm)) {
                            foreach ($diff_rm as $key_b => $col) {
                                if ($this->confirm('Do you drop column ' . $col . ' to ' . $schema->schema_name . '.' . $key_a . '?')) {
                                    $query = "ALTER TABLE " . $schema->schema_name . "." . $key_a . " DROP COLUMN " . $col;
                                    DB::connection()->select($query);
                                }
                            }
                        }
                    }
                }
            }
        }

        $query = "DROP SCHEMA  account_test CASCADE";

        $schemas = DB::connection()->select($query);
    }

    public function camelize($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }
}
