<?php

namespace App\Console\Commands;

use App\Models\GeoData\Commune;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LoadEPCI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:epci';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $departements_db = DB::table('geo_data.departements')->select('name', 'insee')->get();
        $departements = [];
        foreach ($departements_db as $key => $departement_db) {
            $departements[] = $departement_db->name . " --- " . $departement_db->insee;
        }
        $departement_str = $this->anticipate('Quel département?', $departements);
        $insee_dep = explode(" --- ", $departement_str)[1];
        $this->getEPCIFromDepartementInsee($insee_dep);
    }
    public function getEPCIFromDepartementInsee($insee)
    {
        $epcis = DB::table('geo_data.epcis')->select('nom_complet', 'siren_epci')->where('dep_epci', $insee)->get();

        $bar = $this->output->createProgressBar(count($epcis));

        $bar->start();

        foreach ($epcis as $epci) {
            $bar->advance();

            $query = "SELECT insee_commune as insee FROM geo_data.epci_communes  WHERE epci_communes.siren_epci = '" . $epci->siren_epci . "'";

            $result = DB::connection()->select($query);
            $wkt = "";
            foreach ($result as $key => $epci_commune) {
                $this->callSilent('geoapi:get_commune', [
                    'insee' => $epci_commune->insee,
                ]);
                $commune = Commune::where('insee', $epci_commune->insee)->first();

                if ($wkt == "") {
                    $wkt = $this->getWktCommune($epci_commune->insee);
                } else {
                    $co_wkt = $this->getWktCommune($epci_commune->insee);

                    $query = "SELECT ST_AsText(ST_Union(ST_GeomFromText('" . $wkt . "'),ST_GeomFromText('" . $co_wkt . "')) );";
                    $wkt = DB::connection()->select($query)[0]->st_astext;
                }
            }
            $query = "UPDATE geo_data.epcis SET geom = ST_GeomFromText('" . $wkt . "') WHERE siren_epci = '" . $epci->siren_epci . "'";

            DB::connection()->select($query);
        }

        $bar->finish();
        $this->info("");
        $this->info("EPCI ok");
    }
    /**
     * Get communes Wkt
     *
     * @param [string] $insee
     * @return string
     */
    public function getWktCommune($insee)
    {
        $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee = '" . $insee . "'";

        return DB::connection()->select($query)[0]->st_astext;
    }
}
