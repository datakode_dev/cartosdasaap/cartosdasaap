<?php

namespace App\Console\Commands;

use App\Jobs\ProcessServicePEI;
use App\Models\ServicePei;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckPei extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pei';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check service pei refresh';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now();
        $peis = ServicePei::where('updated_at', '<', $today)->where('day_between_refresh', '!=', null)->get();

        $peis = $peis->filter(function ($value, $key) use ($today) {
            return $value->updated_at->isSameDay($today->subDays($value->day_between_refresh)) || $value->updated_at->greaterThan($today->subDays($value->day_between_refresh));
        });
        foreach ($peis as $pei) {
            $pei->status = ServicePei::STATUS_NOT_STARTED;
            //$pei->updated_by = "Automatique";
            $pei->save();
            ProcessServicePEI::dispatch($pei)->onQueue('high_' . $pei->account_id);
        }
    }
}
