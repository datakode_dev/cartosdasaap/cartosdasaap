<?php

namespace App\Console\Commands;

use App\Helpers\GeoHelper;
use App\Models\GeoData\Commune;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Zttp\Zttp;

class getCommune extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geoapi:get_commune {insee}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get commune from data gouv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $insee = $this->argument('insee');

            $commune = Commune::where('insee', $insee)->first();
            if ($commune) {
                $this->info("Commune deja chargée");
                return;
            }

            $commune = new Commune();
            $response = Zttp::get('https://geo.api.gouv.fr/communes/' . $insee . '?geometry=contour&format=geojson');
            $result = $response->json();

            if (empty($result)) {
                $this->info("Not define");
                return;
            }
            $geometry = json_decode(json_encode($result["geometry"]));
            $properties = $result["properties"];

            $wkt = GeoHelper::json2wkt($geometry);

            $commune->name = $properties['nom'];
            $commune->insee = $properties['code'];
            $commune->insee_departement = $properties['codeDepartement'];

            $commune->save();

            $query = "UPDATE geo_data.communes SET geom = ST_GeomFromText('" . $wkt . "') WHERE id = " . $commune->id;

            DB::connection()->select($query);

            $this->info("Commune chargée");
        } catch (\Exception $e) {
            $this->error("An error occurred");
        }
    }
}
