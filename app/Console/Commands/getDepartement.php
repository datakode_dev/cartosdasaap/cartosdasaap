<?php

namespace App\Console\Commands;

use App\Models\GeoData\Commune;
use App\Models\GeoData\Departement;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Zttp\Zttp;

class getDepartement extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "geoapi:get_departement {insee}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get departement from data gouv";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $insee = $this->argument('insee');

            $departement = Departement::where('insee', $insee)->first();
            if ($departement) {
                $this->info("Departement deja chargé");
                $this->getCommunesAndProperties($departement);
                return;
            }

            $departement = new Departement();
            $response = Zttp::get('https://geo.api.gouv.fr/departements/' . $insee);
            $result = $response->json();
            if (empty($result)) {
                $this->info("Not define");
                return;
            }
            $departement->name = $result['nom'];
            $departement->insee = str_replace(' ', '', $result['code']);
            $departement->insee_region = str_replace(' ', '', $result['codeRegion']);

            $departement->save();

            $this->info("Departement chargé");
            $this->getCommunesAndProperties($departement);
        } catch (\Exception $e) {
            $this->error("An error occurred");
        }
    }
    /**
     * Get communes Wkt
     *
     * @param string $insee
     * @return string
     */
    public function getWktCommune($insee)
    {
        $query = "SELECT ST_AsText(geom) FROM geo_data.communes WHERE insee = '" . $insee . "'";

        return DB::connection()->select($query)[0]->st_astext;
    }
    /**
     * Get All Communes in departement and build departement geom
     *
     * @param Departement $departement
     * @return void
     */
    public function getCommunesAndProperties(Departement $departement)
    {
        $insee = str_replace(' ', '', $departement->insee);
        $response = Zttp::get('https://geo.api.gouv.fr/departements/' . $insee . '/communes');
        $result = $response->json();
        $dep_wkt = "";
        foreach ($result as $key => $commune_api) {
            $this->info($commune_api['nom']);
            $this->call('geoapi:get_commune', [
                'insee' => $commune_api['code'],
            ]);
            $commune = Commune::where('insee', $commune_api['code'])->first();

            if ($dep_wkt == "") {
                $dep_wkt = $this->getWktCommune($commune_api['code']);
            } else {
                $co_wkt = $this->getWktCommune($commune_api['code']);

                $query = "SELECT ST_AsText(ST_Union(ST_GeomFromText('" . $dep_wkt . "'),ST_GeomFromText('" . $co_wkt . "')) );";
                $dep_wkt = DB::connection()->select($query)[0]->st_astext;
            }
        }

        $query = "UPDATE geo_data.departements SET geom = ST_GeomFromText('" . $dep_wkt . "') WHERE id = " . $departement->id;

        DB::connection()->select($query);

        $this->info("Departement ok");
    }
}
