<?php

namespace App\Helpers;

use geoPHP;

class GeoHelper
{
    public static function json2wkt($json)
    {
        return geoPHP::load($json, 'json')->out('wkt');
    }
    public static function wkt2json($wkt)
    {
        return geoPHP::load($wkt, 'wkt')->out('json');
    }
}
