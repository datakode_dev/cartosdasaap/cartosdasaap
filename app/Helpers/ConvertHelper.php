<?php
namespace App\Helpers;

class ConvertHelper
{
    public static function csvToJson($fp)
    {
        $fp = str_replace("\xEF\xBB\xBF",'',$fp);
        $parsed = collect(explode("\n", $fp))->filter(function($l){
            return strlen($l) > 0;
        })->map(function($line) {
            return str_getcsv($line, ';', '"');
        });
        $header = $parsed->first();
        $data = $parsed->slice(1)->values();

        return $data->map(function ($d) use ($header) {
            $r = [];
            foreach($d as $i => $field) {
                $r[$header[$i]] = $field;
            }
            return $r;
        })->toArray();
    }
}
