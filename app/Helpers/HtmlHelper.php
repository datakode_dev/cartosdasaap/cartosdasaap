<?php
namespace App\Helpers;

/**
 * Fonction utile pour les views
 */
class HtmlHelper
{
    public static function getGravatarUrl($email, $size = 40)
    {
        return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($email))) . "?d=identicon&s=" . $size;
    }
    public static function formatDate($date)
    {
        if ($date == null) {
            return "";
        }
        return $date->format('d/m/Y');
    }

    public static function getUnitByIsoType($iso_type_id)
    {
        switch ($iso_type_id) {
            case 1:
                return 'min';
                break;
            case 2:
                return 'm';
                break;

            default:
                return '';
                break;
        }
    }
}
