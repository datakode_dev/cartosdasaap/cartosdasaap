<?php
namespace App\Helpers;

class ActivityTreeHelper
{
    /**
     * returns the top level nodes that contain all the nodes from the other tree
     * @param array $origin
     * @param array $tree
     * @return array
     */
    public static function getMainCommonNode($origin, $tree)
    {
        $eqs = self::getMainCommonNodeIndex($origin, $tree);

        return array_flatten($eqs);
    }

    /**
     * @param array $origin
     * @param array $tree
     * @return array
     */
    public static function getMainCommonNodeIndex($origin, $tree)
    {
        // Go around the origin tree, looking for corresponding nodes in the new tree
        // When something doesn't match, mark the branch as incomplete
        // The complete branches are the result
        $equals = [];
        foreach ($origin as $k => $branch) {
            if (!isset($branch['code'])) {
                continue;
            }
            $myBranch = self::findBranch($tree, $branch['code']);

            if ($myBranch === null) {
                continue;
            }

            if (self::compareBranches($branch, $myBranch)) {
                // Those two branches are equal, return the code
                $equals[$k] = $myBranch['code'];
            } else {
                // otherwise keep looking
                if (isset($myBranch['children'])) {
                    $equals[$k] = self::getMainCommonNodeIndex($branch['children'], $myBranch['children']);
                }
            }
        }

        return $equals;
    }

    public static function mapFromRequest($requestData)
    {
        $res = [];
        foreach ($requestData as $k => $data) {
            $res[] = self::requestMap($k, $data);
        }
        return $res;
    }

    private static function requestMap($index, $element)
    {
        if (is_array($element)) {
            $children = [];
            foreach ($element as $k => $e) {
                $children[] = self::requestMap($k, $e);
            }
            return [
                "code" => $index,
                "children" => $children,
            ];
        }
        return [
            "code" => $element,
        ];
    }

    private static function compareChildren($c1, $c2)
    {
        $same = true;

        if (count($c1) === count($c2)) {
            foreach ($c1 as $k => $c) {
                if (!self::compareBranches($c, $c2[$k])) {
                    $same = false;
                }
            }
        }

        return $same;
    }

    private static function compareBranches($a, $b)
    {
        $compare = self::findBranch($a, $b['code']);

        if ($compare) {
            // This branch exists in our tree, compare them
            if (isset($compare['children']) && isset($b['children'])
                && count($compare['children']) === count($b['children'])) {
                //same number of children, check level below
                return self::compareChildren($compare['children'], $b['children']);
            } elseif (isset($compare['children']) && isset($b['children'])
                && count($compare['children']) !== count($b['children'])) {
                // has children but not the same number
                return false;
            }
            //No children, leaf nodes
            return (string)$compare['code'] === (string)$b['code'];
        }
        return false;
    }


    public static function findBranch($tree, $code)
    {
        // Convert ALL CODES to strings because they are sometimes numbers (from the json)
        if (isset($tree['code']) && (string)$tree['code'] === (string)$code) {
            return $tree;
        }

        if (isset($tree['children'])) {
            return self::findBranch($tree['children'], $code);
        }
        $ret = null;
        foreach ($tree as $branch) {
            if (isset($branch['code']) && (string)$branch['code'] === (string)$code) {
                return $branch;
            } elseif (isset($branch['children'])) {
                $br = self::findBranch($branch['children'], (string)$code);

                if ($br) {
                    $ret = $br;
                }
            }
        }

        return $ret;
    }

    /**
     * @param $tree
     * @param $code
     * @return array
     */
    public static function getAllFinalChildren($tree, $code)
    {
        $branch = self::findBranch($tree, (string)$code);

        if (isset($branch['children'])) {
            $brs = [];
            foreach ($branch['children'] as $child) {
                $brs[] = self::getAllFinalChildren($branch['children'], $child['code']);
            }
            return array_flatten($brs);
        }

        return $branch['code'];
    }
}
