<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateMember;
use App\Http\Requests\StoreMember;
use App\Http\Requests\UnArchivateMember;
use App\Http\Requests\UpdateMember;

interface MemberInterface
{
    public function index();
    public function create();
    public function show();
    public function store(StoreMember $request);
    public function edit();
    public function update(UpdateMember $request);
    public function archivate($request);
    public function unarchivate($request);
}
