<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\UpdateProfil;

interface ProfilInterface
{
    public function show();
    public function edit();
    public function update(UpdateProfil $request);
}
