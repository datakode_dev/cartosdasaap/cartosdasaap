<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateAccount;
use App\Http\Requests\StoreAccount;
use App\Http\Requests\UnArchivateAccount;
use App\Http\Requests\UpdateAccount;

interface AccountInterface
{
    public function home();
    public function index();
    public function create();
    public function store(StoreAccount $request);
    public function edit();
    public function update(UpdateAccount $request);
    public function archivate(ArchivateAccount $request);
    public function unarchivate(UnArchivateAccount $request);
}
