<?php
namespace App\Interfaces\Controllers;

interface HomeInterface
{
    public function index();
}
