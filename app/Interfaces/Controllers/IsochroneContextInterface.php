<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateIsochroneContext;
use App\Http\Requests\StoreIsochroneContext;
use App\Http\Requests\UnArchivateIsochroneContext;
use App\Http\Requests\UpdateIsochroneContext;

interface IsochroneContextInterface
{
    public function index();
    public function create();
    public function show();
    public function store(StoreIsochroneContext $request);
    public function edit();
    public function update(UpdateIsochroneContext $request);
    public function updateCalcul();
    public function archivate(ArchivateIsochroneContext $request);
    public function unarchivate(UnArchivateIsochroneContext $request);
}
