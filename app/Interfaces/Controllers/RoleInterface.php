<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateRole;
use App\Http\Requests\StoreRole;
use App\Http\Requests\UnArchivateRole;
use App\Http\Requests\UpdateRole;

interface RoleInterface
{
    public function index();
    public function create();
    public function store(StoreRole $request);
    public function edit();
    public function show();
    public function update(UpdateRole $request);
    public function archivate(ArchivateRole $request);
    public function unarchivate(UnArchivateRole $request);
}
