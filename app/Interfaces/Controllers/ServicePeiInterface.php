<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateServicePei;
use App\Http\Requests\StoreServicePei;
use App\Http\Requests\UnArchivateServicePei;
use App\Http\Requests\UpdateServicePei;
use App\Http\Requests\UpdateServicePoint;

interface ServicePeiInterface
{
    public function index();
    public function create();
    public function store(StoreServicePei $request);
    public function edit();
    public function edit_peis();
    public function edit_point();
    public function show();
    public function update(UpdateServicePei $request);
    public function update_peis(UpdateServicePei $request);
    public function update_point(UpdateServicePoint $request);
    public function archivate(ArchivateServicePei $request);
    public function unarchivate(UnArchivateServicePei $request);
}
