<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateIcoColor;
use App\Http\Requests\StoreIcoColor;
use App\Http\Requests\UnArchivateIcoColor;
use App\Http\Requests\UpdateIcoColor;

interface IcoColorInterface
{
    public function index();
    public function create();
    public function show();
    public function store(StoreIcoColor $request);
    public function edit();
    public function delete();
    public function update(UpdateIcoColor $request);
    public function archivate($request);
    public function unarchivate($request);
}
