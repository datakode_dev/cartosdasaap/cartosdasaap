<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\StoreLayer;
use App\Http\Requests\UpdateLayer;

interface LayerInterface
{
    public function index();
    public function indexDatatable();
    public function create();
    public function store(StoreLayer $request);
    public function edit();
    public function update($accout_id, $id, UpdateLayer $request);
    public function archivate();
    public function unarchivate();
    public function delete();
}
