<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\StoreIgnKey;
use App\Http\Requests\UpdateIgnKey;
use Illuminate\Http\Request;

interface IgnKeyInterface
{
    public function index();
    public function create();
    public function store(StoreIgnKey $request);
    public function edit();
    public function delete();
    public function update(UpdateIgnKey $request);
    public function archivate($request);
    public function unarchivate($request);
}
