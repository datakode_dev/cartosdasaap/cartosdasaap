<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\StorePermission;
use App\Http\Requests\UpdatePermission;

interface PermissionInterface
{
    public function index();
    public function create();
    public function store(StorePermission $request);
    public function edit();
    public function update(UpdatePermission $request);
}
