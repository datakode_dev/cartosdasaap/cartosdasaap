<?php
namespace App\Interfaces\Controllers;

use App\Http\Requests\ArchivateSimulation;
use App\Http\Requests\StoreSimulation;
use App\Http\Requests\UnArchivateSimulation;
use App\Http\Requests\UpdateSimulation;

interface SimulationInterface
{
    public function index();
    public function show();
    public function create();
    public function store(StoreSimulation $request);
    public function edit();
    public function update(UpdateSimulation $request);
    public function archivate(ArchivateSimulation $request);
    public function unarchivate(UnArchivateSimulation $request);
}
