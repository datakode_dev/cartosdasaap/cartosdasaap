@extends('mails.layout')
@section('title')
{{$service_pei->name}} :  Des points n'ont pas pu être géocodés
@stop

@section('content')
<ul>
    @foreach ($errors as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>
@stop