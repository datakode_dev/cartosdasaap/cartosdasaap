@php
    if (!isset($value)) {
        $value = "";
    }
    if (!isset($step)) {
        $step = 1;
    }
@endphp
<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{$name}}">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    <div class="range-values">

        <div class="range-value min">{{$min}}km</div>
        <span class="current" {{ isset($disabled) ? 'style=display:none' : '' }}>{{ old($name) == null ? $value : old($name) }}km</span>
        <div class="range-value max">{{$max}}km</div>
    </div>
<input id="{{$name}}" type="range" min="{{ $min }}" max="{{ $max }}" name="{{$name}}" value="{{ old($name) == null ? $value : old($name) }}" class="custom-range" {{ isset($disabled) ? 'disabled' : '' }} placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}" step="{{ $step }}">
    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif
    

    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
