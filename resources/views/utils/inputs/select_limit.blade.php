<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>

    <select name="{{$name}}" class="form-control select2_ajax" style="width:100%" data-url="{{$url}}" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}">
        @if(isset($current))
        <option value="{{ $current['value'] }}" selected="selected">{{ $current['text'] }}</option>
        @endif
    </select>


    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif


    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
