<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}" id="{{ $name }}">
    <fieldset class="inputs">
        @include('utils.inputs.multiples.tree.tree', ["name"=>"activity", "tree" => $activity_tree])
        @include('utils.inputs.select_limit', ["name"=>"limit","url"=>action('API\GeoController@getLimit')])
        @include('utils.inputs.number', [ "name" => "buffer" ])
        <div class="alert alert-info" style="display:none" id="num-points-div">
            Le service contient <span id="num-points"></span> points.
        </div>
    </fieldset>

    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
