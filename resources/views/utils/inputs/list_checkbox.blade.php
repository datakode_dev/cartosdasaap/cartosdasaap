
@php

if (!isset($value)) {
    $value =collect([]);
} 
$value_format=collect([]);
foreach ($value as $key => $value) {
    $value_format[$value->permission_id]=$value->permission_id;
}
@endphp

<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label>{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    <ul class="list-group">
        @foreach ($collection as $item)
            
            <li class="list-group-item">
                <label>
                    <input name="{{$name."[".$item->value."]"}}" value="{{$item->value}}" type="checkbox" 
                    @if (old($name) == null)
                        @if (isset($value_format[$item->value]))
                        checked=""
                        @endif
                    @else
                        @if (isset(old($name)[$item->value]))
                        checked=""
                        @endif
                    @endif
                    > 
                    {{$item->name}}
                </label>
            </li>
        @endforeach
        
    </ul>
    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>