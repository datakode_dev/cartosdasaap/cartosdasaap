@php
    if (!isset($values)) {
        $values =[];
    }
@endphp
<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    @if (isset($info))
        </br>
            <small>{{$info}}</small>
        </br>
    @endif
    
    <select id="{{ $name }}" name="{{$name}}[]" data-tags="true" multiple class="form-control select2" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}">
        @foreach ($collection as $item)
            @if($item)
            <option
            @if (old($name) === $item || in_array( $item, $values) )
                selected="true"
            @endif
            value="{{$item}}"
            >
                {{$item}}
            </option>
            @endif
        @endforeach

    </select>


    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif


    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
