

<li title="{{ $tree['libelle'] }}"> <i class="fa fa-plus"></i>
    <label style="font-weight: normal">
        <?php
            $par = '';
            if (count($parents) > 0) {
                foreach ($parents as $code) {
                    $par .= '['.$code.']' ;
                }
            }
            $par .= '['.$tree['code'].']';

            ?>
        <input name="activity{{ $par }}" id="xnode-{{ $tree['code'] }}" data-id="custom-{{ $tree['code'] }}" value="{{ $tree['code'] }}" type="checkbox" />
        {{ $tree['libelle'] }}</label>
    @if(isset($tree['children']))
    <ul style="display:none">
        <?php
        $parents[] = $tree['code'];
        ?>
        @foreach($tree['children'] as $branch)
            @include('utils.inputs.multiples.tree.tree-item', ["tree" => $branch, 'parents' => $parents])
        @endforeach
    </ul>
    @endif
</li>

