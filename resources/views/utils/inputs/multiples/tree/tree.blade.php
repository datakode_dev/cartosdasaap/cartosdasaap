@php

@endphp
<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label>{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    <ul id="treeview" style="display:none">
        @foreach($tree as $branch)
            @include('utils.inputs.multiples.tree.tree-item', ["tree" => $branch, "parents" => []])
        @endforeach
    </ul>
</div>
