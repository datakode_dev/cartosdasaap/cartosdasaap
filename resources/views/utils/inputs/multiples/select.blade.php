@php
    if (!isset($values)) {
        $values =[];
    }
@endphp
<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>

    <select id="{{ $name }}" name="{{$name}}[]" multiple class="form-control select2" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}">
        @foreach ($collection as $item)
            <option
            @if (in_array( $item->value, $values) )
                selected='selected'
            @elseif (old($name) != null)
                @foreach (old($name) as $old_name)
                    @if ($old_name == $item->value)
                        selected='selected'
                    @endif
                @endforeach
            @endif
            @if (isset($item->title))
                title="{{$item->title}}"
            @endif
            value="{{$item->value}}"
            >
                {{$item->name}}
            </option>
        @endforeach
    </select>

    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif

    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
