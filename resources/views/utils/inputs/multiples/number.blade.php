@php
    if (!isset($values)) {
        $values =[];
    }
    if (old($name)!= null) {
        $values = old($name);
    }
@endphp
@foreach ($values as $key => $value)
@if ($value != null)
    <div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
        <label for="exampleInputEmail1">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>

    <input type="number" name="{{$name}}[]" value="{{ old($name)[$key] == null ? $value : old($name)[$key] }}" class="form-control" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}" >
        @if (isset($glyphicon))
            <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
        @endif


        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
@endif
@endforeach

<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>

<input type="number" name="{{$name}}[]"  class="form-control" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}" >
    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif


    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>