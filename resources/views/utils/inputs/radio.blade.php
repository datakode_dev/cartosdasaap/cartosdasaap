<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    <div class="testradio">
        @foreach ($collection as $item)
            @if (isset($value) and ( $value === $item->name))
                <input type="radio" name="{{$name}}" value="{{ $item->id }}" class="minimal" checked>{{ $item->name }}
            @else
                <input type="radio" name="{{$name}}" value="{{ $item->id }}" class="minimal">{{ $item->name }}
            @endif
        @endforeach
    </div>

</div>
