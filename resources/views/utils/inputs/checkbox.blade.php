<div class="has-feedback {{ $errors->has($name) ? 'has-error' : '' }}"> <!-- form-group  -->
    <label>
        <input name="{{$name}}" type="checkbox" value="1"
        @if (old($name) == 1)
            checked=""
        @else
            @if (isset($value))
                @if ($value==1)
                    checked=""
                @endif
            @endif
        @endif
        >
        <span class="font-weight-normal">{{ isset($label) ? $label : trans("form.label.".$name) }}</span>
    </label>

    @if ($errors->has($name))
        <span class="help-block">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
        @endif
        @if (isset($info))
        </br>
        <small>{{$info}}</small>
    @endif
</div>