@php
   if (!isset($value)) {
       $value = "";
   } 
@endphp
<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>

    <select name="{{$name}}"class="form-control select2" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}">
        @foreach ($collection as $item)
            <option
            @if (old($name) == $item->value || $value == $item->value )
                selected='selected'
            @endif
            value="{{$item->value}}"
            >
                {{$item->name}}
            </option>
        @endforeach

    </select>


    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif


    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
