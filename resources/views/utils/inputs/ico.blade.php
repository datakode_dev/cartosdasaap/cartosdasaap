@php
    if (!isset($value)) {
        $value ="";
    }
    if (!isset($service)) {
        $service = "";
    }
@endphp

<div id="icon-picker" class="form-group has-feedback {{ $errors->has($name) ? 'has-error2' : '' }}">
<!-- {{ json_encode($service) }} -->
<div>
    <label for="{{$name}}">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    <div class="input-group">
        <span id ="setBackground" class="input-group-addon" style="min-width: 34px;">    
            @if ($service !== "")     
            <!-- EDIT SERVICE -->   
            <i id="icon-preview" class="" data-name="{{$service->icon_name}}" data-svg="{{$service->icon_svg}}" data-path="{{$service->icon_path}}"></i>
            @else <!-- NEW SERVICE -->
            <i id="icon-preview" class=""></i>
            @endif
        </span>

        <input id="{{$name}}" type="text" name="{{$name}}" value="{{ old($name) == null ? $value : old($name) }}" class="form-control previewInput">
        
        <span class="input-group-btn">
            <a data-toggle="modal" data-target="#icon-modal-service" id="search_modal">
                <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-pencil"></i></button>
            </a>
            @include('box.services_pei.chooseIconModalService', ["name"=>"ico", "can_edit"=>$can_edit])
        </span>
    </div>

    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif

    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
</div>
