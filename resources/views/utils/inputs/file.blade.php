@php
    if (!isset($value)) {
        $value ="";
    }
@endphp
<div class="form-group has-feedback {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">{{ isset($label) ? $label : trans("form.label.".$name) }}</label>
    @if (isset($info))
    </br>
        <small>{{$info}}</small>
    </br>
    @endif
<input type="file" name="{{$name}}" value="{{ old($name) == null ? $value : old($name) }}" class="form-control" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}" >
    @if (isset($glyphicon))
        <span class="glyphicon {{$glyphicon}} form-control-feedback"></span>
    @endif

    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
