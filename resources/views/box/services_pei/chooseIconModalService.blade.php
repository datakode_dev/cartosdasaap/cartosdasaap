@php
    if (!isset($value)) {
        $value ="";
    }
@endphp


<div class="modal fade" id="icon-modal-service">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group col-md-12">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Gestion des icônes</h3>
                </div>
                <div class="box-body listing_ico_color">
                    <!-- TAB MENU -->
                    <ul class="nav nav-pills h4" role="tablist">
                        <li role="presentation" class="active"><a href="#fa-icon" role="tab" data-toggle="tab">Rechercher une icône</a>
                        </li>
                        <li role="presentation"><a href="#custom-icon" role="tab" data-toggle="tab">Mes icônes</a>
                        </li>
                    </ul>
                    <!-- TAB CONTENT -->
                    <div class="container-fluid h5">
                        <div class="tab-content">
                            <!-- FA ICONS -->
                            <div id="fa-icon" role="tabpanel" class="tab-pane fade in active">
                                <input id="{{$name}}" type="text" name="{{$name}}" value="{{ old($name) == null ? $value : old($name) }}" class="form-control input-coo" placeholder="{{ isset($placeholder) ? $placeholder : trans("form.placeholder.".$name) }}" >
                            </div>

                            <div id="custom-icon" role="tabpanel" class="tab-pane fade">

                                @if($can_edit)
                                <button id="addIconButton" class="btn btn-block btn-info btn-flat" type="button" data-toggle="collapse" data-target="#addIcon" aria-expanded="false" aria-controls="collapseExample" style="width:auto;margin-bottom: 1rem;margin-right: 1rem;float:left;">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter une icône
                                </button>

                                <button id="deleteIcon" class="btn btn-block btn-danger btn-flat" type="button" style="width:auto;margin-bottom: 1rem;">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Supprimer une icône
                                </button>
                                <button id="cancelDeleteIcon" class="btn btn-block btn-danger btn-flat" type="button" style="width:auto;margin-bottom: 1rem;display:none;">
                                    Annuler la suppresion
                                </button>

                                <div class="collapse" id="addIcon">

                                    {{-- <form id="saveIcon" action="{{action('ServicePeiController@update',[$current_account->id, $service_pei->id])}}" method="post" enctype="multipart/form-data"> --}}

                                    <div class="card card-body row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="iconName" class="control-label">Nom</label>
                                                <input type="text" class="form-control" id="iconName" placeholder="Nom">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="iconSVG" class="control-label">SVG</label>
                                                <input type="file" id="iconSVG" accept=".svg">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button id="saveIcon" class="btn btn-block btn-info btn-flat">Valider</button>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <input type="search" id="iconSearch" class="form-control mb-4" placeholder="Recherche" style="margin-top: 20px;"> <br/>

                                <div id="iconList" style="margin: 60px 0 20px 0;white-space: normal;"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer center-block"> <!-- id valid_ico_color then js -->
                    <!-- <button type="button" id="" class="btn btn-default pull-right" data-dismiss="modal">Valider</button> -->
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
</div>
