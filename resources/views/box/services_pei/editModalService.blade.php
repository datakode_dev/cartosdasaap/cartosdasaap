<!-- affiche dans un modal un ensemble lors du clic choisir ensemble-->
<div class="modal fade" id="edit-modal-service">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group col-md-12 text-center">
                    <b>Choisir un marqueur</b> :
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="box-body listing_ico_color">
                    <div class="form-group col-md-8" >
                        <label>Choisir un marqueur :</label>
                        <select class="search_ico_color" id="search_ico_color" data-json="{{$icons_json}}">
                            <option value="">Choisir marqueur</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer center-block">
                    <button type="button" id="valid_ico_color" class="btn btn-default pull-right" data-dismiss="modal">Valider</button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
</div>
