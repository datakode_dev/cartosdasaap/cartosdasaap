﻿Attribuez un nom à votre contexte.   
Choisissez un type de mesure :  
- isochrone (calcul en un temps donnée)  
- isodistance (calcul à une distance donné)  

Définissez le type de déplacement :  
- voiture  
- à pied    

Recherchez les services à ajouter (préalablement importés).   
Ajoutez des indicateurs.  
Renseignez dans étapes les différentes durées ou distances souhaitées.  
Le nouveau contexte sera ensuite ajouté à la liste de tous les contextes.  