﻿Renommez votre contexte.  
Pour les champs types de déplacement, services et indicateurs : Supprimez les éléments sélectionnés à l’aide de la croix et cliquez pour en ajouter des nouveaux.
Retirez une étape en supprimant la valeur ajoutée.  
Validez afin de mettre à jour votre contexte.  
