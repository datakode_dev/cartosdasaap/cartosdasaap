﻿Visualisez le nom du contexte ou procédez à sa modification avec l'icône ‘crayon’.  
Consultez les indicateurs, les services, la zone correspondante, le graphique et l’isochrone associé au contexte.
Au survol du graphique, les données relatives aux indicateurs choisies apparaissent. Au clic, l’isochrone associé est visible.
Le contexte peut être intégré sur une page web à l’aide du lien d’intégration disponible en fin de page. Copiez le lien à l’aide du bouton ‘copier’.  
