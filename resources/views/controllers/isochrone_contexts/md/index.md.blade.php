﻿Consultez l'ensemble des contextes.  
Un nouveau contexte peut être ajouté avec le bouton "Ajouter".  

'icon crayon' : modifier un contexte  
'icon oeil' : visualiser un contexte  
'icon fichier' : dupliquer un contexte  
'icon dossier' : archiver un contexte  
'icon flèche' : désarchiver un contexte  
'icon poubelle' : supprimer un contexte  