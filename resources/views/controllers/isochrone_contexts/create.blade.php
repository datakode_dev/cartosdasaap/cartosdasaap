@extends('adminlte::page')

@section('title', 'Proxiclic - Créer un contexte')

@section('content_header')
    <h1>Créer un contexte</h1>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contexte</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('IsochroneContextController@store',[$current_account->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        @include('utils.inputs.select', ["name"=>"iso_type","collection"=>$iso_types])
                        @include('utils.inputs.multiples.select', ["name"=>"move_types","collection"=>$move_types])
                        @include('utils.inputs.select_limit', ["name"=>"limit","url"=>action('API\GeoController@getLimit')])
                        @include('utils.inputs.checkbox', ["name"=>"load_full","info"=>"Si cocher, un calcul sur les communes et les EPCI sera effectué"])
                        @include('utils.inputs.multiples.select', ["name"=>"services","collection"=>$service_peis])
                        @include('utils.inputs.multiples.select', ["name"=>"indicators","collection"=>$indicators])
                    </div>
                    <div class="box-body" id="step">
                        @include('utils.inputs.multiples.number', ["name"=>"steps"])
                    </div>
                    <div class="box-body">
                        <button type="button" class="btn btn-default" id="add_step">Ajouter une étape </button>
                    </div>
                    <div class="box-body">
                        @include('utils.inputs.multiples.tags', ["name"=>"referrers", "collection" => [], "info"=>"Saisir les noms de domaine autorisés à afficher le contexte (ex: 'www.datakode.fr,*.proxiclic.fr')."])
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="/js/isochrone_contexts.js"></script>
@endpush
