@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un contexte")

@section('content_header')
    <h1>Edition d'un contexte</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contexte</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('IsochroneContextController@update',[$current_account->id,$context->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name","value"=>$context->name])
                        {{--@include('utils.inputs.multiples.select', ["name"=>"iso_type","collection"=>$iso_types,'values'=>$selected_iso_type])--}}
                        @include('utils.inputs.multiples.select', ["name"=>"move_types","collection"=>$move_types,'values'=>$selected_move_types])
                        @include('utils.inputs.select_limit', [
                            "name" => "limit",
                            "current" =>$current_limit,
                            "url"=>action('API\GeoController@getLimit')
                        ])
                        @include('utils.inputs.checkbox', ["name"=>"load_full","value"=>$context->load_full,"info"=>"Si cocher, un calcul sur les communes et les EPCI sera effectué"])
                        @include('utils.inputs.multiples.select', ["name"=>"services","collection"=>$service_peis,"values"=>$service_peis_ids])
                        @include('utils.inputs.multiples.select', ["name"=>"indicators","collection"=>$indicators,"values"=>json_decode($context->indicators)])
                    </div>
                    <div class="box-body" id="step">
                        @include('utils.inputs.multiples.number', ["name"=>"steps","values"=>$selected_step_moves])
                    </div>
                    <div class="box-body">
                        <button type="button" class="btn btn-default" id="add_step">Ajouter une étape </button>
                    </div>
                    <div class="box-body">
                        @include('utils.inputs.multiples.tags', ["name"=>"referrers", "values" => explode(',', $context->referrers), "collection" => explode(',', $context->referrers), "info"=>"Saisir les noms de domaine autorisés à afficher le contexte (ex: 'www.datakode.fr,*.proxiclic.fr')."])
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
        <script src="/js/isochrone_contexts.js"></script>
    @endpush
