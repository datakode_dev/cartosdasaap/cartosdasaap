@extends('adminlte::page')

@section('title', 'Proxiclic - Contexte : ' . $context->name)

@section('content_header')
    <h1>Contexte</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@stop
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Informations</h3>
                    @if ($can_edit == true)
                        <div class="box-tools pull-right">
                            <a data-toggle='tooltip' title='Recalculer' href='{{action('IsochroneContextController@updateCalcul',[$current_account->id, $context->id])}}'>
                                <i class='fa fa-refresh' aria-hidden='true'></i>
                            </a>
                            <a data-toggle='tooltip' title='Modifier' href='{{action('IsochroneContextController@edit',[$current_account->id, $context->id])}}'>
                                <i class='fa fa-pencil' aria-hidden='true'></i>
                            </a>

                            <form method='POST' class="pull-right" action='{{action('IsochroneContextController@duplicate',[$current_account->id, $context->id])}}'>
                                <input name='_method' type='hidden' value='put'>
                                <a href='' onclick='parentNode.submit();return false;'>
                                    <i class='fa fa-files-o' aria-hidden='true'></i>
                                </a>
                                {!! csrf_field() !!}
                            </form >
                        </div>
                    @endif
                </div>
                <div class="box-body">
                    <dl>
                        <dt>Nom</dt>
                        <dd>{{$context->name}}</dd>
                    </dl>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Indicateurs</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul>
                        @foreach (json_decode($context->indicators) as $indicator)
                            <li>{{trans($indicator)}}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Services</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @if (count($steps) > 0)
                    <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Services</th>
                                @foreach ($steps->first() as $element)
                                    <th style="text-align: center" colspan="{{ $steps->first()->first()->count() }}">
                                        {{$element->first()->move_type_name}}
                                    </th>
                                @endforeach
                            </tr>
                            <tr>
                                <td></td>
                                @foreach ($steps->first() as $col)
                                    @foreach ($col as $element)
                                        <td>
                                            {{$element->value}} {{\App\Helpers\HtmlHelper::getUnitByIsoType($context->iso_type_id)}}
                                        </td>
                                    @endforeach
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($steps as $col)
                                <tr>
                                    <td>{{$col->first()->first()->service_name}}</td>
                                    @foreach ($col as $step_elements)
                                    @foreach ($step_elements as $element)
    
                                        <td>
                                            @switch($element->status)
                                                @case(1)
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                @break
                                                @case(2)
    
                                                @break
                                                @case(3)
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                @break
                                                @default
    
                                            @endswitch
                                        </td>
                                    @endforeach
                                    @endforeach
                                </tr>
    
                            @endforeach
                            </tbody>
                        </table>
                    @else
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-warning"></i>Attention</h4>
                        Aucun service de sélectionné
                    </div>
                    @endif
                   
                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Limite</h3>
                </div>
                <div class="box-body" id="map-limite" data-url="{{$limit_url}}" style="height: 300px;">
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
    </div>
    <script src="{{ route('home') }}/js/embed.js" async data-context-id="{{ $context->id }}"></script>
@stop

@push('js')
    <script type="text/template" id="footer">
        <div class="box-footer">
            <h4>Intégrer ce contexte sur votre page web.</h4>
            <p>Copiez et collez le code ci dessous là où vous souhaitez que le contexte apparaisse.</p>
            <figure class="highlight" style="position:relative;">
                <div class="bs-clipboard">
                    <button type="button" class="btn-clipboard" title="Copier dans le presse papier">
                        Copier
                    </button>
                </div>
                <pre style="text-align:center;"><code class="language-html embed-code" data-lang="html">&lt;script src="{{ route('home') }}/js/embed.js" data-context-id="{{ $context->id }}"&gt;&lt;/script&gt;</code></pre>
            </figure>
        </div>
    </script>
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script src="/js/isochrone_contexts.js"></script>
@endpush
