@extends('adminlte::page')

@section('title', 'Proxiclic - Liste des contextes')

@section('content_header')
    <h1>Liste des contextes</h1>
@stop
@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contextes</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <table id="table_isochrone_contexts" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Territoire</th>
                                <th>Type de mesure</th>
                                <th>Liste des services</th>
                                <th>Mise à jour</th>
                                <th>Statut</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@stop

@push('js')
    <script src="/js/isochrone_contexts.js"></script>
@endpush
