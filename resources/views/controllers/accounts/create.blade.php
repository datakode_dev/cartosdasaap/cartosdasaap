@extends('adminlte::page')

@section('title', 'Proxiclic - Ajouter un compte')

@section('content_header')
    <h1>Ajouter un compte</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Compte</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('AccountController@store')}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        <p class="help-block">you admin</p>
                        @include('utils.inputs.text', ["name"=>"admin_firstname"])
                        @include('utils.inputs.text', ["name"=>"admin_lastname"])
                        @include('utils.inputs.email', ["name"=>"admin_email"])
                        @include('utils.inputs.file', ["name"=>"logo"])
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop