@extends('adminlte::page')

@section('title', 'Proxiclic - Listes des comptes')

@section('content_header')
    <h1>Listes des comptes</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Comptes</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <table id="table_account" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="/js/account.js"></script>
@endpush