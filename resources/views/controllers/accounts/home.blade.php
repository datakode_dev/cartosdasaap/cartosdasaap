@extends('adminlte::page')

@section('title', 'Proxiclic - Accueil')

@section('content_header')
    <h1>Accueil</h1>
@stop

@section('content')
    <div class="row">
        <?php
        $color = $capacity_use >= 85 ? 'yellow' : 'green';
        if ($capacity_use > 100) {
            $color = 'red';
        }
        ?>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-{{ $color }}" style="height:120px">
                <div class="inner">
                    <h3 style="font-size: 1.3em;">Espace utilisé</h3>
                    <div><small>{{ $capacity_use }}%</small></div>
                    <div class="progress" style="height: 1em;">
                        <div class="progress-bar" style="width: {{ $capacity_use }}%"></div>
                    </div>
                    <span class="progress-description">
                        @if ($capacity_use <= 100)
                                {{ $total_space - $db_size }}&nbsp;kB libres
                        @else
                                Capacité dépassée
                        @endif
                    </span>
                </div>
                <div class="icon">
                    <i class="fa fa-gear"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue" style="height:120px">
                <div class="inner">
                    <h3 style="font-size: 1.3em;">Services</h3>
                    <p>
                        Terminés : {{$services_ok}}
                        <br>
                        En cours de calcul : {{$services_in_progress}}
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-download"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue" style="height:120px">
                <div class="inner">
                    <h3 style="font-size: 1.3em;">Contextes</h3>
                    <p>
                        Terminés : {{$contextes_ok}}
                        <br>
                        En cours de calcul : {{$contextes_in_progress}}
                        <br>
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-share-alt"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue" style="height:120px">
                <div class="inner">
                    <h3 style="font-size: 1.3em;">Tableaux de bord</h3>
                    <p>
                        Terminés : {{$boards_ok}}
                        <br>
                        En cours de calcul : {{$boards_in_progress}}
                    <p>
                </div>
                <div class="icon">
                    <i class="fa fa-tachometer"></i>
                </div>
            </div>
        </div>

        <script type="text/template" id="ign-template" data-url="{{ route('API.getHomeIgnData', $current_account->id) }}">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <?php
//                $status = $monitor->statusClass == 'success' ? 'green' : 'red';
//                if ($monitor->responseTimes[0]->value > 1000) {
//                    $status = 'yellow';
//                }
//                ?>
                <div class="small-box bg-__STATUS__">
                    <div class="inner">
                        <h3 style="font-size: 1.3em;">__NAME__</h3>

                        <p>Temps de réponse : __VALUE__ms</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-__ICON__"></i>
                    </div>
                    <a href="https://stats.uptimerobot.com/28xBxu6Q9/__ID__" class="small-box-footer" target="_blank">
                        Plus d'infos <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </script>
    </div>
@stop

    @push('js')
    <script src="/js/account_home.js"></script>
    @endpush