﻿Accédez à votre profil utilisateur en haut à droite pour modifier vos informations personnelles : nom, prénom, adresse mail, mot de passe ou pour procéder à la déconnexion.  
Découvrez les informations liées à votre compte :  

- espace utilisé  
- nombre de services en cours de calcul et terminés    
- nombre de contextes en cours de calcul et terminés    
- nombre de tableaux de bord en cours de calcul et terminés    
- les statuts IGN : le temps de disponibilité est représenté par les couleurs vertes (rapide) et orange (long)  