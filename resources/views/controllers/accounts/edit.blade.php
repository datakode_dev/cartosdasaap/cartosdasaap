@extends('adminlte::page')

@section('title', 'Proxiclic - Ajouter un compte')

@section('content_header')
    <h1>Ajouter un compte</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Compte</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('AccountController@update',[$current_account->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name", "value"=>$account->name])
                        <span><b>Logo actuel : </b></span>
                        @if ($file !== null)
                            <img src="{{$file}}" alt="logo" height="100">
                        @else
                            <span>Il n'y a pas de logo actuellement</span>
                        @endif
                        @include('utils.inputs.file', ["name"=>"change_logo"])
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop