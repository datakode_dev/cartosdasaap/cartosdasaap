@extends('adminlte::page')

@section('title', "Proxiclic - Affichage d'un profil")

@section('content_header')
    <h1>Affichage d'un profil</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Utilisateur</h3>
                        <a data-toggle='tooltip' title='Modifier' href='{{action('ProfilController@edit')}}'>
                            <i class='fa fa-pencil box-tools pull-right' aria-hidden='true'></i></a>
                </div>
                <div class="box-body with-border">
                    <img src="{{$avatar}}"/><br/>
                    <b>Nom :</b>{{$current_user->lastname}}<br/>
                    <b>Prénom :</b>{{$current_user->firstname}}<br/>
                    <b>Email :</b>{{$current_user->email}}<br/>
                </div>
            </div>
        </div>
    </div>
@stop
