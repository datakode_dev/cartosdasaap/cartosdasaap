@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un profil")

@section('content_header')
    <h1>Edition d'un profil</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Profil</h3>
                </div>

                <!-- form start -->
                <form action="{{action('ProfilController@update',[$current_user->id])}}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {!! csrf_field() !!}
                    <!-- on affiche le nom, le prenom et le mail de l'utlisateur -->
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"firstName","value"=>$current_user->firstname])
                        @include('utils.inputs.text', ["name"=>"lastName","value"=>$current_user->lastname])
                        @include('utils.inputs.text', ["name"=>"user_email","value"=>$current_user->email])

                        Entrez votre mot de passe actuel pour effectuer toute modification :
                        <div class="form-group has-feedback {{ $errors->has('actualPassword') ? 'has-error' : '' }}">
                            <input type="password" name="actualPassword" class="form-control"
                                   placeholder="{{ trans('adminlte::adminlte.password') }}">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            @if ($errors->has('actualPassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('actualPassword') }}</strong>
                                </span>
                            @endif
                        </div>

                        Définir un nouveau mot de passe :
                        <div class="form-group has-feedback {{ $errors->has('newPassword') ? 'has-error' : '' }}">
                            <input type="password" name="newPassword" class="form-control"
                                   placeholder="{{ trans('adminlte::adminlte.password') }}">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            @if ($errors->has('newPassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('newPassword') }}</strong>
                                </span>
                            @endif
                        </div>

                        Entrez à nouveau le mot de passe :
                        <div class="form-group has-feedback {{ $errors->has('newPasswordConfirmation') ? 'has-error' : '' }}">
                            <input type="password" name="newPasswordConfirmation" class="form-control"
                                   placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                            @if ($errors->has('newPasswordConfirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('newPasswordConfirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop
