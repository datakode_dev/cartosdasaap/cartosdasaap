﻿Consultez l'ensemble des contextes de recherche crées.  
Ajoutez un nouveau contexte de recherche avec le bouton "Ajouter". 
 
'icon crayon' : modifier un contexte de recherche  
'icon oeil' : visualiser un contexte de recherche  
'icon fichier' : dupliquer un contexte   
'icon dossier' : archiver un contexte de recherche  
'icon fleche' : désarchiver un contexte de recherche  
'icon poubelle' : supprimer un contexte de recherche  