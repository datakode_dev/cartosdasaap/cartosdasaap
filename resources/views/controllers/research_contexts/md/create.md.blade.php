﻿Ajoutez un nouveau contexte de recherche en lui attribuant un nom, une zone (limite), un service et recherchez la clé ign préalablement ajoutée dans la liste des clés IGN (onglet Paramètres).  
Le contexte de recherche ainsi établi s’ajoute à la liste des contextes de recherche.
