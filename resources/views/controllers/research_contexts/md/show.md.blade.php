﻿Ajoutez la catégorie des services et recherchez les services accessibles dans un certain rayon à partir d’une adresse ou d’un trajet.  
Visualisez les services sur la carte.  
Ajoutez un service à votre sélection et imprimez vos données.  
Le contexte de recherche peut être intégré sur une page web à l’aide du lien d’intégration disponible en fin de page.  
Copiez le lien à l’aide du bouton ‘copier’.


