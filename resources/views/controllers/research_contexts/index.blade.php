@extends('adminlte::page')

@section('title', 'Proxiclic - Liste des contextes de recherche')

@section('content_header')
    <h1>Liste des contextes de recherche</h1>
@stop
@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop
@section('content')

@if (!$has_ign_key)
<div class="callout callout-danger">
    <h4>Aucune clé IGN n'est disponible sur votre compte</h4>

    <p>Merci, d'ajouter une clé dans Paramètres->Listes des clés IGN</p>
  </div>    
@endif


    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contextes de recherche</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <table id="table_research_contexts" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>name</th>
                                {{--<th>method</th>
                                <th>nbr_step</th>--}}
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@stop

@push('js')
    <script src="/js/table_research_contexts.js"></script>
@endpush
