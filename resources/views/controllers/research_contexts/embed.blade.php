<head>
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="/css/app.css"/>
    <link rel="stylesheet" href="/css/font.css"/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link rel="stylesheet" href="/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.css">
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />
    <link rel="stylesheet" href="/vendor/leaflet.markercluster/dist/MarkerCluster.Default.css">
    <link rel="stylesheet" href="/vendor/geoportal/GpPluginLeaflet.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body>

    <div class="row" id="search-context-main">
        @if($context->active === false)
            <div class="col-md-12" style="" id="disabled-message">
                <div class="callout callout-warning">
                    <h4>Ce contexte de recherche est désactivé</h4>

                    <p>Les information présentées ne sont peut-être pas à jour.</p>
                </div>
            </div>
        @endif
        <div class="col-md-4">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Recherche</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                    $values = [];
                    if (count($services) > 0) {
                        $values[] = $services[0]->value;
                    }
                    ?>
                    @include('utils.inputs.multiples.select', ["name"=>"service", "collection" => $services, "values" => $values])
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Autour d'une adresse</a></li>
                            <li><a href="#tab_2" data-toggle="tab">Autour d'un trajet</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <form id="search-point-form">
                                    <div class="form-group">
                                        <label for="search-point">Adresse</label>
                                        <select id="search-point" class="address-search"></select>
                                    </div>
                                    @include('utils.inputs.range', ["name"=>"radius", "min" => 0.25, "max" => 10, "disabled" => true, "value" => 5, "step" => 0.25])
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <form id="search-itinerary-form">
                                    <div class="form-group">
                                        <label for="search-start">Départ</label>
                                        <select id="search-start" class="address-search"></select>
                                        <label for="search-end">Arrivée</label>
                                        <select id="search-end" class="address-search"></select>
                                    </div>
                                    @include('utils.inputs.range', ["name"=>"buffer", "min" => 0.25, "max" => 10, "disabled" => true, "value" => 5, "step" => 0.25])
                                </form>
                            </div>
                            <!-- /.tab-pane -->


                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Sélection</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-danger" id="reset">Vider la sélection</button>
                        <button class="btn btn-primary" id="print">Imprimer</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="search-services">
                    <p class="no-selection">Aucune selection</p>
                    <script type="text/template" id="search-service-template">
                        <div class="info-box sm">
                            <span class="info-box-icon"><span></span><i></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text"></span>
                                <span class="info-box-number"></span>
                            </div>
                        </div>
                        <ul class="selection">
                        </ul>
                    </script>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body" id="search-map" style="height: 400px" data-context-id="{{ $context->id }}"  data-api-key="{{ $key->key }}">
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row footer">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <h4>Intégrer ce tableau de bord sur votre page web.</h4>
                    <p>Copiez et collez le code ci dessous là où vous souhaitez que le contexte apparaisse.</p>
                    <figure class="highlight" style="position:relative;">
                        <div class="bs-clipboard">
                            <button type="button" class="btn-clipboard" title="Copier dans le presse papier">
                                Copier
                            </button>
                        </div>
                        <pre style="text-align:center;"><code class="language-html embed-code" data-lang="html">&lt;script src="{{ route('home') }}/js/embed-search.js" data-context-id="{{ $context->id }}"&gt;&lt;/script&gt;</code></pre>
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js"></script>
    <script src="{{ asset('vendor/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.min.js') }}"></script>

    <script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script src="{{ asset('vendor/tarteaucitronjs/tarteaucitron.js') }}"></script>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.js"></script>
    <script type="text/javascript" src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
    <script type="text/javascript" src="/vendor/geoportal/GpPluginLeaflet.js"></script>
    <script src="/js/research_context.js"></script>
</body>
