@extends('adminlte::page')

@section('title', 'Proxiclic - Contexte de recherche :  ' . $context->name)

@section('content_header')
    <h1>Contexte de recherche :  {{$context->name}}</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link rel="stylesheet" href="/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.css">
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />
    <link rel="stylesheet" href="/vendor/leaflet.markercluster/dist/MarkerCluster.Default.css">
    <link rel="stylesheet" href="/vendor/geoportal/GpPluginLeaflet.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@stop
@section('content')
    <div class="row bo-view" id="search-context-main">
        <!-- VIEW ADDED BY EMBED SCRIPT -->
        <div class="row footer">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <h4>Intégrer ce tableau de bord sur votre page web.</h4>
                        <p>Copiez et collez le code ci dessous là où vous souhaitez que le contexte apparaisse.</p>
                        <figure class="highlight" style="position:relative;">
                            <div class="bs-clipboard">
                                <button type="button" class="btn-clipboard" title="Copier dans le presse papier">
                                    Copier
                                </button>
                            </div>
                            <pre style="text-align:center;"><code class="language-html embed-code" data-lang="html">&lt;script src="{{ route('home') }}/js/embed-search.js" data-context-id="{{ $context->id }}"&gt;&lt;/script&gt;</code></pre>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <!-- <script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script src="/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.js"></script>
    <script type="text/javascript" src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
    <script type="text/javascript" src="/vendor/geoportal/GpPluginLeaflet.js"></script> -->
    
    <script src="/js/embed-search.js" data-context-id="{{ $context->id }}"></script>
    <!-- <script src="/js/research_context.js"></script> -->
@endpush