@extends('adminlte::page')

@section('title', 'Proxiclic - Créer un contexte de recherche')

@section('content_header')
    <h1>Edition d'un contexte de recherche</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contexte de recherche</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('ResearchContextsController@update',[$current_account->id,$research_context->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="put">
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name","value"=>$research_context->name])
                        @include('utils.inputs.select_limit', ["name"=>"limit","url"=>action('API\GeoController@getLimit'),"current" => $current_limit])
                        @include('utils.inputs.number', ["name"=>"dist_around_limit","value" => $research_context->dist_around_limit])
                        @include('utils.inputs.multiples.select', ["name"=>"services","collection"=>$service_peis, 'values' => $current_services])
                        @include('utils.inputs.select', ["name"=>"ign_key_id","collection"=>$ign_keys,"value"=>$research_context->ign_key_id])

                    <!-- TRI -->
                    <label>Outils</label>
                        @include('utils.inputs.checkbox', ["name"=>"display_city","value"=>$research_context->display_city])
                        @include('utils.inputs.checkbox', ["name"=>"display_route","value"=>$research_context->display_route])
                        @include('utils.inputs.checkbox', ["name"=>"display_selection","value"=>$research_context->display_selection])
                        @include('utils.inputs.checkbox', ["name"=>"display_legend","value"=>$research_context->display_legend])
                        <br/>
                        @include('utils.inputs.multiples.tags', ["name"=>"referrers", "values" => explode(',', $research_context->referrers), "collection" => explode(',', $research_context->referrers), "info"=>"Saisir les noms de domaine autorisés à afficher le contexte (ex: 'www.datakode.fr,*.proxiclic.fr')."])
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


