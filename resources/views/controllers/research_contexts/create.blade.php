@extends('adminlte::page')

@section('title', 'Proxiclic - Créer un contexte de recherche')

@section('content_header')
    <h1>Créer un contexte de recherche</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contexte de recherche</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('ResearchContextsController@store',[$current_account->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        @include('utils.inputs.select_limit', ["name"=>"limit","url"=>action('API\GeoController@getLimit')])
                        @include('utils.inputs.number', ["name"=>"dist_around_limit","value" => 0])
                        @include('utils.inputs.multiples.select', ["name"=>"services","collection"=>$service_peis])
                        @include('utils.inputs.select', ["name"=>"ign_key_id","collection" => $ign_keys])
                        
                        <!-- TRI -->
                        <label>Outils</label>
                        @include('utils.inputs.checkbox', ["name"=>"display_city"])
                        @include('utils.inputs.checkbox', ["name"=>"display_route"])
                        @include('utils.inputs.checkbox', ["name"=>"display_selection"])
                        @include('utils.inputs.checkbox', ["name"=>"display_legend"])
                        <br/>

                        @if (count($ign_keys) === 0)
                            <a href="{{ route('ign_keys.create', [$current_account->id]) }}">Ajouter une clé IGN</a>
                        @endif
                        @include('utils.inputs.multiples.tags', ["name"=>"referrers", "collection" => [], "info"=>"Saisir les noms de domaine autorisés à afficher le contexte (ex: 'www.datakode.fr,*.proxiclic.fr')."])
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

