@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1></h1>
@stop

@section('content')
    <div class="row">
        @foreach ($accounts as $account)
            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header ">
                        <div class="widget-user-image">
                            {{--<img class="img-circle" src="../dist/img/user7-128x128.jpg" alt="User Avatar">--}}
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">
                            <a href="{{$account->url}}">{{$account->name}}</a>
                        </h3>
                    </div>
                    {{--<div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#"> <span class="pull-right badge bg-blue">6</span></a></li>
                        </ul>
                    </div>--}}
                </div>
                <!-- /.widget-user -->
                
            </div>
        @endforeach
    </div>
@stop