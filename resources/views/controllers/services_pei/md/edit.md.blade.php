﻿Renommez votre service ainsi que sa description.
Créez un nouveau marqueur ou choisissez un autre marqueur existant.
3 modes d’import sont disponibles :  
- Importez un fichier en .csv,.kml,.gpx ou .geojson. Les coordonnées d'un fichier .geojson,.kml ou .gpx doivent respecter le format WGS84.  
- Ajoutez l'adresse du site source (url). Les coordonnées de cette ressource doivent respecter le format WGS84.  
- Récupérez des données SIRENE en renseignant le domaine d'activité, la limite (commune, EPCI, département), la zone tampon en km et le nombre de jours entre chaque recalcul.  
  
Validez afin de mettre à jour votre service.  
