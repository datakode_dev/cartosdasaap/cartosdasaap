﻿Consultez l'ensemble des services importés.  
Ajoutez de nouveaux services avec le bouton "Ajouter".  
  
'icon crayon' : modifier un service sur la carte  
'icon oeil' : visualiser un service sur la carte  
'icon fichier' : dupliquer un service  
'icon dossier' : archiver un service  
'icon flèche' : désarchiver un service  
'icon poubelle' : supprimer un service  