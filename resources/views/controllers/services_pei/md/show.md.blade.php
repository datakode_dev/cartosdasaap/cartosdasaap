﻿Les nombres déterminent l’ensemble des services présents à un emplacement.    
Zoomez pour visualiser l’emplacement exact d’un service.    
Recherchez une adresse à l’aide de la loupe et découvrez les services présents aux alentours.    
Visualisez vos services en mode satellite ou en mode plan.   
Cliquez sur le bouton "modifier les points" pour déplacer, supprimer ou ajouter manuellement des points de ce service sur une carte.   
Un tableau représente tous les points de ce service avec la précision BAN, l'adresse, l'adresse BAN et un crayon pour pouvoir modifier le point sur la carte.
