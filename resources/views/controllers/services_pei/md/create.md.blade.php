﻿Renseignez le nom et décrivez le service qui sera importé.  
Ajoutez une icône et une couleur à votre service afin qu'il soit identifiable sur la carte.  
3 modes d’import sont disponibles :  
- Importez un fichier en .csv,.kml,.gpx ou .geojson. Les coordonnées d'un fichier .geojson,.kml ou .gpx doivent respecter le format WGS84.  
- Ajoutez l'adresse du site source (url). Les coordonnées de cette ressource doivent respecter le format WGS84.  
- Récupérez des données SIRENE en renseignant le domaine d'activité, la limite (commune, EPCI, département), la zone tampon en km et le nombre de jours entre chaque recalcul.  

Pour l'importation d'un fichier du type CSV: les champs Adresse, Code postal et Commune sont obligatoires.  
Il devra avoir le format "CSV UTF-8(délimité par des virgules)(*.csv)".  
  
 