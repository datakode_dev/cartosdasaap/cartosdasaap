@extends('adminlte::page')

@section('title', 'Proxiclic - Liste des services')

@section('content_header')
    <h1>Liste des services</h1>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Services</h3>
                    @if ($can_edit == true)
                        <div class="box-tools pull-right">
                            <a href="{{action('ServicePeiController@create',[$current_account->id])}}" class="btn btn-box-tool btn-default">Ajouter</a>
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <table id="table_services_pei" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Description</th>
                                <th>Nombre d'éléments'</th>
                                <th>Mise à jour</th>
                                <th>Mise à jour par </th>
                                <th>Statut</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop

@push('js')
    <script src="/js/services_pei.js"></script>
@endpush
