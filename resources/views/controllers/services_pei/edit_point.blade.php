@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un service")

@section('content_header')
    <h1>Edition d'un point</h1>
@stop

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link rel="stylesheet" href="/vendor/leaflet-geocoder-ban/leaflet-geocoder-ban.min.css">

    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css">
    <link rel="stylesheet" href="/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.css">

    <link rel="stylesheet" href="/vendor/leaflet.markercluster/dist/MarkerCluster.Default.css">
    <link rel="stylesheet" href="/vendor/leaflet-toolbar/dist/leaflet.toolbar.css">
    <link rel="stylesheet" href="/vendor/leaflet-draw/dist/leaflet.draw.css"/>

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop

 
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Point {{$marker_id}}
                    </h3>
                </div>
                <div class="box-body" data-json="{{$json_marker}}">
                    
                    @php
                        $addr="";
                        $prop = json_decode($pei->prop);
                        if (is_object($prop)) {
                            $prop = (array) $prop;
                            if (isset($prop['Adresse']) && isset($prop['Code postal']) && isset($prop['Commune'])) {
                                $addr = $prop['Adresse']." ".$prop['Code postal']." ".$prop['Commune'];
                            }
                        }
                    @endphp
                    {{$addr}}
                    <div id="maCarte" class="col-md-12" data-json="{{$json_marker}}" style="height: 80vh;">
                    </div>
                </div>
                <div class="box-footer">
                    <div id="sauvegardeMarkers" class="col-md-offset-5 col-md-12" style="visibility: hidden">
                        <form action="{{action('ServicePeiController@update_point',[$current_account->id,$service_id,$marker_id])}}" method="post">
                            <input type="hidden" name="_method" value="put">
                            {!! csrf_field() !!}
                            <input name="marker_update" id="marker_update" type="hidden" value="">
                            <button type="submit" class="btn btn-danger">Enregistrer les modifications</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop


@push('js')
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script type="text/javascript" src="/vendor/leaflet-geocoder-ban/leaflet-geocoder-ban.js"></script>

    <script src="/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.js"></script>
    <script type="text/javascript" src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>

    <script src="/vendor/leaflet-toolbar/dist/leaflet.toolbar.js"></script>
    <script src="/vendor/leaflet-draw/dist/leaflet.draw.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="/js/pei_edit.js"></script>
@endpush