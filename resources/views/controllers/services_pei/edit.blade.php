@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un service")

@section('content_header')
    <h1>Edition d'un service</h1>
@stop

@section('css')
@stop


@section('content')


<form action="{{action('ServicePeiController@update',[$current_account->id, $service_pei->id])}}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="put">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Service</h3>
                </div>
                <div class="box-body">
                    @include('utils.inputs.text', ["name"=>"name","value"=>$service_pei->name])
                    @include('utils.inputs.text', ["name"=>"info","value"=>$service_pei->info])

                    @if ($can_edit)
                        <span><b>Droits d'accès en modification : </b></span><br/>
                        @if ($access === 'all')
                            <label class="radio-import"><input type="radio" id="all" name="access" value="all" checked>Tous les utilisateurs</label>
                            <label class="radio-import"><input type="radio" id="none" name="access" value="none">Administrateurs seulement</label>
                            <label class="radio-import"><input type="radio" id="select_members" name="access" value="select_members">Partager avec</label>
                            <div id="members_access" style="display: none">
                                @include('utils.inputs.multiples.select', ["name"=>"members_access","collection"=>$members,"values"=>$members_service])
                            </div>
                        @else
                            @if ($access === 'none')
                                <label class="radio-import"><input type="radio" id="all" name="access" value="all">Tous les utilisateurs</label>
                                <label class="radio-import"><input type="radio" id="none" name="access" value="none" checked>Administrateurs seulement</label>
                                <label class="radio-import"><input type="radio" id="select_members" name="access" value="select_members">Partager avec</label>
                                <div id="members_access" style="display: none">
                                    @include('utils.inputs.multiples.select', ["name"=>"members_access","collection"=>$members,"values"=>$members_service])
                                </div>
                            @else
                                <label class="radio-import"><input type="radio" id="all" name="access" value="all">Tous les utilisateurs</label>
                                <label class="radio-import"><input type="radio" id="none" name="access" value="none">Administrateurs seulement</label>
                                <label class="radio-import"><input type="radio" id="select_members" name="access" value="select_members" checked>Partager avec</label>
                                <div id="members_access">
                                    @include('utils.inputs.multiples.select', ["name"=>"members_access","collection"=>$members,"values"=>$members_service])
                                </div>
                            @endif
                        @endif
                    @endif
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Choix d'importation des données</h3>
                </div>
                <div id="alert_data_box" class="box-body"></div>
                <div class="box-body type_choice">
                    <label class="radio-import"><input type="radio" id="type1" name="service" value="file"/>Fichier</label>
                    <label class="radio-import"><input type="radio" id="type2" name="service" value="url"/>Url de la source</label>
                    <label class="radio-import"><input type="radio" id="type3" name="service" value="siren"/>Données SIRENE</label>

                    <div class="box-hid" id="hidden1">
                        @include('utils.inputs.file', ["name"=>"fileToUpload","accept"=>".json,.geojson,.csv", "info"=>"Fichier .json, .geojson au format WGS84 ou .csv selon le format précisé dans l'aide"])
                    </div>
                    <div class="box-hid" id="hidden2">
                        @include('utils.inputs.text', ["name"=>"source_url", "value"=>$service_pei->source_url, "info"=>"Ressource .geojson au format WGS84"])
                        @include('utils.inputs.number', ["name"=>"day_between_refresh_url", "value"=>$service_pei->day_between_refresh, "min"=>0])
                    </div>
                    <div class="box-hid" id="hidden3">
                        @include('utils.inputs.sirene', ["name"=>"sirene", "tree" => $activity_tree])
                        @include('utils.inputs.number', ["name"=>"day_between_refresh_siren", "min"=>0, "value"=>$service_pei->day_between_refresh])
                    </div>
                </div>
                <div class="boxfooter" id="type_service">
                </div>
            </div>
            <div id="color-icon" class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Créer une symbologie</h3>
                </div>
                <div id="colorIconContent" class="box-body">
                    <div id="waitingColor">En attente de données</div>

                    <div class="col-md-6 marker">
                        <!-- TO DO -->
                        @include('utils.inputs.color', ["name"=>"color","value"=>$service_pei->color_name])
                    </div>
                    <div class="col-md-6 polygone line">
                        <!-- SET COLOR TYPE WITH GEOJSON TYPE -->
                        @include('utils.inputs.color', ["name"=>"colorOutline","value"=>$service_pei->color_outline_name])
                    </div>
                    <div class="col-md-6 polygone">
                        <!-- SET COLOR TYPE WITH GEOJSON TYPE -->
                        @include('utils.inputs.color', ["name"=>"colorFill","value"=>$service_pei->color_fill_name])
                    </div>
                    <div class="col-md-6 marker">
                            <!-- IF GEOJSON IS POINT -->
                            @include('utils.inputs.ico', ["name"=>"ico","value"=>$service_pei->icon_name,"service"=>$service_pei])
                            {{-- <a data-toggle="modal" data-target="#edit-modal-service" id="search_modal"><button type="submit" class="btn btn-sm btn-primary">Choisir un marqueur existant</button></a> --}}
                            @include('box.services_pei.editModalService')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <button type="submit" class="btn btn-block btn-primary">{{trans("form.btn.submit")}}</button>
        </div>
    </div>
</form>
<script id="service">window.service = {!! json_encode($service_pei) !!}; window.service.availableFields = {!! json_encode($available_fields) !!}</script>
@stop

@push('js')
    <script src="/js/services_pei.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endpush