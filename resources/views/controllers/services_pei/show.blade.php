@extends('adminlte::page')

@section('title', "Proxiclic - Affichage d'un service")

@section('content_header')
    <h1>Affichage d'un service</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link rel="stylesheet" href="/vendor/leaflet-geocoder-ban/leaflet-geocoder-ban.min.css">
    <link rel="stylesheet" href="/vendor/leaflet.markercluster/dist/MarkerCluster.Default.css">
    <link rel="stylesheet" href="/css/app.css"/>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        {{$service_pei['name']}}
                    </h3>
                    @if (!$service_pei['geojson_type'] && $edit_service)
                        <div class="box-tools pull-right">
                            <a class="btn btn-box-tool btn-default" href="{{action('ServicePeiController@edit_peis',[$current_account->id, $service_pei['id']])}}">Modifier les points</a>
                        </div>
                    @endif
                </div>
                <div class="box-body">
                    <div id="maCarte" data-icon="{{ $service_pei }}" data-color="{{ $service_pei['color_name'] }}" data-json="{{$json_addresses}}" style="height: 60vh;" data-map-fields="{{ $service_pei['display_fields'] }}" data-map-selections="{{ $service_pei['display_selection'] }}" data-map-recherches="{{ $service_pei['display_recherche'] }}"></div>
                </div>
                <!-- /.box-body -->
                {{--
                <div class="box-footer">
                    <h4>Intégrer ce service sur votre page web.</h4>
                    <p>Copiez et collez le code ci dessous là où vous souhaitez que la carte du service apparaisse.</p>
                    <figure class="highlight" style="position:relative;">
                        <div class="bs-clipboard">
                            <button type="button" class="btn-clipboard" title="Copier dans le presse papier">
                                Copier
                            </button>
                        </div>
                        <pre style="text-align:center;"><code class="language-html embed-code" data-lang="html">&lt;script src="{{ route('home') }}/js/embed-service.js" data-service-id="{{ $service_pei['id'] }}"&gt;&lt;/script&gt;</code></pre>
                    </figure>
                </div>
                --}}
            </div>
        </div>
    </div>

    @if ((!$url_data_table) === false)
        <div class="row" id="show_points">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Points</h3>
                        <div class="box-tools pull-right">
                            <a class="btn btn-box-tool btn-default" href="{{action('ServicePeiController@edit_peis',[$current_account->id, $service_pei['id']])}}">Modifier les points</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="table_markers_score" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Précision BAN(%)</th>
                                {{--<th>Nom</th> --}}
                                <th>Adresse</th>
                                <th>Adresse BAN</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif

@stop
@push('js')

    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script type="text/javascript" src="/vendor/leaflet-geocoder-ban/leaflet-geocoder-ban.js"></script>
    <script type="text/javascript" src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
    <script src="/js/peis_show.js"></script>
@endpush
