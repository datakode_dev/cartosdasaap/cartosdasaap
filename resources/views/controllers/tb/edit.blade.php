@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un tableau de bord")

@section('content_header')
    <h1>Edition d'un tableau de bord</h1>
@stop
@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tableau de bord</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('TbController@store', [$current_account->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $board->id }}" />
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name", "value" => $board->name])
                        @include('utils.inputs.select_limit', ["name"=>"limit", "url"=>action('API\GeoController@getLimit'), "current" => $current_limit])
                        @include('utils.inputs.checkbox', ["name"=>"load_full","value"=>$board->load_full,"info"=>"Si cocher un calcul sur les communes et les épis sera effectué"])
                        @include('utils.inputs.multiples.select', ["name"=>"layers", "collection"=>$layers, 'values' => $current_layers])
                        @include('utils.inputs.multiples.select', ["name"=>"indicators", "collection"=>$indicators, "values" => $current_indicators])
                        @include('utils.inputs.multiples.tags', ["name"=>"referrers", "values" => explode(',', $board->referrers), "collection" => explode(',', $board->referrers), "info"=>"Saisir les noms de domaine autorisés à afficher le contexte (ex: 'www.datakode.fr,*.proxiclic.fr')."])
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="/js/tb.js"></script>
@endpush
