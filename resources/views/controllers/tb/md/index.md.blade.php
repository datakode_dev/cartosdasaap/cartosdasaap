﻿Consultez l'ensemble des tableaux de bord créés.   
Un nouveau tableau de bord peut être ajouté avec le bouton "Ajouter".   
  
'icon crayon' : modifier un tableau de bord  
'icon oeil' : visualiser un tableau de bord  
'icon dossier' : archiver un tableau de bord  
'icon flèche' : désarchiver un tableau de bord  
'icon poubelle' : supprimer un tableau de bord  
