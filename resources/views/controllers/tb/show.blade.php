@extends('adminlte::page')

@section('title', 'Proxiclic - Tableau de bord : ' . $board->name)

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />

    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@stop
@section('content_header')
    <h1>Tableau de bord</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary" id="board-main" data-id="{{ $board->id }}">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $board->name }}</h3>
                    @if($board->load_full)
                    <div class="box-tools pull-right">
                        <label for="change-limit" class="sr-only">Changement de territoire</label>
                        <select id="change-limit" class="select2_ajax btn-box-tool" data-url="{{ route('API.geo.possibleInseeForBoard', ['account_id' => $current_account->id, 'id' => $board->id]) }}">
                            <option selected value="{{ $board->zone_id }}.{{ $board->insee_limit }}">
                                {{ $board->limit_name }}
                            </option>
                        </select>
                    </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if($board->active === false)
                    <div class="callout callout-warning" id="disabled-message">
                        <h4>Ce tableau de bord est désactivé</h4>

                        <p>Les informations présentées ne sont peut-être pas à jour.</p>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-success box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">{{ $board->description }}</h3>
                                </div>
                                <div class="box-body">
                                    <canvas id='myChart'></canvas>
                                </div>
                            </div>
                            <div class="box box-danger box-solid" id="stats">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Territoires les mieux couverts</h3>
                                </div>
                                <div class="box-body">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box box-default box-solid">
                                <div class="box-body">
                                    <div id='maCarte' class='box col-md-12' style='height: 75vh;'>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <h4>Intégrer ce tableau de bord sur votre page web.</h4>
                    <p>Copiez et collez le code ci dessous là où vous souhaitez que le contexte apparaisse.</p>
                    <figure class="highlight" style="position:relative;">
                        <div class="bs-clipboard">
                            <button type="button" class="btn-clipboard" title="Copier dans le presse papier">
                                Copier
                            </button>
                        </div>
                        <pre style="text-align:center;"><code class="language-html embed-code" data-lang="html">&lt;script src="{{ route('home') }}/js/embed-board.js" data-board-id="{{ $board->id }}"&gt;&lt;/script&gt;</code></pre>
                    </figure>
                </div>
            </div>
        </div>
    </div>

@stop
@push('js')
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
    <script src="/js/board_show.js"></script>
@endpush
