@extends('adminlte::page')

@section('title', 'Proxiclic - Liste des tableaux de bord')

@section('content_header')
    <h1>Liste des tableaux de bord</h1>
@stop
@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tableaux de bord</h3>
                    @if ($can_edit == true)
                        <div class="box-tools pull-right">
                            <a href="{{action('TbController@create',[$current_account->id])}}" class="btn btn-box-tool btn-default">Ajouter</a>
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <table id="table_tb" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Territoire</th>
                                <th>Couches</th>
                                <th>Mise à jour</th>
                                <th>Statut</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@stop

@push('js')
    <script src="/js/tb.js"></script>
@endpush
