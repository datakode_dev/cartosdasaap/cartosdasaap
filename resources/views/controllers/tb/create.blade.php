@extends('adminlte::page')

@section('title', 'Proxiclic - Créer un tableau de bord')

@section('content_header')
    <h1>Créer un tableau de bord</h1>
@stop
@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tableau de bord</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('TbController@store',[$current_account->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        @include('utils.inputs.select_limit', ["name"=>"limit","url"=>action('API\GeoController@getLimit')])
                        @include('utils.inputs.checkbox', ["name"=>"load_full","info"=>"Si coché, un calcul sur les communes et les EPCI sera effectué"])
                        @include('utils.inputs.multiples.select', ["name"=>"layers","collection"=>$layers])
                        @include('utils.inputs.multiples.select', ["name"=>"indicators","collection"=>$indicators])
                        @include('utils.inputs.multiples.tags', ["name"=>"referrers", "values" => [], "collection" => [], "info"=>"Saisir les noms de domaine autorisés à afficher le contexte (ex: 'www.datakode.fr,*.proxiclic.fr')."])
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script src="/js/tb.js"></script>
@endpush
