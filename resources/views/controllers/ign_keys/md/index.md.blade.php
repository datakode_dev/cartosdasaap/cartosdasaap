﻿Consultez l'ensemble des clés IGN créees.  
Ajoutez une nouvelle clé IGN avec le bouton "Ajouter".  

'icon crayon' : modifier une clé IGN  
'icon oeil' : visualiser une clé IGN  

__Tutoriel pour la création de clé IGN:__    
  
Création d’une clé IGN sur le site http://professionnels.ign.fr/ :  
Créez un compte et sélectionnez “commander une clé IGN” dans l’onglet Nos Services.  
Complétez le formulaire :  
- Renseignez le titre de contrat.  
- Sélectionnez “N°1 - Utilisation des Ressources Géoportail - Utilisateur final Grand Public” dans choix du géoservice.  
- Sélectionnez “Moins de 2 000 000 de transactions (GP - gratuit)” dans quantité d’usage.  
- Cochez “Referer”.  
- Renseignez l’adresse du site dans valeur de sécurisation (utilisation de plusieurs sites : séparer les adresses par des virgules).  

Ajoutez votre demande au panier, cochez “Je valide l’ensemble des licences de mon panier” puis finaliser votre commande.   



