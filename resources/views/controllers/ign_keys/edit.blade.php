@extends('adminlte::page')

@section('title', 'Proxiclic - Edition : Clé ' . $ign_key->name)

@section('content_header')
    <h1>Edition : Clé {{$ign_key->name}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Clé</h3>
                </div>

                <!-- form start -->
                <form action="{{action('IgnKeyController@update',[$current_account->id, $ign_key->id])}}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name","value"=>$ign_key->name])
                        @include('utils.inputs.text', ["name"=>"key","value"=>$ign_key->key])
                        @include('utils.inputs.number', ["name"=>"max_count","value"=>$ign_key->max_count])
                        @include('utils.inputs.date', ["name"=>"end_date","value"=>$ign_key->end_date])
                        @include('utils.inputs.radio', ["name"=>"key_type","collection"=>$key_types,"value"=>$ign_key->key_type_name])
                        @include('utils.inputs.text', ["name"=>"ign_login","value"=>$ign_key->login])
                        @include('utils.inputs.password', ["name"=>"ign_password"])
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                        <a href="../" class="btn btn-danger" role="button" aria-pressed="true">Annuler</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
