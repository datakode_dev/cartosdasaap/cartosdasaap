@extends('adminlte::page')

@section('title', "Proxiclic - Ajout d'une clé IGN")

@section('content_header')
    <h1>Ajout d'une clé IGN</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Clé</h3>
                </div>

                <form action="{{action('IgnKeyController@store',[$current_account->id])}}" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        <input type="radio" id="type1" name="key_type" value="1"/>Login/Pass
                        <input type="radio" id="type2" name="key_type" value="2"/>Referer
                        <br><br>
                        <div class="box-hid" id="hidden1">
                            @include('utils.inputs.text', ["name"=>"key"])
                        </div>
                        <div class="box-hid" id="hidden2">
                            @include('utils.inputs.text', ["name"=>"ign_login"])
                            @include('utils.inputs.password', ["name"=>"ign_password"])
                        </div>
                        @include('utils.inputs.text', ["name"=>"max_count"])
                        @include('utils.inputs.date', ["name"=>"end_date"])
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push('js')
        <script src="/js/create_ign_key.js"></script>
    @endpush

@stop