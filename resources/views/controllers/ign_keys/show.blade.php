@extends('adminlte::page')

@section('title', 'Proxiclic - Clé : ' . $ign_key->name)

@section('content_header')
    <h1>Clé : {{$ign_key->name}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Clé</h3>
                    @if ($can_edit == true)
                        <a data-toggle='tooltip' title='Modifier' href='{{action('IgnKeyController@edit',[$current_account->id, $ign_key->id])}}'>
                            <i class='fa fa-pencil box-tools pull-right' aria-hidden='true'></i></a>
                    @endif
                </div>
                <div class="box-body with-border">
                    <b>Nom :</b>{{$ign_key->name}}<br/>
                    <b>Numéro de clé :</b>{{$ign_key->key}}<br/>
                    <b>Type de clé :</b>{{$ign_key->key_type_name}}<br/>
                    <b>Login :</b>{{$ign_key->login}}<br/>
                    <b>Password :</b>{{$ign_key->password}}<br/>
                    <b>Date de fin :</b>{{strftime('%d/%m/%Y',strtotime($ign_key->end_date))}}<br/>
                </div>
            </div>
        </div>
    </div>
@stop
