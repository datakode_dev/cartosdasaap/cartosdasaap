@extends('adminlte::page')

@section('title', 'Proxiclic - Liste des clefs IGN')

@section('content_header')
    <h1>Liste des clés IGN</h1>
@stop
@section('css')
    <link rel="stylesheet" href="/css/app.css"/>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Clés</h3>
                    @if ($can_edit == true)
                        <div class="box-tools pull-right">
                            <a href="{{action('IgnKeyController@create',[$current_account->id])}}" class="btn btn-box-tool btn-default">Ajouter</a>
                        </div>
                    @endif
                </div>
                <div class="box-body">
                    <table id="table_ign_keys" data-url="{{$url_data_table}}" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Type de clé</th>
                                <th>Date de fin</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
@stop

@push('js')
    <script src="/js/ign_keys.js"></script>
@endpush
