@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un marqueur")

@section('content_header')
    <h1>Edition d'un marqueur</h1>
@stop

@section('css')
@stop

@section('content')
<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Marqueur</h3>
                </div>
    <form action="{{action('IcoColorController@update',[$current_account->id,$ico_color->id])}}" method="post">
        <input type="hidden" name="_method" value="put">
        {!! csrf_field() !!}
        <div class="box-body">
                    @include('utils.inputs.text', ["name"=>"marker_name", "value"=>$ico_color->name])
                    @include('utils.inputs.color', ["name"=>"marker_color", "value"=>$color->name])
                    @include('utils.inputs.ico', ["name"=>"marker_icon", "value"=>$icon->name])
        </div>
        
        <!-- /.box-body -->
        <div class="box-footer">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                </div>
            </div>
    </form>
@stop

@push('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
@endpush

