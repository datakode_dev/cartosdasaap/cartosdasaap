@extends('adminlte::page')

@section('title', "Proxiclic - Affichage d'un marqueur")

@section('content_header')
    <h1>Affichage d'un marqueur</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Marqueur</h3>
                    @if ($can_edit == true)
                        <a data-toggle='tooltip' title='Modifier' href='{{action('IcoColorController@edit',[$current_account->id, $ico_color->id])}}'>
                            <i class='fa fa-pencil box-tools pull-right' aria-hidden='true'></i></a>
                    @endif
                </div>
                <div class="box-body with-border">
                    <b>Nom : </b>{{$ico_color->name}}<br/>
                    <b>Couleur : </b><span class="color-sample" style="display: block;width: 25px;height: 25px;background-color:{{ $color->name }};"></span>
                    <b>Icône : </b><br/><span class="glyphicon {{ $icon->name }}"></span><br/>
                </div>
            </div>
        </div>
    </div>
@stop