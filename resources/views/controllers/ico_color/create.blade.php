@extends('adminlte::page')

@section('title', 'Proxiclic - Créer une symbologie')

@section('content_header')
    <h1>Créer une symbologie</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Marqueur</h3>
                </div>
                <form action="{{action('IcoColorController@store',[$current_account->id])}}" method="post">
                {!! csrf_field() !!}
                <div class="box-body">
                    @include('utils.inputs.text', ["name"=>"marker_name"])
                    @include('utils.inputs.color', ["name"=>"marker_color"])
                    @include('utils.inputs.ico', ["name"=>"marker_icon"])
                </div>
                <div class="box-footer">
                    <button type="submit" class="center-block btn btn-primary">{{trans("form.btn.submit")}}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
@endpush
