@extends('adminlte::page')

@section('title', "Proxiclic - Affichage d'un utilisateur")

@section('content_header')
    <h1>Affichage d'un utilisateur</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Utilisateur</h3>
                    @if ($can_edit == true)
                        <a data-toggle='tooltip' title='Modifier' href='{{action('MemberController@edit',[$member->account_id, $member->id])}}'>
                            <i class='fa fa-pencil box-tools pull-right' aria-hidden='true'></i></a>
                    @endif
                </div>
                <div class="box-body with-border">
                    <img src="{{$member_image}}"/><br/>
                    <b>Nom : </b>{{$member->lastname}}<br/>
                    <b>Prénom : </b>{{$member->firstname}}<br/>
                    <b>Email : </b>{{$member->email}}<br/>
                    <b>Rôle : </b>{{$member->name}}<br/>
                    <b>Statut : </b>
                    @if ($member->active)
                        <span class="label label-info">Actif</span>
                    @else
                        <span class="label label-danger">Inactif</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
