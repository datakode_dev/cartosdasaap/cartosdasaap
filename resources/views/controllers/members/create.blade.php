@extends('adminlte::page')

@section('title', "Proxiclic - Ajout d'un membre")

@section('content_header')
    <h1>Ajout d'un membre</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
                <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Utilisateur</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{action('MemberController@store',[$current_account->id])}}" method="post">
                            {!! csrf_field() !!}
                          <div class="box-body">
                                @include('utils.inputs.text', ["name"=>"firstname"])
                                @include('utils.inputs.text', ["name"=>"lastname"])
                                @include('utils.inputs.email', ["name"=>"email"])
                                @include('utils.inputs.select', ["name"=>"role_id","collection"=>$roles])
                          </div>
                          <!-- /.box-body -->
            
                          <div class="box-footer">
                            <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                          </div>
                        </form>
                      </div>
        </div>
    </div>
@stop