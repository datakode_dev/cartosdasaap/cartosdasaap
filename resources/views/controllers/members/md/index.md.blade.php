﻿Les utilisateurs disposant d'un compte sont listés et identifiés par leur nom, prénom et rôle.   
Un nouvel utilisateur peut être ajouté avec le bouton "Ajouter".  
  
'icon crayon' : modifier les informations d'un utilisateur  
'icon oeil' : visualiser les informations de l'utilisateur  
'icon dossier' : désactiver l'utilisateur  
'icon flèche' : activer l'utilisateur  
