@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un utilisateur")

@section('content_header')
    <h1>Edition d'un utilisateur</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Utilisateur</h3>
                </div>

                <!-- form start -->
                <form action="{{action('MemberController@update',[$current_account->id, $member->id])}}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {!! csrf_field() !!}
                    <!-- on affiche le nom, le prenom et le mail ainsi que le choix du role pour un membre -->
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"firstName","value"=>$user->firstname])
                        @include('utils.inputs.text', ["name"=>"lastName","value"=>$user->lastname])
                        @include('utils.inputs.text', ["name"=>"user_email","value"=>$user->email])
                        @include('utils.inputs.select', ["name"=>"role_id","collection"=>$roles,"value"=>$member->role_id])
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
