﻿Consultez la liste des rôles.  
Ajoutez de nouveaux rôles avec le bouton "Ajouter" en gérant les autorisations et modifiez les rôles déjà existants.

Par défaut, tous les utilisateurs ont le droit de consulter tous les services et contextes.
Un utilisateur n'ayant pas le droit de créer ou modifier un service ou un contexte peut modifier un service qui lui est partagé.