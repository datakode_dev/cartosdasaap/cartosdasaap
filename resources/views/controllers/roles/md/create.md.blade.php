﻿Ajoutez un nouveau rôle en lui affectant un nom et des permissions :  

- Gérer les utilisateurs (ajouter, modifier, désactiver des utilisateurs)  
- Gérer les rôles (ajouter, modifier, supprimer des rôles)  
- Ne peut être désactivé (l’utilisateur disposant de cette permission ne peut être désactivé, ex : l’administrateur)  
- Peut créer ou éditer des éléments (créer ou éditer des services, contextes, tableaux de bord, rôles, utilisateurs…)

Par défaut, tous les utilisateurs ont le droit de consulter tous les services et contextes.
Un utilisateur n'ayant pas le droit de créer ou modifier un service ou un contexte peut modifier un service qui lui est partagé.

Le nouveau rôle créé sera visible dans la liste des rôles.  

