﻿Consultez les permissions associées à ce rôle.

Par défaut, tous les utilisateurs ont le droit de consulter tous les services et contextes.
Un utilisateur n'ayant pas le droit de créer ou modifier un service ou un contexte peut modifier un service qui lui est partagé.
