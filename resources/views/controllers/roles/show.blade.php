@extends('adminlte::page')

@section('title', "Proxiclic - Affichage d'un rôle")

@section('content_header')
    <h1>Affichage d'un rôle</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Rôle</h3>
                    @if ($can_edit == true)
                        <a data-toggle='tooltip' title='Modifier' href='{{action('RoleController@edit',[$role->account_id, $role->id])}}'>
                            <i class='fa fa-pencil box-tools pull-right' aria-hidden='true'></i></a>
                    @endif
                </div>
                <div class="box-body with-border">
                    <b>Nom : </b>{{$role->name}}<br/>
                    <b>Permission(s) autorisée(s) pour ce role : </b><br/>
                    @foreach($role_permissions as $role_permission)
                            {{$role_permission->name}}<br/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
