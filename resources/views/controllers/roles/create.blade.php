@extends('adminlte::page')

@section('title', 'Proxiclic - Créer un rôle')

@section('content_header')
    <h1>Créer un rôle</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Rôle</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('RoleController@store',[$current_account->id])}}" method="post">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        @include('utils.inputs.list_checkbox', ["name"=>"permissions","collection"=>$permissions])
                    </div>
                    <!-- /.box-body -->
            
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop