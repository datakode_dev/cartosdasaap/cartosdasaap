@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'un rôle")

@section('content_header')
    <h1>Edition d'un rôle</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Rôle</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('RoleController@update',[$current_account->id, $role->id])}}" method="post">
                    <input type="hidden" name="_method" value="put">
                    {!! csrf_field() !!}
                    <!-- on affiche le nom du role ainsi que toutes les permissions possibles pour ce role -->
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name","value"=>$role->name])
                        @include('utils.inputs.list_checkbox', ["name"=>"permissions","collection"=>$permissions,"value"=>$role_permission])
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@stop
