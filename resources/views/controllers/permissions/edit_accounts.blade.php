@extends('adminlte::page')

@section('title', "Proxiclic - Ajout d'une permission")

@section('content_header')
    <h1>Modifier les comptes d'un utilisateur</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
                <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Autorisations</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{action('PermissionController@storeAccountsUser',[$user->id])}}" method="post">
                            {!! csrf_field() !!}
                          <div class="box-body">
                              <label>Nom : </label> {{ $user['firstname'] }}<br/>
                              <label>Prénom : </label> {{ $user['lastname'] }}<br/>
                              <label>Email : </label> {{ $user['email'] }}
                              @include('utils.inputs.multiples.select', ["name"=>"accounts_id","collection"=>$accounts,"values"=>$accountsMember])
                          </div>
                          <!-- /.box-body -->
            
                          <div class="box-footer">
                            <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                          </div>
                        </form>
                      </div>
        </div>
    </div>
@stop