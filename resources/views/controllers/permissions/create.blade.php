@extends('adminlte::page')

@section('title', "Proxiclic - Ajout d'une permission")

@section('content_header')
    <h1>Ajout d'une permission</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
                <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Autorisation</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{action('PermissionController@store')}}" method="post">
                            {!! csrf_field() !!}
                          <div class="box-body">

                                @include('utils.inputs.text', ["name"=>"name"])
                                @include('utils.inputs.text', ["name"=>"slug"])

                          </div>
                          <!-- /.box-body -->
            
                          <div class="box-footer">
                            <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                          </div>
                        </form>
                      </div>
        </div>
    </div>
@stop