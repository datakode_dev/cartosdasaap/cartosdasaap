@extends('adminlte::page')

@section('title', 'Proxiclic - Liste des utilisateurs')

@section('content_header')
    <h1>Liste des utilisateurs</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
                <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Autorisations</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        
                        <div class="box-body">
                            <table id="users_table_permission" data-url="{{$url_data_table}}" class="table table-bordered table-hover usersDatatable">
                                <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Prénom</th>
                                            <th>Nom</th>
                                            <th>Email</th>
                                            <th>Comptes Associés</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
            
            </div>
        </div>
    </div>
    
@stop

@push('js')
    <script src="/js/permission.js"></script>
@endpush