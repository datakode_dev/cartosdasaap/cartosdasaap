<html>
<head>
    <link href="https://test.geoplateforme17.fr/templates/geoplateforme17/css/template.css" rel="stylesheet">
</head>
<body>
    <div id="show_test">
        <script src="/js/embed-search.js" data-context-id="22"></script>
    </div>
</body>
<script>
    window.addEventListener('proxiclicMap.loaded', function() {
        console.log('map loaded');
        var map = window.proxiclicMap;
        $.get('https://geo.api.gouv.fr/communes/81005?geometry=contour&format=geojson').then(function(res) {
            var layer = L.geoJSON(res);
            map.addLayer(layer);
        });
    });
</script>
</html>

