@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>TEST</h1>
@stop

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link rel="stylesheet" href="../vendor/leaflet-geocoder-ban/leaflet-geocoder-ban.min.css">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop

@section('content')
    <div id="show_test" data-url="{{$url_data_table}}">
    </div>
@stop

@push('js')
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script type="text/javascript" src="../vendor/leaflet-geocoder-ban/leaflet-geocoder-ban.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/js/test.js"></script>
@endpush