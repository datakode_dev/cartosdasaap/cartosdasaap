@extends('adminlte::page')

@section('title', 'Proxiclic - Couche : ' . $layer->name)

@section('content_header')
    <h1>Couche : {{$layer->name}}</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
@stop
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Couche</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
               
                    <div class="box-body" id="map-layer" style="height: 60vh;" data-geo="{{$layer->geo}}">
                  
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        
                    </div>
                
            </div>
        </div>
    </div>

@stop
@push('js')
    <script type="text/javascript" src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script src="/js/layers.js"></script>
@endpush