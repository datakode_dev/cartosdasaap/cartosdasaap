﻿Consultez l'ensemble des couches importées manuellement ou générées par les contextes.  
Le bouton “Ajouter” permet d’importer une nouvelle couche.  
  
'icon crayon' : modifier une couche  
'icon oeil' : visualiser une couche sur la carte  
'icon téléchargement' : Télécharger une couche  
'icon dossier' : archiver une couche  
'icon fichier' : dupliquer un contexte   
'icon fleche' : désarchiver une couche   
'icon poubelle' : supprimer une couche  

  
