@extends('adminlte::page')

@section('title', "Proxiclic - Edition d'une couche")

@section('content_header')
    <h1>Edition d'une couche</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Couche</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('LayerController@update',[$current_account->id,$layer->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                            <input type="hidden" name="_method" value="put">
                        @include('utils.inputs.text', ["name"=>"name","value"=>$layer->name])
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop