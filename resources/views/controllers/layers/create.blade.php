@extends('adminlte::page')

@section('title', 'Proxiclic - Créer une couche')

@section('content_header')
    <h1>Créer une couche</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Couche</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{action('LayerController@store',[$current_account->id])}}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        @include('utils.inputs.text', ["name"=>"name"])
                        @include('utils.inputs.file', ["name"=>"fileToUpload","accept"=>".geojson","info"=>"Ressource .geojson au format WGS84"])
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">{{trans("form.btn.submit")}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop