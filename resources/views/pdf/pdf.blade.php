<!DOCTYPE html>
<html>
<head>
    <title>{{ $name }}</title>
    <link rel="stylesheet" href="{{url('/vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{url('/css/font.css') }}">

<style type="text/css">
    @font-face {
        font-family: 'FontAwesome';
        src: url({{storage_path('fonts/fontawesome-webfont.ttf')}}) format('truetype');
        font-weight: normal;
        font-style: normal;
    }
    .fa {
        display: inline;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 1;
        font-family: FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    body {
        font-family: 'Source Sans Pro',sans-serif;
    }
    header {
        text-align: center;
        font-size: 12px;
        margin-bottom: 20px;
    }
    div.services {
        margin: 20px 0;
    }
    div.service-name {
        /* background : #f7f7f7; */
        padding: 10px 20px;
        font-size: 16px;
        height: 50px;
    }
    .service {
        font-weight: bold;
        font-size:18px;
        text-transform: uppercase;
    }
    .description {
        font-style: italic;
    }
    .points {
        padding: 10px 20px;
    }
    footer {
        position: fixed; 
        bottom: -60px; 
        left: 0px; 
        right: 0px;
    }
    div.services {
        page-break-after: always!important;
    }
    div.services:last-child {
        page-break-after: unset!important;
    }
    .services .icon {
        width: 50px;
        height: 50px;
        float:left;
        margin-right: 20px;
        text-align: center;
    }
    .services .icon i{
        line-height: 38px;
        font-size: 30px;
    }
    .services .icon img {
        margin: 5px;
    }
    .points li {
        list-style-type: none;
        line-height: 1.2;
        padding: 10px 0;
        border-bottom: 1px solid lightgray;
    }
    .points .name {
        font-size: 14px;
        font-weight: bold;

    }
    .points .desc {
        font-size: 12px;
    }
    .points .grey {
        color: #404040;
    }
</style>
</head>

<?php

function pickColor($bgColor) {
    $color = ($bgColor[0] === '#') ? substr($bgColor, 1) : $bgColor;
    $r = intval(substr($color,0, 2), 16); // hexToR
    $g = intval(substr($color, 2, 2), 16); // hexToG
    $b = intval(substr($color, 4, 2), 16); // hexToB
    $uicolors = [$r / 255, $g / 255, $b / 255];

    $c = [];

    foreach($uicolors as $col) {
        if ($col <= 0.03928) {
            $c[] = $col / 12.92;
        } else {
            $c[] = pow(($col + 0.055) / 1.055, 2.4);
        }
    }

    $L = (0.2126 * $c[0]) + (0.7152 * $c[1]) + (0.0722 * $c[2]);
    return ($L > 0.59) ? '#333' : '#fff';
};
        ?>
<body>
    <header>
        <span class="date">{{ date("d-m-Y") }} </span>
        <h3>
        @if(isset($name))
            {{ $name }} - 
        @endif
        {{ $button }}
    </h3>
    </header>
	<div class="container">
        @foreach($services as $service)
        @if (isset($service['points']))
            <div class="services">
                <div class="service-name">

                    <div class="icon" style="background-color : {{ $service['iconColor']['color'] }};">
                        @if ($service['iconColor']['icon_svg'] === 'true' && isset($service['imgData']))
                            <img src="data:image/svg+xml;base64,{{base64_encode($service['imgData'])}}"  width="40" height="40" />
                        @else
                            <i style="color:<?php echo pickColor($service['iconColor']['color']) ?>" class="{{$service['iconColor']['icon']}}"></i>
                        @endif
                    </div>

                    <span class="service">
                        @if(isset($service['name']))
                        {{ $service['name'] }}
                        @endif
                    </span> 
                    <br/>
                    <span class="description">
                    @if(isset($service['info']))
                        {{ $service['info'] }}
                        @endif
                    </span>
                </div>
                <ul class="points">
                @if (isset($service['points']))
                        @foreach($service['points'] as $point)
                        <li>
                            <span class="name">
                                @if(isset($point['name']))
                                    {{ $point['name'] }} 
                                @endif
                            </span> <br/>

                            <span class="desc">
                                @foreach(array_keys($point) as $key)
                                    @if ($key !== 'name' && $key !== 'serviceId' && $key !== 'id' && $key !== 'iconColor')
                                        <span class="grey">{{ $key }} </span>: {{ strip_tags(str_replace('<', ' <', $point[$key])) }} <br/>
                                        <!-- REPLACE TAGS BY A SPACE -->
                                    @endif
                                @endforeach
                            </span>

                        </li>
                        @endforeach
                @endif
                </ul>
            </div>
        @endif
        @endforeach
    </div>
    <!-- FOOTER -->
    <footer>
    </footer>
</body>
</html>
