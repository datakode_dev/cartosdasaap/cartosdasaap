<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">@import url('https://themes.googleusercontent.com/fonts/css?kit=-lTUqgJg2dxbe4D7B5DEICbzagQlgiMMTcfwSOywUzMnp2X6fN-4Vb6D_obrYGgKnVNlHbMyBb_iW8HD63Lm7A');

    .lst-kix_om4skaic2920-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_om4skaic2920-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_b7dczy41r6fp-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_b7dczy41r6fp-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_om4skaic2920-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_b7dczy41r6fp-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_b7dczy41r6fp-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_b7dczy41r6fp-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_b7dczy41r6fp-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_om4skaic2920-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_b7dczy41r6fp-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_b7dczy41r6fp-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_b7dczy41r6fp-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_2l3gtsx2b88y-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_2l3gtsx2b88y-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_2l3gtsx2b88y-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_2l3gtsx2b88y-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_2l3gtsx2b88y-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_2l3gtsx2b88y-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_2l3gtsx2b88y-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_canju575120j-7 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-1 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-6 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-2 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-8 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-0 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-3 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-5 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-2 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-6 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-5 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-3 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-4 {
        list-style-type: none
    }

    .lst-kix_2l3gtsx2b88y-2 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_4lbfa3y5bfx0-4 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-1 {
        list-style-type: none
    }

    ul.lst-kix_4lbfa3y5bfx0-7 {
        list-style-type: none
    }

    ul.lst-kix_canju575120j-0 {
        list-style-type: none
    }

    .lst-kix_2l3gtsx2b88y-3 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_4lbfa3y5bfx0-8 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-2 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-1 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-0 {
        list-style-type: none
    }

    .lst-kix_fkk44nh8d064-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_fkk44nh8d064-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_fkk44nh8d064-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_bdfpaeahaj5a-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_bdfpaeahaj5a-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_bdfpaeahaj5a-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_fkk44nh8d064-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_bdfpaeahaj5a-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_fkk44nh8d064-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_fkk44nh8d064-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_fkk44nh8d064-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_fkk44nh8d064-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_fkk44nh8d064-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_om4skaic2920-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_om4skaic2920-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_om4skaic2920-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_om4skaic2920-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_om4skaic2920-3 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_tuq1e1vdi6iv-1 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-0 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-3 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-2 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-8 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-5 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-4 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-7 {
        list-style-type: none
    }

    ul.lst-kix_tuq1e1vdi6iv-6 {
        list-style-type: none
    }

    .lst-kix_u8q75b5w7sqn-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_9txmhj2rgl1j-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_u8q75b5w7sqn-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_bdfpaeahaj5a-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_9txmhj2rgl1j-3 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_2l3gtsx2b88y-8 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-0 {
        list-style-type: none
    }

    ul.lst-kix_2l3gtsx2b88y-7 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-1 {
        list-style-type: none
    }

    ul.lst-kix_2l3gtsx2b88y-4 {
        list-style-type: none
    }

    ul.lst-kix_2l3gtsx2b88y-3 {
        list-style-type: none
    }

    ul.lst-kix_2l3gtsx2b88y-6 {
        list-style-type: none
    }

    .lst-kix_bdfpaeahaj5a-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_2l3gtsx2b88y-5 {
        list-style-type: none
    }

    .lst-kix_9txmhj2rgl1j-5 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_rgleluijlwoc-8 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-6 {
        list-style-type: none
    }

    .lst-kix_u8q75b5w7sqn-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_b7dczy41r6fp-5 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-6 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-4 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-7 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-3 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-4 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-5 {
        list-style-type: none
    }

    ul.lst-kix_rgleluijlwoc-2 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-8 {
        list-style-type: none
    }

    .lst-kix_9txmhj2rgl1j-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_rgleluijlwoc-3 {
        list-style-type: none
    }

    ul.lst-kix_b7dczy41r6fp-7 {
        list-style-type: none
    }

    .lst-kix_gmm4laefsmo4-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_gmm4laefsmo4-8 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_2l3gtsx2b88y-0 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-6 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-5 {
        list-style-type: none
    }

    ul.lst-kix_2l3gtsx2b88y-2 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-4 {
        list-style-type: none
    }

    .lst-kix_canju575120j-1 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_2l3gtsx2b88y-1 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-3 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-2 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-1 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-0 {
        list-style-type: none
    }

    .lst-kix_gmm4laefsmo4-6 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_u8q75b5w7sqn-2 {
        list-style-type: none
    }

    ul.lst-kix_u8q75b5w7sqn-3 {
        list-style-type: none
    }

    ul.lst-kix_u8q75b5w7sqn-0 {
        list-style-type: none
    }

    .lst-kix_canju575120j-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_canju575120j-5 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_u8q75b5w7sqn-1 {
        list-style-type: none
    }

    ul.lst-kix_u8q75b5w7sqn-6 {
        list-style-type: none
    }

    ul.lst-kix_u8q75b5w7sqn-7 {
        list-style-type: none
    }

    ul.lst-kix_u8q75b5w7sqn-4 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-8 {
        list-style-type: none
    }

    .lst-kix_gmm4laefsmo4-0 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_u8q75b5w7sqn-5 {
        list-style-type: none
    }

    ul.lst-kix_ppweet9nsq8r-7 {
        list-style-type: none
    }

    ul.lst-kix_u8q75b5w7sqn-8 {
        list-style-type: none
    }

    .lst-kix_u8q75b5w7sqn-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_gmm4laefsmo4-2 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_n4dyqjaeqfzg-0 {
        list-style-type: none
    }

    .lst-kix_4dzusxkuxvr6-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_n4dyqjaeqfzg-2 {
        list-style-type: none
    }

    .lst-kix_4dzusxkuxvr6-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_4dzusxkuxvr6-8 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_gmm4laefsmo4-7 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-1 {
        list-style-type: none
    }

    ul.lst-kix_gmm4laefsmo4-8 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-4 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-3 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-6 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-5 {
        list-style-type: none
    }

    .lst-kix_4dzusxkuxvr6-3 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_gmm4laefsmo4-1 {
        list-style-type: none
    }

    .lst-kix_tuq1e1vdi6iv-1 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_gmm4laefsmo4-2 {
        list-style-type: none
    }

    .lst-kix_4dzusxkuxvr6-2 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_gmm4laefsmo4-0 {
        list-style-type: none
    }

    .lst-kix_4dzusxkuxvr6-1 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_gmm4laefsmo4-5 {
        list-style-type: none
    }

    ul.lst-kix_gmm4laefsmo4-6 {
        list-style-type: none
    }

    ul.lst-kix_gmm4laefsmo4-3 {
        list-style-type: none
    }

    .lst-kix_tuq1e1vdi6iv-0 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_9txmhj2rgl1j-0 {
        list-style-type: none
    }

    .lst-kix_rgleluijlwoc-8 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_gmm4laefsmo4-4 {
        list-style-type: none
    }

    ul.lst-kix_9txmhj2rgl1j-1 {
        list-style-type: none
    }

    .lst-kix_tuq1e1vdi6iv-5 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_9txmhj2rgl1j-2 {
        list-style-type: none
    }

    ul.lst-kix_9txmhj2rgl1j-3 {
        list-style-type: none
    }

    ul.lst-kix_9txmhj2rgl1j-4 {
        list-style-type: none
    }

    .lst-kix_8qa575syfy85-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_8qa575syfy85-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_9txmhj2rgl1j-5 {
        list-style-type: none
    }

    ul.lst-kix_9txmhj2rgl1j-6 {
        list-style-type: none
    }

    .lst-kix_4lbfa3y5bfx0-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_4lbfa3y5bfx0-8 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_9txmhj2rgl1j-7 {
        list-style-type: none
    }

    .lst-kix_tuq1e1vdi6iv-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_tuq1e1vdi6iv-6 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_9txmhj2rgl1j-8 {
        list-style-type: none
    }

    .lst-kix_8qa575syfy85-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_8qa575syfy85-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_4lbfa3y5bfx0-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_4dzusxkuxvr6-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_4lbfa3y5bfx0-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_4dzusxkuxvr6-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_tuq1e1vdi6iv-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_tuq1e1vdi6iv-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_8qa575syfy85-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_ppweet9nsq8r-5 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_8qa575syfy85-0 {
        list-style-type: none
    }

    .lst-kix_4lbfa3y5bfx0-2 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_8qa575syfy85-1 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-4 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_8qa575syfy85-2 {
        list-style-type: none
    }

    .lst-kix_4lbfa3y5bfx0-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_4lbfa3y5bfx0-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_rgleluijlwoc-1 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_8qa575syfy85-3 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-3 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_8qa575syfy85-4 {
        list-style-type: none
    }

    .lst-kix_4lbfa3y5bfx0-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_4lbfa3y5bfx0-4 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_8qa575syfy85-5 {
        list-style-type: none
    }

    ul.lst-kix_8qa575syfy85-6 {
        list-style-type: none
    }

    .lst-kix_rgleluijlwoc-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_8qa575syfy85-0 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_8qa575syfy85-7 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-1 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_8qa575syfy85-8 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_ppweet9nsq8r-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_canju575120j-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_8qa575syfy85-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_8qa575syfy85-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_tuq1e1vdi6iv-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_tuq1e1vdi6iv-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_canju575120j-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_rgleluijlwoc-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_8qa575syfy85-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_4dzusxkuxvr6-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_rgleluijlwoc-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_rgleluijlwoc-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_rgleluijlwoc-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_rgleluijlwoc-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_rgleluijlwoc-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_n4dyqjaeqfzg-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_wsdzdgg5vsbl-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_n4dyqjaeqfzg-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_n4dyqjaeqfzg-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_n4dyqjaeqfzg-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_n4dyqjaeqfzg-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_wsdzdgg5vsbl-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_wsdzdgg5vsbl-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_n4dyqjaeqfzg-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_n4dyqjaeqfzg-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_wsdzdgg5vsbl-7 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_wsdzdgg5vsbl-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_n4dyqjaeqfzg-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_wsdzdgg5vsbl-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_wsdzdgg5vsbl-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_n4dyqjaeqfzg-5 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_bdfpaeahaj5a-4 {
        list-style-type: none
    }

    ul.lst-kix_bdfpaeahaj5a-3 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-6 > li:before {
        content: "\0025cf  "
    }

    ul.lst-kix_bdfpaeahaj5a-2 {
        list-style-type: none
    }

    ul.lst-kix_bdfpaeahaj5a-1 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_bdfpaeahaj5a-0 {
        list-style-type: none
    }

    .lst-kix_ppweet9nsq8r-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_wsdzdgg5vsbl-5 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_bdfpaeahaj5a-8 {
        list-style-type: none
    }

    ul.lst-kix_bdfpaeahaj5a-7 {
        list-style-type: none
    }

    .lst-kix_wsdzdgg5vsbl-4 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_bdfpaeahaj5a-6 {
        list-style-type: none
    }

    ul.lst-kix_bdfpaeahaj5a-5 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-1 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-2 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-3 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-4 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-0 {
        list-style-type: none
    }

    ul.lst-kix_wsdzdgg5vsbl-8 {
        list-style-type: none
    }

    .lst-kix_u8q75b5w7sqn-2 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_wsdzdgg5vsbl-7 {
        list-style-type: none
    }

    .lst-kix_9txmhj2rgl1j-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_u8q75b5w7sqn-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_9txmhj2rgl1j-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_9txmhj2rgl1j-4 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_4dzusxkuxvr6-5 {
        list-style-type: none
    }

    .lst-kix_bdfpaeahaj5a-4 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_u8q75b5w7sqn-4 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_4dzusxkuxvr6-6 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-7 {
        list-style-type: none
    }

    ul.lst-kix_4dzusxkuxvr6-8 {
        list-style-type: none
    }

    .lst-kix_bdfpaeahaj5a-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_9txmhj2rgl1j-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_9txmhj2rgl1j-8 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_wsdzdgg5vsbl-0 {
        list-style-type: none
    }

    ul.lst-kix_wsdzdgg5vsbl-2 {
        list-style-type: none
    }

    ul.lst-kix_wsdzdgg5vsbl-1 {
        list-style-type: none
    }

    ul.lst-kix_wsdzdgg5vsbl-4 {
        list-style-type: none
    }

    .lst-kix_bdfpaeahaj5a-8 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_u8q75b5w7sqn-8 > li:before {
        content: "\0025a0  "
    }

    ul.lst-kix_wsdzdgg5vsbl-3 {
        list-style-type: none
    }

    ul.lst-kix_wsdzdgg5vsbl-6 {
        list-style-type: none
    }

    ul.lst-kix_wsdzdgg5vsbl-5 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-2 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-1 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-0 {
        list-style-type: none
    }

    .lst-kix_gmm4laefsmo4-5 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_gmm4laefsmo4-7 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_fkk44nh8d064-6 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-5 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-4 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-3 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-8 {
        list-style-type: none
    }

    ul.lst-kix_fkk44nh8d064-7 {
        list-style-type: none
    }

    .lst-kix_canju575120j-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_gmm4laefsmo4-1 > li:before {
        content: "\0025cb  "
    }

    .lst-kix_canju575120j-2 > li:before {
        content: "\0025a0  "
    }

    .lst-kix_canju575120j-6 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_gmm4laefsmo4-3 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_u8q75b5w7sqn-0 > li:before {
        content: "\0025cf  "
    }

    .lst-kix_canju575120j-4 > li:before {
        content: "\0025cb  "
    }

    ul.lst-kix_om4skaic2920-7 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-8 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-0 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-1 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-2 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-3 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-4 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-5 {
        list-style-type: none
    }

    ul.lst-kix_om4skaic2920-6 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-8 {
        list-style-type: none
    }

    ul.lst-kix_n4dyqjaeqfzg-7 {
        list-style-type: none
    }

    ol {
        margin: 0;
        padding: 0
    }

    table td, table th {
        padding: 0
    }

    .c15 {
        border-right-style: solid;
        padding: 5pt 5pt 5pt 5pt;
        border-bottom-color: #1c1b1c;
        border-top-width: 0pt;
        border-right-width: 0pt;
        border-left-color: #1c1b1c;
        vertical-align: top;
        border-right-color: #1c1b1c;
        border-left-width: 0pt;
        border-top-style: solid;
        border-left-style: solid;
        border-bottom-width: 0pt;
        width: 157.5pt;
        border-top-color: #1c1b1c;
        border-bottom-style: solid
    }

    .c24 {
        border-right-style: solid;
        padding: 5pt 5pt 5pt 5pt;
        border-bottom-color: #1c1b1c;
        border-top-width: 0pt;
        border-right-width: 0pt;
        border-left-color: #1c1b1c;
        vertical-align: top;
        border-right-color: #1c1b1c;
        border-left-width: 0pt;
        border-top-style: solid;
        border-left-style: solid;
        border-bottom-width: 0pt;
        width: 235.5pt;
        border-top-color: #1c1b1c;
        border-bottom-style: solid
    }

    .c23 {
        border-right-style: solid;
        padding: 5pt 5pt 5pt 5pt;
        border-bottom-color: #1c1b1c;
        border-top-width: 0pt;
        border-right-width: 0pt;
        border-left-color: #1c1b1c;
        vertical-align: top;
        border-right-color: #1c1b1c;
        border-left-width: 0pt;
        border-top-style: solid;
        border-left-style: solid;
        border-bottom-width: 0pt;
        width: 82.5pt;
        border-top-color: #1c1b1c;
        border-bottom-style: solid
    }

    .c0 {
        background-color: #fefefe;
        margin-left: 36pt;
        padding-top: 9pt;
        padding-left: 0pt;
        padding-bottom: 9pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .c4 {
        background-color: #fefefe;
        padding-top: 18pt;
        padding-bottom: 4pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .c7 {
        color: #1c1b1c;
        font-weight: 700;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 17pt;
        font-family: "Montserrat";
        font-style: normal
    }

    .c26 {
        -webkit-text-decoration-skip: none;
        color: #1155cc;
        font-weight: 400;
        text-decoration: underline;
        text-decoration-skip-ink: none;
        font-size: 9pt;
        font-family: "Roboto"
    }

    .c3 {
        color: #1c1b1c;
        font-weight: 400;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 9pt;
        font-family: "Roboto";
        font-style: normal
    }

    .c8 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: left;
        height: 11pt
    }

    .c2 {
        background-color: #fefefe;
        padding-top: 9pt;
        padding-bottom: 9pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .c9 {
        color: #000000;
        font-weight: 400;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 11pt;
        font-family: "Arial";
        font-style: normal
    }

    .c6 {
        padding-top: 14pt;
        padding-bottom: 4pt;
        line-height: 1.15;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .c10 {
        font-size: 9pt;
        font-family: "Roboto";
        color: #1c1b1c;
        font-weight: 400
    }

    .c18 {
        font-size: 9pt;
        font-family: "Roboto";
        color: #1c1b1c;
        font-weight: 400
    }

    .c12 {
        font-size: 9pt;
        font-family: "Roboto";
        color: #1c1b1c;
        font-weight: 700
    }

    .c16 {
        padding-top: 12pt;
        padding-bottom: 2pt;
        line-height: 1.15;
        text-align: left
    }

    .c11 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        text-align: left
    }

    .c28 {
        border-spacing: 0;
        border-collapse: collapse;
        margin-right: auto
    }

    .c13 {
        padding-top: 0pt;
        padding-bottom: 0pt;
        line-height: 1.15;
        text-align: center
    }

    .c19 {
        color: #666666;
        font-weight: 400;
        font-size: 12pt;
        font-family: "Arial"
    }

    .c22 {
        color: #1c1b1c;
        font-weight: 400;
        font-size: 13pt;
        font-family: "Montserrat"
    }

    .c27 {
        background-color: #ffffff;
        max-width: 496.1pt;
        padding: 85pt 42.5pt 85pt 56.7pt
    }

    .c17 {
        text-decoration: none;
        vertical-align: baseline;
        font-style: normal
    }

    .c20 {
        color: inherit;
        text-decoration: inherit
    }

    .c1 {
        padding: 0;
        margin: 0
    }

    .c14 {
        orphans: 2;
        widows: 2
    }

    .c21 {
        height: 42.2pt
    }

    .c29 {
        height: 52.8pt
    }

    .c25 {
        height: 37pt
    }

    .c5 {
        background-color: #fefefe
    }

    .title {
        padding-top: 0pt;
        color: #000000;
        font-size: 26pt;
        padding-bottom: 3pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    .subtitle {
        padding-top: 0pt;
        color: #666666;
        font-size: 15pt;
        padding-bottom: 16pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    li {
        color: #000000;
        font-size: 11pt;
        font-family: "Arial"
    }

    p {
        margin: 0;
        color: #000000;
        font-size: 11pt;
        font-family: "Arial"
    }

    h1 {
        padding-top: 20pt;
        color: #000000;
        font-size: 20pt;
        padding-bottom: 6pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h2 {
        padding-top: 18pt;
        color: #000000;
        font-size: 16pt;
        padding-bottom: 6pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h3 {
        padding-top: 16pt;
        color: #434343;
        font-size: 14pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h4 {
        padding-top: 14pt;
        color: #666666;
        font-size: 12pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h5 {
        padding-top: 12pt;
        color: #666666;
        font-size: 11pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        orphans: 2;
        widows: 2;
        text-align: left
    }

    h6 {
        padding-top: 12pt;
        color: #666666;
        font-size: 11pt;
        padding-bottom: 4pt;
        font-family: "Arial";
        line-height: 1.15;
        page-break-after: avoid;
        font-style: italic;
        orphans: 2;
        widows: 2;
        text-align: left
    }</style>
</head>
<h1><span class="c7">Politique de confidentialité des données personnelles</span></h1>
<body class="c27"><p class="c11 c14"><span class="c3 c5">Derni&egrave;re mise &agrave; jour : 29/12/2020</span></p>
<p class="c8"><span class="c3 c5"></span></p>
<h2 id="h.k4rl3ojo21lh" class="c4"><span class="c7">Pr&eacute;ambule</span></h2>
<p class="c2"><span class="c18">La pr&eacute;sente charte d&rsquo;utilisation des donn&eacute;es personnelles s&rsquo;applique au Site internet </span><span
        class="c26"><a class="c20"
                       href="https://www.google.com/url?q=http://www.proxiclic.fr&amp;sa=D&amp;ust=1609846089380000&amp;usg=AOvVaw39Sam054iaceSCQR55BpHy">www.proxiclic.fr</a></span><span
        class="c3">&nbsp;propos&eacute; par le DATAKODE. Elle a pour objectif de vous informer sur :</span></p>
<ul class="c1 lst-kix_u8q75b5w7sqn-0 start">
    <li class="c0"><span class="c3">La mani&egrave;re dont nous pouvons le cas &eacute;ch&eacute;ant collecter et traiter vos donn&eacute;es &agrave; caract&egrave;re personnel ;</span>
    </li>
    <li class="c0"><span class="c3">Les droits dont vous disposez concernant vos propres donn&eacute;es ;</span></li>
    <li class="c0"><span class="c3">L&rsquo;identit&eacute; du responsable de traitement de vos donn&eacute;es ;</span>
    </li>
    <li class="c0"><span class="c3">Le transfert de vos donn&eacute;es vers des tiers ;</span></li>
    <li class="c0"><span class="c3">Notre politique en mati&egrave;re de gestion des &laquo; cookies &raquo;.</span>
    </li>
</ul>
<p class="c2"><span class="c3">Nous nous engageons &agrave; traiter l&rsquo;ensemble des donn&eacute;es collect&eacute;es de mani&egrave;re conforme :</span>
</p>
<ul class="c1 lst-kix_9txmhj2rgl1j-0 start">
    <li class="c0"><span class="c3">Au R&egrave;glement (UE) 2016/679 du Parlement europ&eacute;en et du Conseil du 27 avril 2016 relatif &agrave; la protection des personnes physiques &agrave; l&#39;&eacute;gard du traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es ;</span>
    </li>
    <li class="c0"><span class="c3">A la loi n&deg;78-17 du 6 janvier 1978 dite loi &laquo; Informatique et Libert&eacute;s &raquo; modifi&eacute;e par la loi n&deg; 2018-493 du 20 juin 2018 relative &agrave; la protection des donn&eacute;es personnelles.</span>
    </li>
</ul>
<h2 id="h.h02sq3oa5lc" class="c4"><span class="c7">ARTICLE 1 : LE RESPONSABLE DE TRAITEMENT DES DONN&Eacute;ES</span>
</h2>
<p class="c2"><span class="c3">L&#39;identit&eacute; et les coordonn&eacute;es du Responsable du traitement sont les suivantes :</span>
</p>
<p class="c2"><span class="c3">La soci&eacute;t&eacute; DATAKODE, Soci&eacute;t&eacute; par actions simplifi&eacute;e, &nbsp;dont le si&egrave;ge social est situ&eacute; 22, route de Toulouse 31190 AUTERIVE, dont le repr&eacute;sentant l&eacute;gal est Monsieur Laurent Plainecassagne, en qualit&eacute; de G&eacute;rant.</span>
</p>
<p class="c2"><span
        class="c3">Contact par l&rsquo;adresse postale ou via l&#39;adresse de courriel contact@datakode.fr</span></p>
<h2 id="h.nc897j36ketm" class="c4"><span class="c7">ARTICLE 2 : LES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL COLLECT&Eacute;ES ET TRAIT&Eacute;ES</span>
</h2>
<p class="c2"><span class="c3">Dans le cadre de l&rsquo;exploitation de notre Site www.proxiclic.fr, nous collectons des donn&eacute;es &agrave; caract&egrave;re personnel relatives aux utilisateurs.</span>
</p>
<h3 id="h.yrfjl28keid9" class="c6 c5"><span class="c17 c22">2.1 LES DONN&Eacute;ES G&Eacute;N&Eacute;RALES SONT COLLECT&Eacute;ES POUR LE SITE INTERNET WWW.PROXICLIC.FR :</span>
</h3>
<ul class="c1 lst-kix_rgleluijlwoc-0 start">
    <li class="c0"><span class="c3">Lors de l&rsquo;inscription, de la cr&eacute;ation et de la mise &agrave; jour de votre Compte si vous en disposez ;</span>
    </li>
    <li class="c0"><span class="c3">Lors d&rsquo;&eacute;changes avec nous (demande de contact ou service client)</span>
    </li>
    <li class="c0"><span class="c3">Lorsque vous naviguez sur notre Site et que vous avez autoris&eacute; la collecte de vos donn&eacute;es par le biais des cookies</span>
    </li>
</ul>
<p class="c2"><span class="c3">Lorsque cela est n&eacute;cessaire au regard de la r&eacute;glementation applicable, nous nous engageons &agrave; recueillir le consentement expr&egrave;s, sp&eacute;cifique et pr&eacute;alable des utilisateurs du Site et/ou &agrave; permettre de s&rsquo;opposer &agrave; l&rsquo;utilisation de leurs donn&eacute;es pour certaines finalit&eacute;s. Tout utilisateur dispose &agrave; ce titre, du droit de retirer &agrave; tout moment son consentement.</span>
</p>
<h3 id="h.ao3osd7nkg1x" class="c6 c5"><span class="c22 c17">2.2 NOUS POUVONS &Ecirc;TRE AMEN&Eacute;S &Agrave; TRAITER LES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL SUIVANTES CONFORM&Eacute;MENT AUX FINALIT&Eacute;S D&Eacute;CRITES &Agrave; L&rsquo;ARTICLE 3 DE LA PR&Eacute;SENTE CHARTE :</span>
</h3>
<p class="c8"><span class="c9"></span></p><h4 id="h.m7u83dyihccm" class="c14 c5 c16"><span class="c12">2.2.1 Pour ce qui concerne la gestion de notre Site lorsque vous cr&eacute;ez un Compte et/ou utilisez les diff&eacute;rentes fonctionnalit&eacute;s du Site, les donn&eacute;es collect&eacute;es sont les suivantes :</span>
</h4>
<ul class="c1 lst-kix_2l3gtsx2b88y-0 start">
    <li class="c0"><span class="c3">Donn&eacute;es d&rsquo;identification obligatoires &agrave; la cr&eacute;ation d&rsquo;un Compte et/ou &agrave; la passation d&rsquo;une Commande comprenant : nom, pr&eacute;nom, e-mail et le mot de passe.</span>
    </li>
    <li class="c0"><span class="c3">Les donn&eacute;es int&eacute;gr&eacute;es par les utilisateurs qui peuvent contenir des donn&eacute;es personnelles</span>
    </li>
    <li class="c0"><span class="c3">Informations de navigation comprenant l&rsquo;historique des consultations, adresses IP, param&egrave;tres de navigateur.</span>
    </li>
</ul>
<h4 id="h.gplg7843lsjy" class="c16 c14 c5"><span class="c12 c17">2.2.2 Pour ce qui concerne l&rsquo;interaction avec notre Site, certaines donn&eacute;es sont automatiquement collect&eacute;es depuis votre appareil ou votre navigateur internet. &nbsp;Les informations sur ces pratiques sont pr&eacute;cis&eacute;es &agrave; l&rsquo;article 9 &laquo; Cookies &raquo; ci-apr&egrave;s. Ces donn&eacute;es incluent l&rsquo;adresse IP et les cookies.</span>
</h4>
<p class="c8"><span class="c9"></span></p>
<h2 id="h.yxwxmkvrc1ik" class="c4"><span class="c7">ARTICLE 3 : LES FINALIT&Eacute;S DES TRAITEMENTS</span></h2>
<p class="c2"><span class="c3">Nous collectons et/ou traitons vos donn&eacute;es personnelles dans le respect des lois sur la protection des donn&eacute;es europ&eacute;ennes et fran&ccedil;aises, si n&eacute;cessaire :</span>
</p>
<ul class="c1 lst-kix_b7dczy41r6fp-0 start">
    <li class="c0"><span class="c3">Pour identifier des personnes utilisant notre Site</span></li>
    <li class="c0"><span class="c3">Pour traiter vos Donn&eacute;es</span></li>
    <li class="c0"><span class="c3">Pour vous permettre de vous connecter &agrave; votre Compte</span></li>
    <li class="c0"><span
            class="c3">Pour se conformer &agrave; la loi applicable ou toute autre obligation l&eacute;gale</span></li>
    <li class="c0"><span class="c3">Pour prot&eacute;ger la s&eacute;curit&eacute; des syst&egrave;mes, pour d&eacute;tecter et pr&eacute;venir la fraude, ou veiller &agrave; nos int&eacute;r&ecirc;ts l&eacute;gitimes, sans m&eacute;conna&icirc;tre pour autant vos int&eacute;r&ecirc;ts ou vos libert&eacute;s et droits fondamentaux exigeant une protection accrue de vos donn&eacute;es &agrave; caract&egrave;re personnel.</span>
    </li>
</ul>
<p class="c2"><span class="c3">Vous disposez &agrave; tout moment du droit de retirer votre consentement donn&eacute;, ou de vous opposer &agrave; tout traitement de vos donn&eacute;es personnelles collect&eacute;es.</span>
</p>
<p class="c2"><span class="c3">La collecte et le traitement des donn&eacute;es r&eacute;pondent ainsi aux finalit&eacute;s suivantes :</span>
</p>
<ul class="c1 lst-kix_8qa575syfy85-0 start">
    <li class="c0"><span class="c3">Acc&eacute;der &agrave; notre Site, charger et traiter vos Donn&eacute;es</span>
    </li>
</ul>
<p class="c2"><span class="c3">Lorsque vous acc&eacute;dez &agrave; notre Site, nous collectons et utilisons les informations personnelles communiqu&eacute;es pour vous permettre de charger une Donn&eacute;e et nous permettre de la traiter.</span>
</p>
<ul class="c1 lst-kix_tuq1e1vdi6iv-0 start">
    <li class="c0"><span class="c3">Faire fonctionner, maintenir et am&eacute;liorer les fonctionnalit&eacute;s de notre Site notamment pour le chargement et le traitement de Donn&eacute;es</span>
    </li>
</ul>
<p class="c2"><span class="c3">Les donn&eacute;es personnelles que vous communiquez sont utilis&eacute;es pour faire fonctionner nos traitements. Elles permettent &eacute;galement de d&eacute;tecter des probl&egrave;mes techniques ou de traitement de Donn&eacute;es et administrer notre Site. Les donn&eacute;es personnelles que vous chargez vous m&ecirc;me sur notre Site doivent elles m&ecirc;mes respecter la r&eacute;glementation sur les donn&eacute;es personnelles. Datakode ne pourra en aucun cas &ecirc;tre tenu responsable des donn&eacute;es que vous chargez sur le Site.</span>
</p>
<ul class="c1 lst-kix_om4skaic2920-0 start">
    <li class="c0"><span class="c3">Pour se conformer aux obligations l&eacute;gales, pr&eacute;venir ou d&eacute;tecter des fraudes, abus, utilisations illicites, violations des Conditions G&eacute;n&eacute;rales de Vente, et pour se conformer &agrave; des d&eacute;cisions de justice et requ&ecirc;tes gouvernementales.</span>
    </li>
</ul>
<h2 id="h.i669pwmsntle" class="c4"><span class="c7">ARTICLE 4 : CONSENTEMENT</span></h2>
<p class="c2"><span class="c3">Lors de la cr&eacute;ation d&rsquo;un Compte sur notre Site et/ou lors du chargement de Donn&eacute;es, vous &ecirc;tes amen&eacute; &agrave; remplir des formulaires et communiquer des donn&eacute;es &agrave; caract&egrave;re personnel afin que vous puissiez cr&eacute;er votre Compte et/ou charger une Donn&eacute;e.</span>
</p>
<p class="c2"><span class="c3">Nous nous engageons, lorsque la r&eacute;glementation applicable en vigueur l&rsquo;exige, &agrave; recueillir votre consentement explicite, sp&eacute;cifique et pr&eacute;alable, et/ou &agrave; vous permettre de vous opposer &agrave; l&rsquo;utilisation de vos donn&eacute;es &agrave; caract&egrave;re personnel pour certaines finalit&eacute;s, et/ou &agrave; acc&eacute;der et/ou &agrave; rectifier des informations le concernant.</span>
</p>
<h2 id="h.l9qvx9ujy7ur" class="c4"><span class="c7">ARTICLE 5 : TRANSMISSION DES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL &Agrave; DES TIERS</span>
</h2>
<p class="c2"><span class="c3">Les donn&eacute;es &agrave; caract&egrave;re personnel que nous avons collect&eacute;es sont susceptibles d&rsquo;&ecirc;tre transmises &agrave; des soci&eacute;t&eacute;s tierces dans le cadre de contrats de sous-traitance sign&eacute;s avec nous exclusivement pour les besoins des finalit&eacute;s indiqu&eacute;es &agrave; l&rsquo;article 3. Pr&eacute;alablement aux transferts, nous prendrons toutes les mesures et garanties n&eacute;cessaires pour s&eacute;curiser de tels transferts.</span>
</p>
<p class="c2"><span class="c3">Des transferts peuvent ainsi &ecirc;tre r&eacute;alis&eacute;s dans le cadre des activit&eacute;s et &eacute;v&eacute;nements suivants : prestations informatiques, recherches et statistiques.</span>
</p>
<p class="c2"><span class="c3">Nous ne transmettons aucune donn&eacute;e &agrave; caract&egrave;re personnel &agrave; des partenaires &agrave; des fins d&rsquo;op&eacute;rations commerciales ou &agrave; toutes autres fins qui ne r&eacute;pondraient pas aux besoins des finalit&eacute;s indiqu&eacute;es &agrave; l&rsquo;article 3. Dans l&rsquo;hypoth&egrave;se o&ugrave; nous envisagerions de modifier notre politique de transfert de donn&eacute;es en vue d&rsquo;effectuer des transferts &agrave; des partenaires notamment commerciaux, ceci pourra se faire exclusivement sous r&eacute;serve d&rsquo;avoir obtenu votre consentement explicite et pr&eacute;alable.</span>
</p>
<h2 id="h.41670amr4rt4" class="c4"><span class="c7">ARTICLE 6 : H&Eacute;BERGEMENT ET TRANSFERT DES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL</span>
</h2>
<p class="c2"><span class="c3">Les donn&eacute;es &agrave; caract&egrave;re personnel collect&eacute;es et trait&eacute;es sont h&eacute;berg&eacute;es par tout prestataire de service h&eacute;bergeant nos serveurs en France ou sur tout autre territoire europ&eacute;en.</span>
</p>
<p class="c2"><span class="c3">Le Client est inform&eacute; que nous pouvons, le cas &eacute;ch&eacute;ant, effectuer un transfert de vos donn&eacute;es &agrave; caract&egrave;re personnel vers un pays tiers ou &agrave; une organisation internationale faisant l&#39;objet d&#39;une d&eacute;cision d&#39;ad&eacute;quation rendue par la commission europ&eacute;enne &eacute;tant pr&eacute;cis&eacute; que, en pr&eacute;sence d&#39;un transfert vers un pays ou une organisation internationale ne faisant pas l&#39;objet d&#39;une d&eacute;cision d&#39;ad&eacute;quation, alors cela ne pourra &ecirc;tre effectu&eacute; qu&#39;&agrave; la condition que soient mises en place les garanties appropri&eacute;es et que les utilisateurs concern&eacute;s disposent de droits opposables et de voies de recours effectives, dans les conditions de la R&eacute;glementation en vigueur.</span>
</p>
<h2 id="h.askv9ynq28kk" class="c4"><span class="c7">ARTICLE 7 : CONSERVATION DES DONN&Eacute;ES</span></h2>
<p class="c2"><span class="c3">Les donn&eacute;es &agrave; caract&egrave;re personnel sont conserv&eacute;es pour la dur&eacute;e strictement n&eacute;cessaire &agrave; la r&eacute;alisation des finalit&eacute;s pour lesquelles elles sont trait&eacute;es et selon les recommandations de la CNIL et s&rsquo;inspire de la norme simplifi&eacute;e n&deg; NS-048 relative aux traitements automatis&eacute;s de donn&eacute;es &agrave; caract&egrave;re personnel relatifs &agrave; la gestion de clients et de prospects.</span>
</p>
<p class="c2"><span class="c3">Elles peuvent ensuite &ecirc;tre archiv&eacute;es avec un acc&egrave;s restreint pour une dur&eacute;e suppl&eacute;mentaire pour des raisons limit&eacute;es et autoris&eacute;es par la loi (paiement, garantie, litiges ...).</span>
</p><a id="t.d6c7d75259e3810ae9d0444a868f8565acc6977e"></a><a id="t.0"></a>
<table class="c28">
    <tbody>
    <tr class="c25">
        <td class="c15 c5" colspan="1" rowspan="1"><p class="c13 c14"><span
                class="c12">Finalit&eacute; du traitement</span></p></td>
        <td class="c24 c5" colspan="1" rowspan="1"><p class="c13 c14"><span class="c12">Dur&eacute;e de conservation en base active</span>
        </p></td>
        <td class="c23 c5" colspan="1" rowspan="1"><p class="c13 c14"><span class="c12">Archivage</span></p></td>
    </tr>
    <tr class="c29">
        <td class="c5 c15" colspan="1" rowspan="1"><p class="c11 c14"><span class="c3">Cr&eacute;er un Compte sur notre Site</span>
        </p></td>
        <td class="c24 c5" colspan="1" rowspan="1"><p class="c11"><span class="c3">2 ans si inactivit&eacute; de l&rsquo;usager sur la p&eacute;riode ou non consentement suite &agrave; sollicitation du responsable du traitement.</span>
        </p></td>
        <td class="c5 c23" colspan="1" rowspan="1"><p class="c13"><span class="c3">2 ans</span></p></td>
    </tr>
    <tr class="c21">
        <td class="c15 c5" colspan="1" rowspan="1"><p class="c11"><span class="c3">Chargement de Donn&eacute;es</span>
        </p></td>
        <td class="c5 c24" colspan="1" rowspan="1"><p class="c11"><span class="c3">2 ans si inactivit&eacute; de l&rsquo;usager sur la p&eacute;riode ou non consentement suite &agrave; sollicitation du responsable du traitement.</span>
        </p></td>
        <td class="c23 c5" colspan="1" rowspan="1"><p class="c13"><span class="c3">2 ans</span></p></td>
    </tr>
    </tbody>
</table>
<h2 id="h.frr4fpr9heaa" class="c4"><span class="c7">ARTICLE 8 : COOKIES</span></h2>
<p class="c2"><span class="c3">Lors de la consultation de notre Site, des cookies sont d&eacute;pos&eacute;s sur votre ordinateur, votre mobile ou votre tablette.</span>
</p>
<p class="c2"><span class="c3">Un cookie est un petit fichier texte stock&eacute; dans le navigateur de votre terminal (ordinateur, mobile, tablette) lors de la visite d&rsquo;un site ou de la consultation d&rsquo;une publicit&eacute;. Il est destin&eacute; &agrave; collecter des informations relatives &agrave; votre navigation ou &agrave; vous adresser des services adapt&eacute;s &agrave; votre terminal. Les cookies sont g&eacute;r&eacute;s par votre navigateur Internet.</span>
</p>
<h3 id="h.jcxk0sfg0ipb" class="c6 c5"><span
        class="c22 c17">8.1 LES COOKIES UTILIS&Eacute;S ET LEUR FINALIT&Eacute;</span></h3>
<p class="c2"><span class="c3">Notre Site utilise :</span></p>
<ul class="c1 lst-kix_bdfpaeahaj5a-0 start">
    <li class="c0"><span class="c3">des cookies de session permettant &agrave; l&rsquo;Utilisateur disposant d&rsquo;un Compte d&rsquo;&eacute;viter de s&rsquo;identifier &agrave; nouveau entre deux acc&egrave;s &agrave; notre Site. Ces cookies expirent &agrave; l&rsquo;issue d&rsquo;une p&eacute;riode de 4 heures.</span>
    </li>
    <li class="c0"><span class="c3">des cookies de fonctionnement (obligatoire) garantissant le bon fonctionnement de notre Site et permettent son optimisation. Notre Site ne peut pas fonctionner correctement sans ces cookies. Ces cookies expirent &agrave; l&rsquo;issue d&rsquo;une p&eacute;riode de 13 mois.</span>
    </li>
    <li class="c0"><span class="c3">des cookies analytics permettant d&rsquo;obtenir des statistiques de fr&eacute;quentation anonymes de notre Site afin d&rsquo;optimiser son ergonomie, sa navigation et ses contenus. En d&eacute;sactivant ces cookies, nous ne pourrons pas analyser le trafic de notre Site. Ces cookies expirent &agrave; l&rsquo;issue d&rsquo;une p&eacute;riode de 13 mois.</span>
    </li>
</ul>
<p class="c2"><span class="c3">Ces cookies permettent &agrave; nos services de fonctionner de mani&egrave;re optimale. Ils sont essentiels pour naviguer et acc&eacute;der &agrave; notre Site. Ils sont par cons&eacute;quent toujours actifs. Les cookies collect&eacute;s ne sont pas c&eacute;d&eacute;s &agrave; des tiers ni utilis&eacute;s &agrave; d&rsquo;autres fins que celles &eacute;dict&eacute;es ci-dessus.</span>
</p>
<h3 id="h.6osmgbjrmhs3" class="c6 c5"><span class="c22 c17">8.2 CONFIGURATION DES COOKIES</span></h3>
<p class="c2"><span class="c3">Vous disposez de diff&eacute;rents moyens pour la gestion des cookies.</span></p>
<p class="c2"><span class="c3">Vous avez la possibilit&eacute; ci-dessous de g&eacute;rer la configuration de vos cookies &agrave; tout moment via une interface d&eacute;di&eacute;e ou via votre navigateur et vous opposez ainsi &agrave; l&rsquo;enregistrement de cookies en configurant votre navigateur. La configuration de chaque navigateur est diff&eacute;rente. Elle est d&eacute;crite dans le menu d&#39;aide de votre navigateur, qui vous permettra de savoir de quelle mani&egrave;re modifier vos souhaits en mati&egrave;re de cookies.</span>
</p>
<p class="c2"><span class="c3">Cliquez sur le lien correspondant au navigateur de votre choix pour acc&eacute;der aux instructions d&eacute;taill&eacute;es sur le param&eacute;trage des cookies :</span>
</p>
<ul class="c1 lst-kix_wsdzdgg5vsbl-0 start">
    <li class="c0"><span class="c10"><a class="c20"
                                        href="https://www.google.com/url?q=https://support.microsoft.com/fr-fr/microsoft-edge/supprimer-les-cookies-dans-microsoft-edge-63947406-40ac-c3b8-57b9-2a946a29ae09&amp;sa=D&amp;ust=1609846089391000&amp;usg=AOvVaw2eJgSfqK7tEuZG08v562YU">Microsoft Edge</a></span>
    </li>
    <li class="c0"><span class="c10"><a class="c20"
                                        href="https://www.google.com/url?q=https://support.google.com/chrome/answer/95647?hl%3Dfr&amp;sa=D&amp;ust=1609846089392000&amp;usg=AOvVaw1kEWL3da3bdWaoOQA0TYTG">Google Chrome</a></span>
    </li>
    <li class="c0"><span class="c10"><a class="c20"
                                        href="https://www.google.com/url?q=https://support.mozilla.org/fr/kb/effacer-les-cookies-pour-supprimer-les-information?redirectlocale%3Dfr%26redirectslug%3Deffacer-cookies-supprimer-infos-sites-enregistrees&amp;sa=D&amp;ust=1609846089392000&amp;usg=AOvVaw3ukg318aDX12K0HqlTuCBd">Firefox</a></span>
    </li>
    <li class="c0"><span class="c10"><a class="c20"
                                        href="https://www.google.com/url?q=https://support.apple.com/kb/PH19214?locale%3Dfr_FR%26viewlocale%3Dfr_FR&amp;sa=D&amp;ust=1609846089393000&amp;usg=AOvVaw3Cc8HxxuJR0qMGBIvlKYYp">Safari</a></span><span
            class="c18">&nbsp;et </span><span class="c10"><a class="c20"
                                                             href="https://www.google.com/url?q=https://support.apple.com/fr-fr/HT201265&amp;sa=D&amp;ust=1609846089393000&amp;usg=AOvVaw2D0rchr5dylLeVC4cxgzIL">Safari iOS</a></span>
    </li>
    <li class="c0"><span class="c10"><a class="c20"
                                        href="https://www.google.com/url?q=https://help.opera.com/en/latest/web-preferences/%23cookies&amp;sa=D&amp;ust=1609846089393000&amp;usg=AOvVaw2jwkFL1dPhm5F8761m_IJs">Op&eacute;ra</a></span>
    </li>
</ul>
<h3 id="h.xxvwhvk29cxf" class="c6 c5"><span class="c22 c17">8.3 CONSENTEMENT</span></h3>
<p class="c2"><span class="c3">En naviguant sur notre Site, vous acceptez l&#39;utilisation des cookies pr&eacute;cit&eacute;s.</span>
</p>
<p class="c2"><span class="c3">Lors de votre premi&egrave;re navigation, nous vous avons inform&eacute; de la pr&eacute;sence de cookies pr&eacute;cit&eacute;es et de la possibilit&eacute; de vous y opposer en acc&eacute;dant &agrave; la pr&eacute;sente Charte d&rsquo;utilisation des donn&eacute;es personnelles.</span>
</p>
<p class="c2"><span class="c3">Nous vous informons que le refus des cookies :</span></p>
<ul class="c1 lst-kix_4lbfa3y5bfx0-0 start">
    <li class="c0"><span class="c3">entra&icirc;nera la n&eacute;cessit&eacute; de saisir vos identifiants de connexion &agrave; votre Compte &agrave; chaque acc&egrave;s sur notre Site</span>
    </li>
    <li class="c0"><span class="c3">peut entra&icirc;ner des probl&egrave;mes de fonctionnement de notre Site</span>
    </li>
    <li class="c0"><span
            class="c3">l&rsquo;impossibilit&eacute; d&rsquo;effectuer une analyse du trafic de notre Site</span></li>
</ul>
<p class="c2"><span class="c3">Afin de g&eacute;rer les cookies au plus pr&egrave;s de vos attentes, nous vous invitons &agrave; param&eacute;trer votre navigateur en tenant compte de la finalit&eacute; des cookies.</span>
</p>
<h3 id="h.sok87x6v8726" class="c5 c6"><span class="c22 c17">8.4 DROITS D&rsquo;ACC&Egrave;S, DE SUPPRESSION, D&rsquo;OPPOSITION</span>
</h3>
<p class="c2"><span class="c3">Comme pour les autres donn&eacute;es &agrave; caract&egrave;re personnel, vous disposez des m&ecirc;mes droits pr&eacute;cis&eacute;s &agrave; l&rsquo;article 10 ci-apr&egrave;s.</span>
</p>
<h3 id="h.gjb8yyjzgbc0" class="c6 c5"><span class="c22 c17">8.5 DUR&Eacute;E DE CONSERVATION</span></h3>
<p class="c2"><span
        class="c3">Les cookies collect&eacute;s sont conserv&eacute;s par nous pour une dur&eacute;e de</span></p>
<ul class="c1 lst-kix_canju575120j-0 start">
    <li class="c0"><span class="c3">4 heures maximum pour les cookies de session</span></li>
    <li class="c0"><span class="c3">13 mois pour les cookies de fonctionnement et analytics</span></li>
</ul>
<p class="c2"><span class="c3">Cette dur&eacute;e n&rsquo;est pas prorog&eacute;e automatiquement lors de vos nouvelles visites sur le Site. Au-del&agrave; de cette dur&eacute;e, les donn&eacute;es sont, soit supprim&eacute;es, soit rendues anonymes si elles ne le sont pas.</span>
</p>
<h2 id="h.dcugeo9zly74" class="c4"><span class="c7">ARTICLE 9 : S&Eacute;CURIT&Eacute; DES DONN&Eacute;ES</span></h2>
<p class="c2"><span class="c3">Pour assurer la s&eacute;curit&eacute; des donn&eacute;es personnelles, notamment pour emp&ecirc;cher qu&rsquo;elles soient d&eacute;form&eacute;es, endommag&eacute;es ou que des tiers non autoris&eacute;s y aient acc&egrave;s, nous prenons toutes les pr&eacute;cautions utiles au regard de la nature des donn&eacute;es et des risques pr&eacute;sent&eacute;s par le traitement (protection physique des locaux, proc&eacute;d&eacute; d&rsquo;authentification de nos clients avec acc&egrave;s personnel et s&eacute;curis&eacute; via des identifiants et mots de passe confidentiels pour les titulaires de Compte, journalisation des connexions, chiffrement de certaines donn&eacute;es,&hellip;).</span>
</p>
<h2 id="h.j1e2gzpozaio" class="c4"><span class="c7">ARTICLE 10 : DROITS DES UTILISATEURS</span></h2>
<p class="c2"><span class="c18">Conform&eacute;ment au R&egrave;glement (UE) 2016/679 du Parlement europ&eacute;en et du Conseil du 27 avril 2016 relatif &agrave; la protection des personnes physiques &agrave; l&#39;&eacute;gard du traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es et &agrave; la loi n&deg; 78-17 du 6 janvier 1978 relative &agrave; l&#39;informatique, aux fichiers et aux libert&eacute;s, vous disposez des droits suivants que vous pouvez exercer &agrave; tout moment en prenant contact aupr&egrave;s de DATAKODE &agrave; l&rsquo;adresse suivante </span><span
        class="c26"><a class="c20" href="mailto:contact@datakode.fr">contact@datakode.fr</a></span><span class="c3">&nbsp;:</span>
</p>
<ul class="c1 lst-kix_gmm4laefsmo4-0 start">
    <li class="c0"><span class="c3">Droit d&rsquo;acc&egrave;s, de rectification et d&rsquo;opposition au traitement de vos donn&eacute;es ;</span>
    </li>
    <li class="c0"><span class="c3">Droit &agrave; la limitation du traitement de vos donn&eacute;es ;</span></li>
    <li class="c0"><span class="c3">Droit &agrave; la portabilit&eacute; de vos donn&eacute;es ;</span></li>
    <li class="c0"><span class="c3">Droit de retrait, &agrave; tout moment, de votre consentement au traitement de ses donn&eacute;es &agrave; caract&egrave;re personnel (sans porter atteinte &agrave; la lic&eacute;it&eacute; du Traitement fond&eacute; sur le consentement effectu&eacute; avant le retrait de celui-ci).</span>
    </li>
    <li class="c0"><span class="c3">Droit &agrave; l&rsquo;effacement et &agrave; l&rsquo;oubli num&eacute;rique.</span>
    </li>
</ul>
<p class="c11 c14"><span
        class="c3 c5">ou par courrier postal &agrave; : DATAKODE, 22 route de Toulouse 31190 AUTERIVE</span></p>
<p class="c2"><span class="c3">Il est toutefois pr&eacute;cis&eacute; que l&#39;exercice de votre droit &agrave; l&#39;effacement de vos donn&eacute;es &agrave; caract&egrave;re personnel et/ou l&#39;exercice de votre droit de vous opposer au traitement de vos donn&eacute;es &agrave; caract&egrave;re personnel et/ou l&#39;exercice de votre droit &agrave; une limitation du traitement de vos donn&eacute;es &agrave; caract&egrave;re personnel et/ou de votre droit de retirer votre consentement &agrave; tout moment au traitement de vos donn&eacute;es &agrave; caract&egrave;re personnel est susceptible d&#39;entra&icirc;ner l&rsquo;impossibilit&eacute; pour l&rsquo;Utilisateur d&rsquo;acc&eacute;der &agrave; son Compte et/ou de charger une Donn&eacute;e.</span>
</p>
<p class="c2"><span class="c3">En outre vous &ecirc;tes inform&eacute; de votre droit d&#39;introduire une r&eacute;clamation aupr&egrave;s de la Commission Nationale de l&#39;Informatique et des Libert&eacute;s (CNIL) soit par voie postale 3 Place de Fontenoy - TSA 80715 - 75334 Paris 07 soit en d&eacute;posant une plainte en ligne sur le site www.cnil.fr.</span>
</p>
<h2 id="h.gnmbkrv38333" class="c4"><span class="c7">Article 11. Informations sur le caract&egrave;re contractuel de la fourniture des donn&eacute;es &agrave; caract&egrave;re personnel</span>
</h2>
<p class="c2"><span class="c3">Au titre de la fourniture aux utilisateurs des informations sur la question de savoir si l&#39;exigence de fourniture de donn&eacute;es &agrave; caract&egrave;re personnel a un caract&egrave;re r&eacute;glementaire ou contractuel ou si elle conditionne la conclusion d&#39;un contrat et si la personne concern&eacute;e est tenue de fournir les donn&eacute;es &agrave; caract&egrave;re personnel, ainsi que sur les cons&eacute;quences &eacute;ventuelles de la non-fourniture de ces donn&eacute;es, il est port&eacute; &agrave; la connaissance de l&rsquo;utilisateur les informations suivantes :</span>
</p>
<ul class="c1 lst-kix_n4dyqjaeqfzg-0 start">
    <li class="c0"><span class="c3">la demande de fourniture de donn&eacute;es &agrave; caract&egrave;re personnel a un caract&egrave;re contractuel (dans le cadre de la cadre de la relation que le Client souhaite nouer avec DATAKODE en utilisant notre Site pour effectuer un traitement de Donn&eacute;es),</span>
    </li>
    <li class="c0"><span class="c3">la fourniture de ces donn&eacute;es, en ce qu&#39;elle est n&eacute;cessaire au traitement de la Donn&eacute;e du Client conditionne la conclusion de cette relation contractuelle,</span>
    </li>
    <li class="c0"><span class="c3">&agrave; ce titre le Client est tenu de communiquer ses donn&eacute;es &agrave; caract&egrave;re personnel s&#39;il souhaite effectuer un traitement de Donn&eacute;es sur notre Site,</span>
    </li>
    <li class="c0"><span class="c3">la non fourniture de ces donn&eacute;es &agrave; caract&egrave;re personnel ne permet pas d&rsquo;effectuer un traitement de Donn&eacute;es sur notre Site.</span>
    </li>
</ul>
<h2 id="h.brngnxqnut1i" class="c4"><span class="c7">ARTICLE 12. PRISE DE D&Eacute;CISION AUTOMATIS&Eacute;E</span></h2>
<p class="c2"><span class="c3">Il est indiqu&eacute; qu&#39;il n&#39;est pas proc&eacute;d&eacute;, au moyen des donn&eacute;es &agrave; caract&egrave;re personnel collect&eacute;es, &agrave; une prise de d&eacute;cision automatis&eacute;e au sens de la R&eacute;glementation en vigueur.</span>
</p>
<h2 id="h.1mpagt42jhw5" class="c4"><span class="c7">ARTICLE 13. &Eacute;VENTUEL TRAITEMENT ULT&Eacute;RIEUR DES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL</span>
</h2>
<p class="c2"><span class="c3">Dans le cas o&ugrave; il serait effectu&eacute; un traitement ult&eacute;rieur des donn&eacute;es &agrave; caract&egrave;re personnel pour des finalit&eacute;s autres que celles pour lesquelles les donn&eacute;es &agrave; caract&egrave;re personnel ont &eacute;t&eacute; collect&eacute;es et telles qu&#39;identifi&eacute;es ci-avant, le Responsable du traitement fournira au pr&eacute;alable &agrave; la personne concern&eacute;e des informations au sujet de cette autre finalit&eacute; et toute autre information pertinente l&eacute;gale requise.</span>
</p>
<h2 id="h.striijazid5s" class="c4"><span class="c7">ARTICLE 14. &Acirc;GE DE LA PERSONNE POUVANT FOURNIR SES DONN&Eacute;ES &Agrave; CARACT&Egrave;RE PERSONNEL</span>
</h2>
<p class="c2"><span class="c3">Le recueil de donn&eacute;es personnelles ne peut que concerner les personnes &acirc;g&eacute;es d&#39;au moins 18 ans. Les utilisateurs de notre Site doivent &ecirc;tre majeurs pour y acc&eacute;der et l&rsquo;utiliser pour effectuer un traitement de Donn&eacute;es de sorte que, en effectuant un traitement de Donn&eacute;es via notre Site et/ou en nous communiquant des donn&eacute;es &agrave; caract&egrave;re personnel, vous d&eacute;clarez et garantissez &agrave; DATAKODE que vous &ecirc;tes &acirc;g&eacute;s d&#39;au moins 18 ans. Cette stipulation est sans pr&eacute;judice de l&#39;engagement que souscrit le Client vis-&agrave;-vis de DATAKODE selon lequel il d&eacute;clare et garantit &agrave; DATAKODE qu&rsquo;il a la capacit&eacute; juridique et l&eacute;gale d&#39;utiliser notre Site et de r&eacute;aliser un traitement de Donn&eacute;es.</span>
</p>
<h2 id="h.vwvb9g5zi60x" class="c4"><span class="c7">ARTICLE 15 : LIENS VERS LES AUTRES SITES</span></h2>
<p class="c2"><span class="c3">Notre Site peut contenir des liens vers d&#39;autres sites, sur lesquels nous n&#39;avons aucun contr&ocirc;le. Nous ne sommes aucunement responsables des politiques ou pratiques de protection des donn&eacute;es des autres sites que vous choisissez de visiter &agrave; partir de notre Site. Nous vous invitons donc &agrave; prendre connaissance des politiques de protection des donn&eacute;es de ces autres sites, afin de comprendre leurs modalit&eacute;s de collecte, d&#39;utilisation et de partage de vos donn&eacute;es.</span>
</p>
<h2 id="h.1l0gv8v18vi7" class="c4"><span class="c7">ARTICLE 16 : MODIFICATIONS</span></h2>
<p class="c2"><span class="c3">Nous nous r&eacute;servons le droit de modifier ou de mettre &agrave; jour &agrave; tous moments la pr&eacute;sente Charte d&rsquo;utilisation des donn&eacute;es personnelles. Ces modifications seront publi&eacute;es au sein de la pr&eacute;sente rubrique. Vous &ecirc;tes invit&eacute; &agrave; consulter r&eacute;guli&egrave;rement la Charte d&rsquo;utilisation des donn&eacute;es personnelles et &agrave; v&eacute;rifier si des modifications y ont &eacute;t&eacute; apport&eacute;es.</span>
</p>
<p class="c2"><span class="c18">Vous trouverez de plus amples informations sur la protection des donn&eacute;es personnelles sur le site de la Commission Nationale Informatique et Libert&eacute;s (</span><span
        class="c10"><a class="c20"
                       href="https://www.google.com/url?q=https://www.cnil.fr/&amp;sa=D&amp;ust=1609846089399000&amp;usg=AOvVaw3RPq7w8VYkE1vSWI9Dady7">www.cnil.fr</a></span><span
        class="c3">)</span></p>
<p class="c8"><span class="c9"></span></p></body>
</html>