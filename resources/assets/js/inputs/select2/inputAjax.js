$(document).ready(function () {
    $.fn.select2.defaults.set('language', 'fr');
    $(".select2_ajax").select2({
        placeholder: "Territoire",

        ajax: {
            //url: "https://api.github.com/search/repositories",
            url: $(".select2_ajax").data('url'),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used


                return {
                    results: data.data,

                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });

    function formatRepo(repo) {
        if (repo.text || repo.loading) {
            return repo.text;
        }

        return "<div>" + repo.name + "<span class='label label-default'>" + repo.type + "</span> </div>";
    }

    function formatRepoSelection(repo) {
        if (repo.text || repo.loading) {
            return repo.text;
        }
        return "<div>" + repo.name + "</div>";
    }




});

