
export const getConfig = (apiKey) => {
    return new Promise((resolve, reject) => {
        Gp.Services.getConfig({
            apiKey: apiKey,
            onSuccess: () => resolve(),
            onFailure: (err) => reject(err),
        });
    })
};

export const geoCode = (address, apiKey) => {
    return new Promise((resolve, reject) => {
        Gp.Services.geocode({
            location: address,
            apiKey: apiKey,
            onSuccess: function(result) {
                resolve(result);
            },
            onFailure: function(error) {
                reject(error);
            }
        });
    })
}

export const autoComplete = (address, apiKey, point) => {
    return new Promise((resolve, reject) => {
        console.log('doing autocomplete promise', address, apiKey, point);
        jQuery.get(`https://api-adresse.data.gouv.fr/search/?q=${address}&lat=${point.lat}&lon=${point.lon}`)
            .done((res) => {
                console.log('autocomplete request success', res);
                resolve(res);
            })
            .fail((err) => {
                console.log('autocomplete request fail', err);
                reject(err);
            });
    })
}

export const routing = (start, end, apiKey) => {
    return new Promise((resolve, reject) => {
        Gp.Services.route({
            startPoint: start,
            endPoint: end,
            apiKey,
            onSuccess: function(result) {
                resolve(result)
            },
            onFailure: function(error) {
                reject(error);
            }
        });
    });
}
