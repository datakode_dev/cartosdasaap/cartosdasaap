
import service from './templates/service';
import Api from './Api';
import Map from './Map';
import { pickColor } from './Utils';
import { mapField } from '../controllers/services_pei/point_fields_map'

/*
Usage : <script src="/js/embed-service.js" data-service-id="SERVICE_ID"></script>

Where SERVICE_ID is the id of the service to display
 */

const thisJs = document.querySelector("script[src*='embed-service.js']");

const url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

const baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
console.log('baseurl', baseUrl);
/**
 *
 * @param url
 * @returns {Promise}
 */
const loadScript = (url) => {
    return new Promise((resolve, reject) => {
        const script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

const loadjQuery = () => {
    if (window.jQuery) {
        console.log('jquery ok')
        return Promise.resolve();
    } else {
        return new Promise(function (resolve) {
            setTimeout(() => {
                if (window.jQuery) {
                    console.log('jquery loaded')
                    resolve()
                } else {
                    console.log('jquery loaded from elsewhere')
                    loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(() => {
                        resolve();
                    });

                }
            }, 2000);
        });
    }

};
let css = document.createElement("link");
css.rel = "stylesheet";
css.href = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css";
document.getElementsByTagName("head")[0].appendChild(css);

css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

(function () {
    const serviceId = thisJs.getAttribute('data-service-id');

    const jquery = loadjQuery();

    // Is it worth it to load select2 here ?

    jquery.then(() => {
        const leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');

        leaflet.then(() => {
            const fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            const markerClusters = loadScript('https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js');

            Promise.all([leaflet, fullscreen, markerClusters]).then(() => {
                //everything loaded and ready to go
                if (typeof $ === 'undefined') {
                    window.$ = window.jQuery;
                }

                jQuery(document).ready(() => {
                    jQuery(thisJs).after(jQuery(service));
                    const api = new Api(baseUrl);
                    const req = api.get(`/api/services/${serviceId}`);
                    const box = jQuery('#service-main');

                    req.then((s) => {
                        console.log('service Data', s);
                        if (s.active === false) {
                            jQuery('#disabled-message').show()
                        }

                        box.find('.box-title').text(s.name);

                        const map = new Map(baseUrl);

                        window.proxiclicMap = map.map;
                        const color = s.iconColor.color;
                        const icon = s.iconColor.icon;

                        let points = s.points;

                        if (s.display_fields) {
                            if (!s.display_selection) {
                                s.display_selection = s.display_fields;
                            }
                            points = mapField(s.points, JSON.parse(s.display_fields), JSON.parse(s.display_selection));
                        } else {
                            points = mapField(s.points);
                        }

                        const serviceMarkers = points.map((point) => {
                            const lat = point.geom.coordinates[1];
                            const lon = point.geom.coordinates[0];

                            let title = point.prop && point.prop.name ? point.prop.name : "";
                            const marker = L.marker([lat, lon],{
                                title: title,
                                icon: L.divIcon({
                                    html: '<div style="border-color:' + color + '"><div><i class="' + icon + '" style="color: ' + pickColor(color) + ' !important;"></i></div></div>',
                                    iconSize: [20, 20],
                                    className: 'service-point-icon',
                                    iconAnchor: [17,54],
                                    popupAnchor: [0, -50]
                                })
                            });

                            let popup = '<p class="title"><span class="icon" style="background-color:' + color + '"><i class="' + icon + '" style="color:' + pickColor(color) + '"></i></span> <span class="text" style="color:' + color + '">' + title + '</span></p>';
                            if (point.prop) {
                                Object.keys(point.prop).map(function(key) {
                                    if (key !== 'name') {
                                        popup += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + point.prop[key] + '</strong></p>';
                                    }
                                });
                            }

                            marker.bindPopup(popup);

                            return marker;
                        });

                        const grp = map.makeGroup({ ...s, color }, serviceMarkers);
                        map.map.addLayer(grp);

                        const event = new Event('proxiclicMap.loaded');
                        window.dispatchEvent(event);

                    }).catch((err) => {
                        console.log('error', err);
                        box.html('<div class="alert alert-danger">\n' +
                            '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le service!</h4>\n' +
                            // '    Ce site web n\'est sans doute pas autorisé à intégrer ce tableau de bord.\n' +
                            '    </div>')
                    });
                });
            });
        });
    });
})();
