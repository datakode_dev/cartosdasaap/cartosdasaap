importScripts('https://npmcdn.com/@turf/turf/turf.min.js');

onmessage = function(e) {
    const { action, data } = e.data;
    console.log("action", action);
    if(action === 'newpoints') {
        const points = {
            type: "FeatureCollection",
            features: data.points.map((point) => {
                return {
                    type: "Feature",
                    properties: { ...point.prop, id: point.id, serviceId: data.service.id  },
                    selectionProp: point.selectionProp,
                    geometry: point.geom,
                }
            })
        };
        console.log('Done parsing points');
        postMessage({ action: 'parsedpoints', data: { points: points, service: data.service } });
        console.log('Posted message for service ' + data.service.id + ' with ' + points.features.length + ' points', data.service);

    }
};