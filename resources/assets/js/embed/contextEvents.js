import { pickColor, startLoading, stopLoading } from "./Utils";

let address = null;
let startAddress = null;
let endAddress = null;

let currentShownFilters = [];
let currentActiveFilters = [];

let selected = null;
export const researchEventHandlers = ($, researchContext) => {
    const services = researchContext.services
    // jQuery('#print').prop('disabled', true);
    console.log('researchEventHandlers', researchContext)

    selected = researchContext.context.services.map(service => service.id);
    serviceBox(researchContext);

    setTimeout(() => {

        jQuery('body').find('.address-search').select2({
            dropdownParent: jQuery("#search-itinerary-form"),
            language: 'fr',
            templateResult: (state) => {
                if (!state.id) {
                    return state.text;
                }
                const type = state.type === 'municipality' ? '<span class="label label-default">Commune</span>' : '<span class="label label-default">Adresse</span>';
                return jQuery(`<span class="result">${state.text}</span> ${type}`);
            },
            ajax: {
                url: function (params) {
                    return params.term;
                },
                transport: (params, success, failure) => {
                    if (!params.url) {
                        return success({ results: [] });
                    }

                    console.log('params.url', params.url);
                    researchContext.addressAutocomplete(params.url).then((res) => {
                        console.log('addressAutocomplete result', res);
                        const data = res.features.map((loc) => {
                            return {
                                id: (loc.properties.type === 'municipality' ? loc.properties.postcode + ' ' : '') + loc.properties.label,
                                text: (loc.properties.type === 'municipality' ? loc.properties.postcode + ' ' : '') + loc.properties.label,
                                type: loc.properties.type,

                                loc: loc,
                            }
                        });

                        console.log('addressAutocomplete data is', data);

                        success({ results: data });
                    }).catch((err) => {
                        console.log('addressAutocomplete error', err);
                        failure(err);
                    })
                }
            }
        });
    }, 2000);

    function generatePDF (researchContext, data) {


        console.log('generatePDF', researchContext)
        const url = `${researchContext.baseUrl}/api/accounts/${researchContext.context.account_id}/research_contexts/${researchContext.context.id}/download_pdf`

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-TOKEN', $('input[name="_token"]').attr('value'));
                $("#pdfGenerateAll").html('<button class="btn btn-sm btn-primary disabled"><i class="fa fa-spin fa-spinner"></i></button>');
                $("#download_pdf_selection").html('<i class="fa fa-spin fa-spinner"></i>');

            },
            data: data,
            xhrFields: {
                responseType: 'blob'
            },
            // xhr: function() {
            //     var request = new XMLHttpRequest();
            //     // request.onreadystatechange = function() {
            //     //     if(request.readyState == 4) {
            //     //         if(request.status == 200) {
            //     //             console.log(typeof request.response); // should be a blob
            //     //         } else if(request.responseText != "") {
            //     //             console.log(request.responseText);
            //     //         }
            //     //     } else if(request.readyState == 2) {
            //     //         if(request.status == 200) {
            //     //             request.responseType = "blob";
            //     //         } else {
            //     //             request.responseType = "text";
            //     //         }
            //     //     }
            //     // };


            //     request.onload =  (e) => {
            //         if (request.status === 200) {
            //             console.log('now')

            //             let link = document.createElement('a');
            //             link.href = window.URL.createObjectURL(response);
            //             link.download = `${data.context.name}.pdf`;
            //             link.click();

            //             $("#pdfGenerateAll").html('<button id="download_pdf_all" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"></i></button>');
            //             $("#download_pdf_selection").html('Exporter le PDF');

            //         }
            //     }
            //     console.log('xhr', request)
            //     return request;
            // },
            success: function (response, status, xhr) {
                console.log('response', response, status, xhr)
                // Failed to read the 'responseText' property from 'XMLHttpRequest': The value is only accessible if the object's 'responseType' is '' or 'text' (was 'blob').

        
                // let blob = new Blob([response]);
                let link = document.createElement('a');
                // if (response instanceof Blob ) {
                //     link.href = window.URL.createObjectURL(response);
                // } else {

                //     let pdf  = "data:application/pdf;base64," + response;
                //     let blob = new Blob([pdf]);
                //     link.href = window.URL.createObjectURL(blob);
                // }

                link.href = window.URL.createObjectURL(response);

                // var binaryData = [];
                // binaryData.push(response);
                // link.href = window.URL.createObjectURL(new Blob(binaryData, {type: "application/pdf"}));
                // link.href  = "data:application/pdf;base64," + response;

                link.download = `${data.context.name}.pdf`;
                link.click();

                $("#pdfGenerateAll").html('<button id="download_pdf_all" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"></i></button>');
                $("#download_pdf_selection").html('Exporter le PDF');
            },
            error: function (err) {
                console.log(`Couldn't get current research_context ${JSON.stringify(err)}`)
            },
        });
    }


    jQuery('body')
        .on('click', 'button.itinerary', (ev) => {
            const btn = jQuery(ev.currentTarget);

            const pointId = parseInt(btn.data('point-id'), 10);
            researchContext.routeTo(pointId);
        })
        .on('click', 'a.remove-all', (ev) => {
            ev.stopPropagation();
            const btn = jQuery(ev.currentTarget);
            btn.attr('disabled', true);
            const box = btn.parents('.info-box');
            box.next('.selection').empty();
            const serviceId = parseInt(box.attr('id').replace('service-selected-', ''), 10);
            const selUl = jQuery('.my-selection-details').find('.selection');
            const sel = selUl.find('li.selected-point').filter((i, item) => {
                // console.log(item, serviceId);
                return jQuery(item).data('service-id') !== serviceId;
            });

            selUl.html(sel);
            // console.log(sel);

            const displayNum = jQuery('.sel-num');
            displayNum.text(sel.length);

            $(`#service-selected-${serviceId} a.add-all`).removeAttr('disabled');
            //TODO update selection number
        })
        .on('click', 'button.select-choose', (ev) => {
            const btn = jQuery(ev.currentTarget);
            const pointId = parseInt(btn.data('point-id'), 10);

            let point;
            if (researchContext.points.length) {
                point = researchContext.points.find(point => point.properties.id === pointId)
                point.properties.iconColor = researchContext['services'].find(s => s.id === point.properties.serviceId).iconColor
            } else {
                let points = [];
                Object.values(researchContext.servicePoints).forEach( arr => arr.forEach( a => points.push(a)))
                point = points.find(point => point.properties.id === pointId)
                point.properties.iconColor = researchContext['services'].find(s => s.id === point.properties.serviceId).iconColor
            }
            
            const b = jQuery('#service-selected-' + point.properties.serviceId);
            // jQuery('#print').prop('disabled', false);

            const html = selectPoint(point, researchContext);
            b.find('button.remove-all').attr('disabled', false);
            if (jQuery('.selected-point-' + pointId).length === 0) {
                b.next('.selection').append(html);

                jQuery('.my-selection-details').find('.selection').append(html);
                const displayNum = jQuery('.sel-num');
                let numPoints = parseInt(displayNum.text(), 10);
                displayNum.text(numPoints +1);
            }
        })
        .on('click', '.remove-selected', (ev) => {
            const pointId = jQuery(ev.currentTarget).parents('li').first().data('point-id');

            $('.selected-point-' + pointId).remove();
            const displayNum = jQuery('.sel-num');
            const numPoints = parseInt(displayNum.text(), 10);
            displayNum.text(numPoints - 1);
        })
        .on('click', '#point-search-title', (ev) => {
            jQuery(ev.currentTarget).find('i.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
            jQuery('#search-point-form').slideToggle();
            jQuery('#search-itinerary-form').slideUp();
        })
        .on('click', '#itinerary-search-title', (ev) => {
            jQuery(ev.currentTarget).find('i.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
            jQuery('#search-itinerary-form').slideToggle();
            jQuery('#search-point-form').slideUp();
        })

        //cette fonction permet de reinitialiser la page research_context
        .on('click', '.reset_button', (ev) => {
            //je réinitialise les adresses
            jQuery('#search-point').val([]).trigger('change');
            jQuery('#search-start').val([]).trigger('change');
            jQuery('#search-end').val([]).trigger('change');
            researchContext.home = null;
            researchContext.start = null;
            researchContext.end = null;

            //je remets les curseurs au milieu
            jQuery('#radius').val('5').attr('placeholder', '5');

            jQuery('#buffer').val('5').attr('placeholder', '5');
            jQuery('.current').html('5km');

            researchContext.resetMap();

            //Remove all service and add them again
            const selected = [];
            const unselected = [];
            const toRemove = [];


            jQuery('.info-box.sm').each((i, s) => {
                const serviceBox = jQuery(s);
                console.log(serviceBox);
                //serviceBox.toggleClass('selected');
                const serviceId = parseInt(serviceBox.attr('id').replace('service-selected-', ''), 10);

                console.log('service Id2', serviceId);
                toRemove.push(serviceId);
                if (serviceBox.hasClass('selected')) {
                    selected.push(serviceId);
                } else {
                    unselected.push(serviceId);
                }
            });

            console.log('toremove, toadd', toRemove, selected, unselected);
            researchContext.servicesRemoved(toRemove);
            const newServices = researchContext.servicesAdded(selected);

            const calcs = newServices.map(s => s.calc);

            //Point calculations are done, display the number of points
            Promise.all(calcs).then(() => {
                researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit ? true : false);
                displayNumPoints(researchContext, true);
            });
            researchContext.servicesRemoved(unselected);
        })
        .on('submit', '#search-point-form, #search-itinerary-form', (ev) => {
            ev.preventDefault();
        })
        .on('click', '.itinerary-btn', (ev) => {
            const s = jQuery('.end-point-search');
            $('.show-city').show()
            $('.reset_button').show()
            $('.buffer.form-group').show();
            if (s.is(':visible')) {
                jQuery('#search-start-label').text('Rechercher une adresse ou une commune');
                jQuery(ev.currentTarget).html('<i class="fa fa-map-signs"></i> Itinéraire');
                researchContext.removeEndMarker();
                researchContext.radiusChanged(parseFloat(jQuery('#buffer').val()) * 1000);
                displayNumPoints(researchContext);
                jQuery('#search-end').val([]).trigger('change');
            } else {
                if ($('.itinerary-btn.single').length) {
                    jQuery(ev.currentTarget).hide()
                }
                jQuery('#search-start-label').text('Départ');
                jQuery(ev.currentTarget).html('<i class="fa fa-map-marker"></i> Adresse');
            }

            s.toggle();

        })
        .on('change', '#search-start', (ev) => {
            ev.preventDefault();
            startAddress = jQuery('#search-start').val();
            const data = $(ev.currentTarget).select2('data');

            if (data && data.length) {
                // Set spinner
                const btni = jQuery('.itinerary-btn').find('i');
                const oldClass = btni.attr("class");
                btni.attr("class","fa fa-spin fa-refresh");
                address = null;
                // filterFacette = [];
                // researchContext.setFacettes(filterFacette);

                researchContext.startOrEndAddress(data, 'start').then((result) => {
                    console.log('DATA IS', data);

                    jQuery('#buffer').removeAttr('disabled').parent().find('.current').show();
                    if (!researchContext.end) {
                        if (data && data.length > 0 && data[0].loc.properties.type === 'municipality') {
                            console.log('loc is city', data[0].loc);
                            // $('.buffer.form-group').hide();
                            // HERE NEED TO RESET FACETTES FILTERS ?
                            researchContext.cityChanged(parseFloat(jQuery('#buffer').val()) * 1000);
                        } else if (data && data.length > 0) {
                            $('.buffer.form-group').show();
                            researchContext.radiusChanged(parseFloat(jQuery('#buffer').val()) * 1000);
                        }
                    }
                    btni.attr("class", oldClass);
                    researchContext.serviceFiltered(currentActiveFilters, true);
                    displayNumPoints(researchContext);

                }).catch((e) => {
                    alert('Désolé, L\'api ne répond pas');
                    console.log(e);
                    btni.attr("class", "fa fa-exclamation-triangle");
                });
            }
        })
        .on('change', '#search-end', (ev) => {
            ev.preventDefault();
            endAddress = jQuery('#search-end').val();
            if(!endAddress) {
                return;
            }
            const data = $(ev.currentTarget).select2('data');

            if (data && data.length) {
                // Set spinner
                const btni = jQuery('.itinerary-btn').find('i');
                const oldClass = btni.attr("class");
                btni.attr("class","fa fa-spin fa-refresh");
                address = null;
                // filterFacette = [];
                // researchContext.setFacettes(filterFacette);

                researchContext.startOrEndAddress(data, 'end').then((result) => {
                    jQuery('#buffer').removeAttr('disabled').parent().find('.current').show();
                    researchContext.serviceFiltered(currentActiveFilters, true);
                    displayNumPoints(researchContext);
                    btni.attr("class", oldClass);
                }).catch((e) => {
                    alert('Désolé, L\'api ne répond pas');
                    console.log(e);
                    btni.attr("class", "fa fa-exclamation-triangle");
                });
            }
        })
        .on('click', '#reset-filters', (ev) => {
            currentActiveFilters = []
            jQuery('#filters .select2').val([]).trigger("change")
        })
        .on('click', '#filters .select2', (ev) => {

            $("li.select2-results__option").each(function () {
                if ($(this).attr("aria-selected") === "true") {
                    $(this).addClass('active')
                } else {
                    $(this).removeClass('active')
                }
            });
        })

        .on('change', '#filters .select2', (ev) => {
            const select = jQuery(ev.currentTarget);
            const values = select.val() // ['Accompagnement']
            const field = select.attr('id') // fiche_dispositif_type
            const name = select[0].dataset.nameValue

            
            currentActiveFilters = currentActiveFilters.filter(filter => filter.name != name)
            currentActiveFilters.push({name : name, field : field, values : values})
            currentActiveFilters = currentActiveFilters.filter(filter => filter.values.length)

            //let t = jQuery('input[name=buffer]')

            // if (researchContext.endMarker === null) {
            //     researchContext.radiusChanged(parseFloat(t.val()) * 1000);
            // } else {
            //     researchContext.bufferChanged(parseFloat(t.val()));
            // }

            researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit || researchContext.start);

            // HIDE RESET BUTTON WHEN FILTERS EMPTY
            if (!currentActiveFilters.length) {
                jQuery('#reset-filters').hide();
            } else {
                jQuery('#reset-filters').show();
            }

            displayNumPoints(researchContext);

            console.log('ev', ev, $(`#${field}-selection`))

            $(`#${field}-selection .select2-selection__rendered li:not(.select2-search--inline)`).hide();
            $(`#${field}-selection .counter`).remove();
            let firstSelected = $(`#${field}-selection  .select2-selection__choice`)[0] ? $(`#${field}-selection  .select2-selection__choice`)[0].title : ''
            let counter = $(`#${field}-selection  .select2-selection__choice`).length - 1;
            if ((counter) >= 0) {
                $(`#${field}-selection .select2-selection__rendered`).after(`<div class="counter"><div class="counter-text">${firstSelected}</div><div class="counter-number"> ${counter ? '+ ' + counter : ''}</div></div>`);
                $(`#${field}-selection .select2-selection`).css("background-color", "#1f88be").css("color", "white")
            }
            else {
                $(`#${field}-selection .select2-selection__rendered`).after('')
                $(`#${field}-selection .select2-selection`).css("background-color", "white").css("color", "grey")
            }
        })
        
        .on('change', 'input[name=buffer]', (ev) => {
            const t = jQuery(ev.currentTarget);
            t.parent().find('.current').text(parseFloat(t.val()) + 'km');

            if (researchContext.endMarker === null) {
                console.log('changing radius to ', parseFloat(t.val()) * 1000);
                researchContext.radiusChanged(parseFloat(t.val()) * 1000);
            } else {
                console.log('changing buffer to ', parseFloat(t.val()));
                researchContext.bufferChanged(parseFloat(t.val()));
            }
            // researchContext.serviceFiltered(currentActiveFilters, researchContext.currentCityLimit || researchContext.startcityLimit );
            console.log('CHANGE RADIUS BUFFER', researchContext.startcityLimit, researchContext.end, researchContext.end || researchContext.startcityLimit)
            researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit || researchContext.start );


            displayNumPoints(researchContext);
        })
        .on('click', 'a.add-all', (ev) => {
            
            ev.preventDefault();
            ev.stopPropagation();

            const btn = $(ev.currentTarget);
            const serviceId = btn.closest(".info-box").attr('id').match(/\d/g).join("")

            if (!$(`#service-selected-${serviceId} a.add-all`).is('[disabled]')) { // IF ADD ALL BUTTON IS NOT DISABLED -> ADD TO SELECTION

                // const serviceId = btn.closest(".info-box").attr('id').match(/\d/g).join("")

                // Get points in the current area (circle or buffer)
                const group = researchContext.group;
                if (!group) {
                    return;
                }
                // They are simply the points that are currently displayed on the map
                const points = group.getLayers().filter((l) => l.options.serviceId == serviceId).map(m => m.options.point);

                const b = jQuery('#service-selected-' + serviceId);
                b.find('.remove-all').attr('disabled', false);
                b.find('.add-all').attr('disabled', true);
                // Enable print button
                // jQuery('#print').prop('disabled', false);

                // Remove points that have already been selected and generate the html for the others
                const filteredPoints = points.filter(p => jQuery('.selected-point-' + p.properties.id).length === 0).map( point => ({...point, properties : Object.assign({}, point.properties, {iconColor: researchContext['services'].find(s => s.id === point.properties.serviceId).iconColor})}))

                const html = filteredPoints.map(p => selectPoint(p, researchContext));
                // HERE MAKE THE SAME THING BUT ONLY OBJECT TO SEND IT TO PRINTER
                // console.log('filteredPoints', filteredPoints, html)
                b.next('.selection').append(html);
                jQuery('.my-selection-details').find('.selection').append(html);
                const displayNum = jQuery('.sel-num');
                let numPoints = parseInt(displayNum.text(), 10);
                displayNum.text(numPoints + filteredPoints.length);
            }

        })
        .on('click', '#search-services .info-box', (ev) => {
            // Toggle service selection by clicking on its box
            console.log('changed');

            const serviceBox = jQuery(ev.currentTarget);
            serviceBox.toggleClass('selected');
            const serviceId = parseInt(serviceBox.attr('id').replace('service-selected-', ''), 10);

            if (serviceBox.hasClass('selected')) {
                const res = researchContext.servicesAdded([serviceId], currentActiveFilters);
                res.forEach((r) => {
                    r.calc.then(() => {
                        console.log('servicesAdded', researchContext.radius, researchContext.buffer, researchContext.route, researchContext.end, researchContext.currentCityLimit)
                        researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit || researchContext.start);
                        displayNumPoints(researchContext);
                    })
                });
                jQuery('.show_ico'+ (serviceId)).html('<i class="fa fa-eye"></i>');
                $(`#service-selected-${serviceId} .add-all`).attr('disabled', false)

            } else {
                $(`#service-selected-${serviceId} .add-all`).attr('disabled', true)
                researchContext.servicesRemoved([serviceId]);
                jQuery('.show_ico'+ (serviceId)).html('<i class="fa fa-eye-slash"></i>');
            }
            // if (currentActiveFilters && currentActiveFilters.length) {
            //     researchContext.serviceFiltered(currentActiveFilters, researchContext.buffer || researchContext.currentCityLimit || researchContext.radius ? true : false); // true to reset markers or it duplicates
            // }
        })
        .on('click', '.open-selection', (ev) => {
            ev.preventDefault();
            jQuery('#modal-my-selection').css('display', 'block');
        })
        .on('click', '[data-dismiss=modal]', (ev) => {
            ev.preventDefault();
            jQuery('#modal-my-selection').css('display', 'none');
        })
        .on('click', '#reset', (ev) => {
            $('.selected-point').remove();
            $('.sel-num').text(0);
            $('.remove-all').attr('disabled', true)
        })
        .on('click', '#download_pdf_selection', (ev) => {

            let pointsToFilter = jQuery('li.selected-point').toArray().map((el) => JSON.parse(decodeURIComponent(escape(atob(jQuery(el).data('object')))))) // BACK FROM BASE 64 TO STRINGIFY DATA

            let pointsId = Array.from(new Set(pointsToFilter.map(p => p.id)))

            console.log('pointsToFilter', pointsToFilter, services)

            let points = pointsToFilter.filter( a => { 
                if (pointsId.includes(a.id)) {
                    pointsId.splice(pointsId.indexOf(a.id), 1);
                    return true;
                }
                return false
                })

            let s = services.map( s => ({id : s.id, name : s.name, info : s.info, iconColor : s.iconColor, points : points.filter(p => p.serviceId == s.id)}) )
            const data = {context : { name : researchContext.context.name }, services : s, button: "Votre sélection"}
            console.log('data', data)
            generatePDF(researchContext, data)

        })
        .on('click', '#download_pdf_all', (ev) => {
            // TO GET UPDATED POINTS
            let customServices = researchContext.services.map( s => {
                s.currentPoints = researchContext.group.getLayers().filter( m => m.options.serviceId === s.id ).map( m => m.options.point.properties)
                return s
            })
            const context = { name : researchContext.context.name }
            let s = customServices.map( s => ({id : s.id, name : s.name, info : s.info, iconColor : s.iconColor, points : s.currentPoints}) )

            const data = {context : context, services : s, button: "Votre résultat"}
            generatePDF(researchContext, data)
        })
};

const displayNumPoints = (researchContext, ignoreNonSelected = false) => {
    jQuery('#search-services').find('.info-box').each((i, item) => {

        if (researchContext.context.display_selection) {
            $('.remove-all').show()
            $('.add-all').show().removeAttr('disabled')
        }

        const $item = jQuery(item);

        const id = parseInt($item.attr('id').replace('service-selected-', ''), 10);

        const group = researchContext.group;
        if (!group) {
            return;
        }

        let n = group.getLayers().filter((m) => {
            return m.options.serviceId === id;
        });

        if (ignoreNonSelected && !$item.hasClass('selected')) {
            n = researchContext.servicePoints[id];
        }
        // TO DO : SET POLYGONE LINES NUMBER
        $item.find('.info-box-number').text(n.length);

        // UPDATE FILTERS POINT NUMBER
        $item.find('ul').each(function(e, ul) { 
            const service = researchContext.services.find(s => s.id == $(this).data( "service-id" ) )
            if (service) {
                let nFilter = n.map(l => l.options.point.properties.id )
                let facetteKey = $(this).data("facette-value")
                $(this).children().each(function(i, li) { // VALUE FILTER
                    let data = service.data_recherche.filter(data => nFilter.includes(data.id) && data.prop[facetteKey] == $(this).data( "recherche-value" ))
                    $(this).find('.badge').text(data.length)
                })
            }
        })


        // Change add all button tooltip
        const title = 'Ajouter les '+ n.length +' services à la sélection';
        $item.find('button.add-all').attr('title', title).tooltip('fixTitle');
        if (n.length === 0) {
            $item.find('button.remove-all').attr('disabled', true);
        }

    });
    console.log('DISPLAYNUM POINT', $('.info-box-recherche ul'))
};

const selectPoint = (point, researchContext) => {
    console.log('point', point)
    const pointId = point.properties.id;

    let props = [];
    let name = '';

    // console.log('point', point);
    if (point.selectionProp) {
        props = Object.keys(point.selectionProp).map(function (key) {
            if (key !== 'name' && key !== 'id' && key !== 'serviceId') {
                return '<li>' + key + '&nbsp;: <strong>' + point.selectionProp[key] + '</strong></li>';
            }
        });
        name = typeof point.selectionProp.name !== 'undefined' &&  point.selectionProp.name !== null ? point.selectionProp.name : '&nbsp;';
    }

    console.log('btoa error on dev : ', JSON.stringify(point.properties))
    return `<li data-object='${btoa(unescape(encodeURIComponent(JSON.stringify(point.properties))))}' data-service-id="${point.properties.serviceId}" class="selected-point selected-point-${pointId}" data-point-id="${pointId}">
    <div><span style="background-color:${point.properties.iconColor.color};padding: 10px;margin-right: 10px;">
    ${point.properties.iconColor.icon_path ? `<img src="${researchContext.baseUrl}/storage/${point.properties.iconColor.icon_path}" alt="${point.properties.iconColor.icon}" style="max-width: 20px;"/>` : `<i class="${point.properties.iconColor.icon}" style="font-size: 20px;color:${pickColor(point.properties.iconColor.color)}"></i>`}
    </span>${name}
    <button class="btn btn-danger remove-selected pull-right"><i class="fa fa-close"></i></button></div><ul>${props.join('')}</ul></li>`;
    // return `<li data-object='${btoa(JSON.stringify(point.properties))}' data-service-id="${point.properties.serviceId}" class="selected-point selected-point-${pointId}" data-point-id="${pointId}"><strong>${name}<button class="btn btn-danger remove-selected pull-right"><i class="fa fa-close"></i></button></strong><ul>${props.join('')}</ul></li>`;
    // return `<li data-object='${JSON.stringify(point.properties)}' data-service-id="${point.properties.serviceId}" class="selected-point selected-point-${pointId}" data-point-id="${pointId}"><strong>${name}<button class="btn btn-danger remove-selected pull-right"><i class="fa fa-close"></i></button></strong><ul>${props.join('')}</ul></li>`;

};
function generateServiceFilters(res) {
    console.log('res', res)
    let facettes_array = JSON.parse(res.display_recherche)

    // console.log('display_recherche', res.display_recherche, facettes_array)
    if (facettes_array) {
        delete facettes_array["name"];
        let filterSet = new Set(Object.values(facettes_array));
        let sortedSet = [...filterSet].sort();
        filterSet = new Set(sortedSet);

        filterSet.forEach( facette => {
            let name = Object.keys(facettes_array).find(key => facettes_array[key] === facette)

            if (name !== 'name') {
                let filter = {name : name, type : facette, currentValues : []} // currentValues : {value :}
                res.data_recherche.forEach( point => {  // point.prop : {fiche_dispositif_type: 'Accompagnement', fiche_acces_pmr: 'Oui'}
                    // console.log('point.prop[facette]', point.prop[facette])
                    const isBr = point.prop[facette] && point.prop[facette].includes('<br>')
                    const isSlash = point.prop[facette] && point.prop[facette].includes('|')
                    const isComma = point.prop[facette] && point.prop[facette].includes(';')

                    if (isBr) {
                        const stringSplit = point.prop[facette].split('<br>')
                        stringSplit.forEach(str => {
                            let prop_value = filter.currentValues.find( v => v.value == str)
                            if (!prop_value) {
                                filter.currentValues.push({value : str})
                            }
                        })
                    } else if (isSlash) {
                            const stringSplit = point.prop[facette].split('|')
                            stringSplit.forEach(str => {
                                let prop_value = filter.currentValues.find( v => v.value == str)
                                if (!prop_value) {
                                    filter.currentValues.push({value : str})
                                }
                            })
                    } else if (isComma) {
                        const stringSplit = point.prop[facette].split(';')
                        stringSplit.forEach(str => {
                            let prop_value = filter.currentValues.find( v => v.value == str)
                            if (!prop_value) {
                                filter.currentValues.push({value : str})
                            }
                        })
                    } else {
                        let prop_value = filter.currentValues.find( v => v.value == point.prop[facette])
                        if (!prop_value && point.prop[facette]) {
                            filter.currentValues.push({value : point.prop[facette]})
                        }
                    }
                })
                // SET HTML
                let filterSelection = ''
                let selectHtml = '';

                let filterFound = currentShownFilters.find(f => f.name == filter.name)
                if (filterFound) {
                    filter.currentValues.forEach((v, i) => {
                        let includeOption = filterFound.select.includes(`<option>${v.value}</option>`)
                        if (!includeOption) { // IF VALUE IS NOT IN OPTIONS FROM SAME SELECT -> ADD VALUE AT THE END BEFORE </select>
                            var n = filterFound.select.lastIndexOf("</select>");
                            var str2 = filterFound.select.substring(0,n) + `<option>${v.value}</option>` + filterFound.select.substring(n);
                            currentShownFilters = currentShownFilters.map(f => f.name == filterFound.name ? {...f, select : str2 } : {...f} )
                        }
                    })
                } else {
                    filter.currentValues.forEach((v, i) => {
                        //Ces valeurs sont séparées par des ; ou par des | ou par des <br>.
                        filterSelection += `<option>${v.value}</option>`
                    })
                    // S'IL Y A AU MOINS UN FILTRE
                    // if (filterSelection) {
                    selectHtml += `<div class="col-md-3" id="${filter.type}-selection"><select id="${filter.type}" data-name-value="${filter.name}" multiple class="form-control select2" placeholder="${facette}" data-toggle="tooltip" data-placement="top" title="${filter.name}">${filterSelection}</select></div>`
                    // }
                    currentShownFilters.push({...filter, select : selectHtml}) 
                }
            }
        })
        currentShownFilters = currentShownFilters.filter( filter => filter.select.includes('<option>'))  // EVERY FILTERS ARE INIT. REMOVE THOSE WHO HAVEN'T OPTION
    }
}

const serviceBox = (researchContext) => {
    let vals = selected;
    //Hide map
    startLoading('#search-context-main .col-md-8');
    console.log('researchContext serviceBox', researchContext, researchContext.services[0])
    let added = vals.filter(x => !researchContext.obtained.includes(x));
    let removed = researchContext.obtained.filter(x => !vals.includes(x));
    removed.forEach((serviceId) => {
        jQuery('#service-selected-' + serviceId).next('ul.selection').remove();
        jQuery('#service-selected-' + serviceId).remove();
        if (jQuery('#search-services').find('.info-box').length === 0) {
            jQuery('#search-services').find('.no-selection').show();
        }
    });

    //Display loading box
    added.forEach((id) => {
        const $tmpl = jQuery(jQuery('#search-service-template').text());
        $tmpl.first().attr('id', 'service-loading-' + id);
        $tmpl.find('.info-box-icon').css('background-color', "white");
        $tmpl.find('.info-box-icon > span').css('border-color', "white");
        $tmpl.find('.info-box-icon > i').css('color', 'white');
        $tmpl.find('.info-box-text').text('Chargement en cours');
        $tmpl.find('.info-box-number').html('<i class="fa fa-spin fa-spinner"></i>');
        jQuery('#search-services').append($tmpl).find('.no-selection').hide();
    });

    researchContext.servicesRemoved(removed);
    const newServices = researchContext.servicesAdded(added);

    const requests = newServices.map(s => s.req);
    const calcs = newServices.map(s => s.calc);

    //Point calculations are done, display the number of points
    Promise.all(calcs).then((results) => {
        results.forEach((res) => {
            const d = jQuery('#service-selected-'+res.id);
            const n = researchContext.servicePoints[res.id].length;
            console.log('res', res.id);
            d.find('.info-box-number').text(n);
            d.find('.info-box-content').prepend('<div class="pull-right btn-group"><button title="Afficher/Masquer sur la carte" class="show_ico'+ (res.id) +' btn btn-sm" style="background-color: rgb(239, 239, 239);"><i class="fa fa-eye"></i></button></div>');
            d.find('button.add-all').tooltip();
            d.find('button.remove-all').tooltip();
        });
        displayNumPoints(researchContext);
        
    }).catch((er) => {
        console.log(er);
    });

    //Requests are done, display the data for this service
    Promise.all(requests).then((results) => {
        //researchContext.getPointsInBufferZone();

        results.forEach((res) => {
            const $tmpl = jQuery(jQuery('#search-service-template').text());
            $tmpl.first().attr('id', 'service-selected-' + res.id);

            // SERVICE MARKERS RESULT STYLE
            if (res.iconColor && res.iconColor.color && res.iconColor.icon) {
                $tmpl.find('.info-box-icon').css('background-color', res.iconColor.color);
                $tmpl.find('.info-box-icon > span').css('border-color', res.iconColor.color);

                if (res.iconColor.icon_svg) { // ICON CUSTOM (SVG)
                    $tmpl.find('.info-box-icon').css('color', pickColor(res.iconColor.color)).html(`<img src="${researchContext.baseUrl}/storage/${res.iconColor.icon_path}" alt="${res.iconColor.icon}" style="max-width: 30px;"/>`)
                } else {
                    $tmpl.find('.info-box-icon > i').css('color', pickColor(res.iconColor.color)).addClass(res.iconColor.icon);
                }
            }
            // SERVICE POLYGONE / LINE RESULT STYLE
            else if (res.outline) {
                $tmpl.find('.info-box-icon').css('background', `white`);
                $tmpl.find('.info-box-icon').css('background-color', `${res.geometry === 'Polygon' ? res.fill : '{ffffff'}6f`); // 6f pour opacité - 50%
                $tmpl.find('.info-box-icon').css('border', `5px solid ${res.outline}`);
            }

            // let facettes = generateFacetteText(res);
            let text = res.name ? res.name + (res.info ? '<br><small style="color: #999;text-transform: none;font-size: .8em;">' + res.info + '</small>' : '') : '&nbsp;';
            
            $tmpl.find('.info-box-text').html(text);
            $tmpl.find('.info-box-number').html('<i class="fa fa-spin fa-spinner"></i>');
            // console.log("$tmpl.find('#filters')", $tmpl)
            // $tmpl.find('.info-box-recherche').html(facettes);
            jQuery('#search-services').append($tmpl).find('.no-selection').hide();
            const l = jQuery('#service-loading-'+res.id);
            l.next('ul.selection').remove();
            l.remove();

            generateServiceFilters(res);

        });

        // INIT FILTERS
        currentShownFilters.forEach( (filter, i) => {
            if (i < 10) { // MOINS DE 10 FILTRES MONTRÉS
                $('#filters-box').css('display', 'initial');

                const selector = `#${filter.type}.select2`
                $('#filters').append(filter.select)
                $(selector).select2({
                    placeholder: filter.name,
                    dropdownParent : $('#search-context-main.cartosdaasap-embed')

                });
            }
        })

        // Remove spinner from map
        stopLoading();
    }).catch((er) => {
        console.log(er);
    })
};