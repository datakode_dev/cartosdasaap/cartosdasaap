import { pickColor } from './Utils';

class Map {
    constructor(baseUrl) {
        this.map = L.map('maCarte', {maxZoom: 19}).setView([46.85, 2.3518], 5);
        this.activeLayers = [];
        this.geometry = null;
        this.controls = null;
        this.markers = [];
        const OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });

        if (typeof L.Control.Fullscreen !== 'undefined') {
            this.map.addControl(new L.Control.Fullscreen({
                title: {
                    'false': 'Voir en plein écran',
                    'true': 'Sortir de la vue plein écran'
                }
            }));
        }

        window.proxiclicMap = this.map;

        this.map.addLayer(OpenStreetMap_Mapnik);
    }

    setControls(controls) {
        this.controls = controls
    }

    activateLayer(layer, color, removePrevious = true, fitBounds = true) {
        if (this.activeLayers && removePrevious) {
            this.activeLayers.forEach((l) => {
                this.map.removeLayer(l);
            });
        }

        const geom = this.displayGeometry(layer.geom, color, false, 0.4, fitBounds);
        this.activeLayers.push(geom);


        let ls = [...this.activeLayers];
        ls = ls.reverse();

        setTimeout(() => {
            this.markers.forEach(m => {
                if (m) {
                    m.bringToFront();
                }
            });
        }, 100);


        return geom;
    }

    clear(fit = false) {
        if (this.activeLayers.length > 0) {
            this.activeLayers.forEach((l) => {
                this.map.removeLayer(l);
            });
        }
        if (fit) {
            this.map.fitBounds(this.geometry.getBounds());
        }
    }

    displayGeometry(geometry, color = "blue", dashArray = null, opacity = 0.3, fitBounds = true, type = null) {
        const options = {};
        if (type === 'circle') {
            // These are the ponits from the service
            options.pointToLayer = (feature, latlng) => {
                return L.circleMarker(latlng, {
                    radius: 4,
                });
            };
        }
        const geojsonLayer = L.geoJson(geometry, options);

        // Display service points


        geojsonLayer.setStyle({fillColor: color, color: dashArray === false ? 'transparent' : color, dashArray: null, fillOpacity: opacity, weight: dashArray === false ? 0 : 1});
        geojsonLayer.addTo(this.map);

        geojsonLayer.bringToFront();
        if (fitBounds) {
            this.map.fitBounds(geojsonLayer.getBounds());
        }

        return geojsonLayer;
    }

    changeGeometry(geometry, color = "blue", dashArray = null, opacity = 0.4) {
        if (this.geometry) {
            this.map.removeLayer(this.geometry);
        }
        this.geometry = this.displayGeometry(geometry, color, dashArray, opacity);
    }

    /**
     * Make a cluster group from a list of markers
     *
     * @param service
     * @param serviceMarkers
     */
    makeGroup(service, serviceMarkers) {
        const group = L.markerClusterGroup({
            iconCreateFunction: function (cluster) {
                return L.divIcon({ html: `<div style="background-color:${service.color} !important;color:${pickColor(service.color)} !important"><span>${cluster.getChildCount()}</span></div>`, className: 'custom-cluster', iconSize: L.point(40, 40) });
            }
        });

        group.addLayers(serviceMarkers);

        return group;
    }
}

export default Map;
