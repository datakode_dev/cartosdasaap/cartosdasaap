import Api from './Api'
import { pickColor, createWorker } from './Utils'
import { getConfig, geoCode, routing, autoComplete } from "./Gp";
import { mapField } from '../controllers/services_pei/point_fields_map';

class ResearchContext {
    constructor(contextId, contextLoadedCallback = null, baseUrl) {
        this.api = new Api(baseUrl + '/api');
        this.baseUrl = baseUrl;
        this.obtained = [];
        this.markers = [];
        this.group = this._makeGroup([]);
        this.points = [];
        this.servicePoints = {};
        this.start = null;
        this.end = null;
        this.homeMarker = null;
        this.startMarker = null;
        this.endMarker = null;
        this.buffer = null;
        this.route = null;
        this.context = null;
        this.currentCityLimit = null;
        this.startcityLimit = null;
        this.endcityLimit = null;

        this.services = [];

        this.filters = [];

        this.radius = 5000;
        this.bufferRadius = 5;
        this.map = L.map('search-map', { center: L.latLng(46.85, 2.3518), zoom: 5, maxZoom: 19 });

        // HIDE ZOOM OPTIONS IF MOBILE
        console.log('screen', jQuery(window).width())
        if (jQuery(window).width() < 400) {
            this.map.zoomControl.remove();
        } else {
            if (typeof L.Control.Fullscreen !== 'undefined') {
                this.map.addControl(new L.Control.Fullscreen({
                    title: {
                        'false': 'Voir en plein écran',
                        'true': 'Sortir de la vue plein écran'
                    }
                }));
            }
        }


        this.limit = null;
        this.map.addLayer(this.group);
        const lyrOSM = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png?', { attribution: '&copy; <a href=\"https://osm.org/copyright\">OpenStreetMap</a> contributors' });
        this.map.addLayer(lyrOSM);
        window.proxiclicMap = this.map;

        this.api.get(`/researchcontexts/${contextId}`).then((ctx) => {
            this.context = ctx;
            this.apiKey = ctx.key;


            getConfig(this.apiKey).then(() => {

                const lyrOrtho = L.geoportalLayer.WMTS({
                    layer: "ORTHOIMAGERY.ORTHOPHOTOS",
                });

                L.control.layers({ 'Plan': lyrOSM, 'Photographie a&eacuterienne': lyrOrtho}).addTo(this.map);
            });

            this.api.get(`/insee/${ctx.zone_id}/${ctx.insee_limit}/${ctx.dist_around_limit}`).then((resp) => {
                try {
                    this.limit = L.geoJson(resp, { style: { fillOpacity: 0, color: 'black', weight:1 } });
                    this.limit.addTo(this.map);
                    this.map.fitBounds(this.limit.getBounds());
                    if (contextLoadedCallback) {
                        setTimeout(() => {
                            contextLoadedCallback(ctx);
                        }, 500);

                    }
                } catch (e) {
                    console.log(e);
                    if (contextLoadedCallback) {
                        contextLoadedCallback(null, e);
                    }
                }
            }).catch((er) => {
                console.log(er);
                if (contextLoadedCallback) {
                    contextLoadedCallback(null, er);
                }
            });
        }).catch(err => {
            if (contextLoadedCallback) {
                contextLoadedCallback(null, err);
            }
        });


    }

    servicesRemoved(ids) {
        return ids.map((id) => {
            const serviceId = parseInt(id, 10);
            console.log('removing service', serviceId)
            const cnt = this.markers.length;
            // COMMENTED IT's UNABLE TO RESHOW MARKER ON CLICK
            // this.markers = this.markers.filter(m => m.options.serviceId === serviceId);
            this.points = this.points.filter(p => p.serviceId === serviceId);
            delete this.servicePoints[id];

            this.group.removeLayers(this.group.getLayers().filter(marker => {
                //console.log(marker.options.serviceId, serviceId);
                return marker.options.serviceId === serviceId
            }));
            this.obtained = this.obtained.filter(o => o !== serviceId);
            this.services = this.services.filter(s => s.id !== serviceId);

            return cnt;
        });
    }

    generatePoint(point, data) {
        if (!this.group.getLayers().find(m => m.options.point.properties.id === point.properties.id)) {
            this.points.push(point);
            this.servicePoints[data.service.id].push(point);

            if (point.geometry.type === 'LineString' || point.geometry.type === 'MultiLineString') {
                const line = this._makeLine(point, data.service);
                if (!this.markers.find(m => m.options.point.properties.id === point.properties.id)) this.markers.push(line);
                this.group.addLayer(line);
            } else if (point.geometry.type === 'Polygon' || point.geometry.type === 'MultiPolygon') {
                const polygon = this._makePolygon(point, data.service);
                if (!this.markers.find(m => m.options.point.properties.id === point.properties.id)) this.markers.push(polygon);
                this.group.addLayer(polygon);
            } else if (point.geometry.type === 'MultiPoint') {
                point.geometry.coordinates.forEach( (coord, id) => {
                    const marker = this._makeMarker(point, data.service, id);
                    if (!this.markers.find(m => m.options.point.properties.id === point.properties.id)) this.markers.push(marker);
                    this.group.addLayer(marker);
                })
            } else if (point.geometry.type === 'Point') {
                const marker = this._makeMarker(point, data.service);
                if (!this.markers.find(m => m.options.point.properties.id === point.properties.id)) this.markers.push(marker);
                this.group.addLayer(marker);
            }
        }
    }

    servicesAdded(ids, currentActiveFilters) {
        // this.markers = [];
        console.log('servicesAdded', ids, currentActiveFilters)
        return ids.map((id) => {
            const serviceId = parseInt(id, 10);
            this.obtained.push(serviceId);


            const serviceRequest = this.api.get(`/researchcontexts/${serviceId}/${this.context.id}`);
            const serviceMarkers = [];
            this.servicePoints[id] = [];


            const calculation = new Promise((resolve, reject) => {
                serviceRequest.then((res) => {
                    let servicePoints = [...res.points];

                    // SAVE RECHERCHE DATA TO BE ABLE TO FILTER
                    // WORK--
                    if (res.display_recherche) {
                        let isRecherche = Object.values(JSON.parse(res.display_recherche))
                        res.data_recherche = res.points.map( p => {
                            let point = {...p, prop : {}}
                            isRecherche.forEach(key => point.prop[key] = p.prop[key])
                            return point
                        })
                    }
                    // -----
                    this.services.push({...res});
                    
                    if (res.display_fields || res.display_selection) {
                        servicePoints = mapField(res.points, JSON.parse(res.display_fields),JSON.parse(res.display_selection));
                    } else {
                        servicePoints = mapField(res.points);
                    }

                    //Use webworker so that the page does not block when calculating which points are included in the limit polygon
                    const pointsWorker = createWorker(this.baseUrl + '/js/search-worker.js');
                    try {
                        pointsWorker.postMessage({ action: 'newpoints', data: { points: servicePoints, service: res, limit: this.limit.toGeoJSON() } });
                    } catch (e) {
                        reject(e);
                    }
                    pointsWorker.onmessage = (e) => {
                        const { action, data } = e.data;
                        if (action === 'parsedpoints') {
                            // TO DO CREATE POLYGONE / LINE
                            // this.group.removeLayers(this.group.getLayers())

                            data.points.features.forEach((point) => {
                                this.generatePoint(point, data) // {points: {…}, service: {…}}
                            });

                            // Make sure new service is displayed correctly
                            console.log('will update', this.buffer, this.currentCityLimit, this.radius);
                            if (this.buffer) {
                                console.log('doing buffer');
                                this.getPointsInBufferZone();
                            } else if (this.currentCityLimit) {
                                console.log('doing city limit');
                                this.cityChanged(5);
                            } else if (typeof this.radius === 'number') {
                                console.log('doing radius');
                                this.radiusChanged(this.radius);
                            }
                            resolve(data.service);
                        }
                    };
                });
            });
            return { calc: calculation, req: serviceRequest };
        });
    }
    setFacettes (facettes) {
        console.log('setFacettes', facettes)
        this.lastFacettes = facettes
        // RESET MAP AS IF NO FACETTES
        if (this.lastEdit == "radius") {
            this.radiusChanged(this.radius);
        }
        if (this.lastEdit == "buffer") {
            this.getPointsInBufferZone();
        }
        if (this.lastEdit == "city") {
            this.cityChanged();
        }
        if (!this.lastEdit) {
            // SI AUCUN, AFFICHER TOUS LES MARKERS (resetMap ?)
            this.group.removeLayers(this.group.getLayers())
            this.restorePoints();
        }
    }
    groupFilteredByFacette () {
        console.log('groupFilteredByFacette', this.lastEdit, this.lastFacettes)

        // REMOVE UNSELECTED SERVICES POINTS
        this.services.forEach( s => {
            if (!$(`#service-selected-${s.id}`).hasClass( "selected" )) {
                console.log('this.group.getLayers().filter(p => p.options.point.properties.serviceId == s.id)', this.group.getLayers().filter(p => p.options.point.properties.serviceId == s.id))
                this.group.removeLayers(this.group.getLayers().filter(p => p.options.point.properties.serviceId == s.id))
            }
        })
        this.group.eachLayer( p => {
            if (!this.services.find(s => s.id == p.options.point.properties.serviceId)) {
                this.group.removeLayer(p)
            }
        })
    }

    serviceFiltered (currentFilters, currentLayers = false, endSearchData = false) {
        // currentFilters = [{field : fiche_acces_pmr, name : ACCES PMR, values : []}]

        console.log('serviceFiltered', currentFilters, currentLayers)
        console.log('DATA', this.start,
        this.radius,
        this.end,
        this.homeMarker,
        this.startMarker,
        this.endMarker,
        this.buffer,
        this.route,
        this.context,
        this.currentCityLimit,
        this.startcityLimit,
        this.endcityLimit,
        this.filters
        )
        // CLEAN LAYERS > RESTORE ID TO SHOW PER SERVICE
        this.group.removeLayers(this.group.getLayers())

        if (currentLayers) {
            // NEED TO RESET LAYERS TO GET CITY MARKERS && THEN FILTER ON IT TO NOT SHOW THE WHOLE MAP
            if (this.startcityLimit) {
                this.cityChanged()
            } 
            else if (this.end) {
                this.getPointsInBufferZone()
            } 
            // else if (this.startcityLimit) {
            //     this.cityChanged()
            // } 
            else if (this.radius && this.start) {
                this.radiusChanged(this.radius);
            }
            /* 
            1. GET CURRENT LAYERS IN GROUPLAYERS (SHOWN)
            2. PER SERVICE, GET LAYERS TO CHECK
            3. VERIFY IF LAYERS RESPECT FILTERS
            4. CLEAN AND RESTORE LAYERS
             */
            let currentLayersId = this.group.getLayers().map(p => p.options.point.properties.id)
            let idsToShow = []
            this.services.forEach( currentService => {

                if (currentService && $(`#service-selected-${currentService.id}`).hasClass( "selected" )) {

                    if (Array.isArray(currentService.data_recherche)) {

                        let layersToFilter = currentService.data_recherche.filter(marker => currentLayersId.includes(marker.id))

                        let idsToShowInService = layersToFilter.filter(point => {
                            // let state = []
                            let state = {}

                            currentFilters.forEach( filter => {
                                let display_recherche = JSON.parse(currentService.display_recherche) 

                                if (!state[filter.field]) {
                                    state[filter.field] = []
                                }
                                if (display_recherche[filter.name] == filter.field) {
                                    if (point.prop[filter.field]) {
                                        filter.values.forEach( v => {
                                            if (point.prop[filter.field].includes(v)) {
                                                // state.push(true)
                                                state[filter.field].push(true)
                                            } else {
                                                state[filter.field].push(false)
                                                // state.push(false)
                                            }
                                        })
                                    } else {
                                        state[filter.field].push(false)
                                        // state.push(false)
                                    }
                                }
                            })
                            let showPoint = true;
                            Object.values(state).forEach(arr => {
                                if (arr.length && arr.includes(false)) { // ET
                                // if (arr.length && !arr.includes(true)) { // OU
                                    showPoint = false
                                }
                            })
                            return showPoint;
                        })
                        idsToShow = idsToShow.concat(idsToShowInService)
                    }

                }
            })
            idsToShow = idsToShow.map(point => point.id)
            this.group.removeLayers(this.group.getLayers())
            this.restorePoints(idsToShow)
        } else {
            this.services.forEach( currentService => {
                if (currentService && $(`#service-selected-${currentService.id}`).hasClass( "selected" )) {
                    if (Array.isArray(currentService.data_recherche)) {

                        let idsToShow = currentService.data_recherche.filter(point => {
                            let state = {}
                            // let state = []
                            currentFilters.forEach( filter => { // {field : fiche_acces_pmr, name : ACCES PMR, values : []}

                                let display_recherche = JSON.parse(currentService.display_recherche) 

                                // {DISPOSITIF: 'fiche_dispositif_type', name: 'fiche_dispositif_type', ACCES PMR: 'fiche_acces_pmr', Assistance Type: 'fiche_assistance_type', Paiement: 'fiche_assistance_paiement', …}
                                /*  point : {
                                    "fiche_dispositif_type": "Accompagnement",
                                    "fiche_acces_pmr": "Oui",
                                    "fiche_assistance_type": "Toutes démarches",
                                    "fiche_assistance_paiement": "",
                                    "fiche_mediation_autre_paiement": ""
                                } */
                                if (!state[filter.field]) {
                                    state[filter.field] = []
                                }
                                if (display_recherche[filter.name] == filter.field) {
                                    if (point.prop[filter.field]) {
                                        filter.values.forEach( v => {
                                            if (point.prop[filter.field].includes(v)) {
                                                // state.push(true)
                                                state[filter.field].push(true)
                                            } else {
                                                state[filter.field].push(false)
                                                // state.push(false)
                                            }
                                        })
                                    } else {
                                        state[filter.field].push(false)
                                        // state.push(false)
                                    }
                                }
                            })
                            let showPoint = true;
                            Object.values(state).forEach(arr => {
                                if (arr.length && arr.includes(false)) {
                                // if (arr.length && !arr.includes(true)) { // OU
                                    showPoint = false
                                }
                            })
                            return showPoint;
                            // return state.length ? state.includes(true) : true // Renvoie les points qui n'ont aucun filtre correspondant + qui ont au moins un filtre correspondant qui ets vrai
                            // return state.length ? !state.includes(false) : true // Renvoie les points qui n'ont aucun filtre correspondant + qui ont au moins un filtre correspondant qui ets vrai
                        }).map(point => point.id)
                        this.restorePoints(idsToShow)
                    }
                }
            })
        }
        if (!currentFilters.length) {
            // RESTORE POINTS
            // currentLayers ? this.end ? this.getPointsInBufferZone() : this.cityChanged() : this.restorePoints();
            if (currentLayers) {
                if (this.end) {
                    // console.log('this.endSearchData', this.endSearchData, this.end)
                    this.getPointsInBufferZone()
                } 
                else if (this.startcityLimit) {
                    this.cityChanged()
                } 
                else if (this.radius && this.start) {
                    this.radiusChanged(this.radius);
                } 
            }
            else {
                this.restorePoints();
            }
            
            // THEN HIDE DISABLED SERVICE
            const currentServicesId = this.services.map(s => s.id)
            this.group.removeLayers(this.group.getLayers().filter(marker => {
                return !currentServicesId.includes(marker.options.serviceId)
            }));
        }
        this.map.fitBounds(this.group.getBounds());
    }

    removeEndMarker() {
        if (this.endcityLimit) {
            this.map.removeLayer(this.endcityLimit);
            this.endcityLimit = null;
        }
        this.end = null;
        if (this.endMarker) {
            this.map.removeLayer(this.endMarker);
        }

        this.endMarker = null;

        if (this.buffer) {
            this.map.removeLayer(this.buffer);
            this.buffer = null;
        }
    }

    radiusChanged(newRadius) {
        console.log('radiusChanged', newRadius)
        this.radius = newRadius;
        if (this.buffer !== null) {
            this.map.removeLayer(this.buffer);
            this.buffer = null
        }
        if (this.currentCityLimit) {
            this.map.removeLayer(this.currentCityLimit);
            this.currentCityLimit = null
        }
        if (this.startcityLimit) {
            this.map.removeLayer(this.startcityLimit);
            this.startcityLimit = null
        }
        if (this.endcityLimit) {
            this.map.removeLayer(this.endcityLimit);
            this.endcityLimit = null
        }
        if (this.start && typeof this.radius === 'number') {
            this.group.removeLayers(this.group.getLayers())

            //Remove points that are not in the radius
            if (this.currentDiameter) {
                this.map.removeLayer(this.currentDiameter);
                this.currentDiameter = null
            }

            const polygon = turf.polygon([[[-180, -80], [180, -80], [180, 80], [-180, 80], [-180, -80]]], { name: 'all' });

            const options = { steps: 120, units: 'meters' };
            const circle = turf.circle([this.start.lng, this.start.lat], this.radius, options);

            const diff = turf.difference(polygon, circle);

            this.currentDiameter = L.geoJSON(diff, { fillOpacity: 0.15, opacity: 0.3, fillColor: "#000", color: "#000" });

            this.map.addLayer(this.currentDiameter);
            this.map.fitBounds(L.geoJSON(circle).getBounds());
            console.log('this.group radius', this.group.getLayers())

            this.markers.forEach((marker) => {
                if (this.start.distanceTo(marker.getLatLng()) > this.radius) {
                    this.group.removeLayer(marker);
                    //Remove from servicePoints
                } else if (!this.group.getLayers().find(l => l.options.point.properties.id === marker.options.point.properties.id)) {
                    if($(`#service-selected-${marker.options.serviceId}`).hasClass( "selected" )) {
                        this.group.addLayer(marker);
                    }
                }
            });
            console.log('this.group radius', this.group.getLayers())

            this.lastEdit = "radius";
            this.groupFilteredByFacette();
        }
    }

    cityChanged() {
        console.log('cityChanged-----------------', this.startcityLimit)
        this.radius = null;
        if (this.buffer !== null) {
            this.map.removeLayer(this.buffer);
            this.buffer = null;
        }
        if (this.currentDiameter) {
            this.map.removeLayer(this.currentDiameter);
            this.currentDiameter = null
        }
        if (this.currentCityLimit) {
            this.map.removeLayer(this.currentCityLimit);
            this.currentCityLimit = null
        }
        if (this.startcityLimit) {
            const polygon = turf.polygon([[[-180, -80], [180, -80], [180, 80], [-180, 80], [-180, -80]]], { name: 'all' });

            try {
                jQuery('#legend-title').text("Résultats");
                const geoLimit = this.startcityLimit.toGeoJSON().features[0].geometry;

                //Display city outline by making the whole world except the city grey
                const diff = turf.difference(polygon, geoLimit);

                this.group = this.group
                this.group.removeLayers(this.group.getLayers())

                this.currentCityLimit = L.geoJSON(diff, { fillOpacity: 0.15, opacity: 0.3, fillColor: "#000", color: "#000" });

                this.map.addLayer(this.currentCityLimit);

                console.log('total markers', this.markers.length);
                console.log('total group layers', this.group.getLayers().length);
                let cnt = 0;

                let onMap = this.group.getLayers().map((m) => {
                    return m.options.point.properties.id;
                });

                this.markers.forEach((marker) => {
                    const isInPolygon = turf.pointsWithinPolygon(marker.toGeoJSON(), geoLimit);

                    const exists = onMap.indexOf(marker.options.point.properties.id) >= 0;

                    if (isInPolygon.features.length === 0 && exists) {
                        this.group.removeLayer(marker);
                        cnt ++;
                    } else if (!exists && isInPolygon.features.length > 0) {
                        this.group.addLayer(marker);
                        // console.log('marker', marker);
                        cnt ++;
                    }
                });
                console.log('count done', cnt);
            } catch(e) {
                jQuery('#legend-title').text("Légende");
                console.log(e);
            }
        }
    }

    hidePoints(ids) {
        this.map.closePopup();
        const grp = new L.FeatureGroup();
        this.markers.forEach((marker) => {
            const toRemove = ids.indexOf(marker.options.point.properties.id) === -1;

            if (toRemove) {
                this.group.removeLayer(marker);
            } else if (!this.group.hasLayer(marker)) {
                this.group.addLayer(marker);
                marker.addTo(grp);
            } else {
                marker.addTo(grp);
            }
        });
    }

    restorePoints(ids = null) {
        this.markers.forEach((marker) => {
            // if an array of ids is passed, only restore those markers
            // Otherwise restore all points
            let restore = true;
            if (ids) {
                restore = ids.find(id => id == marker.options.point.properties.id)
            }
            if (!this.group.hasLayer(marker) && restore && $(`#service-selected-${marker.options.serviceId}`).hasClass( "selected" )) {
                this.group.addLayer(marker);
            }
        });
        this.groupFilteredByFacette();
    }

    addressAutocomplete(partialAddress) {
        const point = turf.centroid(this.limit.toGeoJSON());
        console.log('centroid is', point);
        const coords = {
            lat: point.geometry.coordinates[1],
            lon: point.geometry.coordinates[0],
        };
        console.log('centroid coords', coords);
        return autoComplete(partialAddress, this.apiKey, coords);
    }

    bufferChanged(newBuffer) {
        console.log('bufferChanged-----')
        if (this.currentDiameter) {
            this.map.removeLayer(this.currentDiameter);
            this.currentDiameter = null;
        }
        if (this.currentCityLimit) {
            this.map.removeLayer(this.currentCityLimit);
            this.currentCityLimit = null;
        }
        if (this.startcityLimit) {
            this.map.removeLayer(this.startcityLimit);
            this.startcityLimit = null;
        }
        if (this.endcityLimit) {
            this.map.removeLayer(this.endcityLimit);
            this.endcityLimit = null;
        }
        this.bufferRadius = newBuffer;
        this.getPointsInBufferZone();
        if (this.filters && this.filters.length) {
            console.log('this.filters', this.filters)
            this.serviceFiltered(this.filters, true); // true to reset markers or it duplicates
        }
    }

    startOrEndAddress(data, start = 'start') {
        // WORK --
        console.log('startOrEndAddress', data, start)
        if (this.currentDiameter) {
            this.map.removeLayer(this.currentDiameter);
            this.currentDiameter = null
        }
        jQuery('#legend-title').text("Résultats");
        return new Promise((resolve, reject) => {
            if (typeof this.homeMarker !== 'undefined' && this.homeMarker !== null) {
                this.map.removeLayer(this.homeMarker);
            }

            if (typeof this.route !== 'undefined' && this.route !== null) {
                this.map.removeLayer(this.route);
            }
            if (data && data.length > 0) {
                const newAddress = data[0].loc.properties.label;
                console.log('newAddress', newAddress);
                if (data[0].loc.properties.type === 'municipality') {
                    // This is a city, get commune bounds
                    const insee = data[0].loc.properties.id;
                    this.api.get(`/insee/1/${insee}`).then((resp) => {
                        console.log(resp);
                        try {

                            if (this[start+'cityLimit']) {
                                this.map.removeLayer(this[start+'cityLimit']);
                            }
                            this[start+'cityLimit'] = L.geoJson(resp, { style: { fillOpacity: 0, color: 'black', weight:1 } });
                            this[start+'cityLimit'].addTo(this.map);

                            if (start === 'start') {
                                this.map.fitBounds(this[start+'cityLimit'].getBounds());
                            }

                            if (this.end === null || this.start === null) {
                                resolve(data[0].loc);
                            }
                        } catch (e) {
                            console.log(e);
                        }
                    }).catch((er) => {
                        console.log(er);
                    });
                }

                if (typeof this[start + 'Marker'] !== 'undefined' && this[start + 'Marker'] !== null) {
                    this.map.removeLayer(this[start + 'Marker']);
                }

                this[start] = L.latLng(data[0].loc.geometry.coordinates[1], data[0].loc.geometry.coordinates[0]);

                //this.map.panTo(this[start]);
                this[start + 'Marker'] = L.marker(this[start], { title: newAddress, icon: L.AwesomeMarkers.icon({ icon: start === 'start' ? "" : "", prefix: 'fa', markerColor: start === 'start' ? "gray" : "building", iconColor: 'white' }) });
                console.log("this[start + 'Marker']", this[start + 'Marker']);
                this[start + 'Marker'].bindPopup(newAddress);
                this.map.addLayer(this[start + 'Marker']);

                if (this.end !== null && this.start !== null) {
                    // We have both points, draw the itinerary, the buffer and find points
                    this.calculateRoute(this.start, this.end).then((result) => {
                        this.getPointsInBufferZone();
                        resolve(data[0].loc);
                    }).catch((e) => console.log(e) && reject(e));
                } else if(this.end === null && data[0].loc.properties.type !== 'municipality') {
                    resolve(data[0].loc);
                }

                if (this.endMarker === null) {
                    console.log('this.context', this.context)
                    this.markers.forEach((m) => {
                        if (m._popup._content.indexOf('itinerary') === -1) {
                            //m._popup.setContent(m._popup._content + '<button data-point-id="' + m.options.point.properties.id + '" class="btn btn-primary itinerary"><i class="fa fa-road"></i> Itinéraire</button>')
                            let title = '';
                            if (m.options.point.properties && m.options.point.properties.name) {
                                title = m.options.point.properties.name;
                            }

                            const service = this.services.find(s => s.id === m.options.serviceId) ? this.services.find(s => s.id === m.options.serviceId) : this.context.services.find(s => s.id === m.options.serviceId)
                            console.log('ervice123', service)

                            // CUSTOM SVG ICON OR FONT AWESOME
                            let iconContent;
                            if (service) {
                                if (service.iconColor && service.iconColor.icon_svg) {
                                    iconContent = `<img class="svg-tooltip" src="${this.baseUrl}/storage/${service.iconColor.icon_path}" alt="${service.iconColor.icon}" style="color: ${pickColor(service.iconColor.color)} !important;max-width: 18px !important;z-index:2"></i>`
                                } else {
                                    iconContent = '<i class="' + service.iconColor.icon + '" style="color: ' + pickColor(service.iconColor.color) + ' !important"></i>';
                                }
                                m._popup.setContent(this._popupContents(m.options.point, title, service, iconContent));
                            }
                        }
                    });
                }
            }
        });
    }

    getPointsInBufferZone() {
        if (this.route === null) {
            return
        }
        if (this.buffer !== null) {
            this.map.removeLayer(this.buffer);
            this.buffer = null;
        }
        if (this.currentCityLimit !== null) {
            this.map.removeLayer(this.currentCityLimit);
            this.currentCityLimit = null;
        }
        this.group.removeLayers(this.group.getLayers())

        const points = turf.points(this.markers.map((marker) => {
            return marker.toGeoJSON().geometry.coordinates;
        }));

        const buffer = turf.buffer(this.route.toGeoJSON(), this.bufferRadius);
        const within = turf.pointsWithinPolygon(points, buffer);

        const polygon = turf.polygon([[[-180, -80], [180, -80], [180, 80], [-180, 80], [-180, -80]]], { name: 'all' });
        const diff = turf.difference(polygon, buffer.features[0]);

        const g = L.geoJSON(polygon);
        g.setStyle({ fillColor: "#f00", color: "#f00", fillOpacity: 0.15, opacity: 0.3 });

        this.buffer = L.geoJson(diff);
        this.map.fitBounds(L.geoJSON(buffer).getBounds());
        this.buffer.setStyle({ fillColor: "#000", color: "#000", fillOpacity: 0.15, opacity: 0.3 });
        this.buffer.addTo(this.map);

        if (!this.group || this.group.length === 0) {
            return;
        }

        let onMap = this.group.getLayers().map((m) => {
            return m.options.point.properties.id;
        });

        let cnt = 0;
        this.markers.forEach((marker) => {
            // console.log('marker', marker)
            const markerCoords = marker.toGeoJSON().geometry.coordinates;
            const found = within.features.find((feature) => feature.geometry.coordinates[0] === markerCoords[0] && feature.geometry.coordinates[1] === markerCoords[1]);

            if (typeof found !== 'undefined' && found !== null && onMap.indexOf(marker.options.point.properties.id) < 0 && $(`#service-selected-${marker.options.point.properties.serviceId}`).hasClass( "selected" )) {
                // this point is in the buffer, but not yet on the map, add it
                this.group.addLayer(marker);
                cnt++;
            } else if ((found === null || typeof found === 'undefined') && onMap.indexOf(marker.options.point.properties.id) >= 0){
                // This point
                this.group.removeLayer(marker);
                cnt++;
            }
        });

        console.log('cnt', cnt);

        this.lastEdit = "buffer";
        this.groupFilteredByFacette();
    }

    calculateRoute(from, to) {
        return new Promise((resolve, reject) => {
            routing({
                x: from.lng,
                y: from.lat,
            },
                {
                    x: to.lng,
                    y: to.lat,
                },
                this.apiKey).then((result) => {


                    if (typeof this.route !== 'undefined' && this.route !== null) {
                        this.map.removeLayer(this.route);
                    }

                    this.route = L.geoJson(result.routeGeometry, { weight: 6 });
                    this.map.fitBounds(this.route.getBounds());

                    this.route.addTo(this.map);
                    resolve(result);
                }).catch((e) => reject(e));
        })

    }

    routeTo(destPointId) {
        const marker = this.markers.find(p => p.options.point.properties.id === destPointId);

        const dest = new L.LatLng(...marker.options.point.geometry.coordinates);

        this.calculateRoute(this.start, dest).then((result) => {
            const content = jQuery(marker._popup._content).wrap('<div>').parent();

            content.find('p.button').remove();
            content.find('p.button-select').remove();
            content.append('<p>Distance : ' + result.totalDistance + '</p><p>Temps de parcours : ' + Math.round(result.totalTime / 60) + ' min</p>');
            content.append('<p class="button-select"><button data-point-id="' + marker.options.point.properties.id + '" class="btn btn-info select-choose"><i class="fa fa-plus-circle"></i> Ajouter à ma sélection</button></p>');
            marker.options.route = this.route;

            marker._popup.setContent(content.html())
        })
    }

    /**
     * Reset map
     */
    resetMap() {
        jQuery('#legend-title').text("Légende");
        console.log('resetting map')
        //window.location.reload()
        //je réaffiche tous les points
        this.restorePoints();
        //je réinitialise le rond et le buffer
        if (typeof this.currentDiameter !== 'undefined' && this.currentDiameter !== null) {
            this.map.removeLayer(this.currentDiameter);
            this.currentDiameter = null;
        }
        if (typeof this.buffer !== 'undefined' && this.buffer !== null) {
            this.map.removeLayer(this.buffer);
            this.buffer = null;
        }
        if (typeof this.homeMarker !== 'undefined' && this.homeMarker !== null) {
            this.map.removeLayer(this.homeMarker);
            this.homeMarker = null;
        }
        if (typeof this.startMarker !== 'undefined' && this.startMarker !== null) {
            this.map.removeLayer(this.startMarker);
            this.startMarker = null;
        }
        if (typeof this.endMarker !== 'undefined' && this.endMarker !== null) {
            this.map.removeLayer(this.endMarker);
            this.endMarker = null;
        }

        if (typeof this.route !== 'undefined' && this.route !== null) {
            this.map.removeLayer(this.route);
            this.route = null;
        }

        if (typeof this.startcityLimit !== 'undefined' && this.startcityLimit !== null) {
            this.map.removeLayer(this.startcityLimit);
            this.startcityLimit = null;
        }

        if (typeof this.endcityLimit !== 'undefined' && this.endcityLimit !== null) {
            this.map.removeLayer(this.endcityLimit);
            this.endcityLimit = null;
        }

        if (typeof this.currentCityLimit !== 'undefined' && this.currentCityLimit !== null) {
            this.map.removeLayer(this.currentCityLimit);
            this.currentCityLimit = null;
        }

        //Fit to limit bounds on reset
        if (typeof this.limit !== 'undefined' && this.limit !== null) {
            this.map.fitBounds(this.limit.getBounds());
        }

        this.lastEdit = null;
        this.lastFacettes = null;
    }

    /**
     * Generate the popup contents for this marker
     * @param point
     * @param title
     * @param service
     * @returns {string}
     * @private
     */
    _popupContents(point, title, service, iconContent) {
            let newTitle = ""
            if (service && service.iconColor) {
                newTitle = point.geometry.type === 'Point' || point.geometry.type === 'MultiPoint' ? '<p class="title" style="margin-top: 5px;"><span class="icon" style="background-color:' + service.iconColor.color + ' !important"> ' + iconContent + '</span> <span class="text" style="color:' +  service.iconColor.color + ' !important">' + title + '</span></p>' : '<p class="title"><span class="text" style="color:' + service.outline + '">' + title + '</span></p>';
            }

            let contents = '';
            Object.keys(point.properties).map(function (key) {
                if (key !== 'name' && key !== 'id' && key !== 'serviceId' && point.properties[key] && point.properties[key] !== '') {
                    contents += '<p id="popup-custom-content">' + key.toUpperCase() + '&nbsp;: <strong>' + point.properties[key] + '</strong></p>';
                }
            });
            let buttons = '';
            if (this.context.display_selection) {
                // buttons = '<button data-point-id="' + point.properties.id + '" class="btn btn-info select-choose"><i class="fa fa-plus-circle"></i> Ajouter à ma sélection</button>';
                buttons = '<button data-point-id="' + point.properties.id + '" class="btn btn-info select-choose"><i class="fa fa-plus-circle"></i> SÉLECTION</button>';
            }
    
            let h = 36;
    
            if (this.start !== null) {
                buttons += '<button data-point-id="' + point.properties.id + '" class="btn btn-primary itinerary" style="margin-left: 0.5em;"><i class="fa fa-road"></i> Itinéraire</button>';
            }
    
            return newTitle + '<div class="scrollable-popup-content" style="height:'+(240-h)+'px">' + contents + '</div>' + buttons;
    }

    /**
     * Make a cluster group from a list of markers
     *
     * @param serviceMarkers
     * @private
     */
    _makeGroup(serviceMarkers) {
        const group = L.markerClusterGroup({
            iconCreateFunction: function (cluster) {
                return L.divIcon({ html: `<div class="group-marker-icon" style="background-color:#ccc !important;color:#000 !important"><span>${cluster.getChildCount()}</span></div>`, className: 'custom-cluster', iconSize: L.point(40, 40) });
            }
        });

        group.addLayers(serviceMarkers.filter((marker) => {
            return !(this.start !== null && this.radius !== null && marker.getLatLng().distanceTo(this.start) > this.radius);
        }));

        return group;
    }

    /**
     * Generate a marker or this point and this service
     * @param point
     * @param service
     * @param id
     * @return Marker
     */
    _makeMarker(point, service, id) {
        let marker = null;

        let title = "";
        if (point.properties && point.properties.name) {
            title = point.properties.name;
        }
        const iconContent = service.iconColor.icon_svg ? `<img class="svg-tooltip" src="${this.baseUrl}/storage/${service.iconColor.icon_path}" alt="${service.iconColor.icon}" style="color: ${pickColor(service.iconColor.color)}!important;max-width: 18px!important;z-index:2"></i>` : '<i class="' + service.iconColor.icon + '" style="color: ' + pickColor(service.iconColor.color) + ' !important"></i>'
        const text = this._popupContents(point, title, service, iconContent);

        if (point.geometry.type === 'Point') {
            // Coordinates are reversed
            const coords = new L.LatLng(...point.geometry.coordinates.reverse());

            // CUSTOM SVG ICON OR FONT AWESOME
            marker = L.marker(coords, {
                title: title,
                icon: L.divIcon({
                    html: '<div style="border-color:' + service.iconColor.color + ' !important"><div>' + iconContent + '</div></div>',
                    iconSize: [20, 20],
                    className: 'service-point-icon',
                    iconAnchor: [17, 54],
                    popupAnchor: [0, -50],
                }),
                point: point,
                serviceId: service.id,
            });
        }

        if (point.geometry.type === 'MultiPoint') {

            const coords = new L.LatLng(...point.geometry.coordinates[id].reverse());
            marker = L.marker(coords, {
                title: title,
                icon: L.divIcon({
                    html: '<div style="border-color:' + service.iconColor.color + ' !important"><div>' + iconContent + '</div></div>',
                    iconSize: [20, 20],
                    className: 'service-point-icon',
                    iconAnchor: [17, 54],
                    popupAnchor: [0, -50],
                }),
                point: point,
                serviceId: service.id,
            });
        }

        marker.bindPopup(text);
        marker.on('click', (e) => {
            if (typeof e.target.options.route !== 'undefined') {
                //We already have an itinerary for this marker, display it
                if (this.route !== null) {
                    this.map.removeLayer(this.route);
                    this.route = e.target.options.route;
                    this.map.addLayer(this.route);
                }
            }
        });

        return marker;
    }
    /**
     * Generate a polygon or this point and this service
     * @param polygon
     * @param service
     * @return Polygon
     */
    _makePolygon(polygon, service) {
        let title = "";
        if (polygon.properties && polygon.properties.name) {
            title = polygon.properties.name;
        }

        // Coordinates are reversed
        polygon.geometry.type === 'Polygon' &&  polygon.geometry.coordinates.map( layer => layer.map(arr => {arr.reverse()}))
        polygon.geometry.type === 'MultiPolygon' &&  polygon.geometry.coordinates.map(multi => multi.map( layer => layer.map(arr => {arr.reverse()})))

        // CREATE POLYGON
        const polygone = L.polygon(polygon.geometry.coordinates, {
            title: polygon.properties.name,
            color: service.outline,
            fillColor : service.fill,
            point: polygon,
            serviceId: service.id,
        })

        const text = this._popupContents(polygon, title, service);
        polygone.bindPopup(text)

        return polygone;
    }
    /**
     * Generate a line or this point and this service
     * @param line
     * @param service
     * @return Line
     */
    _makeLine(line, service) {

        let title = "";
        if (line.properties && line.properties.name) {
            title = line.properties.name;
        }

        // Coordinates are reversed
        line.geometry.type === 'LineString' &&  line.geometry.coordinates.map(arr => {arr.reverse()})
        line.geometry.type === 'MultiLineString' &&  line.geometry.coordinates.map(multi => multi.map( arr => {arr.reverse()}))

        // CREATE POLYGON
        const polyline = L.polyline(line.geometry.coordinates, {
            title: line.properties.name,
            color: service.outline,
            point: line,
            serviceId: service.id,
        })
        const text = this._popupContents(line, title, service);
        polyline.bindPopup(text)

        return polyline;
    }
}

export default ResearchContext;
