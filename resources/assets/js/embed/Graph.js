import * as utils from './Utils'

class Graph {
    constructor(item) {
        //Can be a board or a context
        // Should have indicators and indicator_names
        this.item = item;

        this.services = [];
        this.moveSteps = [];
        this.activeBar = null;
        this.controls = null;
        this.chart = null;
        this.indicator = 0;
        this.results = null;
    }

    setControls(controls) {
        this.controls = controls
    }

    setMoveSteps(moveSteps) {
        this.moveSteps = moveSteps;
    }

    setIndicator(indicator) {
        this.indicator = indicator;
    }

    setResults(results) {
        this.results = results;
    }

    display() {
        console.log('RESULTS', this.results);
        const color = [];

        const colors = [
            'rgb(206,255,69)',
            'rgb(246,255,104)',
            'rgb(255,223,128)',
            'rgb(255,178,134)',
            'rgb(255,120,120)',
            'rgb(255,91,135)',
        ];
        const data = this.results.map((res, i) => {
            let dataset, label, fullValue = [], currValue = [];

            if (this.indicator !== false) {
                let vals = res.props;
                const indic = this.item.indicators[this.indicator]
                currValue = [res.props[indic]];

                if (res.territoire_props && res.territoire_props[indic]) {
                    dataset = [res.props[indic] / res.territoire_props[indic] * 100];
                    fullValue = [res.territoire_props[indic]];
                } else {
                    dataset = [vals[indic]];
                }

                label = this.moveSteps[i].value;
            } else {
                dataset = this.item.indicators.map((indic) => {
                    currValue.push(res.props[indic]);
                    if (res.territoire_props && res.territoire_props[indic]) {
                        fullValue.push(res.territoire_props[indic]);
                        return res.props[indic] / res.territoire_props[indic] * 100;
                    }

                    return res.props[indic];
                });

                label = this.item.layers.find(l => {
                    return l.context_step_move_id === res.step_id || l.id === res.id;
                }).name;
            }

            // Pick the full range of available colors
            const index = i * Math.ceil(colors.length / this.results.length);
            if (typeof colors[index] !== 'undefined') {
                color[i] = colors[index];
            } else {
                color[i] = colors[5];
            }

            return {
                label,
                data: dataset,
                fullValue,
                currValue,
                backgroundColor: color[i],
                borderWidth:1,
                //hoverBackgroundColor: 'rgba(236,240,245,1)',
                borderColor: 'rgba(0, 0, 0, 0.1)'
            };
        });
        //affichage du graphe
        let labels = [utils.formatLabel(this.item.indicator_names[this.indicator], 30)];
        if (this.indicator === false) {
            labels = this.item.indicator_names.map(i => utils.formatLabel(i, 20))
        }

        console.log('labels', labels);
        console.log('data', data);
        const ctx = document.getElementById('myChart').getContext('2d');
        this.chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: data
            },
            options: {
                scales: {
                    xAxes: [{ barPercentage: 0.5 }],
                    yAxes: [{
                        ticks: {
                            precision: 0,
                            beginAtZero: true,
                            callback: function (value) {
                                return (value / 100).toLocaleString('fr-FR', {style:'percent'});
                            },
                        }
                    }],
                },
                title: {
                    display: false,
                    text: "ffrf"
                },
                hover: {
                    mode:'dataset',
                },
                tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        label: function(tooltipItems, data) {
                            let multistringText = ['Pourcentage : ' + Math.round(tooltipItems.yLabel) + '%'];
                            const cv = Math.round(data.datasets[tooltipItems.datasetIndex].currValue[tooltipItems.index]);
                            let fv = 'Inconnu';
                            if (typeof data.datasets[tooltipItems.datasetIndex].fullValue !== 'undefined') {
                                fv = Math.round(data.datasets[tooltipItems.datasetIndex].fullValue[tooltipItems.index]);
                            }

                            multistringText.push(cv + ' sur un total de ' + fv);
                            return multistringText;
                        }
                    }
                },
                onHover: () => {
                    let activeBar = null;
                    if (this.chart.active && this.chart.active.length){
                        activeBar = this.chart.active[0]._datasetIndex;
                        for (let j=0; j < data.length; j++){

                            if (j <= this.chart.active[0]._datasetIndex){
                                this.chart.config.data.datasets[j].backgroundColor = color[j];
                            } else {
                                this.chart.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
                            }

                        }
                    }

                    if (this.activeBar !== activeBar) {
                        this.chart.update();
                        this.activeBar = activeBar;
                    }

                },
                onClick: (ev) => {
                    ev.preventDefault();
                    // Set context move step ID here
                    if (this.chart.active && this.chart.active.length > 0 && typeof this.moveSteps[this.chart.active[0]._datasetIndex] !== 'undefined') {
                        const clickedId = this.moveSteps[this.chart.active[0]._datasetIndex].id;

                        let showResult = this.results.find(r => clickedId === r.id);
                        if (!showResult) {
                            //find for context
                            showResult = this.results.find(r => clickedId === r.step_id);
                        }

                        if (showResult && typeof showResult.geom !== 'undefined') {
                            // Add layer to map here
                            this.controls.graphClicked(showResult, this.chart.active[0]._view.backgroundColor);
                        }

                        for (let i = this.chart.active[0]._datasetIndex - 1; i >= 0; i--) {
                            const clickedId = this.moveSteps[i].id;

                            let showResult = this.results.find(r => clickedId === r.id);
                            if (!showResult) {
                                //find for context
                                showResult = this.results.find(r => clickedId === r.step_id);
                            }

                            if (showResult && typeof showResult.geom !== 'undefined') {
                                // Add layer to map here
                                this.controls.graphClicked(showResult, this.chart.data.datasets[i].backgroundColor, false);
                            }
                        }
                    }
                    jQuery('.leaflet-marker-shadow').css('display', 'none')
                    jQuery('.leaflet-marker-icon').css('display', 'none')
                }
            }
        });

        const clickedId = this.moveSteps[0].id;

        let showResult = this.results.find(r => clickedId === r.id);
        if (!showResult) {
            //find for context
            showResult = this.results.find(r => clickedId === r.step_id);
        }
        //Make other bars grey
        for (let j=1; j < this.chart.config.data.datasets.length; j++){
            this.chart.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
        }
        this.chart.update();

        if (showResult && typeof showResult.geom !== 'undefined') {
            // Add layer to map here
            this.controls.graphClicked(showResult, this.chart.config.data.datasets[0].backgroundColor);


        }
    }

    destroy() {
        this.chart.destroy();
        this.chart = null;
    }
}

export default Graph;
