const main = `
<div class="row cartosdaasap-embed" id="embed-script-context">
        <div class="col-md-12">
            
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title" id="context-name"></h3>
                    <div class="row">
                            <div class="col-md-3" id="change-service">
                            </div>
                            <div class="col-md-3" id="change-zone">
                            </div>
                            <div class="col-md-3" id="change-indicator">
                            </div>
                            <div class="col-md-3" id="change-movetype">
                            </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <div class="callout callout-warning" style="display:none" id="disabled-message">
                <h4>Ce contexte est désactivé</h4>

                <p>Les informations présentées ne sont peut-être pas à jour.</p>
            </div>
                    <div id="show_layer">
                        <div class='col-md-5' id='box_chart_comment'>
                            <div class='box'>
                               <canvas id='myChart'></canvas>
                               </div>
                           <div class='box'>
                            <div class='box-header with-border'>
                         <h3 class='box-title'>Synthèse statistique :</h3>
                                    </div>
                                <div class='box-body with-border' id="stats-summary">
                                
                               </div>
                              </div>
                              <div class='box box-warning'>
                            <div class='box-header with-border'>
                         <h3 class='box-title'>Télécharger les couches :</h3>
                                    </div>
                                <div class='box-body with-border' id="download-layers">
                                
                               </div>
                              </div>
                            </div>
                             
                        <div class='col-md-7'>
                            <div id='maCarte' class='col-md-12' style='height: 75vh;'>
                               </div>
                            </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                                
                </div>
            </div>
        </div>
    </div>
`;

export default main;
