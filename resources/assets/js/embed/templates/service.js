const board = `
    <div class="row cartosdaasap-embed">
        <div class="col-md-12">
            
            <div class="box box-primary" id="service-main">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id='maCarte' style='height: 75vh;'></div>
                    <div class="callout callout-warning" style="display:none" id="disabled-message">
                    <h4>Ce tableau de bord est désactivé</h4>
    
                    <p>Les information présentées ne sont peut-être pas à jour.</p>
                </div>
            </div>
            
        </div>
    </div>
`;

export default board;
