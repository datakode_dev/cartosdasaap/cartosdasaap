const board = `
    <div class="row cartosdaasap-embed">
        <div class="col-md-12">
            
            <div class="box box-primary" id="board-main">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <label for="change-limit" class="sr-only">Changement de territoire</label>
                        <select id="change-limit" class="select2_ajax btn-box-tool" style="width:100%">

                        </select>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <div class="callout callout-warning" style="display:none" id="disabled-message">
                <h4>Ce tableau de bord est désactivé</h4>

                <p>Les information présentées ne sont peut-être pas à jour.</p>
            </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-success box-solid" id="board-graph">
                                <div class="box-header with-border">
                                    <h3 class="box-title">&nbsp;</h3>
                                </div>
                                <div class="box-body">
                                    <canvas id='myChart'></canvas>
                                </div>
                            </div>
                            <div class="box box-danger box-solid" id="stats">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Territoires les mieux couverts</h3>
                                </div>
                                <div class="box-body">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id='maCarte' class='box col-md-12' style='height: 75vh;'>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>
            </div>
        </div>
    </div>
`;

export default board;
