const board = `
    <div class="row cartosdaasap-embed" id="search-context-main">
        <div class="col-md-12" style="display:none;" id="disabled-message">
            <div class="callout callout-warning">
                <h4>Ce contexte de recherche est désactivé</h4>

                <p>Les information présentées ne sont peut-être pas à jour.</p>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="search-fields">
                <form id="search-itinerary-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group show-city">
                                <label for="search-start" id="search-start-label">Rechercher une adresse ou une commune</label>
                                <select id="search-start" class="address-search"></select>
                            </div>
                            <div class="form-group end-point-search itinerary" style="display:none;">
                                <label for="search-end">Arrivée</label>
                                <select id="search-end" class="address-search"></select>
                            </div>
                            <div class="form-group itinerary">
                                <button class="btn btn-primary itinerary-btn"><i class="fa fa-map-signs"></i> Itinéraire</button>
                                <button class="btn btn-warning reset_button"><i class="fa fa-refresh"></i> Réinitialiser</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group has-feedback buffer distance">
                                <label for="buffer">Distance (km)</label>
                                <div class="range-values">
                                    <div class="range-value min">0,25km</div>
                                    <span class="current" style="display:none"></span>
                                    <div class="range-value max">10km</div>
                                </div>
                                <input id="buffer" type="range" min="0.25" step="0.25" max="10" name="buffer" value="5" class="custom-range" placeholder="form.placeholder.buffer">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-2 my-selection">
            <button class="btn btn-primary open-selection">Ma sélection (<span class="sel-num">0</span>)</button>
        </div>
        <div id="filters-box" class="col-md-10 col-md-offset-2 row" style="display:none">
            <div class="box box-warning"> 
                <div class="box-header with-border">
                    <h3 class="box-title">Filtrer sur des critères avancés</h3>
                </div>
                <div class="box-body" id="filters">
                    <div class="col-md-12"><button id="reset-filters" class="btn btn-warning btn-sm" style="display: none;">Réinitialiser les filtres</button></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 display_legend">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title" id="legend-title">Légende</h3>
                    <div id="pdfGenerateAll" class="box-tools pull-right info-box-content" style="padding-top:0;">
                        <button id="download_pdf_all" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="search-services">
                    <p class="no-selection">Aucune selection</p>
                    <script type="text/template" id="search-service-template">
                        <div class="info-box sm selected">
                            <span class="info-box-icon"><span></span><i></i></span>

                            <div class="info-box-content">
                                <a class="btn btn-sm remove-all pull-right btn-group" disabled title="Enlever tous les points de la sélection" style="display:none;"><i class="fa fa-minus"></i></a>
                                <a class="btn btn-sm add-all pull-right btn-group" title="Ajouter tous les points de la sélection" style="display:none;"><i class="fa fa-plus"></i></a>
                                <span class="info-box-text"></span>
                                <span class="info-box-number"></span>
                                <span class="info-box-recherche"></span>
                            </div>
                        </div>
                        <ul class="selection">
                        </ul>
                    </script>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-8 map-section">
                <!-- /.box-header -->
                <div  id="search-map" style="height: 600px" data-context-id="{{ $context->id }}"  data-api-key="{{ $key->key }}">
                </div>
                <!-- /.box-body -->
        </div>
        <div class="modal fade in" id="modal-my-selection" style="display: none;" >
            <div class="modal-dialog">
                <div class="modal-content" style="max-height: 600px !important;overflow-y: scroll;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Fermer">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4 class="modal-title">Ma sélection</h4>
                    </div>
                    <div class="modal-body my-selection-details">
                        <ul class="selection">
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" id="download_pdf_selection">Exporter le PDF</a>
                        <button type="button" class="btn btn-danger" id="reset">Vider la sélection</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
`;

export default board;
