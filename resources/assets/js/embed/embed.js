import Controls from './Controls';
import main from './templates/main';
import Api from './Api';

/*
Usage : <script src="/js/embed.js" data-context-id="CONTEXT_ID"></script>

Where CONTEXT_ID is the id of the context to display
 */

const thisJs = document.querySelector("script[src*='embed.js']");

const url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

const baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
console.log('baseurl', baseUrl);
/**
 *
 * @param url
 * @returns {Promise}
 */
const loadScript = (url) => {
    return new Promise((resolve, reject) => {
        const script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

const loadjQuery = () => {
    if (window.jQuery) {
        console.log('jquery ok')
        return Promise.resolve();
    } else {
        return new Promise(function (resolve) {
            setTimeout(() => {
                if (window.jQuery) {
                    console.log('jquery loaded')
                    resolve()
                } else {
                    console.log('jquery loaded from elsewhere')
                    loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(() => {
                        resolve();
                    });
                }
            }, 2000);
        });
    }
};

let css = document.createElement("link");
css.rel = "stylesheet";
css.href = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css";
document.getElementsByTagName("head")[0].appendChild(css);

css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

(function () {
    const contextId = thisJs.getAttribute('data-context-id');

    const jquery = loadjQuery();


    // Is it worth it to load select2 here ?

    jquery.then(() => {
        const chartJs = loadScript('//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js');

        let leaflet = Promise;
        if (typeof window.L === 'undefined') {
            //leaflet loaded
            leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');
        } else {
            leaflet = Promise.resolve();
        }
        
        const select2 = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js');
        Promise.all([chartJs, leaflet, select2]).then(() => {
            loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js');
            const fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            fullscreen.then(() => {
                //everything loaded and ready to go
                if (typeof $ === 'undefined') {
                    window.$ = window.jQuery;
                }
                jQuery(document).ready(() => {
                    const api = new Api(baseUrl + '/api');
                    jQuery(thisJs).after(jQuery(main));
                    api.get(`/contexts/${contextId}`).then((context) => {
                        if (context.active === false) {
                            jQuery('#disabled-message').show()
                        }
                        const controls = new Controls(context, baseUrl, window.jQuery);
                        controls.displayAll();
                        const event = new Event('proxiclicMap.loaded');
                        window.dispatchEvent(event);
                    }).catch(err => {
                        jQuery('#embed-script-context').html('<div class="col-md-12"><div class="alert alert-danger">\n' +
                            '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le contexte.</h4>\n' +
                            '    Ce site web n\'est sans doute pas autorisé à intégrer ce contexte.\n' +
                            '    </div></div>');
                        return console.log(err);
                    });
                });
            });
        });
    });
})();
