
import search from './templates/search';

import ResearchContext from "./ResearchContext";
import { researchEventHandlers } from "./contextEvents";

/*
Usage : <script src="/js/embed.js" data-context-id="CONTEXT_ID", data-api-key="IGN_API_KEY"></script>

Where CONTEXT_ID is the id of the context to display
 */

const thisJs = document.querySelector("script[src*='embed-search.js']");

const url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

const baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
/**
 *
 * @param url
 * @returns {Promise}
 */
const loadScript = (url) => {
    return new Promise((resolve, reject) => {
        const script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

const loadjQuery = () => {
    // if (window.jQuery) {
    //     return Promise.resolve();
    // } else {
        return new Promise(function (resolve) {
    //         setTimeout(() => {
    //             if (window.jQuery) {
    //                 console.log('jquery loaded')
    //                 resolve()
    //             } else {
    //                 console.log('jquery loaded from elsewhere')
                    loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(() => {
                        resolve();
                        window.$ = jQuery;
                    });
    //
    //             }
    //         }, 2000);
        });
    // }

};

const contextDidLoad = (ctx, err) => {
    console.log('--- contextDidLoad ---', researchContext)
    researchEventHandlers(window.jQuery, researchContext);
    if (err) {
        jQuery('#search-context-main').html('<div class="col-md-12"><div class="alert alert-danger">\n' +
            '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le contexte.</h4>\n' +
            '    Ce site web n\'est sans doute pas autorisé à intégrer ce contexte.\n' +
            '    </div></div>');
        return console.log(err);
    }
    const b = jQuery('body');
    if (ctx.active === false) {
        jQuery('#disabled-message').show();
    }

    const servDD = b.find('select#service');
    servDD.append(ctx.services.map((service, i) => {
        return `<option selected value="${service.id}" title="${service.info}">${service.name}</option>`
    }));

    b.find('.select2').select2();
    servDD.trigger('change');
    const event = new Event('proxiclicMap.loaded');
    window.dispatchEvent(event);
}

let researchContext;

(function () {
    const contextId = thisJs.getAttribute('data-context-id');

    const jquery = loadjQuery();
    const leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');
    // Is it worth it to load select2 here ?

    Promise.all([jquery, leaflet]).then(() => {
        const awesomeMarkers = loadScript(baseUrl + '/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.js');
        const bootstrap = loadScript(baseUrl + '/vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js');
        const turf = loadScript('https://npmcdn.com/@turf/turf/turf.min.js');
        const markerClusters = loadScript('https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js');
        const gpPlugin = loadScript(baseUrl + '/vendor/geoportal/GpPluginLeaflet.js');
        const select2 = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js');


        Promise.all([leaflet, select2, awesomeMarkers, markerClusters, gpPlugin, turf, bootstrap]).then(() => {
            const fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            const sel2lang = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js');

            Promise.all([fullscreen, sel2lang]).then(() => {
                //everything loaded and ready to go
                //if (typeof $ === 'undefined') {
                window.$ = window.jQuery;
                //}
                jQuery(document).ready(() => {

                    jQuery(thisJs).after(jQuery(search));
                    researchContext = new ResearchContext(contextId, contextDidLoad, baseUrl);

                    // HIDE EMBED SCRIPT ON BACK OFFICE PAGE
                    if (jQuery('#search-context-main.bo-view').length && jQuery('#search-context-main.cartosdaasap-embed').length) {
                        // jQuery('#search-context-main.cartosdaasap-embed').remove();
                        jQuery("#search-context-main.cartosdaasap-embed").prependTo("#search-context-main.bo-view");
                    }

                    // ----- HIDE TOOLS OPTIONS -----

                    setTimeout(() => { 
                        if (researchContext.context && !researchContext.context.display_route) {
                            jQuery('.itinerary-btn').hide()
                            jQuery('.distance').hide()
                        }
                        if (!researchContext.context.display_city) {
                            console.log('show-city', jQuery('.show-city'))
                            jQuery('.show-city').hide()
                            jQuery('.reset_button').hide()
                            jQuery('.distance').hide()
                        }
                        if (!researchContext.context.display_selection) {
                            console.log('select-choose', jQuery('.select-choose'))
                            jQuery('.my-selection').hide()
                            // Ajouter à la sélection Button are hidden
                        }
                        if (!researchContext.context.display_legend) {
                            console.log('display_legend', jQuery('.display_legend'))
                            jQuery('.display_legend').hide()
                            jQuery('.map-section').removeClass("col-md-8").addClass("col-md-12")
                        }
                        jQuery('.itinerary-btn').click( () => {
                            jQuery('.distance').show()
                        })
                        setTimeout(function(){ researchContext.map.invalidateSize()}, 400);
                    }, 1000);
                });
            });
        });
    });
})();

let css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

