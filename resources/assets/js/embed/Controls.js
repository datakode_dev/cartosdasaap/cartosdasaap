import Api from './Api';
import Graph from './Graph';
import Map from './Map';
import * as utils from './Utils'

class Controls {
    constructor(context, baseUrl, $) {
        this.graph = new Graph(context);
        this.graph.setControls(this);

        this.map = new Map(context);
        this.map.setControls(this);
        this.services = [];
        this.context = context;
        this.baseUrl = baseUrl;
        this.selectedService = null;
        this.api = new Api(baseUrl + '/api');
        this.box = jQuery('#embed-script-context');
        this.moveTypes = [];
        this.selectedMoveType = 0;
        this.moveSteps = [];
        // Selected zone
        this.insee = this.context.insee_limit;
        this.selectedIndicator = this.context.indicators[0];
    }


    displayAll() {
        utils.startLoading();
        this.load().then((results) => {
            this.addControls();
            utils.stopLoading();
            this.graph.setResults(results);
            this.graph.display();
            console.log('this', this);

            // Display initial context on map
            this.api.get(`/insee/${this.context.zone_id}/${this.context.insee_limit}`).then((resp) => {
                try {
                    this.map.changeGeometry(resp, 'black', [10, 10], 0);
                    setTimeout(() => {
                        this.map.markers.forEach(m => {
                            m.bringToFront();
                        });
                    }, 100);
                } catch (e) {
                    console.log(e);
                }
            }).catch((er) => {
                console.log(er);
            });

            /* https://app.asana.com/0/1179754124938634/1201174414928393 */
            jQuery('.leaflet-marker-shadow').css('display', 'none')
            jQuery('.leaflet-marker-icon').css('display', 'none')

        }).catch(er => {
            console.log(er);
            utils.fatalError("Impossible de charger vos données, celles-ci sont probablement en cours de calcul. Merci de réessayer plus tard.");
        });
    }

    graphClicked(layer, color, removePrevious = true) {
        try {
            const geom = this.map.activateLayer(layer, color, removePrevious, false);
        } catch (err) {
            console.log(err);
        }

        if (removePrevious) {
            const indic = this.selectedIndicator;
            const i = this.context.indicators.findIndex(ind => ind === this.selectedIndicator);
            const indicName = this.context.indicator_names[i];
            const moveType = this.moveTypes[this.selectedMoveType];
            const service = this.services.find(s => s.id == this.selectedService);
            const step = this.moveSteps.find(step => step.id === layer.step_id);
            const stepUnit = this.context.iso_type.name.toLowerCase() === 'isochrone' ? 'min' : 'm';

            let movetypeString = 'à <strong>pied</strong>';
            if (moveType.name === 'Voiture') {
                movetypeString = 'en <strong>voiture</strong>';
            }

            const percent = Math.round(layer.props[indic] / layer.territoire_props[indic] * 100);
            const text = 'les services de type <strong>"' + service.name + '"</strong> sont accessibles par <strong>' + percent + '%</strong> de la catégorie <strong>"' + indicName + '"</strong> à moins de <strong>' + step.value + ' ' + stepUnit + '</strong> ' + movetypeString;

            jQuery('#stats-summary').html(text);
        }


    }

    getMoveTypes(serviceId) {
        return this.api.get('/contexts/' + this.context.id + '/services/' + serviceId + '/movetypes');
    }

    displayMoveTypes(movetypes) {
        return new Promise((resolve, reject) => {
            // Only taking the first moveType
            // TODO handle different ones for this context
            console.log('movetypes', movetypes);
            this.moveTypes = movetypes;
            const movetype = movetypes[this.selectedMoveType];

            this.api.get('/contexts/' + this.context.id + '/services/' + movetype.iso_context_service_id + '/movetypes/' + movetype.id + '/movesteps').then((moveSteps) => {
                this.moveSteps = moveSteps;
                this.graph.setMoveSteps(moveSteps);
                console.log('got movestesps', moveSteps);
                const requests = moveSteps.map((step) => {
                    return this.api.get(`/accounts/${this.context.account_id}/layers/${step.id}?insee=${this.insee}`);
                });
                console.log('requests', requests);
                const result = Promise.all(requests);
                result.then((results) => {
                    console.log('results', results);
                    //Only get first result for the time being

                    // Display layer download links
                    const links = results.map(res => {
                        return `<li><a href="data:text/geojson;charset=UTF-8,${encodeURIComponent(res[0].geom)}" download="${res[0].layer_name}.geojson">${res[0].layer_name}</a></li>`;
                    }).join('');
                    $('#download-layers').html(`<ul>${links}</ul>`);

                    const parsed = results.map(res => {
                        console.log('single result', res);
                        if (res && res.length > 0) {
                            let geom = null;

                            try {
                                geom = JSON.parse(res[0].geom)
                            } catch (e) {

                                console.log('json parse error', e, res[0])
                            }
                            let props = null;
                            try {
                                props = JSON.parse(res[0].props)
                            } catch (e) {
                                console.log('json parse error', e, res[0])
                            }
                            let territoire_props = null;
                            try {
                                territoire_props = JSON.parse(res[0].territoire_props)
                            } catch (e) {
                                console.log('json parse error', e, res[0])
                            }

                            return {
                                step_id: res[0].step_id,
                                geom,
                                props,
                                territoire_props,
                            };
                        }
                        return null;
                    }).filter((r => r !== null));
                    if (parsed.length > 0) {
                        console.log('got parsed layer clips', parsed);
                        return resolve(parsed);
                    }

                    reject('no data');
                }).catch(er => reject(er));
            }).catch(er => reject(er));
        });
    }

    load() {
        return new Promise((resolve, reject) => {
            this.api.get('/contexts/' + this.context.id + '/services').then((services) => {
                this.services = services;

                if (services.length === 0) {
                    //No services, exit now
                    return
                }

                this.displayServiceMarkers(services[0]);


                this.selectedService = services[0].id;
                //Get layers for the first service
                const res = this.getMoveTypes(services[0].id);
                res.then((movetypes) => {
                    this.displayMoveTypes(movetypes).then(result => resolve(result)).catch(er => reject(er))
                }).catch(er => reject(er));
            }).catch(er => reject(er));
        });
    }

    addControls() {
        console.log('context', this.context);
        console.log('services', this.services);
        const box = this.box;
        if (this.services.length <= 1) {
            const servDiv = box.find('#change-service');
            servDiv.append('<label>Service</label>');
            servDiv.append('<div>' + this.services[0].name + ' <i class="fa fa-info-circle" title="'+this.services[0].info+'"></i></div>');

            servDiv.find('i').tooltip();

        } else {
            const ser = this.createSelect('Service', 'change-service', this.services.map(s => s.id), this.services.map(s => s.name));

            const servDiv = box.find('#change-service');
            servDiv.append(ser.label);
            servDiv.append(ser.select);
            ser.select.on('change', (ev) => {
                this.serviceChanged(jQuery(ev.target).val())
            });
        }
        if (this.context.indicators.length > 1) {
            const res = this.createSelect('Population', 'change-indicator', this.context.indicators, this.context.indicator_names);

            const inDiv = box.find('#change-indicator');
            inDiv.append(res.label);
            inDiv.append(res.select);
            res.select.on('change', (ev) => {
                this.indicatorChanged(jQuery(ev.target).val())
            });
        } else {
            const servDiv = box.find('#change-indicator');
            servDiv.append('<label>Indicateurs</label>');
            servDiv.append('<div>' + this.context.indicator_names[0] + '</div>');
        }

        this.addMoveTypeSelect();

        if (typeof jQuery.fn.select2 !== 'undefined') {
            box.find('select').select2();
        }

        const zDiv = box.find('#change-zone');

        if (this.context.load_full === false) {
            zDiv.append('<label>Territoire</label>');
            return this.api.get(`/limit/name/${this.context.insee_limit}.${this.context.zone_id}`).then((name) => {
                zDiv.append('<div>' + name + '</div>');
            });
        }

        const zoneSel = jQuery('<select name="zone" id="change-zone" class="form-control select2_ajax"></select>');

        zDiv.append(zoneSel);
        zoneSel.before('<label for="change-zone">Territoire</label>');
        zoneSel.on('change', (ev) => {
            this.zoneChanged(jQuery(ev.target).val())
        });
        if (typeof jQuery.fn.select2 !== 'undefined') {
            zDiv.find('select').select2({
                placeholder: "Territoire",
                ajax: {
                    url: this.baseUrl + `/api/accounts/${this.context.account_id}/layers_insee/${this.context.id}`,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.data,
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; },
                minimumInputLength: 1,
                templateResult: function (repo) {
                    if (repo.text || repo.loading) {
                        return repo.text;
                    }

                    return "<div>" + repo.name + "<span class='label label-default'>" + repo.type + "</span> </div>";
                },
                templateSelection: function (repo) {
                    if (repo.text || repo.loading) {
                        return repo.text;
                    }
                    return "<div>" + repo.name + "</div>";
                }
            });
        }
    }

    createSelect(humanName, name, values, displays) {
        const options = values.map((indicator, i) => `<option value="${indicator}">${displays[i]}</option>`);
        const select = jQuery(`<select id="${name}" class="select2">${options}</select>`);

        const label = jQuery(`<label for="${name}">${humanName}</label> `);

        return { select, label };
    }

    moveTypeChanged(newMoveType) {
        console.log('moveTypeChanged', newMoveType);
        console.log('moveTypes', this.moveTypes);

        this.selectedMoveType = this.moveTypes.findIndex((m) => {
            return m.id === parseInt(newMoveType, 10);
        });
        utils.startLoading();
        console.log('selected move type is ', this.selectedMoveType)
        this.map.clear();
        this.displayMoveTypes(this.moveTypes).then((result) => {
            this.graph.destroy();
            this.graph.setResults(result);
            this.graph.display();
            utils.stopLoading();
        }).catch(er => {
            console.log(er);
            utils.fatalError("Les données n'ont pas pu être chargées. Elles sont probablement en cours de calcul. Merci de réessayer plus tard");

        });
    }

    zoneChanged(newZone) {
        console.log('new zone', newZone)
        const z = newZone.split('.');
        this.insee = z[0];
        this.serviceChanged(this.selectedService);

        this.api.get(`/insee/${z[1]}/${z[0]}`).then((resp) => {
            this.map.changeGeometry(resp, 'black', [10, 10], 0.1)
        }).catch((er) => {
            console.log(er);
        });
    }

    indicatorChanged(newIndicator) {
        this.selectedIndicator = newIndicator;
        const index = this.context.indicators.indexOf(newIndicator);
        this.graph.destroy();
        this.graph.setIndicator(index);
        this.graph.display();
    }

    serviceChanged(newService) {
        console.log('new service', newService);
        this.selectedService = newService;
        const serv = this.services.find(s => s.id === parseInt(newService, 10));
        console.log('new SERVICE', serv);
        this.displayServiceMarkers(serv);
        utils.startLoading();
        const res = this.getMoveTypes(newService);
        res.then((movetypes) => {
            this.moveTypes = movetypes;
            const sel = this.addMoveTypeSelect();


            if (sel) {
                sel.val(this.moveTypes[this.selectedMoveType].id).trigger('change');
            } else {
                this.moveTypeChanged(this.moveTypes[this.selectedMoveType].id);
            }

            this.map.clear();
        }).catch(er => {
            utils.fatalError("Les données n'ont pas pu être chargées. Elles sont probablement en cours de calcul. Merci de réessayer plus tard");
            console.log(er)
        });
    }

    addMoveTypeSelect() {
        if (this.moveTypes.length > 1) {
            const movetype = this.createSelect('Type de déplacement', 'change-movetype', this.moveTypes.map(s => s.id), this.moveTypes.map(s => s.name));

            const movetypeDiv = this.box.find('#change-movetype');
            movetypeDiv.html('');
            movetypeDiv.append(movetype.label);
            movetypeDiv.append(movetype.select);
            movetype.select.on('change', (ev) => {
                this.moveTypeChanged(jQuery(ev.target).val())
            });
            let sel = movetype.select;
            if (typeof jQuery.fn.select2 !== 'undefined') {
                sel = movetypeDiv.find('select').select2();
            }
            return sel;
        }
        const movetypeDiv = this.box.find('#change-movetype');
        movetypeDiv.html('');
        movetypeDiv.append('<label>Type de déplacement</label>');
        movetypeDiv.append('<div>' + this.moveTypes[0].name + '</div>');
    }

    displayServiceMarkers(service) {
        // Remove all map markers
        this.map.markers.forEach((m) => {
            this.map.map.removeLayer(m);
        });
        this.map.markers = [];
        //Display service points
        service.points.forEach((p) => {
            const coords = new L.LatLng(p.geometry.coordinates[1], p.geometry.coordinates[0]);
            let marker = L.circleMarker(coords, {
                radius: 4,
                color: "#000000",
                fillOpacity: 0,
            });

            const title = utils.getPopup(p);

            marker.bindPopup(title);
            this.map.markers.push(marker);
            this.map.map.addLayer(marker);
            marker.bringToFront();
        });
    }
}

export default Controls
