
import board from './templates/board';
import Board from './Board';

/*
Usage : <script src="/js/embed.js" data-context-id="CONTEXT_ID"></script>

Where CONTEXT_ID is the id of the context to display
 */

const thisJs = document.querySelector("script[src*='embed-board.js']");

const url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

const baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
console.log('baseurl', baseUrl);
/**
 *
 * @param url
 * @returns {Promise}
 */
const loadScript = (url) => {
    return new Promise((resolve, reject) => {
        const script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

const loadjQuery = () => {
    if (window.jQuery) {
        console.log('jquery ok')
        return Promise.resolve();
    } else {
        return new Promise(function (resolve) {
            setTimeout(() => {
                if (window.jQuery) {
                    console.log('jquery loaded')
                    resolve()
                } else {
                    console.log('jquery loaded from elsewhere')
                    loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(() => {
                        resolve();
                    });

                }
            }, 2000);
        });
    }

};
let css = document.createElement("link");
css.rel = "stylesheet";
css.href = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css";
document.getElementsByTagName("head")[0].appendChild(css);

css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

(function () {
    const boardId = thisJs.getAttribute('data-board-id');

    const jquery = loadjQuery();


    // Is it worth it to load select2 here ?

    jquery.then(() => {
        const chartJs = loadScript('//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js');
        const leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');
        const select2 = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js');

        Promise.all([chartJs, leaflet, select2]).then(() => {
            loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js');
            const fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            fullscreen.then(() => {
                //everything loaded and ready to go
                if (typeof $ === 'undefined') {
                    window.$ = window.jQuery;
                }
                jQuery(document).ready(() => {
                    jQuery(thisJs).after(jQuery(board));
                    const bd = new Board(baseUrl);
                    const req = bd.loadBoard(boardId);
                    req.then((b) => {
                        console.log('BOARD', b)
                        if (bd.active === false) {
                            jQuery('#disabled-message').show()
                        }
                        if (bd.load_full === false) {
                            jQuery('#board-main').find('box-tools').hide();
                        }
                        bd.displayLimit(b.zone_id, b.insee_limit);
                        bd.loadData();
                        const tables = bd.displayStats();
                        jQuery('#board-main').find('> .box-header > .box-title').text(b.name);
                        jQuery('#board-graph').find('.box-title').text(b.description);
                        const statsBox = jQuery('#stats').find('.box-body')
                        tables.forEach((t) => {
                            statsBox.append(t);
                        });
                        jQuery.get(`${baseUrl}/api/limit/name/${b.insee_limit}.${b.zone_id}`).then((name) => {
                            //Dispatch loaded event
                            const event = new Event('proxiclicMap.loaded');
                            window.dispatchEvent(event);
                            jQuery('#change-limit').append(`<option selected value="${b.insee_limit}.${b.zone_id}">${name}</option>`);
                            if (typeof $.fn.select2 !== 'undefined') {
                                jQuery('#change-limit').select2({
                                    placeholder: "Territoire",
                                    ajax: {
                                        url: baseUrl + `/api/accounts/${b.account_id}/layers_insee_board/${b.id}`,
                                        dataType: 'json',
                                        delay: 250,
                                        data: function (params) {
                                            return {
                                                q: params.term,
                                            };
                                        },
                                        processResults: function (data, params) {
                                            return {
                                                results: data.data,
                                            };
                                        },
                                        cache: true
                                    },
                                    escapeMarkup: function (markup) { return markup; },
                                    minimumInputLength: 1,
                                    templateResult: function (repo) {
                                        if (repo.text || repo.loading) {
                                            return repo.text;
                                        }

                                        return "<div>" + repo.name + "<span class='label label-default'>" + repo.type + "</span> </div>";
                                    },
                                    templateSelection: function (repo) {
                                        if (repo.text || repo.loading) {
                                            return repo.text;
                                        }
                                        return "<div>" + repo.name + "</div>";
                                    }
                                });
                            }
                        });
                    }).catch((err) => {
                        console.log('error', err);
                        jQuery('#board-main').html('<div class="alert alert-danger">\n' +
                            '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le tableau de bord!</h4>\n' +
                            '    Ce site web n\'est sans doute pas autorisé à intégrer ce tableau de bord.\n' +
                            '    </div>')
                    });

                    jQuery('body').on('change', '#change-limit', (ev) => {
                        const val = jQuery(ev.currentTarget).val();
                        bd.map.clear();
                        bd.graph.destroy();
                        const d = val.split('.');
                        bd.insee = d[0];
                        bd.displayLimit(d[1], d[0]);
                        bd.loadData();
                    });



                });
            });
        });
    });
})();
