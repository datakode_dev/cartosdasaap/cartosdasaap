import Api from './Api';
import Graph from './Graph';
import Map from './Map';
import  { getPopup} from "./Utils";

class Board {
    constructor(baseUrl) {
        this.api = new Api(baseUrl + '/api');
    }

    loadBoard(id) {
        const req = this.api.get(`/boards/${id}`);
        req.then((board) => {
            this.board = board;
            this.graph = new Graph(board);
            this.graph.setControls(this);
            this.insee = board.insee_limit;
            this.graph.setIndicator(false);
            this.map = new Map(board);
            this.map.setControls(this);
            this.api.get(`/boards/${id}/points`).then((points) => {
                points.forEach((p) => {
                    const coords = new L.LatLng(...p.geometry.coordinates.reverse());
                    let marker = L.circleMarker(coords, {
                        radius: 4,
                        color: "#000000",
                        fillOpacity: 0,
                    });

                    const title = getPopup(p);

                    marker.bindPopup(title);
                    marker.bringToFront();
                    this.map.map.addLayer(marker);
                    this.map.markers.push(marker);
                });

            });
        });

        return req;
    }

    loadData() {
        const requests = this.board.layers.map((layer) => {
            return this.api.get(`/accounts/${this.board.account_id}/boards/layers/${layer.id}?insee=${this.insee}`);
        });
        const result = Promise.all(requests);

        result.then((results) => {
            const parsed = results.map(res => {
                if (res && res.length > 0) {
                    let geom = null;

                    try {
                        geom = JSON.parse(res[0].geom)
                    } catch(e) {
                        console.log('json parse error', e, res[0])
                    }

                    let props = null;
                    try {
                        props = JSON.parse(res[0].props)
                    } catch(e) {
                        console.log('json parse error', e, res[0])
                    }

                    let territoire_props = null;
                    try {
                        territoire_props = JSON.parse(res[0].territoire_props)
                    } catch(e) {
                        console.log('json parse error', e, res[0])
                    }
                    return {
                        id: typeof res[0].layer_id !== 'undefined' ? res[0].layer_id : res[0].step_id,
                        geom,
                        props,
                        territoire_props,
                    };
                }
                return null;
            }).filter((r => r !== null));
            this.graph.setResults(parsed);
            this.graph.setMoveSteps(this.board.layers.map(l => {
                return { value: l.name, id: l.id }
            }));
            this.graph.display();
        });

        return result
    }

    displayLimit(zoneId, insee) {
        return this.api.get(`/insee/${zoneId}/${insee}`).then((resp) => {
            this.map.changeGeometry(resp, 'black', '5, 10', 0)
        }).catch((er) => {
            console.log(er);
        });
    }

    graphClicked(layer, color, removePrevious = true) {
        try {
            const geom = this.map.activateLayer(layer, color, removePrevious, false);
        } catch(err) {
            console.log(err);
        }
    }

    formatStats() {
        return this.board.layers.map((l) => {
            const data = l.props.map((p, i) => {
                const percentages = this.board.indicators.map((indic) => {
                    if (p.insee === l.total_props[i].insee
                        && typeof p.props[indic] !== 'undefined'
                        && typeof l.total_props[i].props[indic] !== 'undefined') {
                        if (l.total_props[i].props[indic] === 0) {
                            return 0;
                        }
                        return p.props[indic] / l.total_props[i].props[indic] * 100;
                    }
                    return 0;
                });
                return {
                    name: p.name,
                    percentages: percentages,
                }
            }).sort((a, b) => {
                return a.percentages[0] > b.percentages[0] ? -1 : 1;
            });
            return {
                name: l.name,
                info: l.info,
                indicators: this.board.indicators,
                indicator_names: this.board.indicator_names,
                data,
            }
        });
    }

    displayStats() {
        const stats = this.formatStats();

        return stats.map((s) => {
            console.log('s', s);
            let table = '<h5>' + s.name + ' <i class="fa fa-info-circle" title="'+s.info+'"></i></h5><div style="max-height: 200px;overflow:auto;max-width: 100%"><table class="table table striped" style="table-layout: fixed;max-width: 100%"><thead><tr>';
            table += '<th>&nbsp;</th>';
            table += s.indicator_names.map((i) => '<th style="">' + i + '</th>').join('');
            table += '</tr></thead>';
            table += s.data.map((item, i) => {
                const cells = item.percentages.map((p) => '<td>' + Math.round(p) + '%</td>').join('')
                return '<tr><th>' + item.name + '</th>' + cells + '</tr>'
            }).join('');
            table += "</table></div>";
            return table;
        })
    }
}

export default Board;
