class Api {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    get(url) {
        if (typeof $ === 'undefined') {
            $ = window.jQuery;
        }
        return new Promise((resolve, reject) => {
            jQuery.get(this.baseUrl + url).then((res) => resolve(res)).fail(err => reject(err));
        });
    }
}

export default Api;
