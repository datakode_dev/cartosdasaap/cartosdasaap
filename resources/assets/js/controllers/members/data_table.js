var element = document.getElementById('table_member');
if (element !== null) {
    $('#table_member').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [
            {
                mData: 'firstname',
                name: 'firstname'
            },
            {
                mData: 'lastname',
                name: 'lastname'
            },
            {
                mData: 'name',
                name: 'name'
            },
            {
                "data": "links",
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    var html = "";
                    console.log(sData.edit);
                    if (sData.edit !== undefined) {
                        html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                    }
                    if (sData.show !== undefined) {
                        html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                    }
                    if (sData.archivate !== undefined) {
                        html += "<form method='POST' action='" + sData.archivate + "'>";
                        html += "<input name='_method' type='hidden' value='put'>";
                        html += "<a href='' data-toggle='tooltip' title='Archiver' onclick='parentNode.submit();return false;'><i class='fa fa-archive' aria-hidden='true'></i></a>";
                        html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                        html += "</form >";
                    }
                    if (sData.unarchivate !== undefined) {
                        html += "<form method='POST' action='" + sData.unarchivate + "'>";
                        html += "<input name='_method' type='hidden' value='put'>";
                        html += "<a href='' data-toggle='tooltip' title='Désarchiver' onclick='parentNode.submit();return false;'><i class='fa fa-arrow-up' aria-hidden='true'></i></a>";
                        html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                        html += "</form >";
                    }
                    if (sData.delete !== undefined) {
                        html += "<a data-toggle='tooltip' title='Supprimer' href='" + sData.delete + "'><i class='fa fa-trash' aria-hidden='true'></i></a> ";
                    }
                    $(nTd).html(html);
                }
            },
        ],
    });
}