var element = document.getElementById('table_permission');
if (element !== null) {
    $('#table_permission').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'id',
            name: 'id'
        },
        {
            mData: 'slug',
            name: 'slug'
        },
        {
            mData: 'name',
            name: 'name'
        },
        ],
    });
}
const userTable = document.getElementById('users_table_permission');
if (userTable !== null) {
    $('#users_table_permission').DataTable({
        ajax: userTable.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'id',
            name: 'id'
        },
        {
            mData: 'lastname',
            name: 'lastname'
        },
        {
            mData: 'firstname',
            name: 'firstname'
        },
        {
            mData: 'email',
            name: 'email'
        },
        {
            "data": 'accounts',
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                var html = "<p>";
                console.log(nTd, sData, oData, iRow, iCol)

                sData.length && sData.forEach(account => {
                    html += `<span class="btn btn-primary btn-xs">${account.name}</span> `
                })

                html += "</p>"

                $(nTd).html(html);
            }
        },
        {
            "data": "links",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                console.log('dd',oData);
                var html = "";
                html += "<a title='Modifier' href='/permissions/" + oData.id + "/edit_accounts'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                $(nTd).html(html);
            }
        }
        ],
    });
}