//gestion de l affichage de la page show d'un contexte
$(document).ready(function() {

    $('#show_test').append("" +
        "<div class='col-md-5' id='box_chart_comment'>" +
        "    <div class='box'>\n" +
        "        <canvas id='myChart'></canvas>\n" +
        "    </div>" +
        "    <div class='box'>\n" +
        "        <div class='box-header with-border'>\n" +
        "            <h3 class='box-title'>Commentaires :</h3>\n" +
        "        </div>\n" +
        "        <div class='box-body with-border'>\n" +
        "        </div>\n" +
        "    </div>" +
        "</div>" +
        "<div class='col-md-7'>\n" +
        "    <div id='maCarte' class='box col-md-12' style='height: 75vh;'>\n" +
        "    </div>\n" +
        "</div>");


    //definition du nombre de colonnes du tableau
    var nbcolumns = 4;

    //nom de la couche et titre du graphe
    var layers = ["hopital"];
    var title = '% de la population qui a accès à un hopital à pied en fonction du temps';

    //attribution des couleurs de façon aléatoire pour chaque barre
    var color = [];
    for (var i=0; i<(nbcolumns); i++){
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);
        color[i] = "rgb(" + r + "," + g + "," + b + ")";
    }

    //je remplis un tableau datas avec toutes les valeurs et tous les labels
    var name = ["5mns","10mns","15mns","20mns"];
    var value = ["60","70","83","95"];
    var datas = [];
    for (var i=0; i<(nbcolumns); i++){
        datas[i] = {
            label: name[i],
            data: [value[i]],
            backgroundColor: color[i],
            borderWidth:1,
            borderColor: 'rgba(0, 0, 0, 0.1)'
        }
    }

    //affichage du graphe
    function chart(){
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: layers,
                datasets: datas
            },
            options: {
                scales: {
                    xAxes: [{ barPercentage: 0.5 }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                },
                title: {
                    display: true,
                    text: title
                },
                hover: {
                    mode:'dataset',
                },

                onHover: function(){
                    //Boucle pour effacer ou remettre la couleur dans les barres du tableau
                    for (i=0; i < nbcolumns; i++) {
                        //si le curseur est placé sur la barre i, j'active toutes les couleurs jusqu'a la barre i
                        //je supprime la couleur au dela de i.
                        if ((this.active[0]._datasetIndex) == i){
                            for (var j=0; j < nbcolumns; j++){
                                if (j < (i+1)){
                                    this.config.data.datasets[j].backgroundColor = color[j];
                                }
                                else {
                                    this.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
                                }
                            }
                        }
                        this.update();
                    }
                }
            }
        });
    }


    //Affichage de la carte
    function map() {
        var map = L.map('maCarte').setView([46.85, 2.3518], 5);

        //  Couches OpensStreetMap et Esri_WorldImagery
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });
        // Layer par défaut
        map.addLayer(OpenStreetMap_Mapnik);

        // possibilité de changer de type de carte
        map.addControl(new L.Control.Layers({
            'OpenStreetMap_Mapnik': OpenStreetMap_Mapnik,
            'Esri_WorldImagery': Esri_WorldImagery,
        }));

        // ajout de la barre de recherche pour rechercher un lieu en fonction de l adresse.
        var geocoder = L.geocoderBAN({collapsed: true}).addTo(map);

        //affichage de la zone choisi sur la carte
        $.ajax({
            type: "GET",
            url: $('#show_test').data('url'),
            dataType: 'json',
            success: function (response) {
                geojsonLayer = L.geoJson(response).addTo(map);
                map.fitBounds(geojsonLayer.getBounds());
            },
        });



    }

    chart();
    map();

});