var element = document.getElementById('table_research_contexts');
if (element !== null) {
    $('#table_research_contexts').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [
            {
                mData: 'name',
                name: 'name'
            },
            {
                "data": "links",
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    var html = "";
                    //TODO CSS
                    if (sData.edit != undefined) {
                        html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                    }
                    if (sData.show != undefined) {
                        html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                    }

                    if (sData.duplicate != undefined) {
                        html += "<form method='POST' action='" + sData.duplicate + "'>";
                        html += "<input name='_method' type='hidden' value='put'>";
                        html += "<a href='' title='Dupliquer' onclick='parentNode.submit();return false;'><i class='fa fa-files-o' aria-hidden='true'></i></a>";
                        html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                        html += "</form >";
                    }
                    if (sData.archivate != undefined) {
                        html += "<form method='POST' action='" + sData.archivate + "'>";
                        html += "<input name='_method' type='hidden' value='put'>";
                        html += "<a href='' data-toggle='tooltip' title='Archiver' onclick='parentNode.submit();return false;'><i class='fa fa-archive' aria-hidden='true'></i></a>";
                        html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                        html += "</form >";
                    }


                    if (sData.unarchivate != undefined) {
                        html += "<form method='POST' action='" + sData.unarchivate + "'>";
                        html += "<input name='_method' type='hidden' value='put'>";
                        html += "<a href='' data-toggle='tooltip' title='Désarchiver' onclick='parentNode.submit();return false;'><i class='fa fa-arrow-up' aria-hidden='true'></i></a>";
                        html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                        html += "</form >";
                    }

                    if (sData.delete != undefined) {
                        html += "<form method='POST' action='" + sData.delete + "'>";
                        html += "<input name='_method' type='hidden' value='delete'>";
                        html += "<a href='' data-toggle='tooltip' title='Supprimer' onclick='parentNode.submit();return false;'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                        html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                        html += "</form >";
                    }
                    $(nTd).html(html);
                }
            },
        ],
    });
}
