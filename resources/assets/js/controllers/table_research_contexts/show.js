
import ResearchContext from '../../embed/ResearchContext'
import { researchEventHandlers } from "../../embed/contextEvents";
const thisJs = document.querySelector("script[src*='research_context.js']");

const url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));
const baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');

let researchContext;

const callback = (ctx, err = null) => {
    if (err !== null) {
        return console.log(err);
    }

    researchEventHandlers($, researchContext);
};

$(document).ready(() => {
    const m = $('#search-map');
    const contextId = m.data('context-id');

    researchContext = new ResearchContext(contextId, callback, baseUrl);

    $('body').on('click', '.btn-clipboard', (ev) => {
        const el = $(ev.currentTarget);
        const copyTextArea = document.createElement("textarea");
        copyTextArea.value = el.parents('figure').find('.embed-code').text();
        document.body.appendChild(copyTextArea);
        copyTextArea.select();
        const res = document.execCommand('copy');
        const msg = res ? 'Copié avec succès' : 'Impossible de copier automatiquement';
        el.attr('data-original-title', msg).tooltip('show');
        document.body.removeChild(copyTextArea);
    }).tooltip();
});
