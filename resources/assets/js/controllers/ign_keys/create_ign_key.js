$(document).ready(() => {

    $('#hidden1').hide();
    $('#hidden2').hide();

    if($('#type1').prop('checked')) {
        $('#hidden2').show();
        $('#hidden1').hide();
    }

    if($('#type2').prop('checked')) {
        $('#hidden1').show();
        $('#hidden2').hide();
    }

    //choix du type d'importation
    $('#type1').on('click', function (){
        $('#hidden2').show();
        $('#hidden1').hide();
    });

    $('#type2').on('click', function(){
        $('#hidden1').show();
        $('#hidden2').hide();
    });

});