var element = document.getElementById('table_ico_color');
if (element !== null) {
    $('#table_ico_color').DataTable({
        language: window.data_table_lang,
        ajax: element.dataset.url,
        columns: [{
            mData: 'name',
            name: 'name'
        },
        {
            mData: 'icone',
            name: 'icone',
        },
        {
            "data": "links",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                var html = "";
                console.log(sData.edit)
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                if (sData.show != undefined) {
                    html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                }
                if (sData.delete != undefined) {
                    html += "<a data-toggle='tooltip' title='Supprimer' href='" + sData.delete + "'><i class='fa fa-trash' aria-hidden='true'></i></a> ";
                }
                $(nTd).html(html);
            }
        },
        ],
    });
}