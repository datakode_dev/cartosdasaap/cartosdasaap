import {editFields} from './edit_fields'
import { pickColor } from '../../embed/Utils';

var nodes;
function showSiren() {

    var tv = $("#treeview");
    tv.show().hummingbird();
    tv.on("CheckUncheckDone", function(){
        var List = {"id" : [], "dataid" : [], "text" : []};
        tv.hummingbird("getChecked",{list:List,onlyEndNodes:false});
        var L = List.id.length;
        console.log(List);

        // get only the final nodes (the NAF code has 5 characters)
        nodes = List.id.map(function(item) {
            return item.replace('xnode-', '');
        }).filter(function(item) {
            return item.length === 5;
        }).join('&refine.apen700=');
        displayNumPoints();
    });
    
}
function displayNumPoints() {
    var limit = $('select[name=limit]').val();
    var buffer = $('input[name=buffer]').val();
    
    if (!limit) {
        return;
    }

    if (!nodes) {
        console.log('nodes are empty');
        $("#num-points-div").hide();
        return;
    }

    $.get('/api/sirene/limit/'+limit+'/'+buffer).then(function(res) {
        if (res.length > 0) {
            var points = res[0].map(function(p) {
                return '(' + p.reverse().join(',') + ')';
            });

            var no = nodes.split('&refine.apen700=').map(function(n) {
                return n.substr(0,2)+'.'+n.substr(2);
            }).join('&refine.activiteprincipaleunitelegale=')

            var url = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=economicref-france-sirene-v3&facet=libellecommuneetablissement&geofilter.polygon=${points}&refine.activiteprincipaleunitelegale=${no}&disjunctive.activiteprincipaleunitelegale=true&rows=2&fields=prenom1unitelegale,nomunitelegale,denominationusuelleetablissement,geolocetablissement`

            //var url = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene&facet=libcom&geofilter.polygon=${points}&refine.apen700=${nodes}&disjunctive.apen700=true&rows=2&fields=l1_normalisee,coordonnees`;

            $.get(url).then(function(res) {
                $('#num-points').html(res.nhits);
                const pdiv = $("#num-points-div")
                pdiv.show();

                pdiv.find('p').remove();
                if (res.nhits > 1000 && res.nhits <= 5000) {
                    pdiv.removeClass().addClass('alert alert-warning');
                } else if (res.nhits > 5000) {
                    pdiv.removeClass().addClass('alert alert-danger').append('<p>Seuls les 5000 premiers points seront récupérés</p>');
                } else {
                    $("#num-points-div").removeClass().addClass('alert alert-info');
                }
            })
        }
    });
}

$(document).ready(function() {
    console.log('ready')

    editFields($, window.service);

    $('#choose-fields #data').sortable({ cancel: ".title-line", handle : "#sortable-arrow", items: "> div" });

    $("input[name='access']").on('change', function(){
        if ($("input[name=access]:checked").val() === 'all' || $("input[name=access]:checked").val() ==='none'){
            $('#members_access').hide();
        } else {
            $('#members_access').show();
        }
    })

    $('select[name=limit]').on('change', function() {
        displayNumPoints();
    });


    $('input[name=buffer]').on('change', function() {
        displayNumPoints();
    });

    const refresh_days = $('input[name=day_between_refresh_url], input[name=day_between_refresh_siren]');
    refresh_days.on('change', function() {

        $('.num-days-div').remove();
        const num_days = parseInt($(this).val(), 10);
        console.log('changed', num_days);
        if (num_days < 7) {
            $(this).after('<div class="alert alert-danger num-days-div" style="margin-top: 1em;" >La mise à jour automatique implique la relance des calculs. Il est donc recommandé de l\'utiliser avec précaution pour ne pas pas saturer le serveur</div>');
        }
    });
    $('#sirene > label').on('click', function() {
        const box = $('#sirene');
        box.find('.inputs').toggle()
        box.find('>label>i.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up')
    });

    const existingUrl = $('#source_url').val();
    if (typeof existingUrl !== 'undefined' && existingUrl.indexOf('/api/sirene') >= 0) {
        //We have an existing Sirene URL, parse it and set proper data
        //$('#source_url').val('');
        showSiren();
        const a = document.createElement('a');
        a.href = existingUrl;
        $('#source_url').val('');
        refresh_days.val('');
        const searchParams = decodeURIComponent(a.search.replace('?', '')).split('&')

        searchParams.forEach(function(param) {
            const ar = param.split('=');
            if (ar[0].indexOf('nodes') === 0) {
                $("#treeview").hummingbird("checkNode", { attr:"id", name: "xnode-"+ar[1], expandParents: true });
            }
            if (ar[0].indexOf('buffer') === 0) {
                $('input[name=buffer]').val(ar[1]);
            }
            if (ar[0].indexOf('limit') === 0) {
                $.get('/api/limit/name/' + ar[1]).then((res) => {
                    const newOption = new Option(res, ar[1], false, false);
                    $('select[name=limit]').append(newOption).trigger('change');
                });
            }
        })
    }

    //je recupere les donnees du fichier icons_json
    var list_ensemble = $('.search_ico_color').data("json");

    //je liste tous les ensembles à partir du fichier json pour pouvoir les choisir dans le modal choisir ensemble
    $.each(list_ensemble, function (index, value) {
        $('.search_ico_color').append('<option value="' + index + '">' + value.name + '</option>')
    });

    try {
        $('.search_ico_color').length && $('.search_ico_color').select2();
    } catch (e) {
        console.log('select2 not init')
    }

    $('#valid_ico_color').on("click",function(){
        //je recupere l'id de l'ensemble choisi
        var color_index = $('.search_ico_color').val();
        //je recupere le code couleur et l icone de cet ensemble

        var color_name = list_ensemble[color_index].color_name;
        var icon_name = list_ensemble[color_index].icon_name;

        // je sauvegarde les caracteristiques de l ensemble dans le formulaire de creation de service
        $('input[name="color"]').val(color_name);
        $('input[name="ico"]').val(icon_name);

        $('#setBackground').css('background-color', color_name)
        $('#setBackground .custom').remove()
        $('#icon-preview').removeClass().addClass(icon_name)
    });

    //choix du type d'importation
    $('#type1').on('click', function (){
        $('#hidden1').show();
        $('.num-days-div').remove();
        $('#hidden2').hide();
        $('#hidden3').hide();

        $('#colorIconContent > div').hide()
        $('#colorIconContent #waitingColor').show()

    });

    $('#type2').on('click', function(){
        $('#hidden2').show();
        $('.num-days-div').remove();
        $('#hidden1').hide();
        $('#hidden3').hide();

        $('#colorIconContent > div').hide()
        $('#colorIconContent #waitingColor').show()
    });

    $('#type3').on('click', function (){
        $('#hidden3').show();
        showSiren();
        $('.num-days-div').remove();
        $('#hidden1').hide();
        $('#hidden2').hide();

        // SHOW MARKER INPUTS
        $('#colorIconContent > div').hide()
        $('#colorIconContent .marker').show()
    });
    if (!$('#type1').prop("checked")){
        $('#hidden1').hide();
    }
    if (!$('#type2').prop("checked")){
        $('#hidden2').hide();
    }
    if (!$('#type3').prop("checked")){
        $('#hidden3').hide();
    }

    $('.iconpicker-item').on('click', function () {
        $('#setBackground .custom').remove()

    })

    function customIconAction(ev) {
        ev.stopPropagation()
        ev.preventDefault();
        if (!deleteMode) {
            let name = ev.currentTarget.getAttribute("name")
        
            let currentImg = ev.target.localName === "button" ? ev.target.children[0] : ev.target
    
            if ($('#setBackground .custom')) {
                $('#setBackground .custom').remove()
            }
            $('input[name="ico"]').val(name)
            $('#icon-preview').removeClass()
            let html = currentImg.outerHTML
    
            $('#setBackground').append(html)
        }
    }
    // ------ DELETE ICONS JS ------
    let deleteMode = false
    $('#deleteIcon').click( (ev) => {
        ev.stopPropagation()
        ev.preventDefault();
        deleteMode = true
        // SET RED ICONS
        $("button[class^='container-']").css("border", "1px solid red");
        $("button[class^='container-']").css("background", "#ffe6e6");
        $('#deleteIcon').hide()
        $('#cancelDeleteIcon').show()

        $('.svg').click( (ev) => {
            console.log('name', ev.currentTarget.getAttribute("name"), $('#ico'), $('#ico').val())

            if (deleteMode && confirm(`Souhaitez-vous supprimer l'icône ${ev.currentTarget.getAttribute("name")} ?`) == true) {
                // DELETE ICON
                const url = window.location.href.split('/services/');
                console.log('delete icon ', `${url[0]}/deleteIconSvg/${ev.currentTarget.getAttribute("id")}`)
                $.ajax({
                    type: 'GET',
                    url: `${url[0]}/deleteIconSvg/${ev.currentTarget.getAttribute("id")}`,
                    success: (res)  => {
                        console.log('ico delete res',res)
                        // RENVOIE 'OK' si supprimé ou 'KO' si pas supprimé 
                        if (res === 'OK') {
                            ev.currentTarget.style.display = 'none'
                            $('#custom-icon').append(`<div class="alert alert-success alert-dismissible show" role="alert">L'icône a été supprimée.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
                        } else {
                            // SHOW ALERT
                            $('#custom-icon').append(`<div class="alert alert-warning alert-dismissible show" role="alert">L'icône est associé à un service, et n'a donc pas pu être supprimée.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
                        }
                    },
                    error: (err) => {
                        console.log(`Couldn't delete icon ${JSON.stringify(err)}`)
                    },
                })

                // DELETE ICON IF IN ICON INPUT
                if ($('#ico').val() === ev.currentTarget.getAttribute("name")) {
                    $('.listing_ico_color #ico').val('');
                    $('#ico').val('');
                    // DELETE ICON PREVIEW
                    $('#setBackground .custom').remove()
                }
            }
        })
    })
    $('#cancelDeleteIcon').click( (ev) => {
        deleteMode = false
        ev.stopPropagation()
        ev.preventDefault();
        $("button[class^='container-']").css("border", "none");
        $("button[class^='container-']").css("background", "white");
        $('#cancelDeleteIcon').hide()
        $('#deleteIcon').show()
    })
    $('#addIconButton').click( (ev) => {
        deleteMode = false
        $("button[class^='container-']").css("border", "none");
        $("button[class^='container-']").css("background", "white");
        $('#cancelDeleteIcon').hide()
        $('#deleteIcon').show()
    })
    // TO FILTER ICON SHOWN
    $('#iconSearch').on('input', (ev) => {
        $("#iconList button").each ((id, button) => {
            console.log(id, button, button.getAttribute("name"))
            button.style.display = 'none';
            if (button.getAttribute("name").toUpperCase().includes(ev.target.value.toUpperCase())) {
                button.style.display = 'inline-block';
            }
            
        })
    })

    // ----- SAVE ACCOUNT SVG ICON -----
    $('#saveIcon').click( (ev) => {
        ev.stopPropagation()
        ev.preventDefault();

        let data = new FormData()
        jQuery.each(jQuery('#iconSVG')[0].files, function(i, file) {
            data.append('svg', file);
            data.append('name', $('#iconName').val());
        });

        const url = window.location.href.split('/services/');
        $.ajax({
            type: 'POST',
            url: url[0]+'/save_custom_icon',
            beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', $('input[name="_token"]').attr('value'));},
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: (svg)  => {
                console.log('ico',svg)

                // ----- ADD ICON IN LIST + SET CLICK -----
                let img = $('<img class="custom" style="width:18px;height:18px;">');
                img.attr('src', `${window.location.origin}/storage/${svg.path}`);
                let container = $(`<button id="${svg.id}" class="container-${svg.id} svg" name=${svg.name}></button>`);
                container.appendTo('#iconList');
                img.appendTo(`.container-${svg.id}`);

                $(`.container-${svg.id}`).on("click", (ev) => customIconAction(ev));

            },
            error: (err) => {
                console.log(`Couldn't get current research_context ${JSON.stringify(err)}`)
            },
        })
    })
    // ----- FETCH ACCOUNT SVG ICONS -----
    $.ajax({
        type: 'GET',
        url: `${window.location.href.split('/services')[0]}/custom_icon`,
        // http://cartosdasaap.local/accounts/2/custom_icon
        
        success: (imgs)  => {
            console.log('icons', imgs, window.location)

            // ----- SHOW ICONS -----
            imgs.forEach( svg => {
                let img = $('<img class="custom" style="width:18px;height:18px;">');
                img.attr('src', `${window.location.origin}/storage/${svg.path}`);
                let container = $(`<button id="${svg.id}" class="container-${svg.id} svg" name=${svg.name}></button>`);
                container.appendTo('#iconList');
                img.appendTo(`.container-${svg.id}`);
            })
            // ----- ICONS ON CLICK -----
            $('.svg').on("click", (ev) => customIconAction(ev));

            console.log("$('.svg')", $('.svg'))
        },
        error: (err) => {
            console.log(`Couldn't get current svg ${JSON.stringify(err)}`)
        },
    })
    // TO DO
    // SET COLOR INPUT WITH DATA LOADED
});

if ($('.previewInput')) {
    // SET PREVIEW ICON FONT AWESOME
    !$('#icon-preview').data("path") && $('#icon-preview').addClass($('.previewInput').val())
    // SET PREVIEW ICON SVG
    $('#icon-preview').data("svg") && $('#icon-preview').data("path") && $('#icon-preview').html(`<img class="custom" src="${window.location.origin}/storage/${$('#icon-preview').data("path")}" style="width: 15px;"/>`);
    // SET PREVIEW BACKGROUND
    $('#setBackground').css('background-color', $('.colorPreviewInput').val()) 
    $('.colorPreviewInput').val() && $('#icon-preview').css('color', pickColor($('.colorPreviewInput').val()))
    $('#icon-picker').removeClass('has-error') // REMOVE HAS ERROR ON SERVICE EDIT WHEN THERE IS SVG FILE

}
$('.colorPreviewInput').change( () => {
    $('#setBackground').css('background-color', $('.colorPreviewInput').val()) 
    $('.colorPreviewInput').val() && $('#icon-preview').css('color', pickColor($('.colorPreviewInput').val())) 

})
$('.previewInput').change( () => {
    $('#setBackground').css('background-color', $('.colorPreviewInput').val()) 
})
// HIDE COLOR CHOICE WHEN LOADING
console.log('hide here')
$('#colorIconContent > div').hide()
$('#colorIconContent #waitingColor').show()
