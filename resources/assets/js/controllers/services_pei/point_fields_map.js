export const mapField = (points, fieldMap, selectionMap) => {
    if (!selectionMap || typeof selectionMap !== 'object') {
        selectionMap = fieldMap;
    }
    if (!fieldMap || typeof fieldMap !== 'object') {
        return points.map(point => {
            Object.keys(point.prop).forEach(k => {
                let val = point.prop[k];
                if (!val) {
                    delete point.prop[k];
                }
                if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                    //Is a link, replace with link
                    val = `<a href="${val}" target="_blank">${val}</a>`;
                    point.prop[k] = val;
                }
            });
            point.selectionProp = point.prop;

            return point;
        });
    }
    return points.map((point) => {
        const newProps = {};
        Object.keys(fieldMap).forEach(k => {
            let val = point.prop[fieldMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = `<a href="${val}" target="_blank">${val}</a>`;
            }
            if (val) {
                newProps[k] = val;
            }
        });

        const newSelectionProps = {};
        Object.keys(selectionMap).forEach(k => {
            let val = point.prop[selectionMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = `<a href="${val}" target="_blank">${val}</a>`;
            }
            if (val) {
                newSelectionProps[k] = val;
            }
        });

        point.selectionProp = newSelectionProps;
        point.prop = newProps;
        return point;
    });
};
