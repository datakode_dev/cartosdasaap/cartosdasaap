var element = document.getElementById('table_markers_score');
if (element === null) {
} else {
    $('#table_markers_score').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'score',
            name: 'score'
        },
        /*{
            mData: 'name',
            name: 'name'
        },*/
        {
            mData: 'adresse',
            name: 'adresse'
        },
        {
            mData: 'adresse_ban',
            name: 'adresse_ban'
        },
        {
            "data": "links",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                var html = "";
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                $(nTd).html(html);
            }
        }]
    });
}
