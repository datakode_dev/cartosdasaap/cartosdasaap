import Papa from 'papaparse';

// option format
const template = '<option value="{{name}}" {{selected}}>{{name}}</option>';

// Define each form line format
const line =
    '<div class="form-group row" style="background: #f2f2f2;margin: 1rem;padding: 1rem; position: relative;">' +
        '<div id="sortable-arrow" class="col-sm-1 p-0" style="padding: 0; font-size: 20px; cursor: grab; margin-top: 2rem;padding-left: 2rem;">' +
            '<i class="fa fa-arrows-v" aria-hidden="true"></i>' +
        '</div>' +
        '<div class="col-sm-2" style="padding: 0;">' +
            '<label for="field1">Nom du champ</label>' +
            '<input id="field{{id}}" class="form-control form-control-sm" name="field-names[{{id}}]" type="text" value="{{value}}" />' +
        '</div>' +
        '<div class="col-sm-3">' +
            '<label for="field{{id}}-value">Valeur du champ</label>' +
            '<select id="field1-value" class="select2 form-control form-control-sm" name="field-values[{{id}}]" style="width:100%">{{options}}</select>' +
        '</div>' +
        '<div class="col-sm-2" style="margin-top: 2em;">' +
            '<input type="checkbox" name="infobulle-values[{{id}}]" {{checkedinfo}} style="margin-right: 10px"/>' +
            '<label for="infobulle-values[{{id}}]">Infobulle</label>' +
        '</div>' +
        '<div class="col-sm-2" style="margin-top: 2em;">' +
            '<input type="checkbox" name="selection-values[{{id}}]" {{checkedselection}} style="margin-right: 10px"/>' +
            '<label for="selection-values[{{id}}]">Selection</label>' +
        '</div>' +
        '<div class="col-sm-2" style="margin-top: 2em;">' +
            '<input type="checkbox" name="recherche-values[{{id}}]" class="search-checkbox" {{checkedrecherche}} style="margin-right: 10px"/>' +
            '<label for="recherche-values[{{id}}]">Recherche</label>' +
        '</div>' +
    '</div>';

// Specific line for the title field chooser, with hidden input
const titleLine =
    '<span id="title-line" class="form-group row">' +
        '<div class="col-sm-6">' +

            '<input id="dataType" name="dataType" type="text" value="" disabled  class="hidden" />' +

            '<input id="field{{id}}" name="field-names[{{id}}]" type="hidden" value="name" />' +
            '<input type="hidden" name="selection-values[{{id}}]" value="true" />' +
            '<input type="hidden" name="infobulle-values[{{id}}]" value="true" />' +
            '<input type="hidden" name="recherche-values[{{id}}]" value="true" />' +
        '</div>' +
        '<div class="col-sm-6">' +
            '<label for="field{{id}}-value">Titre du point (Champ à utiliser)</label>' +
            '<select id="field1-value" class="select2" name="field-values[{{id}}]" style="width:100%">{{options}}</select>' +
        '</div>' +
    '</span>';

const displayForm = (data) => {
    console.log('displayForm')
    console.log('data----', data)
    // Get the properties from the first point
    let props = data.features[0].properties;

    if (Array.isArray(props)) {
        props = props[0];
    }
    // Get the properties names
    const keys = Object.keys(props);

    const options = keys.map(k => template.replace(new RegExp('{{name}}', 'g'), k)).join('');
    let id = 1;
    $('#choose-fields #data').append(titleLine.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options));
    titleLine.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options).appendTo()

    id = 2;
    $('#choose-fields #data')
        .append(line.replace('{{checkedselection}}','checked').replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options).replace('{{value}}', ''))
        .append('<div class="row" style="margin: 1em;"><div class="col-sm-12 text-center"><button class="btn btn-warning add-field"><i class="fa fa-plus-circle"></i> Ajouter un champ</button></div></div>');
    $('#choose-fields #data').find('.select2').select2();

    $('body').on('click', '.add-field', (ev) => {
        ev.preventDefault();
        id++;
        console.log($(ev.currentTarget).parent().closest('.row'))
        $(ev.currentTarget).parent().closest('.row').before(line.replace('{{checkedselection}}','checked').replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{value}}', '').replace('{{options}}', options));
        if ($('#choose-fields #data').length) {
            $('#choose-fields #data').find('.select2').select2()
        }
    });
};

const makeContainer = (type = 'file') => {
    if ($('#choose-fields #data').length >= 0) {
        $('#choose-fields #data').remove();
        $('body').off('click', '.add-field');
    }

    let t = 1;
    if (type === 'sirene') {
        t = 3
    } else if (type === 'url') {
        t = 2
    }

    $('#type' + t).prop("checked", true);
    $('#hidden' + t).show();
    $('#hidden' + t).append('<div class="form-group" id="choose-fields" style="margin-top: 1em;"><div class="box-header with-border"><h3 class="box-title">Champs à afficher</h3><div id="data"></div></div></div>');
};

export const editFields = ($, existingService) => {
    if (existingService) {

        // HIDE en attente de donnée
        $('#colorIconContent #waitingColor').hide()

        if (existingService.path) {
            console.log('existingService', existingService)
            makeContainer('file');
            (existingService.geometry === 'Polygon' || existingService.geometry === 'MultiPolygon') && $('#colorIconContent .polygone').show();
            (existingService.geometry === 'LineString' || existingService.geometry === 'MultiLineString') && $('#colorIconContent .line').show();
            (existingService.geometry === null || existingService.geometry === 'Point' || existingService.geometry === 'MultiPoint') && $('#colorIconContent .marker').show();

        } else if (existingService.source_url && existingService.source_url.indexOf("api/sirene") >= 0) {
            makeContainer('sirene');
            $('#colorIconContent .marker').show()
        } else {
            makeContainer('url');
            (existingService.geometry === 'Polygon' || existingService.geometry === 'MultiPolygon') && $('#colorIconContent .polygone').show();
            (existingService.geometry === 'LineString' || existingService.geometry === 'MultiLineString') && $('#colorIconContent .line').show();
            (existingService.geometry === null || existingService.geometry === 'Point' || existingService.geometry === 'MultiPoint') && $('#colorIconContent .marker').show();
        }

        try {
            console.log('existingService', existingService);
            let fields = JSON.parse(existingService.display_fields);

            if (!fields) {
                fields = {};
            }

            let selectionFields = JSON.parse(existingService.display_selection);

            if (!selectionFields) {
                selectionFields = {};
            }
            
            let rechercheFields = JSON.parse(existingService.display_recherche);

            if (!rechercheFields) {
                rechercheFields = {};
            }

            let availableFields = existingService.availableFields;
            if (typeof existingService.availableFields === 'string') {
                availableFields = JSON.parse(existingService.availableFields);
            }

            // Get all used field names.
            let allFields = Object.keys(fields).concat(Object.keys(selectionFields));
            allFields = allFields.concat(Object.keys(rechercheFields))

            //Remove duplicates
            allFields = [...new Set(allFields)];

            let allFieldsObject = {};

            //create object with all field names as keys and target field as value
            allFields.forEach((fk) => {
                allFieldsObject[fk] = typeof fields[fk] !== 'undefined' ? fields[fk] : typeof selectionFields[fk] !== 'undefined' ? selectionFields[fk] : rechercheFields[fk];
                // allFieldsObject[fk] = typeof fields[fk] !== 'undefined' ? fields[fk] : selectionFields[fk];
            });

            const keys = Object.keys(availableFields);
            let id = 1;

            const allOptions = keys.map(opk => {
                //Create options and mark as not selected
                return template.replace(new RegExp('{{name}}', 'g'), opk).replace('{{selected}}', '')
            }).join('');
            

            Object.keys(allFieldsObject).forEach((k) => {
                const options = keys.map(opk => {
                    //Create options and mark as seleted
                    return template.replace(new RegExp('{{name}}', 'g'), opk).replace('{{selected}}', opk === allFieldsObject[k] ? 'selected' : '')
                }).join('');
                //Append line
                let parsedLine = line.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options).replace('{{value}}', k);

                // if (allFieldsObject[k] === allFieldsObject['name']) { // PREVENT NAME TO BE DRAG AND DROPPED
                //     parsedLine = parsedLine.replace("form-group row", "form-group row title-line");
                // }

                if (k === 'name') {
                    parsedLine = titleLine.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options);
                    parsedLine = parsedLine.replace("form-group row", "form-group row title-line");
                }

                if (typeof fields[k] !== 'undefined') {
                    parsedLine = parsedLine.replace('{{checkedinfo}}', 'checked');
                }
                if (typeof selectionFields[k] !== 'undefined') {
                    parsedLine = parsedLine.replace('{{checkedselection}}', 'checked');
                }
                if (typeof rechercheFields[k] !== 'undefined') {
                    parsedLine = parsedLine.replace('{{checkedrecherche}}', 'checked');
                }
                $('#choose-fields #data').append(parsedLine);
                id++;
            });


            $('#choose-fields #data').append('<div class="row" style="margin: 1em;"><div class="col-sm-12 text-center"><button class="btn btn-warning add-field"><i class="fa fa-plus-circle"></i> Ajouter un champ</button></div></div>');
            
            try {
                $('#choose-fields #data').find('.select2').select2();
            } catch (e) {
                console.log('select2 not init')
            }

            $('body').on('click', '.add-field', (ev) => {
                ev.preventDefault();
                id++;
                console.log('seb',$(ev.currentTarget).parents('.row').first());
                $(ev.currentTarget).parents('.row').first().before(line.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{checkedselection}}','checked').replace('{{value}}', '').replace('{{options}}', allOptions));
                $('#choose-fields #data').find('.select2').select2()
            });
        } catch (er) {
            console.log('could not read existing display fields', er)
        }

        // SET DATA TYPE INPUT
        console.log('SET DATA INPUT LOAD', existingService)
        existingService.geometry && $('#dataType').val(existingService.geometry)
        !existingService.geometry && $('#dataType').val('Point')
    }

    $('#source_url').on('change', (ev) => {
        console.log('change url source')
        const input = $(ev.currentTarget);
        const val = input.val();
        // Change to use proxy.
        const url = `${window.location.protocol}//${window.location.host}/p?url=${encodeURIComponent(val)}`;
        $.get(url).then((res) => {
            let data = res;
            console.log(data);
            if (data !== null && typeof data.features === 'undefined' || data.features.length === 0) {
                try {
                    data = JSON.parse(data);
                    if (data !== null && typeof data.features === 'undefined' || data.features.length === 0) {
                        alert("L'adresse donnée ne contient pas de geojson valide");
                        return;
                    }
                } catch(e) {
                    alert("L'adresse donnée ne contient pas de geojson valide");
                    return;
                }
            }

            $('#colorIconContent > div').hide()
            const featureType = data.features[0].geometry.type
            console.log('featureType', featureType)
            data.features.forEach( feature => {
                if (feature.geometry.type !== featureType) { // IF NOT ONLY ONE TYPE PER FILE, CLEAN INPUT
                    $('#alert_data_box').append(`<div class="alert alert-warning alert-dismissible show" role="alert">Le fichier n'a pas pu être importé, il contient plusieurs types de géométrie.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
                    data = null
                    $('input[name=source_url]').val('')
                    // HIDE COLOR CHOICE
                    $('#colorIconContent #waitingColor').show()
                    return;
                }
            })
            // IF ONLY ONE DATA TYPE -> UPDATE COLOR DIV
            if (data && (featureType === "Point" || featureType === "MultiPoint")) {

                setTimeout(() => { 
                    $('#dataType').val('Point')
                }, 500);
                $('#colorIconContent .marker').show()
            } else if (data && (featureType === "Polygon" || featureType === "MultiPolygon")) {
                setTimeout(() => { 
                    $('#dataType').val('Polygon')
                }, 500);
                $('#colorIconContent .polygone').show()
            }
            else if (data && (featureType === "LineString" || featureType === "MultiLineString")) { 
                setTimeout(() => { 
                    $('#dataType').val('LineString')
                }, 500);
                $('#colorIconContent .line').show()
            }

            makeContainer('url');
            console.log('launch display')
            displayForm(data);
        }).fail((e) => {
            alert("impossible de charger cette adresse (" + e.responseJSON.message + ")");
            return console.log('could not read url', e);
        })
    });

    $('input[name=fileToUpload]').on('change', (ev) => {
        console.log('change file source')

        const input = $(ev.currentTarget);
        const f = input.get(0).files;
        if (f.length === 0) {
            return;
        }
        const file = f[0];
        console.log(file);
        const reader = new FileReader();
        reader.onload = (res => {
            let data = null;

            try {
                data = JSON.parse(res.target.result);
                console.log('data file', data)
                // CHECK DATA TYPE (POINT / POLYGONE / LINE) TO ADAPT COLOR
                const featureType = data.features[0].geometry.type
                console.log('featureType', featureType)
                data.features.forEach( feature => {
                    if (feature.geometry.type !== featureType) { // IF NOT ONLY ONE TYPE PER FILE, CLEAN INPUT
                        $('#alert_data_box').append(`<div class="alert alert-warning alert-dismissible show" role="alert">Le fichier n'a pas pu être importé, il contient plusieurs types de géométrie.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
                        data = null
                        $('input[name=fileToUpload]').val('')
                        // HIDE COLOR CHOICE
                        $('#colorIconContent > div').hide()
                        $('#colorIconContent #waitingColor').show()
                        return;
                    }
                })
                // IF ONLY ONE DATA TYPE -> UPDATE COLOR DIV

                if (data && (featureType === "Point" || featureType === "MultiPoint")) {
                    $('#colorIconContent > div').hide() // IN IF TO NOT HIDE waitongColor when false file
                    $('#colorIconContent .marker').show()
                } else if (data && (featureType === "Polygon" || featureType === "MultiPolygon")) {
                    $('#colorIconContent > div').hide()
                    $('#colorIconContent .polygone').show()
                }
                else if (data && (featureType === "LineString" || featureType === "MultiLineString")) { 
                    $('#colorIconContent > div').hide()
                    $('#colorIconContent .line').show()
                }
                setTimeout(() => { 
                    $('#dataType').val(featureType)
                }, 500);
                console.log('SET DATA TYPE', featureType)

                // ------
            } catch (err) {
                //Data is not geojson, try CSV
                data = Papa.parse(res.target.result, {
                    header: true
                });
                if (typeof data.data !== 'undefined') {
                    data.features = data.data.map(item => {
                        return { properties: item }
                    });
                }
            }

            if (data !== null && typeof data.features === 'undefined' || data.features.length === 0) {
                return;
            }

            makeContainer('file');

            displayForm(data);
        });

        reader.readAsText(file);
    });

    $('body').on('click', '.search-checkbox', (ev) => {
        console.log('research checked', ev, $(this))
        $('#search-number-selected').remove();
        if($('.search-checkbox:checked').length > 10) {
            $('#choose-fields #data').append(`<div id="search-number-selected" class="alert alert-warning" role="alert">Attention, plus de 10 champs de recherche sont sélectionnés (Actuellement ${$('.search-checkbox:checked').length})<br>Ils ne seront pas tous affichés dans le contexte de recherche associé.</div>`)
        }

    })
    if($('.search-checkbox:checked').length > 10) {
        $('#choose-fields #data').append(`<div id="search-number-selected" class="alert alert-warning" role="alert">Attention, plus de 10 champs de recherche sont sélectionnés (Actuellement ${$('.search-checkbox:checked').length})<br>Ils ne seront pas tous affichés dans le contexte de recherche associé.</div>`)
    }
};
