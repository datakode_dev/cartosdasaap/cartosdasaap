import { pickColor } from '../../embed/Utils';
import { mapField } from './point_fields_map';

// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function() {

    //******************************************************************************************************************
    //************************************* Affichage de la carte ******************************************************
    //******************************************************************************************************************

    function map(){
        var map = L.map('maCarte').setView([46.85, 2.3518], 5);

        console.log('map', map)

        //  Couches OpensStreetMap et Esri_WorldImagery
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });
        // Layer par défaut
        map.addLayer(OpenStreetMap_Mapnik);

        // possibilité de changer de type de carte
        map.addControl(new L.Control.Layers({
            'OpenStreetMap_Mapnik': OpenStreetMap_Mapnik,
            'Esri_WorldImagery': Esri_WorldImagery,
        }));


        // ajout de la barre de recherche pour rechercher un lieu en fonction de l adresse.
        var geocoder = L.geocoderBAN({collapsed: true}).addTo(map);
        const mapDiv = $('#maCarte');
        var json = mapDiv.data("json");

        console.log('mapDiv', mapDiv)
        console.log('json', json)

        var selectionMap;
        if (!selectionMap) { selectionMap = mapDiv.data('map-selections') }
        json = mapField(json, mapDiv.data('map-fields'), selectionMap);

        let layerType = json[0].geom.type

        // ----- POLYGONS -----
        if (json.length && (layerType === 'Polygon' || layerType === 'MultiPolygon')) {
            const colorOutline = mapDiv.data("icon").color_outline_name;
            const colorFill = mapDiv.data("icon").color_fill_name;

            console.log('LAYER BEFORE', json)

             // REVERSE GEOJSON COORDINATES FOR LEAFLET || GENERATE LAYERS BY GEOJSON WASNT WORKING
            layerType === 'Polygon' &&  json.map(layer => layer.geom.coordinates).map( layer => layer[0].map(arr => {arr.reverse()}))
            layerType === 'MultiPolygon' && json.map(layer => layer.geom.coordinates).map( layer => layer[0].map(arr => arr.map(arr2 =>{arr2.reverse()})))

            let polygons = [] // TO FIT BOUNDS

            json.forEach( value => {

                // SET POPUP CONTENT
                let title = value.prop && value.prop.name ? value.prop.name : "";
                title = '<p class="title"><span class="text" style="color:' + colorOutline + '">' + title + '</span></p>';
                if (value.prop) {
                    Object.keys(value.prop).map(function (key) {
                        if (key !== 'name') {
                            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + value.prop[key] + '</strong></p>';
                        }
                    });
                }
                // CREATE POLYGON
                const poly = L.polygon(value.geom.coordinates).setStyle({color: colorOutline, fillColor: colorFill}).bindPopup(title).addTo(map)
                polygons.push(poly)
            })

            const bounds = polygons.map(p=>p.getBounds());
            map.fitBounds(bounds);
        }

        // ----- LINES ----- SAME AS POLYGON
        if (json.length && (layerType === "LineString" || layerType === 'MultiLineString')) {
            const colorOutline = mapDiv.data("icon").color_outline_name;

             // REVERSE GEOJSON COORDINATES FOR LEAFLET
            layerType === 'LineString' && json.map(layer => layer.geom.coordinates).map( layer => layer.map(arr => {arr.reverse()}))
            layerType === 'MultiLineString' && json.map(multi => multi.geom.coordinates).map( coor => coor.map(path =>path.map(point => {point.reverse()})))

            let polylines = [] // TO FIT BOUNDS
            console.log(json)

            json.forEach( value => {
                // SET POPUP CONTENT
                let title = value.prop && value.prop.name ? value.prop.name : "";
                title = '<p class="title"><span class="text" style="color:' + colorOutline + '">' + title + '</span></p>';
                if (value.prop) {
                    Object.keys(value.prop).map(function (key) {
                        if (key !== 'name') {
                            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + value.prop[key] + '</strong></p>';
                        }
                    });
                }
                // CREATE POLYGON
                const poly = L.polyline(value.geom.coordinates).setStyle({color: colorOutline}).bindPopup(title).addTo(map)
                polylines.push(poly)
            })

            const bounds = polylines.map(p=>p.getBounds());
            map.fitBounds(bounds);
        }

        // ----- MARKERS -----
        if (json.length && (layerType === 'Point' || layerType === 'MultiPoint') ) {
            const color = mapDiv.data("color");
            const icon = mapDiv.data("icon");

            const markers = L.markerClusterGroup({
                iconCreateFunction: function(cluster) {
                    return L.divIcon({ html: `<div style="background-color:${color};color:${pickColor(color)}"><span>${cluster.getChildCount()}</span></div>`, className: 'custom-cluster', iconSize: L.point(40, 40) });
                }
            });

            $.each(json, function (index, value) {

                const iconContent = icon.icon_svg ? `<img src="${window.location.origin}/storage/${icon.icon_path}" alt="${icon.icon_name}" style="color: ${pickColor(color)}!important;max-width: 18px!important;"></i>` : '<i class="' + icon.icon_name + '" style="color: ' + pickColor(color) + ' !important"></i>'
                
                let title = value.prop && value.prop.name ? value.prop.name : "";
                title = '<p class="title"><span class="icon" style="background-color:' + color + '">' + iconContent + '</span> <span class="text" style="color:' + color + '">' + title + '</span></p>';
                if (value.prop) {
                    Object.keys(value.prop).map(function (key) {
                        if (key !== 'name') {
                            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + value.prop[key] + '</strong></p>';
                        }
                    });
                }

                if (json.length && json[0].geom.type === 'Point') {
                    var latitude = value.geom.coordinates[1];
                    var longitude = value.geom.coordinates[0];

                    const marker = L.marker([latitude, longitude],{
                        title: title,
                        icon: L.divIcon({
                            html: '<div style="border-color:' + color + '"><div>' + iconContent + '</div></div>',
                            iconSize: [20, 20],
                            className: 'service-point-icon',
                            iconAnchor: [17, 54],
                            popupAnchor: [0, -50]
                        })
                    }).bindPopup(title)
                    markers.addLayer(marker);
                }
                if (json.length && json[0].geom.type === 'MultiPoint') {

                    json.map(multi => multi.geom.coordinates).map( coor => coor.map(point => {point.reverse()}))

                    value.geom.coordinates.forEach( point => {
                        let marker = L.marker(point,{
                            title: title,
                            icon: L.divIcon({
                                html: '<div style="border-color:' + color + '"><div>' + iconContent + '</div></div>',
                                iconSize: [20, 20],
                                className: 'service-point-icon',
                                iconAnchor: [17, 54],
                                popupAnchor: [0, -50]
                            })
                        }).bindPopup(title)
                        markers.addLayer(marker)
                    })
                }
            });

            map.fitBounds(markers.getBounds());
            map.addLayer(markers);
        }
    }

    map();

    // Copy embed code
    $('body').on('click', '.btn-clipboard', (ev) => {
        const el = $(ev.currentTarget);
        const copyTextArea = document.createElement("textarea");
        copyTextArea.value = el.parents('figure').find('.embed-code').text();
        document.body.appendChild(copyTextArea);
        copyTextArea.select();
        const res = document.execCommand('copy');
        const msg = res ? 'Copié avec succès' : 'Impossible de copier automatiquement';
        el.attr('data-original-title', msg).tooltip('show');
        document.body.removeChild(copyTextArea);
    }).tooltip();
});
