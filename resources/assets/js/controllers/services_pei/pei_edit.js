// edition d'un point issu d'un service
// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function() {

    var marker;
    //***** Affichage de la carte *********
    function map(){

        // Récupération du marqueur avec ses coordonnées
        var markerdata = $('#maCarte').data("json");
        var latitude = markerdata['coordonnees'][1];
        var longitude = markerdata['coordonnees'][0];

        // Centrage de la carte en fonction du marqueur
        var map = L.map('maCarte').setView([latitude, longitude], 8);

        // Couches OpensStreetMap et Esri_WorldImagery
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });
        // Layer par défaut
        map.addLayer(OpenStreetMap_Mapnik);

        // possibilité de changer de type de carte
        map.addControl(new L.Control.Layers({
            'OpenStreetMap_Mapnik': OpenStreetMap_Mapnik,
            'Esri_WorldImagery': Esri_WorldImagery,
        }));

        //affichage du marker sur la carte
        marker = L.marker([latitude, longitude],{
            draggable:true,
            id:markerdata['id'],
            update:true});
        map.addLayer(marker);

        //si on deplace le marqueur, on affiche le bouton pour sauvegarder
        marker.on('dragend',function(value) {
            $('#sauvegardeMarkers').css('visibility', 'visible');
        });

        //Sauvegarde du marqueur
        $('#sauvegardeMarkers').on('click', function() {
            //je stocke le marqueur contient l'id,latitude,longitude,actif,update
            var marker_id = marker.options.id;
            var marker_lat = marker._latlng.lat;
            var marker_lng = marker._latlng.lng;
            var update = marker.options.update;
            var marker_update = [marker_id,marker_lat,marker_lng,true,update];
            //je convertis le marker en string et je le mets dans la variable markers_update
            $('#marker_update').val(JSON.stringify(marker_update));
        });
    }
    map();
});