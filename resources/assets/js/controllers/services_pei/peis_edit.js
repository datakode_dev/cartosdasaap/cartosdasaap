// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function () {

    //***** Affichage de la carte *********
    function map() {
        // Centrage de la carte
        var map = L.map('maCarte').setView([46.85, 2.3518], 5);

        // Couches OpensStreetMap et Esri_WorldImagery
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });
        // Layer par défaut
        map.addLayer(OpenStreetMap_Mapnik);

        // possibilité de changer de type de carte
        map.addControl(new L.Control.Layers({
            'OpenStreetMap_Mapnik': OpenStreetMap_Mapnik,
            'Esri_WorldImagery': Esri_WorldImagery,
        }));

        // Ajout de la barre de recherche pour rechercher un lieu en fonction de l adresse.
        var geocoder = L.geocoderBAN({ collapsed: true }).addTo(map);

        // Récuperation de tous les markers dans la variable json
        var json = $('#maCarte').data("json");
        var markers_cluster = L.markerClusterGroup({});
        var marker;
        var icon;
        var color;

        // Affichage de tous les markers avec l id dans le title
        $.each(json, function (index, value) {
            var latitude = value['1']['coordinates'][1];
            var longitude = value['1']['coordinates'][0];
            //color = value['3'];
            icon = value['2'];

            var color;
            var score = 1;
            if (typeof JSON.parse(value['4'])['ban.score'] !== 'undefined') {
                score = JSON.parse(value['4'])['ban.score'];
            }
            if (score < 0.75 && score >= 0.50) {
                color = "yellow";
            } else if (score < 0.50 && score >= 0.25) {
                color = "orange";
            }
            else if (score < 0.25) {
                color = "red";
            }
            else {
                color = "green";
            }
            var params = JSON.parse(value['4']);
            var addr;
            if (typeof params['Adresse'] !== 'undefined' && typeof params['Code postal'] !== 'undefined' && typeof params['Commune'] !== 'undefined') {
                addr = params['Adresse'] + " " + params['Code postal'] + " " + params["Commune"];
            } else {
                addr = "Aucune adresse";
            }



            var popupContent = "<input type='button' id='erase' value='Supprimer' class='marker-delete-button'/>";
            marker = L.marker([latitude, longitude], {
                draggable: true,
                title: addr,
                id: value['0'],
                update: false,
                icon: L.AwesomeMarkers.icon({ icon: icon, prefix: 'fa', markerColor: color, iconColor: 'white' })
                // TO DO
            }).bindPopup(popupContent);
            markers_cluster.addLayer(marker);
            map.addLayer(markers_cluster);
            //Si le marqueur a été déplacé, il faut reprendre la position du marqueur et mettre updated à true
            marker.on('dragend', function (value) {
                latitude = value.target._latlng.lat;
                longitude = value.target._latlng.lng;
                var marker_id = value.target.options.id;
                var leaflet_id = value.target._leaflet_id;

                //je supprime le marqueur selectionné dans le tableau afin de mettre le nouveau avec la bonne position
                $.each(markers_cluster._featureGroup._layers, function (index, value) {
                    if (value._leaflet_id === leaflet_id) {
                        markers_cluster.removeLayer(value);
                        var popupContent = "<input type='button' id='erase' value='Supprimer' class='marker-activate-button'/>";
                        var drag_marker = L.marker([latitude, longitude], {
                            draggable: true,
                            update: true,
                            id: marker_id,
                            icon: L.AwesomeMarkers.icon({ icon: icon, prefix: 'fa', markerColor: 'cadetblue', iconColor: color })
                        }).bindPopup(popupContent);
                        markers_cluster.addLayer(drag_marker);
                    }
                });
                $('#sauvegardeMarkers').css('visibility', 'visible');
            });

        });

        // Ajout manuel de markers
        var featureGroup = L.featureGroup().addTo(map);
        var drawControl = new L.Control.Draw({
            draw: {
                polygon: false,
                marker: true,
                polyline: false,
                rectangle: false,
                circle: false,
                circlemarker: false,
            },
        }).addTo(map);

        //Possibilité d ajouter des marqueurs manuellement qui seront ajoutés au markers_cluster
        map.on('draw:created', function (e) {
            var popupContent = "<input type='button' id='erase' value='Supprimer' class='marker-activate-button'/>";
            var marker = L.marker(e.layer._latlng, {
                draggable: true,
                updated: false,
                icon: L.AwesomeMarkers.icon({ icon: icon, prefix: 'fa', markerColor: 'blue', iconColor: color })
            }).bindPopup(popupContent);
            markers_cluster.addLayer(marker);
            map.removeLayer(markers_cluster);
            map.addLayer(markers_cluster);
        });

        // Suppression et activation manuelle d'un marqueur existant
        markers_cluster.on('click', function (e) {

            //lorsque l utilisateur veut supprimer un marqueur
            $('#erase').on('click', function () {
                //je recupere l id de ce marqueur
                var marker_id = e.layer._leaflet_id;

                //je supprime le marqueur selectionné dans le tableau (changement de couleur pour le marqueur)
                $.each(markers_cluster._featureGroup._layers, function (index, value) {
                    if (value._leaflet_id === marker_id) {
                        markers_cluster.removeLayer(value);
                        var popupContent = "<input type='button' id='activate' value='Réactiver' class='marker-activate-button'/>";
                        var marker = L.marker(e.latlng, {
                            draggable: true,
                            id: value.options.id,
                            title: value.options.title,
                            icon: L.AwesomeMarkers.icon({ icon: icon, prefix: 'fa', markerColor: 'red' })
                        }).bindPopup(popupContent);
                        markers_cluster.addLayer(marker);
                    }
                });

                //j'enleve l affichage des marqueurs sur la carte pour actualiser les marqueurs
                map.removeLayer(markers_cluster);
                //j'affiche les marqueurs actualisés'
                map.addLayer(markers_cluster);

                $('#sauvegardeMarkers').css('visibility', 'visible');
            });

            //lorsque l'utilisateur veut réactiver un marqueur
            $('#activate').on('click', function () {
                //je recupere l id de ce marqueur
                var marker_id = e.layer._leaflet_id;

                //je supprime le marqueur selectionné dans le tableau
                $.each(markers_cluster._featureGroup._layers, function (index, value) {
                    if (value._leaflet_id === marker_id) {
                        markers_cluster.removeLayer(value);
                        var popupContent = "TEST <input type='button' id='erase' value='Supprimer' class='marker-delete-button'/>";
                        var marker = L.marker(e.latlng, {
                            draggable: true,
                            title: value.options.title,
                            icon: L.AwesomeMarkers.icon({ icon: icon, prefix: 'fa', markerColor: 'blue', iconColor: color })
                        }).bindPopup(popupContent);
                        markers_cluster.addLayer(marker);
                    }
                });

                //j'enleve l affichage des marqueurs sur la carte pour actualiser les marqueurs
                map.removeLayer(markers_cluster);
                //j'affiche les marqueurs actualisés'
                map.addLayer(markers_cluster);
            });
        });

        $('.leaflet-draw-draw-marker').on('click', function () {
            $('#sauvegardeMarkers').css('visibility', 'visible');
        });

        //Sauvegarde de tous les marqueurs
        $('#sauvegardeMarkers').on('click', function () {
            var new_markers = [];
            //Je recupere tous les markers actifs du marker_cluster dans un array new_markers
            var all_markers = markers_cluster.getLayers();

            //je stocke tous les marqueurs dans un tableau item qui contient l'id,latitude,longitude,actif
            var item = {};
            $.each(all_markers, function (index, value) {

                var marker_id = value.options.id;
                var marker_lat = value._latlng.lat;
                var marker_lng = value._latlng.lng;
                var update = value.options.update;

                var actif = true;
                if (value.options.icon.options.markerColor === 'red') { actif = false }
                item = [marker_id, marker_lat, marker_lng, actif, update];
                new_markers.push(item);
            });

            //je convertis le tableau en string et je le mets dans la variable markers_update
            $('#markers_update').val(JSON.stringify(new_markers));
        });
    }

    map();

});