var map = L.map('map-layer').setView([45.82879925192134, 2.4609375], 4);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    id: 'osm'
}).addTo(map);


var geojson = $('#map-layer').data('geo');

var geojsonLayer = L.geoJson(geojson).addTo(map);

map.fitBounds(geojsonLayer.getBounds());

