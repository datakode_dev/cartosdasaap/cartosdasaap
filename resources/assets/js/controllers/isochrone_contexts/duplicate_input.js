// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function () {

    $("#add_step").click(boucle);

    function boucle() {
        $("#step").append('<div class="form-group">' +
            '<label>Étape (mètre ou minute) </label>' +
            '<input type="number" class="form-control" name="steps[]" value="" placeholder="Ex: 10">' +
            '</div>');
    }
});