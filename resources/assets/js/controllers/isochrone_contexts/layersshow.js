//gestion de l affichage de la page show d'un contexte
$(document).ready(function() {
  /*----------------------------------*/
  /*---MOVED TO resources/js/embed ---*/
  /*----------------------------------*/

  // Wait for the footer container to ba available, and add it then

  const int = setInterval(() => {
    const footer = $('#embed-script-context').find('.box-footer');
      if (footer.length > 0) {
        console.log('found footer');
        footer.html($('#footer').text());
        clearInterval(int);
      }
    console.log('NOT found footer');
  }, 500);


    // Copy embed code
    $('body').on('click', '.btn-clipboard', (ev) => {
        const el = $(ev.currentTarget);
        const copyTextArea = document.createElement("textarea");
        copyTextArea.value = el.parents('figure').find('.embed-code').text();
        document.body.appendChild(copyTextArea);
        copyTextArea.select();
        const res = document.execCommand('copy');
        const msg = res ? 'Copié avec succès' : 'Impossible de copier automatiquement';
        el.attr('data-original-title', msg).tooltip('show');
        document.body.removeChild(copyTextArea);
    }).tooltip();
});
