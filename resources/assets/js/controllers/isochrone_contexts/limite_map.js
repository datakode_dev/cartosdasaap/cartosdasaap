var map = L.map('map-limite', { "dragging": false, "scrollWheelZoom": false, "doubleClickZoom": false, "boxZoom": false }).setView([45.82879925192134, 2.4609375], 4);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    id: 'osm'
}).addTo(map);


map.removeControl(map.zoomControl);

$.ajax({
    type: "GET",
    url: $('#map-limite').data('url'),
    dataType: 'json',
    success: function (response) {
        geojsonLayer = L.geoJson(response, {fillOpacity: 0, color: 'black', weight: 1}).addTo(map);

        map.fitBounds(geojsonLayer.getBounds());
    }
});
