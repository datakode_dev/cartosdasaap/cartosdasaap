import Board from '../../embed/Board';

const thisJs = document.querySelector("script[src*='board_show.js']");

const url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

const baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');


const id = $('#board-main').data('id');
const board = new Board(baseUrl);

board.loadBoard(id).then((b) => {
    board.displayLimit(b.zone_id, b.insee_limit);
    board.loadData().then(() => {
        const tables = board.displayStats();

        const statsBox = $('#stats').find('.box-body');
        tables.forEach((t) => {
            statsBox.append(t);
        });
        statsBox.find('i').tooltip();
    });

});/*.catch(err => {

    $('#board-main').find('.box-body').html('<div class="alert alert-danger">\n' +
        '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le tableau de bord!</h4>\n' +
        '    Ce site web n\'est sans doute pas autorisé à intégrer ce tableau de bord.\n' +
        '    </div>')
});*/

$(document).ready(() => {
    $('#change-limit').on('change', (ev) => {
        const val = $(ev.currentTarget).val();
        board.map.clear();
        board.graph.destroy();
        const d = val.split('.');
        board.insee = d[0];
        board.displayLimit(d[1], d[0]);
        board.loadData();
    });
    $('body').on('click', '.btn-clipboard', (ev) => {
        const el = $(ev.currentTarget);
        const copyTextArea = document.createElement("textarea");
        copyTextArea.value = el.parents('figure').find('.embed-code').text();
        document.body.appendChild(copyTextArea);
        copyTextArea.select();
        const res = document.execCommand('copy');
        const msg = res ? 'Copié avec succès' : 'Impossible de copier automatiquement';
        el.attr('data-original-title', msg).tooltip('show');
        document.body.removeChild(copyTextArea);
    }).tooltip();
});
