var element = document.getElementById('table_account');
if (element !== null) {
    $('#table_account').DataTable({
        language: window.data_table_lang,
        ajax: element.dataset.url,
        columns: [{
            mData: 'id',
            name: 'id'
        },
        {
            mData: 'name',
            name: 'name'
        },
        {
            "data": "links",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                var html = "";
                console.log(sData.edit)
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                $(nTd).html(html);
            }
        }],
    });
}