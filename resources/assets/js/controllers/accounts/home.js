$(document).ready(() => {
    const sc = $('#ign-template')
    const tmpl = sc.text();

    const utrMonitors = {
        778877637: 'map-marker',
        778877639: 'globe',
        778877627: 'male',
        778877625: 'car',
        778877632: 'male',
        778877629: 'car',
    };

    $.get(sc.data('url')).then((res) => {
        const html = res.map((monitor) => {
            let status = monitor.statusClass === 'success' ? 'green' : 'red';

            if (monitor.responseTimes[0].value > 1000) {
                status = 'yellow';
            }

            return tmpl.replace('__STATUS__', status)
                .replace('__NAME__', monitor.name)
                .replace('__VALUE__', monitor.responseTimes[0].value)
                .replace('__ICON__', utrMonitors[monitor.monitorId])
                .replace('__ID__', monitor.monitorId)
        });

        sc.after(html);
    });
});