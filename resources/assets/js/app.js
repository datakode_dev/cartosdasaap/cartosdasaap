import { icons } from './icons';

window.data_table_lang = {
    "lengthMenu": "Afficher _MENU_ enregistrements par page",
    "zeroRecords": "Rien trouvé - désolé",
    "info": "Afficher la page _PAGE_ de _PAGES_",
    "infoEmpty": "Aucun enregistrement disponible",
    "infoFiltered": "(filtré de _MAX_ enregistrements au total)",
    "paginate": {
        "next": "Suivant",
        "previous": "Précédent"
    },
    "loadingRecords": "Chargement...",
    "processing": "En traitement...",
    "search": "Recherche:",
};

$('.input-coo').iconpicker({
    icons: icons,
    placement: "inline",
    templates: {
        search: '<input type="search" class="form-control iconpicker-search" placeholder="Recherche" />',
    }
});
$('.input-coo').on('iconpickerSelected', (event) => {
    $('.previewInput').val(event.iconpickerValue);
    const preview = $('#icon-preview')
    preview.removeClass();
    preview.addClass(event.iconpickerValue)
});

function changeText (){ $('#tarteaucitronManager').html(
    '       Gestion des cookies et RGPD       <span id="tarteaucitronDot"><span id="tarteaucitronDotGreen" style="width: 100%;"></span><span id="tarteaucitronDotYellow" style="width: 0%;"></span><span id="tarteaucitronDotRed" style="width: 0%;"></span></span>');
}

function tm() {
    if ($('#tarteaucitronManager').length) {
        changeText()
    } else {
        setTimeout(tm, 10)
    }
}

tm();

$(document).ready(function () {
    try {
        $('.select2').select2();
    }
    catch (e) {
        console.log('select2 not init')
    }
    // TO REMOVE IF PREPROD
    console.log = function () {}
});

tarteaucitron.init({
    "privacyUrl": "/charte", /* Privacy policy url */

    "hashtag": "#tarteaucitron", /* Open the panel with this hashtag */
    "cookieName": "tarteaucitron", /* Cookie name */

    "orientation": "top", /* Banner position (top - bottom) */
    "showAlertSmall": true, /* Show the small banner on bottom right */
    "cookieslist": true, /* Show the cookie list */

    "adblocker": false, /* Show a Warning if an adblocker is detected */
    "AcceptAllCta": true, /* Show the accept all button when highPrivacy on */
    "highPrivacy": false, /* Disable auto consent */
    "handleBrowserDNTRequest": false, /* If Do Not Track == 1, disallow all */

    "removeCredit": false, /* Remove credit link */
    "moreInfoLink": true, /* Show more info link */
    "useExternalCss": false, /* If false, the tarteaucitron.css file will be loaded */

    //"cookieDomain": ".my-multisite-domaine.fr", /* Shared cookie for subdomain website */

    "readmoreLink": "/cookiespolicy" /* Change the default readmore link pointing to opt-out.ferank.eu */
});
tarteaucitron.user.matomoId = "8";
tarteaucitron.user.matomoHost = "https://matomo.datakode.fr/";
(tarteaucitron.job = tarteaucitron.job || []).push('matomo');