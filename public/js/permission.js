/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 22);
/******/ })
/************************************************************************/
/******/ ({

/***/ 22:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(23);


/***/ }),

/***/ 23:
/***/ (function(module, exports) {

var element = document.getElementById('table_permission');
if (element !== null) {
    $('#table_permission').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'id',
            name: 'id'
        }, {
            mData: 'slug',
            name: 'slug'
        }, {
            mData: 'name',
            name: 'name'
        }]
    });
}
var userTable = document.getElementById('users_table_permission');
if (userTable !== null) {
    $('#users_table_permission').DataTable({
        ajax: userTable.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'id',
            name: 'id'
        }, {
            mData: 'lastname',
            name: 'lastname'
        }, {
            mData: 'firstname',
            name: 'firstname'
        }, {
            mData: 'email',
            name: 'email'
        }, {
            "data": 'accounts',
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                var html = "<p>";
                console.log(nTd, sData, oData, iRow, iCol);

                sData.length && sData.forEach(function (account) {
                    html += '<span class="btn btn-primary btn-xs">' + account.name + '</span> ';
                });

                html += "</p>";

                $(nTd).html(html);
            }
        }, {
            "data": "links",
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                console.log('dd', oData);
                var html = "";
                html += "<a title='Modifier' href='/permissions/" + oData.id + "/edit_accounts'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                $(nTd).html(html);
            }
        }]
    });
}

/***/ })

/******/ });