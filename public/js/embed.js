/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 26);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fatalError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return formatLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return pickColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createWorker; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getPopup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__ = __webpack_require__(1);



var startLoading = function startLoading() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '#embed-script-context';

    var spinner = jQuery('.overlay');
    if (spinner.length === 0) {
        var div = jQuery(target).find('.box');

        if (target === '#embed-script-context') {
            div = jQuery(target).find('.box .box');
        }

        div.append('<div style="transform: translate3d(0px, 0px, 5px);z-index:1000;" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    spinner.html('<i class="fa fa-refresh fa-spin">');
    spinner.show();
};

var stopLoading = function stopLoading() {
    jQuery('.overlay').hide();
};

var fatalError = function fatalError(message) {
    jQuery('.overlay').html('<i class="fa fa-exclamation-triangle"></i><p class="text-muted">' + message + '</p>');
};

var formatLabel = function formatLabel(str, maxwidth) {
    var sections = [];
    if (typeof str === 'undefined') {
        return "";
    }
    var words = str.split(" ");
    var temp = "";

    words.forEach(function (item, index) {
        if (temp.length > 0) {
            var concat = temp + ' ' + item;

            if (concat.length > maxwidth) {
                sections.push(temp);
                temp = "";
            } else {
                if (index === words.length - 1) {
                    sections.push(concat);
                    return;
                } else {
                    temp = concat;
                    return;
                }
            }
        }

        if (index === words.length - 1) {
            sections.push(item);
            return;
        }

        if (item.length < maxwidth) {
            temp = item;
        } else {
            sections.push(item);
        }
    });

    return sections;
};

var pickColor = function pickColor(bgColor) {
    var color = bgColor.charAt(0) === '#' ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map(function (col) {
        if (col <= 0.03928) {
            return col / 12.92;
        }
        return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
    return L > 0.59 ? '#333' : '#fff';
};

var createWorker = function createWorker(workerUrl) {
    var worker = null;
    try {
        var blob = void 0;
        try {
            blob = new Blob(["importScripts('" + workerUrl + "');"], { "type": 'application/javascript' });
        } catch (e) {
            var builder = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder)();
            builder.append("importScripts('" + workerUrl + "');");
            blob = builder.getBlob('application/javascript');
        }
        var url = window.URL || window.webkitURL;
        var blobUrl = url.createObjectURL(blob);
        worker = new Worker(blobUrl);
    } catch (e1) {
        //if it still fails, there is nothing much we can do
    }
    return worker;
};

var getPopup = function getPopup(p) {
    p.properties = Object(__WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__["a" /* mapField */])([{ prop: p.properties }], p.display_fields)[0].prop;
    var title = "";
    if (p.properties && p.properties.name) {
        title = p.properties.name;
    }
    title = '<p class="title"><span class="icon" style="background-color:' + p.color + '"><i class="' + p.icon + '" style="color:' + pickColor(p.color) + '"></i></span> <span class="text" style="color:' + p.color + '">' + title + '</span></p>';
    Object.keys(p.properties).map(function (key) {
        if (key !== 'name' && key !== 'id' && key !== 'serviceId' && p.properties[key] && p.properties[key] !== '') {
            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + p.properties[key] + '</strong></p>';
        }
    });
    return title;
};

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapField; });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var mapField = function mapField(points, fieldMap, selectionMap) {
    if (!selectionMap || (typeof selectionMap === 'undefined' ? 'undefined' : _typeof(selectionMap)) !== 'object') {
        selectionMap = fieldMap;
    }
    if (!fieldMap || (typeof fieldMap === 'undefined' ? 'undefined' : _typeof(fieldMap)) !== 'object') {
        return points.map(function (point) {
            Object.keys(point.prop).forEach(function (k) {
                var val = point.prop[k];
                if (!val) {
                    delete point.prop[k];
                }
                if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                    //Is a link, replace with link
                    val = '<a href="' + val + '" target="_blank">' + val + '</a>';
                    point.prop[k] = val;
                }
            });
            point.selectionProp = point.prop;

            return point;
        });
    }
    return points.map(function (point) {
        var newProps = {};
        Object.keys(fieldMap).forEach(function (k) {
            var val = point.prop[fieldMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newProps[k] = val;
            }
        });

        var newSelectionProps = {};
        Object.keys(selectionMap).forEach(function (k) {
            var val = point.prop[selectionMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newSelectionProps[k] = val;
            }
        });

        point.selectionProp = newSelectionProps;
        point.prop = newProps;
        return point;
    });
};

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Api = function () {
    function Api(baseUrl) {
        _classCallCheck(this, Api);

        this.baseUrl = baseUrl;
    }

    _createClass(Api, [{
        key: 'get',
        value: function get(url) {
            var _this = this;

            if (typeof $ === 'undefined') {
                $ = window.jQuery;
            }
            return new Promise(function (resolve, reject) {
                jQuery.get(_this.baseUrl + url).then(function (res) {
                    return resolve(res);
                }).fail(function (err) {
                    return reject(err);
                });
            });
        }
    }]);

    return Api;
}();

/* harmony default export */ __webpack_exports__["a"] = (Api);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Map = function () {
    function Map(baseUrl) {
        _classCallCheck(this, Map);

        this.map = L.map('maCarte', { maxZoom: 19 }).setView([46.85, 2.3518], 5);
        this.activeLayers = [];
        this.geometry = null;
        this.controls = null;
        this.markers = [];
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });

        if (typeof L.Control.Fullscreen !== 'undefined') {
            this.map.addControl(new L.Control.Fullscreen({
                title: {
                    'false': 'Voir en plein écran',
                    'true': 'Sortir de la vue plein écran'
                }
            }));
        }

        window.proxiclicMap = this.map;

        this.map.addLayer(OpenStreetMap_Mapnik);
    }

    _createClass(Map, [{
        key: 'setControls',
        value: function setControls(controls) {
            this.controls = controls;
        }
    }, {
        key: 'activateLayer',
        value: function activateLayer(layer, color) {
            var _this = this;

            var removePrevious = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
            var fitBounds = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

            if (this.activeLayers && removePrevious) {
                this.activeLayers.forEach(function (l) {
                    _this.map.removeLayer(l);
                });
            }

            var geom = this.displayGeometry(layer.geom, color, false, 0.4, fitBounds);
            this.activeLayers.push(geom);

            var ls = [].concat(_toConsumableArray(this.activeLayers));
            ls = ls.reverse();

            setTimeout(function () {
                _this.markers.forEach(function (m) {
                    if (m) {
                        m.bringToFront();
                    }
                });
            }, 100);

            return geom;
        }
    }, {
        key: 'clear',
        value: function clear() {
            var _this2 = this;

            var fit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

            if (this.activeLayers.length > 0) {
                this.activeLayers.forEach(function (l) {
                    _this2.map.removeLayer(l);
                });
            }
            if (fit) {
                this.map.fitBounds(this.geometry.getBounds());
            }
        }
    }, {
        key: 'displayGeometry',
        value: function displayGeometry(geometry) {
            var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "blue";
            var dashArray = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var opacity = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0.3;
            var fitBounds = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
            var type = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

            var options = {};
            if (type === 'circle') {
                // These are the ponits from the service
                options.pointToLayer = function (feature, latlng) {
                    return L.circleMarker(latlng, {
                        radius: 4
                    });
                };
            }
            var geojsonLayer = L.geoJson(geometry, options);

            // Display service points


            geojsonLayer.setStyle({ fillColor: color, color: dashArray === false ? 'transparent' : color, dashArray: null, fillOpacity: opacity, weight: dashArray === false ? 0 : 1 });
            geojsonLayer.addTo(this.map);

            geojsonLayer.bringToFront();
            if (fitBounds) {
                this.map.fitBounds(geojsonLayer.getBounds());
            }

            return geojsonLayer;
        }
    }, {
        key: 'changeGeometry',
        value: function changeGeometry(geometry) {
            var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "blue";
            var dashArray = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var opacity = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0.4;

            if (this.geometry) {
                this.map.removeLayer(this.geometry);
            }
            this.geometry = this.displayGeometry(geometry, color, dashArray, opacity);
        }

        /**
         * Make a cluster group from a list of markers
         *
         * @param service
         * @param serviceMarkers
         */

    }, {
        key: 'makeGroup',
        value: function makeGroup(service, serviceMarkers) {
            var group = L.markerClusterGroup({
                iconCreateFunction: function iconCreateFunction(cluster) {
                    return L.divIcon({ html: '<div style="background-color:' + service.color + ' !important;color:' + Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["e" /* pickColor */])(service.color) + ' !important"><span>' + cluster.getChildCount() + '</span></div>', className: 'custom-cluster', iconSize: L.point(40, 40) });
                }
            });

            group.addLayers(serviceMarkers);

            return group;
        }
    }]);

    return Map;
}();

/* harmony default export */ __webpack_exports__["a"] = (Map);

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Graph = function () {
    function Graph(item) {
        _classCallCheck(this, Graph);

        //Can be a board or a context
        // Should have indicators and indicator_names
        this.item = item;

        this.services = [];
        this.moveSteps = [];
        this.activeBar = null;
        this.controls = null;
        this.chart = null;
        this.indicator = 0;
        this.results = null;
    }

    _createClass(Graph, [{
        key: 'setControls',
        value: function setControls(controls) {
            this.controls = controls;
        }
    }, {
        key: 'setMoveSteps',
        value: function setMoveSteps(moveSteps) {
            this.moveSteps = moveSteps;
        }
    }, {
        key: 'setIndicator',
        value: function setIndicator(indicator) {
            this.indicator = indicator;
        }
    }, {
        key: 'setResults',
        value: function setResults(results) {
            this.results = results;
        }
    }, {
        key: 'display',
        value: function display() {
            var _this = this;

            console.log('RESULTS', this.results);
            var color = [];

            var colors = ['rgb(206,255,69)', 'rgb(246,255,104)', 'rgb(255,223,128)', 'rgb(255,178,134)', 'rgb(255,120,120)', 'rgb(255,91,135)'];
            var data = this.results.map(function (res, i) {
                var dataset = void 0,
                    label = void 0,
                    fullValue = [],
                    currValue = [];

                if (_this.indicator !== false) {
                    var vals = res.props;
                    var indic = _this.item.indicators[_this.indicator];
                    currValue = [res.props[indic]];

                    if (res.territoire_props && res.territoire_props[indic]) {
                        dataset = [res.props[indic] / res.territoire_props[indic] * 100];
                        fullValue = [res.territoire_props[indic]];
                    } else {
                        dataset = [vals[indic]];
                    }

                    label = _this.moveSteps[i].value;
                } else {
                    dataset = _this.item.indicators.map(function (indic) {
                        currValue.push(res.props[indic]);
                        if (res.territoire_props && res.territoire_props[indic]) {
                            fullValue.push(res.territoire_props[indic]);
                            return res.props[indic] / res.territoire_props[indic] * 100;
                        }

                        return res.props[indic];
                    });

                    label = _this.item.layers.find(function (l) {
                        return l.context_step_move_id === res.step_id || l.id === res.id;
                    }).name;
                }

                // Pick the full range of available colors
                var index = i * Math.ceil(colors.length / _this.results.length);
                if (typeof colors[index] !== 'undefined') {
                    color[i] = colors[index];
                } else {
                    color[i] = colors[5];
                }

                return {
                    label: label,
                    data: dataset,
                    fullValue: fullValue,
                    currValue: currValue,
                    backgroundColor: color[i],
                    borderWidth: 1,
                    //hoverBackgroundColor: 'rgba(236,240,245,1)',
                    borderColor: 'rgba(0, 0, 0, 0.1)'
                };
            });
            //affichage du graphe
            var labels = [__WEBPACK_IMPORTED_MODULE_0__Utils__["c" /* formatLabel */](this.item.indicator_names[this.indicator], 30)];
            if (this.indicator === false) {
                labels = this.item.indicator_names.map(function (i) {
                    return __WEBPACK_IMPORTED_MODULE_0__Utils__["c" /* formatLabel */](i, 20);
                });
            }

            console.log('labels', labels);
            console.log('data', data);
            var ctx = document.getElementById('myChart').getContext('2d');
            this.chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: data
                },
                options: {
                    scales: {
                        xAxes: [{ barPercentage: 0.5 }],
                        yAxes: [{
                            ticks: {
                                precision: 0,
                                beginAtZero: true,
                                callback: function callback(value) {
                                    return (value / 100).toLocaleString('fr-FR', { style: 'percent' });
                                }
                            }
                        }]
                    },
                    title: {
                        display: false,
                        text: "ffrf"
                    },
                    hover: {
                        mode: 'dataset'
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function label(tooltipItems, data) {
                                var multistringText = ['Pourcentage : ' + Math.round(tooltipItems.yLabel) + '%'];
                                var cv = Math.round(data.datasets[tooltipItems.datasetIndex].currValue[tooltipItems.index]);
                                var fv = 'Inconnu';
                                if (typeof data.datasets[tooltipItems.datasetIndex].fullValue !== 'undefined') {
                                    fv = Math.round(data.datasets[tooltipItems.datasetIndex].fullValue[tooltipItems.index]);
                                }

                                multistringText.push(cv + ' sur un total de ' + fv);
                                return multistringText;
                            }
                        }
                    },
                    onHover: function onHover() {
                        var activeBar = null;
                        if (_this.chart.active && _this.chart.active.length) {
                            activeBar = _this.chart.active[0]._datasetIndex;
                            for (var j = 0; j < data.length; j++) {

                                if (j <= _this.chart.active[0]._datasetIndex) {
                                    _this.chart.config.data.datasets[j].backgroundColor = color[j];
                                } else {
                                    _this.chart.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
                                }
                            }
                        }

                        if (_this.activeBar !== activeBar) {
                            _this.chart.update();
                            _this.activeBar = activeBar;
                        }
                    },
                    onClick: function onClick(ev) {
                        ev.preventDefault();
                        // Set context move step ID here
                        if (_this.chart.active && _this.chart.active.length > 0 && typeof _this.moveSteps[_this.chart.active[0]._datasetIndex] !== 'undefined') {
                            var _clickedId = _this.moveSteps[_this.chart.active[0]._datasetIndex].id;

                            var _showResult = _this.results.find(function (r) {
                                return _clickedId === r.id;
                            });
                            if (!_showResult) {
                                //find for context
                                _showResult = _this.results.find(function (r) {
                                    return _clickedId === r.step_id;
                                });
                            }

                            if (_showResult && typeof _showResult.geom !== 'undefined') {
                                // Add layer to map here
                                _this.controls.graphClicked(_showResult, _this.chart.active[0]._view.backgroundColor);
                            }

                            var _loop = function _loop(i) {
                                var clickedId = _this.moveSteps[i].id;

                                var showResult = _this.results.find(function (r) {
                                    return clickedId === r.id;
                                });
                                if (!showResult) {
                                    //find for context
                                    showResult = _this.results.find(function (r) {
                                        return clickedId === r.step_id;
                                    });
                                }

                                if (showResult && typeof showResult.geom !== 'undefined') {
                                    // Add layer to map here
                                    _this.controls.graphClicked(showResult, _this.chart.data.datasets[i].backgroundColor, false);
                                }
                            };

                            for (var i = _this.chart.active[0]._datasetIndex - 1; i >= 0; i--) {
                                _loop(i);
                            }
                        }
                        jQuery('.leaflet-marker-shadow').css('display', 'none');
                        jQuery('.leaflet-marker-icon').css('display', 'none');
                    }
                }
            });

            var clickedId = this.moveSteps[0].id;

            var showResult = this.results.find(function (r) {
                return clickedId === r.id;
            });
            if (!showResult) {
                //find for context
                showResult = this.results.find(function (r) {
                    return clickedId === r.step_id;
                });
            }
            //Make other bars grey
            for (var j = 1; j < this.chart.config.data.datasets.length; j++) {
                this.chart.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
            }
            this.chart.update();

            if (showResult && typeof showResult.geom !== 'undefined') {
                // Add layer to map here
                this.controls.graphClicked(showResult, this.chart.config.data.datasets[0].backgroundColor);
            }
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            this.chart.destroy();
            this.chart = null;
        }
    }]);

    return Graph;
}();

/* harmony default export */ __webpack_exports__["a"] = (Graph);

/***/ }),
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(27);


/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Controls__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__templates_main__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Api__ = __webpack_require__(2);




/*
Usage : <script src="/js/embed.js" data-context-id="CONTEXT_ID"></script>

Where CONTEXT_ID is the id of the context to display
 */

var thisJs = document.querySelector("script[src*='embed.js']");

var url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

var baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
console.log('baseurl', baseUrl);
/**
 *
 * @param url
 * @returns {Promise}
 */
var loadScript = function loadScript(url) {
    return new Promise(function (resolve, reject) {
        var script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

var loadjQuery = function loadjQuery() {
    if (window.jQuery) {
        console.log('jquery ok');
        return Promise.resolve();
    } else {
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (window.jQuery) {
                    console.log('jquery loaded');
                    resolve();
                } else {
                    console.log('jquery loaded from elsewhere');
                    loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(function () {
                        resolve();
                    });
                }
            }, 2000);
        });
    }
};

var css = document.createElement("link");
css.rel = "stylesheet";
css.href = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css";
document.getElementsByTagName("head")[0].appendChild(css);

css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

(function () {
    var contextId = thisJs.getAttribute('data-context-id');

    var jquery = loadjQuery();

    // Is it worth it to load select2 here ?

    jquery.then(function () {
        var chartJs = loadScript('//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js');

        var leaflet = Promise;
        if (typeof window.L === 'undefined') {
            //leaflet loaded
            leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');
        } else {
            leaflet = Promise.resolve();
        }

        var select2 = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js');
        Promise.all([chartJs, leaflet, select2]).then(function () {
            loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js');
            var fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            fullscreen.then(function () {
                //everything loaded and ready to go
                if (typeof $ === 'undefined') {
                    window.$ = window.jQuery;
                }
                jQuery(document).ready(function () {
                    var api = new __WEBPACK_IMPORTED_MODULE_2__Api__["a" /* default */](baseUrl + '/api');
                    jQuery(thisJs).after(jQuery(__WEBPACK_IMPORTED_MODULE_1__templates_main__["a" /* default */]));
                    api.get('/contexts/' + contextId).then(function (context) {
                        if (context.active === false) {
                            jQuery('#disabled-message').show();
                        }
                        var controls = new __WEBPACK_IMPORTED_MODULE_0__Controls__["a" /* default */](context, baseUrl, window.jQuery);
                        controls.displayAll();
                        var event = new Event('proxiclicMap.loaded');
                        window.dispatchEvent(event);
                    }).catch(function (err) {
                        jQuery('#embed-script-context').html('<div class="col-md-12"><div class="alert alert-danger">\n' + '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le contexte.</h4>\n' + '    Ce site web n\'est sans doute pas autorisé à intégrer ce contexte.\n' + '    </div></div>');
                        return console.log(err);
                    });
                });
            });
        });
    });
})();

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Api__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Graph__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Map__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }






var Controls = function () {
    function Controls(context, baseUrl, $) {
        _classCallCheck(this, Controls);

        this.graph = new __WEBPACK_IMPORTED_MODULE_1__Graph__["a" /* default */](context);
        this.graph.setControls(this);

        this.map = new __WEBPACK_IMPORTED_MODULE_2__Map__["a" /* default */](context);
        this.map.setControls(this);
        this.services = [];
        this.context = context;
        this.baseUrl = baseUrl;
        this.selectedService = null;
        this.api = new __WEBPACK_IMPORTED_MODULE_0__Api__["a" /* default */](baseUrl + '/api');
        this.box = jQuery('#embed-script-context');
        this.moveTypes = [];
        this.selectedMoveType = 0;
        this.moveSteps = [];
        // Selected zone
        this.insee = this.context.insee_limit;
        this.selectedIndicator = this.context.indicators[0];
    }

    _createClass(Controls, [{
        key: 'displayAll',
        value: function displayAll() {
            var _this = this;

            __WEBPACK_IMPORTED_MODULE_3__Utils__["f" /* startLoading */]();
            this.load().then(function (results) {
                _this.addControls();
                __WEBPACK_IMPORTED_MODULE_3__Utils__["g" /* stopLoading */]();
                _this.graph.setResults(results);
                _this.graph.display();
                console.log('this', _this);

                // Display initial context on map
                _this.api.get('/insee/' + _this.context.zone_id + '/' + _this.context.insee_limit).then(function (resp) {
                    try {
                        _this.map.changeGeometry(resp, 'black', [10, 10], 0);
                        setTimeout(function () {
                            _this.map.markers.forEach(function (m) {
                                m.bringToFront();
                            });
                        }, 100);
                    } catch (e) {
                        console.log(e);
                    }
                }).catch(function (er) {
                    console.log(er);
                });

                /* https://app.asana.com/0/1179754124938634/1201174414928393 */
                jQuery('.leaflet-marker-shadow').css('display', 'none');
                jQuery('.leaflet-marker-icon').css('display', 'none');
            }).catch(function (er) {
                console.log(er);
                __WEBPACK_IMPORTED_MODULE_3__Utils__["b" /* fatalError */]("Impossible de charger vos données, celles-ci sont probablement en cours de calcul. Merci de réessayer plus tard.");
            });
        }
    }, {
        key: 'graphClicked',
        value: function graphClicked(layer, color) {
            var _this2 = this;

            var removePrevious = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

            try {
                var geom = this.map.activateLayer(layer, color, removePrevious, false);
            } catch (err) {
                console.log(err);
            }

            if (removePrevious) {
                var indic = this.selectedIndicator;
                var i = this.context.indicators.findIndex(function (ind) {
                    return ind === _this2.selectedIndicator;
                });
                var indicName = this.context.indicator_names[i];
                var moveType = this.moveTypes[this.selectedMoveType];
                var service = this.services.find(function (s) {
                    return s.id == _this2.selectedService;
                });
                var step = this.moveSteps.find(function (step) {
                    return step.id === layer.step_id;
                });
                var stepUnit = this.context.iso_type.name.toLowerCase() === 'isochrone' ? 'min' : 'm';

                var movetypeString = 'à <strong>pied</strong>';
                if (moveType.name === 'Voiture') {
                    movetypeString = 'en <strong>voiture</strong>';
                }

                var percent = Math.round(layer.props[indic] / layer.territoire_props[indic] * 100);
                var text = 'les services de type <strong>"' + service.name + '"</strong> sont accessibles par <strong>' + percent + '%</strong> de la catégorie <strong>"' + indicName + '"</strong> à moins de <strong>' + step.value + ' ' + stepUnit + '</strong> ' + movetypeString;

                jQuery('#stats-summary').html(text);
            }
        }
    }, {
        key: 'getMoveTypes',
        value: function getMoveTypes(serviceId) {
            return this.api.get('/contexts/' + this.context.id + '/services/' + serviceId + '/movetypes');
        }
    }, {
        key: 'displayMoveTypes',
        value: function displayMoveTypes(movetypes) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
                // Only taking the first moveType
                // TODO handle different ones for this context
                console.log('movetypes', movetypes);
                _this3.moveTypes = movetypes;
                var movetype = movetypes[_this3.selectedMoveType];

                _this3.api.get('/contexts/' + _this3.context.id + '/services/' + movetype.iso_context_service_id + '/movetypes/' + movetype.id + '/movesteps').then(function (moveSteps) {
                    _this3.moveSteps = moveSteps;
                    _this3.graph.setMoveSteps(moveSteps);
                    console.log('got movestesps', moveSteps);
                    var requests = moveSteps.map(function (step) {
                        return _this3.api.get('/accounts/' + _this3.context.account_id + '/layers/' + step.id + '?insee=' + _this3.insee);
                    });
                    console.log('requests', requests);
                    var result = Promise.all(requests);
                    result.then(function (results) {
                        console.log('results', results);
                        //Only get first result for the time being

                        // Display layer download links
                        var links = results.map(function (res) {
                            return '<li><a href="data:text/geojson;charset=UTF-8,' + encodeURIComponent(res[0].geom) + '" download="' + res[0].layer_name + '.geojson">' + res[0].layer_name + '</a></li>';
                        }).join('');
                        $('#download-layers').html('<ul>' + links + '</ul>');

                        var parsed = results.map(function (res) {
                            console.log('single result', res);
                            if (res && res.length > 0) {
                                var geom = null;

                                try {
                                    geom = JSON.parse(res[0].geom);
                                } catch (e) {

                                    console.log('json parse error', e, res[0]);
                                }
                                var props = null;
                                try {
                                    props = JSON.parse(res[0].props);
                                } catch (e) {
                                    console.log('json parse error', e, res[0]);
                                }
                                var territoire_props = null;
                                try {
                                    territoire_props = JSON.parse(res[0].territoire_props);
                                } catch (e) {
                                    console.log('json parse error', e, res[0]);
                                }

                                return {
                                    step_id: res[0].step_id,
                                    geom: geom,
                                    props: props,
                                    territoire_props: territoire_props
                                };
                            }
                            return null;
                        }).filter(function (r) {
                            return r !== null;
                        });
                        if (parsed.length > 0) {
                            console.log('got parsed layer clips', parsed);
                            return resolve(parsed);
                        }

                        reject('no data');
                    }).catch(function (er) {
                        return reject(er);
                    });
                }).catch(function (er) {
                    return reject(er);
                });
            });
        }
    }, {
        key: 'load',
        value: function load() {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
                _this4.api.get('/contexts/' + _this4.context.id + '/services').then(function (services) {
                    _this4.services = services;

                    if (services.length === 0) {
                        //No services, exit now
                        return;
                    }

                    _this4.displayServiceMarkers(services[0]);

                    _this4.selectedService = services[0].id;
                    //Get layers for the first service
                    var res = _this4.getMoveTypes(services[0].id);
                    res.then(function (movetypes) {
                        _this4.displayMoveTypes(movetypes).then(function (result) {
                            return resolve(result);
                        }).catch(function (er) {
                            return reject(er);
                        });
                    }).catch(function (er) {
                        return reject(er);
                    });
                }).catch(function (er) {
                    return reject(er);
                });
            });
        }
    }, {
        key: 'addControls',
        value: function addControls() {
            var _this5 = this;

            console.log('context', this.context);
            console.log('services', this.services);
            var box = this.box;
            if (this.services.length <= 1) {
                var servDiv = box.find('#change-service');
                servDiv.append('<label>Service</label>');
                servDiv.append('<div>' + this.services[0].name + ' <i class="fa fa-info-circle" title="' + this.services[0].info + '"></i></div>');

                servDiv.find('i').tooltip();
            } else {
                var ser = this.createSelect('Service', 'change-service', this.services.map(function (s) {
                    return s.id;
                }), this.services.map(function (s) {
                    return s.name;
                }));

                var _servDiv = box.find('#change-service');
                _servDiv.append(ser.label);
                _servDiv.append(ser.select);
                ser.select.on('change', function (ev) {
                    _this5.serviceChanged(jQuery(ev.target).val());
                });
            }
            if (this.context.indicators.length > 1) {
                var res = this.createSelect('Population', 'change-indicator', this.context.indicators, this.context.indicator_names);

                var inDiv = box.find('#change-indicator');
                inDiv.append(res.label);
                inDiv.append(res.select);
                res.select.on('change', function (ev) {
                    _this5.indicatorChanged(jQuery(ev.target).val());
                });
            } else {
                var _servDiv2 = box.find('#change-indicator');
                _servDiv2.append('<label>Indicateurs</label>');
                _servDiv2.append('<div>' + this.context.indicator_names[0] + '</div>');
            }

            this.addMoveTypeSelect();

            if (typeof jQuery.fn.select2 !== 'undefined') {
                box.find('select').select2();
            }

            var zDiv = box.find('#change-zone');

            if (this.context.load_full === false) {
                zDiv.append('<label>Territoire</label>');
                return this.api.get('/limit/name/' + this.context.insee_limit + '.' + this.context.zone_id).then(function (name) {
                    zDiv.append('<div>' + name + '</div>');
                });
            }

            var zoneSel = jQuery('<select name="zone" id="change-zone" class="form-control select2_ajax"></select>');

            zDiv.append(zoneSel);
            zoneSel.before('<label for="change-zone">Territoire</label>');
            zoneSel.on('change', function (ev) {
                _this5.zoneChanged(jQuery(ev.target).val());
            });
            if (typeof jQuery.fn.select2 !== 'undefined') {
                zDiv.find('select').select2({
                    placeholder: "Territoire",
                    ajax: {
                        url: this.baseUrl + ('/api/accounts/' + this.context.account_id + '/layers_insee/' + this.context.id),
                        dataType: 'json',
                        delay: 250,
                        data: function data(params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function processResults(data, params) {
                            return {
                                results: data.data
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function escapeMarkup(markup) {
                        return markup;
                    },
                    minimumInputLength: 1,
                    templateResult: function templateResult(repo) {
                        if (repo.text || repo.loading) {
                            return repo.text;
                        }

                        return "<div>" + repo.name + "<span class='label label-default'>" + repo.type + "</span> </div>";
                    },
                    templateSelection: function templateSelection(repo) {
                        if (repo.text || repo.loading) {
                            return repo.text;
                        }
                        return "<div>" + repo.name + "</div>";
                    }
                });
            }
        }
    }, {
        key: 'createSelect',
        value: function createSelect(humanName, name, values, displays) {
            var options = values.map(function (indicator, i) {
                return '<option value="' + indicator + '">' + displays[i] + '</option>';
            });
            var select = jQuery('<select id="' + name + '" class="select2">' + options + '</select>');

            var label = jQuery('<label for="' + name + '">' + humanName + '</label> ');

            return { select: select, label: label };
        }
    }, {
        key: 'moveTypeChanged',
        value: function moveTypeChanged(newMoveType) {
            var _this6 = this;

            console.log('moveTypeChanged', newMoveType);
            console.log('moveTypes', this.moveTypes);

            this.selectedMoveType = this.moveTypes.findIndex(function (m) {
                return m.id === parseInt(newMoveType, 10);
            });
            __WEBPACK_IMPORTED_MODULE_3__Utils__["f" /* startLoading */]();
            console.log('selected move type is ', this.selectedMoveType);
            this.map.clear();
            this.displayMoveTypes(this.moveTypes).then(function (result) {
                _this6.graph.destroy();
                _this6.graph.setResults(result);
                _this6.graph.display();
                __WEBPACK_IMPORTED_MODULE_3__Utils__["g" /* stopLoading */]();
            }).catch(function (er) {
                console.log(er);
                __WEBPACK_IMPORTED_MODULE_3__Utils__["b" /* fatalError */]("Les données n'ont pas pu être chargées. Elles sont probablement en cours de calcul. Merci de réessayer plus tard");
            });
        }
    }, {
        key: 'zoneChanged',
        value: function zoneChanged(newZone) {
            var _this7 = this;

            console.log('new zone', newZone);
            var z = newZone.split('.');
            this.insee = z[0];
            this.serviceChanged(this.selectedService);

            this.api.get('/insee/' + z[1] + '/' + z[0]).then(function (resp) {
                _this7.map.changeGeometry(resp, 'black', [10, 10], 0.1);
            }).catch(function (er) {
                console.log(er);
            });
        }
    }, {
        key: 'indicatorChanged',
        value: function indicatorChanged(newIndicator) {
            this.selectedIndicator = newIndicator;
            var index = this.context.indicators.indexOf(newIndicator);
            this.graph.destroy();
            this.graph.setIndicator(index);
            this.graph.display();
        }
    }, {
        key: 'serviceChanged',
        value: function serviceChanged(newService) {
            var _this8 = this;

            console.log('new service', newService);
            this.selectedService = newService;
            var serv = this.services.find(function (s) {
                return s.id === parseInt(newService, 10);
            });
            console.log('new SERVICE', serv);
            this.displayServiceMarkers(serv);
            __WEBPACK_IMPORTED_MODULE_3__Utils__["f" /* startLoading */]();
            var res = this.getMoveTypes(newService);
            res.then(function (movetypes) {
                _this8.moveTypes = movetypes;
                var sel = _this8.addMoveTypeSelect();

                if (sel) {
                    sel.val(_this8.moveTypes[_this8.selectedMoveType].id).trigger('change');
                } else {
                    _this8.moveTypeChanged(_this8.moveTypes[_this8.selectedMoveType].id);
                }

                _this8.map.clear();
            }).catch(function (er) {
                __WEBPACK_IMPORTED_MODULE_3__Utils__["b" /* fatalError */]("Les données n'ont pas pu être chargées. Elles sont probablement en cours de calcul. Merci de réessayer plus tard");
                console.log(er);
            });
        }
    }, {
        key: 'addMoveTypeSelect',
        value: function addMoveTypeSelect() {
            var _this9 = this;

            if (this.moveTypes.length > 1) {
                var movetype = this.createSelect('Type de déplacement', 'change-movetype', this.moveTypes.map(function (s) {
                    return s.id;
                }), this.moveTypes.map(function (s) {
                    return s.name;
                }));

                var _movetypeDiv = this.box.find('#change-movetype');
                _movetypeDiv.html('');
                _movetypeDiv.append(movetype.label);
                _movetypeDiv.append(movetype.select);
                movetype.select.on('change', function (ev) {
                    _this9.moveTypeChanged(jQuery(ev.target).val());
                });
                var sel = movetype.select;
                if (typeof jQuery.fn.select2 !== 'undefined') {
                    sel = _movetypeDiv.find('select').select2();
                }
                return sel;
            }
            var movetypeDiv = this.box.find('#change-movetype');
            movetypeDiv.html('');
            movetypeDiv.append('<label>Type de déplacement</label>');
            movetypeDiv.append('<div>' + this.moveTypes[0].name + '</div>');
        }
    }, {
        key: 'displayServiceMarkers',
        value: function displayServiceMarkers(service) {
            var _this10 = this;

            // Remove all map markers
            this.map.markers.forEach(function (m) {
                _this10.map.map.removeLayer(m);
            });
            this.map.markers = [];
            //Display service points
            service.points.forEach(function (p) {
                var coords = new L.LatLng(p.geometry.coordinates[1], p.geometry.coordinates[0]);
                var marker = L.circleMarker(coords, {
                    radius: 4,
                    color: "#000000",
                    fillOpacity: 0
                });

                var title = __WEBPACK_IMPORTED_MODULE_3__Utils__["d" /* getPopup */](p);

                marker.bindPopup(title);
                _this10.map.markers.push(marker);
                _this10.map.map.addLayer(marker);
                marker.bringToFront();
            });
        }
    }]);

    return Controls;
}();

/* harmony default export */ __webpack_exports__["a"] = (Controls);

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var main = "\n<div class=\"row cartosdaasap-embed\" id=\"embed-script-context\">\n        <div class=\"col-md-12\">\n            \n            <div class=\"box box-primary\">\n                <div class=\"box-header with-border\">\n                    <h3 class=\"box-title\" id=\"context-name\"></h3>\n                    <div class=\"row\">\n                            <div class=\"col-md-3\" id=\"change-service\">\n                            </div>\n                            <div class=\"col-md-3\" id=\"change-zone\">\n                            </div>\n                            <div class=\"col-md-3\" id=\"change-indicator\">\n                            </div>\n                            <div class=\"col-md-3\" id=\"change-movetype\">\n                            </div>\n                    </div>\n                </div>\n                <!-- /.box-header -->\n                <div class=\"box-body\">\n                <div class=\"callout callout-warning\" style=\"display:none\" id=\"disabled-message\">\n                <h4>Ce contexte est d\xE9sactiv\xE9</h4>\n\n                <p>Les informations pr\xE9sent\xE9es ne sont peut-\xEAtre pas \xE0 jour.</p>\n            </div>\n                    <div id=\"show_layer\">\n                        <div class='col-md-5' id='box_chart_comment'>\n                            <div class='box'>\n                               <canvas id='myChart'></canvas>\n                               </div>\n                           <div class='box'>\n                            <div class='box-header with-border'>\n                         <h3 class='box-title'>Synth\xE8se statistique :</h3>\n                                    </div>\n                                <div class='box-body with-border' id=\"stats-summary\">\n                                \n                               </div>\n                              </div>\n                              <div class='box box-warning'>\n                            <div class='box-header with-border'>\n                         <h3 class='box-title'>T\xE9l\xE9charger les couches :</h3>\n                                    </div>\n                                <div class='box-body with-border' id=\"download-layers\">\n                                \n                               </div>\n                              </div>\n                            </div>\n                             \n                        <div class='col-md-7'>\n                            <div id='maCarte' class='col-md-12' style='height: 75vh;'>\n                               </div>\n                            </div>\n                    </div>\n                </div>\n                <!-- /.box-body -->\n                <div class=\"box-footer\">\n                                \n                </div>\n            </div>\n        </div>\n    </div>\n";

/* harmony default export */ __webpack_exports__["a"] = (main);

/***/ })
/******/ ]);