/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 58);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fatalError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return formatLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return pickColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createWorker; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getPopup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__ = __webpack_require__(1);



var startLoading = function startLoading() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '#embed-script-context';

    var spinner = jQuery('.overlay');
    if (spinner.length === 0) {
        var div = jQuery(target).find('.box');

        if (target === '#embed-script-context') {
            div = jQuery(target).find('.box .box');
        }

        div.append('<div style="transform: translate3d(0px, 0px, 5px);z-index:1000;" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    spinner.html('<i class="fa fa-refresh fa-spin">');
    spinner.show();
};

var stopLoading = function stopLoading() {
    jQuery('.overlay').hide();
};

var fatalError = function fatalError(message) {
    jQuery('.overlay').html('<i class="fa fa-exclamation-triangle"></i><p class="text-muted">' + message + '</p>');
};

var formatLabel = function formatLabel(str, maxwidth) {
    var sections = [];
    if (typeof str === 'undefined') {
        return "";
    }
    var words = str.split(" ");
    var temp = "";

    words.forEach(function (item, index) {
        if (temp.length > 0) {
            var concat = temp + ' ' + item;

            if (concat.length > maxwidth) {
                sections.push(temp);
                temp = "";
            } else {
                if (index === words.length - 1) {
                    sections.push(concat);
                    return;
                } else {
                    temp = concat;
                    return;
                }
            }
        }

        if (index === words.length - 1) {
            sections.push(item);
            return;
        }

        if (item.length < maxwidth) {
            temp = item;
        } else {
            sections.push(item);
        }
    });

    return sections;
};

var pickColor = function pickColor(bgColor) {
    var color = bgColor.charAt(0) === '#' ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map(function (col) {
        if (col <= 0.03928) {
            return col / 12.92;
        }
        return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
    return L > 0.59 ? '#333' : '#fff';
};

var createWorker = function createWorker(workerUrl) {
    var worker = null;
    try {
        var blob = void 0;
        try {
            blob = new Blob(["importScripts('" + workerUrl + "');"], { "type": 'application/javascript' });
        } catch (e) {
            var builder = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder)();
            builder.append("importScripts('" + workerUrl + "');");
            blob = builder.getBlob('application/javascript');
        }
        var url = window.URL || window.webkitURL;
        var blobUrl = url.createObjectURL(blob);
        worker = new Worker(blobUrl);
    } catch (e1) {
        //if it still fails, there is nothing much we can do
    }
    return worker;
};

var getPopup = function getPopup(p) {
    p.properties = Object(__WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__["a" /* mapField */])([{ prop: p.properties }], p.display_fields)[0].prop;
    var title = "";
    if (p.properties && p.properties.name) {
        title = p.properties.name;
    }
    title = '<p class="title"><span class="icon" style="background-color:' + p.color + '"><i class="' + p.icon + '" style="color:' + pickColor(p.color) + '"></i></span> <span class="text" style="color:' + p.color + '">' + title + '</span></p>';
    Object.keys(p.properties).map(function (key) {
        if (key !== 'name' && key !== 'id' && key !== 'serviceId' && p.properties[key] && p.properties[key] !== '') {
            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + p.properties[key] + '</strong></p>';
        }
    });
    return title;
};

/***/ }),

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapField; });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var mapField = function mapField(points, fieldMap, selectionMap) {
    if (!selectionMap || (typeof selectionMap === 'undefined' ? 'undefined' : _typeof(selectionMap)) !== 'object') {
        selectionMap = fieldMap;
    }
    if (!fieldMap || (typeof fieldMap === 'undefined' ? 'undefined' : _typeof(fieldMap)) !== 'object') {
        return points.map(function (point) {
            Object.keys(point.prop).forEach(function (k) {
                var val = point.prop[k];
                if (!val) {
                    delete point.prop[k];
                }
                if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                    //Is a link, replace with link
                    val = '<a href="' + val + '" target="_blank">' + val + '</a>';
                    point.prop[k] = val;
                }
            });
            point.selectionProp = point.prop;

            return point;
        });
    }
    return points.map(function (point) {
        var newProps = {};
        Object.keys(fieldMap).forEach(function (k) {
            var val = point.prop[fieldMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newProps[k] = val;
            }
        });

        var newSelectionProps = {};
        Object.keys(selectionMap).forEach(function (k) {
            var val = point.prop[selectionMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newSelectionProps[k] = val;
            }
        });

        point.selectionProp = newSelectionProps;
        point.prop = newProps;
        return point;
    });
};

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(59);
module.exports = __webpack_require__(60);


/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__embed_Utils__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__point_fields_map__ = __webpack_require__(1);



// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function () {

    //******************************************************************************************************************
    //************************************* Affichage de la carte ******************************************************
    //******************************************************************************************************************

    function map() {
        var map = L.map('maCarte').setView([46.85, 2.3518], 5);

        console.log('map', map);

        //  Couches OpensStreetMap et Esri_WorldImagery
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });
        // Layer par défaut
        map.addLayer(OpenStreetMap_Mapnik);

        // possibilité de changer de type de carte
        map.addControl(new L.Control.Layers({
            'OpenStreetMap_Mapnik': OpenStreetMap_Mapnik,
            'Esri_WorldImagery': Esri_WorldImagery
        }));

        // ajout de la barre de recherche pour rechercher un lieu en fonction de l adresse.
        var geocoder = L.geocoderBAN({ collapsed: true }).addTo(map);
        var mapDiv = $('#maCarte');
        var json = mapDiv.data("json");

        console.log('mapDiv', mapDiv);
        console.log('json', json);

        var selectionMap;
        if (!selectionMap) {
            selectionMap = mapDiv.data('map-selections');
        }
        json = Object(__WEBPACK_IMPORTED_MODULE_1__point_fields_map__["a" /* mapField */])(json, mapDiv.data('map-fields'), selectionMap);

        var layerType = json[0].geom.type;

        // ----- POLYGONS -----
        if (json.length && (layerType === 'Polygon' || layerType === 'MultiPolygon')) {
            var colorOutline = mapDiv.data("icon").color_outline_name;
            var colorFill = mapDiv.data("icon").color_fill_name;

            console.log('LAYER BEFORE', json);

            // REVERSE GEOJSON COORDINATES FOR LEAFLET || GENERATE LAYERS BY GEOJSON WASNT WORKING
            layerType === 'Polygon' && json.map(function (layer) {
                return layer.geom.coordinates;
            }).map(function (layer) {
                return layer[0].map(function (arr) {
                    arr.reverse();
                });
            });
            layerType === 'MultiPolygon' && json.map(function (layer) {
                return layer.geom.coordinates;
            }).map(function (layer) {
                return layer[0].map(function (arr) {
                    return arr.map(function (arr2) {
                        arr2.reverse();
                    });
                });
            });

            var polygons = []; // TO FIT BOUNDS

            json.forEach(function (value) {

                // SET POPUP CONTENT
                var title = value.prop && value.prop.name ? value.prop.name : "";
                title = '<p class="title"><span class="text" style="color:' + colorOutline + '">' + title + '</span></p>';
                if (value.prop) {
                    Object.keys(value.prop).map(function (key) {
                        if (key !== 'name') {
                            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + value.prop[key] + '</strong></p>';
                        }
                    });
                }
                // CREATE POLYGON
                var poly = L.polygon(value.geom.coordinates).setStyle({ color: colorOutline, fillColor: colorFill }).bindPopup(title).addTo(map);
                polygons.push(poly);
            });

            var bounds = polygons.map(function (p) {
                return p.getBounds();
            });
            map.fitBounds(bounds);
        }

        // ----- LINES ----- SAME AS POLYGON
        if (json.length && (layerType === "LineString" || layerType === 'MultiLineString')) {
            var _colorOutline = mapDiv.data("icon").color_outline_name;

            // REVERSE GEOJSON COORDINATES FOR LEAFLET
            layerType === 'LineString' && json.map(function (layer) {
                return layer.geom.coordinates;
            }).map(function (layer) {
                return layer.map(function (arr) {
                    arr.reverse();
                });
            });
            layerType === 'MultiLineString' && json.map(function (multi) {
                return multi.geom.coordinates;
            }).map(function (coor) {
                return coor.map(function (path) {
                    return path.map(function (point) {
                        point.reverse();
                    });
                });
            });

            var polylines = []; // TO FIT BOUNDS
            console.log(json);

            json.forEach(function (value) {
                // SET POPUP CONTENT
                var title = value.prop && value.prop.name ? value.prop.name : "";
                title = '<p class="title"><span class="text" style="color:' + _colorOutline + '">' + title + '</span></p>';
                if (value.prop) {
                    Object.keys(value.prop).map(function (key) {
                        if (key !== 'name') {
                            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + value.prop[key] + '</strong></p>';
                        }
                    });
                }
                // CREATE POLYGON
                var poly = L.polyline(value.geom.coordinates).setStyle({ color: _colorOutline }).bindPopup(title).addTo(map);
                polylines.push(poly);
            });

            var _bounds = polylines.map(function (p) {
                return p.getBounds();
            });
            map.fitBounds(_bounds);
        }

        // ----- MARKERS -----
        if (json.length && (layerType === 'Point' || layerType === 'MultiPoint')) {
            var color = mapDiv.data("color");
            var icon = mapDiv.data("icon");

            var markers = L.markerClusterGroup({
                iconCreateFunction: function iconCreateFunction(cluster) {
                    return L.divIcon({ html: '<div style="background-color:' + color + ';color:' + Object(__WEBPACK_IMPORTED_MODULE_0__embed_Utils__["e" /* pickColor */])(color) + '"><span>' + cluster.getChildCount() + '</span></div>', className: 'custom-cluster', iconSize: L.point(40, 40) });
                }
            });

            $.each(json, function (index, value) {

                var iconContent = icon.icon_svg ? '<img src="' + window.location.origin + '/storage/' + icon.icon_path + '" alt="' + icon.icon_name + '" style="color: ' + Object(__WEBPACK_IMPORTED_MODULE_0__embed_Utils__["e" /* pickColor */])(color) + '!important;max-width: 18px!important;"></i>' : '<i class="' + icon.icon_name + '" style="color: ' + Object(__WEBPACK_IMPORTED_MODULE_0__embed_Utils__["e" /* pickColor */])(color) + ' !important"></i>';

                var title = value.prop && value.prop.name ? value.prop.name : "";
                title = '<p class="title"><span class="icon" style="background-color:' + color + '">' + iconContent + '</span> <span class="text" style="color:' + color + '">' + title + '</span></p>';
                if (value.prop) {
                    Object.keys(value.prop).map(function (key) {
                        if (key !== 'name') {
                            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + value.prop[key] + '</strong></p>';
                        }
                    });
                }

                if (json.length && json[0].geom.type === 'Point') {
                    var latitude = value.geom.coordinates[1];
                    var longitude = value.geom.coordinates[0];

                    var marker = L.marker([latitude, longitude], {
                        title: title,
                        icon: L.divIcon({
                            html: '<div style="border-color:' + color + '"><div>' + iconContent + '</div></div>',
                            iconSize: [20, 20],
                            className: 'service-point-icon',
                            iconAnchor: [17, 54],
                            popupAnchor: [0, -50]
                        })
                    }).bindPopup(title);
                    markers.addLayer(marker);
                }
                if (json.length && json[0].geom.type === 'MultiPoint') {

                    json.map(function (multi) {
                        return multi.geom.coordinates;
                    }).map(function (coor) {
                        return coor.map(function (point) {
                            point.reverse();
                        });
                    });

                    value.geom.coordinates.forEach(function (point) {
                        var marker = L.marker(point, {
                            title: title,
                            icon: L.divIcon({
                                html: '<div style="border-color:' + color + '"><div>' + iconContent + '</div></div>',
                                iconSize: [20, 20],
                                className: 'service-point-icon',
                                iconAnchor: [17, 54],
                                popupAnchor: [0, -50]
                            })
                        }).bindPopup(title);
                        markers.addLayer(marker);
                    });
                }
            });

            map.fitBounds(markers.getBounds());
            map.addLayer(markers);
        }
    }

    map();

    // Copy embed code
    $('body').on('click', '.btn-clipboard', function (ev) {
        var el = $(ev.currentTarget);
        var copyTextArea = document.createElement("textarea");
        copyTextArea.value = el.parents('figure').find('.embed-code').text();
        document.body.appendChild(copyTextArea);
        copyTextArea.select();
        var res = document.execCommand('copy');
        var msg = res ? 'Copié avec succès' : 'Impossible de copier automatiquement';
        el.attr('data-original-title', msg).tooltip('show');
        document.body.removeChild(copyTextArea);
    }).tooltip();
});

/***/ }),

/***/ 60:
/***/ (function(module, exports) {

var element = document.getElementById('table_markers_score');
if (element === null) {} else {
    $('#table_markers_score').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'score',
            name: 'score'
        },
        /*{
            mData: 'name',
            name: 'name'
        },*/
        {
            mData: 'adresse',
            name: 'adresse'
        }, {
            mData: 'adresse_ban',
            name: 'adresse_ban'
        }, {
            "data": "links",
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                var html = "";
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                $(nTd).html(html);
            }
        }]
    });
}

/***/ })

/******/ });