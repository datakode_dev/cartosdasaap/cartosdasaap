/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 73);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fatalError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return formatLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return pickColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createWorker; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getPopup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__ = __webpack_require__(1);



var startLoading = function startLoading() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '#embed-script-context';

    var spinner = jQuery('.overlay');
    if (spinner.length === 0) {
        var div = jQuery(target).find('.box');

        if (target === '#embed-script-context') {
            div = jQuery(target).find('.box .box');
        }

        div.append('<div style="transform: translate3d(0px, 0px, 5px);z-index:1000;" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    spinner.html('<i class="fa fa-refresh fa-spin">');
    spinner.show();
};

var stopLoading = function stopLoading() {
    jQuery('.overlay').hide();
};

var fatalError = function fatalError(message) {
    jQuery('.overlay').html('<i class="fa fa-exclamation-triangle"></i><p class="text-muted">' + message + '</p>');
};

var formatLabel = function formatLabel(str, maxwidth) {
    var sections = [];
    if (typeof str === 'undefined') {
        return "";
    }
    var words = str.split(" ");
    var temp = "";

    words.forEach(function (item, index) {
        if (temp.length > 0) {
            var concat = temp + ' ' + item;

            if (concat.length > maxwidth) {
                sections.push(temp);
                temp = "";
            } else {
                if (index === words.length - 1) {
                    sections.push(concat);
                    return;
                } else {
                    temp = concat;
                    return;
                }
            }
        }

        if (index === words.length - 1) {
            sections.push(item);
            return;
        }

        if (item.length < maxwidth) {
            temp = item;
        } else {
            sections.push(item);
        }
    });

    return sections;
};

var pickColor = function pickColor(bgColor) {
    var color = bgColor.charAt(0) === '#' ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map(function (col) {
        if (col <= 0.03928) {
            return col / 12.92;
        }
        return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
    return L > 0.59 ? '#333' : '#fff';
};

var createWorker = function createWorker(workerUrl) {
    var worker = null;
    try {
        var blob = void 0;
        try {
            blob = new Blob(["importScripts('" + workerUrl + "');"], { "type": 'application/javascript' });
        } catch (e) {
            var builder = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder)();
            builder.append("importScripts('" + workerUrl + "');");
            blob = builder.getBlob('application/javascript');
        }
        var url = window.URL || window.webkitURL;
        var blobUrl = url.createObjectURL(blob);
        worker = new Worker(blobUrl);
    } catch (e1) {
        //if it still fails, there is nothing much we can do
    }
    return worker;
};

var getPopup = function getPopup(p) {
    p.properties = Object(__WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__["a" /* mapField */])([{ prop: p.properties }], p.display_fields)[0].prop;
    var title = "";
    if (p.properties && p.properties.name) {
        title = p.properties.name;
    }
    title = '<p class="title"><span class="icon" style="background-color:' + p.color + '"><i class="' + p.icon + '" style="color:' + pickColor(p.color) + '"></i></span> <span class="text" style="color:' + p.color + '">' + title + '</span></p>';
    Object.keys(p.properties).map(function (key) {
        if (key !== 'name' && key !== 'id' && key !== 'serviceId' && p.properties[key] && p.properties[key] !== '') {
            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + p.properties[key] + '</strong></p>';
        }
    });
    return title;
};

/***/ }),

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapField; });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var mapField = function mapField(points, fieldMap, selectionMap) {
    if (!selectionMap || (typeof selectionMap === 'undefined' ? 'undefined' : _typeof(selectionMap)) !== 'object') {
        selectionMap = fieldMap;
    }
    if (!fieldMap || (typeof fieldMap === 'undefined' ? 'undefined' : _typeof(fieldMap)) !== 'object') {
        return points.map(function (point) {
            Object.keys(point.prop).forEach(function (k) {
                var val = point.prop[k];
                if (!val) {
                    delete point.prop[k];
                }
                if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                    //Is a link, replace with link
                    val = '<a href="' + val + '" target="_blank">' + val + '</a>';
                    point.prop[k] = val;
                }
            });
            point.selectionProp = point.prop;

            return point;
        });
    }
    return points.map(function (point) {
        var newProps = {};
        Object.keys(fieldMap).forEach(function (k) {
            var val = point.prop[fieldMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newProps[k] = val;
            }
        });

        var newSelectionProps = {};
        Object.keys(selectionMap).forEach(function (k) {
            var val = point.prop[selectionMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newSelectionProps[k] = val;
            }
        });

        point.selectionProp = newSelectionProps;
        point.prop = newProps;
        return point;
    });
};

/***/ }),

/***/ 2:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Api = function () {
    function Api(baseUrl) {
        _classCallCheck(this, Api);

        this.baseUrl = baseUrl;
    }

    _createClass(Api, [{
        key: 'get',
        value: function get(url) {
            var _this = this;

            if (typeof $ === 'undefined') {
                $ = window.jQuery;
            }
            return new Promise(function (resolve, reject) {
                jQuery.get(_this.baseUrl + url).then(function (res) {
                    return resolve(res);
                }).fail(function (err) {
                    return reject(err);
                });
            });
        }
    }]);

    return Api;
}();

/* harmony default export */ __webpack_exports__["a"] = (Api);

/***/ }),

/***/ 6:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Api__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Gp__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__controllers_services_pei_point_fields_map__ = __webpack_require__(1);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }






var ResearchContext = function () {
    function ResearchContext(contextId) {
        var _this = this;

        var contextLoadedCallback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
        var baseUrl = arguments[2];

        _classCallCheck(this, ResearchContext);

        this.api = new __WEBPACK_IMPORTED_MODULE_0__Api__["a" /* default */](baseUrl + '/api');
        this.baseUrl = baseUrl;
        this.obtained = [];
        this.markers = [];
        this.group = this._makeGroup([]);
        this.points = [];
        this.servicePoints = {};
        this.start = null;
        this.end = null;
        this.homeMarker = null;
        this.startMarker = null;
        this.endMarker = null;
        this.buffer = null;
        this.route = null;
        this.context = null;
        this.currentCityLimit = null;
        this.startcityLimit = null;
        this.endcityLimit = null;

        this.services = [];

        this.filters = [];

        this.radius = 5000;
        this.bufferRadius = 5;
        this.map = L.map('search-map', { center: L.latLng(46.85, 2.3518), zoom: 5, maxZoom: 19 });

        // HIDE ZOOM OPTIONS IF MOBILE
        console.log('screen', jQuery(window).width());
        if (jQuery(window).width() < 400) {
            this.map.zoomControl.remove();
        } else {
            if (typeof L.Control.Fullscreen !== 'undefined') {
                this.map.addControl(new L.Control.Fullscreen({
                    title: {
                        'false': 'Voir en plein écran',
                        'true': 'Sortir de la vue plein écran'
                    }
                }));
            }
        }

        this.limit = null;
        this.map.addLayer(this.group);
        var lyrOSM = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png?', { attribution: '&copy; <a href=\"https://osm.org/copyright\">OpenStreetMap</a> contributors' });
        this.map.addLayer(lyrOSM);
        window.proxiclicMap = this.map;

        this.api.get('/researchcontexts/' + contextId).then(function (ctx) {
            _this.context = ctx;
            _this.apiKey = ctx.key;

            Object(__WEBPACK_IMPORTED_MODULE_2__Gp__["b" /* getConfig */])(_this.apiKey).then(function () {

                var lyrOrtho = L.geoportalLayer.WMTS({
                    layer: "ORTHOIMAGERY.ORTHOPHOTOS"
                });

                L.control.layers({ 'Plan': lyrOSM, 'Photographie a&eacuterienne': lyrOrtho }).addTo(_this.map);
            });

            _this.api.get('/insee/' + ctx.zone_id + '/' + ctx.insee_limit + '/' + ctx.dist_around_limit).then(function (resp) {
                try {
                    _this.limit = L.geoJson(resp, { style: { fillOpacity: 0, color: 'black', weight: 1 } });
                    _this.limit.addTo(_this.map);
                    _this.map.fitBounds(_this.limit.getBounds());
                    if (contextLoadedCallback) {
                        setTimeout(function () {
                            contextLoadedCallback(ctx);
                        }, 500);
                    }
                } catch (e) {
                    console.log(e);
                    if (contextLoadedCallback) {
                        contextLoadedCallback(null, e);
                    }
                }
            }).catch(function (er) {
                console.log(er);
                if (contextLoadedCallback) {
                    contextLoadedCallback(null, er);
                }
            });
        }).catch(function (err) {
            if (contextLoadedCallback) {
                contextLoadedCallback(null, err);
            }
        });
    }

    _createClass(ResearchContext, [{
        key: 'servicesRemoved',
        value: function servicesRemoved(ids) {
            var _this2 = this;

            return ids.map(function (id) {
                var serviceId = parseInt(id, 10);
                console.log('removing service', serviceId);
                var cnt = _this2.markers.length;
                // COMMENTED IT's UNABLE TO RESHOW MARKER ON CLICK
                // this.markers = this.markers.filter(m => m.options.serviceId === serviceId);
                _this2.points = _this2.points.filter(function (p) {
                    return p.serviceId === serviceId;
                });
                delete _this2.servicePoints[id];

                _this2.group.removeLayers(_this2.group.getLayers().filter(function (marker) {
                    //console.log(marker.options.serviceId, serviceId);
                    return marker.options.serviceId === serviceId;
                }));
                _this2.obtained = _this2.obtained.filter(function (o) {
                    return o !== serviceId;
                });
                _this2.services = _this2.services.filter(function (s) {
                    return s.id !== serviceId;
                });

                return cnt;
            });
        }
    }, {
        key: 'generatePoint',
        value: function generatePoint(point, data) {
            var _this3 = this;

            if (!this.group.getLayers().find(function (m) {
                return m.options.point.properties.id === point.properties.id;
            })) {
                this.points.push(point);
                this.servicePoints[data.service.id].push(point);

                if (point.geometry.type === 'LineString' || point.geometry.type === 'MultiLineString') {
                    var line = this._makeLine(point, data.service);
                    if (!this.markers.find(function (m) {
                        return m.options.point.properties.id === point.properties.id;
                    })) this.markers.push(line);
                    this.group.addLayer(line);
                } else if (point.geometry.type === 'Polygon' || point.geometry.type === 'MultiPolygon') {
                    var polygon = this._makePolygon(point, data.service);
                    if (!this.markers.find(function (m) {
                        return m.options.point.properties.id === point.properties.id;
                    })) this.markers.push(polygon);
                    this.group.addLayer(polygon);
                } else if (point.geometry.type === 'MultiPoint') {
                    point.geometry.coordinates.forEach(function (coord, id) {
                        var marker = _this3._makeMarker(point, data.service, id);
                        if (!_this3.markers.find(function (m) {
                            return m.options.point.properties.id === point.properties.id;
                        })) _this3.markers.push(marker);
                        _this3.group.addLayer(marker);
                    });
                } else if (point.geometry.type === 'Point') {
                    var marker = this._makeMarker(point, data.service);
                    if (!this.markers.find(function (m) {
                        return m.options.point.properties.id === point.properties.id;
                    })) this.markers.push(marker);
                    this.group.addLayer(marker);
                }
            }
        }
    }, {
        key: 'servicesAdded',
        value: function servicesAdded(ids, currentActiveFilters) {
            var _this4 = this;

            // this.markers = [];
            console.log('servicesAdded', ids, currentActiveFilters);
            return ids.map(function (id) {
                var serviceId = parseInt(id, 10);
                _this4.obtained.push(serviceId);

                var serviceRequest = _this4.api.get('/researchcontexts/' + serviceId + '/' + _this4.context.id);
                var serviceMarkers = [];
                _this4.servicePoints[id] = [];

                var calculation = new Promise(function (resolve, reject) {
                    serviceRequest.then(function (res) {
                        var servicePoints = [].concat(_toConsumableArray(res.points));

                        // SAVE RECHERCHE DATA TO BE ABLE TO FILTER
                        // WORK--
                        if (res.display_recherche) {
                            var isRecherche = Object.values(JSON.parse(res.display_recherche));
                            res.data_recherche = res.points.map(function (p) {
                                var point = _extends({}, p, { prop: {} });
                                isRecherche.forEach(function (key) {
                                    return point.prop[key] = p.prop[key];
                                });
                                return point;
                            });
                        }
                        // -----
                        _this4.services.push(_extends({}, res));

                        if (res.display_fields || res.display_selection) {
                            servicePoints = Object(__WEBPACK_IMPORTED_MODULE_3__controllers_services_pei_point_fields_map__["a" /* mapField */])(res.points, JSON.parse(res.display_fields), JSON.parse(res.display_selection));
                        } else {
                            servicePoints = Object(__WEBPACK_IMPORTED_MODULE_3__controllers_services_pei_point_fields_map__["a" /* mapField */])(res.points);
                        }

                        //Use webworker so that the page does not block when calculating which points are included in the limit polygon
                        var pointsWorker = Object(__WEBPACK_IMPORTED_MODULE_1__Utils__["a" /* createWorker */])(_this4.baseUrl + '/js/search-worker.js');
                        try {
                            pointsWorker.postMessage({ action: 'newpoints', data: { points: servicePoints, service: res, limit: _this4.limit.toGeoJSON() } });
                        } catch (e) {
                            reject(e);
                        }
                        pointsWorker.onmessage = function (e) {
                            var _e$data = e.data,
                                action = _e$data.action,
                                data = _e$data.data;

                            if (action === 'parsedpoints') {
                                // TO DO CREATE POLYGONE / LINE
                                // this.group.removeLayers(this.group.getLayers())

                                data.points.features.forEach(function (point) {
                                    _this4.generatePoint(point, data); // {points: {…}, service: {…}}
                                });

                                // Make sure new service is displayed correctly
                                console.log('will update', _this4.buffer, _this4.currentCityLimit, _this4.radius);
                                if (_this4.buffer) {
                                    console.log('doing buffer');
                                    _this4.getPointsInBufferZone();
                                } else if (_this4.currentCityLimit) {
                                    console.log('doing city limit');
                                    _this4.cityChanged(5);
                                } else if (typeof _this4.radius === 'number') {
                                    console.log('doing radius');
                                    _this4.radiusChanged(_this4.radius);
                                }
                                resolve(data.service);
                            }
                        };
                    });
                });
                return { calc: calculation, req: serviceRequest };
            });
        }
    }, {
        key: 'setFacettes',
        value: function setFacettes(facettes) {
            console.log('setFacettes', facettes);
            this.lastFacettes = facettes;
            // RESET MAP AS IF NO FACETTES
            if (this.lastEdit == "radius") {
                this.radiusChanged(this.radius);
            }
            if (this.lastEdit == "buffer") {
                this.getPointsInBufferZone();
            }
            if (this.lastEdit == "city") {
                this.cityChanged();
            }
            if (!this.lastEdit) {
                // SI AUCUN, AFFICHER TOUS LES MARKERS (resetMap ?)
                this.group.removeLayers(this.group.getLayers());
                this.restorePoints();
            }
        }
    }, {
        key: 'groupFilteredByFacette',
        value: function groupFilteredByFacette() {
            var _this5 = this;

            console.log('groupFilteredByFacette', this.lastEdit, this.lastFacettes);

            // REMOVE UNSELECTED SERVICES POINTS
            this.services.forEach(function (s) {
                if (!$('#service-selected-' + s.id).hasClass("selected")) {
                    console.log('this.group.getLayers().filter(p => p.options.point.properties.serviceId == s.id)', _this5.group.getLayers().filter(function (p) {
                        return p.options.point.properties.serviceId == s.id;
                    }));
                    _this5.group.removeLayers(_this5.group.getLayers().filter(function (p) {
                        return p.options.point.properties.serviceId == s.id;
                    }));
                }
            });
            this.group.eachLayer(function (p) {
                if (!_this5.services.find(function (s) {
                    return s.id == p.options.point.properties.serviceId;
                })) {
                    _this5.group.removeLayer(p);
                }
            });
        }
    }, {
        key: 'serviceFiltered',
        value: function serviceFiltered(currentFilters) {
            var _this6 = this;

            var currentLayers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
            var endSearchData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            // currentFilters = [{field : fiche_acces_pmr, name : ACCES PMR, values : []}]

            console.log('serviceFiltered', currentFilters, currentLayers);
            console.log('DATA', this.start, this.radius, this.end, this.homeMarker, this.startMarker, this.endMarker, this.buffer, this.route, this.context, this.currentCityLimit, this.startcityLimit, this.endcityLimit, this.filters);
            // CLEAN LAYERS > RESTORE ID TO SHOW PER SERVICE
            this.group.removeLayers(this.group.getLayers());

            if (currentLayers) {
                // NEED TO RESET LAYERS TO GET CITY MARKERS && THEN FILTER ON IT TO NOT SHOW THE WHOLE MAP
                if (this.startcityLimit) {
                    this.cityChanged();
                } else if (this.end) {
                    this.getPointsInBufferZone();
                }
                // else if (this.startcityLimit) {
                //     this.cityChanged()
                // } 
                else if (this.radius && this.start) {
                        this.radiusChanged(this.radius);
                    }
                /* 
                1. GET CURRENT LAYERS IN GROUPLAYERS (SHOWN)
                2. PER SERVICE, GET LAYERS TO CHECK
                3. VERIFY IF LAYERS RESPECT FILTERS
                4. CLEAN AND RESTORE LAYERS
                 */
                var currentLayersId = this.group.getLayers().map(function (p) {
                    return p.options.point.properties.id;
                });
                var idsToShow = [];
                this.services.forEach(function (currentService) {

                    if (currentService && $('#service-selected-' + currentService.id).hasClass("selected")) {

                        if (Array.isArray(currentService.data_recherche)) {

                            var layersToFilter = currentService.data_recherche.filter(function (marker) {
                                return currentLayersId.includes(marker.id);
                            });

                            var idsToShowInService = layersToFilter.filter(function (point) {
                                // let state = []
                                var state = {};

                                currentFilters.forEach(function (filter) {
                                    var display_recherche = JSON.parse(currentService.display_recherche);

                                    if (!state[filter.field]) {
                                        state[filter.field] = [];
                                    }
                                    if (display_recherche[filter.name] == filter.field) {
                                        if (point.prop[filter.field]) {
                                            filter.values.forEach(function (v) {
                                                if (point.prop[filter.field].includes(v)) {
                                                    // state.push(true)
                                                    state[filter.field].push(true);
                                                } else {
                                                    state[filter.field].push(false);
                                                    // state.push(false)
                                                }
                                            });
                                        } else {
                                            state[filter.field].push(false);
                                            // state.push(false)
                                        }
                                    }
                                });
                                var showPoint = true;
                                Object.values(state).forEach(function (arr) {
                                    if (arr.length && arr.includes(false)) {
                                        // ET
                                        // if (arr.length && !arr.includes(true)) { // OU
                                        showPoint = false;
                                    }
                                });
                                return showPoint;
                            });
                            idsToShow = idsToShow.concat(idsToShowInService);
                        }
                    }
                });
                idsToShow = idsToShow.map(function (point) {
                    return point.id;
                });
                this.group.removeLayers(this.group.getLayers());
                this.restorePoints(idsToShow);
            } else {
                this.services.forEach(function (currentService) {
                    if (currentService && $('#service-selected-' + currentService.id).hasClass("selected")) {
                        if (Array.isArray(currentService.data_recherche)) {

                            var _idsToShow = currentService.data_recherche.filter(function (point) {
                                var state = {};
                                // let state = []
                                currentFilters.forEach(function (filter) {
                                    // {field : fiche_acces_pmr, name : ACCES PMR, values : []}

                                    var display_recherche = JSON.parse(currentService.display_recherche);

                                    // {DISPOSITIF: 'fiche_dispositif_type', name: 'fiche_dispositif_type', ACCES PMR: 'fiche_acces_pmr', Assistance Type: 'fiche_assistance_type', Paiement: 'fiche_assistance_paiement', …}
                                    /*  point : {
                                        "fiche_dispositif_type": "Accompagnement",
                                        "fiche_acces_pmr": "Oui",
                                        "fiche_assistance_type": "Toutes démarches",
                                        "fiche_assistance_paiement": "",
                                        "fiche_mediation_autre_paiement": ""
                                    } */
                                    if (!state[filter.field]) {
                                        state[filter.field] = [];
                                    }
                                    if (display_recherche[filter.name] == filter.field) {
                                        if (point.prop[filter.field]) {
                                            filter.values.forEach(function (v) {
                                                if (point.prop[filter.field].includes(v)) {
                                                    // state.push(true)
                                                    state[filter.field].push(true);
                                                } else {
                                                    state[filter.field].push(false);
                                                    // state.push(false)
                                                }
                                            });
                                        } else {
                                            state[filter.field].push(false);
                                            // state.push(false)
                                        }
                                    }
                                });
                                var showPoint = true;
                                Object.values(state).forEach(function (arr) {
                                    if (arr.length && arr.includes(false)) {
                                        // if (arr.length && !arr.includes(true)) { // OU
                                        showPoint = false;
                                    }
                                });
                                return showPoint;
                                // return state.length ? state.includes(true) : true // Renvoie les points qui n'ont aucun filtre correspondant + qui ont au moins un filtre correspondant qui ets vrai
                                // return state.length ? !state.includes(false) : true // Renvoie les points qui n'ont aucun filtre correspondant + qui ont au moins un filtre correspondant qui ets vrai
                            }).map(function (point) {
                                return point.id;
                            });
                            _this6.restorePoints(_idsToShow);
                        }
                    }
                });
            }
            if (!currentFilters.length) {
                // RESTORE POINTS
                // currentLayers ? this.end ? this.getPointsInBufferZone() : this.cityChanged() : this.restorePoints();
                if (currentLayers) {
                    if (this.end) {
                        // console.log('this.endSearchData', this.endSearchData, this.end)
                        this.getPointsInBufferZone();
                    } else if (this.startcityLimit) {
                        this.cityChanged();
                    } else if (this.radius && this.start) {
                        this.radiusChanged(this.radius);
                    }
                } else {
                    this.restorePoints();
                }

                // THEN HIDE DISABLED SERVICE
                var currentServicesId = this.services.map(function (s) {
                    return s.id;
                });
                this.group.removeLayers(this.group.getLayers().filter(function (marker) {
                    return !currentServicesId.includes(marker.options.serviceId);
                }));
            }
            this.map.fitBounds(this.group.getBounds());
        }
    }, {
        key: 'removeEndMarker',
        value: function removeEndMarker() {
            if (this.endcityLimit) {
                this.map.removeLayer(this.endcityLimit);
                this.endcityLimit = null;
            }
            this.end = null;
            if (this.endMarker) {
                this.map.removeLayer(this.endMarker);
            }

            this.endMarker = null;

            if (this.buffer) {
                this.map.removeLayer(this.buffer);
                this.buffer = null;
            }
        }
    }, {
        key: 'radiusChanged',
        value: function radiusChanged(newRadius) {
            var _this7 = this;

            console.log('radiusChanged', newRadius);
            this.radius = newRadius;
            if (this.buffer !== null) {
                this.map.removeLayer(this.buffer);
                this.buffer = null;
            }
            if (this.currentCityLimit) {
                this.map.removeLayer(this.currentCityLimit);
                this.currentCityLimit = null;
            }
            if (this.startcityLimit) {
                this.map.removeLayer(this.startcityLimit);
                this.startcityLimit = null;
            }
            if (this.endcityLimit) {
                this.map.removeLayer(this.endcityLimit);
                this.endcityLimit = null;
            }
            if (this.start && typeof this.radius === 'number') {
                this.group.removeLayers(this.group.getLayers());

                //Remove points that are not in the radius
                if (this.currentDiameter) {
                    this.map.removeLayer(this.currentDiameter);
                    this.currentDiameter = null;
                }

                var polygon = turf.polygon([[[-180, -80], [180, -80], [180, 80], [-180, 80], [-180, -80]]], { name: 'all' });

                var options = { steps: 120, units: 'meters' };
                var circle = turf.circle([this.start.lng, this.start.lat], this.radius, options);

                var diff = turf.difference(polygon, circle);

                this.currentDiameter = L.geoJSON(diff, { fillOpacity: 0.15, opacity: 0.3, fillColor: "#000", color: "#000" });

                this.map.addLayer(this.currentDiameter);
                this.map.fitBounds(L.geoJSON(circle).getBounds());
                console.log('this.group radius', this.group.getLayers());

                this.markers.forEach(function (marker) {
                    if (_this7.start.distanceTo(marker.getLatLng()) > _this7.radius) {
                        _this7.group.removeLayer(marker);
                        //Remove from servicePoints
                    } else if (!_this7.group.getLayers().find(function (l) {
                        return l.options.point.properties.id === marker.options.point.properties.id;
                    })) {
                        if ($('#service-selected-' + marker.options.serviceId).hasClass("selected")) {
                            _this7.group.addLayer(marker);
                        }
                    }
                });
                console.log('this.group radius', this.group.getLayers());

                this.lastEdit = "radius";
                this.groupFilteredByFacette();
            }
        }
    }, {
        key: 'cityChanged',
        value: function cityChanged() {
            var _this8 = this;

            console.log('cityChanged-----------------', this.startcityLimit);
            this.radius = null;
            if (this.buffer !== null) {
                this.map.removeLayer(this.buffer);
                this.buffer = null;
            }
            if (this.currentDiameter) {
                this.map.removeLayer(this.currentDiameter);
                this.currentDiameter = null;
            }
            if (this.currentCityLimit) {
                this.map.removeLayer(this.currentCityLimit);
                this.currentCityLimit = null;
            }
            if (this.startcityLimit) {
                var polygon = turf.polygon([[[-180, -80], [180, -80], [180, 80], [-180, 80], [-180, -80]]], { name: 'all' });

                try {
                    jQuery('#legend-title').text("Résultats");
                    var geoLimit = this.startcityLimit.toGeoJSON().features[0].geometry;

                    //Display city outline by making the whole world except the city grey
                    var diff = turf.difference(polygon, geoLimit);

                    this.group = this.group;
                    this.group.removeLayers(this.group.getLayers());

                    this.currentCityLimit = L.geoJSON(diff, { fillOpacity: 0.15, opacity: 0.3, fillColor: "#000", color: "#000" });

                    this.map.addLayer(this.currentCityLimit);

                    console.log('total markers', this.markers.length);
                    console.log('total group layers', this.group.getLayers().length);
                    var cnt = 0;

                    var onMap = this.group.getLayers().map(function (m) {
                        return m.options.point.properties.id;
                    });

                    this.markers.forEach(function (marker) {
                        var isInPolygon = turf.pointsWithinPolygon(marker.toGeoJSON(), geoLimit);

                        var exists = onMap.indexOf(marker.options.point.properties.id) >= 0;

                        if (isInPolygon.features.length === 0 && exists) {
                            _this8.group.removeLayer(marker);
                            cnt++;
                        } else if (!exists && isInPolygon.features.length > 0) {
                            _this8.group.addLayer(marker);
                            // console.log('marker', marker);
                            cnt++;
                        }
                    });
                    console.log('count done', cnt);
                } catch (e) {
                    jQuery('#legend-title').text("Légende");
                    console.log(e);
                }
            }
        }
    }, {
        key: 'hidePoints',
        value: function hidePoints(ids) {
            var _this9 = this;

            this.map.closePopup();
            var grp = new L.FeatureGroup();
            this.markers.forEach(function (marker) {
                var toRemove = ids.indexOf(marker.options.point.properties.id) === -1;

                if (toRemove) {
                    _this9.group.removeLayer(marker);
                } else if (!_this9.group.hasLayer(marker)) {
                    _this9.group.addLayer(marker);
                    marker.addTo(grp);
                } else {
                    marker.addTo(grp);
                }
            });
        }
    }, {
        key: 'restorePoints',
        value: function restorePoints() {
            var _this10 = this;

            var ids = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            this.markers.forEach(function (marker) {
                // if an array of ids is passed, only restore those markers
                // Otherwise restore all points
                var restore = true;
                if (ids) {
                    restore = ids.find(function (id) {
                        return id == marker.options.point.properties.id;
                    });
                }
                if (!_this10.group.hasLayer(marker) && restore && $('#service-selected-' + marker.options.serviceId).hasClass("selected")) {
                    _this10.group.addLayer(marker);
                }
            });
            this.groupFilteredByFacette();
        }
    }, {
        key: 'addressAutocomplete',
        value: function addressAutocomplete(partialAddress) {
            var point = turf.centroid(this.limit.toGeoJSON());
            console.log('centroid is', point);
            var coords = {
                lat: point.geometry.coordinates[1],
                lon: point.geometry.coordinates[0]
            };
            console.log('centroid coords', coords);
            return Object(__WEBPACK_IMPORTED_MODULE_2__Gp__["a" /* autoComplete */])(partialAddress, this.apiKey, coords);
        }
    }, {
        key: 'bufferChanged',
        value: function bufferChanged(newBuffer) {
            console.log('bufferChanged-----');
            if (this.currentDiameter) {
                this.map.removeLayer(this.currentDiameter);
                this.currentDiameter = null;
            }
            if (this.currentCityLimit) {
                this.map.removeLayer(this.currentCityLimit);
                this.currentCityLimit = null;
            }
            if (this.startcityLimit) {
                this.map.removeLayer(this.startcityLimit);
                this.startcityLimit = null;
            }
            if (this.endcityLimit) {
                this.map.removeLayer(this.endcityLimit);
                this.endcityLimit = null;
            }
            this.bufferRadius = newBuffer;
            this.getPointsInBufferZone();
            if (this.filters && this.filters.length) {
                console.log('this.filters', this.filters);
                this.serviceFiltered(this.filters, true); // true to reset markers or it duplicates
            }
        }
    }, {
        key: 'startOrEndAddress',
        value: function startOrEndAddress(data) {
            var _this11 = this;

            var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'start';

            // WORK --
            console.log('startOrEndAddress', data, start);
            if (this.currentDiameter) {
                this.map.removeLayer(this.currentDiameter);
                this.currentDiameter = null;
            }
            jQuery('#legend-title').text("Résultats");
            return new Promise(function (resolve, reject) {
                if (typeof _this11.homeMarker !== 'undefined' && _this11.homeMarker !== null) {
                    _this11.map.removeLayer(_this11.homeMarker);
                }

                if (typeof _this11.route !== 'undefined' && _this11.route !== null) {
                    _this11.map.removeLayer(_this11.route);
                }
                if (data && data.length > 0) {
                    var newAddress = data[0].loc.properties.label;
                    console.log('newAddress', newAddress);
                    if (data[0].loc.properties.type === 'municipality') {
                        // This is a city, get commune bounds
                        var insee = data[0].loc.properties.id;
                        _this11.api.get('/insee/1/' + insee).then(function (resp) {
                            console.log(resp);
                            try {

                                if (_this11[start + 'cityLimit']) {
                                    _this11.map.removeLayer(_this11[start + 'cityLimit']);
                                }
                                _this11[start + 'cityLimit'] = L.geoJson(resp, { style: { fillOpacity: 0, color: 'black', weight: 1 } });
                                _this11[start + 'cityLimit'].addTo(_this11.map);

                                if (start === 'start') {
                                    _this11.map.fitBounds(_this11[start + 'cityLimit'].getBounds());
                                }

                                if (_this11.end === null || _this11.start === null) {
                                    resolve(data[0].loc);
                                }
                            } catch (e) {
                                console.log(e);
                            }
                        }).catch(function (er) {
                            console.log(er);
                        });
                    }

                    if (typeof _this11[start + 'Marker'] !== 'undefined' && _this11[start + 'Marker'] !== null) {
                        _this11.map.removeLayer(_this11[start + 'Marker']);
                    }

                    _this11[start] = L.latLng(data[0].loc.geometry.coordinates[1], data[0].loc.geometry.coordinates[0]);

                    //this.map.panTo(this[start]);
                    _this11[start + 'Marker'] = L.marker(_this11[start], { title: newAddress, icon: L.AwesomeMarkers.icon({ icon: start === 'start' ? "" : "", prefix: 'fa', markerColor: start === 'start' ? "gray" : "building", iconColor: 'white' }) });
                    console.log("this[start + 'Marker']", _this11[start + 'Marker']);
                    _this11[start + 'Marker'].bindPopup(newAddress);
                    _this11.map.addLayer(_this11[start + 'Marker']);

                    if (_this11.end !== null && _this11.start !== null) {
                        // We have both points, draw the itinerary, the buffer and find points
                        _this11.calculateRoute(_this11.start, _this11.end).then(function (result) {
                            _this11.getPointsInBufferZone();
                            resolve(data[0].loc);
                        }).catch(function (e) {
                            return console.log(e) && reject(e);
                        });
                    } else if (_this11.end === null && data[0].loc.properties.type !== 'municipality') {
                        resolve(data[0].loc);
                    }

                    if (_this11.endMarker === null) {
                        console.log('this.context', _this11.context);
                        _this11.markers.forEach(function (m) {
                            if (m._popup._content.indexOf('itinerary') === -1) {
                                //m._popup.setContent(m._popup._content + '<button data-point-id="' + m.options.point.properties.id + '" class="btn btn-primary itinerary"><i class="fa fa-road"></i> Itinéraire</button>')
                                var title = '';
                                if (m.options.point.properties && m.options.point.properties.name) {
                                    title = m.options.point.properties.name;
                                }

                                var service = _this11.services.find(function (s) {
                                    return s.id === m.options.serviceId;
                                }) ? _this11.services.find(function (s) {
                                    return s.id === m.options.serviceId;
                                }) : _this11.context.services.find(function (s) {
                                    return s.id === m.options.serviceId;
                                });
                                console.log('ervice123', service);

                                // CUSTOM SVG ICON OR FONT AWESOME
                                var iconContent = void 0;
                                if (service) {
                                    if (service.iconColor && service.iconColor.icon_svg) {
                                        iconContent = '<img class="svg-tooltip" src="' + _this11.baseUrl + '/storage/' + service.iconColor.icon_path + '" alt="' + service.iconColor.icon + '" style="color: ' + Object(__WEBPACK_IMPORTED_MODULE_1__Utils__["e" /* pickColor */])(service.iconColor.color) + ' !important;max-width: 18px !important;z-index:2"></i>';
                                    } else {
                                        iconContent = '<i class="' + service.iconColor.icon + '" style="color: ' + Object(__WEBPACK_IMPORTED_MODULE_1__Utils__["e" /* pickColor */])(service.iconColor.color) + ' !important"></i>';
                                    }
                                    m._popup.setContent(_this11._popupContents(m.options.point, title, service, iconContent));
                                }
                            }
                        });
                    }
                }
            });
        }
    }, {
        key: 'getPointsInBufferZone',
        value: function getPointsInBufferZone() {
            var _this12 = this;

            if (this.route === null) {
                return;
            }
            if (this.buffer !== null) {
                this.map.removeLayer(this.buffer);
                this.buffer = null;
            }
            if (this.currentCityLimit !== null) {
                this.map.removeLayer(this.currentCityLimit);
                this.currentCityLimit = null;
            }
            this.group.removeLayers(this.group.getLayers());

            var points = turf.points(this.markers.map(function (marker) {
                return marker.toGeoJSON().geometry.coordinates;
            }));

            var buffer = turf.buffer(this.route.toGeoJSON(), this.bufferRadius);
            var within = turf.pointsWithinPolygon(points, buffer);

            var polygon = turf.polygon([[[-180, -80], [180, -80], [180, 80], [-180, 80], [-180, -80]]], { name: 'all' });
            var diff = turf.difference(polygon, buffer.features[0]);

            var g = L.geoJSON(polygon);
            g.setStyle({ fillColor: "#f00", color: "#f00", fillOpacity: 0.15, opacity: 0.3 });

            this.buffer = L.geoJson(diff);
            this.map.fitBounds(L.geoJSON(buffer).getBounds());
            this.buffer.setStyle({ fillColor: "#000", color: "#000", fillOpacity: 0.15, opacity: 0.3 });
            this.buffer.addTo(this.map);

            if (!this.group || this.group.length === 0) {
                return;
            }

            var onMap = this.group.getLayers().map(function (m) {
                return m.options.point.properties.id;
            });

            var cnt = 0;
            this.markers.forEach(function (marker) {
                // console.log('marker', marker)
                var markerCoords = marker.toGeoJSON().geometry.coordinates;
                var found = within.features.find(function (feature) {
                    return feature.geometry.coordinates[0] === markerCoords[0] && feature.geometry.coordinates[1] === markerCoords[1];
                });

                if (typeof found !== 'undefined' && found !== null && onMap.indexOf(marker.options.point.properties.id) < 0 && $('#service-selected-' + marker.options.point.properties.serviceId).hasClass("selected")) {
                    // this point is in the buffer, but not yet on the map, add it
                    _this12.group.addLayer(marker);
                    cnt++;
                } else if ((found === null || typeof found === 'undefined') && onMap.indexOf(marker.options.point.properties.id) >= 0) {
                    // This point
                    _this12.group.removeLayer(marker);
                    cnt++;
                }
            });

            console.log('cnt', cnt);

            this.lastEdit = "buffer";
            this.groupFilteredByFacette();
        }
    }, {
        key: 'calculateRoute',
        value: function calculateRoute(from, to) {
            var _this13 = this;

            return new Promise(function (resolve, reject) {
                Object(__WEBPACK_IMPORTED_MODULE_2__Gp__["c" /* routing */])({
                    x: from.lng,
                    y: from.lat
                }, {
                    x: to.lng,
                    y: to.lat
                }, _this13.apiKey).then(function (result) {

                    if (typeof _this13.route !== 'undefined' && _this13.route !== null) {
                        _this13.map.removeLayer(_this13.route);
                    }

                    _this13.route = L.geoJson(result.routeGeometry, { weight: 6 });
                    _this13.map.fitBounds(_this13.route.getBounds());

                    _this13.route.addTo(_this13.map);
                    resolve(result);
                }).catch(function (e) {
                    return reject(e);
                });
            });
        }
    }, {
        key: 'routeTo',
        value: function routeTo(destPointId) {
            var _this14 = this;

            var marker = this.markers.find(function (p) {
                return p.options.point.properties.id === destPointId;
            });

            var dest = new (Function.prototype.bind.apply(L.LatLng, [null].concat(_toConsumableArray(marker.options.point.geometry.coordinates))))();

            this.calculateRoute(this.start, dest).then(function (result) {
                var content = jQuery(marker._popup._content).wrap('<div>').parent();

                content.find('p.button').remove();
                content.find('p.button-select').remove();
                content.append('<p>Distance : ' + result.totalDistance + '</p><p>Temps de parcours : ' + Math.round(result.totalTime / 60) + ' min</p>');
                content.append('<p class="button-select"><button data-point-id="' + marker.options.point.properties.id + '" class="btn btn-info select-choose"><i class="fa fa-plus-circle"></i> Ajouter à ma sélection</button></p>');
                marker.options.route = _this14.route;

                marker._popup.setContent(content.html());
            });
        }

        /**
         * Reset map
         */

    }, {
        key: 'resetMap',
        value: function resetMap() {
            jQuery('#legend-title').text("Légende");
            console.log('resetting map');
            //window.location.reload()
            //je réaffiche tous les points
            this.restorePoints();
            //je réinitialise le rond et le buffer
            if (typeof this.currentDiameter !== 'undefined' && this.currentDiameter !== null) {
                this.map.removeLayer(this.currentDiameter);
                this.currentDiameter = null;
            }
            if (typeof this.buffer !== 'undefined' && this.buffer !== null) {
                this.map.removeLayer(this.buffer);
                this.buffer = null;
            }
            if (typeof this.homeMarker !== 'undefined' && this.homeMarker !== null) {
                this.map.removeLayer(this.homeMarker);
                this.homeMarker = null;
            }
            if (typeof this.startMarker !== 'undefined' && this.startMarker !== null) {
                this.map.removeLayer(this.startMarker);
                this.startMarker = null;
            }
            if (typeof this.endMarker !== 'undefined' && this.endMarker !== null) {
                this.map.removeLayer(this.endMarker);
                this.endMarker = null;
            }

            if (typeof this.route !== 'undefined' && this.route !== null) {
                this.map.removeLayer(this.route);
                this.route = null;
            }

            if (typeof this.startcityLimit !== 'undefined' && this.startcityLimit !== null) {
                this.map.removeLayer(this.startcityLimit);
                this.startcityLimit = null;
            }

            if (typeof this.endcityLimit !== 'undefined' && this.endcityLimit !== null) {
                this.map.removeLayer(this.endcityLimit);
                this.endcityLimit = null;
            }

            if (typeof this.currentCityLimit !== 'undefined' && this.currentCityLimit !== null) {
                this.map.removeLayer(this.currentCityLimit);
                this.currentCityLimit = null;
            }

            //Fit to limit bounds on reset
            if (typeof this.limit !== 'undefined' && this.limit !== null) {
                this.map.fitBounds(this.limit.getBounds());
            }

            this.lastEdit = null;
            this.lastFacettes = null;
        }

        /**
         * Generate the popup contents for this marker
         * @param point
         * @param title
         * @param service
         * @returns {string}
         * @private
         */

    }, {
        key: '_popupContents',
        value: function _popupContents(point, title, service, iconContent) {
            var newTitle = "";
            if (service && service.iconColor) {
                newTitle = point.geometry.type === 'Point' || point.geometry.type === 'MultiPoint' ? '<p class="title" style="margin-top: 5px;"><span class="icon" style="background-color:' + service.iconColor.color + ' !important"> ' + iconContent + '</span> <span class="text" style="color:' + service.iconColor.color + ' !important">' + title + '</span></p>' : '<p class="title"><span class="text" style="color:' + service.outline + '">' + title + '</span></p>';
            }

            var contents = '';
            Object.keys(point.properties).map(function (key) {
                if (key !== 'name' && key !== 'id' && key !== 'serviceId' && point.properties[key] && point.properties[key] !== '') {
                    contents += '<p id="popup-custom-content">' + key.toUpperCase() + '&nbsp;: <strong>' + point.properties[key] + '</strong></p>';
                }
            });
            var buttons = '';
            if (this.context.display_selection) {
                // buttons = '<button data-point-id="' + point.properties.id + '" class="btn btn-info select-choose"><i class="fa fa-plus-circle"></i> Ajouter à ma sélection</button>';
                buttons = '<button data-point-id="' + point.properties.id + '" class="btn btn-info select-choose"><i class="fa fa-plus-circle"></i> SÉLECTION</button>';
            }

            var h = 36;

            if (this.start !== null) {
                buttons += '<button data-point-id="' + point.properties.id + '" class="btn btn-primary itinerary" style="margin-left: 0.5em;"><i class="fa fa-road"></i> Itinéraire</button>';
            }

            return newTitle + '<div class="scrollable-popup-content" style="height:' + (240 - h) + 'px">' + contents + '</div>' + buttons;
        }

        /**
         * Make a cluster group from a list of markers
         *
         * @param serviceMarkers
         * @private
         */

    }, {
        key: '_makeGroup',
        value: function _makeGroup(serviceMarkers) {
            var _this15 = this;

            var group = L.markerClusterGroup({
                iconCreateFunction: function iconCreateFunction(cluster) {
                    return L.divIcon({ html: '<div class="group-marker-icon" style="background-color:#ccc !important;color:#000 !important"><span>' + cluster.getChildCount() + '</span></div>', className: 'custom-cluster', iconSize: L.point(40, 40) });
                }
            });

            group.addLayers(serviceMarkers.filter(function (marker) {
                return !(_this15.start !== null && _this15.radius !== null && marker.getLatLng().distanceTo(_this15.start) > _this15.radius);
            }));

            return group;
        }

        /**
         * Generate a marker or this point and this service
         * @param point
         * @param service
         * @param id
         * @return Marker
         */

    }, {
        key: '_makeMarker',
        value: function _makeMarker(point, service, id) {
            var _this16 = this;

            var marker = null;

            var title = "";
            if (point.properties && point.properties.name) {
                title = point.properties.name;
            }
            var iconContent = service.iconColor.icon_svg ? '<img class="svg-tooltip" src="' + this.baseUrl + '/storage/' + service.iconColor.icon_path + '" alt="' + service.iconColor.icon + '" style="color: ' + Object(__WEBPACK_IMPORTED_MODULE_1__Utils__["e" /* pickColor */])(service.iconColor.color) + '!important;max-width: 18px!important;z-index:2"></i>' : '<i class="' + service.iconColor.icon + '" style="color: ' + Object(__WEBPACK_IMPORTED_MODULE_1__Utils__["e" /* pickColor */])(service.iconColor.color) + ' !important"></i>';
            var text = this._popupContents(point, title, service, iconContent);

            if (point.geometry.type === 'Point') {
                // Coordinates are reversed
                var coords = new (Function.prototype.bind.apply(L.LatLng, [null].concat(_toConsumableArray(point.geometry.coordinates.reverse()))))();

                // CUSTOM SVG ICON OR FONT AWESOME
                marker = L.marker(coords, {
                    title: title,
                    icon: L.divIcon({
                        html: '<div style="border-color:' + service.iconColor.color + ' !important"><div>' + iconContent + '</div></div>',
                        iconSize: [20, 20],
                        className: 'service-point-icon',
                        iconAnchor: [17, 54],
                        popupAnchor: [0, -50]
                    }),
                    point: point,
                    serviceId: service.id
                });
            }

            if (point.geometry.type === 'MultiPoint') {

                var _coords = new (Function.prototype.bind.apply(L.LatLng, [null].concat(_toConsumableArray(point.geometry.coordinates[id].reverse()))))();
                marker = L.marker(_coords, {
                    title: title,
                    icon: L.divIcon({
                        html: '<div style="border-color:' + service.iconColor.color + ' !important"><div>' + iconContent + '</div></div>',
                        iconSize: [20, 20],
                        className: 'service-point-icon',
                        iconAnchor: [17, 54],
                        popupAnchor: [0, -50]
                    }),
                    point: point,
                    serviceId: service.id
                });
            }

            marker.bindPopup(text);
            marker.on('click', function (e) {
                if (typeof e.target.options.route !== 'undefined') {
                    //We already have an itinerary for this marker, display it
                    if (_this16.route !== null) {
                        _this16.map.removeLayer(_this16.route);
                        _this16.route = e.target.options.route;
                        _this16.map.addLayer(_this16.route);
                    }
                }
            });

            return marker;
        }
        /**
         * Generate a polygon or this point and this service
         * @param polygon
         * @param service
         * @return Polygon
         */

    }, {
        key: '_makePolygon',
        value: function _makePolygon(polygon, service) {
            var title = "";
            if (polygon.properties && polygon.properties.name) {
                title = polygon.properties.name;
            }

            // Coordinates are reversed
            polygon.geometry.type === 'Polygon' && polygon.geometry.coordinates.map(function (layer) {
                return layer.map(function (arr) {
                    arr.reverse();
                });
            });
            polygon.geometry.type === 'MultiPolygon' && polygon.geometry.coordinates.map(function (multi) {
                return multi.map(function (layer) {
                    return layer.map(function (arr) {
                        arr.reverse();
                    });
                });
            });

            // CREATE POLYGON
            var polygone = L.polygon(polygon.geometry.coordinates, {
                title: polygon.properties.name,
                color: service.outline,
                fillColor: service.fill,
                point: polygon,
                serviceId: service.id
            });

            var text = this._popupContents(polygon, title, service);
            polygone.bindPopup(text);

            return polygone;
        }
        /**
         * Generate a line or this point and this service
         * @param line
         * @param service
         * @return Line
         */

    }, {
        key: '_makeLine',
        value: function _makeLine(line, service) {

            var title = "";
            if (line.properties && line.properties.name) {
                title = line.properties.name;
            }

            // Coordinates are reversed
            line.geometry.type === 'LineString' && line.geometry.coordinates.map(function (arr) {
                arr.reverse();
            });
            line.geometry.type === 'MultiLineString' && line.geometry.coordinates.map(function (multi) {
                return multi.map(function (arr) {
                    arr.reverse();
                });
            });

            // CREATE POLYGON
            var polyline = L.polyline(line.geometry.coordinates, {
                title: line.properties.name,
                color: service.outline,
                point: line,
                serviceId: service.id
            });
            var text = this._popupContents(line, title, service);
            polyline.bindPopup(text);

            return polyline;
        }
    }]);

    return ResearchContext;
}();

/* harmony default export */ __webpack_exports__["a"] = (ResearchContext);

/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getConfig; });
/* unused harmony export geoCode */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return autoComplete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return routing; });

var getConfig = function getConfig(apiKey) {
    return new Promise(function (resolve, reject) {
        Gp.Services.getConfig({
            apiKey: apiKey,
            onSuccess: function onSuccess() {
                return resolve();
            },
            onFailure: function onFailure(err) {
                return reject(err);
            }
        });
    });
};

var geoCode = function geoCode(address, apiKey) {
    return new Promise(function (resolve, reject) {
        Gp.Services.geocode({
            location: address,
            apiKey: apiKey,
            onSuccess: function onSuccess(result) {
                resolve(result);
            },
            onFailure: function onFailure(error) {
                reject(error);
            }
        });
    });
};

var autoComplete = function autoComplete(address, apiKey, point) {
    return new Promise(function (resolve, reject) {
        console.log('doing autocomplete promise', address, apiKey, point);
        jQuery.get('https://api-adresse.data.gouv.fr/search/?q=' + address + '&lat=' + point.lat + '&lon=' + point.lon).done(function (res) {
            console.log('autocomplete request success', res);
            resolve(res);
        }).fail(function (err) {
            console.log('autocomplete request fail', err);
            reject(err);
        });
    });
};

var routing = function routing(start, end, apiKey) {
    return new Promise(function (resolve, reject) {
        Gp.Services.route({
            startPoint: start,
            endPoint: end,
            apiKey: apiKey,
            onSuccess: function onSuccess(result) {
                resolve(result);
            },
            onFailure: function onFailure(error) {
                reject(error);
            }
        });
    });
};

/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(74);


/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__templates_search__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ResearchContext__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contextEvents__ = __webpack_require__(8);






/*
Usage : <script src="/js/embed.js" data-context-id="CONTEXT_ID", data-api-key="IGN_API_KEY"></script>

Where CONTEXT_ID is the id of the context to display
 */

var thisJs = document.querySelector("script[src*='embed-search.js']");

var url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

var baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
/**
 *
 * @param url
 * @returns {Promise}
 */
var loadScript = function loadScript(url) {
    return new Promise(function (resolve, reject) {
        var script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

var loadjQuery = function loadjQuery() {
    // if (window.jQuery) {
    //     return Promise.resolve();
    // } else {
    return new Promise(function (resolve) {
        //         setTimeout(() => {
        //             if (window.jQuery) {
        //                 console.log('jquery loaded')
        //                 resolve()
        //             } else {
        //                 console.log('jquery loaded from elsewhere')
        loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(function () {
            resolve();
            window.$ = jQuery;
        });
        //
        //             }
        //         }, 2000);
    });
    // }
};

var contextDidLoad = function contextDidLoad(ctx, err) {
    console.log('--- contextDidLoad ---', researchContext);
    Object(__WEBPACK_IMPORTED_MODULE_2__contextEvents__["a" /* researchEventHandlers */])(window.jQuery, researchContext);
    if (err) {
        jQuery('#search-context-main').html('<div class="col-md-12"><div class="alert alert-danger">\n' + '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le contexte.</h4>\n' + '    Ce site web n\'est sans doute pas autorisé à intégrer ce contexte.\n' + '    </div></div>');
        return console.log(err);
    }
    var b = jQuery('body');
    if (ctx.active === false) {
        jQuery('#disabled-message').show();
    }

    var servDD = b.find('select#service');
    servDD.append(ctx.services.map(function (service, i) {
        return "<option selected value=\"" + service.id + "\" title=\"" + service.info + "\">" + service.name + "</option>";
    }));

    b.find('.select2').select2();
    servDD.trigger('change');
    var event = new Event('proxiclicMap.loaded');
    window.dispatchEvent(event);
};

var researchContext = void 0;

(function () {
    var contextId = thisJs.getAttribute('data-context-id');

    var jquery = loadjQuery();
    var leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');
    // Is it worth it to load select2 here ?

    Promise.all([jquery, leaflet]).then(function () {
        var awesomeMarkers = loadScript(baseUrl + '/vendor/leaflet.awesome-markers/dist/leaflet.awesome-markers.js');
        var bootstrap = loadScript(baseUrl + '/vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js');
        var turf = loadScript('https://npmcdn.com/@turf/turf/turf.min.js');
        var markerClusters = loadScript('https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js');
        var gpPlugin = loadScript(baseUrl + '/vendor/geoportal/GpPluginLeaflet.js');
        var select2 = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js');

        Promise.all([leaflet, select2, awesomeMarkers, markerClusters, gpPlugin, turf, bootstrap]).then(function () {
            var fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            var sel2lang = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js');

            Promise.all([fullscreen, sel2lang]).then(function () {
                //everything loaded and ready to go
                //if (typeof $ === 'undefined') {
                window.$ = window.jQuery;
                //}
                jQuery(document).ready(function () {

                    jQuery(thisJs).after(jQuery(__WEBPACK_IMPORTED_MODULE_0__templates_search__["a" /* default */]));
                    researchContext = new __WEBPACK_IMPORTED_MODULE_1__ResearchContext__["a" /* default */](contextId, contextDidLoad, baseUrl);

                    // HIDE EMBED SCRIPT ON BACK OFFICE PAGE
                    if (jQuery('#search-context-main.bo-view').length && jQuery('#search-context-main.cartosdaasap-embed').length) {
                        // jQuery('#search-context-main.cartosdaasap-embed').remove();
                        jQuery("#search-context-main.cartosdaasap-embed").prependTo("#search-context-main.bo-view");
                    }

                    // ----- HIDE TOOLS OPTIONS -----

                    setTimeout(function () {
                        if (researchContext.context && !researchContext.context.display_route) {
                            jQuery('.itinerary-btn').hide();
                            jQuery('.distance').hide();
                        }
                        if (!researchContext.context.display_city) {
                            console.log('show-city', jQuery('.show-city'));
                            jQuery('.show-city').hide();
                            jQuery('.reset_button').hide();
                            jQuery('.distance').hide();
                        }
                        if (!researchContext.context.display_selection) {
                            console.log('select-choose', jQuery('.select-choose'));
                            jQuery('.my-selection').hide();
                            // Ajouter à la sélection Button are hidden
                        }
                        if (!researchContext.context.display_legend) {
                            console.log('display_legend', jQuery('.display_legend'));
                            jQuery('.display_legend').hide();
                            jQuery('.map-section').removeClass("col-md-8").addClass("col-md-12");
                        }
                        jQuery('.itinerary-btn').click(function () {
                            jQuery('.distance').show();
                        });
                        setTimeout(function () {
                            researchContext.map.invalidateSize();
                        }, 400);
                    }, 1000);
                });
            });
        });
    });
})();

var css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var board = "\n    <div class=\"row cartosdaasap-embed\" id=\"search-context-main\">\n        <div class=\"col-md-12\" style=\"display:none;\" id=\"disabled-message\">\n            <div class=\"callout callout-warning\">\n                <h4>Ce contexte de recherche est d\xE9sactiv\xE9</h4>\n\n                <p>Les information pr\xE9sent\xE9es ne sont peut-\xEAtre pas \xE0 jour.</p>\n            </div>\n        </div>\n        <div class=\"col-md-8 col-md-offset-2\">\n            <div class=\"search-fields\">\n                <form id=\"search-itinerary-form\">\n                    <div class=\"row\">\n                        <div class=\"col-md-6\">\n                            <div class=\"form-group show-city\">\n                                <label for=\"search-start\" id=\"search-start-label\">Rechercher une adresse ou une commune</label>\n                                <select id=\"search-start\" class=\"address-search\"></select>\n                            </div>\n                            <div class=\"form-group end-point-search itinerary\" style=\"display:none;\">\n                                <label for=\"search-end\">Arriv\xE9e</label>\n                                <select id=\"search-end\" class=\"address-search\"></select>\n                            </div>\n                            <div class=\"form-group itinerary\">\n                                <button class=\"btn btn-primary itinerary-btn\"><i class=\"fa fa-map-signs\"></i> Itin\xE9raire</button>\n                                <button class=\"btn btn-warning reset_button\"><i class=\"fa fa-refresh\"></i> R\xE9initialiser</button>\n                            </div>\n                        </div>\n                        <div class=\"col-md-6\">\n                            <div class=\"form-group has-feedback buffer distance\">\n                                <label for=\"buffer\">Distance (km)</label>\n                                <div class=\"range-values\">\n                                    <div class=\"range-value min\">0,25km</div>\n                                    <span class=\"current\" style=\"display:none\"></span>\n                                    <div class=\"range-value max\">10km</div>\n                                </div>\n                                <input id=\"buffer\" type=\"range\" min=\"0.25\" step=\"0.25\" max=\"10\" name=\"buffer\" value=\"5\" class=\"custom-range\" placeholder=\"form.placeholder.buffer\">\n                            </div>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class=\"col-md-2 my-selection\">\n            <button class=\"btn btn-primary open-selection\">Ma s\xE9lection (<span class=\"sel-num\">0</span>)</button>\n        </div>\n        <div id=\"filters-box\" class=\"col-md-10 col-md-offset-2 row\" style=\"display:none\">\n            <div class=\"box box-warning\"> \n                <div class=\"box-header with-border\">\n                    <h3 class=\"box-title\">Filtrer sur des crit\xE8res avanc\xE9s</h3>\n                </div>\n                <div class=\"box-body\" id=\"filters\">\n                    <div class=\"col-md-12\"><button id=\"reset-filters\" class=\"btn btn-warning btn-sm\" style=\"display: none;\">R\xE9initialiser les filtres</button></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-md-4 display_legend\">\n            <div class=\"box box-warning\">\n                <div class=\"box-header with-border\">\n                    <h3 class=\"box-title\" id=\"legend-title\">L\xE9gende</h3>\n                    <div id=\"pdfGenerateAll\" class=\"box-tools pull-right info-box-content\" style=\"padding-top:0;\">\n                        <button id=\"download_pdf_all\" class=\"btn btn-sm btn-primary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Export PDF\"><i class=\"fa fa-file-pdf-o\"></i></a>\n                    </div>\n                </div>\n                <!-- /.box-header -->\n                <div class=\"box-body\" id=\"search-services\">\n                    <p class=\"no-selection\">Aucune selection</p>\n                    <script type=\"text/template\" id=\"search-service-template\">\n                        <div class=\"info-box sm selected\">\n                            <span class=\"info-box-icon\"><span></span><i></i></span>\n\n                            <div class=\"info-box-content\">\n                                <a class=\"btn btn-sm remove-all pull-right btn-group\" disabled title=\"Enlever tous les points de la s\xE9lection\" style=\"display:none;\"><i class=\"fa fa-minus\"></i></a>\n                                <a class=\"btn btn-sm add-all pull-right btn-group\" title=\"Ajouter tous les points de la s\xE9lection\" style=\"display:none;\"><i class=\"fa fa-plus\"></i></a>\n                                <span class=\"info-box-text\"></span>\n                                <span class=\"info-box-number\"></span>\n                                <span class=\"info-box-recherche\"></span>\n                            </div>\n                        </div>\n                        <ul class=\"selection\">\n                        </ul>\n                    </script>\n                </div>\n                <!-- /.box-body -->\n            </div>\n        </div>\n        <div class=\"col-md-8 map-section\">\n                <!-- /.box-header -->\n                <div  id=\"search-map\" style=\"height: 600px\" data-context-id=\"{{ $context->id }}\"  data-api-key=\"{{ $key->key }}\">\n                </div>\n                <!-- /.box-body -->\n        </div>\n        <div class=\"modal fade in\" id=\"modal-my-selection\" style=\"display: none;\" >\n            <div class=\"modal-dialog\">\n                <div class=\"modal-content\" style=\"max-height: 600px !important;overflow-y: scroll;\">\n                    <div class=\"modal-header\">\n                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" title=\"Fermer\">\n                            <i class=\"fa fa-times\"></i>\n                        </button>\n                        <h4 class=\"modal-title\">Ma s\xE9lection</h4>\n                    </div>\n                    <div class=\"modal-body my-selection-details\">\n                        <ul class=\"selection\">\n                        </ul>\n                    </div>\n                    <div class=\"modal-footer\">\n                        <button class=\"btn btn-info\" id=\"download_pdf_selection\">Exporter le PDF</a>\n                        <button type=\"button\" class=\"btn btn-danger\" id=\"reset\">Vider la s\xE9lection</button>\n                    </div>\n                </div>\n                <!-- /.modal-content -->\n            </div>\n            <!-- /.modal-dialog -->\n        </div>\n    </div>\n";

/* harmony default export */ __webpack_exports__["a"] = (board);

/***/ }),

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return researchEventHandlers; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(0);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }



var address = null;
var startAddress = null;
var endAddress = null;

var currentShownFilters = [];
var currentActiveFilters = [];

var selected = null;
var researchEventHandlers = function researchEventHandlers($, researchContext) {
    var services = researchContext.services;
    // jQuery('#print').prop('disabled', true);
    console.log('researchEventHandlers', researchContext);

    selected = researchContext.context.services.map(function (service) {
        return service.id;
    });
    serviceBox(researchContext);

    setTimeout(function () {

        jQuery('body').find('.address-search').select2({
            dropdownParent: jQuery("#search-itinerary-form"),
            language: 'fr',
            templateResult: function templateResult(state) {
                if (!state.id) {
                    return state.text;
                }
                var type = state.type === 'municipality' ? '<span class="label label-default">Commune</span>' : '<span class="label label-default">Adresse</span>';
                return jQuery('<span class="result">' + state.text + '</span> ' + type);
            },
            ajax: {
                url: function url(params) {
                    return params.term;
                },
                transport: function transport(params, success, failure) {
                    if (!params.url) {
                        return success({ results: [] });
                    }

                    console.log('params.url', params.url);
                    researchContext.addressAutocomplete(params.url).then(function (res) {
                        console.log('addressAutocomplete result', res);
                        var data = res.features.map(function (loc) {
                            return {
                                id: (loc.properties.type === 'municipality' ? loc.properties.postcode + ' ' : '') + loc.properties.label,
                                text: (loc.properties.type === 'municipality' ? loc.properties.postcode + ' ' : '') + loc.properties.label,
                                type: loc.properties.type,

                                loc: loc
                            };
                        });

                        console.log('addressAutocomplete data is', data);

                        success({ results: data });
                    }).catch(function (err) {
                        console.log('addressAutocomplete error', err);
                        failure(err);
                    });
                }
            }
        });
    }, 2000);

    function generatePDF(researchContext, data) {

        console.log('generatePDF', researchContext);
        var url = researchContext.baseUrl + '/api/accounts/' + researchContext.context.account_id + '/research_contexts/' + researchContext.context.id + '/download_pdf';

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function beforeSend(xhr) {
                xhr.setRequestHeader('X-CSRF-TOKEN', $('input[name="_token"]').attr('value'));
                $("#pdfGenerateAll").html('<button class="btn btn-sm btn-primary disabled"><i class="fa fa-spin fa-spinner"></i></button>');
                $("#download_pdf_selection").html('<i class="fa fa-spin fa-spinner"></i>');
            },
            data: data,
            xhrFields: {
                responseType: 'blob'
            },
            // xhr: function() {
            //     var request = new XMLHttpRequest();
            //     // request.onreadystatechange = function() {
            //     //     if(request.readyState == 4) {
            //     //         if(request.status == 200) {
            //     //             console.log(typeof request.response); // should be a blob
            //     //         } else if(request.responseText != "") {
            //     //             console.log(request.responseText);
            //     //         }
            //     //     } else if(request.readyState == 2) {
            //     //         if(request.status == 200) {
            //     //             request.responseType = "blob";
            //     //         } else {
            //     //             request.responseType = "text";
            //     //         }
            //     //     }
            //     // };


            //     request.onload =  (e) => {
            //         if (request.status === 200) {
            //             console.log('now')

            //             let link = document.createElement('a');
            //             link.href = window.URL.createObjectURL(response);
            //             link.download = `${data.context.name}.pdf`;
            //             link.click();

            //             $("#pdfGenerateAll").html('<button id="download_pdf_all" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"></i></button>');
            //             $("#download_pdf_selection").html('Exporter le PDF');

            //         }
            //     }
            //     console.log('xhr', request)
            //     return request;
            // },
            success: function success(response, status, xhr) {
                console.log('response', response, status, xhr);
                // Failed to read the 'responseText' property from 'XMLHttpRequest': The value is only accessible if the object's 'responseType' is '' or 'text' (was 'blob').


                // let blob = new Blob([response]);
                var link = document.createElement('a');
                // if (response instanceof Blob ) {
                //     link.href = window.URL.createObjectURL(response);
                // } else {

                //     let pdf  = "data:application/pdf;base64," + response;
                //     let blob = new Blob([pdf]);
                //     link.href = window.URL.createObjectURL(blob);
                // }

                link.href = window.URL.createObjectURL(response);

                // var binaryData = [];
                // binaryData.push(response);
                // link.href = window.URL.createObjectURL(new Blob(binaryData, {type: "application/pdf"}));
                // link.href  = "data:application/pdf;base64," + response;

                link.download = data.context.name + '.pdf';
                link.click();

                $("#pdfGenerateAll").html('<button id="download_pdf_all" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"></i></button>');
                $("#download_pdf_selection").html('Exporter le PDF');
            },
            error: function error(err) {
                console.log('Couldn\'t get current research_context ' + JSON.stringify(err));
            }
        });
    }

    jQuery('body').on('click', 'button.itinerary', function (ev) {
        var btn = jQuery(ev.currentTarget);

        var pointId = parseInt(btn.data('point-id'), 10);
        researchContext.routeTo(pointId);
    }).on('click', 'a.remove-all', function (ev) {
        ev.stopPropagation();
        var btn = jQuery(ev.currentTarget);
        btn.attr('disabled', true);
        var box = btn.parents('.info-box');
        box.next('.selection').empty();
        var serviceId = parseInt(box.attr('id').replace('service-selected-', ''), 10);
        var selUl = jQuery('.my-selection-details').find('.selection');
        var sel = selUl.find('li.selected-point').filter(function (i, item) {
            // console.log(item, serviceId);
            return jQuery(item).data('service-id') !== serviceId;
        });

        selUl.html(sel);
        // console.log(sel);

        var displayNum = jQuery('.sel-num');
        displayNum.text(sel.length);

        $('#service-selected-' + serviceId + ' a.add-all').removeAttr('disabled');
        //TODO update selection number
    }).on('click', 'button.select-choose', function (ev) {
        var btn = jQuery(ev.currentTarget);
        var pointId = parseInt(btn.data('point-id'), 10);

        var point = void 0;
        if (researchContext.points.length) {
            point = researchContext.points.find(function (point) {
                return point.properties.id === pointId;
            });
            point.properties.iconColor = researchContext['services'].find(function (s) {
                return s.id === point.properties.serviceId;
            }).iconColor;
        } else {
            var points = [];
            Object.values(researchContext.servicePoints).forEach(function (arr) {
                return arr.forEach(function (a) {
                    return points.push(a);
                });
            });
            point = points.find(function (point) {
                return point.properties.id === pointId;
            });
            point.properties.iconColor = researchContext['services'].find(function (s) {
                return s.id === point.properties.serviceId;
            }).iconColor;
        }

        var b = jQuery('#service-selected-' + point.properties.serviceId);
        // jQuery('#print').prop('disabled', false);

        var html = selectPoint(point, researchContext);
        b.find('button.remove-all').attr('disabled', false);
        if (jQuery('.selected-point-' + pointId).length === 0) {
            b.next('.selection').append(html);

            jQuery('.my-selection-details').find('.selection').append(html);
            var displayNum = jQuery('.sel-num');
            var numPoints = parseInt(displayNum.text(), 10);
            displayNum.text(numPoints + 1);
        }
    }).on('click', '.remove-selected', function (ev) {
        var pointId = jQuery(ev.currentTarget).parents('li').first().data('point-id');

        $('.selected-point-' + pointId).remove();
        var displayNum = jQuery('.sel-num');
        var numPoints = parseInt(displayNum.text(), 10);
        displayNum.text(numPoints - 1);
    }).on('click', '#point-search-title', function (ev) {
        jQuery(ev.currentTarget).find('i.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
        jQuery('#search-point-form').slideToggle();
        jQuery('#search-itinerary-form').slideUp();
    }).on('click', '#itinerary-search-title', function (ev) {
        jQuery(ev.currentTarget).find('i.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
        jQuery('#search-itinerary-form').slideToggle();
        jQuery('#search-point-form').slideUp();
    })

    //cette fonction permet de reinitialiser la page research_context
    .on('click', '.reset_button', function (ev) {
        //je réinitialise les adresses
        jQuery('#search-point').val([]).trigger('change');
        jQuery('#search-start').val([]).trigger('change');
        jQuery('#search-end').val([]).trigger('change');
        researchContext.home = null;
        researchContext.start = null;
        researchContext.end = null;

        //je remets les curseurs au milieu
        jQuery('#radius').val('5').attr('placeholder', '5');

        jQuery('#buffer').val('5').attr('placeholder', '5');
        jQuery('.current').html('5km');

        researchContext.resetMap();

        //Remove all service and add them again
        var selected = [];
        var unselected = [];
        var toRemove = [];

        jQuery('.info-box.sm').each(function (i, s) {
            var serviceBox = jQuery(s);
            console.log(serviceBox);
            //serviceBox.toggleClass('selected');
            var serviceId = parseInt(serviceBox.attr('id').replace('service-selected-', ''), 10);

            console.log('service Id2', serviceId);
            toRemove.push(serviceId);
            if (serviceBox.hasClass('selected')) {
                selected.push(serviceId);
            } else {
                unselected.push(serviceId);
            }
        });

        console.log('toremove, toadd', toRemove, selected, unselected);
        researchContext.servicesRemoved(toRemove);
        var newServices = researchContext.servicesAdded(selected);

        var calcs = newServices.map(function (s) {
            return s.calc;
        });

        //Point calculations are done, display the number of points
        Promise.all(calcs).then(function () {
            researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit ? true : false);
            displayNumPoints(researchContext, true);
        });
        researchContext.servicesRemoved(unselected);
    }).on('submit', '#search-point-form, #search-itinerary-form', function (ev) {
        ev.preventDefault();
    }).on('click', '.itinerary-btn', function (ev) {
        var s = jQuery('.end-point-search');
        $('.show-city').show();
        $('.reset_button').show();
        $('.buffer.form-group').show();
        if (s.is(':visible')) {
            jQuery('#search-start-label').text('Rechercher une adresse ou une commune');
            jQuery(ev.currentTarget).html('<i class="fa fa-map-signs"></i> Itinéraire');
            researchContext.removeEndMarker();
            researchContext.radiusChanged(parseFloat(jQuery('#buffer').val()) * 1000);
            displayNumPoints(researchContext);
            jQuery('#search-end').val([]).trigger('change');
        } else {
            if ($('.itinerary-btn.single').length) {
                jQuery(ev.currentTarget).hide();
            }
            jQuery('#search-start-label').text('Départ');
            jQuery(ev.currentTarget).html('<i class="fa fa-map-marker"></i> Adresse');
        }

        s.toggle();
    }).on('change', '#search-start', function (ev) {
        ev.preventDefault();
        startAddress = jQuery('#search-start').val();
        var data = $(ev.currentTarget).select2('data');

        if (data && data.length) {
            // Set spinner
            var btni = jQuery('.itinerary-btn').find('i');
            var oldClass = btni.attr("class");
            btni.attr("class", "fa fa-spin fa-refresh");
            address = null;
            // filterFacette = [];
            // researchContext.setFacettes(filterFacette);

            researchContext.startOrEndAddress(data, 'start').then(function (result) {
                console.log('DATA IS', data);

                jQuery('#buffer').removeAttr('disabled').parent().find('.current').show();
                if (!researchContext.end) {
                    if (data && data.length > 0 && data[0].loc.properties.type === 'municipality') {
                        console.log('loc is city', data[0].loc);
                        // $('.buffer.form-group').hide();
                        // HERE NEED TO RESET FACETTES FILTERS ?
                        researchContext.cityChanged(parseFloat(jQuery('#buffer').val()) * 1000);
                    } else if (data && data.length > 0) {
                        $('.buffer.form-group').show();
                        researchContext.radiusChanged(parseFloat(jQuery('#buffer').val()) * 1000);
                    }
                }
                btni.attr("class", oldClass);
                researchContext.serviceFiltered(currentActiveFilters, true);
                displayNumPoints(researchContext);
            }).catch(function (e) {
                alert('Désolé, L\'api ne répond pas');
                console.log(e);
                btni.attr("class", "fa fa-exclamation-triangle");
            });
        }
    }).on('change', '#search-end', function (ev) {
        ev.preventDefault();
        endAddress = jQuery('#search-end').val();
        if (!endAddress) {
            return;
        }
        var data = $(ev.currentTarget).select2('data');

        if (data && data.length) {
            // Set spinner
            var btni = jQuery('.itinerary-btn').find('i');
            var oldClass = btni.attr("class");
            btni.attr("class", "fa fa-spin fa-refresh");
            address = null;
            // filterFacette = [];
            // researchContext.setFacettes(filterFacette);

            researchContext.startOrEndAddress(data, 'end').then(function (result) {
                jQuery('#buffer').removeAttr('disabled').parent().find('.current').show();
                researchContext.serviceFiltered(currentActiveFilters, true);
                displayNumPoints(researchContext);
                btni.attr("class", oldClass);
            }).catch(function (e) {
                alert('Désolé, L\'api ne répond pas');
                console.log(e);
                btni.attr("class", "fa fa-exclamation-triangle");
            });
        }
    }).on('click', '#reset-filters', function (ev) {
        currentActiveFilters = [];
        jQuery('#filters .select2').val([]).trigger("change");
    }).on('click', '#filters .select2', function (ev) {

        $("li.select2-results__option").each(function () {
            if ($(this).attr("aria-selected") === "true") {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }).on('change', '#filters .select2', function (ev) {
        var select = jQuery(ev.currentTarget);
        var values = select.val(); // ['Accompagnement']
        var field = select.attr('id'); // fiche_dispositif_type
        var name = select[0].dataset.nameValue;

        currentActiveFilters = currentActiveFilters.filter(function (filter) {
            return filter.name != name;
        });
        currentActiveFilters.push({ name: name, field: field, values: values });
        currentActiveFilters = currentActiveFilters.filter(function (filter) {
            return filter.values.length;
        });

        //let t = jQuery('input[name=buffer]')

        // if (researchContext.endMarker === null) {
        //     researchContext.radiusChanged(parseFloat(t.val()) * 1000);
        // } else {
        //     researchContext.bufferChanged(parseFloat(t.val()));
        // }

        researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit || researchContext.start);

        // HIDE RESET BUTTON WHEN FILTERS EMPTY
        if (!currentActiveFilters.length) {
            jQuery('#reset-filters').hide();
        } else {
            jQuery('#reset-filters').show();
        }

        displayNumPoints(researchContext);

        console.log('ev', ev, $('#' + field + '-selection'));

        $('#' + field + '-selection .select2-selection__rendered li:not(.select2-search--inline)').hide();
        $('#' + field + '-selection .counter').remove();
        var firstSelected = $('#' + field + '-selection  .select2-selection__choice')[0] ? $('#' + field + '-selection  .select2-selection__choice')[0].title : '';
        var counter = $('#' + field + '-selection  .select2-selection__choice').length - 1;
        if (counter >= 0) {
            $('#' + field + '-selection .select2-selection__rendered').after('<div class="counter"><div class="counter-text">' + firstSelected + '</div><div class="counter-number"> ' + (counter ? '+ ' + counter : '') + '</div></div>');
            $('#' + field + '-selection .select2-selection').css("background-color", "#1f88be").css("color", "white");
        } else {
            $('#' + field + '-selection .select2-selection__rendered').after('');
            $('#' + field + '-selection .select2-selection').css("background-color", "white").css("color", "grey");
        }
    }).on('change', 'input[name=buffer]', function (ev) {
        var t = jQuery(ev.currentTarget);
        t.parent().find('.current').text(parseFloat(t.val()) + 'km');

        if (researchContext.endMarker === null) {
            console.log('changing radius to ', parseFloat(t.val()) * 1000);
            researchContext.radiusChanged(parseFloat(t.val()) * 1000);
        } else {
            console.log('changing buffer to ', parseFloat(t.val()));
            researchContext.bufferChanged(parseFloat(t.val()));
        }
        // researchContext.serviceFiltered(currentActiveFilters, researchContext.currentCityLimit || researchContext.startcityLimit );
        console.log('CHANGE RADIUS BUFFER', researchContext.startcityLimit, researchContext.end, researchContext.end || researchContext.startcityLimit);
        researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit || researchContext.start);

        displayNumPoints(researchContext);
    }).on('click', 'a.add-all', function (ev) {

        ev.preventDefault();
        ev.stopPropagation();

        var btn = $(ev.currentTarget);
        var serviceId = btn.closest(".info-box").attr('id').match(/\d/g).join("");

        if (!$('#service-selected-' + serviceId + ' a.add-all').is('[disabled]')) {
            // IF ADD ALL BUTTON IS NOT DISABLED -> ADD TO SELECTION

            // const serviceId = btn.closest(".info-box").attr('id').match(/\d/g).join("")

            // Get points in the current area (circle or buffer)
            var group = researchContext.group;
            if (!group) {
                return;
            }
            // They are simply the points that are currently displayed on the map
            var points = group.getLayers().filter(function (l) {
                return l.options.serviceId == serviceId;
            }).map(function (m) {
                return m.options.point;
            });

            var b = jQuery('#service-selected-' + serviceId);
            b.find('.remove-all').attr('disabled', false);
            b.find('.add-all').attr('disabled', true);
            // Enable print button
            // jQuery('#print').prop('disabled', false);

            // Remove points that have already been selected and generate the html for the others
            var filteredPoints = points.filter(function (p) {
                return jQuery('.selected-point-' + p.properties.id).length === 0;
            }).map(function (point) {
                return _extends({}, point, { properties: Object.assign({}, point.properties, { iconColor: researchContext['services'].find(function (s) {
                            return s.id === point.properties.serviceId;
                        }).iconColor }) });
            });

            var html = filteredPoints.map(function (p) {
                return selectPoint(p, researchContext);
            });
            // HERE MAKE THE SAME THING BUT ONLY OBJECT TO SEND IT TO PRINTER
            // console.log('filteredPoints', filteredPoints, html)
            b.next('.selection').append(html);
            jQuery('.my-selection-details').find('.selection').append(html);
            var displayNum = jQuery('.sel-num');
            var numPoints = parseInt(displayNum.text(), 10);
            displayNum.text(numPoints + filteredPoints.length);
        }
    }).on('click', '#search-services .info-box', function (ev) {
        // Toggle service selection by clicking on its box
        console.log('changed');

        var serviceBox = jQuery(ev.currentTarget);
        serviceBox.toggleClass('selected');
        var serviceId = parseInt(serviceBox.attr('id').replace('service-selected-', ''), 10);

        if (serviceBox.hasClass('selected')) {
            var res = researchContext.servicesAdded([serviceId], currentActiveFilters);
            res.forEach(function (r) {
                r.calc.then(function () {
                    console.log('servicesAdded', researchContext.radius, researchContext.buffer, researchContext.route, researchContext.end, researchContext.currentCityLimit);
                    researchContext.serviceFiltered(currentActiveFilters, researchContext.end || researchContext.startcityLimit || researchContext.start);
                    displayNumPoints(researchContext);
                });
            });
            jQuery('.show_ico' + serviceId).html('<i class="fa fa-eye"></i>');
            $('#service-selected-' + serviceId + ' .add-all').attr('disabled', false);
        } else {
            $('#service-selected-' + serviceId + ' .add-all').attr('disabled', true);
            researchContext.servicesRemoved([serviceId]);
            jQuery('.show_ico' + serviceId).html('<i class="fa fa-eye-slash"></i>');
        }
        // if (currentActiveFilters && currentActiveFilters.length) {
        //     researchContext.serviceFiltered(currentActiveFilters, researchContext.buffer || researchContext.currentCityLimit || researchContext.radius ? true : false); // true to reset markers or it duplicates
        // }
    }).on('click', '.open-selection', function (ev) {
        ev.preventDefault();
        jQuery('#modal-my-selection').css('display', 'block');
    }).on('click', '[data-dismiss=modal]', function (ev) {
        ev.preventDefault();
        jQuery('#modal-my-selection').css('display', 'none');
    }).on('click', '#reset', function (ev) {
        $('.selected-point').remove();
        $('.sel-num').text(0);
        $('.remove-all').attr('disabled', true);
    }).on('click', '#download_pdf_selection', function (ev) {

        var pointsToFilter = jQuery('li.selected-point').toArray().map(function (el) {
            return JSON.parse(decodeURIComponent(escape(atob(jQuery(el).data('object')))));
        }); // BACK FROM BASE 64 TO STRINGIFY DATA

        var pointsId = Array.from(new Set(pointsToFilter.map(function (p) {
            return p.id;
        })));

        console.log('pointsToFilter', pointsToFilter, services);

        var points = pointsToFilter.filter(function (a) {
            if (pointsId.includes(a.id)) {
                pointsId.splice(pointsId.indexOf(a.id), 1);
                return true;
            }
            return false;
        });

        var s = services.map(function (s) {
            return { id: s.id, name: s.name, info: s.info, iconColor: s.iconColor, points: points.filter(function (p) {
                    return p.serviceId == s.id;
                }) };
        });
        var data = { context: { name: researchContext.context.name }, services: s, button: "Votre sélection" };
        console.log('data', data);
        generatePDF(researchContext, data);
    }).on('click', '#download_pdf_all', function (ev) {
        // TO GET UPDATED POINTS
        var customServices = researchContext.services.map(function (s) {
            s.currentPoints = researchContext.group.getLayers().filter(function (m) {
                return m.options.serviceId === s.id;
            }).map(function (m) {
                return m.options.point.properties;
            });
            return s;
        });
        var context = { name: researchContext.context.name };
        var s = customServices.map(function (s) {
            return { id: s.id, name: s.name, info: s.info, iconColor: s.iconColor, points: s.currentPoints };
        });

        var data = { context: context, services: s, button: "Votre résultat" };
        generatePDF(researchContext, data);
    });
};

var displayNumPoints = function displayNumPoints(researchContext) {
    var ignoreNonSelected = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    jQuery('#search-services').find('.info-box').each(function (i, item) {

        if (researchContext.context.display_selection) {
            $('.remove-all').show();
            $('.add-all').show().removeAttr('disabled');
        }

        var $item = jQuery(item);

        var id = parseInt($item.attr('id').replace('service-selected-', ''), 10);

        var group = researchContext.group;
        if (!group) {
            return;
        }

        var n = group.getLayers().filter(function (m) {
            return m.options.serviceId === id;
        });

        if (ignoreNonSelected && !$item.hasClass('selected')) {
            n = researchContext.servicePoints[id];
        }
        // TO DO : SET POLYGONE LINES NUMBER
        $item.find('.info-box-number').text(n.length);

        // UPDATE FILTERS POINT NUMBER
        $item.find('ul').each(function (e, ul) {
            var _this = this;

            var service = researchContext.services.find(function (s) {
                return s.id == $(_this).data("service-id");
            });
            if (service) {
                var nFilter = n.map(function (l) {
                    return l.options.point.properties.id;
                });
                var facetteKey = $(this).data("facette-value");
                $(this).children().each(function (i, li) {
                    var _this2 = this;

                    // VALUE FILTER
                    var data = service.data_recherche.filter(function (data) {
                        return nFilter.includes(data.id) && data.prop[facetteKey] == $(_this2).data("recherche-value");
                    });
                    $(this).find('.badge').text(data.length);
                });
            }
        });

        // Change add all button tooltip
        var title = 'Ajouter les ' + n.length + ' services à la sélection';
        $item.find('button.add-all').attr('title', title).tooltip('fixTitle');
        if (n.length === 0) {
            $item.find('button.remove-all').attr('disabled', true);
        }
    });
    console.log('DISPLAYNUM POINT', $('.info-box-recherche ul'));
};

var selectPoint = function selectPoint(point, researchContext) {
    console.log('point', point);
    var pointId = point.properties.id;

    var props = [];
    var name = '';

    // console.log('point', point);
    if (point.selectionProp) {
        props = Object.keys(point.selectionProp).map(function (key) {
            if (key !== 'name' && key !== 'id' && key !== 'serviceId') {
                return '<li>' + key + '&nbsp;: <strong>' + point.selectionProp[key] + '</strong></li>';
            }
        });
        name = typeof point.selectionProp.name !== 'undefined' && point.selectionProp.name !== null ? point.selectionProp.name : '&nbsp;';
    }

    console.log('btoa error on dev : ', JSON.stringify(point.properties));
    return '<li data-object=\'' + btoa(unescape(encodeURIComponent(JSON.stringify(point.properties)))) + '\' data-service-id="' + point.properties.serviceId + '" class="selected-point selected-point-' + pointId + '" data-point-id="' + pointId + '">\n    <div><span style="background-color:' + point.properties.iconColor.color + ';padding: 10px;margin-right: 10px;">\n    ' + (point.properties.iconColor.icon_path ? '<img src="' + researchContext.baseUrl + '/storage/' + point.properties.iconColor.icon_path + '" alt="' + point.properties.iconColor.icon + '" style="max-width: 20px;"/>' : '<i class="' + point.properties.iconColor.icon + '" style="font-size: 20px;color:' + Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["e" /* pickColor */])(point.properties.iconColor.color) + '"></i>') + '\n    </span>' + name + '\n    <button class="btn btn-danger remove-selected pull-right"><i class="fa fa-close"></i></button></div><ul>' + props.join('') + '</ul></li>';
    // return `<li data-object='${btoa(JSON.stringify(point.properties))}' data-service-id="${point.properties.serviceId}" class="selected-point selected-point-${pointId}" data-point-id="${pointId}"><strong>${name}<button class="btn btn-danger remove-selected pull-right"><i class="fa fa-close"></i></button></strong><ul>${props.join('')}</ul></li>`;
    // return `<li data-object='${JSON.stringify(point.properties)}' data-service-id="${point.properties.serviceId}" class="selected-point selected-point-${pointId}" data-point-id="${pointId}"><strong>${name}<button class="btn btn-danger remove-selected pull-right"><i class="fa fa-close"></i></button></strong><ul>${props.join('')}</ul></li>`;
};
function generateServiceFilters(res) {
    console.log('res', res);
    var facettes_array = JSON.parse(res.display_recherche);

    console.log('display_recherche', res.display_recherche, facettes_array);
    if (facettes_array) {
        delete facettes_array["name"];
        var filterSet = new Set(Object.values(facettes_array));
        var sortedSet = [].concat(_toConsumableArray(filterSet)).sort();
        filterSet = new Set(sortedSet);

        filterSet.forEach(function (facette) {
            var name = Object.keys(facettes_array).find(function (key) {
                return facettes_array[key] === facette;
            });

            if (name !== 'name') {
                var filter = { name: name, type: facette, currentValues: [] // currentValues : {value :}
                };res.data_recherche.forEach(function (point) {
                    // point.prop : {fiche_dispositif_type: 'Accompagnement', fiche_acces_pmr: 'Oui'}
                    console.log('point.prop[facette]', point.prop[facette]);
                    var isBr = point.prop[facette] && point.prop[facette].includes('<br>');
                    var isSlash = point.prop[facette] && point.prop[facette].includes('|');
                    var isComma = point.prop[facette] && point.prop[facette].includes(';');

                    if (isBr) {
                        var stringSplit = point.prop[facette].split('<br>');
                        stringSplit.forEach(function (str) {
                            var prop_value = filter.currentValues.find(function (v) {
                                return v.value == str;
                            });
                            if (!prop_value) {
                                filter.currentValues.push({ value: str });
                            }
                        });
                    } else if (isSlash) {
                        var _stringSplit = point.prop[facette].split('|');
                        _stringSplit.forEach(function (str) {
                            var prop_value = filter.currentValues.find(function (v) {
                                return v.value == str;
                            });
                            if (!prop_value) {
                                filter.currentValues.push({ value: str });
                            }
                        });
                    } else if (isComma) {
                        var _stringSplit2 = point.prop[facette].split(';');
                        _stringSplit2.forEach(function (str) {
                            var prop_value = filter.currentValues.find(function (v) {
                                return v.value == str;
                            });
                            if (!prop_value) {
                                filter.currentValues.push({ value: str });
                            }
                        });
                    } else {
                        var prop_value = filter.currentValues.find(function (v) {
                            return v.value == point.prop[facette];
                        });
                        if (!prop_value && point.prop[facette]) {
                            filter.currentValues.push({ value: point.prop[facette] });
                        }
                    }
                });
                // SET HTML
                var filterSelection = '';
                var selectHtml = '';

                var filterFound = currentShownFilters.find(function (f) {
                    return f.name == filter.name;
                });
                if (filterFound) {
                    filter.currentValues.forEach(function (v, i) {
                        var includeOption = filterFound.select.includes('<option>' + v.value + '</option>');
                        if (!includeOption) {
                            // IF VALUE IS NOT IN OPTIONS FROM SAME SELECT -> ADD VALUE AT THE END BEFORE </select>
                            var n = filterFound.select.lastIndexOf("</select>");
                            var str2 = filterFound.select.substring(0, n) + ('<option>' + v.value + '</option>') + filterFound.select.substring(n);
                            currentShownFilters = currentShownFilters.map(function (f) {
                                return f.name == filterFound.name ? _extends({}, f, { select: str2 }) : _extends({}, f);
                            });
                        }
                    });
                } else {
                    filter.currentValues.forEach(function (v, i) {
                        console.log('v', v);
                        //Ces valeurs sont séparées par des ; ou par des | ou par des <br>.
                        filterSelection += '<option>' + v.value + '</option>';
                    });
                    // S'IL Y A AU MOINS UN FILTRE
                    if (filterSelection) {
                        selectHtml += '<div class="col-md-3" id="' + filter.type + '-selection"><select id="' + filter.type + '" data-name-value="' + filter.name + '" multiple class="form-control select2" placeholder="' + facette + '" data-toggle="tooltip" data-placement="top" title="' + filter.name + '">' + filterSelection + '</select></div>';
                    }
                    currentShownFilters.push(_extends({}, filter, { select: selectHtml }));
                }
            }
        });
    }
}

var serviceBox = function serviceBox(researchContext) {
    var vals = selected;
    //Hide map
    Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["f" /* startLoading */])('#search-context-main .col-md-8');
    console.log('researchContext serviceBox', researchContext, researchContext.services[0]);
    var added = vals.filter(function (x) {
        return !researchContext.obtained.includes(x);
    });
    var removed = researchContext.obtained.filter(function (x) {
        return !vals.includes(x);
    });
    removed.forEach(function (serviceId) {
        jQuery('#service-selected-' + serviceId).next('ul.selection').remove();
        jQuery('#service-selected-' + serviceId).remove();
        if (jQuery('#search-services').find('.info-box').length === 0) {
            jQuery('#search-services').find('.no-selection').show();
        }
    });

    //Display loading box
    added.forEach(function (id) {
        var $tmpl = jQuery(jQuery('#search-service-template').text());
        $tmpl.first().attr('id', 'service-loading-' + id);
        $tmpl.find('.info-box-icon').css('background-color', "white");
        $tmpl.find('.info-box-icon > span').css('border-color', "white");
        $tmpl.find('.info-box-icon > i').css('color', 'white');
        $tmpl.find('.info-box-text').text('Chargement en cours');
        $tmpl.find('.info-box-number').html('<i class="fa fa-spin fa-spinner"></i>');
        jQuery('#search-services').append($tmpl).find('.no-selection').hide();
    });

    researchContext.servicesRemoved(removed);
    var newServices = researchContext.servicesAdded(added);

    var requests = newServices.map(function (s) {
        return s.req;
    });
    var calcs = newServices.map(function (s) {
        return s.calc;
    });

    //Point calculations are done, display the number of points
    Promise.all(calcs).then(function (results) {
        results.forEach(function (res) {
            var d = jQuery('#service-selected-' + res.id);
            var n = researchContext.servicePoints[res.id].length;
            console.log('res', res.id);
            d.find('.info-box-number').text(n);
            d.find('.info-box-content').prepend('<div class="pull-right btn-group"><button title="Afficher/Masquer sur la carte" class="show_ico' + res.id + ' btn btn-sm" style="background-color: rgb(239, 239, 239);"><i class="fa fa-eye"></i></button></div>');
            d.find('button.add-all').tooltip();
            d.find('button.remove-all').tooltip();
        });
        displayNumPoints(researchContext);
    }).catch(function (er) {
        console.log(er);
    });

    //Requests are done, display the data for this service
    Promise.all(requests).then(function (results) {
        //researchContext.getPointsInBufferZone();

        results.forEach(function (res) {
            var $tmpl = jQuery(jQuery('#search-service-template').text());
            $tmpl.first().attr('id', 'service-selected-' + res.id);

            // SERVICE MARKERS RESULT STYLE
            if (res.iconColor && res.iconColor.color && res.iconColor.icon) {
                $tmpl.find('.info-box-icon').css('background-color', res.iconColor.color);
                $tmpl.find('.info-box-icon > span').css('border-color', res.iconColor.color);

                if (res.iconColor.icon_svg) {
                    // ICON CUSTOM (SVG)
                    $tmpl.find('.info-box-icon').css('color', Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["e" /* pickColor */])(res.iconColor.color)).html('<img src="' + researchContext.baseUrl + '/storage/' + res.iconColor.icon_path + '" alt="' + res.iconColor.icon + '" style="max-width: 30px;"/>');
                } else {
                    $tmpl.find('.info-box-icon > i').css('color', Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["e" /* pickColor */])(res.iconColor.color)).addClass(res.iconColor.icon);
                }
            }
            // SERVICE POLYGONE / LINE RESULT STYLE
            else if (res.outline) {
                    $tmpl.find('.info-box-icon').css('background', 'white');
                    $tmpl.find('.info-box-icon').css('background-color', (res.geometry === 'Polygon' ? res.fill : '{ffffff') + '6f'); // 6f pour opacité - 50%
                    $tmpl.find('.info-box-icon').css('border', '5px solid ' + res.outline);
                }

            // let facettes = generateFacetteText(res);
            var text = res.name ? res.name + (res.info ? '<br><small style="color: #999;text-transform: none;font-size: .8em;">' + res.info + '</small>' : '') : '&nbsp;';

            $tmpl.find('.info-box-text').html(text);
            $tmpl.find('.info-box-number').html('<i class="fa fa-spin fa-spinner"></i>');
            console.log("$tmpl.find('#filters')", $tmpl);
            // $tmpl.find('.info-box-recherche').html(facettes);
            jQuery('#search-services').append($tmpl).find('.no-selection').hide();
            var l = jQuery('#service-loading-' + res.id);
            l.next('ul.selection').remove();
            l.remove();

            generateServiceFilters(res);
        });

        // INIT FILTERS
        currentShownFilters.forEach(function (filter, i) {
            if (i < 10) {
                // MOINS DE 10 FILTRES MONTRÉS
                $('#filters-box').css('display', 'initial');

                var selector = '#' + filter.type + '.select2';
                $('#filters').append(filter.select);
                $(selector).select2({
                    placeholder: filter.name,
                    dropdownParent: $('#search-context-main.cartosdaasap-embed')

                });
            }
        });

        // Remove spinner from map
        Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["g" /* stopLoading */])();
    }).catch(function (er) {
        console.log(er);
    });
};

/***/ })

/******/ });