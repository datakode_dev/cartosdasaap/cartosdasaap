/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 63);
/******/ })
/************************************************************************/
/******/ ({

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(64);


/***/ }),

/***/ 64:
/***/ (function(module, exports) {

// edition d'un point issu d'un service
// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function () {

    var marker;
    //***** Affichage de la carte *********
    function map() {

        // Récupération du marqueur avec ses coordonnées
        var markerdata = $('#maCarte').data("json");
        var latitude = markerdata['coordonnees'][1];
        var longitude = markerdata['coordonnees'][0];

        // Centrage de la carte en fonction du marqueur
        var map = L.map('maCarte').setView([latitude, longitude], 8);

        // Couches OpensStreetMap et Esri_WorldImagery
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });
        var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        });
        // Layer par défaut
        map.addLayer(OpenStreetMap_Mapnik);

        // possibilité de changer de type de carte
        map.addControl(new L.Control.Layers({
            'OpenStreetMap_Mapnik': OpenStreetMap_Mapnik,
            'Esri_WorldImagery': Esri_WorldImagery
        }));

        //affichage du marker sur la carte
        marker = L.marker([latitude, longitude], {
            draggable: true,
            id: markerdata['id'],
            update: true });
        map.addLayer(marker);

        //si on deplace le marqueur, on affiche le bouton pour sauvegarder
        marker.on('dragend', function (value) {
            $('#sauvegardeMarkers').css('visibility', 'visible');
        });

        //Sauvegarde du marqueur
        $('#sauvegardeMarkers').on('click', function () {
            //je stocke le marqueur contient l'id,latitude,longitude,actif,update
            var marker_id = marker.options.id;
            var marker_lat = marker._latlng.lat;
            var marker_lng = marker._latlng.lng;
            var update = marker.options.update;
            var marker_update = [marker_id, marker_lat, marker_lng, true, update];
            //je convertis le marker en string et je le mets dans la variable markers_update
            $('#marker_update').val(JSON.stringify(marker_update));
        });
    }
    map();
});

/***/ })

/******/ });