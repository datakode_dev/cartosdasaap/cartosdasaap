/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 38);
/******/ })
/************************************************************************/
/******/ ({

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(39);


/***/ }),

/***/ 39:
/***/ (function(module, exports) {

var element = document.getElementById('table_ico_color');
if (element !== null) {
    $('#table_ico_color').DataTable({
        language: window.data_table_lang,
        ajax: element.dataset.url,
        columns: [{
            mData: 'name',
            name: 'name'
        }, {
            mData: 'icone',
            name: 'icone'
        }, {
            "data": "links",
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                var html = "";
                console.log(sData.edit);
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                if (sData.show != undefined) {
                    html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                }
                if (sData.delete != undefined) {
                    html += "<a data-toggle='tooltip' title='Supprimer' href='" + sData.delete + "'><i class='fa fa-trash' aria-hidden='true'></i></a> ";
                }
                $(nTd).html(html);
            }
        }]
    });
}

/***/ })

/******/ });