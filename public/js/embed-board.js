/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 35);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fatalError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return formatLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return pickColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createWorker; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getPopup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__ = __webpack_require__(1);



var startLoading = function startLoading() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '#embed-script-context';

    var spinner = jQuery('.overlay');
    if (spinner.length === 0) {
        var div = jQuery(target).find('.box');

        if (target === '#embed-script-context') {
            div = jQuery(target).find('.box .box');
        }

        div.append('<div style="transform: translate3d(0px, 0px, 5px);z-index:1000;" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    spinner.html('<i class="fa fa-refresh fa-spin">');
    spinner.show();
};

var stopLoading = function stopLoading() {
    jQuery('.overlay').hide();
};

var fatalError = function fatalError(message) {
    jQuery('.overlay').html('<i class="fa fa-exclamation-triangle"></i><p class="text-muted">' + message + '</p>');
};

var formatLabel = function formatLabel(str, maxwidth) {
    var sections = [];
    if (typeof str === 'undefined') {
        return "";
    }
    var words = str.split(" ");
    var temp = "";

    words.forEach(function (item, index) {
        if (temp.length > 0) {
            var concat = temp + ' ' + item;

            if (concat.length > maxwidth) {
                sections.push(temp);
                temp = "";
            } else {
                if (index === words.length - 1) {
                    sections.push(concat);
                    return;
                } else {
                    temp = concat;
                    return;
                }
            }
        }

        if (index === words.length - 1) {
            sections.push(item);
            return;
        }

        if (item.length < maxwidth) {
            temp = item;
        } else {
            sections.push(item);
        }
    });

    return sections;
};

var pickColor = function pickColor(bgColor) {
    var color = bgColor.charAt(0) === '#' ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map(function (col) {
        if (col <= 0.03928) {
            return col / 12.92;
        }
        return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
    return L > 0.59 ? '#333' : '#fff';
};

var createWorker = function createWorker(workerUrl) {
    var worker = null;
    try {
        var blob = void 0;
        try {
            blob = new Blob(["importScripts('" + workerUrl + "');"], { "type": 'application/javascript' });
        } catch (e) {
            var builder = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder)();
            builder.append("importScripts('" + workerUrl + "');");
            blob = builder.getBlob('application/javascript');
        }
        var url = window.URL || window.webkitURL;
        var blobUrl = url.createObjectURL(blob);
        worker = new Worker(blobUrl);
    } catch (e1) {
        //if it still fails, there is nothing much we can do
    }
    return worker;
};

var getPopup = function getPopup(p) {
    p.properties = Object(__WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__["a" /* mapField */])([{ prop: p.properties }], p.display_fields)[0].prop;
    var title = "";
    if (p.properties && p.properties.name) {
        title = p.properties.name;
    }
    title = '<p class="title"><span class="icon" style="background-color:' + p.color + '"><i class="' + p.icon + '" style="color:' + pickColor(p.color) + '"></i></span> <span class="text" style="color:' + p.color + '">' + title + '</span></p>';
    Object.keys(p.properties).map(function (key) {
        if (key !== 'name' && key !== 'id' && key !== 'serviceId' && p.properties[key] && p.properties[key] !== '') {
            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + p.properties[key] + '</strong></p>';
        }
    });
    return title;
};

/***/ }),

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapField; });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var mapField = function mapField(points, fieldMap, selectionMap) {
    if (!selectionMap || (typeof selectionMap === 'undefined' ? 'undefined' : _typeof(selectionMap)) !== 'object') {
        selectionMap = fieldMap;
    }
    if (!fieldMap || (typeof fieldMap === 'undefined' ? 'undefined' : _typeof(fieldMap)) !== 'object') {
        return points.map(function (point) {
            Object.keys(point.prop).forEach(function (k) {
                var val = point.prop[k];
                if (!val) {
                    delete point.prop[k];
                }
                if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                    //Is a link, replace with link
                    val = '<a href="' + val + '" target="_blank">' + val + '</a>';
                    point.prop[k] = val;
                }
            });
            point.selectionProp = point.prop;

            return point;
        });
    }
    return points.map(function (point) {
        var newProps = {};
        Object.keys(fieldMap).forEach(function (k) {
            var val = point.prop[fieldMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newProps[k] = val;
            }
        });

        var newSelectionProps = {};
        Object.keys(selectionMap).forEach(function (k) {
            var val = point.prop[selectionMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newSelectionProps[k] = val;
            }
        });

        point.selectionProp = newSelectionProps;
        point.prop = newProps;
        return point;
    });
};

/***/ }),

/***/ 2:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Api = function () {
    function Api(baseUrl) {
        _classCallCheck(this, Api);

        this.baseUrl = baseUrl;
    }

    _createClass(Api, [{
        key: 'get',
        value: function get(url) {
            var _this = this;

            if (typeof $ === 'undefined') {
                $ = window.jQuery;
            }
            return new Promise(function (resolve, reject) {
                jQuery.get(_this.baseUrl + url).then(function (res) {
                    return resolve(res);
                }).fail(function (err) {
                    return reject(err);
                });
            });
        }
    }]);

    return Api;
}();

/* harmony default export */ __webpack_exports__["a"] = (Api);

/***/ }),

/***/ 3:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Map = function () {
    function Map(baseUrl) {
        _classCallCheck(this, Map);

        this.map = L.map('maCarte', { maxZoom: 19 }).setView([46.85, 2.3518], 5);
        this.activeLayers = [];
        this.geometry = null;
        this.controls = null;
        this.markers = [];
        var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        });

        if (typeof L.Control.Fullscreen !== 'undefined') {
            this.map.addControl(new L.Control.Fullscreen({
                title: {
                    'false': 'Voir en plein écran',
                    'true': 'Sortir de la vue plein écran'
                }
            }));
        }

        window.proxiclicMap = this.map;

        this.map.addLayer(OpenStreetMap_Mapnik);
    }

    _createClass(Map, [{
        key: 'setControls',
        value: function setControls(controls) {
            this.controls = controls;
        }
    }, {
        key: 'activateLayer',
        value: function activateLayer(layer, color) {
            var _this = this;

            var removePrevious = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
            var fitBounds = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

            if (this.activeLayers && removePrevious) {
                this.activeLayers.forEach(function (l) {
                    _this.map.removeLayer(l);
                });
            }

            var geom = this.displayGeometry(layer.geom, color, false, 0.4, fitBounds);
            this.activeLayers.push(geom);

            var ls = [].concat(_toConsumableArray(this.activeLayers));
            ls = ls.reverse();

            setTimeout(function () {
                _this.markers.forEach(function (m) {
                    if (m) {
                        m.bringToFront();
                    }
                });
            }, 100);

            return geom;
        }
    }, {
        key: 'clear',
        value: function clear() {
            var _this2 = this;

            var fit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

            if (this.activeLayers.length > 0) {
                this.activeLayers.forEach(function (l) {
                    _this2.map.removeLayer(l);
                });
            }
            if (fit) {
                this.map.fitBounds(this.geometry.getBounds());
            }
        }
    }, {
        key: 'displayGeometry',
        value: function displayGeometry(geometry) {
            var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "blue";
            var dashArray = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var opacity = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0.3;
            var fitBounds = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
            var type = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

            var options = {};
            if (type === 'circle') {
                // These are the ponits from the service
                options.pointToLayer = function (feature, latlng) {
                    return L.circleMarker(latlng, {
                        radius: 4
                    });
                };
            }
            var geojsonLayer = L.geoJson(geometry, options);

            // Display service points


            geojsonLayer.setStyle({ fillColor: color, color: dashArray === false ? 'transparent' : color, dashArray: null, fillOpacity: opacity, weight: dashArray === false ? 0 : 1 });
            geojsonLayer.addTo(this.map);

            geojsonLayer.bringToFront();
            if (fitBounds) {
                this.map.fitBounds(geojsonLayer.getBounds());
            }

            return geojsonLayer;
        }
    }, {
        key: 'changeGeometry',
        value: function changeGeometry(geometry) {
            var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "blue";
            var dashArray = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var opacity = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0.4;

            if (this.geometry) {
                this.map.removeLayer(this.geometry);
            }
            this.geometry = this.displayGeometry(geometry, color, dashArray, opacity);
        }

        /**
         * Make a cluster group from a list of markers
         *
         * @param service
         * @param serviceMarkers
         */

    }, {
        key: 'makeGroup',
        value: function makeGroup(service, serviceMarkers) {
            var group = L.markerClusterGroup({
                iconCreateFunction: function iconCreateFunction(cluster) {
                    return L.divIcon({ html: '<div style="background-color:' + service.color + ' !important;color:' + Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["e" /* pickColor */])(service.color) + ' !important"><span>' + cluster.getChildCount() + '</span></div>', className: 'custom-cluster', iconSize: L.point(40, 40) });
                }
            });

            group.addLayers(serviceMarkers);

            return group;
        }
    }]);

    return Map;
}();

/* harmony default export */ __webpack_exports__["a"] = (Map);

/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(36);


/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__templates_board__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Board__ = __webpack_require__(5);




/*
Usage : <script src="/js/embed.js" data-context-id="CONTEXT_ID"></script>

Where CONTEXT_ID is the id of the context to display
 */

var thisJs = document.querySelector("script[src*='embed-board.js']");

var url = document.createElement('a');
//  Set href to any path
url.setAttribute('href', thisJs.getAttribute('src'));

var baseUrl = url.protocol + '//' + url.hostname + (url.port ? ':' + url.port : '');
console.log('baseurl', baseUrl);
/**
 *
 * @param url
 * @returns {Promise}
 */
var loadScript = function loadScript(url) {
    return new Promise(function (resolve, reject) {
        var script = document.createElement("script");
        script.onload = resolve;
        script.onerror = reject;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    });
};

var loadjQuery = function loadjQuery() {
    if (window.jQuery) {
        console.log('jquery ok');
        return Promise.resolve();
    } else {
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (window.jQuery) {
                    console.log('jquery loaded');
                    resolve();
                } else {
                    console.log('jquery loaded from elsewhere');
                    loadScript('https://code.jquery.com/jquery-3.4.1.min.js').then(function () {
                        resolve();
                    });
                }
            }, 2000);
        });
    }
};
var css = document.createElement("link");
css.rel = "stylesheet";
css.href = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css";
document.getElementsByTagName("head")[0].appendChild(css);

css = document.createElement("link");
css.rel = "stylesheet";
css.href = baseUrl + "/css/embed.css";
document.getElementsByTagName("head")[0].appendChild(css);

(function () {
    var boardId = thisJs.getAttribute('data-board-id');

    var jquery = loadjQuery();

    // Is it worth it to load select2 here ?

    jquery.then(function () {
        var chartJs = loadScript('//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js');
        var leaflet = loadScript('https://unpkg.com/leaflet@1.3.1/dist/leaflet.js');
        var select2 = loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js');

        Promise.all([chartJs, leaflet, select2]).then(function () {
            loadScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js');
            var fullscreen = loadScript('https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js');
            fullscreen.then(function () {
                //everything loaded and ready to go
                if (typeof $ === 'undefined') {
                    window.$ = window.jQuery;
                }
                jQuery(document).ready(function () {
                    jQuery(thisJs).after(jQuery(__WEBPACK_IMPORTED_MODULE_0__templates_board__["a" /* default */]));
                    var bd = new __WEBPACK_IMPORTED_MODULE_1__Board__["a" /* default */](baseUrl);
                    var req = bd.loadBoard(boardId);
                    req.then(function (b) {
                        console.log('BOARD', b);
                        if (bd.active === false) {
                            jQuery('#disabled-message').show();
                        }
                        if (bd.load_full === false) {
                            jQuery('#board-main').find('box-tools').hide();
                        }
                        bd.displayLimit(b.zone_id, b.insee_limit);
                        bd.loadData();
                        var tables = bd.displayStats();
                        jQuery('#board-main').find('> .box-header > .box-title').text(b.name);
                        jQuery('#board-graph').find('.box-title').text(b.description);
                        var statsBox = jQuery('#stats').find('.box-body');
                        tables.forEach(function (t) {
                            statsBox.append(t);
                        });
                        jQuery.get(baseUrl + '/api/limit/name/' + b.insee_limit + '.' + b.zone_id).then(function (name) {
                            //Dispatch loaded event
                            var event = new Event('proxiclicMap.loaded');
                            window.dispatchEvent(event);
                            jQuery('#change-limit').append('<option selected value="' + b.insee_limit + '.' + b.zone_id + '">' + name + '</option>');
                            if (typeof $.fn.select2 !== 'undefined') {
                                jQuery('#change-limit').select2({
                                    placeholder: "Territoire",
                                    ajax: {
                                        url: baseUrl + ('/api/accounts/' + b.account_id + '/layers_insee_board/' + b.id),
                                        dataType: 'json',
                                        delay: 250,
                                        data: function data(params) {
                                            return {
                                                q: params.term
                                            };
                                        },
                                        processResults: function processResults(data, params) {
                                            return {
                                                results: data.data
                                            };
                                        },
                                        cache: true
                                    },
                                    escapeMarkup: function escapeMarkup(markup) {
                                        return markup;
                                    },
                                    minimumInputLength: 1,
                                    templateResult: function templateResult(repo) {
                                        if (repo.text || repo.loading) {
                                            return repo.text;
                                        }

                                        return "<div>" + repo.name + "<span class='label label-default'>" + repo.type + "</span> </div>";
                                    },
                                    templateSelection: function templateSelection(repo) {
                                        if (repo.text || repo.loading) {
                                            return repo.text;
                                        }
                                        return "<div>" + repo.name + "</div>";
                                    }
                                });
                            }
                        });
                    }).catch(function (err) {
                        console.log('error', err);
                        jQuery('#board-main').html('<div class="alert alert-danger">\n' + '    <h4><i class="icon fa fa-ban"></i> Impossible de charger le tableau de bord!</h4>\n' + '    Ce site web n\'est sans doute pas autorisé à intégrer ce tableau de bord.\n' + '    </div>');
                    });

                    jQuery('body').on('change', '#change-limit', function (ev) {
                        var val = jQuery(ev.currentTarget).val();
                        bd.map.clear();
                        bd.graph.destroy();
                        var d = val.split('.');
                        bd.insee = d[0];
                        bd.displayLimit(d[1], d[0]);
                        bd.loadData();
                    });
                });
            });
        });
    });
})();

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var board = "\n    <div class=\"row cartosdaasap-embed\">\n        <div class=\"col-md-12\">\n            \n            <div class=\"box box-primary\" id=\"board-main\">\n                <div class=\"box-header with-border\">\n                    <h3 class=\"box-title\"></h3>\n                    <div class=\"box-tools pull-right\">\n                        <label for=\"change-limit\" class=\"sr-only\">Changement de territoire</label>\n                        <select id=\"change-limit\" class=\"select2_ajax btn-box-tool\" style=\"width:100%\">\n\n                        </select>\n                    </div>\n                </div>\n                <!-- /.box-header -->\n                <div class=\"box-body\">\n                <div class=\"callout callout-warning\" style=\"display:none\" id=\"disabled-message\">\n                <h4>Ce tableau de bord est d\xE9sactiv\xE9</h4>\n\n                <p>Les information pr\xE9sent\xE9es ne sont peut-\xEAtre pas \xE0 jour.</p>\n            </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-6\">\n                            <div class=\"box box-success box-solid\" id=\"board-graph\">\n                                <div class=\"box-header with-border\">\n                                    <h3 class=\"box-title\">&nbsp;</h3>\n                                </div>\n                                <div class=\"box-body\">\n                                    <canvas id='myChart'></canvas>\n                                </div>\n                            </div>\n                            <div class=\"box box-danger box-solid\" id=\"stats\">\n                                <div class=\"box-header with-border\">\n                                    <h3 class=\"box-title\">Territoires les mieux couverts</h3>\n                                </div>\n                                <div class=\"box-body\">\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"col-md-6\">\n                            <div id='maCarte' class='box col-md-12' style='height: 75vh;'>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <!-- /.box-body -->\n\n                <div class=\"box-footer\">\n\n                </div>\n            </div>\n        </div>\n    </div>\n";

/* harmony default export */ __webpack_exports__["a"] = (board);

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Graph = function () {
    function Graph(item) {
        _classCallCheck(this, Graph);

        //Can be a board or a context
        // Should have indicators and indicator_names
        this.item = item;

        this.services = [];
        this.moveSteps = [];
        this.activeBar = null;
        this.controls = null;
        this.chart = null;
        this.indicator = 0;
        this.results = null;
    }

    _createClass(Graph, [{
        key: 'setControls',
        value: function setControls(controls) {
            this.controls = controls;
        }
    }, {
        key: 'setMoveSteps',
        value: function setMoveSteps(moveSteps) {
            this.moveSteps = moveSteps;
        }
    }, {
        key: 'setIndicator',
        value: function setIndicator(indicator) {
            this.indicator = indicator;
        }
    }, {
        key: 'setResults',
        value: function setResults(results) {
            this.results = results;
        }
    }, {
        key: 'display',
        value: function display() {
            var _this = this;

            console.log('RESULTS', this.results);
            var color = [];

            var colors = ['rgb(206,255,69)', 'rgb(246,255,104)', 'rgb(255,223,128)', 'rgb(255,178,134)', 'rgb(255,120,120)', 'rgb(255,91,135)'];
            var data = this.results.map(function (res, i) {
                var dataset = void 0,
                    label = void 0,
                    fullValue = [],
                    currValue = [];

                if (_this.indicator !== false) {
                    var vals = res.props;
                    var indic = _this.item.indicators[_this.indicator];
                    currValue = [res.props[indic]];

                    if (res.territoire_props && res.territoire_props[indic]) {
                        dataset = [res.props[indic] / res.territoire_props[indic] * 100];
                        fullValue = [res.territoire_props[indic]];
                    } else {
                        dataset = [vals[indic]];
                    }

                    label = _this.moveSteps[i].value;
                } else {
                    dataset = _this.item.indicators.map(function (indic) {
                        currValue.push(res.props[indic]);
                        if (res.territoire_props && res.territoire_props[indic]) {
                            fullValue.push(res.territoire_props[indic]);
                            return res.props[indic] / res.territoire_props[indic] * 100;
                        }

                        return res.props[indic];
                    });

                    label = _this.item.layers.find(function (l) {
                        return l.context_step_move_id === res.step_id || l.id === res.id;
                    }).name;
                }

                // Pick the full range of available colors
                var index = i * Math.ceil(colors.length / _this.results.length);
                if (typeof colors[index] !== 'undefined') {
                    color[i] = colors[index];
                } else {
                    color[i] = colors[5];
                }

                return {
                    label: label,
                    data: dataset,
                    fullValue: fullValue,
                    currValue: currValue,
                    backgroundColor: color[i],
                    borderWidth: 1,
                    //hoverBackgroundColor: 'rgba(236,240,245,1)',
                    borderColor: 'rgba(0, 0, 0, 0.1)'
                };
            });
            //affichage du graphe
            var labels = [__WEBPACK_IMPORTED_MODULE_0__Utils__["c" /* formatLabel */](this.item.indicator_names[this.indicator], 30)];
            if (this.indicator === false) {
                labels = this.item.indicator_names.map(function (i) {
                    return __WEBPACK_IMPORTED_MODULE_0__Utils__["c" /* formatLabel */](i, 20);
                });
            }

            console.log('labels', labels);
            console.log('data', data);
            var ctx = document.getElementById('myChart').getContext('2d');
            this.chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: data
                },
                options: {
                    scales: {
                        xAxes: [{ barPercentage: 0.5 }],
                        yAxes: [{
                            ticks: {
                                precision: 0,
                                beginAtZero: true,
                                callback: function callback(value) {
                                    return (value / 100).toLocaleString('fr-FR', { style: 'percent' });
                                }
                            }
                        }]
                    },
                    title: {
                        display: false,
                        text: "ffrf"
                    },
                    hover: {
                        mode: 'dataset'
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function label(tooltipItems, data) {
                                var multistringText = ['Pourcentage : ' + Math.round(tooltipItems.yLabel) + '%'];
                                var cv = Math.round(data.datasets[tooltipItems.datasetIndex].currValue[tooltipItems.index]);
                                var fv = 'Inconnu';
                                if (typeof data.datasets[tooltipItems.datasetIndex].fullValue !== 'undefined') {
                                    fv = Math.round(data.datasets[tooltipItems.datasetIndex].fullValue[tooltipItems.index]);
                                }

                                multistringText.push(cv + ' sur un total de ' + fv);
                                return multistringText;
                            }
                        }
                    },
                    onHover: function onHover() {
                        var activeBar = null;
                        if (_this.chart.active && _this.chart.active.length) {
                            activeBar = _this.chart.active[0]._datasetIndex;
                            for (var j = 0; j < data.length; j++) {

                                if (j <= _this.chart.active[0]._datasetIndex) {
                                    _this.chart.config.data.datasets[j].backgroundColor = color[j];
                                } else {
                                    _this.chart.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
                                }
                            }
                        }

                        if (_this.activeBar !== activeBar) {
                            _this.chart.update();
                            _this.activeBar = activeBar;
                        }
                    },
                    onClick: function onClick(ev) {
                        ev.preventDefault();
                        // Set context move step ID here
                        if (_this.chart.active && _this.chart.active.length > 0 && typeof _this.moveSteps[_this.chart.active[0]._datasetIndex] !== 'undefined') {
                            var _clickedId = _this.moveSteps[_this.chart.active[0]._datasetIndex].id;

                            var _showResult = _this.results.find(function (r) {
                                return _clickedId === r.id;
                            });
                            if (!_showResult) {
                                //find for context
                                _showResult = _this.results.find(function (r) {
                                    return _clickedId === r.step_id;
                                });
                            }

                            if (_showResult && typeof _showResult.geom !== 'undefined') {
                                // Add layer to map here
                                _this.controls.graphClicked(_showResult, _this.chart.active[0]._view.backgroundColor);
                            }

                            var _loop = function _loop(i) {
                                var clickedId = _this.moveSteps[i].id;

                                var showResult = _this.results.find(function (r) {
                                    return clickedId === r.id;
                                });
                                if (!showResult) {
                                    //find for context
                                    showResult = _this.results.find(function (r) {
                                        return clickedId === r.step_id;
                                    });
                                }

                                if (showResult && typeof showResult.geom !== 'undefined') {
                                    // Add layer to map here
                                    _this.controls.graphClicked(showResult, _this.chart.data.datasets[i].backgroundColor, false);
                                }
                            };

                            for (var i = _this.chart.active[0]._datasetIndex - 1; i >= 0; i--) {
                                _loop(i);
                            }
                        }
                        jQuery('.leaflet-marker-shadow').css('display', 'none');
                        jQuery('.leaflet-marker-icon').css('display', 'none');
                    }
                }
            });

            var clickedId = this.moveSteps[0].id;

            var showResult = this.results.find(function (r) {
                return clickedId === r.id;
            });
            if (!showResult) {
                //find for context
                showResult = this.results.find(function (r) {
                    return clickedId === r.step_id;
                });
            }
            //Make other bars grey
            for (var j = 1; j < this.chart.config.data.datasets.length; j++) {
                this.chart.config.data.datasets[j].backgroundColor = 'rgba(236,240,245,1)';
            }
            this.chart.update();

            if (showResult && typeof showResult.geom !== 'undefined') {
                // Add layer to map here
                this.controls.graphClicked(showResult, this.chart.config.data.datasets[0].backgroundColor);
            }
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            this.chart.destroy();
            this.chart = null;
        }
    }]);

    return Graph;
}();

/* harmony default export */ __webpack_exports__["a"] = (Graph);

/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Api__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Graph__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Map__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils__ = __webpack_require__(0);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }






var Board = function () {
    function Board(baseUrl) {
        _classCallCheck(this, Board);

        this.api = new __WEBPACK_IMPORTED_MODULE_0__Api__["a" /* default */](baseUrl + '/api');
    }

    _createClass(Board, [{
        key: 'loadBoard',
        value: function loadBoard(id) {
            var _this = this;

            var req = this.api.get('/boards/' + id);
            req.then(function (board) {
                _this.board = board;
                _this.graph = new __WEBPACK_IMPORTED_MODULE_1__Graph__["a" /* default */](board);
                _this.graph.setControls(_this);
                _this.insee = board.insee_limit;
                _this.graph.setIndicator(false);
                _this.map = new __WEBPACK_IMPORTED_MODULE_2__Map__["a" /* default */](board);
                _this.map.setControls(_this);
                _this.api.get('/boards/' + id + '/points').then(function (points) {
                    points.forEach(function (p) {
                        var coords = new (Function.prototype.bind.apply(L.LatLng, [null].concat(_toConsumableArray(p.geometry.coordinates.reverse()))))();
                        var marker = L.circleMarker(coords, {
                            radius: 4,
                            color: "#000000",
                            fillOpacity: 0
                        });

                        var title = Object(__WEBPACK_IMPORTED_MODULE_3__Utils__["d" /* getPopup */])(p);

                        marker.bindPopup(title);
                        marker.bringToFront();
                        _this.map.map.addLayer(marker);
                        _this.map.markers.push(marker);
                    });
                });
            });

            return req;
        }
    }, {
        key: 'loadData',
        value: function loadData() {
            var _this2 = this;

            var requests = this.board.layers.map(function (layer) {
                return _this2.api.get('/accounts/' + _this2.board.account_id + '/boards/layers/' + layer.id + '?insee=' + _this2.insee);
            });
            var result = Promise.all(requests);

            result.then(function (results) {
                var parsed = results.map(function (res) {
                    if (res && res.length > 0) {
                        var geom = null;

                        try {
                            geom = JSON.parse(res[0].geom);
                        } catch (e) {
                            console.log('json parse error', e, res[0]);
                        }

                        var props = null;
                        try {
                            props = JSON.parse(res[0].props);
                        } catch (e) {
                            console.log('json parse error', e, res[0]);
                        }

                        var territoire_props = null;
                        try {
                            territoire_props = JSON.parse(res[0].territoire_props);
                        } catch (e) {
                            console.log('json parse error', e, res[0]);
                        }
                        return {
                            id: typeof res[0].layer_id !== 'undefined' ? res[0].layer_id : res[0].step_id,
                            geom: geom,
                            props: props,
                            territoire_props: territoire_props
                        };
                    }
                    return null;
                }).filter(function (r) {
                    return r !== null;
                });
                _this2.graph.setResults(parsed);
                _this2.graph.setMoveSteps(_this2.board.layers.map(function (l) {
                    return { value: l.name, id: l.id };
                }));
                _this2.graph.display();
            });

            return result;
        }
    }, {
        key: 'displayLimit',
        value: function displayLimit(zoneId, insee) {
            var _this3 = this;

            return this.api.get('/insee/' + zoneId + '/' + insee).then(function (resp) {
                _this3.map.changeGeometry(resp, 'black', '5, 10', 0);
            }).catch(function (er) {
                console.log(er);
            });
        }
    }, {
        key: 'graphClicked',
        value: function graphClicked(layer, color) {
            var removePrevious = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

            try {
                var geom = this.map.activateLayer(layer, color, removePrevious, false);
            } catch (err) {
                console.log(err);
            }
        }
    }, {
        key: 'formatStats',
        value: function formatStats() {
            var _this4 = this;

            return this.board.layers.map(function (l) {
                var data = l.props.map(function (p, i) {
                    var percentages = _this4.board.indicators.map(function (indic) {
                        if (p.insee === l.total_props[i].insee && typeof p.props[indic] !== 'undefined' && typeof l.total_props[i].props[indic] !== 'undefined') {
                            if (l.total_props[i].props[indic] === 0) {
                                return 0;
                            }
                            return p.props[indic] / l.total_props[i].props[indic] * 100;
                        }
                        return 0;
                    });
                    return {
                        name: p.name,
                        percentages: percentages
                    };
                }).sort(function (a, b) {
                    return a.percentages[0] > b.percentages[0] ? -1 : 1;
                });
                return {
                    name: l.name,
                    info: l.info,
                    indicators: _this4.board.indicators,
                    indicator_names: _this4.board.indicator_names,
                    data: data
                };
            });
        }
    }, {
        key: 'displayStats',
        value: function displayStats() {
            var stats = this.formatStats();

            return stats.map(function (s) {
                console.log('s', s);
                var table = '<h5>' + s.name + ' <i class="fa fa-info-circle" title="' + s.info + '"></i></h5><div style="max-height: 200px;overflow:auto;max-width: 100%"><table class="table table striped" style="table-layout: fixed;max-width: 100%"><thead><tr>';
                table += '<th>&nbsp;</th>';
                table += s.indicator_names.map(function (i) {
                    return '<th style="">' + i + '</th>';
                }).join('');
                table += '</tr></thead>';
                table += s.data.map(function (item, i) {
                    var cells = item.percentages.map(function (p) {
                        return '<td>' + Math.round(p) + '%</td>';
                    }).join('');
                    return '<tr><th>' + item.name + '</th>' + cells + '</tr>';
                }).join('');
                table += "</table></div>";
                return table;
            });
        }
    }]);

    return Board;
}();

/* harmony default export */ __webpack_exports__["a"] = (Board);

/***/ })

/******/ });