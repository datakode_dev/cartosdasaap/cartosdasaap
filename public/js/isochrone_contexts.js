/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 49);
/******/ })
/************************************************************************/
/******/ ({

/***/ 49:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(50);
__webpack_require__(51);
__webpack_require__(52);
module.exports = __webpack_require__(53);


/***/ }),

/***/ 50:
/***/ (function(module, exports) {

// ajout d un input quand l utilisateur clique sur un input
$(document).ready(function () {

    $("#add_step").click(boucle);

    function boucle() {
        $("#step").append('<div class="form-group">' + '<label>Étape (mètre ou minute) </label>' + '<input type="number" class="form-control" name="steps[]" value="" placeholder="Ex: 10">' + '</div>');
    }
});

/***/ }),

/***/ 51:
/***/ (function(module, exports) {

var element = document.getElementById('table_isochrone_contexts');
if (element !== null) {
    $('#table_isochrone_contexts').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'name',
            name: 'name'
        }, {
            mData: 'limit',
            name: 'limit'
        }, {
            mData: 'iso_type',
            name: 'iso_type'
        }, {
            mData: 'service',
            name: 'service'
        }, {
            data: 'updated_at',
            fnCreatedCell: function fnCreatedCell(nTd, sData) {
                var date_test = new Date(sData.replace(/-/g, "/"));
                $(nTd).html(date_test.toLocaleDateString('fr-FR'));
            }
        }, {
            data: 'status',
            fnCreatedCell: function fnCreatedCell(nTd, sData) {
                if (typeof sData.label === 'undefined') {
                    sData.label = 'Inconnu';
                }

                var html = '<span class="label label-{{color}}"><i class="fa fa-{{icon}}" title="' + sData.label + '"></i>&nbsp;&nbsp;' + sData.label + '</span>';
                switch (sData.code) {
                    case 1:
                        // Not started
                        html = html.replace('{{icon}}', 'times').replace('{{color}}', 'info');
                        break;
                    case 2:
                        //started
                        html = html.replace('{{icon}}', 'refresh fa-spin').replace('{{color}}', 'warning');
                        break;
                    case 3:
                        //done
                        html = html.replace('{{icon}}', 'check').replace('{{color}}', 'success');
                        break;
                    case 4:
                        //error
                        html = html.replace('{{icon}}', 'exclamation-triangle').replace('{{color}}', 'danger');
                        break;
                    default:
                        html = html.replace('{{icon}}', 'question').replace('{{color}}', 'primary');
                }

                $(nTd).html(html);
            }
        }, {
            "data": "links",
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                var html = "";
                //TODO CSS
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                if (sData.show != undefined) {
                    html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                }
                if (sData.duplicate != undefined) {
                    html += "<form method='POST' action='" + sData.duplicate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a href='' title='Dupliquer' onclick='parentNode.submit();return false;'><i class='fa fa-files-o' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.archivate != undefined) {
                    html += "<form method='POST' action='" + sData.archivate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a data-toggle='tooltip' title='Archiver' href='' onclick='parentNode.submit();return false;'><i class='fa fa-archive' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.unarchivate != undefined) {
                    html += "<form method='POST' action='" + sData.unarchivate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a data-toggle='tooltip' title='Désarchiver' href='' onclick='parentNode.submit();return false;'><i class='fa fa-arrow-up' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.delete != undefined) {
                    html += "<form method='POST' action='" + sData.delete + "'>";
                    html += "<input name='_method' type='hidden' value='delete'>";
                    html += "<a href='' data-toggle='tooltip' title='Supprimer' onclick='parentNode.submit();return false;'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }
                $(nTd).html(html);
            }
        }]
    });
}

/***/ }),

/***/ 52:
/***/ (function(module, exports) {

var map = L.map('map-limite', { "dragging": false, "scrollWheelZoom": false, "doubleClickZoom": false, "boxZoom": false }).setView([45.82879925192134, 2.4609375], 4);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    id: 'osm'
}).addTo(map);

map.removeControl(map.zoomControl);

$.ajax({
    type: "GET",
    url: $('#map-limite').data('url'),
    dataType: 'json',
    success: function success(response) {
        geojsonLayer = L.geoJson(response, { fillOpacity: 0, color: 'black', weight: 1 }).addTo(map);

        map.fitBounds(geojsonLayer.getBounds());
    }
});

/***/ }),

/***/ 53:
/***/ (function(module, exports) {

//gestion de l affichage de la page show d'un contexte
$(document).ready(function () {
  /*----------------------------------*/
  /*---MOVED TO resources/js/embed ---*/
  /*----------------------------------*/

  // Wait for the footer container to ba available, and add it then

  var int = setInterval(function () {
    var footer = $('#embed-script-context').find('.box-footer');
    if (footer.length > 0) {
      console.log('found footer');
      footer.html($('#footer').text());
      clearInterval(int);
    }
    console.log('NOT found footer');
  }, 500);

  // Copy embed code
  $('body').on('click', '.btn-clipboard', function (ev) {
    var el = $(ev.currentTarget);
    var copyTextArea = document.createElement("textarea");
    copyTextArea.value = el.parents('figure').find('.embed-code').text();
    document.body.appendChild(copyTextArea);
    copyTextArea.select();
    var res = document.execCommand('copy');
    var msg = res ? 'Copié avec succès' : 'Impossible de copier automatiquement';
    el.attr('data-original-title', msg).tooltip('show');
    document.body.removeChild(copyTextArea);
  }).tooltip();
});

/***/ })

/******/ });