/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 40);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fatalError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return formatLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return pickColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return createWorker; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getPopup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__ = __webpack_require__(1);



var startLoading = function startLoading() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '#embed-script-context';

    var spinner = jQuery('.overlay');
    if (spinner.length === 0) {
        var div = jQuery(target).find('.box');

        if (target === '#embed-script-context') {
            div = jQuery(target).find('.box .box');
        }

        div.append('<div style="transform: translate3d(0px, 0px, 5px);z-index:1000;" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    spinner.html('<i class="fa fa-refresh fa-spin">');
    spinner.show();
};

var stopLoading = function stopLoading() {
    jQuery('.overlay').hide();
};

var fatalError = function fatalError(message) {
    jQuery('.overlay').html('<i class="fa fa-exclamation-triangle"></i><p class="text-muted">' + message + '</p>');
};

var formatLabel = function formatLabel(str, maxwidth) {
    var sections = [];
    if (typeof str === 'undefined') {
        return "";
    }
    var words = str.split(" ");
    var temp = "";

    words.forEach(function (item, index) {
        if (temp.length > 0) {
            var concat = temp + ' ' + item;

            if (concat.length > maxwidth) {
                sections.push(temp);
                temp = "";
            } else {
                if (index === words.length - 1) {
                    sections.push(concat);
                    return;
                } else {
                    temp = concat;
                    return;
                }
            }
        }

        if (index === words.length - 1) {
            sections.push(item);
            return;
        }

        if (item.length < maxwidth) {
            temp = item;
        } else {
            sections.push(item);
        }
    });

    return sections;
};

var pickColor = function pickColor(bgColor) {
    var color = bgColor.charAt(0) === '#' ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map(function (col) {
        if (col <= 0.03928) {
            return col / 12.92;
        }
        return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = 0.2126 * c[0] + 0.7152 * c[1] + 0.0722 * c[2];
    return L > 0.59 ? '#333' : '#fff';
};

var createWorker = function createWorker(workerUrl) {
    var worker = null;
    try {
        var blob = void 0;
        try {
            blob = new Blob(["importScripts('" + workerUrl + "');"], { "type": 'application/javascript' });
        } catch (e) {
            var builder = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder)();
            builder.append("importScripts('" + workerUrl + "');");
            blob = builder.getBlob('application/javascript');
        }
        var url = window.URL || window.webkitURL;
        var blobUrl = url.createObjectURL(blob);
        worker = new Worker(blobUrl);
    } catch (e1) {
        //if it still fails, there is nothing much we can do
    }
    return worker;
};

var getPopup = function getPopup(p) {
    p.properties = Object(__WEBPACK_IMPORTED_MODULE_0__controllers_services_pei_point_fields_map__["a" /* mapField */])([{ prop: p.properties }], p.display_fields)[0].prop;
    var title = "";
    if (p.properties && p.properties.name) {
        title = p.properties.name;
    }
    title = '<p class="title"><span class="icon" style="background-color:' + p.color + '"><i class="' + p.icon + '" style="color:' + pickColor(p.color) + '"></i></span> <span class="text" style="color:' + p.color + '">' + title + '</span></p>';
    Object.keys(p.properties).map(function (key) {
        if (key !== 'name' && key !== 'id' && key !== 'serviceId' && p.properties[key] && p.properties[key] !== '') {
            title += '<p>' + key.toUpperCase() + '&nbsp;: <strong>' + p.properties[key] + '</strong></p>';
        }
    });
    return title;
};

/***/ }),

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapField; });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var mapField = function mapField(points, fieldMap, selectionMap) {
    if (!selectionMap || (typeof selectionMap === 'undefined' ? 'undefined' : _typeof(selectionMap)) !== 'object') {
        selectionMap = fieldMap;
    }
    if (!fieldMap || (typeof fieldMap === 'undefined' ? 'undefined' : _typeof(fieldMap)) !== 'object') {
        return points.map(function (point) {
            Object.keys(point.prop).forEach(function (k) {
                var val = point.prop[k];
                if (!val) {
                    delete point.prop[k];
                }
                if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                    //Is a link, replace with link
                    val = '<a href="' + val + '" target="_blank">' + val + '</a>';
                    point.prop[k] = val;
                }
            });
            point.selectionProp = point.prop;

            return point;
        });
    }
    return points.map(function (point) {
        var newProps = {};
        Object.keys(fieldMap).forEach(function (k) {
            var val = point.prop[fieldMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newProps[k] = val;
            }
        });

        var newSelectionProps = {};
        Object.keys(selectionMap).forEach(function (k) {
            var val = point.prop[selectionMap[k]];
            if (typeof val === 'string' && (val.indexOf('http://') === 0 || val.indexOf('https://') === 0 || val.indexOf('www.') === 0)) {
                if (val.indexOf('www.') === 0) {
                    val = 'http://' + val;
                }
                //Is a link, replace with link
                val = '<a href="' + val + '" target="_blank">' + val + '</a>';
            }
            if (val) {
                newSelectionProps[k] = val;
            }
        });

        point.selectionProp = newSelectionProps;
        point.prop = newProps;
        return point;
    });
};

/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(41);
__webpack_require__(42);
module.exports = __webpack_require__(45);


/***/ }),

/***/ 41:
/***/ (function(module, exports) {

(function ($) {

	//global variables

	//this is needed to temporarily disable doubles, e.g. for uncheckAll
	var uncheckAll_doubles = false;

	//this is now default and not changable anymore
	var checkDisabled = true;

	//
	var checkboxesGroups_grayed = false;
	var checkboxesGroups = false;

	//converter
	$(document).ready(function () {
		var all_converter = $(".hummingbird-treeview-converter");
		//console.log(all_converter);
		var converter_num = 1;
		var converter_str = "";
		$.each(all_converter, function (e) {
			if (converter_num > 1) {
				converter_str = converter_num.toString();
			}
			converter_num++;

			var converter = $(this);
			//console.log(converter)

			//hide simple treeview structure
			converter.hide();

			var converter_height = converter.attr("data-height");
			var converter_scroll = converter.attr("data-scroll");
			if (converter_scroll == "true") {
				converter_scroll = "scroll";
			}

			//create new treeview container
			var tree_html = '<div id="treeview_container' + converter_str + '" class="hummingbird-treeview" style="height: ' + converter_height + '; overflow-y: ' + converter_scroll + ';">' + '<ul id="treeview' + converter_str + '" class="hummingbird-base">';

			//get treeview elements
			var tree = converter.children("li");

			//loop through the elements and create tree
			var id_num = 0;
			var id_str = "";
			var data_id = "";
			var item = "";
			var allowed = true;
			var msg = "";
			$.each(tree, function (i, e) {
				var treeText = $(this).text();

				//Regular Expression for all leading hyphens
				var regExp = /^-+/;

				//Get leading hyphens
				var numHyphenMatch = treeText.match(regExp);
				var numHyphen_nextMatch = $(this).next().text().match(regExp);

				//Get count of leading hyphens
				//Now supports using hyphens anywhere except for the first character of the label
				var numHyphen = numHyphenMatch != null ? numHyphenMatch[0].length : 0;
				var numHyphen_next = numHyphen_nextMatch != null ? numHyphen_nextMatch[0].length : 0;

				//remove leading hyphens
				treeText = treeText.replace(regExp, "");
				//extract optional id and data-id		
				if ($(this).attr("id")) {
					id_str = $(this).attr("id");
				} else {
					id_num++;
					id_str = "hum" + converter_str + "_" + id_num;
				}
				if ($(this).attr("data-id")) {
					data_id = $(this).attr("data-id");
				} else {
					data_id = treeText;
				}

				//what is this, parent, children or sibling
				//this is a parent
				//open an ul
				if (numHyphen < numHyphen_next) {
					//check format
					//down the tree it is not allowed to jump over a generation / instance
					//
					var check_diff = numHyphen_next - numHyphen;
					if (check_diff > 1) {
						msg = '<h4 style="color:red;">Error!</h4>The item after <span style="color:red;">' + treeText + ' </span>has too much hyphens, i.e. it is too far intended. Note that down the tree, the items are only allowed to be intended by one instance, i.e. one hyphen more than the item before. In contrast, up the tree arbitrarily large jumps are allowed.';
						//alert(msg);
						allowed = false;
					}
					//
					item = item + '<li>' + "\n";
					item = item + '<i class="fa fa-plus"></i>' + "\n";
					item = item + '<label>' + "\n";
					item = item + '<input id="' + id_str + '" data-id="' + data_id + '" type="checkbox" /> ' + treeText;
					item = item + '</label>' + "\n";
					item = item + '<ul>' + "\n";
					//console.log(item);
				}
				//hummingbird-end-node
				if (numHyphen == numHyphen_next) {
					item = item + '<li>' + "\n";
					item = item + '<label>' + "\n";
					item = item + '<input class="hummingbird-end-node" id="' + id_str + '" data-id="' + data_id + '" type="checkbox" /> ' + treeText;
					item = item + '</label>' + "\n";
					item = item + '</li>' + "\n";
					//console.log(item);
				}
				//this is still a hummingbird-end-node
				//after this it goes up
				//thus close this ul
				if (numHyphen > numHyphen_next) {
					item = item + '<li>' + "\n";
					item = item + '<label>' + "\n";
					item = item + '<input class="hummingbird-end-node" id="' + id_str + '" data-id="' + data_id + '" type="checkbox" /> ' + treeText;
					item = item + '</label>' + "\n";
					item = item + '</li>' + "\n";
					item = item + '</ul>' + "\n";
					//console.log(item);

					//if numHyphen - numHyphen_next > 1
					//it means that we have to close the group
					var hyphen_diff = numHyphen - numHyphen_next;
					for (var m = 2; m <= hyphen_diff; m++) {
						item = item + '</ul>' + "\n";
						item = item + '</li>' + "\n";
					}
					//
				}
			});
			item = item + '</ul></div>';
			//console.log(item)
			tree_html = tree_html + item;
			if (allowed == true) {
				//$(".hummingbird-treeview-converter").after(tree_html);
				converter.after(tree_html);
			} else {
				//$(".hummingbird-treeview-converter").after(msg);
				converter.after(msg);
			}
		});
		//end converter
	});

	$.fn.hummingbird = function (options) {

		var methodName = options;
		var args = arguments;
		var options = $.extend({}, $.fn.hummingbird.defaults, options);
		//initialisation
		if (typeof methodName == "undefined") {
			return this.each(function () {
				//-------------------options-------------------------------------------------------//

				//change symbol prefix
				//font-awesome 4.7 uses fa
				//font-awesome 5. uses fas
				if (options.SymbolPrefix != "fa") {
					$(this).find("i").removeClass("fa").addClass(options.SymbolPrefix);
				}

				//change symbols
				if (options.collapsedSymbol != "fa-plus") {
					$(this).find("i").removeClass("fa-plus").addClass(options.collapsedSymbol);
				}

				//hide checkboxes
				if (options.checkboxes == "disabled") {
					$(this).find("input:checkbox").hide();
				}

				if (options.checkboxesGroups == "disabled") {
					checkboxesGroups = true;
					//find all checkboxes which have children and disable them
					//tri-state logic will still be applied
					//this_checkbox.prop("disabled",true).parent("label").css({'color':'#c8c8c8'});
					var groups = $(this).find('input:checkbox:not(".hummingbird-end-node")');
					groups.prop("disabled", true).parent("label").css({ "cursor": "not-allowed" });
				}
				if (options.checkboxesGroups == "disabled_grayed") {
					checkboxesGroups_grayed = true;
					//find all checkboxes which have children and disable them
					//tri-state logic will still be applied
					//this_checkbox.prop("disabled",true).parent("label").css({'color':'#c8c8c8'});
					var groups = $(this).find('input:checkbox:not(".hummingbird-end-node")');
					groups.prop("disabled", true).parent("label").css({ "cursor": "not-allowed", 'color': '#c8c8c8' });
				}

				//collapseAll
				if (options.collapseAll === false) {
					$.fn.hummingbird.expandAll($(this), options.collapsedSymbol, options.expandedSymbol);
				}
				//-------------------options-------------------------------------------------------//


				//initialise doubles logic
				var doubleMode = false;
				var allVariables = new Object();
				if (options.checkDoubles) {
					$(this).find('input:checkbox.hummingbird-end-node').each(function () {
						if (allVariables[$(this).attr("data-id")]) {
							allVariables[$(this).attr("data-id")].push($(this).attr("id"));
							//console.log($(this))
						} else {
							allVariables[$(this).attr("data-id")] = [$(this).attr("id")];
						}
					});
					//console.log(JSON.stringify(allVariables));
				}

				//three state logic
				//$.fn.hummingbird.ThreeStateLogic($(this),doubleMode,allVariables,options.checkDoubles,options.checkDisabled);		
				$.fn.hummingbird.ThreeStateLogic($(this), doubleMode, allVariables, options.checkDoubles, checkDisabled);

				//expandSingle
				$(this).on("click", 'li i.' + options.collapsedSymbol, function () {
					$.fn.hummingbird.expandSingle($(this), options.collapsedSymbol, options.expandedSymbol);
				});
				//collapseSingle
				$(this).on("click", 'li i.' + options.expandedSymbol, function () {
					$.fn.hummingbird.collapseSingle($(this), options.collapsedSymbol, options.expandedSymbol);
				});
			});
		}

		//checkAll
		if (methodName == "checkAll") {
			return this.each(function () {
				$.fn.hummingbird.checkAll($(this));
			});
		}

		//ucheckAll
		if (methodName == "uncheckAll") {
			return this.each(function () {
				$.fn.hummingbird.uncheckAll($(this));
			});
		}

		//disableNode
		if (methodName == "disableNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				var state = args[1].state;
				if (typeof args[1].disableChildren !== 'undefined') {
					var disableChildren = args[1].disableChildren;
				} else {
					var disableChildren = true;
				}
				$.fn.hummingbird.disableNode($(this), attr, name, state, disableChildren);
			});
		}

		//enableNode
		if (methodName == "enableNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				var state = args[1].state;
				if (typeof args[1].enableChildren !== 'undefined') {
					var enableChildren = args[1].enableChildren;
				} else {
					var enableChildren = true;
				}
				$.fn.hummingbird.enableNode($(this), attr, name, state, enableChildren);
			});
		}

		//checkNode
		if (methodName == "checkNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				var expandParents = args[1].expandParents;
				$.fn.hummingbird.checkNode($(this), attr, name);
				if (expandParents == true) {
					$.fn.hummingbird.expandNode($(this), attr, name, expandParents, options.collapsedSymbol, options.expandedSymbol);
				}
			});
		}

		//uncheckNode
		if (methodName == "uncheckNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				var collapseChildren = args[1].collapseChildren;
				$.fn.hummingbird.uncheckNode($(this), attr, name);
				if (collapseChildren == true) {
					$.fn.hummingbird.collapseNode($(this), attr, name, collapseChildren, options.collapsedSymbol, options.expandedSymbol);
				}
			});
		}

		//setNodeColor
		if (methodName == "setNodeColor") {
			return this.each(function () {
				var attr = args[1];
				var ID = args[2];
				var color = args[3];
				$.fn.hummingbird.setNodeColor($(this), attr, ID, color);
			});
		}

		//collapseAll
		if (methodName == "collapseAll") {
			return this.each(function () {
				$.fn.hummingbird.collapseAll($(this), options.collapsedSymbol, options.expandedSymbol);
			});
		}

		//expandAll
		if (methodName == "expandAll") {
			return this.each(function () {
				$.fn.hummingbird.expandAll($(this), options.collapsedSymbol, options.expandedSymbol);
			});
		}

		//expandNode
		if (methodName == "expandNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				var expandParents = args[1].expandParents;
				$.fn.hummingbird.expandNode($(this), attr, name, expandParents, options.collapsedSymbol, options.expandedSymbol);
			});
		}

		//collapseNode
		if (methodName == "collapseNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				var collapseChildren = args[1].collapseChildren;
				$.fn.hummingbird.collapseNode($(this), attr, name, collapseChildren, options.collapsedSymbol, options.expandedSymbol);
			});
		}

		//getChecked
		if (methodName == "getChecked") {
			return this.each(function () {
				var list = args[1].list;
				var onlyEndNodes = args[1].onlyEndNodes;
				$.fn.hummingbird.getChecked($(this), list, onlyEndNodes);
			});
		}

		//getUnchecked
		if (methodName == "getUnchecked") {
			return this.each(function () {
				var list = args[1].list;
				var onlyEndNodes = args[1].onlyEndNodes;
				$.fn.hummingbird.getUnchecked($(this), list, onlyEndNodes);
			});
		}

		//addNode
		if (methodName == "addNode") {
			return this.each(function () {
				var pos = args[1].pos; //before or after
				var anchor_attr = args[1].anchor_attr; //the anchor node
				var anchor_name = args[1].anchor_name; //the anchor node
				var text = args[1].text;
				var the_id = args[1].the_id;
				var data_id = args[1].data_id;
				if (typeof args[1].end_node !== 'undefined') {
					var end_node = args[1].end_node;
				} else {
					var end_node = true;
				}
				if (typeof args[1].children !== 'undefined') {
					var children = args[1].children;
				} else {
					var children = false;
				}
				$.fn.hummingbird.addNode($(this), pos, anchor_attr, anchor_name, text, the_id, data_id, end_node, children, options.collapsedSymbol);
			});
		}

		//removeNode
		if (methodName == "removeNode") {
			return this.each(function () {
				var name = args[1].name;
				var attr = args[1].attr;
				$.fn.hummingbird.removeNode($(this), attr, name);
			});
		}

		//filter
		if (methodName == "filter") {
			return this.each(function () {
				var str = args[1].str;
				if (typeof args[1].box_disable !== 'undefined') {
					var box_disable = args[1].box_disable;
				} else {
					var box_disable = false;
				}
				if (typeof args[1].filterChildren !== 'undefined') {
					var filterChildren = args[1].filterChildren;
				} else {
					var filterChildren = true;
				}
				var onlyEndNodes = args[1].onlyEndNodes;
				$.fn.hummingbird.filter($(this), str, box_disable, onlyEndNodes, filterChildren);
			});
		}

		//search
		if (methodName == "search") {
			return this.each(function () {
				var treeview_container = args[1].treeview_container;
				var search_input = args[1].search_input;
				var search_output = args[1].search_output;
				var search_button = args[1].search_button;
				if (typeof args[1].dialog !== 'undefined') {
					var dialog = args[1].dialog;
				} else {
					var dialog = "";
				}
				if (typeof args[1].enter_key_1 !== 'undefined') {
					var enter_key_1 = args[1].enter_key_1;
				} else {
					var enter_key_1 = true;
				}
				if (typeof args[1].enter_key_2 !== 'undefined') {
					var enter_key_2 = args[1].enter_key_2;
				} else {
					var enter_key_2 = true;
				}
				if (typeof args[1].scrollOffset !== 'undefined') {
					var scrollOffset = args[1].scrollOffset;
				} else {
					var scrollOffset = false;
				}
				if (typeof args[1].onlyEndNodes !== 'undefined') {
					var onlyEndNodes = args[1].onlyEndNodes;
				} else {
					var onlyEndNodes = false;
				}
				if (typeof args[1].EnterKey !== 'undefined') {
					var EnterKey = args[1].EnterKey;
				} else {
					var EnterKey = true;
				}
				$.fn.hummingbird.search($(this), treeview_container, search_input, search_output, search_button, dialog, enter_key_1, enter_key_2, options.collapsedSymbol, options.expandedSymbol, scrollOffset, onlyEndNodes, EnterKey);
			});
		}
	};

	//options defaults
	$.fn.hummingbird.defaults = {
		SymbolPrefix: "fa",
		expandedSymbol: "fa-minus",
		collapsedSymbol: "fa-plus",
		collapseAll: true,
		checkboxes: "enabled",
		checkboxesGroups: "enabled",
		checkDoubles: false
		//checkDisabled: false,   //this is now not changeable and true always
	};

	//global vars
	var nodeDisabled = false;
	var nodeEnabled = false;

	//-------------------methods--------------------------------------------------------------------------//

	//-------------------checkAll---------------//
	$.fn.hummingbird.checkAll = function (tree) {
		tree.children("li").children("label").children("input:checkbox").prop('indeterminate', false).prop('checked', false).trigger("click");
	};

	//-------------------uncheckAll---------------//
	$.fn.hummingbird.uncheckAll = function (tree) {
		//console.log(tree.children("li").children("label").children("input:checkbox"))
		//find disabled groups
		var disabled_groups = tree.find('input:checkbox:disabled:not(.hummingbird-end-node)');
		//console.log(disabled_groups)
		//enable these
		disabled_groups.prop('disabled', false);
		//disable checking for doubles temporarily
		uncheckAll_doubles = true;
		tree.children("li").children("label").children("input:checkbox").prop('indeterminate', false).prop('checked', true).trigger("click");
		uncheckAll_doubles = false;
		//disable disabled groups again
		disabled_groups.prop('disabled', true);
		//console.log(tree.children("li").children("label").children("input:checkbox"));
	};

	//-------------------collapseAll---------------//
	$.fn.hummingbird.collapseAll = function (tree, collapsedSymbol, expandedSymbol) {
		tree.find("ul").hide();
		tree.find('.' + expandedSymbol).removeClass(expandedSymbol).addClass(collapsedSymbol);
	};

	//------------------expandAll------------------//
	$.fn.hummingbird.expandAll = function (tree, collapsedSymbol, expandedSymbol) {
		tree.find("ul").show();
		tree.find('.' + collapsedSymbol).removeClass(collapsedSymbol).addClass(expandedSymbol);
	};

	//-------------------collapseSingle---------------//
	$.fn.hummingbird.collapseSingle = function (node, collapsedSymbol, expandedSymbol) {
		node.parent("li").children("ul").hide();
		node.removeClass(expandedSymbol).addClass(collapsedSymbol);
	};

	//-------------------expandSingle---------------//
	$.fn.hummingbird.expandSingle = function (node, collapsedSymbol, expandedSymbol) {
		node.parent("li").children("ul").show();
		node.removeClass(collapsedSymbol).addClass(expandedSymbol);
	};

	//-------------------expandNode---------------//
	$.fn.hummingbird.expandNode = function (tree, attr, name, expandParents, collapsedSymbol, expandedSymbol) {
		var that_node = tree.find('input[' + attr + '=' + name + ']');
		var that_ul = that_node.parent("label").siblings("ul");
		that_ul.show().siblings("i").removeClass(collapsedSymbol).addClass(expandedSymbol);
		//expand all parents and change symbol
		if (expandParents === true) {
			that_node.parents("ul").show().siblings("i").removeClass(collapsedSymbol).addClass(expandedSymbol);
		}
	};

	//-------------------collapseNode---------------//
	$.fn.hummingbird.collapseNode = function (tree, attr, name, collapseChildren, collapsedSymbol, expandedSymbol) {
		var that_node = tree.find('input[' + attr + '=' + name + ']');
		var that_ul = that_node.parent("label").siblings("ul");
		//collapse children and change symbol
		if (collapseChildren === true) {
			that_node.parent("label").parent("li").find("ul").hide().siblings("i").removeClass(expandedSymbol).addClass(collapsedSymbol);
		} else {
			that_ul.hide().siblings("i").removeClass(expandedSymbol).addClass(collapsedSymbol);
		}
	};

	//-------------------checkNode---------------//
	$.fn.hummingbird.checkNode = function (tree, attr, name) {
		if (attr == "text") {
			name = name.trim();
			var that_nodes = tree.find('input:checkbox:not(:checked)').prop("indeterminate", false).parent('label:contains(' + name + ')');
			//console.log(that_nodes)
			that_nodes.children('input:checkbox').trigger("click");
		} else {
			tree.find('input:checkbox:not(:checked)[' + attr + '=' + name + ']').prop("indeterminate", false).trigger("click");
		}
	};

	//-------------------uncheckNode---------------//
	$.fn.hummingbird.uncheckNode = function (tree, attr, name) {
		if (attr == "text") {
			name = name.trim();
			var that_nodes = tree.find('input:checkbox:checked').prop("indeterminate", false).parent('label:contains(' + name + ')');
			//console.log(that_nodes)
			that_nodes.children('input:checkbox').trigger("click");
		} else {
			tree.find('input:checkbox:checked[' + attr + '=' + name + ']').prop("indeterminate", false).trigger("click");
		}
	};

	//-------------------removeNode---------------//
	$.fn.hummingbird.removeNode = function (tree, attr, name) {
		if (attr == "text") {
			name = name.trim();
			tree.find('input:checkbox').parent('label:contains(' + name + ')').parent('li').remove();
		} else {
			tree.find('input:checkbox[' + attr + '=' + name + ']').parent("label").parent('li').remove();
		}
	};

	//-------------------addNode---------------//
	$.fn.hummingbird.addNode = function (tree, pos, anchor_attr, anchor_name, text, the_id, data_id, end_node, children, collapsedSymbol) {
		//find the node
		if (anchor_attr == "text") {
			anchor_name = anchor_name.trim();
			var that_node = tree.find('input:checkbox').parent('label:contains(' + anchor_name + ')').parent("li");
		} else {
			var that_node = tree.find('input:checkbox[' + anchor_attr + '=' + anchor_name + ']').parent("label").parent("li");
		}
		//
		//console.log(that_node)
		//
		if (end_node) {
			var Xclass = "hummingbird-end-node";
			if (pos == "before") {
				that_node.before('<li><label><input class="' + Xclass + '" id="' + the_id + '" data-id="' + data_id + '" type="checkbox"> ' + text + '</label></li>');
			}
			if (pos == "after") {
				that_node.after('<li><label><input class="' + Xclass + '" id="' + the_id + '" data-id="' + data_id + '" type="checkbox"> ' + text + '</label></li>');
			}
		} else {
			var Xclass = "";
			//create subtree
			var subtree = "";
			$.each(children, function (i, e) {
				console.log(e);
				subtree = subtree + '<li><label><input class="' + 'hummingbird-end-node' + '" id="' + e.id + '" data-id="' + e.data_id + '" type="checkbox"> ' + e.text + '</label></li>';
			});
			if (pos == "before") {
				that_node.before('<li>' + "\n" + '<i class="fa ' + collapsedSymbol + '"></i>' + "\n" + '<label>' + "\n" + '<input class="' + Xclass + '" id="' + the_id + '" data-id="' + data_id + '" type="checkbox"> ' + text + '</label>' + "\n" + '<ul>' + "\n" + subtree + '</ul>' + "\n" + '</li>');
			}
			if (pos == "after") {
				that_node.after('<li>' + "\n" + '<i class="fa ' + collapsedSymbol + '"></i>' + "\n" + '<label>' + "\n" + '<input class="' + Xclass + '" id="' + the_id + '" data-id="' + data_id + '" type="checkbox"> ' + text + '</label>' + "\n" + '<ul>' + "\n" + subtree + '</ul>' + "\n" + '</li>');
			}
		}
		//
	};

	//-------------------filter--------------------//
	$.fn.hummingbird.filter = function (tree, str, box_disable, onlyEndNodes, filterChildren) {
		if (onlyEndNodes) {
			var entries = tree.find('input:checkbox.hummingbird-end-node');
		} else {
			var entries = tree.find('input:checkbox');
		}
		var re = new RegExp(str, 'g');
		$.each(entries, function () {
			var entry = $(this).parent("label").text();
			//if we have a match we add class to all parent li's
			if (entry.match(re)) {
				$(this).parents("li").addClass("noFilter");
				if (filterChildren == false) {
					$(this).parent("label").parent("li").find("li").addClass("noFilter");
					//console.log($(this).parent("label").parent("li").find("li"))
				}
			}
		});
		//now remove or disable all, which do not match
		if (box_disable) {
			tree.find("li").not('.noFilter').prop("disabled", true);
		} else {
			tree.find("li").not('.noFilter').remove();
		}
	};

	//-------------------disableNode---------------//
	$.fn.hummingbird.disableNode = function (tree, attr, name, state, disableChildren) {
		if (attr == "text") {
			name = name.trim();
			var that_nodes = tree.find('input:checkbox:not(:disabled)').parent('label:contains(' + name + ')');
			//console.log(that_nodes)
			var this_checkbox = that_nodes.children('input:checkbox');
		} else {
			var this_checkbox = tree.find('input:checkbox:not(:disabled)[' + attr + '=' + name + ']');
		}
		//for a disabled unchecked node, set node checked and then trigger a click to uncheck
		//for a disabled checked node, set node unchecked and then trigger a click to check
		this_checkbox.prop("checked", state === false);
		nodeDisabled = true;
		this_checkbox.trigger("click");
		//disable this node and all children
		if (disableChildren === true) {
			this_checkbox.parent("label").parent("li").find('input:checkbox').prop("disabled", true).parent("label").parent("li").css({ 'color': '#c8c8c8', "cursor": "not-allowed" });
		} else {
			//console.log(this_checkbox.prop("disabled",true).parent("label").parent("li"))
			this_checkbox.prop("disabled", true).parent("label").parent("li").css({ 'color': '#c8c8c8', "cursor": "not-allowed" });
		}
	};

	//-------------------enableNode---------------//
	$.fn.hummingbird.enableNode = function (tree, attr, name, state, enableChildren) {
		var this_checkbox = {};
		if (attr == "text") {
			name = name.trim();
			var that_nodes = tree.find('input:checkbox:disabled').parent('label:contains(' + name + ')');
			//console.log(that_nodes)
			var this_checkbox = that_nodes.children('input:checkbox');
		} else {
			this_checkbox = tree.find('input:checkbox:disabled[' + attr + '=' + name + ']');
		}
		//console.log(this_checkbox)
		//a checkbox cannot be enabled if all children are disabled AND enableChildren is false
		//get children checkboxes which are not disabled
		var children_not_disabled_sum = this_checkbox.parent("label").next("ul").children("li").children("label").children("input:checkbox:not(:disabled)").length;
		if (children_not_disabled_sum == 0 && enableChildren == false) {
			//console.log("NOW!!!!!!!!!!!!!!!!!!!!!")
			return;
		}
		//the state where a parent is enabled and all children are disabled must be forbidden

		//for a disabled unchecked node, set node checked and then trigger a click to uncheck
		//for a disabled checked node, set node unchecked and then trigger a click to check
		this_checkbox.prop("disabled", false).parent("label").parent("li").css({ 'color': '#636b6f', "cursor": "default" });
		//all parents enabled
		//no action on parents
		//this_checkbox.parent("label").parent("li").parents("li").children("label").children("input[type='checkbox']").prop("disabled",false).parents("label").parent("li").css({'color':'#636b6f',"cursor":"default"});
		//all children enabled
		if (enableChildren === true) {
			this_checkbox.parent("label").parent("li").find('input:checkbox').prop("disabled", false).parent("label").parent("li").css({ 'color': '#636b6f', "cursor": "default" });
		}
		this_checkbox.prop("checked", state === false);
		nodeEnabled = true;
		this_checkbox.trigger("click");
	};

	//--------------get all checked items------------------//
	$.fn.hummingbird.getChecked = function (tree, list, onlyEndNodes) {
		if (onlyEndNodes == true) {
			tree.find('input:checkbox.hummingbird-end-node:checked').each(function () {
				list.text.push($(this).parent("label").parent("li").text());
				list.id.push($(this).attr("id"));
				list.dataid.push($(this).attr("data-id"));
			});
		} else {
			tree.find('input:checkbox:checked').each(function () {
				list.text.push($(this).parent("label").parent("li").text());
				list.id.push($(this).attr("id"));
				list.dataid.push($(this).attr("data-id"));
			});
		}
	};
	//--------------get all checked items------------------//

	//--------------get all unchecked items------------------//
	$.fn.hummingbird.getUnchecked = function (tree, list, onlyEndNodes) {
		if (onlyEndNodes == true) {
			tree.find('input:checkbox.hummingbird-end-node:not(:checked)').each(function () {
				list.text.push($(this).parent("label").parent("li").text());
				list.id.push($(this).attr("id"));
				list.dataid.push($(this).attr("data-id"));
			});
		} else {
			tree.find('input:checkbox:not(:checked)').each(function () {
				list.text.push($(this).parent("label").parent("li").text());
				list.id.push($(this).attr("id"));
				list.dataid.push($(this).attr("data-id"));
			});
		}
	};
	//--------------get all unchecked items------------------//


	//-------------------setNodeColor---------------//
	$.fn.hummingbird.setNodeColor = function (tree, attr, ID, color) {
		tree.find('input:checkbox[' + attr + '=' + ID + ']').parent("li").css({ 'color': color });
	};

	//--------------three-state-logic----------------------//
	$.fn.hummingbird.ThreeStateLogic = function (tree, doubleMode, allVariables, checkDoubles, checkDisabled) {
		tree.find('input:checkbox').on('click', function (e) {
			//console.log($(this))
			//check / uncheck all checkboxes below that node, if they have children weather they are disabled or not
			//do nothing with end-node-checkboxes which are disabled
			//thus three state logic is applyed to groups although they are disabled and if they have children

			//all not disabled
			var nodes_below_not_disabled = $(this).parent("label").parent("li").find("input:checkbox:not(:disabled)");

			//all disabled without hummingbird-end-node
			var nodes_below_disabled_groups = $(this).parent("label").parent("li").find('input:checkbox:disabled:not(.hummingbird-end-node)');

			//add them together
			var nodes_below = nodes_below_not_disabled.add(nodes_below_disabled_groups);

			//merge
			//var nodes_below = ;


			var ids = [];
			nodes_below.each(function () {
				ids.push($(this).attr("id"));
			});
			//console.log("this");
			//console.log($(this));
			if ($(this).prop("checked")) {
				var state = true;
				var checkSiblings = "input:checkbox:not(:checked)";
				//fire event
				tree.trigger("nodeChecked", ids.join());
			} else {
				var state = false;
				var checkSiblings = "input:checkbox:checked";
				//fire event
				tree.trigger("nodeUnchecked", ids.join());
			}
			//check / uncheck all checkboxes below that node
			nodes_below.prop("indeterminate", false).prop("checked", state);
			//set all parents indeterminate and unchecked
			//if checkDisabled===false treat all disabled as if they were not there
			//do it for all if checkDisabled===true
			//if checkDisabled===false do it if it was not a disabled node, i.e. nodeDisabled===false
			//if (checkDisabled || nodeDisabled === false) {
			$(this).parent("label").parent().parents("li").children("label").children("input:checkbox").prop("indeterminate", true);
			$(this).parent("label").parent().parents("li").children("label").children("input:checkbox").prop("checked", false);

			//travel down the dom through all ul's, but not the ul directly under that, because that will be changed
			//console.log($(this).parent("label").parent("li").find("ul"))
			$(this).parent("label").siblings("ul").find("ul").map(function () {
				//console.log($(this))


				//check if children are disabled
				//console.log($(this).parent("label").next("ul").children("li").children("label").children("input:checkbox"))
				//var not_disabled_sum_children = $(this).parent("label").next("ul").children("li").children("label").children("input:checkbox:not(:disabled)").length;
				var disabled_sum_children = $(this).children("li").children("label").children("input:checkbox:disabled").length;
				//console.log("disabled_sum_children= " + disabled_sum_children)
				//if a check has happened count how many are checked
				//if an uncheck has happened count how many are unchecked
				//var checked_unchecked_sum_children = $(this).parent("label").next("ul").children("li").children("label").children(checkSiblings).length;
				//a check has happened
				var checked_sum_children = $(this).children("li").children("label").children("input:checkbox:checked").length;
				var unchecked_sum_children = $(this).children("li").children("label").children("input:checkbox:not(:checked)").length;
				//console.log("checked_sum_children= " + checked_sum_children)
				//console.log("unchecked_sum_children= " + unchecked_sum_children)
				//if all children disabled set appropriate state of this checkbox
				//This happens e.g. if all children of this box are disabled and checked, so this box is actually also checked and disabled, but because it is
				//not an end-node it can be checked, unchecked. Now a parent of this has been unchecked, thus this box is also unchecked, although all children are checked
				//thus the appropriate state has to be set again

				//there are children disabled:
				//if a check happened, all children are checked
				//if an uncheck happened all children are unchecked
				if (disabled_sum_children > 0) {
					if (checked_sum_children == 0) {
						$(this).siblings("label").children("input:checkbox").prop("checked", false);
					}
					if (unchecked_sum_children == 0) {
						$(this).siblings("label").children("input:checkbox").prop("checked", true);
					}
					if (checked_sum_children > 0 && unchecked_sum_children > 0) {
						$(this).siblings("label").children("input:checkbox").prop("checked", false).prop("indeterminate", true);
					}
				}
			});

			//}
			//travel up the DOM
			//test if siblings are all checked / unchecked / indeterminate       
			//check / uncheck parents if all siblings are checked /unchecked
			//thus, set parents checked / unchecked, if children are all checked or all unchecked with no indeterminates
			//do this for all
			//if (checkDisabled) {
			$(this).parent("label").parents("li").map(function () {
				//console.log($(this))
				var indeterminate_sum = 0;
				//number of checked if an uncheck happened or number of unchecked if a check happened
				var checked_unchecked_sum = $(this).siblings().addBack().children("label").children(checkSiblings).length;
				//check how many not disabled are here (below that parent)
				if (checkDisabled) {
					var not_disabled_sum = $(this).siblings().addBack().children("label").children("input:checkbox:not(:disabled)").length;
					//console.log("not_disabled_sum= " + not_disabled_sum)
				}

				//console.log("checkDisabled= " + checkDisabled)
				//checkDisabled means that disabled boxes are considered by the tri-state logic
				//these are the not disabled siblings together with node itself
				//var not_disabled_sum = $(this).siblings().addBack().children("label").children("input:checkbox:not(:disabled)").length;
				//console.log("not_disabled_sum= " + not_disabled_sum);
				$(this).siblings().addBack().children("label").children("input:checkbox").map(function () {
					indeterminate_sum = indeterminate_sum + $(this).prop("indeterminate");
				});
				//this is 0 if there are no checked, thus an uncheck has happened
				if (indeterminate_sum + checked_unchecked_sum == 0) {
					$(this).parent().parent().children("label").children("input:checkbox").prop("indeterminate", false);
					$(this).parent().parent().children("label").children("input:checkbox").prop("checked", state);
				}

				//if this click was together width a disableNode then, there is one more disabled child, thus not_disabled_sum--
				if (checkDisabled) {
					if (nodeDisabled == true) {
						not_disabled_sum--;
					}
					//if all children are disabled, disable also parent
					if (not_disabled_sum == 0) {
						//console.log("here")
						//console.log($(this).parent().parent().children("label").children("input[type='checkbox']"))
						//only disable parents if they are not already disabled by the checkboxesGroups options
						if (checkboxesGroups_grayed == false && checkboxesGroups == false) {
							$(this).parent().parent().children("label").children("input[type='checkbox']").prop("disabled", true).parent("label").parent("li").css({ 'color': '#c8c8c8' });
						}
					}
				}
			});
			//}

			//------------------check if this variable has doubles-----------------------//
			//------------------and click doubles if needed------------------------------//
			//------------------only if this is not a double check-----------------------//
			if (checkDoubles == true && uncheckAll_doubles == false) {
				if (doubleMode == false) {
					//do this for all checked / unchecked checkboxes below that node
					$(this).parent("label").parent("li").find("input.hummingbird-end-node[type='checkbox']").each(function () {
						//check if this node has doubles
						var L = allVariables[$(this).attr("data-id")].length;
						if (L > 1) {
							doubleMode = true;
							//if state of these checkboxes is not similar to state
							//-> trigger click
							var Zvar = allVariables[$(this).attr("data-id")];
							for (var i = 0; i < L; i++) {
								if ($("#" + Zvar[i]).prop("checked") != state) {
									$("#" + Zvar[i]).trigger("click");
								}
							}
						}
					});
					doubleMode = false;
				}
			}
			//------------------check if this variable has doubles------------------------//


			//--------------------------disabled-----------------------------------------//
			//check if this box has hummingbird-end-node children
			if (checkDisabled) {
				if ($(this).hasClass("hummingbird-end-node") === false) {
					//if this box has been checked, check if "not checked disabled" exist
					if (state === true) {
						var disabledCheckboxes = $(this).parent("label").parent("li").find("input:checkbox:not(:checked):disabled");
						var num_state_inverse_Checkboxes = $(this).parent("label").parent("li").find("input:checkbox:checked");
					}
					//if this box has been unchecked, check if "checked disabled" exist
					if (state === false) {
						var disabledCheckboxes = $(this).parent("label").parent("li").find("input:checkbox:checked:disabled");
						var num_state_inverse_Checkboxes = $(this).parent("label").parent("li").find("input:checkbox:not(:checked)");
					}
					//if this box has been checked and unchecked disabled exist
					//set this and all parents indeterminate and checked false. Setting checked false is important to make this box ready for a check
					//not if all checked or unchecked
					if (disabledCheckboxes.length > 0 && num_state_inverse_Checkboxes.length > 0) {
						//only if the boxes are enabled
						disabledCheckboxes.parent("label").parent("li").parents("li").children("label").children("input:checkbox:not(:disabled)").prop("indeterminate", true).prop("checked", state);
					}
				}
			}
			//--------------------------disabled-----------------------------------------//


			//set nodeDisabled and nodeEnabled back to false
			//so that the next clicked node can be a normal node or again a disbaled node
			nodeDisabled = false;
			nodeEnabled = false;

			//fire event
			tree.trigger("CheckUncheckDone");
		});
	};
	//--------------three-state-logic----------------------//


	//----------------------------search--------------------------------------//
	$.fn.hummingbird.search = function (tree, treeview_container, search_input, search_output, search_button, dialog, enter_key_1, enter_key_2, collapsedSymbol, expandedSymbol, scrollOffset, onlyEndNodes, EnterKey) {

		//trigger search on enter key 
		if (EnterKey == true) {
			$(document).keyup(function (e) {
				if (e.which == 13) {
					//console.log("current_page= " + enter_key_2)
					if (enter_key_1 == enter_key_2) {
						$(dialog + " #" + search_button).trigger("click");
					}
				}
			});
		}
		var first_search = true;
		var this_var_checkbox = {};
		//hide dropdown search list
		$(dialog + " #" + search_input).on("click", function (e) {
			$(dialog + " #" + search_output).hide();
		});

		$(dialog + " #" + search_button).on("click", function (e) {
			//show dropdown search list
			$(dialog + " #" + search_output).show();
			var search_str = $(dialog + " #" + search_input).val().trim();
			//empty dropdown
			$(dialog + " #" + search_output).empty();
			//loop through treeview
			var num_search_results = 0;
			if (onlyEndNodes == true) {
				var onlyEndNodes_Class = ".hummingbird-end-node";
			} else {
				var onlyEndNodes_Class = "";
			}
			tree.find('input:checkbox' + onlyEndNodes_Class).each(function () {
				if ($(this).parent().text().toUpperCase().includes(search_str.toUpperCase())) {
					//add items to dropdown
					$(dialog + " #" + search_output).append('<li id="drop_' + $(this).attr("id") + '"><a href="#">' + $(this).parent().text() + '</a></li>');
					num_search_results++;
				}
			});
			if (num_search_results == 0) {
				$(dialog + " #" + search_output).append("&nbsp; &nbsp; Nothing found");
			}
			//click on search dropdown
			$(dialog + " #" + search_output + " li").on("click", function (e) {
				//no focus on the input field to trigger the search scrolling
				e.preventDefault();
				//hide dropdown
				$(dialog + " #" + search_output).hide();
				//set value of input field
				$(dialog + " #" + search_input).val($(this).text());
				//reset color of last selection
				if (first_search == false) {
					if (this_var_checkbox.prop("disabled")) {
						this_var_checkbox.parent("label").parent("li").css({ 'color': '#c8c8c8', "cursor": "not-allowed" });
					} else {
						this_var_checkbox.parent("label").parent("li").css({ 'color': '#636b6f', "cursor": "default" });
					}
				}
				//before jumping to the hummingbird-end-node a collapse all is needed
				tree.hummingbird("collapseAll");
				//get this checkbox
				this_var_checkbox = tree.find('input[id="' + $(this).attr("id").substring(5) + '"]');
				//parent uls
				var prev_uls = this_var_checkbox.parents("ul");
				//change plus to minus
				prev_uls.closest("li").children("i").removeClass(collapsedSymbol).addClass(expandedSymbol);
				//highlight hummingbird-end-node
				this_var_checkbox.parent("label").parent("li").css({ 'color': '#f0ad4e' });
				first_search = false;
				//expand parent uls
				prev_uls.show();
				//---------------------------scrolling-----------------------------------//
				//set scroll position to zero
				if (treeview_container == "body") {
					//Chrome
					document.body.scrollTop = 0;
					//Firefox
					document.documentElement.scrollTop = 0;
				} else {
					$(dialog + " #" + treeview_container)[0].scrollTop = 0;
				}
				//get position and offset of element
				var this_var_checkbox_position = this_var_checkbox.position().top;
				this_var_checkbox_position = this_var_checkbox_position + scrollOffset;

				if (treeview_container == "body") {
					//Chrome
					document.body.scrollTop += this_var_checkbox_position;
					//Firefox
					document.documentElement.scrollTop += this_var_checkbox_position;
				} else {
					$(dialog + " #" + treeview_container)[0].scrollTop = this_var_checkbox_position;
				}
				//---------------------------scrolling-----------------------------------//
			});
			//if there is only one search result -> go to this without showing the dropdown
			if (num_search_results == 1) {
				var one_search_id = $("#" + search_output + " li").attr("id");
				$("#" + one_search_id).trigger("click");
			}
		});
	};
	//----------------------------search--------------------------------------//
})(jQuery);

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_fields__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__embed_Utils__ = __webpack_require__(0);



var nodes;
function showSiren() {

    var tv = $("#treeview");
    tv.show().hummingbird();
    tv.on("CheckUncheckDone", function () {
        var List = { "id": [], "dataid": [], "text": [] };
        tv.hummingbird("getChecked", { list: List, onlyEndNodes: false });
        var L = List.id.length;
        console.log(List);

        // get only the final nodes (the NAF code has 5 characters)
        nodes = List.id.map(function (item) {
            return item.replace('xnode-', '');
        }).filter(function (item) {
            return item.length === 5;
        }).join('&refine.apen700=');
        displayNumPoints();
    });
}
function displayNumPoints() {
    var limit = $('select[name=limit]').val();
    var buffer = $('input[name=buffer]').val();

    if (!limit) {
        return;
    }

    if (!nodes) {
        console.log('nodes are empty');
        $("#num-points-div").hide();
        return;
    }

    $.get('/api/sirene/limit/' + limit + '/' + buffer).then(function (res) {
        if (res.length > 0) {
            var points = res[0].map(function (p) {
                return '(' + p.reverse().join(',') + ')';
            });

            var no = nodes.split('&refine.apen700=').map(function (n) {
                return n.substr(0, 2) + '.' + n.substr(2);
            }).join('&refine.activiteprincipaleunitelegale=');

            var url = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=economicref-france-sirene-v3&facet=libellecommuneetablissement&geofilter.polygon=' + points + '&refine.activiteprincipaleunitelegale=' + no + '&disjunctive.activiteprincipaleunitelegale=true&rows=2&fields=prenom1unitelegale,nomunitelegale,denominationusuelleetablissement,geolocetablissement';

            //var url = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene&facet=libcom&geofilter.polygon=${points}&refine.apen700=${nodes}&disjunctive.apen700=true&rows=2&fields=l1_normalisee,coordonnees`;

            $.get(url).then(function (res) {
                $('#num-points').html(res.nhits);
                var pdiv = $("#num-points-div");
                pdiv.show();

                pdiv.find('p').remove();
                if (res.nhits > 1000 && res.nhits <= 5000) {
                    pdiv.removeClass().addClass('alert alert-warning');
                } else if (res.nhits > 5000) {
                    pdiv.removeClass().addClass('alert alert-danger').append('<p>Seuls les 5000 premiers points seront récupérés</p>');
                } else {
                    $("#num-points-div").removeClass().addClass('alert alert-info');
                }
            });
        }
    });
}

$(document).ready(function () {
    console.log('ready');

    Object(__WEBPACK_IMPORTED_MODULE_0__edit_fields__["a" /* editFields */])($, window.service);

    $('#choose-fields #data').sortable({ cancel: ".title-line", handle: "#sortable-arrow" });

    $("input[name='access']").on('change', function () {
        if ($("input[name=access]:checked").val() === 'all' || $("input[name=access]:checked").val() === 'none') {
            $('#members_access').hide();
        } else {
            $('#members_access').show();
        }
    });

    $('select[name=limit]').on('change', function () {
        displayNumPoints();
    });

    $('input[name=buffer]').on('change', function () {
        displayNumPoints();
    });

    var refresh_days = $('input[name=day_between_refresh_url], input[name=day_between_refresh_siren]');
    refresh_days.on('change', function () {

        $('.num-days-div').remove();
        var num_days = parseInt($(this).val(), 10);
        console.log('changed', num_days);
        if (num_days < 7) {
            $(this).after('<div class="alert alert-danger num-days-div" style="margin-top: 1em;" >La mise à jour automatique implique la relance des calculs. Il est donc recommandé de l\'utiliser avec précaution pour ne pas pas saturer le serveur</div>');
        }
    });
    $('#sirene > label').on('click', function () {
        var box = $('#sirene');
        box.find('.inputs').toggle();
        box.find('>label>i.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
    });

    var existingUrl = $('#source_url').val();
    if (typeof existingUrl !== 'undefined' && existingUrl.indexOf('/api/sirene') >= 0) {
        //We have an existing Sirene URL, parse it and set proper data
        //$('#source_url').val('');
        showSiren();
        var a = document.createElement('a');
        a.href = existingUrl;
        $('#source_url').val('');
        refresh_days.val('');
        var searchParams = decodeURIComponent(a.search.replace('?', '')).split('&');

        searchParams.forEach(function (param) {
            var ar = param.split('=');
            if (ar[0].indexOf('nodes') === 0) {
                $("#treeview").hummingbird("checkNode", { attr: "id", name: "xnode-" + ar[1], expandParents: true });
            }
            if (ar[0].indexOf('buffer') === 0) {
                $('input[name=buffer]').val(ar[1]);
            }
            if (ar[0].indexOf('limit') === 0) {
                $.get('/api/limit/name/' + ar[1]).then(function (res) {
                    var newOption = new Option(res, ar[1], false, false);
                    $('select[name=limit]').append(newOption).trigger('change');
                });
            }
        });
    }

    //je recupere les donnees du fichier icons_json
    var list_ensemble = $('.search_ico_color').data("json");

    //je liste tous les ensembles à partir du fichier json pour pouvoir les choisir dans le modal choisir ensemble
    $.each(list_ensemble, function (index, value) {
        $('.search_ico_color').append('<option value="' + index + '">' + value.name + '</option>');
    });

    try {
        $('.search_ico_color').length && $('.search_ico_color').select2();
    } catch (e) {
        console.log('select2 not init');
    }

    $('#valid_ico_color').on("click", function () {
        //je recupere l'id de l'ensemble choisi
        var color_index = $('.search_ico_color').val();
        //je recupere le code couleur et l icone de cet ensemble

        var color_name = list_ensemble[color_index].color_name;
        var icon_name = list_ensemble[color_index].icon_name;

        // je sauvegarde les caracteristiques de l ensemble dans le formulaire de creation de service
        $('input[name="color"]').val(color_name);
        $('input[name="ico"]').val(icon_name);

        $('#setBackground').css('background-color', color_name);
        $('#setBackground .custom').remove();
        $('#icon-preview').removeClass().addClass(icon_name);
    });

    //choix du type d'importation
    $('#type1').on('click', function () {
        $('#hidden1').show();
        $('.num-days-div').remove();
        $('#hidden2').hide();
        $('#hidden3').hide();

        $('#colorIconContent > div').hide();
        $('#colorIconContent #waitingColor').show();
    });

    $('#type2').on('click', function () {
        $('#hidden2').show();
        $('.num-days-div').remove();
        $('#hidden1').hide();
        $('#hidden3').hide();

        $('#colorIconContent > div').hide();
        $('#colorIconContent #waitingColor').show();
    });

    $('#type3').on('click', function () {
        $('#hidden3').show();
        showSiren();
        $('.num-days-div').remove();
        $('#hidden1').hide();
        $('#hidden2').hide();

        // SHOW MARKER INPUTS
        $('#colorIconContent > div').hide();
        $('#colorIconContent .marker').show();
    });
    if (!$('#type1').prop("checked")) {
        $('#hidden1').hide();
    }
    if (!$('#type2').prop("checked")) {
        $('#hidden2').hide();
    }
    if (!$('#type3').prop("checked")) {
        $('#hidden3').hide();
    }

    $('.iconpicker-item').on('click', function () {
        $('#setBackground .custom').remove();
    });

    function customIconAction(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        if (!deleteMode) {
            var name = ev.currentTarget.getAttribute("name");

            var currentImg = ev.target.localName === "button" ? ev.target.children[0] : ev.target;

            if ($('#setBackground .custom')) {
                $('#setBackground .custom').remove();
            }
            $('input[name="ico"]').val(name);
            $('#icon-preview').removeClass();
            var html = currentImg.outerHTML;

            $('#setBackground').append(html);
        }
    }
    // ------ DELETE ICONS JS ------
    var deleteMode = false;
    $('#deleteIcon').click(function (ev) {
        ev.stopPropagation();
        ev.preventDefault();
        deleteMode = true;
        // SET RED ICONS
        $("button[class^='container-']").css("border", "1px solid red");
        $("button[class^='container-']").css("background", "#ffe6e6");
        $('#deleteIcon').hide();
        $('#cancelDeleteIcon').show();

        $('.svg').click(function (ev) {
            console.log('name', ev.currentTarget.getAttribute("name"), $('#ico'), $('#ico').val());

            if (deleteMode && confirm('Souhaitez-vous supprimer l\'ic\xF4ne ' + ev.currentTarget.getAttribute("name") + ' ?') == true) {
                // DELETE ICON
                var url = window.location.href.split('/services/');
                console.log('delete icon ', url[0] + '/deleteIconSvg/' + ev.currentTarget.getAttribute("id"));
                $.ajax({
                    type: 'GET',
                    url: url[0] + '/deleteIconSvg/' + ev.currentTarget.getAttribute("id"),
                    success: function success(res) {
                        console.log('ico delete res', res);
                        // RENVOIE 'OK' si supprimé ou 'KO' si pas supprimé 
                        if (res === 'OK') {
                            ev.currentTarget.style.display = 'none';
                            $('#custom-icon').append('<div class="alert alert-success alert-dismissible show" role="alert">L\'ic\xF4ne a \xE9t\xE9 supprim\xE9e.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        } else {
                            // SHOW ALERT
                            $('#custom-icon').append('<div class="alert alert-warning alert-dismissible show" role="alert">L\'ic\xF4ne est associ\xE9 \xE0 un service, et n\'a donc pas pu \xEAtre supprim\xE9e.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    },
                    error: function error(err) {
                        console.log('Couldn\'t delete icon ' + JSON.stringify(err));
                    }
                });

                // DELETE ICON IF IN ICON INPUT
                if ($('#ico').val() === ev.currentTarget.getAttribute("name")) {
                    $('.listing_ico_color #ico').val('');
                    $('#ico').val('');
                    // DELETE ICON PREVIEW
                    $('#setBackground .custom').remove();
                }
            }
        });
    });
    $('#cancelDeleteIcon').click(function (ev) {
        deleteMode = false;
        ev.stopPropagation();
        ev.preventDefault();
        $("button[class^='container-']").css("border", "none");
        $("button[class^='container-']").css("background", "white");
        $('#cancelDeleteIcon').hide();
        $('#deleteIcon').show();
    });
    $('#addIconButton').click(function (ev) {
        deleteMode = false;
        $("button[class^='container-']").css("border", "none");
        $("button[class^='container-']").css("background", "white");
        $('#cancelDeleteIcon').hide();
        $('#deleteIcon').show();
    });
    // TO FILTER ICON SHOWN
    $('#iconSearch').on('input', function (ev) {
        $("#iconList button").each(function (id, button) {
            console.log(id, button, button.getAttribute("name"));
            button.style.display = 'none';
            if (button.getAttribute("name").toUpperCase().includes(ev.target.value.toUpperCase())) {
                button.style.display = 'inline-block';
            }
        });
    });

    // ----- SAVE ACCOUNT SVG ICON -----
    $('#saveIcon').click(function (ev) {
        ev.stopPropagation();
        ev.preventDefault();

        var data = new FormData();
        jQuery.each(jQuery('#iconSVG')[0].files, function (i, file) {
            data.append('svg', file);
            data.append('name', $('#iconName').val());
        });

        var url = window.location.href.split('/services/');
        $.ajax({
            type: 'POST',
            url: url[0] + '/save_custom_icon',
            beforeSend: function beforeSend(xhr) {
                xhr.setRequestHeader('X-CSRF-TOKEN', $('input[name="_token"]').attr('value'));
            },
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function success(svg) {
                console.log('ico', svg);

                // ----- ADD ICON IN LIST + SET CLICK -----
                var img = $('<img class="custom" style="width:18px;height:18px;">');
                img.attr('src', window.location.origin + '/storage/' + svg.path);
                var container = $('<button id="' + svg.id + '" class="container-' + svg.id + ' svg" name=' + svg.name + '></button>');
                container.appendTo('#iconList');
                img.appendTo('.container-' + svg.id);

                $('.container-' + svg.id).on("click", function (ev) {
                    return customIconAction(ev);
                });
            },
            error: function error(err) {
                console.log('Couldn\'t get current research_context ' + JSON.stringify(err));
            }
        });
    });
    // ----- FETCH ACCOUNT SVG ICONS -----
    $.ajax({
        type: 'GET',
        url: window.location.href.split('/services')[0] + '/custom_icon',
        // http://cartosdasaap.local/accounts/2/custom_icon

        success: function success(imgs) {
            console.log('icons', imgs, window.location);

            // ----- SHOW ICONS -----
            imgs.forEach(function (svg) {
                var img = $('<img class="custom" style="width:18px;height:18px;">');
                img.attr('src', window.location.origin + '/storage/' + svg.path);
                var container = $('<button id="' + svg.id + '" class="container-' + svg.id + ' svg" name=' + svg.name + '></button>');
                container.appendTo('#iconList');
                img.appendTo('.container-' + svg.id);
            });
            // ----- ICONS ON CLICK -----
            $('.svg').on("click", function (ev) {
                return customIconAction(ev);
            });

            console.log("$('.svg')", $('.svg'));
        },
        error: function error(err) {
            console.log('Couldn\'t get current svg ' + JSON.stringify(err));
        }
    });
    // TO DO
    // SET COLOR INPUT WITH DATA LOADED
});

if ($('.previewInput')) {
    // SET PREVIEW ICON FONT AWESOME
    !$('#icon-preview').data("path") && $('#icon-preview').addClass($('.previewInput').val());
    // SET PREVIEW ICON SVG
    $('#icon-preview').data("svg") && $('#icon-preview').data("path") && $('#icon-preview').html('<img class="custom" src="' + window.location.origin + '/storage/' + $('#icon-preview').data("path") + '" style="width: 15px;"/>');
    // SET PREVIEW BACKGROUND
    $('#setBackground').css('background-color', $('.colorPreviewInput').val());
    $('.colorPreviewInput').val() && $('#icon-preview').css('color', Object(__WEBPACK_IMPORTED_MODULE_1__embed_Utils__["e" /* pickColor */])($('.colorPreviewInput').val()));
    $('#icon-picker').removeClass('has-error'); // REMOVE HAS ERROR ON SERVICE EDIT WHEN THERE IS SVG FILE
}
$('.colorPreviewInput').change(function () {
    $('#setBackground').css('background-color', $('.colorPreviewInput').val());
    $('.colorPreviewInput').val() && $('#icon-preview').css('color', Object(__WEBPACK_IMPORTED_MODULE_1__embed_Utils__["e" /* pickColor */])($('.colorPreviewInput').val()));
});
$('.previewInput').change(function () {
    $('#setBackground').css('background-color', $('.colorPreviewInput').val());
});
// HIDE COLOR CHOICE WHEN LOADING
console.log('hide here');
$('#colorIconContent > div').hide();
$('#colorIconContent #waitingColor').show();

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editFields; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_papaparse__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_papaparse___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_papaparse__);
var _this = this;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }



// option format
var template = '<option value="{{name}}" {{selected}}>{{name}}</option>';

// Define each form line format
var line = '<div class="form-group row" style="background: #f2f2f2;margin: 1rem;padding: 1rem; position: relative;">' + '<div id="sortable-arrow" class="col-sm-1 p-0" style="padding: 0; font-size:16px; margin-top: 2rem;padding-left: 2rem;">' + '<i class="fa fa-arrows-v" aria-hidden="true"></i>' + '</div>' + '<div class="col-sm-2" style="padding: 0;">' + '<label for="field1">Nom du champ</label>' + '<input id="field{{id}}" class="form-control form-control-sm" name="field-names[{{id}}]" type="text" value="{{value}}" />' + '</div>' + '<div class="col-sm-3">' + '<label for="field{{id}}-value">Valeur du champ</label>' + '<select id="field1-value" class="select2 form-control form-control-sm" name="field-values[{{id}}]" style="width:100%">{{options}}</select>' + '</div>' + '<div class="col-sm-2" style="margin-top: 2em;">' + '<input type="checkbox" name="infobulle-values[{{id}}]" {{checkedinfo}} style="margin-right: 10px"/>' + '<label for="infobulle-values[{{id}}]">Infobulle</label>' + '</div>' + '<div class="col-sm-2" style="margin-top: 2em;">' + '<input type="checkbox" name="selection-values[{{id}}]" {{checkedselection}} style="margin-right: 10px"/>' + '<label for="selection-values[{{id}}]">Selection</label>' + '</div>' + '<div class="col-sm-2" style="margin-top: 2em;">' + '<input type="checkbox" name="recherche-values[{{id}}]" class="search-checkbox" {{checkedrecherche}} style="margin-right: 10px"/>' + '<label for="recherche-values[{{id}}]">Recherche</label>' + '</div>' + '</div>';

// Specific line for the title field chooser, with hidden input
var titleLine = '<div id="title-line" class="form-group row">' + '<div class="col-sm-6">' + '<input id="dataType" name="dataType" type="text" value="" disabled  class="hidden" />' + '<input id="field{{id}}" name="field-names[{{id}}]" type="hidden" value="name" />' + '<input type="hidden" name="selection-values[{{id}}]" value="true" />' + '<input type="hidden" name="infobulle-values[{{id}}]" value="true" />' + '<input type="hidden" name="recherche-values[{{id}}]" value="true" />' + '</div>' + '<div class="col-sm-6">' + '<label for="field{{id}}-value">Titre du point (Champ à utiliser)</label>' + '<select id="field1-value" class="select2" name="field-values[{{id}}]" style="width:100%">{{options}}</select>' + '</div>' + '</div>';

var displayForm = function displayForm(data) {
    console.log('displayForm');
    console.log('data----', data);
    // Get the properties from the first point
    var props = data.features[0].properties;

    if (Array.isArray(props)) {
        props = props[0];
    }
    // Get the properties names
    var keys = Object.keys(props);

    var options = keys.map(function (k) {
        return template.replace(new RegExp('{{name}}', 'g'), k);
    }).join('');
    var id = 1;
    $('#choose-fields #data').append(titleLine.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options));
    titleLine.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options).appendTo();

    console.log('data', data);

    id = 2;
    $('#choose-fields #data').append(line.replace('{{checkedselection}}', 'checked').replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options).replace('{{value}}', '')).append('<div class="row" style="margin: 1em;"><div class="col-sm-12 text-center"><button class="btn btn-warning add-field"><i class="fa fa-plus-circle"></i> Ajouter un champ</button></div></div>');
    $('#choose-fields #data').find('.select2').select2();

    $('body').on('click', '.add-field', function (ev) {
        ev.preventDefault();
        id++;
        console.log($(ev.currentTarget).parent().closest('.row'));
        $(ev.currentTarget).parent().closest('.row').before(line.replace('{{checkedselection}}', 'checked').replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{value}}', '').replace('{{options}}', options));
        if ($('#choose-fields #data').length) {
            $('#choose-fields #data').find('.select2').select2();
        }
    });
};

var makeContainer = function makeContainer() {
    var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'file';

    if ($('#choose-fields #data').length >= 0) {
        $('#choose-fields #data').remove();
        $('body').off('click', '.add-field');
    }

    var t = 1;
    if (type === 'sirene') {
        t = 3;
    } else if (type === 'url') {
        t = 2;
    }

    $('#type' + t).prop("checked", true);
    $('#hidden' + t).show();
    $('#hidden' + t).append('<div class="form-group" id="choose-fields" style="margin-top: 1em;"><div class="box-header with-border"><div id="data"><h3 class="box-title">Champs à afficher</h3></div></div></div>');
};

var editFields = function editFields($, existingService) {
    if (existingService) {

        // HIDE en attente de donnée
        $('#colorIconContent #waitingColor').hide();

        if (existingService.path) {
            console.log('existingService', existingService);
            makeContainer('file');
            (existingService.geometry === 'Polygon' || existingService.geometry === 'MultiPolygon') && $('#colorIconContent .polygone').show();
            (existingService.geometry === 'LineString' || existingService.geometry === 'MultiLineString') && $('#colorIconContent .line').show();
            (existingService.geometry === null || existingService.geometry === 'Point' || existingService.geometry === 'MultiPoint') && $('#colorIconContent .marker').show();
        } else if (existingService.source_url && existingService.source_url.indexOf("api/sirene") >= 0) {
            makeContainer('sirene');
            $('#colorIconContent .marker').show();
        } else {
            makeContainer('url');
            (existingService.geometry === 'Polygon' || existingService.geometry === 'MultiPolygon') && $('#colorIconContent .polygone').show();
            (existingService.geometry === 'LineString' || existingService.geometry === 'MultiLineString') && $('#colorIconContent .line').show();
            (existingService.geometry === null || existingService.geometry === 'Point' || existingService.geometry === 'MultiPoint') && $('#colorIconContent .marker').show();
        }

        try {
            console.log('existingService', existingService);
            var fields = JSON.parse(existingService.display_fields);

            if (!fields) {
                fields = {};
            }

            var selectionFields = JSON.parse(existingService.display_selection);

            if (!selectionFields) {
                selectionFields = {};
            }

            var rechercheFields = JSON.parse(existingService.display_recherche);

            if (!rechercheFields) {
                rechercheFields = {};
            }

            var availableFields = existingService.availableFields;
            if (typeof existingService.availableFields === 'string') {
                availableFields = JSON.parse(existingService.availableFields);
            }

            // Get all used field names.
            var allFields = Object.keys(fields).concat(Object.keys(selectionFields));
            allFields = allFields.concat(Object.keys(rechercheFields));

            //Remove duplicates
            allFields = [].concat(_toConsumableArray(new Set(allFields)));

            var allFieldsObject = {};

            //create object with all field names as keys and target field as value
            allFields.forEach(function (fk) {
                allFieldsObject[fk] = typeof fields[fk] !== 'undefined' ? fields[fk] : typeof selectionFields[fk] !== 'undefined' ? selectionFields[fk] : rechercheFields[fk];
                // allFieldsObject[fk] = typeof fields[fk] !== 'undefined' ? fields[fk] : selectionFields[fk];
            });

            var keys = Object.keys(availableFields);
            var id = 1;

            var allOptions = keys.map(function (opk) {
                //Create options and mark as not selected
                return template.replace(new RegExp('{{name}}', 'g'), opk).replace('{{selected}}', '');
            }).join('');

            Object.keys(allFieldsObject).forEach(function (k) {
                var options = keys.map(function (opk) {
                    //Create options and mark as seleted
                    return template.replace(new RegExp('{{name}}', 'g'), opk).replace('{{selected}}', opk === allFieldsObject[k] ? 'selected' : '');
                }).join('');
                //Append line
                var parsedLine = line.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options).replace('{{value}}', k);

                // if (allFieldsObject[k] === allFieldsObject['name']) { // PREVENT NAME TO BE DRAG AND DROPPED
                //     parsedLine = parsedLine.replace("form-group row", "form-group row title-line");
                // }

                if (k === 'name') {
                    parsedLine = titleLine.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{options}}', options);
                    parsedLine = parsedLine.replace("form-group row", "form-group row title-line");
                }

                if (typeof fields[k] !== 'undefined') {
                    parsedLine = parsedLine.replace('{{checkedinfo}}', 'checked');
                }
                if (typeof selectionFields[k] !== 'undefined') {
                    parsedLine = parsedLine.replace('{{checkedselection}}', 'checked');
                }
                if (typeof rechercheFields[k] !== 'undefined') {
                    parsedLine = parsedLine.replace('{{checkedrecherche}}', 'checked');
                }
                $('#choose-fields #data').append(parsedLine);
                id++;
            });

            $('#choose-fields #data').append('<div class="row" style="margin: 1em;"><div class="col-sm-12 text-center"><button class="btn btn-warning add-field"><i class="fa fa-plus-circle"></i> Ajouter un champ</button></div></div>');

            try {
                $('#choose-fields #data').find('.select2').select2();
            } catch (e) {
                console.log('select2 not init');
            }

            $('body').on('click', '.add-field', function (ev) {
                ev.preventDefault();
                id++;
                console.log('seb', $(ev.currentTarget).parents('.row').first());
                $(ev.currentTarget).parents('.row').first().before(line.replace(new RegExp('{{id}}', 'g'), id.toString()).replace('{{checkedselection}}', 'checked').replace('{{value}}', '').replace('{{options}}', allOptions));
                $('#choose-fields #data').find('.select2').select2();
            });
        } catch (er) {
            console.log('could not read existing display fields', er);
        }

        // SET DATA TYPE INPUT
        console.log('SET DATA INPUT LOAD', existingService);
        existingService.geometry && $('#dataType').val(existingService.geometry);
        !existingService.geometry && $('#dataType').val('Point');
    }

    $('#source_url').on('change', function (ev) {
        console.log('change url source');
        var input = $(ev.currentTarget);
        var val = input.val();
        // Change to use proxy.
        var url = window.location.protocol + '//' + window.location.host + '/p?url=' + encodeURIComponent(val);
        $.get(url).then(function (res) {
            var data = res;
            console.log(data);
            if (data !== null && typeof data.features === 'undefined' || data.features.length === 0) {
                try {
                    data = JSON.parse(data);
                    if (data !== null && typeof data.features === 'undefined' || data.features.length === 0) {
                        alert("L'adresse donnée ne contient pas de geojson valide");
                        return;
                    }
                } catch (e) {
                    alert("L'adresse donnée ne contient pas de geojson valide");
                    return;
                }
            }

            $('#colorIconContent > div').hide();
            var featureType = data.features[0].geometry.type;
            console.log('featureType', featureType);
            data.features.forEach(function (feature) {
                if (feature.geometry.type !== featureType) {
                    // IF NOT ONLY ONE TYPE PER FILE, CLEAN INPUT
                    $('#alert_data_box').append('<div class="alert alert-warning alert-dismissible show" role="alert">Le fichier n\'a pas pu \xEAtre import\xE9, il contient plusieurs types de g\xE9om\xE9trie.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    data = null;
                    $('input[name=source_url]').val('');
                    // HIDE COLOR CHOICE
                    $('#colorIconContent #waitingColor').show();
                    return;
                }
            });
            // IF ONLY ONE DATA TYPE -> UPDATE COLOR DIV
            if (data && (featureType === "Point" || featureType === "MultiPoint")) {

                setTimeout(function () {
                    $('#dataType').val('Point');
                }, 500);
                $('#colorIconContent .marker').show();
            } else if (data && (featureType === "Polygon" || featureType === "MultiPolygon")) {
                setTimeout(function () {
                    $('#dataType').val('Polygon');
                }, 500);
                $('#colorIconContent .polygone').show();
            } else if (data && (featureType === "LineString" || featureType === "MultiLineString")) {
                setTimeout(function () {
                    $('#dataType').val('LineString');
                }, 500);
                $('#colorIconContent .line').show();
            }

            makeContainer('url');
            console.log('launch display');
            displayForm(data);
        }).fail(function (e) {
            alert("impossible de charger cette adresse (" + e.responseJSON.message + ")");
            return console.log('could not read url', e);
        });
    });

    $('input[name=fileToUpload]').on('change', function (ev) {
        console.log('change file source');

        var input = $(ev.currentTarget);
        var f = input.get(0).files;
        if (f.length === 0) {
            return;
        }
        var file = f[0];
        console.log(file);
        var reader = new FileReader();
        reader.onload = function (res) {
            var data = null;

            try {
                data = JSON.parse(res.target.result);
                console.log('data file', data);
                // CHECK DATA TYPE (POINT / POLYGONE / LINE) TO ADAPT COLOR
                var featureType = data.features[0].geometry.type;
                console.log('featureType', featureType);
                data.features.forEach(function (feature) {
                    if (feature.geometry.type !== featureType) {
                        // IF NOT ONLY ONE TYPE PER FILE, CLEAN INPUT
                        $('#alert_data_box').append('<div class="alert alert-warning alert-dismissible show" role="alert">Le fichier n\'a pas pu \xEAtre import\xE9, il contient plusieurs types de g\xE9om\xE9trie.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        data = null;
                        $('input[name=fileToUpload]').val('');
                        // HIDE COLOR CHOICE
                        $('#colorIconContent > div').hide();
                        $('#colorIconContent #waitingColor').show();
                        return;
                    }
                });
                // IF ONLY ONE DATA TYPE -> UPDATE COLOR DIV

                if (data && (featureType === "Point" || featureType === "MultiPoint")) {
                    $('#colorIconContent > div').hide(); // IN IF TO NOT HIDE waitongColor when false file
                    $('#colorIconContent .marker').show();
                } else if (data && (featureType === "Polygon" || featureType === "MultiPolygon")) {
                    $('#colorIconContent > div').hide();
                    $('#colorIconContent .polygone').show();
                } else if (data && (featureType === "LineString" || featureType === "MultiLineString")) {
                    $('#colorIconContent > div').hide();
                    $('#colorIconContent .line').show();
                }
                setTimeout(function () {
                    $('#dataType').val(featureType);
                }, 500);
                console.log('SET DATA TYPE', featureType);

                // ------
            } catch (err) {
                //Data is not geojson, try CSV
                data = __WEBPACK_IMPORTED_MODULE_0_papaparse___default.a.parse(res.target.result, {
                    header: true
                });
                if (typeof data.data !== 'undefined') {
                    data.features = data.data.map(function (item) {
                        return { properties: item };
                    });
                }
            }

            if (data !== null && typeof data.features === 'undefined' || data.features.length === 0) {
                return;
            }

            makeContainer('file');

            displayForm(data);
        };

        reader.readAsText(file);
    });

    $('body').on('click', '.search-checkbox', function (ev) {
        console.log('research checked', ev, $(_this));
        $('#search-number-selected').remove();
        if ($('.search-checkbox:checked').length > 10) {
            $('#choose-fields #data').append('<div id="search-number-selected" class="alert alert-warning" role="alert">Attention, plus de 10 champs de recherche sont s\xE9lectionn\xE9s (Actuellement ' + $('.search-checkbox:checked').length + ')<br>Ils ne seront pas tous affich\xE9s dans le contexte de recherche associ\xE9.</div>');
        }
    });
    if ($('.search-checkbox:checked').length > 10) {
        $('#choose-fields #data').append('<div id="search-number-selected" class="alert alert-warning" role="alert">Attention, plus de 10 champs de recherche sont s\xE9lectionn\xE9s (Actuellement ' + $('.search-checkbox:checked').length + ')<br>Ils ne seront pas tous affich\xE9s dans le contexte de recherche associ\xE9.</div>');
    }
};

/***/ }),

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* @license
Papa Parse
v5.3.0
https://github.com/mholt/PapaParse
License: MIT
*/
!function(e,t){ true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):"object"==typeof module&&"undefined"!=typeof exports?module.exports=t():e.Papa=t()}(this,function s(){"use strict";var f="undefined"!=typeof self?self:"undefined"!=typeof window?window:void 0!==f?f:{};var n=!f.document&&!!f.postMessage,o=n&&/blob:/i.test((f.location||{}).protocol),a={},h=0,b={parse:function(e,t){var i=(t=t||{}).dynamicTyping||!1;U(i)&&(t.dynamicTypingFunction=i,i={});if(t.dynamicTyping=i,t.transform=!!U(t.transform)&&t.transform,t.worker&&b.WORKERS_SUPPORTED){var r=function(){if(!b.WORKERS_SUPPORTED)return!1;var e=(i=f.URL||f.webkitURL||null,r=s.toString(),b.BLOB_URL||(b.BLOB_URL=i.createObjectURL(new Blob(["(",r,")();"],{type:"text/javascript"})))),t=new f.Worker(e);var i,r;return t.onmessage=m,t.id=h++,a[t.id]=t}();return r.userStep=t.step,r.userChunk=t.chunk,r.userComplete=t.complete,r.userError=t.error,t.step=U(t.step),t.chunk=U(t.chunk),t.complete=U(t.complete),t.error=U(t.error),delete t.worker,void r.postMessage({input:e,config:t,workerId:r.id})}var n=null;b.NODE_STREAM_INPUT,"string"==typeof e?n=t.download?new l(t):new p(t):!0===e.readable&&U(e.read)&&U(e.on)?n=new g(t):(f.File&&e instanceof File||e instanceof Object)&&(n=new c(t));return n.stream(e)},unparse:function(e,t){var n=!1,m=!0,_=",",v="\r\n",s='"',a=s+s,i=!1,r=null,o=!1;!function(){if("object"!=typeof t)return;"string"!=typeof t.delimiter||b.BAD_DELIMITERS.filter(function(e){return-1!==t.delimiter.indexOf(e)}).length||(_=t.delimiter);("boolean"==typeof t.quotes||"function"==typeof t.quotes||Array.isArray(t.quotes))&&(n=t.quotes);"boolean"!=typeof t.skipEmptyLines&&"string"!=typeof t.skipEmptyLines||(i=t.skipEmptyLines);"string"==typeof t.newline&&(v=t.newline);"string"==typeof t.quoteChar&&(s=t.quoteChar);"boolean"==typeof t.header&&(m=t.header);if(Array.isArray(t.columns)){if(0===t.columns.length)throw new Error("Option columns is empty");r=t.columns}void 0!==t.escapeChar&&(a=t.escapeChar+s);"boolean"==typeof t.escapeFormulae&&(o=t.escapeFormulae)}();var h=new RegExp(q(s),"g");"string"==typeof e&&(e=JSON.parse(e));if(Array.isArray(e)){if(!e.length||Array.isArray(e[0]))return f(null,e,i);if("object"==typeof e[0])return f(r||u(e[0]),e,i)}else if("object"==typeof e)return"string"==typeof e.data&&(e.data=JSON.parse(e.data)),Array.isArray(e.data)&&(e.fields||(e.fields=e.meta&&e.meta.fields),e.fields||(e.fields=Array.isArray(e.data[0])?e.fields:u(e.data[0])),Array.isArray(e.data[0])||"object"==typeof e.data[0]||(e.data=[e.data])),f(e.fields||[],e.data||[],i);throw new Error("Unable to serialize unrecognized input");function u(e){if("object"!=typeof e)return[];var t=[];for(var i in e)t.push(i);return t}function f(e,t,i){var r="";"string"==typeof e&&(e=JSON.parse(e)),"string"==typeof t&&(t=JSON.parse(t));var n=Array.isArray(e)&&0<e.length,s=!Array.isArray(t[0]);if(n&&m){for(var a=0;a<e.length;a++)0<a&&(r+=_),r+=y(e[a],a);0<t.length&&(r+=v)}for(var o=0;o<t.length;o++){var h=n?e.length:t[o].length,u=!1,f=n?0===Object.keys(t[o]).length:0===t[o].length;if(i&&!n&&(u="greedy"===i?""===t[o].join("").trim():1===t[o].length&&0===t[o][0].length),"greedy"===i&&n){for(var d=[],l=0;l<h;l++){var c=s?e[l]:l;d.push(t[o][c])}u=""===d.join("").trim()}if(!u){for(var p=0;p<h;p++){0<p&&!f&&(r+=_);var g=n&&s?e[p]:p;r+=y(t[o][g],p)}o<t.length-1&&(!i||0<h&&!f)&&(r+=v)}}return r}function y(e,t){if(null==e)return"";if(e.constructor===Date)return JSON.stringify(e).slice(1,25);!0===o&&"string"==typeof e&&null!==e.match(/^[=+\-@].*$/)&&(e="'"+e);var i=e.toString().replace(h,a),r="boolean"==typeof n&&n||"function"==typeof n&&n(e,t)||Array.isArray(n)&&n[t]||function(e,t){for(var i=0;i<t.length;i++)if(-1<e.indexOf(t[i]))return!0;return!1}(i,b.BAD_DELIMITERS)||-1<i.indexOf(_)||" "===i.charAt(0)||" "===i.charAt(i.length-1);return r?s+i+s:i}}};if(b.RECORD_SEP=String.fromCharCode(30),b.UNIT_SEP=String.fromCharCode(31),b.BYTE_ORDER_MARK="\ufeff",b.BAD_DELIMITERS=["\r","\n",'"',b.BYTE_ORDER_MARK],b.WORKERS_SUPPORTED=!n&&!!f.Worker,b.NODE_STREAM_INPUT=1,b.LocalChunkSize=10485760,b.RemoteChunkSize=5242880,b.DefaultDelimiter=",",b.Parser=w,b.ParserHandle=i,b.NetworkStreamer=l,b.FileStreamer=c,b.StringStreamer=p,b.ReadableStreamStreamer=g,f.jQuery){var d=f.jQuery;d.fn.parse=function(o){var i=o.config||{},h=[];return this.each(function(e){if(!("INPUT"===d(this).prop("tagName").toUpperCase()&&"file"===d(this).attr("type").toLowerCase()&&f.FileReader)||!this.files||0===this.files.length)return!0;for(var t=0;t<this.files.length;t++)h.push({file:this.files[t],inputElem:this,instanceConfig:d.extend({},i)})}),e(),this;function e(){if(0!==h.length){var e,t,i,r,n=h[0];if(U(o.before)){var s=o.before(n.file,n.inputElem);if("object"==typeof s){if("abort"===s.action)return e="AbortError",t=n.file,i=n.inputElem,r=s.reason,void(U(o.error)&&o.error({name:e},t,i,r));if("skip"===s.action)return void u();"object"==typeof s.config&&(n.instanceConfig=d.extend(n.instanceConfig,s.config))}else if("skip"===s)return void u()}var a=n.instanceConfig.complete;n.instanceConfig.complete=function(e){U(a)&&a(e,n.file,n.inputElem),u()},b.parse(n.file,n.instanceConfig)}else U(o.complete)&&o.complete()}function u(){h.splice(0,1),e()}}}function u(e){this._handle=null,this._finished=!1,this._completed=!1,this._halted=!1,this._input=null,this._baseIndex=0,this._partialLine="",this._rowCount=0,this._start=0,this._nextChunk=null,this.isFirstChunk=!0,this._completeResults={data:[],errors:[],meta:{}},function(e){var t=E(e);t.chunkSize=parseInt(t.chunkSize),e.step||e.chunk||(t.chunkSize=null);this._handle=new i(t),(this._handle.streamer=this)._config=t}.call(this,e),this.parseChunk=function(e,t){if(this.isFirstChunk&&U(this._config.beforeFirstChunk)){var i=this._config.beforeFirstChunk(e);void 0!==i&&(e=i)}this.isFirstChunk=!1,this._halted=!1;var r=this._partialLine+e;this._partialLine="";var n=this._handle.parse(r,this._baseIndex,!this._finished);if(!this._handle.paused()&&!this._handle.aborted()){var s=n.meta.cursor;this._finished||(this._partialLine=r.substring(s-this._baseIndex),this._baseIndex=s),n&&n.data&&(this._rowCount+=n.data.length);var a=this._finished||this._config.preview&&this._rowCount>=this._config.preview;if(o)f.postMessage({results:n,workerId:b.WORKER_ID,finished:a});else if(U(this._config.chunk)&&!t){if(this._config.chunk(n,this._handle),this._handle.paused()||this._handle.aborted())return void(this._halted=!0);n=void 0,this._completeResults=void 0}return this._config.step||this._config.chunk||(this._completeResults.data=this._completeResults.data.concat(n.data),this._completeResults.errors=this._completeResults.errors.concat(n.errors),this._completeResults.meta=n.meta),this._completed||!a||!U(this._config.complete)||n&&n.meta.aborted||(this._config.complete(this._completeResults,this._input),this._completed=!0),a||n&&n.meta.paused||this._nextChunk(),n}this._halted=!0},this._sendError=function(e){U(this._config.error)?this._config.error(e):o&&this._config.error&&f.postMessage({workerId:b.WORKER_ID,error:e,finished:!1})}}function l(e){var r;(e=e||{}).chunkSize||(e.chunkSize=b.RemoteChunkSize),u.call(this,e),this._nextChunk=n?function(){this._readChunk(),this._chunkLoaded()}:function(){this._readChunk()},this.stream=function(e){this._input=e,this._nextChunk()},this._readChunk=function(){if(this._finished)this._chunkLoaded();else{if(r=new XMLHttpRequest,this._config.withCredentials&&(r.withCredentials=this._config.withCredentials),n||(r.onload=y(this._chunkLoaded,this),r.onerror=y(this._chunkError,this)),r.open(this._config.downloadRequestBody?"POST":"GET",this._input,!n),this._config.downloadRequestHeaders){var e=this._config.downloadRequestHeaders;for(var t in e)r.setRequestHeader(t,e[t])}if(this._config.chunkSize){var i=this._start+this._config.chunkSize-1;r.setRequestHeader("Range","bytes="+this._start+"-"+i)}try{r.send(this._config.downloadRequestBody)}catch(e){this._chunkError(e.message)}n&&0===r.status&&this._chunkError()}},this._chunkLoaded=function(){4===r.readyState&&(r.status<200||400<=r.status?this._chunkError():(this._start+=this._config.chunkSize?this._config.chunkSize:r.responseText.length,this._finished=!this._config.chunkSize||this._start>=function(e){var t=e.getResponseHeader("Content-Range");if(null===t)return-1;return parseInt(t.substring(t.lastIndexOf("/")+1))}(r),this.parseChunk(r.responseText)))},this._chunkError=function(e){var t=r.statusText||e;this._sendError(new Error(t))}}function c(e){var r,n;(e=e||{}).chunkSize||(e.chunkSize=b.LocalChunkSize),u.call(this,e);var s="undefined"!=typeof FileReader;this.stream=function(e){this._input=e,n=e.slice||e.webkitSlice||e.mozSlice,s?((r=new FileReader).onload=y(this._chunkLoaded,this),r.onerror=y(this._chunkError,this)):r=new FileReaderSync,this._nextChunk()},this._nextChunk=function(){this._finished||this._config.preview&&!(this._rowCount<this._config.preview)||this._readChunk()},this._readChunk=function(){var e=this._input;if(this._config.chunkSize){var t=Math.min(this._start+this._config.chunkSize,this._input.size);e=n.call(e,this._start,t)}var i=r.readAsText(e,this._config.encoding);s||this._chunkLoaded({target:{result:i}})},this._chunkLoaded=function(e){this._start+=this._config.chunkSize,this._finished=!this._config.chunkSize||this._start>=this._input.size,this.parseChunk(e.target.result)},this._chunkError=function(){this._sendError(r.error)}}function p(e){var i;u.call(this,e=e||{}),this.stream=function(e){return i=e,this._nextChunk()},this._nextChunk=function(){if(!this._finished){var e,t=this._config.chunkSize;return t?(e=i.substring(0,t),i=i.substring(t)):(e=i,i=""),this._finished=!i,this.parseChunk(e)}}}function g(e){u.call(this,e=e||{});var t=[],i=!0,r=!1;this.pause=function(){u.prototype.pause.apply(this,arguments),this._input.pause()},this.resume=function(){u.prototype.resume.apply(this,arguments),this._input.resume()},this.stream=function(e){this._input=e,this._input.on("data",this._streamData),this._input.on("end",this._streamEnd),this._input.on("error",this._streamError)},this._checkIsFinished=function(){r&&1===t.length&&(this._finished=!0)},this._nextChunk=function(){this._checkIsFinished(),t.length?this.parseChunk(t.shift()):i=!0},this._streamData=y(function(e){try{t.push("string"==typeof e?e:e.toString(this._config.encoding)),i&&(i=!1,this._checkIsFinished(),this.parseChunk(t.shift()))}catch(e){this._streamError(e)}},this),this._streamError=y(function(e){this._streamCleanUp(),this._sendError(e)},this),this._streamEnd=y(function(){this._streamCleanUp(),r=!0,this._streamData("")},this),this._streamCleanUp=y(function(){this._input.removeListener("data",this._streamData),this._input.removeListener("end",this._streamEnd),this._input.removeListener("error",this._streamError)},this)}function i(_){var a,o,h,r=Math.pow(2,53),n=-r,s=/^\s*-?(\d+\.?|\.\d+|\d+\.\d+)(e[-+]?\d+)?\s*$/,u=/(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/,t=this,i=0,f=0,d=!1,e=!1,l=[],c={data:[],errors:[],meta:{}};if(U(_.step)){var p=_.step;_.step=function(e){if(c=e,m())g();else{if(g(),0===c.data.length)return;i+=e.data.length,_.preview&&i>_.preview?o.abort():(c.data=c.data[0],p(c,t))}}}function v(e){return"greedy"===_.skipEmptyLines?""===e.join("").trim():1===e.length&&0===e[0].length}function g(){if(c&&h&&(k("Delimiter","UndetectableDelimiter","Unable to auto-detect delimiting character; defaulted to '"+b.DefaultDelimiter+"'"),h=!1),_.skipEmptyLines)for(var e=0;e<c.data.length;e++)v(c.data[e])&&c.data.splice(e--,1);return m()&&function(){if(!c)return;function e(e,t){U(_.transformHeader)&&(e=_.transformHeader(e,t)),l.push(e)}if(Array.isArray(c.data[0])){for(var t=0;m()&&t<c.data.length;t++)c.data[t].forEach(e);c.data.splice(0,1)}else c.data.forEach(e)}(),function(){if(!c||!_.header&&!_.dynamicTyping&&!_.transform)return c;function e(e,t){var i,r=_.header?{}:[];for(i=0;i<e.length;i++){var n=i,s=e[i];_.header&&(n=i>=l.length?"__parsed_extra":l[i]),_.transform&&(s=_.transform(s,n)),s=y(n,s),"__parsed_extra"===n?(r[n]=r[n]||[],r[n].push(s)):r[n]=s}return _.header&&(i>l.length?k("FieldMismatch","TooManyFields","Too many fields: expected "+l.length+" fields but parsed "+i,f+t):i<l.length&&k("FieldMismatch","TooFewFields","Too few fields: expected "+l.length+" fields but parsed "+i,f+t)),r}var t=1;!c.data.length||Array.isArray(c.data[0])?(c.data=c.data.map(e),t=c.data.length):c.data=e(c.data,0);_.header&&c.meta&&(c.meta.fields=l);return f+=t,c}()}function m(){return _.header&&0===l.length}function y(e,t){return i=e,_.dynamicTypingFunction&&void 0===_.dynamicTyping[i]&&(_.dynamicTyping[i]=_.dynamicTypingFunction(i)),!0===(_.dynamicTyping[i]||_.dynamicTyping)?"true"===t||"TRUE"===t||"false"!==t&&"FALSE"!==t&&(function(e){if(s.test(e)){var t=parseFloat(e);if(n<t&&t<r)return!0}return!1}(t)?parseFloat(t):u.test(t)?new Date(t):""===t?null:t):t;var i}function k(e,t,i,r){var n={type:e,code:t,message:i};void 0!==r&&(n.row=r),c.errors.push(n)}this.parse=function(e,t,i){var r=_.quoteChar||'"';if(_.newline||(_.newline=function(e,t){e=e.substring(0,1048576);var i=new RegExp(q(t)+"([^]*?)"+q(t),"gm"),r=(e=e.replace(i,"")).split("\r"),n=e.split("\n"),s=1<n.length&&n[0].length<r[0].length;if(1===r.length||s)return"\n";for(var a=0,o=0;o<r.length;o++)"\n"===r[o][0]&&a++;return a>=r.length/2?"\r\n":"\r"}(e,r)),h=!1,_.delimiter)U(_.delimiter)&&(_.delimiter=_.delimiter(e),c.meta.delimiter=_.delimiter);else{var n=function(e,t,i,r,n){var s,a,o,h;n=n||[",","\t","|",";",b.RECORD_SEP,b.UNIT_SEP];for(var u=0;u<n.length;u++){var f=n[u],d=0,l=0,c=0;o=void 0;for(var p=new w({comments:r,delimiter:f,newline:t,preview:10}).parse(e),g=0;g<p.data.length;g++)if(i&&v(p.data[g]))c++;else{var m=p.data[g].length;l+=m,void 0!==o?0<m&&(d+=Math.abs(m-o),o=m):o=m}0<p.data.length&&(l/=p.data.length-c),(void 0===a||d<=a)&&(void 0===h||h<l)&&1.99<l&&(a=d,s=f,h=l)}return{successful:!!(_.delimiter=s),bestDelimiter:s}}(e,_.newline,_.skipEmptyLines,_.comments,_.delimitersToGuess);n.successful?_.delimiter=n.bestDelimiter:(h=!0,_.delimiter=b.DefaultDelimiter),c.meta.delimiter=_.delimiter}var s=E(_);return _.preview&&_.header&&s.preview++,a=e,o=new w(s),c=o.parse(a,t,i),g(),d?{meta:{paused:!0}}:c||{meta:{paused:!1}}},this.paused=function(){return d},this.pause=function(){d=!0,o.abort(),a=U(_.chunk)?"":a.substring(o.getCharIndex())},this.resume=function(){t.streamer._halted?(d=!1,t.streamer.parseChunk(a,!0)):setTimeout(t.resume,3)},this.aborted=function(){return e},this.abort=function(){e=!0,o.abort(),c.meta.aborted=!0,U(_.complete)&&_.complete(c),a=""}}function q(e){return e.replace(/[.*+?^${}()|[\]\\]/g,"\\$&")}function w(e){var O,D=(e=e||{}).delimiter,I=e.newline,T=e.comments,A=e.step,L=e.preview,F=e.fastMode,z=O=void 0===e.quoteChar?'"':e.quoteChar;if(void 0!==e.escapeChar&&(z=e.escapeChar),("string"!=typeof D||-1<b.BAD_DELIMITERS.indexOf(D))&&(D=","),T===D)throw new Error("Comment character same as delimiter");!0===T?T="#":("string"!=typeof T||-1<b.BAD_DELIMITERS.indexOf(T))&&(T=!1),"\n"!==I&&"\r"!==I&&"\r\n"!==I&&(I="\n");var M=0,j=!1;this.parse=function(a,t,i){if("string"!=typeof a)throw new Error("Input must be a string");var r=a.length,e=D.length,n=I.length,s=T.length,o=U(A),h=[],u=[],f=[],d=M=0;if(!a)return R();if(F||!1!==F&&-1===a.indexOf(O)){for(var l=a.split(I),c=0;c<l.length;c++){if(f=l[c],M+=f.length,c!==l.length-1)M+=I.length;else if(i)return R();if(!T||f.substring(0,s)!==T){if(o){if(h=[],b(f.split(D)),S(),j)return R()}else b(f.split(D));if(L&&L<=c)return h=h.slice(0,L),R(!0)}}return R()}for(var p=a.indexOf(D,M),g=a.indexOf(I,M),m=new RegExp(q(z)+q(O),"g"),_=a.indexOf(O,M);;)if(a[M]!==O)if(T&&0===f.length&&a.substring(M,M+s)===T){if(-1===g)return R();M=g+n,g=a.indexOf(I,M),p=a.indexOf(D,M)}else{if(-1!==p&&(p<g||-1===g)){if(!(p<_)){f.push(a.substring(M,p)),M=p+e,p=a.indexOf(D,M);continue}var v=x(p,_,g);if(v&&void 0!==v.nextDelim){p=v.nextDelim,_=v.quoteSearch,f.push(a.substring(M,p)),M=p+e,p=a.indexOf(D,M);continue}}if(-1===g)break;if(f.push(a.substring(M,g)),C(g+n),o&&(S(),j))return R();if(L&&h.length>=L)return R(!0)}else for(_=M,M++;;){if(-1===(_=a.indexOf(O,_+1)))return i||u.push({type:"Quotes",code:"MissingQuotes",message:"Quoted field unterminated",row:h.length,index:M}),E();if(_===r-1)return E(a.substring(M,_).replace(m,O));if(O!==z||a[_+1]!==z){if(O===z||0===_||a[_-1]!==z){-1!==p&&p<_+1&&(p=a.indexOf(D,_+1)),-1!==g&&g<_+1&&(g=a.indexOf(I,_+1));var y=w(-1===g?p:Math.min(p,g));if(a[_+1+y]===D){f.push(a.substring(M,_).replace(m,O)),a[M=_+1+y+e]!==O&&(_=a.indexOf(O,M)),p=a.indexOf(D,M),g=a.indexOf(I,M);break}var k=w(g);if(a.substring(_+1+k,_+1+k+n)===I){if(f.push(a.substring(M,_).replace(m,O)),C(_+1+k+n),p=a.indexOf(D,M),_=a.indexOf(O,M),o&&(S(),j))return R();if(L&&h.length>=L)return R(!0);break}u.push({type:"Quotes",code:"InvalidQuotes",message:"Trailing quote on quoted field is malformed",row:h.length,index:M}),_++}}else _++}return E();function b(e){h.push(e),d=M}function w(e){var t=0;if(-1!==e){var i=a.substring(_+1,e);i&&""===i.trim()&&(t=i.length)}return t}function E(e){return i||(void 0===e&&(e=a.substring(M)),f.push(e),M=r,b(f),o&&S()),R()}function C(e){M=e,b(f),f=[],g=a.indexOf(I,M)}function R(e){return{data:h,errors:u,meta:{delimiter:D,linebreak:I,aborted:j,truncated:!!e,cursor:d+(t||0)}}}function S(){A(R()),h=[],u=[]}function x(e,t,i){var r={nextDelim:void 0,quoteSearch:void 0},n=a.indexOf(O,t+1);if(t<e&&e<n&&(n<i||-1===i)){var s=a.indexOf(D,n);if(-1===s)return r;n<s&&(n=a.indexOf(O,n+1)),r=x(s,n,i)}else r={nextDelim:e,quoteSearch:t};return r}},this.abort=function(){j=!0},this.getCharIndex=function(){return M}}function m(e){var t=e.data,i=a[t.workerId],r=!1;if(t.error)i.userError(t.error,t.file);else if(t.results&&t.results.data){var n={abort:function(){r=!0,_(t.workerId,{data:[],errors:[],meta:{aborted:!0}})},pause:v,resume:v};if(U(i.userStep)){for(var s=0;s<t.results.data.length&&(i.userStep({data:t.results.data[s],errors:t.results.errors,meta:t.results.meta},n),!r);s++);delete t.results}else U(i.userChunk)&&(i.userChunk(t.results,n,t.file),delete t.results)}t.finished&&!r&&_(t.workerId,t.results)}function _(e,t){var i=a[e];U(i.userComplete)&&i.userComplete(t),i.terminate(),delete a[e]}function v(){throw new Error("Not implemented.")}function E(e){if("object"!=typeof e||null===e)return e;var t=Array.isArray(e)?[]:{};for(var i in e)t[i]=E(e[i]);return t}function y(e,t){return function(){e.apply(t,arguments)}}function U(e){return"function"==typeof e}return o&&(f.onmessage=function(e){var t=e.data;void 0===b.WORKER_ID&&t&&(b.WORKER_ID=t.workerId);if("string"==typeof t.input)f.postMessage({workerId:b.WORKER_ID,results:b.parse(t.input,t.config),finished:!0});else if(f.File&&t.input instanceof File||t.input instanceof Object){var i=b.parse(t.input,t.config);i&&f.postMessage({workerId:b.WORKER_ID,results:i,finished:!0})}}),(l.prototype=Object.create(u.prototype)).constructor=l,(c.prototype=Object.create(u.prototype)).constructor=c,(p.prototype=Object.create(p.prototype)).constructor=p,(g.prototype=Object.create(u.prototype)).constructor=g,b});

/***/ }),

/***/ 45:
/***/ (function(module, exports) {

var element = document.getElementById('table_services_pei');
if (element === null) {} else {
    $('#table_services_pei').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'name',
            name: 'name'
        }, {
            mData: 'info',
            name: 'info'
        }, {
            mData: 'count',
            name: 'count'
        }, {
            data: 'updated_at',
            fnCreatedCell: function fnCreatedCell(nTd, sData) {
                var date_test = new Date(sData.replace(/-/g, "/"));
                $(nTd).html(date_test.toLocaleDateString('fr-FR'));
            }
        }, {
            mData: 'updated_by',
            name: 'updated_by'
        }, {
            data: 'status',
            fnCreatedCell: function fnCreatedCell(nTd, sData) {
                if (typeof sData.label === 'undefined') {
                    sData.label = 'Inconnu';
                }

                var html = '<span class="label label-{{color}}"><i class="fa fa-{{icon}}" title="' + sData.label + '"></i>&nbsp;&nbsp;' + sData.label + '</span>';
                switch (sData.code) {
                    case 1:
                        // Not started
                        html = html.replace('{{icon}}', 'times').replace('{{color}}', 'info');
                        break;
                    case 2:
                        //started
                        html = html.replace('{{icon}}', 'refresh fa-spin').replace('{{color}}', 'warning');
                        break;
                    case 3:
                        //done
                        html = html.replace('{{icon}}', 'check').replace('{{color}}', 'success');
                        break;
                    case 4:
                        //error
                        html = html.replace('{{icon}}', 'exclamation-triangle').replace('{{color}}', 'danger');
                        break;
                    default:
                        html = html.replace('{{icon}}', 'question').replace('{{color}}', 'primary');
                }

                $(nTd).html(html);
            }
        }, {
            "data": "links",
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                var html = "";
                console.log(sData);
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                if (sData.show != undefined) {
                    html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                }
                if (sData.duplicate != undefined) {
                    html += "<form method='POST' action='" + sData.duplicate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a title='Dupliquer' href='' onclick='parentNode.submit();return false;'><i class='fa fa-files-o' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }
                if (sData.archivate != undefined) {
                    html += "<form method='POST' action='" + sData.archivate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a href='' data-toggle='tooltip' title='Archiver' onclick='parentNode.submit();return false;'><i class='fa fa-archive' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.unarchivate != undefined) {
                    html += "<form method='POST' action='" + sData.unarchivate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a href='' data-toggle='tooltip' title='Désarchiver' onclick='parentNode.submit();return false;'><i class='fa fa-arrow-up' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.delete != undefined) {
                    html += "<form method='POST' action='" + sData.delete + "'>";
                    html += "<input name='_method' type='hidden' value='delete'>";
                    html += "<a href='' data-toggle='tooltip' title='Supprimer' onclick='parentNode.submit();return false;'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }
                $(nTd).html(html);
            }
        }]
    });
}

/***/ })

/******/ });