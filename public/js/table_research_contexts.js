/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 54);
/******/ })
/************************************************************************/
/******/ ({

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(55);


/***/ }),

/***/ 55:
/***/ (function(module, exports) {

var element = document.getElementById('table_research_contexts');
if (element !== null) {
    $('#table_research_contexts').DataTable({
        ajax: element.dataset.url,
        language: window.data_table_lang,
        columns: [{
            mData: 'name',
            name: 'name'
        }, {
            "data": "links",
            "fnCreatedCell": function fnCreatedCell(nTd, sData, oData, iRow, iCol) {
                var html = "";
                //TODO CSS
                if (sData.edit != undefined) {
                    html += "<a data-toggle='tooltip' title='Modifier' href='" + sData.edit + "'><i class='fa fa-pencil' aria-hidden='true'></i></a> ";
                }
                if (sData.show != undefined) {
                    html += "<a data-toggle='tooltip' title='Voir' href='" + sData.show + "'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                }

                if (sData.duplicate != undefined) {
                    html += "<form method='POST' action='" + sData.duplicate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a href='' title='Dupliquer' onclick='parentNode.submit();return false;'><i class='fa fa-files-o' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }
                if (sData.archivate != undefined) {
                    html += "<form method='POST' action='" + sData.archivate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a href='' data-toggle='tooltip' title='Archiver' onclick='parentNode.submit();return false;'><i class='fa fa-archive' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.unarchivate != undefined) {
                    html += "<form method='POST' action='" + sData.unarchivate + "'>";
                    html += "<input name='_method' type='hidden' value='put'>";
                    html += "<a href='' data-toggle='tooltip' title='Désarchiver' onclick='parentNode.submit();return false;'><i class='fa fa-arrow-up' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }

                if (sData.delete != undefined) {
                    html += "<form method='POST' action='" + sData.delete + "'>";
                    html += "<input name='_method' type='hidden' value='delete'>";
                    html += "<a href='' data-toggle='tooltip' title='Supprimer' onclick='parentNode.submit();return false;'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                    html += "<input type='hidden' name='_token' value=" + sData.token + ">";
                    html += "</form >";
                }
                $(nTd).html(html);
            }
        }]
    });
}

/***/ })

/******/ });