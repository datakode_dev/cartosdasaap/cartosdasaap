<?php
$treenaf_all = <<<EOT
[
   {
      "code":"A",
      "libelle":"[A] Agriculture, sylviculture et pêche",
      "children":[
         {
            "code":"01",
            "code_section":"A",
            "libelle":"[01] Culture et production animale, chasse et services annexes",
            "children":[
               {
                  "code":"011",
                  "libelle":"[011] Cultures non permanentes",
                  "children":[
                     {
                        "code":"0111",
                        "libelle":"[0111] Culture de céréales (à l'exception du riz), de légumineuses et de graines oléagineuses",
                        "children":[
                           {
                              "code":"0111Z",
                              "libelle":"[0111Z] Culture de céréales (à l'exception du riz), de légumineuses et de graines oléagineuses"
                           }
                        ]
                     },
                     {
                        "code":"0112",
                        "libelle":"[0112] Culture du riz",
                        "children":[
                           {
                              "code":"0112Z",
                              "libelle":"[0112Z] Culture du riz"
                           }
                        ]
                     },
                     {
                        "code":"0113",
                        "libelle":"[0113] Culture de légumes, de melons, de racines et de tubercules",
                        "children":[
                           {
                              "code":"0113Z",
                              "libelle":"[0113Z] Culture de légumes, de melons, de racines et de tubercules"
                           }
                        ]
                     },
                     {
                        "code":"0114",
                        "libelle":"[0114] Culture de la canne à sucre",
                        "children":[
                           {
                              "code":"0114Z",
                              "libelle":"[0114Z] Culture de la canne à sucre"
                           }
                        ]
                     },
                     {
                        "code":"0115",
                        "libelle":"[0115] Culture du tabac",
                        "children":[
                           {
                              "code":"0115Z",
                              "libelle":"[0115Z] Culture du tabac"
                           }
                        ]
                     },
                     {
                        "code":"0116",
                        "libelle":"[0116] Culture de plantes à fibres",
                        "children":[
                           {
                              "code":"0116Z",
                              "libelle":"[0116Z] Culture de plantes à fibres"
                           }
                        ]
                     },
                     {
                        "code":"0119",
                        "libelle":"[0119] Autres cultures non permanentes",
                        "children":[
                           {
                              "code":"0119Z",
                              "libelle":"[0119Z] Autres cultures non permanentes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"012",
                  "libelle":"[012] Cultures permanentes",
                  "children":[
                     {
                        "code":"0121",
                        "libelle":"[0121] Culture de la vigne",
                        "children":[
                           {
                              "code":"0121Z",
                              "libelle":"[0121Z] Culture de la vigne"
                           }
                        ]
                     },
                     {
                        "code":"0122",
                        "libelle":"[0122] Culture de fruits tropicaux et subtropicaux",
                        "children":[
                           {
                              "code":"0122Z",
                              "libelle":"[0122Z] Culture de fruits tropicaux et subtropicaux"
                           }
                        ]
                     },
                     {
                        "code":"0123",
                        "libelle":"[0123] Culture d'agrumes",
                        "children":[
                           {
                              "code":"0123Z",
                              "libelle":"[0123Z] Culture d'agrumes"
                           }
                        ]
                     },
                     {
                        "code":"0124",
                        "libelle":"[0124] Culture de fruits à pépins et à noyau",
                        "children":[
                           {
                              "code":"0124Z",
                              "libelle":"[0124Z] Culture de fruits à pépins et à noyau"
                           }
                        ]
                     },
                     {
                        "code":"0125",
                        "libelle":"[0125] Culture d'autres fruits d'arbres ou d'arbustes et de fruits à coque",
                        "children":[
                           {
                              "code":"0125Z",
                              "libelle":"[0125Z] Culture d'autres fruits d'arbres ou d'arbustes et de fruits à coque"
                           }
                        ]
                     },
                     {
                        "code":"0126",
                        "libelle":"[0126] Culture de fruits oléagineux",
                        "children":[
                           {
                              "code":"0126Z",
                              "libelle":"[0126Z] Culture de fruits oléagineux"
                           }
                        ]
                     },
                     {
                        "code":"0127",
                        "libelle":"[0127] Culture de plantes à boissons",
                        "children":[
                           {
                              "code":"0127Z",
                              "libelle":"[0127Z] Culture de plantes à boissons"
                           }
                        ]
                     },
                     {
                        "code":"0128",
                        "libelle":"[0128] Culture de plantes à épices, aromatiques, médicinales et pharmaceutiques",
                        "children":[
                           {
                              "code":"0128Z",
                              "libelle":"[0128Z] Culture de plantes à épices, aromatiques, médicinales et pharmaceutiques"
                           }
                        ]
                     },
                     {
                        "code":"0129",
                        "libelle":"[0129] Autres cultures permanentes",
                        "children":[
                           {
                              "code":"0129Z",
                              "libelle":"[0129Z] Autres cultures permanentes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"013",
                  "libelle":"[013] Reproduction de plantes",
                  "children":[
                     {
                        "code":"0130",
                        "libelle":"[0130] Reproduction de plantes",
                        "children":[
                           {
                              "code":"0130Z",
                              "libelle":"[0130Z] Reproduction de plantes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"014",
                  "libelle":"[014] Production animale",
                  "children":[
                     {
                        "code":"0141",
                        "libelle":"[0141] Élevage de vaches laitières",
                        "children":[
                           {
                              "code":"0141Z",
                              "libelle":"[0141Z] Élevage de vaches laitières"
                           }
                        ]
                     },
                     {
                        "code":"0142",
                        "libelle":"[0142] Élevage d'autres bovins et de buffles",
                        "children":[
                           {
                              "code":"0142Z",
                              "libelle":"[0142Z] Élevage d'autres bovins et de buffles"
                           }
                        ]
                     },
                     {
                        "code":"0143",
                        "libelle":"[0143] Élevage de chevaux et d'autres équidés",
                        "children":[
                           {
                              "code":"0143Z",
                              "libelle":"[0143Z] Élevage de chevaux et d'autres équidés"
                           }
                        ]
                     },
                     {
                        "code":"0144",
                        "libelle":"[0144] Élevage de chameaux et d'autres camélidés",
                        "children":[
                           {
                              "code":"0144Z",
                              "libelle":"[0144Z] Élevage de chameaux et d'autres camélidés"
                           }
                        ]
                     },
                     {
                        "code":"0145",
                        "libelle":"[0145] Élevage d'ovins et de caprins",
                        "children":[
                           {
                              "code":"0145Z",
                              "libelle":"[0145Z] Élevage d'ovins et de caprins"
                           }
                        ]
                     },
                     {
                        "code":"0146",
                        "libelle":"[0146] Élevage de porcins",
                        "children":[
                           {
                              "code":"0146Z",
                              "libelle":"[0146Z] Élevage de porcins"
                           }
                        ]
                     },
                     {
                        "code":"0147",
                        "libelle":"[0147] Élevage de volailles",
                        "children":[
                           {
                              "code":"0147Z",
                              "libelle":"[0147Z] Élevage de volailles"
                           }
                        ]
                     },
                     {
                        "code":"0149",
                        "libelle":"[0149] Élevage d'autres animaux",
                        "children":[
                           {
                              "code":"0149Z",
                              "libelle":"[0149Z] Élevage d'autres animaux"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"015",
                  "libelle":"[015] Culture et élevage associés",
                  "children":[
                     {
                        "code":"0150",
                        "libelle":"[0150] Culture et élevage associés",
                        "children":[
                           {
                              "code":"0150Z",
                              "libelle":"[0150Z] Culture et élevage associés"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"016",
                  "libelle":"[016] Activités de soutien à l'agriculture et traitement primaire des récoltes",
                  "children":[
                     {
                        "code":"0161",
                        "libelle":"[0161] Activités de soutien aux cultures",
                        "children":[
                           {
                              "code":"0161Z",
                              "libelle":"[0161Z] Activités de soutien aux cultures"
                           }
                        ]
                     },
                     {
                        "code":"0162",
                        "libelle":"[0162] Activités de soutien à la production animale",
                        "children":[
                           {
                              "code":"0162Z",
                              "libelle":"[0162Z] Activités de soutien à la production animale"
                           }
                        ]
                     },
                     {
                        "code":"0163",
                        "libelle":"[0163] Traitement primaire des récoltes",
                        "children":[
                           {
                              "code":"0163Z",
                              "libelle":"[0163Z] Traitement primaire des récoltes"
                           }
                        ]
                     },
                     {
                        "code":"0164",
                        "libelle":"[0164] Traitement des semences",
                        "children":[
                           {
                              "code":"0164Z",
                              "libelle":"[0164Z] Traitement des semences"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"017",
                  "libelle":"[017] Chasse, piégeage et services annexes",
                  "children":[
                     {
                        "code":"0170",
                        "libelle":"[0170] Chasse, piégeage et services annexes",
                        "children":[
                           {
                              "code":"0170Z",
                              "libelle":"[0170Z] Chasse, piégeage et services annexes"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"02",
            "code_section":"A",
            "libelle":"[02] Sylviculture et exploitation forestière",
            "children":[
               {
                  "code":"021",
                  "libelle":"[021] Sylviculture et autres activités forestières",
                  "children":[
                     {
                        "code":"0210",
                        "libelle":"[0210] Sylviculture et autres activités forestières",
                        "children":[
                           {
                              "code":"0210Z",
                              "libelle":"[0210Z] Sylviculture et autres activités forestières"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"022",
                  "libelle":"[022] Exploitation forestière",
                  "children":[
                     {
                        "code":"0220",
                        "libelle":"[0220] Exploitation forestière",
                        "children":[
                           {
                              "code":"0220Z",
                              "libelle":"[0220Z] Exploitation forestière"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"023",
                  "libelle":"[023] Récolte de produits forestiers non ligneux poussant à l'état sauvage",
                  "children":[
                     {
                        "code":"0230",
                        "libelle":"[0230] Récolte de produits forestiers non ligneux poussant à l'état sauvage",
                        "children":[
                           {
                              "code":"0230Z",
                              "libelle":"[0230Z] Récolte de produits forestiers non ligneux poussant à l'état sauvage"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"024",
                  "libelle":"[024] Services de soutien à l'exploitation forestière",
                  "children":[
                     {
                        "code":"0240",
                        "libelle":"[0240] Services de soutien à l'exploitation forestière",
                        "children":[
                           {
                              "code":"0240Z",
                              "libelle":"[0240Z] Services de soutien à l'exploitation forestière"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"03",
            "code_section":"A",
            "libelle":"[03] Pêche et aquaculture",
            "children":[
               {
                  "code":"031",
                  "libelle":"[031] Pêche",
                  "children":[
                     {
                        "code":"0311",
                        "libelle":"[0311] Pêche en mer",
                        "children":[
                           {
                              "code":"0311Z",
                              "libelle":"[0311Z] Pêche en mer"
                           }
                        ]
                     },
                     {
                        "code":"0312",
                        "libelle":"[0312] Pêche en eau douce",
                        "children":[
                           {
                              "code":"0312Z",
                              "libelle":"[0312Z] Pêche en eau douce"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"032",
                  "libelle":"[032] Aquaculture",
                  "children":[
                     {
                        "code":"0321",
                        "libelle":"[0321] Aquaculture en mer",
                        "children":[
                           {
                              "code":"0321Z",
                              "libelle":"[0321Z] Aquaculture en mer"
                           }
                        ]
                     },
                     {
                        "code":"0322",
                        "libelle":"[0322] Aquaculture en eau douce",
                        "children":[
                           {
                              "code":"0322Z",
                              "libelle":"[0322Z] Aquaculture en eau douce"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"B",
      "libelle":"[B] Industries extractives",
      "children":[
         {
            "code":"05",
            "code_section":"B",
            "libelle":"[05] Extraction de houille et de lignite",
            "children":[
               {
                  "code":"051",
                  "libelle":"[051] Extraction de houille",
                  "children":[
                     {
                        "code":"0510",
                        "libelle":"[0510] Extraction de houille",
                        "children":[
                           {
                              "code":"0510Z",
                              "libelle":"[0510Z] Extraction de houille"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"052",
                  "libelle":"[052] Extraction de lignite",
                  "children":[
                     {
                        "code":"0520",
                        "libelle":"[0520] Extraction de lignite",
                        "children":[
                           {
                              "code":"0520Z",
                              "libelle":"[0520Z] Extraction de lignite"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"06",
            "code_section":"B",
            "libelle":"[06] Extraction d'hydrocarbures",
            "children":[
               {
                  "code":"061",
                  "libelle":"[061] Extraction de pétrole brut",
                  "children":[
                     {
                        "code":"0610",
                        "libelle":"[0610] Extraction de pétrole brut",
                        "children":[
                           {
                              "code":"0610Z",
                              "libelle":"[0610Z] Extraction de pétrole brut"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"062",
                  "libelle":"[062] Extraction de gaz naturel",
                  "children":[
                     {
                        "code":"0620",
                        "libelle":"[0620] Extraction de gaz naturel",
                        "children":[
                           {
                              "code":"0620Z",
                              "libelle":"[0620Z] Extraction de gaz naturel"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"07",
            "code_section":"B",
            "libelle":"[07] Extraction de minerais métalliques",
            "children":[
               {
                  "code":"071",
                  "libelle":"[071] Extraction de minerais de fer",
                  "children":[
                     {
                        "code":"0710",
                        "libelle":"[0710] Extraction de minerais de fer",
                        "children":[
                           {
                              "code":"0710Z",
                              "libelle":"[0710Z] Extraction de minerais de fer"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"072",
                  "libelle":"[072] Extraction de minerais de métaux non ferreux",
                  "children":[
                     {
                        "code":"0721",
                        "libelle":"[0721] Extraction de minerais d'uranium et de thorium",
                        "children":[
                           {
                              "code":"0721Z",
                              "libelle":"[0721Z] Extraction de minerais d'uranium et de thorium"
                           }
                        ]
                     },
                     {
                        "code":"0729",
                        "libelle":"[0729] Extraction d'autres minerais de métaux non ferreux",
                        "children":[
                           {
                              "code":"0729Z",
                              "libelle":"[0729Z] Extraction d'autres minerais de métaux non ferreux"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"08",
            "code_section":"B",
            "libelle":"[08] Autres industries extractives",
            "children":[
               {
                  "code":"081",
                  "libelle":"[081] Extraction de pierres, de sables et d'argiles",
                  "children":[
                     {
                        "code":"0811",
                        "libelle":"[0811] Extraction de pierres ornementales et de construction, de calcaire industriel, de gypse, de craie et d'ardoise",
                        "children":[
                           {
                              "code":"0811Z",
                              "libelle":"[0811Z] Extraction de pierres ornementales et de construction, de calcaire industriel, de gypse, de craie et d'ardoise"
                           }
                        ]
                     },
                     {
                        "code":"0812",
                        "libelle":"[0812] Exploitation de gravières et sablières, extraction d'argiles et de kaolin",
                        "children":[
                           {
                              "code":"0812Z",
                              "libelle":"[0812Z] Exploitation de gravières et sablières, extraction d'argiles et de kaolin"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"089",
                  "libelle":"[089] Activités extractives n.c.a.",
                  "children":[
                     {
                        "code":"0891",
                        "libelle":"[0891] Extraction des minéraux chimiques et d'engrais minéraux",
                        "children":[
                           {
                              "code":"0891Z",
                              "libelle":"[0891Z] Extraction des minéraux chimiques et d'engrais minéraux"
                           }
                        ]
                     },
                     {
                        "code":"0892",
                        "libelle":"[0892] Extraction de tourbe",
                        "children":[
                           {
                              "code":"0892Z",
                              "libelle":"[0892Z] Extraction de tourbe"
                           }
                        ]
                     },
                     {
                        "code":"0893",
                        "libelle":"[0893] Production de sel",
                        "children":[
                           {
                              "code":"0893Z",
                              "libelle":"[0893Z] Production de sel"
                           }
                        ]
                     },
                     {
                        "code":"0899",
                        "libelle":"[0899] Autres activités extractives n.c.a.",
                        "children":[
                           {
                              "code":"0899Z",
                              "libelle":"[0899Z] Autres activités extractives n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"09",
            "code_section":"B",
            "libelle":"[09] Services de soutien aux industries extractives",
            "children":[
               {
                  "code":"091",
                  "libelle":"[091] Activités de soutien à l'extraction d'hydrocarbures",
                  "children":[
                     {
                        "code":"0910",
                        "libelle":"[0910] Activités de soutien à l'extraction d'hydrocarbures",
                        "children":[
                           {
                              "code":"0910Z",
                              "libelle":"[0910Z] Activités de soutien à l'extraction d'hydrocarbures"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"099",
                  "libelle":"[099] Activités de soutien aux autres industries extractives",
                  "children":[
                     {
                        "code":"0990",
                        "libelle":"[0990] Activités de soutien aux autres industries extractives",
                        "children":[
                           {
                              "code":"0990Z",
                              "libelle":"[0990Z] Activités de soutien aux autres industries extractives"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"C",
      "libelle":"[C] Industrie manufacturière",
      "children":[
         {
            "code":"10",
            "code_section":"C",
            "libelle":"[10] Industries alimentaires",
            "children":[
               {
                  "code":"101",
                  "libelle":"[101] Transformation et conservation de la viande et préparation de produits à base de viande",
                  "children":[
                     {
                        "code":"1011",
                        "libelle":"[1011] Transformation et conservation de la viande de boucherie",
                        "children":[
                           {
                              "code":"1011Z",
                              "libelle":"[1011Z] Transformation et conservation de la viande de boucherie"
                           }
                        ]
                     },
                     {
                        "code":"1012",
                        "libelle":"[1012] Transformation et conservation de la viande de volaille",
                        "children":[
                           {
                              "code":"1012Z",
                              "libelle":"[1012Z] Transformation et conservation de la viande de volaille"
                           }
                        ]
                     },
                     {
                        "code":"1013",
                        "libelle":"[1013] Préparation de produits à base de viande",
                        "children":[
                           {
                              "code":"1013A",
                              "libelle":"[1013A] Préparation industrielle de produits à base de viande"
                           },
                           {
                              "code":"1013B",
                              "libelle":"[1013B] Charcuterie"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"102",
                  "libelle":"[102] Transformation et conservation de poisson, de crustacés et de mollusques",
                  "children":[
                     {
                        "code":"1020",
                        "libelle":"[1020] Transformation et conservation de poisson, de crustacés et de mollusques",
                        "children":[
                           {
                              "code":"1020Z",
                              "libelle":"[1020Z] Transformation et conservation de poisson, de crustacés et de mollusques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"103",
                  "libelle":"[103] Transformation et conservation de fruits et légumes",
                  "children":[
                     {
                        "code":"1031",
                        "libelle":"[1031] Transformation et conservation de pommes de terre",
                        "children":[
                           {
                              "code":"1031Z",
                              "libelle":"[1031Z] Transformation et conservation de pommes de terre"
                           }
                        ]
                     },
                     {
                        "code":"1032",
                        "libelle":"[1032] Préparation de jus de fruits et légumes",
                        "children":[
                           {
                              "code":"1032Z",
                              "libelle":"[1032Z] Préparation de jus de fruits et légumes"
                           }
                        ]
                     },
                     {
                        "code":"1039",
                        "libelle":"[1039] Autre transformation et conservation de fruits et légumes",
                        "children":[
                           {
                              "code":"1039A",
                              "libelle":"[1039A] Autre transformation et conservation de légumes"
                           },
                           {
                              "code":"1039B",
                              "libelle":"[1039B] Transformation et conservation de fruits"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"104",
                  "libelle":"[104] Fabrication d'huiles et graisses végétales et animales",
                  "children":[
                     {
                        "code":"1041",
                        "libelle":"[1041] Fabrication d'huiles et graisses",
                        "children":[
                           {
                              "code":"1041A",
                              "libelle":"[1041A] Fabrication d'huiles et graisses brutes"
                           },
                           {
                              "code":"1041B",
                              "libelle":"[1041B] Fabrication d'huiles et graisses raffinées"
                           }
                        ]
                     },
                     {
                        "code":"1042",
                        "libelle":"[1042] Fabrication de margarine et graisses comestibles similaires",
                        "children":[
                           {
                              "code":"1042Z",
                              "libelle":"[1042Z] Fabrication de margarine et graisses comestibles similaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"105",
                  "libelle":"[105] Fabrication de produits laitiers",
                  "children":[
                     {
                        "code":"1051",
                        "libelle":"[1051] Exploitation de laiteries et fabrication de fromage",
                        "children":[
                           {
                              "code":"1051A",
                              "libelle":"[1051A] Fabrication de lait liquide et de produits frais"
                           },
                           {
                              "code":"1051B",
                              "libelle":"[1051B] Fabrication de beurre"
                           },
                           {
                              "code":"1051C",
                              "libelle":"[1051C] Fabrication de fromage"
                           },
                           {
                              "code":"1051D",
                              "libelle":"[1051D] Fabrication d'autres produits laitiers"
                           }
                        ]
                     },
                     {
                        "code":"1052",
                        "libelle":"[1052] Fabrication de glaces et sorbets",
                        "children":[
                           {
                              "code":"1052Z",
                              "libelle":"[1052Z] Fabrication de glaces et sorbets"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"106",
                  "libelle":"[106] Travail des grains ; fabrication de produits amylacés",
                  "children":[
                     {
                        "code":"1061",
                        "libelle":"[1061] Travail des grains",
                        "children":[
                           {
                              "code":"1061A",
                              "libelle":"[1061A] Meunerie"
                           },
                           {
                              "code":"1061B",
                              "libelle":"[1061B] Autres activités du travail des grains"
                           }
                        ]
                     },
                     {
                        "code":"1062",
                        "libelle":"[1062] Fabrication de produits amylacés",
                        "children":[
                           {
                              "code":"1062Z",
                              "libelle":"[1062Z] Fabrication de produits amylacés"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"107",
                  "libelle":"[107] Fabrication de produits de boulangerie-pâtisserie et de pâtes alimentaires",
                  "children":[
                     {
                        "code":"1071",
                        "libelle":"[1071] Fabrication de pain et de pâtisserie fraîche",
                        "children":[
                           {
                              "code":"1071A",
                              "libelle":"[1071A] Fabrication industrielle de pain et de pâtisserie fraîche"
                           },
                           {
                              "code":"1071B",
                              "libelle":"[1071B] Cuisson de produits de boulangerie"
                           },
                           {
                              "code":"1071C",
                              "libelle":"[1071C] Boulangerie et boulangerie-pâtisserie"
                           },
                           {
                              "code":"1071D",
                              "libelle":"[1071D] Pâtisserie"
                           }
                        ]
                     },
                     {
                        "code":"1072",
                        "libelle":"[1072] Fabrication de biscuits, biscottes et pâtisseries de conservation",
                        "children":[
                           {
                              "code":"1072Z",
                              "libelle":"[1072Z] Fabrication de biscuits, biscottes et pâtisseries de conservation"
                           }
                        ]
                     },
                     {
                        "code":"1073",
                        "libelle":"[1073] Fabrication de pâtes alimentaires",
                        "children":[
                           {
                              "code":"1073Z",
                              "libelle":"[1073Z] Fabrication de pâtes alimentaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"108",
                  "libelle":"[108] Fabrication d'autres produits alimentaires",
                  "children":[
                     {
                        "code":"1081",
                        "libelle":"[1081] Fabrication de sucre",
                        "children":[
                           {
                              "code":"1081Z",
                              "libelle":"[1081Z] Fabrication de sucre"
                           }
                        ]
                     },
                     {
                        "code":"1082",
                        "libelle":"[1082] Fabrication de cacao, chocolat et de produits de confiserie",
                        "children":[
                           {
                              "code":"1082Z",
                              "libelle":"[1082Z] Fabrication de cacao, chocolat et de produits de confiserie"
                           }
                        ]
                     },
                     {
                        "code":"1083",
                        "libelle":"[1083] Transformation du thé et du café",
                        "children":[
                           {
                              "code":"1083Z",
                              "libelle":"[1083Z] Transformation du thé et du café"
                           }
                        ]
                     },
                     {
                        "code":"1084",
                        "libelle":"[1084] Fabrication de condiments et assaisonnements",
                        "children":[
                           {
                              "code":"1084Z",
                              "libelle":"[1084Z] Fabrication de condiments et assaisonnements"
                           }
                        ]
                     },
                     {
                        "code":"1085",
                        "libelle":"[1085] Fabrication de plats préparés",
                        "children":[
                           {
                              "code":"1085Z",
                              "libelle":"[1085Z] Fabrication de plats préparés"
                           }
                        ]
                     },
                     {
                        "code":"1086",
                        "libelle":"[1086] Fabrication d'aliments homogénéisés et diététiques",
                        "children":[
                           {
                              "code":"1086Z",
                              "libelle":"[1086Z] Fabrication d'aliments homogénéisés et diététiques"
                           }
                        ]
                     },
                     {
                        "code":"1089",
                        "libelle":"[1089] Fabrication d'autres produits alimentaires n.c.a.",
                        "children":[
                           {
                              "code":"1089Z",
                              "libelle":"[1089Z] Fabrication d'autres produits alimentaires n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"109",
                  "libelle":"[109] Fabrication d'aliments pour animaux",
                  "children":[
                     {
                        "code":"1091",
                        "libelle":"[1091] Fabrication d'aliments pour animaux de ferme",
                        "children":[
                           {
                              "code":"1091Z",
                              "libelle":"[1091Z] Fabrication d'aliments pour animaux de ferme"
                           }
                        ]
                     },
                     {
                        "code":"1092",
                        "libelle":"[1092] Fabrication d'aliments pour animaux de compagnie",
                        "children":[
                           {
                              "code":"1092Z",
                              "libelle":"[1092Z] Fabrication d'aliments pour animaux de compagnie"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"11",
            "code_section":"C",
            "libelle":"[11] Fabrication de boissons",
            "children":[
               {
                  "code":"110",
                  "libelle":"[110] Fabrication de boissons",
                  "children":[
                     {
                        "code":"1101",
                        "libelle":"[1101] Production de boissons alcooliques distillées",
                        "children":[
                           {
                              "code":"1101Z",
                              "libelle":"[1101Z] Production de boissons alcooliques distillées"
                           }
                        ]
                     },
                     {
                        "code":"1102",
                        "libelle":"[1102] Production de vin (de raisin)",
                        "children":[
                           {
                              "code":"1102A",
                              "libelle":"[1102A] Fabrication de vins effervescents"
                           },
                           {
                              "code":"1102B",
                              "libelle":"[1102B] Vinification"
                           }
                        ]
                     },
                     {
                        "code":"1103",
                        "libelle":"[1103] Fabrication de cidre et de vins de fruits",
                        "children":[
                           {
                              "code":"1103Z",
                              "libelle":"[1103Z] Fabrication de cidre et de vins de fruits"
                           }
                        ]
                     },
                     {
                        "code":"1104",
                        "libelle":"[1104] Production d'autres boissons fermentées non distillées",
                        "children":[
                           {
                              "code":"1104Z",
                              "libelle":"[1104Z] Production d'autres boissons fermentées non distillées"
                           }
                        ]
                     },
                     {
                        "code":"1105",
                        "libelle":"[1105] Fabrication de bière",
                        "children":[
                           {
                              "code":"1105Z",
                              "libelle":"[1105Z] Fabrication de bière"
                           }
                        ]
                     },
                     {
                        "code":"1106",
                        "libelle":"[1106] Fabrication de malt",
                        "children":[
                           {
                              "code":"1106Z",
                              "libelle":"[1106Z] Fabrication de malt"
                           }
                        ]
                     },
                     {
                        "code":"1107",
                        "libelle":"[1107] Industrie des eaux minérales et autres eaux embouteillées et des boissons rafraîchissantes",
                        "children":[
                           {
                              "code":"1107A",
                              "libelle":"[1107A] Industrie des eaux de table"
                           },
                           {
                              "code":"1107B",
                              "libelle":"[1107B] Production de boissons rafraîchissantes"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"12",
            "code_section":"C",
            "libelle":"[12] Fabrication de produits à base de tabac",
            "children":[
               {
                  "code":"120",
                  "libelle":"[120] Fabrication de produits à base de tabac",
                  "children":[
                     {
                        "code":"1200",
                        "libelle":"[1200] Fabrication de produits à base de tabac",
                        "children":[
                           {
                              "code":"1200Z",
                              "libelle":"[1200Z] Fabrication de produits à base de tabac"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"13",
            "code_section":"C",
            "libelle":"[13] Fabrication de textiles",
            "children":[
               {
                  "code":"131",
                  "libelle":"[131] Préparation de fibres textiles et filature",
                  "children":[
                     {
                        "code":"1310",
                        "libelle":"[1310] Préparation de fibres textiles et filature",
                        "children":[
                           {
                              "code":"1310Z",
                              "libelle":"[1310Z] Préparation de fibres textiles et filature"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"132",
                  "libelle":"[132] Tissage",
                  "children":[
                     {
                        "code":"1320",
                        "libelle":"[1320] Tissage",
                        "children":[
                           {
                              "code":"1320Z",
                              "libelle":"[1320Z] Tissage"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"133",
                  "libelle":"[133] Ennoblissement textile",
                  "children":[
                     {
                        "code":"1330",
                        "libelle":"[1330] Ennoblissement textile",
                        "children":[
                           {
                              "code":"1330Z",
                              "libelle":"[1330Z] Ennoblissement textile"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"139",
                  "libelle":"[139] Fabrication d'autres textiles",
                  "children":[
                     {
                        "code":"1391",
                        "libelle":"[1391] Fabrication d'étoffes à mailles",
                        "children":[
                           {
                              "code":"1391Z",
                              "libelle":"[1391Z] Fabrication d'étoffes à mailles"
                           }
                        ]
                     },
                     {
                        "code":"1392",
                        "libelle":"[1392] Fabrication d'articles textiles, sauf habillement",
                        "children":[
                           {
                              "code":"1392Z",
                              "libelle":"[1392Z] Fabrication d'articles textiles, sauf habillement"
                           }
                        ]
                     },
                     {
                        "code":"1393",
                        "libelle":"[1393] Fabrication de tapis et moquettes",
                        "children":[
                           {
                              "code":"1393Z",
                              "libelle":"[1393Z] Fabrication de tapis et moquettes"
                           }
                        ]
                     },
                     {
                        "code":"1394",
                        "libelle":"[1394] Fabrication de ficelles, cordes et filets",
                        "children":[
                           {
                              "code":"1394Z",
                              "libelle":"[1394Z] Fabrication de ficelles, cordes et filets"
                           }
                        ]
                     },
                     {
                        "code":"1395",
                        "libelle":"[1395] Fabrication de non-tissés, sauf habillement",
                        "children":[
                           {
                              "code":"1395Z",
                              "libelle":"[1395Z] Fabrication de non-tissés, sauf habillement"
                           }
                        ]
                     },
                     {
                        "code":"1396",
                        "libelle":"[1396] Fabrication d'autres textiles techniques et industriels",
                        "children":[
                           {
                              "code":"1396Z",
                              "libelle":"[1396Z] Fabrication d'autres textiles techniques et industriels"
                           }
                        ]
                     },
                     {
                        "code":"1399",
                        "libelle":"[1399] Fabrication d'autres textiles n.c.a.",
                        "children":[
                           {
                              "code":"1399Z",
                              "libelle":"[1399Z] Fabrication d'autres textiles n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"14",
            "code_section":"C",
            "libelle":"[14] Industrie de l'habillement",
            "children":[
               {
                  "code":"141",
                  "libelle":"[141] Fabrication de vêtements, autres qu'en fourrure",
                  "children":[
                     {
                        "code":"1411",
                        "libelle":"[1411] Fabrication de vêtements en cuir",
                        "children":[
                           {
                              "code":"1411Z",
                              "libelle":"[1411Z] Fabrication de vêtements en cuir"
                           }
                        ]
                     },
                     {
                        "code":"1412",
                        "libelle":"[1412] Fabrication de vêtements de travail",
                        "children":[
                           {
                              "code":"1412Z",
                              "libelle":"[1412Z] Fabrication de vêtements de travail"
                           }
                        ]
                     },
                     {
                        "code":"1413",
                        "libelle":"[1413] Fabrication de vêtements de dessus",
                        "children":[
                           {
                              "code":"1413Z",
                              "libelle":"[1413Z] Fabrication de vêtements de dessus"
                           }
                        ]
                     },
                     {
                        "code":"1414",
                        "libelle":"[1414] Fabrication de vêtements de dessous",
                        "children":[
                           {
                              "code":"1414Z",
                              "libelle":"[1414Z] Fabrication de vêtements de dessous"
                           }
                        ]
                     },
                     {
                        "code":"1419",
                        "libelle":"[1419] Fabrication d'autres vêtements et accessoires",
                        "children":[
                           {
                              "code":"1419Z",
                              "libelle":"[1419Z] Fabrication d'autres vêtements et accessoires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"142",
                  "libelle":"[142] Fabrication d'articles en fourrure",
                  "children":[
                     {
                        "code":"1420",
                        "libelle":"[1420] Fabrication d'articles en fourrure",
                        "children":[
                           {
                              "code":"1420Z",
                              "libelle":"[1420Z] Fabrication d'articles en fourrure"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"143",
                  "libelle":"[143] Fabrication d'articles à mailles",
                  "children":[
                     {
                        "code":"1431",
                        "libelle":"[1431] Fabrication d'articles chaussants à mailles",
                        "children":[
                           {
                              "code":"1431Z",
                              "libelle":"[1431Z] Fabrication d'articles chaussants à mailles"
                           }
                        ]
                     },
                     {
                        "code":"1439",
                        "libelle":"[1439] Fabrication d'autres articles à mailles",
                        "children":[
                           {
                              "code":"1439Z",
                              "libelle":"[1439Z] Fabrication d'autres articles à mailles"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"15",
            "code_section":"C",
            "libelle":"[15] Industrie du cuir et de la chaussure",
            "children":[
               {
                  "code":"151",
                  "libelle":"[151] Apprêt et tannage des cuirs ; préparation et teinture des fourrures ; fabrication d'articles de voyage, de maroquinerie et de sellerie",
                  "children":[
                     {
                        "code":"1511",
                        "libelle":"[1511] Apprêt et tannage des cuirs ; préparation et teinture des fourrures",
                        "children":[
                           {
                              "code":"1511Z",
                              "libelle":"[1511Z] Apprêt et tannage des cuirs ; préparation et teinture des fourrures"
                           }
                        ]
                     },
                     {
                        "code":"1512",
                        "libelle":"[1512] Fabrication d'articles de voyage, de maroquinerie et de sellerie",
                        "children":[
                           {
                              "code":"1512Z",
                              "libelle":"[1512Z] Fabrication d'articles de voyage, de maroquinerie et de sellerie"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"152",
                  "libelle":"[152] Fabrication de chaussures",
                  "children":[
                     {
                        "code":"1520",
                        "libelle":"[1520] Fabrication de chaussures",
                        "children":[
                           {
                              "code":"1520Z",
                              "libelle":"[1520Z] Fabrication de chaussures"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"16",
            "code_section":"C",
            "libelle":"[16] Travail du bois et fabrication d'articles en bois et en liège, à l'exception des meubles ; fabrication d'articles en vannerie et sparterie",
            "children":[
               {
                  "code":"161",
                  "libelle":"[161] Sciage et rabotage du bois",
                  "children":[
                     {
                        "code":"1610",
                        "libelle":"[1610] Sciage et rabotage du bois",
                        "children":[
                           {
                              "code":"1610A",
                              "libelle":"[1610A] Sciage et rabotage du bois, hors imprégnation"
                           },
                           {
                              "code":"1610B",
                              "libelle":"[1610B] Imprégnation du bois"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"162",
                  "libelle":"[162] Fabrication d'articles en bois, liège, vannerie et sparterie",
                  "children":[
                     {
                        "code":"1621",
                        "libelle":"[1621] Fabrication de placage et de panneaux de bois",
                        "children":[
                           {
                              "code":"1621Z",
                              "libelle":"[1621Z] Fabrication de placage et de panneaux de bois"
                           }
                        ]
                     },
                     {
                        "code":"1622",
                        "libelle":"[1622] Fabrication de parquets assemblés",
                        "children":[
                           {
                              "code":"1622Z",
                              "libelle":"[1622Z] Fabrication de parquets assemblés"
                           }
                        ]
                     },
                     {
                        "code":"1623",
                        "libelle":"[1623] Fabrication de charpentes et d'autres menuiseries",
                        "children":[
                           {
                              "code":"1623Z",
                              "libelle":"[1623Z] Fabrication de charpentes et d'autres menuiseries"
                           }
                        ]
                     },
                     {
                        "code":"1624",
                        "libelle":"[1624] Fabrication d'emballages en bois",
                        "children":[
                           {
                              "code":"1624Z",
                              "libelle":"[1624Z] Fabrication d'emballages en bois"
                           }
                        ]
                     },
                     {
                        "code":"1629",
                        "libelle":"[1629] Fabrication d'objets divers en bois ; fabrication d'objets en liège, vannerie et sparterie",
                        "children":[
                           {
                              "code":"1629Z",
                              "libelle":"[1629Z] Fabrication d'objets divers en bois ; fabrication d'objets en liège, vannerie et sparterie"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"17",
            "code_section":"C",
            "libelle":"[17] Industrie du papier et du carton",
            "children":[
               {
                  "code":"171",
                  "libelle":"[171] Fabrication de pâte à papier, de papier et de carton",
                  "children":[
                     {
                        "code":"1711",
                        "libelle":"[1711] Fabrication de pâte à papier",
                        "children":[
                           {
                              "code":"1711Z",
                              "libelle":"[1711Z] Fabrication de pâte à papier"
                           }
                        ]
                     },
                     {
                        "code":"1712",
                        "libelle":"[1712] Fabrication de papier et de carton",
                        "children":[
                           {
                              "code":"1712Z",
                              "libelle":"[1712Z] Fabrication de papier et de carton"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"172",
                  "libelle":"[172] Fabrication d'articles en papier ou en carton",
                  "children":[
                     {
                        "code":"1721",
                        "libelle":"[1721] Fabrication de papier et carton ondulés et d'emballages en papier ou en carton",
                        "children":[
                           {
                              "code":"1721A",
                              "libelle":"[1721A] Fabrication de carton ondulé"
                           },
                           {
                              "code":"1721B",
                              "libelle":"[1721B] Fabrication de cartonnages"
                           },
                           {
                              "code":"1721C",
                              "libelle":"[1721C] Fabrication d'emballages en papier"
                           }
                        ]
                     },
                     {
                        "code":"1722",
                        "libelle":"[1722] Fabrication d'articles en papier à usage sanitaire ou domestique",
                        "children":[
                           {
                              "code":"1722Z",
                              "libelle":"[1722Z] Fabrication d'articles en papier à usage sanitaire ou domestique"
                           }
                        ]
                     },
                     {
                        "code":"1723",
                        "libelle":"[1723] Fabrication d'articles de papeterie",
                        "children":[
                           {
                              "code":"1723Z",
                              "libelle":"[1723Z] Fabrication d'articles de papeterie"
                           }
                        ]
                     },
                     {
                        "code":"1724",
                        "libelle":"[1724] Fabrication de papiers peints",
                        "children":[
                           {
                              "code":"1724Z",
                              "libelle":"[1724Z] Fabrication de papiers peints"
                           }
                        ]
                     },
                     {
                        "code":"1729",
                        "libelle":"[1729] Fabrication d'autres articles en papier ou en carton",
                        "children":[
                           {
                              "code":"1729Z",
                              "libelle":"[1729Z] Fabrication d'autres articles en papier ou en carton"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"18",
            "code_section":"C",
            "libelle":"[18] Imprimerie et reproduction d'enregistrements",
            "children":[
               {
                  "code":"181",
                  "libelle":"[181] Imprimerie et services annexes",
                  "children":[
                     {
                        "code":"1811",
                        "libelle":"[1811] Imprimerie de journaux",
                        "children":[
                           {
                              "code":"1811Z",
                              "libelle":"[1811Z] Imprimerie de journaux"
                           }
                        ]
                     },
                     {
                        "code":"1812",
                        "libelle":"[1812] Autre imprimerie (labeur)",
                        "children":[
                           {
                              "code":"1812Z",
                              "libelle":"[1812Z] Autre imprimerie (labeur)"
                           }
                        ]
                     },
                     {
                        "code":"1813",
                        "libelle":"[1813] Activités de pré-presse",
                        "children":[
                           {
                              "code":"1813Z",
                              "libelle":"[1813Z] Activités de pré-presse"
                           }
                        ]
                     },
                     {
                        "code":"1814",
                        "libelle":"[1814] Reliure et activités connexes",
                        "children":[
                           {
                              "code":"1814Z",
                              "libelle":"[1814Z] Reliure et activités connexes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"182",
                  "libelle":"[182] Reproduction d'enregistrements",
                  "children":[
                     {
                        "code":"1820",
                        "libelle":"[1820] Reproduction d'enregistrements",
                        "children":[
                           {
                              "code":"1820Z",
                              "libelle":"[1820Z] Reproduction d'enregistrements"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"19",
            "code_section":"C",
            "libelle":"[19] Cokéfaction et raffinage",
            "children":[
               {
                  "code":"191",
                  "libelle":"[191] Cokéfaction",
                  "children":[
                     {
                        "code":"1910",
                        "libelle":"[1910] Cokéfaction",
                        "children":[
                           {
                              "code":"1910Z",
                              "libelle":"[1910Z] Cokéfaction"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"192",
                  "libelle":"[192] Raffinage du pétrole",
                  "children":[
                     {
                        "code":"1920",
                        "libelle":"[1920] Raffinage du pétrole",
                        "children":[
                           {
                              "code":"1920Z",
                              "libelle":"[1920Z] Raffinage du pétrole"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"20",
            "code_section":"C",
            "libelle":"[20] Industrie chimique",
            "children":[
               {
                  "code":"201",
                  "libelle":"[201] Fabrication de produits chimiques de base, de produits azotés et d'engrais, de matières plastiques de base et de caoutchouc synthétique",
                  "children":[
                     {
                        "code":"2011",
                        "libelle":"[2011] Fabrication de gaz industriels",
                        "children":[
                           {
                              "code":"2011Z",
                              "libelle":"[2011Z] Fabrication de gaz industriels"
                           }
                        ]
                     },
                     {
                        "code":"2012",
                        "libelle":"[2012] Fabrication de colorants et de pigments",
                        "children":[
                           {
                              "code":"2012Z",
                              "libelle":"[2012Z] Fabrication de colorants et de pigments"
                           }
                        ]
                     },
                     {
                        "code":"2013",
                        "libelle":"[2013] Fabrication d'autres produits chimiques inorganiques de base",
                        "children":[
                           {
                              "code":"2013A",
                              "libelle":"[2013A] Enrichissement et retraitement de matières nucléaires"
                           },
                           {
                              "code":"2013B",
                              "libelle":"[2013B] Fabrication d'autres produits chimiques inorganiques de base n.c.a."
                           }
                        ]
                     },
                     {
                        "code":"2014",
                        "libelle":"[2014] Fabrication d'autres produits chimiques organiques de base",
                        "children":[
                           {
                              "code":"2014Z",
                              "libelle":"[2014Z] Fabrication d'autres produits chimiques organiques de base"
                           }
                        ]
                     },
                     {
                        "code":"2015",
                        "libelle":"[2015] Fabrication de produits azotés et d'engrais",
                        "children":[
                           {
                              "code":"2015Z",
                              "libelle":"[2015Z] Fabrication de produits azotés et d'engrais"
                           }
                        ]
                     },
                     {
                        "code":"2016",
                        "libelle":"[2016] Fabrication de matières plastiques de base",
                        "children":[
                           {
                              "code":"2016Z",
                              "libelle":"[2016Z] Fabrication de matières plastiques de base"
                           }
                        ]
                     },
                     {
                        "code":"2017",
                        "libelle":"[2017] Fabrication de caoutchouc synthétique",
                        "children":[
                           {
                              "code":"2017Z",
                              "libelle":"[2017Z] Fabrication de caoutchouc synthétique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"202",
                  "libelle":"[202] Fabrication de pesticides et d'autres produits agrochimiques",
                  "children":[
                     {
                        "code":"2020",
                        "libelle":"[2020] Fabrication de pesticides et d'autres produits agrochimiques",
                        "children":[
                           {
                              "code":"2020Z",
                              "libelle":"[2020Z] Fabrication de pesticides et d'autres produits agrochimiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"203",
                  "libelle":"[203] Fabrication de peintures, vernis, encres et mastics",
                  "children":[
                     {
                        "code":"2030",
                        "libelle":"[2030] Fabrication de peintures, vernis, encres et mastics",
                        "children":[
                           {
                              "code":"2030Z",
                              "libelle":"[2030Z] Fabrication de peintures, vernis, encres et mastics"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"204",
                  "libelle":"[204] Fabrication de savons, de produits d'entretien et de parfums",
                  "children":[
                     {
                        "code":"2041",
                        "libelle":"[2041] Fabrication de savons, détergents et produits d'entretien",
                        "children":[
                           {
                              "code":"2041Z",
                              "libelle":"[2041Z] Fabrication de savons, détergents et produits d'entretien"
                           }
                        ]
                     },
                     {
                        "code":"2042",
                        "libelle":"[2042] Fabrication de parfums et de produits pour la toilette",
                        "children":[
                           {
                              "code":"2042Z",
                              "libelle":"[2042Z] Fabrication de parfums et de produits pour la toilette"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"205",
                  "libelle":"[205] Fabrication d'autres produits chimiques",
                  "children":[
                     {
                        "code":"2051",
                        "libelle":"[2051] Fabrication de produits explosifs",
                        "children":[
                           {
                              "code":"2051Z",
                              "libelle":"[2051Z] Fabrication de produits explosifs"
                           }
                        ]
                     },
                     {
                        "code":"2052",
                        "libelle":"[2052] Fabrication de colles",
                        "children":[
                           {
                              "code":"2052Z",
                              "libelle":"[2052Z] Fabrication de colles"
                           }
                        ]
                     },
                     {
                        "code":"2053",
                        "libelle":"[2053] Fabrication d'huiles essentielles",
                        "children":[
                           {
                              "code":"2053Z",
                              "libelle":"[2053Z] Fabrication d'huiles essentielles"
                           }
                        ]
                     },
                     {
                        "code":"2059",
                        "libelle":"[2059] Fabrication d'autres produits chimiques n.c.a.",
                        "children":[
                           {
                              "code":"2059Z",
                              "libelle":"[2059Z] Fabrication d'autres produits chimiques n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"206",
                  "libelle":"[206] Fabrication de fibres artificielles ou synthétiques",
                  "children":[
                     {
                        "code":"2060",
                        "libelle":"[2060] Fabrication de fibres artificielles ou synthétiques",
                        "children":[
                           {
                              "code":"2060Z",
                              "libelle":"[2060Z] Fabrication de fibres artificielles ou synthétiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"21",
            "code_section":"C",
            "libelle":"[21] Industrie pharmaceutique",
            "children":[
               {
                  "code":"211",
                  "libelle":"[211] Fabrication de produits pharmaceutiques de base",
                  "children":[
                     {
                        "code":"2110",
                        "libelle":"[2110] Fabrication de produits pharmaceutiques de base",
                        "children":[
                           {
                              "code":"2110Z",
                              "libelle":"[2110Z] Fabrication de produits pharmaceutiques de base"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"212",
                  "libelle":"[212] Fabrication de préparations pharmaceutiques",
                  "children":[
                     {
                        "code":"2120",
                        "libelle":"[2120] Fabrication de préparations pharmaceutiques",
                        "children":[
                           {
                              "code":"2120Z",
                              "libelle":"[2120Z] Fabrication de préparations pharmaceutiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"22",
            "code_section":"C",
            "libelle":"[22] Fabrication de produits en caoutchouc et en plastique",
            "children":[
               {
                  "code":"221",
                  "libelle":"[221] Fabrication de produits en caoutchouc",
                  "children":[
                     {
                        "code":"2211",
                        "libelle":"[2211] Fabrication et rechapage de pneumatiques",
                        "children":[
                           {
                              "code":"2211Z",
                              "libelle":"[2211Z] Fabrication et rechapage de pneumatiques"
                           }
                        ]
                     },
                     {
                        "code":"2219",
                        "libelle":"[2219] Fabrication d'autres articles en caoutchouc",
                        "children":[
                           {
                              "code":"2219Z",
                              "libelle":"[2219Z] Fabrication d'autres articles en caoutchouc"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"222",
                  "libelle":"[222] Fabrication de produits en plastique",
                  "children":[
                     {
                        "code":"2221",
                        "libelle":"[2221] Fabrication de plaques, feuilles, tubes et profilés en matières plastiques",
                        "children":[
                           {
                              "code":"2221Z",
                              "libelle":"[2221Z] Fabrication de plaques, feuilles, tubes et profilés en matières plastiques"
                           }
                        ]
                     },
                     {
                        "code":"2222",
                        "libelle":"[2222] Fabrication d'emballages en matières plastiques",
                        "children":[
                           {
                              "code":"2222Z",
                              "libelle":"[2222Z] Fabrication d'emballages en matières plastiques"
                           }
                        ]
                     },
                     {
                        "code":"2223",
                        "libelle":"[2223] Fabrication d'éléments en matières plastiques pour la construction",
                        "children":[
                           {
                              "code":"2223Z",
                              "libelle":"[2223Z] Fabrication d'éléments en matières plastiques pour la construction"
                           }
                        ]
                     },
                     {
                        "code":"2229",
                        "libelle":"[2229] Fabrication d'autres articles en matières plastiques",
                        "children":[
                           {
                              "code":"2229A",
                              "libelle":"[2229A] Fabrication de pièces techniques à base de matières plastiques"
                           },
                           {
                              "code":"2229B",
                              "libelle":"[2229B] Fabrication de produits de consommation courante en matières plastiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"23",
            "code_section":"C",
            "libelle":"[23] Fabrication d'autres produits minéraux non métalliques",
            "children":[
               {
                  "code":"231",
                  "libelle":"[231] Fabrication de verre et d'articles en verre",
                  "children":[
                     {
                        "code":"2311",
                        "libelle":"[2311] Fabrication de verre plat",
                        "children":[
                           {
                              "code":"2311Z",
                              "libelle":"[2311Z] Fabrication de verre plat"
                           }
                        ]
                     },
                     {
                        "code":"2312",
                        "libelle":"[2312] Façonnage et transformation du verre plat",
                        "children":[
                           {
                              "code":"2312Z",
                              "libelle":"[2312Z] Façonnage et transformation du verre plat"
                           }
                        ]
                     },
                     {
                        "code":"2313",
                        "libelle":"[2313] Fabrication de verre creux",
                        "children":[
                           {
                              "code":"2313Z",
                              "libelle":"[2313Z] Fabrication de verre creux"
                           }
                        ]
                     },
                     {
                        "code":"2314",
                        "libelle":"[2314] Fabrication de fibres de verre",
                        "children":[
                           {
                              "code":"2314Z",
                              "libelle":"[2314Z] Fabrication de fibres de verre"
                           }
                        ]
                     },
                     {
                        "code":"2319",
                        "libelle":"[2319] Fabrication et façonnage d'autres articles en verre, y compris verre technique",
                        "children":[
                           {
                              "code":"2319Z",
                              "libelle":"[2319Z] Fabrication et façonnage d'autres articles en verre, y compris verre technique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"232",
                  "libelle":"[232] Fabrication de produits réfractaires",
                  "children":[
                     {
                        "code":"2320",
                        "libelle":"[2320] Fabrication de produits réfractaires",
                        "children":[
                           {
                              "code":"2320Z",
                              "libelle":"[2320Z] Fabrication de produits réfractaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"233",
                  "libelle":"[233] Fabrication de matériaux de construction en terre cuite",
                  "children":[
                     {
                        "code":"2331",
                        "libelle":"[2331] Fabrication de carreaux en céramique",
                        "children":[
                           {
                              "code":"2331Z",
                              "libelle":"[2331Z] Fabrication de carreaux en céramique"
                           }
                        ]
                     },
                     {
                        "code":"2332",
                        "libelle":"[2332] Fabrication de briques, tuiles et produits de construction, en terre cuite",
                        "children":[
                           {
                              "code":"2332Z",
                              "libelle":"[2332Z] Fabrication de briques, tuiles et produits de construction, en terre cuite"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"234",
                  "libelle":"[234] Fabrication d'autres produits en céramique et en porcelaine",
                  "children":[
                     {
                        "code":"2341",
                        "libelle":"[2341] Fabrication d'articles céramiques à usage domestique ou ornemental",
                        "children":[
                           {
                              "code":"2341Z",
                              "libelle":"[2341Z] Fabrication d'articles céramiques à usage domestique ou ornemental"
                           }
                        ]
                     },
                     {
                        "code":"2342",
                        "libelle":"[2342] Fabrication d'appareils sanitaires en céramique",
                        "children":[
                           {
                              "code":"2342Z",
                              "libelle":"[2342Z] Fabrication d'appareils sanitaires en céramique"
                           }
                        ]
                     },
                     {
                        "code":"2343",
                        "libelle":"[2343] Fabrication d'isolateurs et pièces isolantes en céramique",
                        "children":[
                           {
                              "code":"2343Z",
                              "libelle":"[2343Z] Fabrication d'isolateurs et pièces isolantes en céramique"
                           }
                        ]
                     },
                     {
                        "code":"2344",
                        "libelle":"[2344] Fabrication d'autres produits céramiques à usage technique",
                        "children":[
                           {
                              "code":"2344Z",
                              "libelle":"[2344Z] Fabrication d'autres produits céramiques à usage technique"
                           }
                        ]
                     },
                     {
                        "code":"2349",
                        "libelle":"[2349] Fabrication d'autres produits céramiques",
                        "children":[
                           {
                              "code":"2349Z",
                              "libelle":"[2349Z] Fabrication d'autres produits céramiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"235",
                  "libelle":"[235] Fabrication de ciment, chaux et plâtre",
                  "children":[
                     {
                        "code":"2351",
                        "libelle":"[2351] Fabrication de ciment",
                        "children":[
                           {
                              "code":"2351Z",
                              "libelle":"[2351Z] Fabrication de ciment"
                           }
                        ]
                     },
                     {
                        "code":"2352",
                        "libelle":"[2352] Fabrication de chaux et plâtre",
                        "children":[
                           {
                              "code":"2352Z",
                              "libelle":"[2352Z] Fabrication de chaux et plâtre"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"236",
                  "libelle":"[236] Fabrication d'ouvrages en béton, en ciment ou en plâtre",
                  "children":[
                     {
                        "code":"2361",
                        "libelle":"[2361] Fabrication d'éléments en béton pour la construction",
                        "children":[
                           {
                              "code":"2361Z",
                              "libelle":"[2361Z] Fabrication d'éléments en béton pour la construction"
                           }
                        ]
                     },
                     {
                        "code":"2362",
                        "libelle":"[2362] Fabrication d'éléments en plâtre pour la construction",
                        "children":[
                           {
                              "code":"2362Z",
                              "libelle":"[2362Z] Fabrication d'éléments en plâtre pour la construction"
                           }
                        ]
                     },
                     {
                        "code":"2363",
                        "libelle":"[2363] Fabrication de béton prêt à l'emploi",
                        "children":[
                           {
                              "code":"2363Z",
                              "libelle":"[2363Z] Fabrication de béton prêt à l'emploi"
                           }
                        ]
                     },
                     {
                        "code":"2364",
                        "libelle":"[2364] Fabrication de mortiers et bétons secs",
                        "children":[
                           {
                              "code":"2364Z",
                              "libelle":"[2364Z] Fabrication de mortiers et bétons secs"
                           }
                        ]
                     },
                     {
                        "code":"2365",
                        "libelle":"[2365] Fabrication d'ouvrages en fibre-ciment",
                        "children":[
                           {
                              "code":"2365Z",
                              "libelle":"[2365Z] Fabrication d'ouvrages en fibre-ciment"
                           }
                        ]
                     },
                     {
                        "code":"2369",
                        "libelle":"[2369] Fabrication d'autres ouvrages en béton, en ciment ou en plâtre",
                        "children":[
                           {
                              "code":"2369Z",
                              "libelle":"[2369Z] Fabrication d'autres ouvrages en béton, en ciment ou en plâtre"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"237",
                  "libelle":"[237] Taille, façonnage et finissage de pierres",
                  "children":[
                     {
                        "code":"2370",
                        "libelle":"[2370] Taille, façonnage et finissage de pierres",
                        "children":[
                           {
                              "code":"2370Z",
                              "libelle":"[2370Z] Taille, façonnage et finissage de pierres"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"239",
                  "libelle":"[239] Fabrication de produits abrasifs et de produits minéraux non métalliques n.c.a.",
                  "children":[
                     {
                        "code":"2391",
                        "libelle":"[2391] Fabrication de produits abrasifs",
                        "children":[
                           {
                              "code":"2391Z",
                              "libelle":"[2391Z] Fabrication de produits abrasifs"
                           }
                        ]
                     },
                     {
                        "code":"2399",
                        "libelle":"[2399] Fabrication d'autres produits minéraux non métalliques n.c.a.",
                        "children":[
                           {
                              "code":"2399Z",
                              "libelle":"[2399Z] Fabrication d'autres produits minéraux non métalliques n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"24",
            "code_section":"C",
            "libelle":"[24] Métallurgie",
            "children":[
               {
                  "code":"241",
                  "libelle":"[241] Sidérurgie",
                  "children":[
                     {
                        "code":"2410",
                        "libelle":"[2410] Sidérurgie",
                        "children":[
                           {
                              "code":"2410Z",
                              "libelle":"[2410Z] Sidérurgie"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"242",
                  "libelle":"[242] Fabrication de tubes, tuyaux, profilés creux et accessoires correspondants en acier",
                  "children":[
                     {
                        "code":"2420",
                        "libelle":"[2420] Fabrication de tubes, tuyaux, profilés creux et accessoires correspondants en acier",
                        "children":[
                           {
                              "code":"2420Z",
                              "libelle":"[2420Z] Fabrication de tubes, tuyaux, profilés creux et accessoires correspondants en acier"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"243",
                  "libelle":"[243] Fabrication d'autres produits de première transformation de l'acier",
                  "children":[
                     {
                        "code":"2431",
                        "libelle":"[2431] Étirage à froid de barres",
                        "children":[
                           {
                              "code":"2431Z",
                              "libelle":"[2431Z] Étirage à froid de barres"
                           }
                        ]
                     },
                     {
                        "code":"2432",
                        "libelle":"[2432] Laminage à froid de feuillards",
                        "children":[
                           {
                              "code":"2432Z",
                              "libelle":"[2432Z] Laminage à froid de feuillards"
                           }
                        ]
                     },
                     {
                        "code":"2433",
                        "libelle":"[2433] Profilage à froid par formage ou pliage",
                        "children":[
                           {
                              "code":"2433Z",
                              "libelle":"[2433Z] Profilage à froid par formage ou pliage"
                           }
                        ]
                     },
                     {
                        "code":"2434",
                        "libelle":"[2434] Tréfilage à froid",
                        "children":[
                           {
                              "code":"2434Z",
                              "libelle":"[2434Z] Tréfilage à froid"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"244",
                  "libelle":"[244] Production de métaux précieux et d'autres métaux non ferreux",
                  "children":[
                     {
                        "code":"2441",
                        "libelle":"[2441] Production de métaux précieux",
                        "children":[
                           {
                              "code":"2441Z",
                              "libelle":"[2441Z] Production de métaux précieux"
                           }
                        ]
                     },
                     {
                        "code":"2442",
                        "libelle":"[2442] Métallurgie de l'aluminium",
                        "children":[
                           {
                              "code":"2442Z",
                              "libelle":"[2442Z] Métallurgie de l'aluminium"
                           }
                        ]
                     },
                     {
                        "code":"2443",
                        "libelle":"[2443] Métallurgie du plomb, du zinc ou de l'étain",
                        "children":[
                           {
                              "code":"2443Z",
                              "libelle":"[2443Z] Métallurgie du plomb, du zinc ou de l'étain"
                           }
                        ]
                     },
                     {
                        "code":"2444",
                        "libelle":"[2444] Métallurgie du cuivre",
                        "children":[
                           {
                              "code":"2444Z",
                              "libelle":"[2444Z] Métallurgie du cuivre"
                           }
                        ]
                     },
                     {
                        "code":"2445",
                        "libelle":"[2445] Métallurgie des autres métaux non ferreux",
                        "children":[
                           {
                              "code":"2445Z",
                              "libelle":"[2445Z] Métallurgie des autres métaux non ferreux"
                           }
                        ]
                     },
                     {
                        "code":"2446",
                        "libelle":"[2446] Élaboration et transformation de matières nucléaires",
                        "children":[
                           {
                              "code":"2446Z",
                              "libelle":"[2446Z] Élaboration et transformation de matières nucléaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"245",
                  "libelle":"[245] Fonderie",
                  "children":[
                     {
                        "code":"2451",
                        "libelle":"[2451] Fonderie de fonte",
                        "children":[
                           {
                              "code":"2451Z",
                              "libelle":"[2451Z] Fonderie de fonte"
                           }
                        ]
                     },
                     {
                        "code":"2452",
                        "libelle":"[2452] Fonderie d'acier",
                        "children":[
                           {
                              "code":"2452Z",
                              "libelle":"[2452Z] Fonderie d'acier"
                           }
                        ]
                     },
                     {
                        "code":"2453",
                        "libelle":"[2453] Fonderie de métaux légers",
                        "children":[
                           {
                              "code":"2453Z",
                              "libelle":"[2453Z] Fonderie de métaux légers"
                           }
                        ]
                     },
                     {
                        "code":"2454",
                        "libelle":"[2454] Fonderie d'autres métaux non ferreux",
                        "children":[
                           {
                              "code":"2454Z",
                              "libelle":"[2454Z] Fonderie d'autres métaux non ferreux"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"25",
            "code_section":"C",
            "libelle":"[25] Fabrication de produits métalliques, à l'exception des machines et des équipements",
            "children":[
               {
                  "code":"251",
                  "libelle":"[251] Fabrication d'éléments en métal pour la construction",
                  "children":[
                     {
                        "code":"2511",
                        "libelle":"[2511] Fabrication de structures métalliques et de parties de structures",
                        "children":[
                           {
                              "code":"2511Z",
                              "libelle":"[2511Z] Fabrication de structures métalliques et de parties de structures"
                           }
                        ]
                     },
                     {
                        "code":"2512",
                        "libelle":"[2512] Fabrication de portes et fenêtres en métal",
                        "children":[
                           {
                              "code":"2512Z",
                              "libelle":"[2512Z] Fabrication de portes et fenêtres en métal"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"252",
                  "libelle":"[252] Fabrication de réservoirs, citernes et conteneurs métalliques",
                  "children":[
                     {
                        "code":"2521",
                        "libelle":"[2521] Fabrication de radiateurs et de chaudières pour le chauffage central",
                        "children":[
                           {
                              "code":"2521Z",
                              "libelle":"[2521Z] Fabrication de radiateurs et de chaudières pour le chauffage central"
                           }
                        ]
                     },
                     {
                        "code":"2529",
                        "libelle":"[2529] Fabrication d'autres réservoirs, citernes et conteneurs métalliques",
                        "children":[
                           {
                              "code":"2529Z",
                              "libelle":"[2529Z] Fabrication d'autres réservoirs, citernes et conteneurs métalliques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"253",
                  "libelle":"[253] Fabrication de générateurs de vapeur, à l'exception des chaudières pour le chauffage central",
                  "children":[
                     {
                        "code":"2530",
                        "libelle":"[2530] Fabrication de générateurs de vapeur, à l'exception des chaudières pour le chauffage central",
                        "children":[
                           {
                              "code":"2530Z",
                              "libelle":"[2530Z] Fabrication de générateurs de vapeur, à l'exception des chaudières pour le chauffage central"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"254",
                  "libelle":"[254] Fabrication d'armes et de munitions",
                  "children":[
                     {
                        "code":"2540",
                        "libelle":"[2540] Fabrication d'armes et de munitions",
                        "children":[
                           {
                              "code":"2540Z",
                              "libelle":"[2540Z] Fabrication d'armes et de munitions"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"255",
                  "libelle":"[255] Forge, emboutissage, estampage ; métallurgie des poudres",
                  "children":[
                     {
                        "code":"2550",
                        "libelle":"[2550] Forge, emboutissage, estampage ; métallurgie des poudres",
                        "children":[
                           {
                              "code":"2550A",
                              "libelle":"[2550A] Forge, estampage, matriçage ; métallurgie des poudres"
                           },
                           {
                              "code":"2550B",
                              "libelle":"[2550B] Découpage, emboutissage"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"256",
                  "libelle":"[256] Traitement et revêtement des métaux ; usinage",
                  "children":[
                     {
                        "code":"2561",
                        "libelle":"[2561] Traitement et revêtement des métaux",
                        "children":[
                           {
                              "code":"2561Z",
                              "libelle":"[2561Z] Traitement et revêtement des métaux"
                           }
                        ]
                     },
                     {
                        "code":"2562",
                        "libelle":"[2562] Usinage",
                        "children":[
                           {
                              "code":"2562A",
                              "libelle":"[2562A] Décolletage"
                           },
                           {
                              "code":"2562B",
                              "libelle":"[2562B] Mécanique industrielle"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"257",
                  "libelle":"[257] Fabrication de coutellerie, d'outillage et de quincaillerie",
                  "children":[
                     {
                        "code":"2571",
                        "libelle":"[2571] Fabrication de coutellerie",
                        "children":[
                           {
                              "code":"2571Z",
                              "libelle":"[2571Z] Fabrication de coutellerie"
                           }
                        ]
                     },
                     {
                        "code":"2572",
                        "libelle":"[2572] Fabrication de serrures et de ferrures",
                        "children":[
                           {
                              "code":"2572Z",
                              "libelle":"[2572Z] Fabrication de serrures et de ferrures"
                           }
                        ]
                     },
                     {
                        "code":"2573",
                        "libelle":"[2573] Fabrication d'outillage",
                        "children":[
                           {
                              "code":"2573A",
                              "libelle":"[2573A] Fabrication de moules et modèles"
                           },
                           {
                              "code":"2573B",
                              "libelle":"[2573B] Fabrication d'autres outillages"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"259",
                  "libelle":"[259] Fabrication d'autres ouvrages en métaux",
                  "children":[
                     {
                        "code":"2591",
                        "libelle":"[2591] Fabrication de fûts et emballages métalliques similaires",
                        "children":[
                           {
                              "code":"2591Z",
                              "libelle":"[2591Z] Fabrication de fûts et emballages métalliques similaires"
                           }
                        ]
                     },
                     {
                        "code":"2592",
                        "libelle":"[2592] Fabrication d'emballages métalliques légers",
                        "children":[
                           {
                              "code":"2592Z",
                              "libelle":"[2592Z] Fabrication d'emballages métalliques légers"
                           }
                        ]
                     },
                     {
                        "code":"2593",
                        "libelle":"[2593] Fabrication d'articles en fils métalliques, de chaînes et de ressorts",
                        "children":[
                           {
                              "code":"2593Z",
                              "libelle":"[2593Z] Fabrication d'articles en fils métalliques, de chaînes et de ressorts"
                           }
                        ]
                     },
                     {
                        "code":"2594",
                        "libelle":"[2594] Fabrication de vis et de boulons",
                        "children":[
                           {
                              "code":"2594Z",
                              "libelle":"[2594Z] Fabrication de vis et de boulons"
                           }
                        ]
                     },
                     {
                        "code":"2599",
                        "libelle":"[2599] Fabrication d'autres produits métalliques n.c.a.",
                        "children":[
                           {
                              "code":"2599A",
                              "libelle":"[2599A] Fabrication d'articles métalliques ménagers"
                           },
                           {
                              "code":"2599B",
                              "libelle":"[2599B] Fabrication d'autres articles métalliques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"26",
            "code_section":"C",
            "libelle":"[26] Fabrication de produits informatiques, électroniques et optiques",
            "children":[
               {
                  "code":"261",
                  "libelle":"[261] Fabrication de composants et cartes électroniques",
                  "children":[
                     {
                        "code":"2611",
                        "libelle":"[2611] Fabrication de composants électroniques",
                        "children":[
                           {
                              "code":"2611Z",
                              "libelle":"[2611Z] Fabrication de composants électroniques"
                           }
                        ]
                     },
                     {
                        "code":"2612",
                        "libelle":"[2612] Fabrication de cartes électroniques assemblées",
                        "children":[
                           {
                              "code":"2612Z",
                              "libelle":"[2612Z] Fabrication de cartes électroniques assemblées"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"262",
                  "libelle":"[262] Fabrication d'ordinateurs et d'équipements périphériques",
                  "children":[
                     {
                        "code":"2620",
                        "libelle":"[2620] Fabrication d'ordinateurs et d'équipements périphériques",
                        "children":[
                           {
                              "code":"2620Z",
                              "libelle":"[2620Z] Fabrication d'ordinateurs et d'équipements périphériques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"263",
                  "libelle":"[263] Fabrication d'équipements de communication",
                  "children":[
                     {
                        "code":"2630",
                        "libelle":"[2630] Fabrication d'équipements de communication",
                        "children":[
                           {
                              "code":"2630Z",
                              "libelle":"[2630Z] Fabrication d'équipements de communication"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"264",
                  "libelle":"[264] Fabrication de produits électroniques grand public",
                  "children":[
                     {
                        "code":"2640",
                        "libelle":"[2640] Fabrication de produits électroniques grand public",
                        "children":[
                           {
                              "code":"2640Z",
                              "libelle":"[2640Z] Fabrication de produits électroniques grand public"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"265",
                  "libelle":"[265] Fabrication d'instruments et d'appareils de mesure, d'essai et de navigation ; horlogerie",
                  "children":[
                     {
                        "code":"2651",
                        "libelle":"[2651] Fabrication d'instruments et d'appareils de mesure, d'essai et de navigation",
                        "children":[
                           {
                              "code":"2651A",
                              "libelle":"[2651A] Fabrication d'équipements d'aide à la navigation"
                           },
                           {
                              "code":"2651B",
                              "libelle":"[2651B] Fabrication d'instrumentation scientifique et technique"
                           }
                        ]
                     },
                     {
                        "code":"2652",
                        "libelle":"[2652] Horlogerie",
                        "children":[
                           {
                              "code":"2652Z",
                              "libelle":"[2652Z] Horlogerie"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"266",
                  "libelle":"[266] Fabrication d'équipements d'irradiation médicale, d'équipements électromédicaux et électrothérapeutiques",
                  "children":[
                     {
                        "code":"2660",
                        "libelle":"[2660] Fabrication d'équipements d'irradiation médicale, d'équipements électromédicaux et électrothérapeutiques",
                        "children":[
                           {
                              "code":"2660Z",
                              "libelle":"[2660Z] Fabrication d'équipements d'irradiation médicale, d'équipements électromédicaux et électrothérapeutiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"267",
                  "libelle":"[267] Fabrication de matériels optique et photographique",
                  "children":[
                     {
                        "code":"2670",
                        "libelle":"[2670] Fabrication de matériels optique et photographique",
                        "children":[
                           {
                              "code":"2670Z",
                              "libelle":"[2670Z] Fabrication de matériels optique et photographique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"268",
                  "libelle":"[268] Fabrication de supports magnétiques et optiques",
                  "children":[
                     {
                        "code":"2680",
                        "libelle":"[2680] Fabrication de supports magnétiques et optiques",
                        "children":[
                           {
                              "code":"2680Z",
                              "libelle":"[2680Z] Fabrication de supports magnétiques et optiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"27",
            "code_section":"C",
            "libelle":"[27] Fabrication d'équipements électriques",
            "children":[
               {
                  "code":"271",
                  "libelle":"[271] Fabrication de moteurs, génératrices et transformateurs électriques et de matériel de distribution et de commande électrique",
                  "children":[
                     {
                        "code":"2711",
                        "libelle":"[2711] Fabrication de moteurs, génératrices et transformateurs électriques",
                        "children":[
                           {
                              "code":"2711Z",
                              "libelle":"[2711Z] Fabrication de moteurs, génératrices et transformateurs électriques"
                           }
                        ]
                     },
                     {
                        "code":"2712",
                        "libelle":"[2712] Fabrication de matériel de distribution et de commande électrique",
                        "children":[
                           {
                              "code":"2712Z",
                              "libelle":"[2712Z] Fabrication de matériel de distribution et de commande électrique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"272",
                  "libelle":"[272] Fabrication de piles et d'accumulateurs électriques",
                  "children":[
                     {
                        "code":"2720",
                        "libelle":"[2720] Fabrication de piles et d'accumulateurs électriques",
                        "children":[
                           {
                              "code":"2720Z",
                              "libelle":"[2720Z] Fabrication de piles et d'accumulateurs électriques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"273",
                  "libelle":"[273] Fabrication de fils et câbles et de matériel d'installation électrique",
                  "children":[
                     {
                        "code":"2731",
                        "libelle":"[2731] Fabrication de câbles de fibres optiques",
                        "children":[
                           {
                              "code":"2731Z",
                              "libelle":"[2731Z] Fabrication de câbles de fibres optiques"
                           }
                        ]
                     },
                     {
                        "code":"2732",
                        "libelle":"[2732] Fabrication d'autres fils et câbles électroniques ou électriques",
                        "children":[
                           {
                              "code":"2732Z",
                              "libelle":"[2732Z] Fabrication d'autres fils et câbles électroniques ou électriques"
                           }
                        ]
                     },
                     {
                        "code":"2733",
                        "libelle":"[2733] Fabrication de matériel d'installation électrique",
                        "children":[
                           {
                              "code":"2733Z",
                              "libelle":"[2733Z] Fabrication de matériel d'installation électrique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"274",
                  "libelle":"[274] Fabrication d'appareils d'éclairage électrique",
                  "children":[
                     {
                        "code":"2740",
                        "libelle":"[2740] Fabrication d'appareils d'éclairage électrique",
                        "children":[
                           {
                              "code":"2740Z",
                              "libelle":"[2740Z] Fabrication d'appareils d'éclairage électrique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"275",
                  "libelle":"[275] Fabrication d'appareils ménagers",
                  "children":[
                     {
                        "code":"2751",
                        "libelle":"[2751] Fabrication d'appareils électroménagers",
                        "children":[
                           {
                              "code":"2751Z",
                              "libelle":"[2751Z] Fabrication d'appareils électroménagers"
                           }
                        ]
                     },
                     {
                        "code":"2752",
                        "libelle":"[2752] Fabrication d'appareils ménagers non électriques",
                        "children":[
                           {
                              "code":"2752Z",
                              "libelle":"[2752Z] Fabrication d'appareils ménagers non électriques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"279",
                  "libelle":"[279] Fabrication d'autres matériels électriques",
                  "children":[
                     {
                        "code":"2790",
                        "libelle":"[2790] Fabrication d'autres matériels électriques",
                        "children":[
                           {
                              "code":"2790Z",
                              "libelle":"[2790Z] Fabrication d'autres matériels électriques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"28",
            "code_section":"C",
            "libelle":"[28] Fabrication de machines et équipements n.c.a.",
            "children":[
               {
                  "code":"281",
                  "libelle":"[281] Fabrication de machines d'usage général",
                  "children":[
                     {
                        "code":"2811",
                        "libelle":"[2811] Fabrication de moteurs et turbines, à l'exception des moteurs d'avions et de véhicules",
                        "children":[
                           {
                              "code":"2811Z",
                              "libelle":"[2811Z] Fabrication de moteurs et turbines, à l'exception des moteurs d'avions et de véhicules"
                           }
                        ]
                     },
                     {
                        "code":"2812",
                        "libelle":"[2812] Fabrication d'équipements hydrauliques et pneumatiques",
                        "children":[
                           {
                              "code":"2812Z",
                              "libelle":"[2812Z] Fabrication d'équipements hydrauliques et pneumatiques"
                           }
                        ]
                     },
                     {
                        "code":"2813",
                        "libelle":"[2813] Fabrication d'autres pompes et compresseurs",
                        "children":[
                           {
                              "code":"2813Z",
                              "libelle":"[2813Z] Fabrication d'autres pompes et compresseurs"
                           }
                        ]
                     },
                     {
                        "code":"2814",
                        "libelle":"[2814] Fabrication d'autres articles de robinetterie",
                        "children":[
                           {
                              "code":"2814Z",
                              "libelle":"[2814Z] Fabrication d'autres articles de robinetterie"
                           }
                        ]
                     },
                     {
                        "code":"2815",
                        "libelle":"[2815] Fabrication d'engrenages et d'organes mécaniques de transmission",
                        "children":[
                           {
                              "code":"2815Z",
                              "libelle":"[2815Z] Fabrication d'engrenages et d'organes mécaniques de transmission"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"282",
                  "libelle":"[282] Fabrication d'autres machines d'usage général",
                  "children":[
                     {
                        "code":"2821",
                        "libelle":"[2821] Fabrication de fours et brûleurs",
                        "children":[
                           {
                              "code":"2821Z",
                              "libelle":"[2821Z] Fabrication de fours et brûleurs"
                           }
                        ]
                     },
                     {
                        "code":"2822",
                        "libelle":"[2822] Fabrication de matériel de levage et de manutention",
                        "children":[
                           {
                              "code":"2822Z",
                              "libelle":"[2822Z] Fabrication de matériel de levage et de manutention"
                           }
                        ]
                     },
                     {
                        "code":"2823",
                        "libelle":"[2823] Fabrication de machines et d'équipements de bureau (à l'exception des ordinateurs et équipements périphériques)",
                        "children":[
                           {
                              "code":"2823Z",
                              "libelle":"[2823Z] Fabrication de machines et d'équipements de bureau (à l'exception des ordinateurs et équipements périphériques)"
                           }
                        ]
                     },
                     {
                        "code":"2824",
                        "libelle":"[2824] Fabrication d'outillage portatif à moteur incorporé",
                        "children":[
                           {
                              "code":"2824Z",
                              "libelle":"[2824Z] Fabrication d'outillage portatif à moteur incorporé"
                           }
                        ]
                     },
                     {
                        "code":"2825",
                        "libelle":"[2825] Fabrication d'équipements aérauliques et frigorifiques industriels",
                        "children":[
                           {
                              "code":"2825Z",
                              "libelle":"[2825Z] Fabrication d'équipements aérauliques et frigorifiques industriels"
                           }
                        ]
                     },
                     {
                        "code":"2829",
                        "libelle":"[2829] Fabrication de machines diverses d'usage général",
                        "children":[
                           {
                              "code":"2829A",
                              "libelle":"[2829A] Fabrication d'équipements d'emballage, de conditionnement et de pesage"
                           },
                           {
                              "code":"2829B",
                              "libelle":"[2829B] Fabrication d'autres machines d'usage général"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"283",
                  "libelle":"[283] Fabrication de machines agricoles et forestières",
                  "children":[
                     {
                        "code":"2830",
                        "libelle":"[2830] Fabrication de machines agricoles et forestières",
                        "children":[
                           {
                              "code":"2830Z",
                              "libelle":"[2830Z] Fabrication de machines agricoles et forestières"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"284",
                  "libelle":"[284] Fabrication de machines de formage des métaux et de machines-outils",
                  "children":[
                     {
                        "code":"2841",
                        "libelle":"[2841] Fabrication de machines de formage des métaux",
                        "children":[
                           {
                              "code":"2841Z",
                              "libelle":"[2841Z] Fabrication de machines-outils pour le travail des métaux"
                           }
                        ]
                     },
                     {
                        "code":"2849",
                        "libelle":"[2849] Fabrication d'autres machines-outils",
                        "children":[
                           {
                              "code":"2849Z",
                              "libelle":"[2849Z] Fabrication d'autres machines-outils"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"289",
                  "libelle":"[289] Fabrication d'autres machines d'usage spécifique",
                  "children":[
                     {
                        "code":"2891",
                        "libelle":"[2891] Fabrication de machines pour la métallurgie",
                        "children":[
                           {
                              "code":"2891Z",
                              "libelle":"[2891Z] Fabrication de machines pour la métallurgie"
                           }
                        ]
                     },
                     {
                        "code":"2892",
                        "libelle":"[2892] Fabrication de machines pour l'extraction ou la construction",
                        "children":[
                           {
                              "code":"2892Z",
                              "libelle":"[2892Z] Fabrication de machines pour l'extraction ou la construction"
                           }
                        ]
                     },
                     {
                        "code":"2893",
                        "libelle":"[2893] Fabrication de machines pour l'industrie agro-alimentaire",
                        "children":[
                           {
                              "code":"2893Z",
                              "libelle":"[2893Z] Fabrication de machines pour l'industrie agro-alimentaire"
                           }
                        ]
                     },
                     {
                        "code":"2894",
                        "libelle":"[2894] Fabrication de machines pour les industries textiles",
                        "children":[
                           {
                              "code":"2894Z",
                              "libelle":"[2894Z] Fabrication de machines pour les industries textiles"
                           }
                        ]
                     },
                     {
                        "code":"2895",
                        "libelle":"[2895] Fabrication de machines pour les industries du papier et du carton",
                        "children":[
                           {
                              "code":"2895Z",
                              "libelle":"[2895Z] Fabrication de machines pour les industries du papier et du carton"
                           }
                        ]
                     },
                     {
                        "code":"2896",
                        "libelle":"[2896] Fabrication de machines pour le travail du caoutchouc ou des plastiques",
                        "children":[
                           {
                              "code":"2896Z",
                              "libelle":"[2896Z] Fabrication de machines pour le travail du caoutchouc ou des plastiques"
                           }
                        ]
                     },
                     {
                        "code":"2899",
                        "libelle":"[2899] Fabrication d'autres machines d'usage spécifique n.c.a.",
                        "children":[
                           {
                              "code":"2899A",
                              "libelle":"[2899A] Fabrication de machines d'imprimerie"
                           },
                           {
                              "code":"2899B",
                              "libelle":"[2899B] Fabrication d'autres machines spécialisées"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"29",
            "code_section":"C",
            "libelle":"[29] Industrie automobile",
            "children":[
               {
                  "code":"291",
                  "libelle":"[291] Construction de véhicules automobiles",
                  "children":[
                     {
                        "code":"2910",
                        "libelle":"[2910] Construction de véhicules automobiles",
                        "children":[
                           {
                              "code":"2910Z",
                              "libelle":"[2910Z] Construction de véhicules automobiles"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"292",
                  "libelle":"[292] Fabrication de carrosseries et remorques",
                  "children":[
                     {
                        "code":"2920",
                        "libelle":"[2920] Fabrication de carrosseries et remorques",
                        "children":[
                           {
                              "code":"2920Z",
                              "libelle":"[2920Z] Fabrication de carrosseries et remorques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"293",
                  "libelle":"[293] Fabrication d'équipements automobiles",
                  "children":[
                     {
                        "code":"2931",
                        "libelle":"[2931] Fabrication d'équipements électriques et électroniques automobiles",
                        "children":[
                           {
                              "code":"2931Z",
                              "libelle":"[2931Z] Fabrication d'équipements électriques et électroniques automobiles"
                           }
                        ]
                     },
                     {
                        "code":"2932",
                        "libelle":"[2932] Fabrication d'autres équipements automobiles",
                        "children":[
                           {
                              "code":"2932Z",
                              "libelle":"[2932Z] Fabrication d'autres équipements automobiles"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"30",
            "code_section":"C",
            "libelle":"[30] Fabrication d'autres matériels de transport",
            "children":[
               {
                  "code":"301",
                  "libelle":"[301] Construction navale",
                  "children":[
                     {
                        "code":"3011",
                        "libelle":"[3011] Construction de navires et de structures flottantes",
                        "children":[
                           {
                              "code":"3011Z",
                              "libelle":"[3011Z] Construction de navires et de structures flottantes"
                           }
                        ]
                     },
                     {
                        "code":"3012",
                        "libelle":"[3012] Construction de bateaux de plaisance",
                        "children":[
                           {
                              "code":"3012Z",
                              "libelle":"[3012Z] Construction de bateaux de plaisance"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"302",
                  "libelle":"[302] Construction de locomotives et d'autre matériel ferroviaire roulant",
                  "children":[
                     {
                        "code":"3020",
                        "libelle":"[3020] Construction de locomotives et d'autre matériel ferroviaire roulant",
                        "children":[
                           {
                              "code":"3020Z",
                              "libelle":"[3020Z] Construction de locomotives et d'autre matériel ferroviaire roulant"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"303",
                  "libelle":"[303] Construction aéronautique et spatiale",
                  "children":[
                     {
                        "code":"3030",
                        "libelle":"[3030] Construction aéronautique et spatiale",
                        "children":[
                           {
                              "code":"3030Z",
                              "libelle":"[3030Z] Construction aéronautique et spatiale"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"304",
                  "libelle":"[304] Construction de véhicules militaires de combat",
                  "children":[
                     {
                        "code":"3040",
                        "libelle":"[3040] Construction de véhicules militaires de combat",
                        "children":[
                           {
                              "code":"3040Z",
                              "libelle":"[3040Z] Construction de véhicules militaires de combat"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"309",
                  "libelle":"[309] Fabrication de matériels de transport n.c.a.",
                  "children":[
                     {
                        "code":"3091",
                        "libelle":"[3091] Fabrication de motocycles",
                        "children":[
                           {
                              "code":"3091Z",
                              "libelle":"[3091Z] Fabrication de motocycles"
                           }
                        ]
                     },
                     {
                        "code":"3092",
                        "libelle":"[3092] Fabrication de bicyclettes et de véhicules pour invalides",
                        "children":[
                           {
                              "code":"3092Z",
                              "libelle":"[3092Z] Fabrication de bicyclettes et de véhicules pour invalides"
                           }
                        ]
                     },
                     {
                        "code":"3099",
                        "libelle":"[3099] Fabrication d'autres équipements de transport n.c.a.",
                        "children":[
                           {
                              "code":"3099Z",
                              "libelle":"[3099Z] Fabrication d'autres équipements de transport n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"31",
            "code_section":"C",
            "libelle":"[31] Fabrication de meubles",
            "children":[
               {
                  "code":"310",
                  "libelle":"[310] Fabrication de meubles",
                  "children":[
                     {
                        "code":"3101",
                        "libelle":"[3101] Fabrication de meubles de bureau et de magasin",
                        "children":[
                           {
                              "code":"3101Z",
                              "libelle":"[3101Z] Fabrication de meubles de bureau et de magasin"
                           }
                        ]
                     },
                     {
                        "code":"3102",
                        "libelle":"[3102] Fabrication de meubles de cuisine",
                        "children":[
                           {
                              "code":"3102Z",
                              "libelle":"[3102Z] Fabrication de meubles de cuisine"
                           }
                        ]
                     },
                     {
                        "code":"3103",
                        "libelle":"[3103] Fabrication de matelas",
                        "children":[
                           {
                              "code":"3103Z",
                              "libelle":"[3103Z] Fabrication de matelas"
                           }
                        ]
                     },
                     {
                        "code":"3109",
                        "libelle":"[3109] Fabrication d'autres meubles",
                        "children":[
                           {
                              "code":"3109A",
                              "libelle":"[3109A] Fabrication de sièges d'ameublement d'intérieur"
                           },
                           {
                              "code":"3109B",
                              "libelle":"[3109B] Fabrication d'autres meubles et industries connexes de l'ameublement"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"32",
            "code_section":"C",
            "libelle":"[32] Autres industries manufacturières",
            "children":[
               {
                  "code":"321",
                  "libelle":"[321] Fabrication d'articles de joaillerie, bijouterie et articles similaires",
                  "children":[
                     {
                        "code":"3211",
                        "libelle":"[3211] Frappe de monnaie",
                        "children":[
                           {
                              "code":"3211Z",
                              "libelle":"[3211Z] Frappe de monnaie"
                           }
                        ]
                     },
                     {
                        "code":"3212",
                        "libelle":"[3212] Fabrication d'articles de joaillerie et bijouterie",
                        "children":[
                           {
                              "code":"3212Z",
                              "libelle":"[3212Z] Fabrication d'articles de joaillerie et bijouterie"
                           }
                        ]
                     },
                     {
                        "code":"3213",
                        "libelle":"[3213] Fabrication d'articles de bijouterie fantaisie et articles similaires",
                        "children":[
                           {
                              "code":"3213Z",
                              "libelle":"[3213Z] Fabrication d'articles de bijouterie fantaisie et articles similaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"322",
                  "libelle":"[322] Fabrication d'instruments de musique",
                  "children":[
                     {
                        "code":"3220",
                        "libelle":"[3220] Fabrication d'instruments de musique",
                        "children":[
                           {
                              "code":"3220Z",
                              "libelle":"[3220Z] Fabrication d'instruments de musique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"323",
                  "libelle":"[323] Fabrication d'articles de sport",
                  "children":[
                     {
                        "code":"3230",
                        "libelle":"[3230] Fabrication d'articles de sport",
                        "children":[
                           {
                              "code":"3230Z",
                              "libelle":"[3230Z] Fabrication d'articles de sport"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"324",
                  "libelle":"[324] Fabrication de jeux et jouets",
                  "children":[
                     {
                        "code":"3240",
                        "libelle":"[3240] Fabrication de jeux et jouets",
                        "children":[
                           {
                              "code":"3240Z",
                              "libelle":"[3240Z] Fabrication de jeux et jouets"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"325",
                  "libelle":"[325] Fabrication d'instruments et de fournitures à usage médical et dentaire",
                  "children":[
                     {
                        "code":"3250",
                        "libelle":"[3250] Fabrication d'instruments et de fournitures à usage médical et dentaire",
                        "children":[
                           {
                              "code":"3250A",
                              "libelle":"[3250A] Fabrication de matériel médico-chirurgical et dentaire"
                           },
                           {
                              "code":"3250B",
                              "libelle":"[3250B] Fabrication de lunettes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"329",
                  "libelle":"[329] Activités manufacturières n.c.a.",
                  "children":[
                     {
                        "code":"3291",
                        "libelle":"[3291] Fabrication d'articles de brosserie",
                        "children":[
                           {
                              "code":"3291Z",
                              "libelle":"[3291Z] Fabrication d'articles de brosserie"
                           }
                        ]
                     },
                     {
                        "code":"3299",
                        "libelle":"[3299] Autres activités manufacturières n.c.a.",
                        "children":[
                           {
                              "code":"3299Z",
                              "libelle":"[3299Z] Autres activités manufacturières n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"33",
            "code_section":"C",
            "libelle":"[33] Réparation et installation de machines et d'équipements",
            "children":[
               {
                  "code":"331",
                  "libelle":"[331] Réparation d'ouvrages en métaux, de machines et d'équipements",
                  "children":[
                     {
                        "code":"3311",
                        "libelle":"[3311] Réparation d'ouvrages en métaux",
                        "children":[
                           {
                              "code":"3311Z",
                              "libelle":"[3311Z] Réparation d'ouvrages en métaux"
                           }
                        ]
                     },
                     {
                        "code":"3312",
                        "libelle":"[3312] Réparation de machines et équipements mécaniques",
                        "children":[
                           {
                              "code":"3312Z",
                              "libelle":"[3312Z] Réparation de machines et équipements mécaniques"
                           }
                        ]
                     },
                     {
                        "code":"3313",
                        "libelle":"[3313] Réparation de matériels électroniques et optiques",
                        "children":[
                           {
                              "code":"3313Z",
                              "libelle":"[3313Z] Réparation de matériels électroniques et optiques"
                           }
                        ]
                     },
                     {
                        "code":"3314",
                        "libelle":"[3314] Réparation d'équipements électriques",
                        "children":[
                           {
                              "code":"3314Z",
                              "libelle":"[3314Z] Réparation d'équipements électriques"
                           }
                        ]
                     },
                     {
                        "code":"3315",
                        "libelle":"[3315] Réparation et maintenance navale",
                        "children":[
                           {
                              "code":"3315Z",
                              "libelle":"[3315Z] Réparation et maintenance navale"
                           }
                        ]
                     },
                     {
                        "code":"3316",
                        "libelle":"[3316] Réparation et maintenance d'aéronefs et d'engins spatiaux",
                        "children":[
                           {
                              "code":"3316Z",
                              "libelle":"[3316Z] Réparation et maintenance d'aéronefs et d'engins spatiaux"
                           }
                        ]
                     },
                     {
                        "code":"3317",
                        "libelle":"[3317] Réparation et maintenance d'autres équipements de transport",
                        "children":[
                           {
                              "code":"3317Z",
                              "libelle":"[3317Z] Réparation et maintenance d'autres équipements de transport"
                           }
                        ]
                     },
                     {
                        "code":"3319",
                        "libelle":"[3319] Réparation d'autres équipements",
                        "children":[
                           {
                              "code":"3319Z",
                              "libelle":"[3319Z] Réparation d'autres équipements"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"332",
                  "libelle":"[332] Installation de machines et d'équipements industriels",
                  "children":[
                     {
                        "code":"3320",
                        "libelle":"[3320] Installation de machines et d'équipements industriels",
                        "children":[
                           {
                              "code":"3320A",
                              "libelle":"[3320A] Installation de structures métalliques, chaudronnées et de tuyauterie"
                           },
                           {
                              "code":"3320B",
                              "libelle":"[3320B] Installation de machines et équipements mécaniques"
                           },
                           {
                              "code":"3320C",
                              "libelle":"[3320C] Conception d'ensemble et assemblage sur site industriel d'équipements de contrôle des processus industriels"
                           },
                           {
                              "code":"3320D",
                              "libelle":"[3320D] Installation d'équipements électriques, de matériels électroniques et optiques ou d'autres matériels"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"D",
      "libelle":"[D] Production et distribution d'électricité, de gaz, de vapeur et d'air conditionné",
      "children":[
         {
            "code":"35",
            "code_section":"D",
            "libelle":"[35] Production et distribution d'électricité, de gaz, de vapeur et d'air conditionné",
            "children":[
               {
                  "code":"351",
                  "libelle":"[351] Production, transport et distribution d'électricité",
                  "children":[
                     {
                        "code":"3511",
                        "libelle":"[3511] Production d'électricité",
                        "children":[
                           {
                              "code":"3511Z",
                              "libelle":"[3511Z] Production d'électricité"
                           }
                        ]
                     },
                     {
                        "code":"3512",
                        "libelle":"[3512] Transport d'électricité",
                        "children":[
                           {
                              "code":"3512Z",
                              "libelle":"[3512Z] Transport d'électricité"
                           }
                        ]
                     },
                     {
                        "code":"3513",
                        "libelle":"[3513] Distribution d'électricité",
                        "children":[
                           {
                              "code":"3513Z",
                              "libelle":"[3513Z] Distribution d'électricité"
                           }
                        ]
                     },
                     {
                        "code":"3514",
                        "libelle":"[3514] Commerce d'électricité",
                        "children":[
                           {
                              "code":"3514Z",
                              "libelle":"[3514Z] Commerce d'électricité"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"352",
                  "libelle":"[352] Production et distribution de combustibles gazeux",
                  "children":[
                     {
                        "code":"3521",
                        "libelle":"[3521] Production de combustibles gazeux",
                        "children":[
                           {
                              "code":"3521Z",
                              "libelle":"[3521Z] Production de combustibles gazeux"
                           }
                        ]
                     },
                     {
                        "code":"3522",
                        "libelle":"[3522] Distribution de combustibles gazeux par conduites",
                        "children":[
                           {
                              "code":"3522Z",
                              "libelle":"[3522Z] Distribution de combustibles gazeux par conduites"
                           }
                        ]
                     },
                     {
                        "code":"3523",
                        "libelle":"[3523] Commerce de combustibles gazeux par conduites",
                        "children":[
                           {
                              "code":"3523Z",
                              "libelle":"[3523Z] Commerce de combustibles gazeux par conduites"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"353",
                  "libelle":"[353] Production et distribution de vapeur et d'air conditionné",
                  "children":[
                     {
                        "code":"3530",
                        "libelle":"[3530] Production et distribution de vapeur et d'air conditionné",
                        "children":[
                           {
                              "code":"3530Z",
                              "libelle":"[3530Z] Production et distribution de vapeur et d'air conditionné"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"E",
      "libelle":"[E] Production et distribution d'eau ; assainissement, gestion des déchets et dépollution",
      "children":[
         {
            "code":"36",
            "code_section":"E",
            "libelle":"[36] Captage, traitement et distribution d'eau",
            "children":[
               {
                  "code":"360",
                  "libelle":"[360] Captage, traitement et distribution d'eau",
                  "children":[
                     {
                        "code":"3600",
                        "libelle":"[3600] Captage, traitement et distribution d'eau",
                        "children":[
                           {
                              "code":"3600Z",
                              "libelle":"[3600Z] Captage, traitement et distribution d'eau"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"37",
            "code_section":"E",
            "libelle":"[37] Collecte et traitement des eaux usées",
            "children":[
               {
                  "code":"370",
                  "libelle":"[370] Collecte et traitement des eaux usées",
                  "children":[
                     {
                        "code":"3700",
                        "libelle":"[3700] Collecte et traitement des eaux usées",
                        "children":[
                           {
                              "code":"3700Z",
                              "libelle":"[3700Z] Collecte et traitement des eaux usées"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"38",
            "code_section":"E",
            "libelle":"[38] Collecte, traitement et élimination des déchets ; récupération",
            "children":[
               {
                  "code":"381",
                  "libelle":"[381] Collecte des déchets",
                  "children":[
                     {
                        "code":"3811",
                        "libelle":"[3811] Collecte des déchets non dangereux",
                        "children":[
                           {
                              "code":"3811Z",
                              "libelle":"[3811Z] Collecte des déchets non dangereux"
                           }
                        ]
                     },
                     {
                        "code":"3812",
                        "libelle":"[3812] Collecte des déchets dangereux",
                        "children":[
                           {
                              "code":"3812Z",
                              "libelle":"[3812Z] Collecte des déchets dangereux"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"382",
                  "libelle":"[382] Traitement et élimination des déchets",
                  "children":[
                     {
                        "code":"3821",
                        "libelle":"[3821] Traitement et élimination des déchets non dangereux",
                        "children":[
                           {
                              "code":"3821Z",
                              "libelle":"[3821Z] Traitement et élimination des déchets non dangereux"
                           }
                        ]
                     },
                     {
                        "code":"3822",
                        "libelle":"[3822] Traitement et élimination des déchets dangereux",
                        "children":[
                           {
                              "code":"3822Z",
                              "libelle":"[3822Z] Traitement et élimination des déchets dangereux"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"383",
                  "libelle":"[383] Récupération",
                  "children":[
                     {
                        "code":"3831",
                        "libelle":"[3831] Démantèlement d'épaves",
                        "children":[
                           {
                              "code":"3831Z",
                              "libelle":"[3831Z] Démantèlement d'épaves"
                           }
                        ]
                     },
                     {
                        "code":"3832",
                        "libelle":"[3832] Récupération de déchets triés",
                        "children":[
                           {
                              "code":"3832Z",
                              "libelle":"[3832Z] Récupération de déchets triés"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"39",
            "code_section":"E",
            "libelle":"[39] Dépollution et autres services de gestion des déchets",
            "children":[
               {
                  "code":"390",
                  "libelle":"[390] Dépollution et autres services de gestion des déchets",
                  "children":[
                     {
                        "code":"3900",
                        "libelle":"[3900] Dépollution et autres services de gestion des déchets",
                        "children":[
                           {
                              "code":"3900Z",
                              "libelle":"[3900Z] Dépollution et autres services de gestion des déchets"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"F",
      "libelle":"[F] Construction",
      "children":[
         {
            "code":"41",
            "code_section":"F",
            "libelle":"[41] Construction de bâtiments",
            "children":[
               {
                  "code":"411",
                  "libelle":"[411] Promotion immobilière",
                  "children":[
                     {
                        "code":"4110",
                        "libelle":"[4110] Promotion immobilière",
                        "children":[
                           {
                              "code":"4110A",
                              "libelle":"[4110A] Promotion immobilière de logements"
                           },
                           {
                              "code":"4110B",
                              "libelle":"[4110B] Promotion immobilière de bureaux"
                           },
                           {
                              "code":"4110C",
                              "libelle":"[4110C] Promotion immobilière d'autres bâtiments"
                           },
                           {
                              "code":"4110D",
                              "libelle":"[4110D] Supports juridiques de programmes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"412",
                  "libelle":"[412] Construction de bâtiments résidentiels et non résidentiels",
                  "children":[
                     {
                        "code":"4120",
                        "libelle":"[4120] Construction de bâtiments résidentiels et non résidentiels",
                        "children":[
                           {
                              "code":"4120A",
                              "libelle":"[4120A] Construction de maisons individuelles"
                           },
                           {
                              "code":"4120B",
                              "libelle":"[4120B] Construction d'autres bâtiments"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"42",
            "code_section":"F",
            "libelle":"[42] Génie civil",
            "children":[
               {
                  "code":"421",
                  "libelle":"[421] Construction de routes et de voies ferrées",
                  "children":[
                     {
                        "code":"4211",
                        "libelle":"[4211] Construction de routes et autoroutes",
                        "children":[
                           {
                              "code":"4211Z",
                              "libelle":"[4211Z] Construction de routes et autoroutes"
                           }
                        ]
                     },
                     {
                        "code":"4212",
                        "libelle":"[4212] Construction de voies ferrées de surface et souterraines",
                        "children":[
                           {
                              "code":"4212Z",
                              "libelle":"[4212Z] Construction de voies ferrées de surface et souterraines"
                           }
                        ]
                     },
                     {
                        "code":"4213",
                        "libelle":"[4213] Construction de ponts et tunnels",
                        "children":[
                           {
                              "code":"4213A",
                              "libelle":"[4213A] Construction d'ouvrages d'art"
                           },
                           {
                              "code":"4213B",
                              "libelle":"[4213B] Construction et entretien de tunnels"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"422",
                  "libelle":"[422] Construction de réseaux et de lignes",
                  "children":[
                     {
                        "code":"4221",
                        "libelle":"[4221] Construction de réseaux pour fluides",
                        "children":[
                           {
                              "code":"4221Z",
                              "libelle":"[4221Z] Construction de réseaux pour fluides"
                           }
                        ]
                     },
                     {
                        "code":"4222",
                        "libelle":"[4222] Construction de réseaux électriques et de télécommunications",
                        "children":[
                           {
                              "code":"4222Z",
                              "libelle":"[4222Z] Construction de réseaux électriques et de télécommunications"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"429",
                  "libelle":"[429] Construction d'autres ouvrages de génie civil",
                  "children":[
                     {
                        "code":"4291",
                        "libelle":"[4291] Construction d'ouvrages maritimes et fluviaux",
                        "children":[
                           {
                              "code":"4291Z",
                              "libelle":"[4291Z] Construction d'ouvrages maritimes et fluviaux"
                           }
                        ]
                     },
                     {
                        "code":"4299",
                        "libelle":"[4299] Construction d'autres ouvrages de génie civil n.c.a.",
                        "children":[
                           {
                              "code":"4299Z",
                              "libelle":"[4299Z] Construction d'autres ouvrages de génie civil n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"43",
            "code_section":"F",
            "libelle":"[43] Travaux de construction spécialisés",
            "children":[
               {
                  "code":"431",
                  "libelle":"[431] Démolition et préparation des sites",
                  "children":[
                     {
                        "code":"4311",
                        "libelle":"[4311] Travaux de démolition",
                        "children":[
                           {
                              "code":"4311Z",
                              "libelle":"[4311Z] Travaux de démolition"
                           }
                        ]
                     },
                     {
                        "code":"4312",
                        "libelle":"[4312] Travaux de préparation des sites",
                        "children":[
                           {
                              "code":"4312A",
                              "libelle":"[4312A] Travaux de terrassement courants et travaux préparatoires"
                           },
                           {
                              "code":"4312B",
                              "libelle":"[4312B] Travaux de terrassement spécialisés ou de grande masse"
                           }
                        ]
                     },
                     {
                        "code":"4313",
                        "libelle":"[4313] Forages et sondages",
                        "children":[
                           {
                              "code":"4313Z",
                              "libelle":"[4313Z] Forages et sondages"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"432",
                  "libelle":"[432] Travaux d'installation électrique, plomberie et autres travaux d'installation",
                  "children":[
                     {
                        "code":"4321",
                        "libelle":"[4321] Installation électrique",
                        "children":[
                           {
                              "code":"4321A",
                              "libelle":"[4321A] Travaux d'installation électrique dans tous locaux"
                           },
                           {
                              "code":"4321B",
                              "libelle":"[4321B] Travaux d'installation électrique sur la voie publique"
                           }
                        ]
                     },
                     {
                        "code":"4322",
                        "libelle":"[4322] Travaux de plomberie et installation de chauffage et de conditionnement d'air",
                        "children":[
                           {
                              "code":"4322A",
                              "libelle":"[4322A] Travaux d'installation d'eau et de gaz en tous locaux"
                           },
                           {
                              "code":"4322B",
                              "libelle":"[4322B] Travaux d'installation d'équipements thermiques et de climatisation"
                           }
                        ]
                     },
                     {
                        "code":"4329",
                        "libelle":"[4329] Autres travaux d'installation",
                        "children":[
                           {
                              "code":"4329A",
                              "libelle":"[4329A] Travaux d'isolation"
                           },
                           {
                              "code":"4329B",
                              "libelle":"[4329B] Autres travaux d'installation n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"433",
                  "libelle":"[433] Travaux de finition",
                  "children":[
                     {
                        "code":"4331",
                        "libelle":"[4331] Travaux de plâtrerie",
                        "children":[
                           {
                              "code":"4331Z",
                              "libelle":"[4331Z] Travaux de plâtrerie"
                           }
                        ]
                     },
                     {
                        "code":"4332",
                        "libelle":"[4332] Travaux de menuiserie",
                        "children":[
                           {
                              "code":"4332A",
                              "libelle":"[4332A] Travaux de menuiserie bois et PVC"
                           },
                           {
                              "code":"4332B",
                              "libelle":"[4332B] Travaux de menuiserie métallique et serrurerie"
                           },
                           {
                              "code":"4332C",
                              "libelle":"[4332C] Agencement de lieux de vente"
                           }
                        ]
                     },
                     {
                        "code":"4333",
                        "libelle":"[4333] Travaux de revêtement des sols et des murs",
                        "children":[
                           {
                              "code":"4333Z",
                              "libelle":"[4333Z] Travaux de revêtement des sols et des murs"
                           }
                        ]
                     },
                     {
                        "code":"4334",
                        "libelle":"[4334] Travaux de peinture et vitrerie",
                        "children":[
                           {
                              "code":"4334Z",
                              "libelle":"[4334Z] Travaux de peinture et vitrerie"
                           }
                        ]
                     },
                     {
                        "code":"4339",
                        "libelle":"[4339] Autres travaux de finition",
                        "children":[
                           {
                              "code":"4339Z",
                              "libelle":"[4339Z] Autres travaux de finition"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"439",
                  "libelle":"[439] Autres travaux de construction spécialisés",
                  "children":[
                     {
                        "code":"4391",
                        "libelle":"[4391] Travaux de couverture",
                        "children":[
                           {
                              "code":"4391A",
                              "libelle":"[4391A] Travaux de charpente"
                           },
                           {
                              "code":"4391B",
                              "libelle":"[4391B] Travaux de couverture par éléments"
                           }
                        ]
                     },
                     {
                        "code":"4399",
                        "libelle":"[4399] Autres travaux de construction spécialisés n.c.a.",
                        "children":[
                           {
                              "code":"4399A",
                              "libelle":"[4399A] Travaux d'étanchéification"
                           },
                           {
                              "code":"4399B",
                              "libelle":"[4399B] Travaux de montage de structures métalliques"
                           },
                           {
                              "code":"4399C",
                              "libelle":"[4399C] Travaux de maçonnerie générale et gros œuvre de bâtiment"
                           },
                           {
                              "code":"4399D",
                              "libelle":"[4399D] Autres travaux spécialisés de construction"
                           },
                           {
                              "code":"4399E",
                              "libelle":"[4399E] Location avec opérateur de matériel de construction"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"G",
      "libelle":"[G] Commerce ; réparation d'automobiles et de motocycles",
      "children":[
         {
            "code":"45",
            "code_section":"G",
            "libelle":"[45] Commerce et réparation d'automobiles et de motocycles",
            "children":[
               {
                  "code":"451",
                  "libelle":"[451] Commerce de véhicules automobiles",
                  "children":[
                     {
                        "code":"4511",
                        "libelle":"[4511] Commerce de voitures et de véhicules automobiles légers",
                        "children":[
                           {
                              "code":"4511Z",
                              "libelle":"[4511Z] Commerce de voitures et de véhicules automobiles légers"
                           }
                        ]
                     },
                     {
                        "code":"4519",
                        "libelle":"[4519] Commerce d'autres véhicules automobiles",
                        "children":[
                           {
                              "code":"4519Z",
                              "libelle":"[4519Z] Commerce d'autres véhicules automobiles"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"452",
                  "libelle":"[452] Entretien et réparation de véhicules automobiles",
                  "children":[
                     {
                        "code":"4520",
                        "libelle":"[4520] Entretien et réparation de véhicules automobiles",
                        "children":[
                           {
                              "code":"4520A",
                              "libelle":"[4520A] Entretien et réparation de véhicules automobiles légers"
                           },
                           {
                              "code":"4520B",
                              "libelle":"[4520B] Entretien et réparation d'autres véhicules automobiles"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"453",
                  "libelle":"[453] Commerce d'équipements automobiles",
                  "children":[
                     {
                        "code":"4531",
                        "libelle":"[4531] Commerce de gros d'équipements automobiles",
                        "children":[
                           {
                              "code":"4531Z",
                              "libelle":"[4531Z] Commerce de gros d'équipements automobiles"
                           }
                        ]
                     },
                     {
                        "code":"4532",
                        "libelle":"[4532] Commerce de détail d'équipements automobiles",
                        "children":[
                           {
                              "code":"4532Z",
                              "libelle":"[4532Z] Commerce de détail d'équipements automobiles"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"454",
                  "libelle":"[454] Commerce et réparation de motocycles",
                  "children":[
                     {
                        "code":"4540",
                        "libelle":"[4540] Commerce et réparation de motocycles",
                        "children":[
                           {
                              "code":"4540Z",
                              "libelle":"[4540Z] Commerce et réparation de motocycles"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"46",
            "code_section":"G",
            "libelle":"[46] Commerce de gros, à l'exception des automobiles et des motocycles",
            "children":[
               {
                  "code":"461",
                  "libelle":"[461] Intermédiaires du commerce de gros",
                  "children":[
                     {
                        "code":"4611",
                        "libelle":"[4611] Intermédiaires du commerce en matières premières agricoles, animaux vivants, matières premières textiles et produits semi-finis",
                        "children":[
                           {
                              "code":"4611Z",
                              "libelle":"[4611Z] Intermédiaires du commerce en matières premières agricoles, animaux vivants, matières premières textiles et produits semi-finis"
                           }
                        ]
                     },
                     {
                        "code":"4612",
                        "libelle":"[4612] Intermédiaires du commerce en combustibles, métaux, minéraux et produits chimiques",
                        "children":[
                           {
                              "code":"4612A",
                              "libelle":"[4612A] Centrales d'achat de carburant"
                           },
                           {
                              "code":"4612B",
                              "libelle":"[4612B] Autres intermédiaires du commerce en combustibles, métaux, minéraux et produits chimiques"
                           }
                        ]
                     },
                     {
                        "code":"4613",
                        "libelle":"[4613] Intermédiaires du commerce en bois et matériaux de construction",
                        "children":[
                           {
                              "code":"4613Z",
                              "libelle":"[4613Z] Intermédiaires du commerce en bois et matériaux de construction"
                           }
                        ]
                     },
                     {
                        "code":"4614",
                        "libelle":"[4614] Intermédiaires du commerce en machines, équipements industriels, navires et avions",
                        "children":[
                           {
                              "code":"4614Z",
                              "libelle":"[4614Z] Intermédiaires du commerce en machines, équipements industriels, navires et avions"
                           }
                        ]
                     },
                     {
                        "code":"4615",
                        "libelle":"[4615] Intermédiaires du commerce en meubles, articles de ménage et quincaillerie",
                        "children":[
                           {
                              "code":"4615Z",
                              "libelle":"[4615Z] Intermédiaires du commerce en meubles, articles de ménage et quincaillerie"
                           }
                        ]
                     },
                     {
                        "code":"4616",
                        "libelle":"[4616] Intermédiaires du commerce en textiles, habillement, fourrures, chaussures et articles en cuir",
                        "children":[
                           {
                              "code":"4616Z",
                              "libelle":"[4616Z] Intermédiaires du commerce en textiles, habillement, fourrures, chaussures et articles en cuir"
                           }
                        ]
                     },
                     {
                        "code":"4617",
                        "libelle":"[4617] Intermédiaires du commerce en denrées, boissons et tabac",
                        "children":[
                           {
                              "code":"4617A",
                              "libelle":"[4617A] Centrales d'achat alimentaires"
                           },
                           {
                              "code":"4617B",
                              "libelle":"[4617B] Autres intermédiaires du commerce en denrées, boissons et tabac"
                           }
                        ]
                     },
                     {
                        "code":"4618",
                        "libelle":"[4618] Intermédiaires spécialisés dans le commerce d'autres produits spécifiques",
                        "children":[
                           {
                              "code":"4618Z",
                              "libelle":"[4618Z] Intermédiaires spécialisés dans le commerce d'autres produits spécifiques"
                           }
                        ]
                     },
                     {
                        "code":"4619",
                        "libelle":"[4619] Intermédiaires du commerce en produits divers",
                        "children":[
                           {
                              "code":"4619A",
                              "libelle":"[4619A] Centrales d'achat non alimentaires"
                           },
                           {
                              "code":"4619B",
                              "libelle":"[4619B] Autres intermédiaires du commerce en produits divers"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"462",
                  "libelle":"[462] Commerce de gros de produits agricoles bruts et d'animaux vivants",
                  "children":[
                     {
                        "code":"4621",
                        "libelle":"[4621] Commerce de gros de céréales, de tabac non manufacturé, de semences et d'aliments pour le bétail",
                        "children":[
                           {
                              "code":"4621Z",
                              "libelle":"[4621Z] Commerce de gros (commerce interentreprises) de céréales, de tabac non manufacturé, de semences et d'aliments pour le bétail"
                           }
                        ]
                     },
                     {
                        "code":"4622",
                        "libelle":"[4622] Commerce de gros de fleurs et plantes",
                        "children":[
                           {
                              "code":"4622Z",
                              "libelle":"[4622Z] Commerce de gros (commerce interentreprises) de fleurs et plantes"
                           }
                        ]
                     },
                     {
                        "code":"4623",
                        "libelle":"[4623] Commerce de gros d'animaux vivants",
                        "children":[
                           {
                              "code":"4623Z",
                              "libelle":"[4623Z] Commerce de gros (commerce interentreprises) d'animaux vivants"
                           }
                        ]
                     },
                     {
                        "code":"4624",
                        "libelle":"[4624] Commerce de gros de cuirs et peaux",
                        "children":[
                           {
                              "code":"4624Z",
                              "libelle":"[4624Z] Commerce de gros (commerce interentreprises) de cuirs et peaux"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"463",
                  "libelle":"[463] Commerce de gros de produits alimentaires, de boissons et de tabac",
                  "children":[
                     {
                        "code":"4631",
                        "libelle":"[4631] Commerce de gros de fruits et légumes",
                        "children":[
                           {
                              "code":"4631Z",
                              "libelle":"[4631Z] Commerce de gros (commerce interentreprises) de fruits et légumes"
                           }
                        ]
                     },
                     {
                        "code":"4632",
                        "libelle":"[4632] Commerce de gros de viandes et de produits à base de viande",
                        "children":[
                           {
                              "code":"4632A",
                              "libelle":"[4632A] Commerce de gros (commerce interentreprises) de viandes de boucherie"
                           },
                           {
                              "code":"4632B",
                              "libelle":"[4632B] Commerce de gros (commerce interentreprises) de produits à base de viande"
                           },
                           {
                              "code":"4632C",
                              "libelle":"[4632C] Commerce de gros (commerce interentreprises) de volailles et gibier"
                           }
                        ]
                     },
                     {
                        "code":"4633",
                        "libelle":"[4633] Commerce de gros de produits laitiers, œufs, huiles et matières grasses comestibles",
                        "children":[
                           {
                              "code":"4633Z",
                              "libelle":"[4633Z] Commerce de gros (commerce interentreprises) de produits laitiers, œufs, huiles et matières grasses comestibles"
                           }
                        ]
                     },
                     {
                        "code":"4634",
                        "libelle":"[4634] Commerce de gros de boissons",
                        "children":[
                           {
                              "code":"4634Z",
                              "libelle":"[4634Z] Commerce de gros (commerce interentreprises) de boissons"
                           }
                        ]
                     },
                     {
                        "code":"4635",
                        "libelle":"[4635] Commerce de gros de produits à base de tabac",
                        "children":[
                           {
                              "code":"4635Z",
                              "libelle":"[4635Z] Commerce de gros (commerce interentreprises) de produits à base de tabac"
                           }
                        ]
                     },
                     {
                        "code":"4636",
                        "libelle":"[4636] Commerce de gros de sucre, chocolat et confiserie",
                        "children":[
                           {
                              "code":"4636Z",
                              "libelle":"[4636Z] Commerce de gros (commerce interentreprises) de sucre, chocolat et confiserie"
                           }
                        ]
                     },
                     {
                        "code":"4637",
                        "libelle":"[4637] Commerce de gros de café, thé, cacao et épices",
                        "children":[
                           {
                              "code":"4637Z",
                              "libelle":"[4637Z] Commerce de gros (commerce interentreprises) de café, thé, cacao et épices"
                           }
                        ]
                     },
                     {
                        "code":"4638",
                        "libelle":"[4638] Commerce de gros d'autres produits alimentaires, y compris poissons, crustacés et mollusques",
                        "children":[
                           {
                              "code":"4638A",
                              "libelle":"[4638A] Commerce de gros (commerce interentreprises) de poissons, crustacés et mollusques"
                           },
                           {
                              "code":"4638B",
                              "libelle":"[4638B] Commerce de gros (commerce interentreprises) alimentaire spécialisé divers"
                           }
                        ]
                     },
                     {
                        "code":"4639",
                        "libelle":"[4639] Commerce de gros non spécialisé de denrées, boissons et tabac",
                        "children":[
                           {
                              "code":"4639A",
                              "libelle":"[4639A] Commerce de gros (commerce interentreprises) de produits surgelés"
                           },
                           {
                              "code":"4639B",
                              "libelle":"[4639B] Commerce de gros (commerce interentreprises) alimentaire non spécialisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"464",
                  "libelle":"[464] Commerce de gros de biens domestiques",
                  "children":[
                     {
                        "code":"4641",
                        "libelle":"[4641] Commerce de gros de textiles",
                        "children":[
                           {
                              "code":"4641Z",
                              "libelle":"[4641Z] Commerce de gros (commerce interentreprises) de textiles"
                           }
                        ]
                     },
                     {
                        "code":"4642",
                        "libelle":"[4642] Commerce de gros d'habillement et de chaussures",
                        "children":[
                           {
                              "code":"4642Z",
                              "libelle":"[4642Z] Commerce de gros (commerce interentreprises) d'habillement et de chaussures"
                           }
                        ]
                     },
                     {
                        "code":"4643",
                        "libelle":"[4643] Commerce de gros d'appareils électroménagers",
                        "children":[
                           {
                              "code":"4643Z",
                              "libelle":"[4643Z] Commerce de gros (commerce interentreprises) d'appareils électroménagers"
                           }
                        ]
                     },
                     {
                        "code":"4644",
                        "libelle":"[4644] Commerce de gros de vaisselle, verrerie et produits d'entretien",
                        "children":[
                           {
                              "code":"4644Z",
                              "libelle":"[4644Z] Commerce de gros (commerce interentreprises) de vaisselle, verrerie et produits d'entretien"
                           }
                        ]
                     },
                     {
                        "code":"4645",
                        "libelle":"[4645] Commerce de gros de parfumerie et de produits de beauté",
                        "children":[
                           {
                              "code":"4645Z",
                              "libelle":"[4645Z] Commerce de gros (commerce interentreprises) de parfumerie et de produits de beauté"
                           }
                        ]
                     },
                     {
                        "code":"4646",
                        "libelle":"[4646] Commerce de gros de produits pharmaceutiques",
                        "children":[
                           {
                              "code":"4646Z",
                              "libelle":"[4646Z] Commerce de gros (commerce interentreprises) de produits pharmaceutiques"
                           }
                        ]
                     },
                     {
                        "code":"4647",
                        "libelle":"[4647] Commerce de gros de meubles, de tapis et d'appareils d'éclairage",
                        "children":[
                           {
                              "code":"4647Z",
                              "libelle":"[4647Z] Commerce de gros (commerce interentreprises) de meubles, de tapis et d'appareils d'éclairage"
                           }
                        ]
                     },
                     {
                        "code":"4648",
                        "libelle":"[4648] Commerce de gros d'articles d'horlogerie et de bijouterie",
                        "children":[
                           {
                              "code":"4648Z",
                              "libelle":"[4648Z] Commerce de gros (commerce interentreprises) d'articles d'horlogerie et de bijouterie"
                           }
                        ]
                     },
                     {
                        "code":"4649",
                        "libelle":"[4649] Commerce de gros d'autres biens domestiques",
                        "children":[
                           {
                              "code":"4649Z",
                              "libelle":"[4649Z] Commerce de gros (commerce interentreprises) d'autres biens domestiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"465",
                  "libelle":"[465] Commerce de gros d'équipements de l'information et de la communication",
                  "children":[
                     {
                        "code":"4651",
                        "libelle":"[4651] Commerce de gros d'ordinateurs, d'équipements informatiques périphériques et de logiciels",
                        "children":[
                           {
                              "code":"4651Z",
                              "libelle":"[4651Z] Commerce de gros (commerce interentreprises) d'ordinateurs, d'équipements informatiques périphériques et de logiciels"
                           }
                        ]
                     },
                     {
                        "code":"4652",
                        "libelle":"[4652] Commerce de gros de composants et d'équipements électroniques et de télécommunication",
                        "children":[
                           {
                              "code":"4652Z",
                              "libelle":"[4652Z] Commerce de gros (commerce interentreprises) de composants et d'équipements électroniques et de télécommunication"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"466",
                  "libelle":"[466] Commerce de gros d'autres équipements industriels",
                  "children":[
                     {
                        "code":"4661",
                        "libelle":"[4661] Commerce de gros de matériel agricole",
                        "children":[
                           {
                              "code":"4661Z",
                              "libelle":"[4661Z] Commerce de gros (commerce interentreprises) de matériel agricole"
                           }
                        ]
                     },
                     {
                        "code":"4662",
                        "libelle":"[4662] Commerce de gros de machines-outils",
                        "children":[
                           {
                              "code":"4662Z",
                              "libelle":"[4662Z] Commerce de gros (commerce interentreprises) de machines-outils"
                           }
                        ]
                     },
                     {
                        "code":"4663",
                        "libelle":"[4663] Commerce de gros de machines pour l'extraction, la construction et le génie civil",
                        "children":[
                           {
                              "code":"4663Z",
                              "libelle":"[4663Z] Commerce de gros (commerce interentreprises) de machines pour l'extraction, la construction et le génie civil"
                           }
                        ]
                     },
                     {
                        "code":"4664",
                        "libelle":"[4664] Commerce de gros de machines pour l'industrie textile et l'habillement",
                        "children":[
                           {
                              "code":"4664Z",
                              "libelle":"[4664Z] Commerce de gros (commerce interentreprises) de machines pour l'industrie textile et l'habillement"
                           }
                        ]
                     },
                     {
                        "code":"4665",
                        "libelle":"[4665] Commerce de gros de mobilier de bureau",
                        "children":[
                           {
                              "code":"4665Z",
                              "libelle":"[4665Z] Commerce de gros (commerce interentreprises) de mobilier de bureau"
                           }
                        ]
                     },
                     {
                        "code":"4666",
                        "libelle":"[4666] Commerce de gros d'autres machines et équipements de bureau",
                        "children":[
                           {
                              "code":"4666Z",
                              "libelle":"[4666Z] Commerce de gros (commerce interentreprises) d'autres machines et équipements de bureau"
                           }
                        ]
                     },
                     {
                        "code":"4669",
                        "libelle":"[4669] Commerce de gros d'autres machines et équipements",
                        "children":[
                           {
                              "code":"4669A",
                              "libelle":"[4669A] Commerce de gros (commerce interentreprises) de matériel électrique"
                           },
                           {
                              "code":"4669B",
                              "libelle":"[4669B] Commerce de gros (commerce interentreprises) de fournitures et équipements industriels divers"
                           },
                           {
                              "code":"4669C",
                              "libelle":"[4669C] Commerce de gros (commerce interentreprises) de fournitures et équipements divers pour le commerce et les services"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"467",
                  "libelle":"[467] Autres commerces de gros spécialisés",
                  "children":[
                     {
                        "code":"4671",
                        "libelle":"[4671] Commerce de gros de combustibles et de produits annexes",
                        "children":[
                           {
                              "code":"4671Z",
                              "libelle":"[4671Z] Commerce de gros (commerce interentreprises) de combustibles et de produits annexes"
                           }
                        ]
                     },
                     {
                        "code":"4672",
                        "libelle":"[4672] Commerce de gros de minerais et métaux",
                        "children":[
                           {
                              "code":"4672Z",
                              "libelle":"[4672Z] Commerce de gros (commerce interentreprises) de minerais et métaux"
                           }
                        ]
                     },
                     {
                        "code":"4673",
                        "libelle":"[4673] Commerce de gros de bois, de matériaux de construction et d'appareils sanitaires",
                        "children":[
                           {
                              "code":"4673A",
                              "libelle":"[4673A] Commerce de gros (commerce interentreprises) de bois et de matériaux de construction"
                           },
                           {
                              "code":"4673B",
                              "libelle":"[4673B] Commerce de gros (commerce interentreprises) d'appareils sanitaires et de produits de décoration"
                           }
                        ]
                     },
                     {
                        "code":"4674",
                        "libelle":"[4674] Commerce de gros de quincaillerie et fournitures pour plomberie et chauffage",
                        "children":[
                           {
                              "code":"4674A",
                              "libelle":"[4674A] Commerce de gros (commerce interentreprises) de quincaillerie"
                           },
                           {
                              "code":"4674B",
                              "libelle":"[4674B] Commerce de gros (commerce interentreprises) de fournitures pour la plomberie et le chauffage"
                           }
                        ]
                     },
                     {
                        "code":"4675",
                        "libelle":"[4675] Commerce de gros de produits chimiques",
                        "children":[
                           {
                              "code":"4675Z",
                              "libelle":"[4675Z] Commerce de gros (commerce interentreprises) de produits chimiques"
                           }
                        ]
                     },
                     {
                        "code":"4676",
                        "libelle":"[4676] Commerce de gros d'autres produits intermédiaires",
                        "children":[
                           {
                              "code":"4676Z",
                              "libelle":"[4676Z] Commerce de gros (commerce interentreprises) d'autres produits intermédiaires"
                           }
                        ]
                     },
                     {
                        "code":"4677",
                        "libelle":"[4677] Commerce de gros de déchets et débris",
                        "children":[
                           {
                              "code":"4677Z",
                              "libelle":"[4677Z] Commerce de gros (commerce interentreprises) de déchets et débris"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"469",
                  "libelle":"[469] Commerce de gros non spécialisé",
                  "children":[
                     {
                        "code":"4690",
                        "libelle":"[4690] Commerce de gros non spécialisé",
                        "children":[
                           {
                              "code":"4690Z",
                              "libelle":"[4690Z] Commerce de gros (commerce interentreprises) non spécialisé"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"47",
            "code_section":"G",
            "libelle":"[47] Commerce de détail, à l'exception des automobiles et des motocycles",
            "children":[
               {
                  "code":"471",
                  "libelle":"[471] Commerce de détail en magasin non spécialisé",
                  "children":[
                     {
                        "code":"4711",
                        "libelle":"[4711] Commerce de détail en magasin non spécialisé à prédominance alimentaire",
                        "children":[
                           {
                              "code":"4711A",
                              "libelle":"[4711A] Commerce de détail de produits surgelés"
                           },
                           {
                              "code":"4711B",
                              "libelle":"[4711B] Commerce d'alimentation générale"
                           },
                           {
                              "code":"4711C",
                              "libelle":"[4711C] Supérettes"
                           },
                           {
                              "code":"4711D",
                              "libelle":"[4711D] Supermarchés"
                           },
                           {
                              "code":"4711E",
                              "libelle":"[4711E] Magasins multi-commerces"
                           },
                           {
                              "code":"4711F",
                              "libelle":"[4711F] Hypermarchés"
                           }
                        ]
                     },
                     {
                        "code":"4719",
                        "libelle":"[4719] Autre commerce de détail en magasin non spécialisé",
                        "children":[
                           {
                              "code":"4719A",
                              "libelle":"[4719A] Grands magasins"
                           },
                           {
                              "code":"4719B",
                              "libelle":"[4719B] Autres commerces de détail en magasin non spécialisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"472",
                  "libelle":"[472] Commerce de détail alimentaire en magasin spécialisé",
                  "children":[
                     {
                        "code":"4721",
                        "libelle":"[4721] Commerce de détail de fruits et légumes en magasin spécialisé",
                        "children":[
                           {
                              "code":"4721Z",
                              "libelle":"[4721Z] Commerce de détail de fruits et légumes en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4722",
                        "libelle":"[4722] Commerce de détail de viandes et de produits à base de viande en magasin spécialisé",
                        "children":[
                           {
                              "code":"4722Z",
                              "libelle":"[4722Z] Commerce de détail de viandes et de produits à base de viande en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4723",
                        "libelle":"[4723] Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé",
                        "children":[
                           {
                              "code":"4723Z",
                              "libelle":"[4723Z] Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4724",
                        "libelle":"[4724] Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé",
                        "children":[
                           {
                              "code":"4724Z",
                              "libelle":"[4724Z] Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4725",
                        "libelle":"[4725] Commerce de détail de boissons en magasin spécialisé",
                        "children":[
                           {
                              "code":"4725Z",
                              "libelle":"[4725Z] Commerce de détail de boissons en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4726",
                        "libelle":"[4726] Commerce de détail de produits à base de tabac en magasin spécialisé",
                        "children":[
                           {
                              "code":"4726Z",
                              "libelle":"[4726Z] Commerce de détail de produits à base de tabac en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4729",
                        "libelle":"[4729] Autres commerces de détail alimentaires en magasin spécialisé",
                        "children":[
                           {
                              "code":"4729Z",
                              "libelle":"[4729Z] Autres commerces de détail alimentaires en magasin spécialisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"473",
                  "libelle":"[473] Commerce de détail de carburants en magasin spécialisé",
                  "children":[
                     {
                        "code":"4730",
                        "libelle":"[4730] Commerce de détail de carburants en magasin spécialisé",
                        "children":[
                           {
                              "code":"4730Z",
                              "libelle":"[4730Z] Commerce de détail de carburants en magasin spécialisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"474",
                  "libelle":"[474] Commerce de détail d'équipements de l'information et de la communication en magasin spécialisé",
                  "children":[
                     {
                        "code":"4741",
                        "libelle":"[4741] Commerce de détail d'ordinateurs, d'unités périphériques et de logiciels en magasin spécialisé",
                        "children":[
                           {
                              "code":"4741Z",
                              "libelle":"[4741Z] Commerce de détail d'ordinateurs, d'unités périphériques et de logiciels en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4742",
                        "libelle":"[4742] Commerce de détail de matériels de télécommunication en magasin spécialisé",
                        "children":[
                           {
                              "code":"4742Z",
                              "libelle":"[4742Z] Commerce de détail de matériels de télécommunication en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4743",
                        "libelle":"[4743] Commerce de détail de matériels audio/vidéo en magasin spécialisé",
                        "children":[
                           {
                              "code":"4743Z",
                              "libelle":"[4743Z] Commerce de détail de matériels audio et vidéo en magasin spécialisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"475",
                  "libelle":"[475] Commerce de détail d'autres équipements du foyer en magasin spécialisé",
                  "children":[
                     {
                        "code":"4751",
                        "libelle":"[4751] Commerce de détail de textiles en magasin spécialisé",
                        "children":[
                           {
                              "code":"4751Z",
                              "libelle":"[4751Z] Commerce de détail de textiles en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4752",
                        "libelle":"[4752] Commerce de détail de quincaillerie, peintures et verres en magasin spécialisé",
                        "children":[
                           {
                              "code":"4752A",
                              "libelle":"[4752A] Commerce de détail de quincaillerie, peintures et verres en petites surfaces (moins de 400 m²)"
                           },
                           {
                              "code":"4752B",
                              "libelle":"[4752B] Commerce de détail de quincaillerie, peintures et verres en grandes surfaces (400 m² et plus)"
                           }
                        ]
                     },
                     {
                        "code":"4753",
                        "libelle":"[4753] Commerce de détail de tapis, moquettes et revêtements de murs et de sols en magasin spécialisé",
                        "children":[
                           {
                              "code":"4753Z",
                              "libelle":"[4753Z] Commerce de détail de tapis, moquettes et revêtements de murs et de sols en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4754",
                        "libelle":"[4754] Commerce de détail d'appareils électroménagers en magasin spécialisé",
                        "children":[
                           {
                              "code":"4754Z",
                              "libelle":"[4754Z] Commerce de détail d'appareils électroménagers en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4759",
                        "libelle":"[4759] Commerce de détail de meubles, appareils d'éclairage et autres articles de ménage en magasin spécialisé",
                        "children":[
                           {
                              "code":"4759A",
                              "libelle":"[4759A] Commerce de détail de meubles"
                           },
                           {
                              "code":"4759B",
                              "libelle":"[4759B] Commerce de détail d'autres équipements du foyer"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"476",
                  "libelle":"[476] Commerce de détail de biens culturels et de loisirs en magasin spécialisé",
                  "children":[
                     {
                        "code":"4761",
                        "libelle":"[4761] Commerce de détail de livres en magasin spécialisé",
                        "children":[
                           {
                              "code":"4761Z",
                              "libelle":"[4761Z] Commerce de détail de livres en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4762",
                        "libelle":"[4762] Commerce de détail de journaux et papeterie en magasin spécialisé",
                        "children":[
                           {
                              "code":"4762Z",
                              "libelle":"[4762Z] Commerce de détail de journaux et papeterie en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4763",
                        "libelle":"[4763] Commerce de détail d'enregistrements musicaux et vidéo en magasin spécialisé",
                        "children":[
                           {
                              "code":"4763Z",
                              "libelle":"[4763Z] Commerce de détail d'enregistrements musicaux et vidéo en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4764",
                        "libelle":"[4764] Commerce de détail d'articles de sport en magasin spécialisé",
                        "children":[
                           {
                              "code":"4764Z",
                              "libelle":"[4764Z] Commerce de détail d'articles de sport en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4765",
                        "libelle":"[4765] Commerce de détail de jeux et jouets en magasin spécialisé",
                        "children":[
                           {
                              "code":"4765Z",
                              "libelle":"[4765Z] Commerce de détail de jeux et jouets en magasin spécialisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"477",
                  "libelle":"[477] Autres commerces de détail en magasin spécialisé",
                  "children":[
                     {
                        "code":"4771",
                        "libelle":"[4771] Commerce de détail d'habillement en magasin spécialisé",
                        "children":[
                           {
                              "code":"4771Z",
                              "libelle":"[4771Z] Commerce de détail d'habillement en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4772",
                        "libelle":"[4772] Commerce de détail de chaussures et d'articles en cuir en magasin spécialisé",
                        "children":[
                           {
                              "code":"4772A",
                              "libelle":"[4772A] Commerce de détail de la chaussure"
                           },
                           {
                              "code":"4772B",
                              "libelle":"[4772B] Commerce de détail de maroquinerie et d'articles de voyage"
                           }
                        ]
                     },
                     {
                        "code":"4773",
                        "libelle":"[4773] Commerce de détail de produits pharmaceutiques en magasin spécialisé",
                        "children":[
                           {
                              "code":"4773Z",
                              "libelle":"[4773Z] Commerce de détail de produits pharmaceutiques en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4774",
                        "libelle":"[4774] Commerce de détail d'articles médicaux et orthopédiques en magasin spécialisé",
                        "children":[
                           {
                              "code":"4774Z",
                              "libelle":"[4774Z] Commerce de détail d'articles médicaux et orthopédiques en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4775",
                        "libelle":"[4775] Commerce de détail de parfumerie et de produits de beauté en magasin spécialisé",
                        "children":[
                           {
                              "code":"4775Z",
                              "libelle":"[4775Z] Commerce de détail de parfumerie et de produits de beauté en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4776",
                        "libelle":"[4776] Commerce de détail de fleurs, plantes, graines, engrais, animaux de compagnie et aliments pour ces animaux en magasin spécialisé",
                        "children":[
                           {
                              "code":"4776Z",
                              "libelle":"[4776Z] Commerce de détail de fleurs, plantes, graines, engrais, animaux de compagnie et aliments pour ces animaux en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4777",
                        "libelle":"[4777] Commerce de détail d'articles d'horlogerie et de bijouterie en magasin spécialisé",
                        "children":[
                           {
                              "code":"4777Z",
                              "libelle":"[4777Z] Commerce de détail d'articles d'horlogerie et de bijouterie en magasin spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4778",
                        "libelle":"[4778] Autre commerce de détail de biens neufs en magasin spécialisé",
                        "children":[
                           {
                              "code":"4778A",
                              "libelle":"[4778A] Commerces de détail d'optique"
                           },
                           {
                              "code":"4778B",
                              "libelle":"[4778B] Commerces de détail de charbons et combustibles"
                           },
                           {
                              "code":"4778C",
                              "libelle":"[4778C] Autres commerces de détail spécialisés divers"
                           }
                        ]
                     },
                     {
                        "code":"4779",
                        "libelle":"[4779] Commerce de détail de biens d'occasion en magasin",
                        "children":[
                           {
                              "code":"4779Z",
                              "libelle":"[4779Z] Commerce de détail de biens d'occasion en magasin"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"478",
                  "libelle":"[478] Commerce de détail sur éventaires et marchés",
                  "children":[
                     {
                        "code":"4781",
                        "libelle":"[4781] Commerce de détail alimentaire sur éventaires et marchés",
                        "children":[
                           {
                              "code":"4781Z",
                              "libelle":"[4781Z] Commerce de détail alimentaire sur éventaires et marchés"
                           }
                        ]
                     },
                     {
                        "code":"4782",
                        "libelle":"[4782] Commerce de détail de textiles, d'habillement et de chaussures sur éventaires et marchés",
                        "children":[
                           {
                              "code":"4782Z",
                              "libelle":"[4782Z] Commerce de détail de textiles, d'habillement et de chaussures sur éventaires et marchés"
                           }
                        ]
                     },
                     {
                        "code":"4789",
                        "libelle":"[4789] Autres commerces de détail sur éventaires et marchés",
                        "children":[
                           {
                              "code":"4789Z",
                              "libelle":"[4789Z] Autres commerces de détail sur éventaires et marchés"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"479",
                  "libelle":"[479] Commerce de détail hors magasin, éventaires ou marchés",
                  "children":[
                     {
                        "code":"4791",
                        "libelle":"[4791] Vente à distance",
                        "children":[
                           {
                              "code":"4791A",
                              "libelle":"[4791A] Vente à distance sur catalogue général"
                           },
                           {
                              "code":"4791B",
                              "libelle":"[4791B] Vente à distance sur catalogue spécialisé"
                           }
                        ]
                     },
                     {
                        "code":"4799",
                        "libelle":"[4799] Autres commerces de détail hors magasin, éventaires ou marchés",
                        "children":[
                           {
                              "code":"4799A",
                              "libelle":"[4799A] Vente à domicile"
                           },
                           {
                              "code":"4799B",
                              "libelle":"[4799B] Vente par automates et autres commerces de détail hors magasin, éventaires ou marchés n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"H",
      "libelle":"[H] Transports et entreposage",
      "children":[
         {
            "code":"49",
            "code_section":"H",
            "libelle":"[49] Transports terrestres et transport par conduites",
            "children":[
               {
                  "code":"491",
                  "libelle":"[491] Transport ferroviaire interurbain de voyageurs",
                  "children":[
                     {
                        "code":"4910",
                        "libelle":"[4910] Transport ferroviaire interurbain de voyageurs",
                        "children":[
                           {
                              "code":"4910Z",
                              "libelle":"[4910Z] Transport ferroviaire interurbain de voyageurs"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"492",
                  "libelle":"[492] Transports ferroviaires de fret",
                  "children":[
                     {
                        "code":"4920",
                        "libelle":"[4920] Transports ferroviaires de fret",
                        "children":[
                           {
                              "code":"4920Z",
                              "libelle":"[4920Z] Transports ferroviaires de fret"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"493",
                  "libelle":"[493] Autres transports terrestres de voyageurs",
                  "children":[
                     {
                        "code":"4931",
                        "libelle":"[4931] Transports urbains et suburbains de voyageurs",
                        "children":[
                           {
                              "code":"4931Z",
                              "libelle":"[4931Z] Transports urbains et suburbains de voyageurs"
                           }
                        ]
                     },
                     {
                        "code":"4932",
                        "libelle":"[4932] Transports de voyageurs par taxis",
                        "children":[
                           {
                              "code":"4932Z",
                              "libelle":"[4932Z] Transports de voyageurs par taxis"
                           }
                        ]
                     },
                     {
                        "code":"4939",
                        "libelle":"[4939] Autres transports terrestres de voyageurs n.c.a.",
                        "children":[
                           {
                              "code":"4939A",
                              "libelle":"[4939A] Transports routiers réguliers de voyageurs"
                           },
                           {
                              "code":"4939B",
                              "libelle":"[4939B] Autres transports routiers de voyageurs"
                           },
                           {
                              "code":"4939C",
                              "libelle":"[4939C] Téléphériques et remontées mécaniques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"494",
                  "libelle":"[494] Transports routiers de fret et services de déménagement",
                  "children":[
                     {
                        "code":"4941",
                        "libelle":"[4941] Transports routiers de fret",
                        "children":[
                           {
                              "code":"4941A",
                              "libelle":"[4941A] Transports routiers de fret interurbains"
                           },
                           {
                              "code":"4941B",
                              "libelle":"[4941B] Transports routiers de fret de proximité"
                           },
                           {
                              "code":"4941C",
                              "libelle":"[4941C] Location de camions avec chauffeur"
                           }
                        ]
                     },
                     {
                        "code":"4942",
                        "libelle":"[4942] Services de déménagement",
                        "children":[
                           {
                              "code":"4942Z",
                              "libelle":"[4942Z] Services de déménagement"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"495",
                  "libelle":"[495] Transports par conduites",
                  "children":[
                     {
                        "code":"4950",
                        "libelle":"[4950] Transports par conduites",
                        "children":[
                           {
                              "code":"4950Z",
                              "libelle":"[4950Z] Transports par conduites"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"50",
            "code_section":"H",
            "libelle":"[50] Transports par eau",
            "children":[
               {
                  "code":"501",
                  "libelle":"[501] Transports maritimes et côtiers de passagers",
                  "children":[
                     {
                        "code":"5010",
                        "libelle":"[5010] Transports maritimes et côtiers de passagers",
                        "children":[
                           {
                              "code":"5010Z",
                              "libelle":"[5010Z] Transports maritimes et côtiers de passagers"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"502",
                  "libelle":"[502] Transports maritimes et côtiers de fret",
                  "children":[
                     {
                        "code":"5020",
                        "libelle":"[5020] Transports maritimes et côtiers de fret",
                        "children":[
                           {
                              "code":"5020Z",
                              "libelle":"[5020Z] Transports maritimes et côtiers de fret"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"503",
                  "libelle":"[503] Transports fluviaux de passagers",
                  "children":[
                     {
                        "code":"5030",
                        "libelle":"[5030] Transports fluviaux de passagers",
                        "children":[
                           {
                              "code":"5030Z",
                              "libelle":"[5030Z] Transports fluviaux de passagers"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"504",
                  "libelle":"[504] Transports fluviaux de fret",
                  "children":[
                     {
                        "code":"5040",
                        "libelle":"[5040] Transports fluviaux de fret",
                        "children":[
                           {
                              "code":"5040Z",
                              "libelle":"[5040Z] Transports fluviaux de fret"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"51",
            "code_section":"H",
            "libelle":"[51] Transports aériens",
            "children":[
               {
                  "code":"511",
                  "libelle":"[511] Transports aériens de passagers",
                  "children":[
                     {
                        "code":"5110",
                        "libelle":"[5110] Transports aériens de passagers",
                        "children":[
                           {
                              "code":"5110Z",
                              "libelle":"[5110Z] Transports aériens de passagers"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"512",
                  "libelle":"[512] Transports aériens de fret et transports spatiaux",
                  "children":[
                     {
                        "code":"5121",
                        "libelle":"[5121] Transports aériens de fret",
                        "children":[
                           {
                              "code":"5121Z",
                              "libelle":"[5121Z] Transports aériens de fret"
                           }
                        ]
                     },
                     {
                        "code":"5122",
                        "libelle":"[5122] Transports spatiaux",
                        "children":[
                           {
                              "code":"5122Z",
                              "libelle":"[5122Z] Transports spatiaux"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"52",
            "code_section":"H",
            "libelle":"[52] Entreposage et services auxiliaires des transports",
            "children":[
               {
                  "code":"521",
                  "libelle":"[521] Entreposage et stockage",
                  "children":[
                     {
                        "code":"5210",
                        "libelle":"[5210] Entreposage et stockage",
                        "children":[
                           {
                              "code":"5210A",
                              "libelle":"[5210A] Entreposage et stockage frigorifique"
                           },
                           {
                              "code":"5210B",
                              "libelle":"[5210B] Entreposage et stockage non frigorifique"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"522",
                  "libelle":"[522] Services auxiliaires des transports",
                  "children":[
                     {
                        "code":"5221",
                        "libelle":"[5221] Services auxiliaires des transports terrestres",
                        "children":[
                           {
                              "code":"5221Z",
                              "libelle":"[5221Z] Services auxiliaires des transports terrestres"
                           }
                        ]
                     },
                     {
                        "code":"5222",
                        "libelle":"[5222] Services auxiliaires des transports par eau",
                        "children":[
                           {
                              "code":"5222Z",
                              "libelle":"[5222Z] Services auxiliaires des transports par eau"
                           }
                        ]
                     },
                     {
                        "code":"5223",
                        "libelle":"[5223] Services auxiliaires des transports aériens",
                        "children":[
                           {
                              "code":"5223Z",
                              "libelle":"[5223Z] Services auxiliaires des transports aériens"
                           }
                        ]
                     },
                     {
                        "code":"5224",
                        "libelle":"[5224] Manutention",
                        "children":[
                           {
                              "code":"5224A",
                              "libelle":"[5224A] Manutention portuaire"
                           },
                           {
                              "code":"5224B",
                              "libelle":"[5224B] Manutention non portuaire"
                           }
                        ]
                     },
                     {
                        "code":"5229",
                        "libelle":"[5229] Autres services auxiliaires des transports",
                        "children":[
                           {
                              "code":"5229A",
                              "libelle":"[5229A] Messagerie, fret express"
                           },
                           {
                              "code":"5229B",
                              "libelle":"[5229B] Affrètement et organisation des transports"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"53",
            "code_section":"H",
            "libelle":"[53] Activités de poste et de courrier",
            "children":[
               {
                  "code":"531",
                  "libelle":"[531] Activités de poste dans le cadre d'une obligation de service universel",
                  "children":[
                     {
                        "code":"5310",
                        "libelle":"[5310] Activités de poste dans le cadre d'une obligation de service universel",
                        "children":[
                           {
                              "code":"5310Z",
                              "libelle":"[5310Z] Activités de poste dans le cadre d'une obligation de service universel"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"532",
                  "libelle":"[532] Autres activités de poste et de courrier",
                  "children":[
                     {
                        "code":"5320",
                        "libelle":"[5320] Autres activités de poste et de courrier",
                        "children":[
                           {
                              "code":"5320Z",
                              "libelle":"[5320Z] Autres activités de poste et de courrier"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"I",
      "libelle":"[I] Hébergement et restauration",
      "children":[
         {
            "code":"55",
            "code_section":"I",
            "libelle":"[55] Hébergement",
            "children":[
               {
                  "code":"551",
                  "libelle":"[551] Hôtels et hébergement similaire",
                  "children":[
                     {
                        "code":"5510",
                        "libelle":"[5510] Hôtels et hébergement similaire",
                        "children":[
                           {
                              "code":"5510Z",
                              "libelle":"[5510Z] Hôtels et hébergement similaire"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"552",
                  "libelle":"[552] Hébergement touristique et autre hébergement de courte durée",
                  "children":[
                     {
                        "code":"5520",
                        "libelle":"[5520] Hébergement touristique et autre hébergement de courte durée",
                        "children":[
                           {
                              "code":"5520Z",
                              "libelle":"[5520Z] Hébergement touristique et autre hébergement de courte durée"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"553",
                  "libelle":"[553] Terrains de camping et parcs pour caravanes ou véhicules de loisirs",
                  "children":[
                     {
                        "code":"5530",
                        "libelle":"[5530] Terrains de camping et parcs pour caravanes ou véhicules de loisirs",
                        "children":[
                           {
                              "code":"5530Z",
                              "libelle":"[5530Z] Terrains de camping et parcs pour caravanes ou véhicules de loisirs"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"559",
                  "libelle":"[559] Autres hébergements",
                  "children":[
                     {
                        "code":"5590",
                        "libelle":"[5590] Autres hébergements",
                        "children":[
                           {
                              "code":"5590Z",
                              "libelle":"[5590Z] Autres hébergements"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"56",
            "code_section":"I",
            "libelle":"[56] Restauration",
            "children":[
               {
                  "code":"561",
                  "libelle":"[561] Restaurants et services de restauration mobile",
                  "children":[
                     {
                        "code":"5610",
                        "libelle":"[5610] Restaurants et services de restauration mobile",
                        "children":[
                           {
                              "code":"5610A",
                              "libelle":"[5610A] Restauration traditionnelle"
                           },
                           {
                              "code":"5610B",
                              "libelle":"[5610B] Cafétérias et autres libres-services"
                           },
                           {
                              "code":"5610C",
                              "libelle":"[5610C] Restauration de type rapide"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"562",
                  "libelle":"[562] Traiteurs et autres services de restauration",
                  "children":[
                     {
                        "code":"5621",
                        "libelle":"[5621] Services des traiteurs",
                        "children":[
                           {
                              "code":"5621Z",
                              "libelle":"[5621Z] Services des traiteurs"
                           }
                        ]
                     },
                     {
                        "code":"5629",
                        "libelle":"[5629] Autres services de restauration",
                        "children":[
                           {
                              "code":"5629A",
                              "libelle":"[5629A] Restauration collective sous contrat"
                           },
                           {
                              "code":"5629B",
                              "libelle":"[5629B] Autres services de restauration n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"563",
                  "libelle":"[563] Débits de boissons",
                  "children":[
                     {
                        "code":"5630",
                        "libelle":"[5630] Débits de boissons",
                        "children":[
                           {
                              "code":"5630Z",
                              "libelle":"[5630Z] Débits de boissons"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"J",
      "libelle":"[J] Information et communication",
      "children":[
         {
            "code":"58",
            "code_section":"J",
            "libelle":"[58] Édition",
            "children":[
               {
                  "code":"581",
                  "libelle":"[581] Édition de livres et périodiques et autres activités d'édition",
                  "children":[
                     {
                        "code":"5811",
                        "libelle":"[5811] Édition de livres",
                        "children":[
                           {
                              "code":"5811Z",
                              "libelle":"[5811Z] Édition de livres"
                           }
                        ]
                     },
                     {
                        "code":"5812",
                        "libelle":"[5812] Édition de répertoires et de fichiers d'adresses",
                        "children":[
                           {
                              "code":"5812Z",
                              "libelle":"[5812Z] Édition de répertoires et de fichiers d'adresses"
                           }
                        ]
                     },
                     {
                        "code":"5813",
                        "libelle":"[5813] Édition de journaux",
                        "children":[
                           {
                              "code":"5813Z",
                              "libelle":"[5813Z] Édition de journaux"
                           }
                        ]
                     },
                     {
                        "code":"5814",
                        "libelle":"[5814] Édition de revues et périodiques",
                        "children":[
                           {
                              "code":"5814Z",
                              "libelle":"[5814Z] Édition de revues et périodiques"
                           }
                        ]
                     },
                     {
                        "code":"5819",
                        "libelle":"[5819] Autres activités d'édition",
                        "children":[
                           {
                              "code":"5819Z",
                              "libelle":"[5819Z] Autres activités d'édition"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"582",
                  "libelle":"[582] Édition de logiciels",
                  "children":[
                     {
                        "code":"5821",
                        "libelle":"[5821] Édition de jeux électroniques",
                        "children":[
                           {
                              "code":"5821Z",
                              "libelle":"[5821Z] Édition de jeux électroniques"
                           }
                        ]
                     },
                     {
                        "code":"5829",
                        "libelle":"[5829] Édition d'autres logiciels",
                        "children":[
                           {
                              "code":"5829A",
                              "libelle":"[5829A] Édition de logiciels système et de réseau"
                           },
                           {
                              "code":"5829B",
                              "libelle":"[5829B] Édition de logiciels outils de développement et de langages"
                           },
                           {
                              "code":"5829C",
                              "libelle":"[5829C] Édition de logiciels applicatifs"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"59",
            "code_section":"J",
            "libelle":"[59] Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale",
            "children":[
               {
                  "code":"591",
                  "libelle":"[591] Activités cinématographiques, vidéo et de télévision",
                  "children":[
                     {
                        "code":"5911",
                        "libelle":"[5911] Production de films cinématographiques, de vidéo et de programmes de télévision",
                        "children":[
                           {
                              "code":"5911A",
                              "libelle":"[5911A] Production de films et de programmes pour la télévision"
                           },
                           {
                              "code":"5911B",
                              "libelle":"[5911B] Production de films institutionnels et publicitaires"
                           },
                           {
                              "code":"5911C",
                              "libelle":"[5911C] Production de films pour le cinéma"
                           }
                        ]
                     },
                     {
                        "code":"5912",
                        "libelle":"[5912] Post-production de films cinématographiques, de vidéo et de programmes de télévision",
                        "children":[
                           {
                              "code":"5912Z",
                              "libelle":"[5912Z] Post-production de films cinématographiques, de vidéo et de programmes de télévision"
                           }
                        ]
                     },
                     {
                        "code":"5913",
                        "libelle":"[5913] Distribution de films cinématographiques, de vidéo et de programmes de télévision",
                        "children":[
                           {
                              "code":"5913A",
                              "libelle":"[5913A] Distribution de films cinématographiques"
                           },
                           {
                              "code":"5913B",
                              "libelle":"[5913B] Édition et distribution vidéo"
                           }
                        ]
                     },
                     {
                        "code":"5914",
                        "libelle":"[5914] Projection de films cinématographiques",
                        "children":[
                           {
                              "code":"5914Z",
                              "libelle":"[5914Z] Projection de films cinématographiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"592",
                  "libelle":"[592] Enregistrement sonore et édition musicale",
                  "children":[
                     {
                        "code":"5920",
                        "libelle":"[5920] Enregistrement sonore et édition musicale",
                        "children":[
                           {
                              "code":"5920Z",
                              "libelle":"[5920Z] Enregistrement sonore et édition musicale"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"60",
            "code_section":"J",
            "libelle":"[60] Programmation et diffusion",
            "children":[
               {
                  "code":"601",
                  "libelle":"[601] Édition et diffusion de programmes radio",
                  "children":[
                     {
                        "code":"6010",
                        "libelle":"[6010] Édition et diffusion de programmes radio",
                        "children":[
                           {
                              "code":"6010Z",
                              "libelle":"[6010Z] Édition et diffusion de programmes radio"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"602",
                  "libelle":"[602] Programmation de télévision et télédiffusion",
                  "children":[
                     {
                        "code":"6020",
                        "libelle":"[6020] Programmation de télévision et télédiffusion",
                        "children":[
                           {
                              "code":"6020A",
                              "libelle":"[6020A] Édition de chaînes généralistes"
                           },
                           {
                              "code":"6020B",
                              "libelle":"[6020B] Édition de chaînes thématiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"61",
            "code_section":"J",
            "libelle":"[61] Télécommunications",
            "children":[
               {
                  "code":"611",
                  "libelle":"[611] Télécommunications filaires",
                  "children":[
                     {
                        "code":"6110",
                        "libelle":"[6110] Télécommunications filaires",
                        "children":[
                           {
                              "code":"6110Z",
                              "libelle":"[6110Z] Télécommunications filaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"612",
                  "libelle":"[612] Télécommunications sans fil",
                  "children":[
                     {
                        "code":"6120",
                        "libelle":"[6120] Télécommunications sans fil",
                        "children":[
                           {
                              "code":"6120Z",
                              "libelle":"[6120Z] Télécommunications sans fil"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"613",
                  "libelle":"[613] Télécommunications par satellite",
                  "children":[
                     {
                        "code":"6130",
                        "libelle":"[6130] Télécommunications par satellite",
                        "children":[
                           {
                              "code":"6130Z",
                              "libelle":"[6130Z] Télécommunications par satellite"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"619",
                  "libelle":"[619] Autres activités de télécommunication",
                  "children":[
                     {
                        "code":"6190",
                        "libelle":"[6190] Autres activités de télécommunication",
                        "children":[
                           {
                              "code":"6190Z",
                              "libelle":"[6190Z] Autres activités de télécommunication"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"62",
            "code_section":"J",
            "libelle":"[62] Programmation, conseil et autres activités informatiques",
            "children":[
               {
                  "code":"620",
                  "libelle":"[620] Programmation, conseil et autres activités informatiques",
                  "children":[
                     {
                        "code":"6201",
                        "libelle":"[6201] Programmation informatique",
                        "children":[
                           {
                              "code":"6201Z",
                              "libelle":"[6201Z] Programmation informatique"
                           }
                        ]
                     },
                     {
                        "code":"6202",
                        "libelle":"[6202] Conseil informatique",
                        "children":[
                           {
                              "code":"6202A",
                              "libelle":"[6202A] Conseil en systèmes et logiciels informatiques"
                           },
                           {
                              "code":"6202B",
                              "libelle":"[6202B] Tierce maintenance de systèmes et d'applications informatiques"
                           }
                        ]
                     },
                     {
                        "code":"6203",
                        "libelle":"[6203] Gestion d'installations informatiques",
                        "children":[
                           {
                              "code":"6203Z",
                              "libelle":"[6203Z] Gestion d'installations informatiques"
                           }
                        ]
                     },
                     {
                        "code":"6209",
                        "libelle":"[6209] Autres activités informatiques",
                        "children":[
                           {
                              "code":"6209Z",
                              "libelle":"[6209Z] Autres activités informatiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"63",
            "code_section":"J",
            "libelle":"[63] Services d'information",
            "children":[
               {
                  "code":"631",
                  "libelle":"[631] Traitement de données, hébergement et activités connexes ; portails Internet",
                  "children":[
                     {
                        "code":"6311",
                        "libelle":"[6311] Traitement de données, hébergement et activités connexes",
                        "children":[
                           {
                              "code":"6311Z",
                              "libelle":"[6311Z] Traitement de données, hébergement et activités connexes"
                           }
                        ]
                     },
                     {
                        "code":"6312",
                        "libelle":"[6312] Portails Internet",
                        "children":[
                           {
                              "code":"6312Z",
                              "libelle":"[6312Z] Portails Internet"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"639",
                  "libelle":"[639] Autres services d'information",
                  "children":[
                     {
                        "code":"6391",
                        "libelle":"[6391] Activités des agences de presse",
                        "children":[
                           {
                              "code":"6391Z",
                              "libelle":"[6391Z] Activités des agences de presse"
                           }
                        ]
                     },
                     {
                        "code":"6399",
                        "libelle":"[6399] Autres services d'information n.c.a.",
                        "children":[
                           {
                              "code":"6399Z",
                              "libelle":"[6399Z] Autres services d'information n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"K",
      "libelle":"[K] Activités financières et d'assurance",
      "children":[
         {
            "code":"64",
            "code_section":"K",
            "libelle":"[64] Activités des services financiers, hors assurance et caisses de retraite",
            "children":[
               {
                  "code":"641",
                  "libelle":"[641] Intermédiation monétaire",
                  "children":[
                     {
                        "code":"6411",
                        "libelle":"[6411] Activités de banque centrale",
                        "children":[
                           {
                              "code":"6411Z",
                              "libelle":"[6411Z] Activités de banque centrale"
                           }
                        ]
                     },
                     {
                        "code":"6419",
                        "libelle":"[6419] Autres intermédiations monétaires",
                        "children":[
                           {
                              "code":"6419Z",
                              "libelle":"[6419Z] Autres intermédiations monétaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"642",
                  "libelle":"[642] Activités des sociétés holding",
                  "children":[
                     {
                        "code":"6420",
                        "libelle":"[6420] Activités des sociétés holding",
                        "children":[
                           {
                              "code":"6420Z",
                              "libelle":"[6420Z] Activités des sociétés holding"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"643",
                  "libelle":"[643] Fonds de placement et entités financières similaires",
                  "children":[
                     {
                        "code":"6430",
                        "libelle":"[6430] Fonds de placement et entités financières similaires",
                        "children":[
                           {
                              "code":"6430Z",
                              "libelle":"[6430Z] Fonds de placement et entités financières similaires"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"649",
                  "libelle":"[649] Autres activités des services financiers, hors assurance et caisses de retraite",
                  "children":[
                     {
                        "code":"6491",
                        "libelle":"[6491] Crédit-bail",
                        "children":[
                           {
                              "code":"6491Z",
                              "libelle":"[6491Z] Crédit-bail"
                           }
                        ]
                     },
                     {
                        "code":"6492",
                        "libelle":"[6492] Autre distribution de crédit",
                        "children":[
                           {
                              "code":"6492Z",
                              "libelle":"[6492Z] Autre distribution de crédit"
                           }
                        ]
                     },
                     {
                        "code":"6499",
                        "libelle":"[6499] Autres activités des services financiers, hors assurance et caisses de retraite, n.c.a.",
                        "children":[
                           {
                              "code":"6499Z",
                              "libelle":"[6499Z] Autres activités des services financiers, hors assurance et caisses de retraite, n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"65",
            "code_section":"K",
            "libelle":"[65] Assurance",
            "children":[
               {
                  "code":"651",
                  "libelle":"[651] Assurance",
                  "children":[
                     {
                        "code":"6511",
                        "libelle":"[6511] Assurance vie",
                        "children":[
                           {
                              "code":"6511Z",
                              "libelle":"[6511Z] Assurance vie"
                           }
                        ]
                     },
                     {
                        "code":"6512",
                        "libelle":"[6512] Autres assurances",
                        "children":[
                           {
                              "code":"6512Z",
                              "libelle":"[6512Z] Autres assurances"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"652",
                  "libelle":"[652] Réassurance",
                  "children":[
                     {
                        "code":"6520",
                        "libelle":"[6520] Réassurance",
                        "children":[
                           {
                              "code":"6520Z",
                              "libelle":"[6520Z] Réassurance"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"653",
                  "libelle":"[653] Caisses de retraite",
                  "children":[
                     {
                        "code":"6530",
                        "libelle":"[6530] Caisses de retraite",
                        "children":[
                           {
                              "code":"6530Z",
                              "libelle":"[6530Z] Caisses de retraite"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"66",
            "code_section":"K",
            "libelle":"[66] Activités auxiliaires de services financiers et d'assurance",
            "children":[
               {
                  "code":"661",
                  "libelle":"[661] Activités auxiliaires de services financiers, hors assurance et caisses de retraite",
                  "children":[
                     {
                        "code":"6611",
                        "libelle":"[6611] Administration de marchés financiers",
                        "children":[
                           {
                              "code":"6611Z",
                              "libelle":"[6611Z] Administration de marchés financiers"
                           }
                        ]
                     },
                     {
                        "code":"6612",
                        "libelle":"[6612] Courtage de valeurs mobilières et de marchandises",
                        "children":[
                           {
                              "code":"6612Z",
                              "libelle":"[6612Z] Courtage de valeurs mobilières et de marchandises"
                           }
                        ]
                     },
                     {
                        "code":"6619",
                        "libelle":"[6619] Autres activités auxiliaires de services financiers, hors assurance et caisses de retraite",
                        "children":[
                           {
                              "code":"6619A",
                              "libelle":"[6619A] Supports juridiques de gestion de patrimoine mobilier"
                           },
                           {
                              "code":"6619B",
                              "libelle":"[6619B] Autres activités auxiliaires de services financiers, hors assurance et caisses de retraite, n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"662",
                  "libelle":"[662] Activités auxiliaires d'assurance et de caisses de retraite",
                  "children":[
                     {
                        "code":"6621",
                        "libelle":"[6621] Évaluation des risques et dommages",
                        "children":[
                           {
                              "code":"6621Z",
                              "libelle":"[6621Z] Évaluation des risques et dommages"
                           }
                        ]
                     },
                     {
                        "code":"6622",
                        "libelle":"[6622] Activités des agents et courtiers d'assurances",
                        "children":[
                           {
                              "code":"6622Z",
                              "libelle":"[6622Z] Activités des agents et courtiers d'assurances"
                           }
                        ]
                     },
                     {
                        "code":"6629",
                        "libelle":"[6629] Autres activités auxiliaires d'assurance et de caisses de retraite",
                        "children":[
                           {
                              "code":"6629Z",
                              "libelle":"[6629Z] Autres activités auxiliaires d'assurance et de caisses de retraite"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"663",
                  "libelle":"[663] Gestion de fonds",
                  "children":[
                     {
                        "code":"6630",
                        "libelle":"[6630] Gestion de fonds",
                        "children":[
                           {
                              "code":"6630Z",
                              "libelle":"[6630Z] Gestion de fonds"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"L",
      "libelle":"[L] Activités immobilières",
      "children":[
         {
            "code":"68",
            "code_section":"L",
            "libelle":"[68] Activités immobilières",
            "children":[
               {
                  "code":"681",
                  "libelle":"[681] Activités des marchands de biens immobiliers",
                  "children":[
                     {
                        "code":"6810",
                        "libelle":"[6810] Activités des marchands de biens immobiliers",
                        "children":[
                           {
                              "code":"6810Z",
                              "libelle":"[6810Z] Activités des marchands de biens immobiliers"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"682",
                  "libelle":"[682] Location et exploitation de biens immobiliers propres ou loués",
                  "children":[
                     {
                        "code":"6820",
                        "libelle":"[6820] Location et exploitation de biens immobiliers propres ou loués",
                        "children":[
                           {
                              "code":"6820A",
                              "libelle":"[6820A] Location de logements"
                           },
                           {
                              "code":"6820B",
                              "libelle":"[6820B] Location de terrains et d'autres biens immobiliers"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"683",
                  "libelle":"[683] Activités immobilières pour compte de tiers",
                  "children":[
                     {
                        "code":"6831",
                        "libelle":"[6831] Agences immobilières",
                        "children":[
                           {
                              "code":"6831Z",
                              "libelle":"[6831Z] Agences immobilières"
                           }
                        ]
                     },
                     {
                        "code":"6832",
                        "libelle":"[6832] Administration de biens immobiliers",
                        "children":[
                           {
                              "code":"6832A",
                              "libelle":"[6832A] Administration d'immeubles et autres biens immobiliers"
                           },
                           {
                              "code":"6832B",
                              "libelle":"[6832B] Supports juridiques de gestion de patrimoine immobilier"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"M",
      "libelle":"[M] Activités spécialisées, scientifiques et techniques",
      "children":[
         {
            "code":"69",
            "code_section":"M",
            "libelle":"[69] Activités juridiques et comptables",
            "children":[
               {
                  "code":"691",
                  "libelle":"[691] Activités juridiques",
                  "children":[
                     {
                        "code":"6910",
                        "libelle":"[6910] Activités juridiques",
                        "children":[
                           {
                              "code":"6910Z",
                              "libelle":"[6910Z] Activités juridiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"692",
                  "libelle":"[692] Activités comptables",
                  "children":[
                     {
                        "code":"6920",
                        "libelle":"[6920] Activités comptables",
                        "children":[
                           {
                              "code":"6920Z",
                              "libelle":"[6920Z] Activités comptables"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"70",
            "code_section":"M",
            "libelle":"[70] Activités des sièges sociaux ; conseil de gestion",
            "children":[
               {
                  "code":"701",
                  "libelle":"[701] Activités des sièges sociaux",
                  "children":[
                     {
                        "code":"7010",
                        "libelle":"[7010] Activités des sièges sociaux",
                        "children":[
                           {
                              "code":"7010Z",
                              "libelle":"[7010Z] Activités des sièges sociaux"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"702",
                  "libelle":"[702] Conseil de gestion",
                  "children":[
                     {
                        "code":"7021",
                        "libelle":"[7021] Conseil en relations publiques et communication",
                        "children":[
                           {
                              "code":"7021Z",
                              "libelle":"[7021Z] Conseil en relations publiques et communication"
                           }
                        ]
                     },
                     {
                        "code":"7022",
                        "libelle":"[7022] Conseil pour les affaires et autres conseils de gestion",
                        "children":[
                           {
                              "code":"7022Z",
                              "libelle":"[7022Z] Conseil pour les affaires et autres conseils de gestion"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"71",
            "code_section":"M",
            "libelle":"[71] Activités d'architecture et d'ingénierie ; activités de contrôle et analyses techniques",
            "children":[
               {
                  "code":"711",
                  "libelle":"[711] Activités d'architecture et d'ingénierie",
                  "children":[
                     {
                        "code":"7111",
                        "libelle":"[7111] Activités d'architecture",
                        "children":[
                           {
                              "code":"7111Z",
                              "libelle":"[7111Z] Activités d'architecture"
                           }
                        ]
                     },
                     {
                        "code":"7112",
                        "libelle":"[7112] Activités d'ingénierie",
                        "children":[
                           {
                              "code":"7112A",
                              "libelle":"[7112A] Activité des géomètres"
                           },
                           {
                              "code":"7112B",
                              "libelle":"[7112B] Ingénierie, études techniques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"712",
                  "libelle":"[712] Activités de contrôle et analyses techniques",
                  "children":[
                     {
                        "code":"7120",
                        "libelle":"[7120] Activités de contrôle et analyses techniques",
                        "children":[
                           {
                              "code":"7120A",
                              "libelle":"[7120A] Contrôle technique automobile"
                           },
                           {
                              "code":"7120B",
                              "libelle":"[7120B] Analyses, essais et inspections techniques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"72",
            "code_section":"M",
            "libelle":"[72] Recherche-développement scientifique",
            "children":[
               {
                  "code":"721",
                  "libelle":"[721] Recherche-développement en sciences physiques et naturelles",
                  "children":[
                     {
                        "code":"7211",
                        "libelle":"[7211] Recherche-développement en biotechnologie",
                        "children":[
                           {
                              "code":"7211Z",
                              "libelle":"[7211Z] Recherche-développement en biotechnologie"
                           }
                        ]
                     },
                     {
                        "code":"7219",
                        "libelle":"[7219] Recherche-développement en autres sciences physiques et naturelles",
                        "children":[
                           {
                              "code":"7219Z",
                              "libelle":"[7219Z] Recherche-développement en autres sciences physiques et naturelles"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"722",
                  "libelle":"[722] Recherche-développement en sciences humaines et sociales",
                  "children":[
                     {
                        "code":"7220",
                        "libelle":"[7220] Recherche-développement en sciences humaines et sociales",
                        "children":[
                           {
                              "code":"7220Z",
                              "libelle":"[7220Z] Recherche-développement en sciences humaines et sociales"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"73",
            "code_section":"M",
            "libelle":"[73] Publicité et études de marché",
            "children":[
               {
                  "code":"731",
                  "libelle":"[731] Publicité",
                  "children":[
                     {
                        "code":"7311",
                        "libelle":"[7311] Activités des agences de publicité",
                        "children":[
                           {
                              "code":"7311Z",
                              "libelle":"[7311Z] Activités des agences de publicité"
                           }
                        ]
                     },
                     {
                        "code":"7312",
                        "libelle":"[7312] Régie publicitaire de médias",
                        "children":[
                           {
                              "code":"7312Z",
                              "libelle":"[7312Z] Régie publicitaire de médias"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"732",
                  "libelle":"[732] Études de marché et sondages",
                  "children":[
                     {
                        "code":"7320",
                        "libelle":"[7320] Études de marché et sondages",
                        "children":[
                           {
                              "code":"7320Z",
                              "libelle":"[7320Z] Études de marché et sondages"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"74",
            "code_section":"M",
            "libelle":"[74] Autres activités spécialisées, scientifiques et techniques",
            "children":[
               {
                  "code":"741",
                  "libelle":"[741] Activités spécialisées de design",
                  "children":[
                     {
                        "code":"7410",
                        "libelle":"[7410] Activités spécialisées de design",
                        "children":[
                           {
                              "code":"7410Z",
                              "libelle":"[7410Z] Activités spécialisées de design"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"742",
                  "libelle":"[742] Activités photographiques",
                  "children":[
                     {
                        "code":"7420",
                        "libelle":"[7420] Activités photographiques",
                        "children":[
                           {
                              "code":"7420Z",
                              "libelle":"[7420Z] Activités photographiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"743",
                  "libelle":"[743] Traduction et interprétation",
                  "children":[
                     {
                        "code":"7430",
                        "libelle":"[7430] Traduction et interprétation",
                        "children":[
                           {
                              "code":"7430Z",
                              "libelle":"[7430Z] Traduction et interprétation"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"749",
                  "libelle":"[749] Autres activités spécialisées, scientifiques et techniques n.c.a.",
                  "children":[
                     {
                        "code":"7490",
                        "libelle":"[7490] Autres activités spécialisées, scientifiques et techniques n.c.a.",
                        "children":[
                           {
                              "code":"7490A",
                              "libelle":"[7490A] Activité des économistes de la construction"
                           },
                           {
                              "code":"7490B",
                              "libelle":"[7490B] Activités spécialisées, scientifiques et techniques diverses"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"75",
            "code_section":"M",
            "libelle":"[75] Activités vétérinaires",
            "children":[
               {
                  "code":"750",
                  "libelle":"[750] Activités vétérinaires",
                  "children":[
                     {
                        "code":"7500",
                        "libelle":"[7500] Activités vétérinaires",
                        "children":[
                           {
                              "code":"7500Z",
                              "libelle":"[7500Z] Activités vétérinaires"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"N",
      "libelle":"[N] Activités de services administratifs et de soutien",
      "children":[
         {
            "code":"77",
            "code_section":"N",
            "libelle":"[77] Activités de location et location-bail",
            "children":[
               {
                  "code":"771",
                  "libelle":"[771] Location et location-bail de véhicules automobiles",
                  "children":[
                     {
                        "code":"7711",
                        "libelle":"[7711] Location et location-bail de voitures et de véhicules automobiles légers",
                        "children":[
                           {
                              "code":"7711A",
                              "libelle":"[7711A] Location de courte durée de voitures et de véhicules automobiles légers"
                           },
                           {
                              "code":"7711B",
                              "libelle":"[7711B] Location de longue durée de voitures et de véhicules automobiles légers"
                           }
                        ]
                     },
                     {
                        "code":"7712",
                        "libelle":"[7712] Location et location-bail de camions",
                        "children":[
                           {
                              "code":"7712Z",
                              "libelle":"[7712Z] Location et location-bail de camions"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"772",
                  "libelle":"[772] Location et location-bail de biens personnels et domestiques",
                  "children":[
                     {
                        "code":"7721",
                        "libelle":"[7721] Location et location-bail d'articles de loisirs et de sport",
                        "children":[
                           {
                              "code":"7721Z",
                              "libelle":"[7721Z] Location et location-bail d'articles de loisirs et de sport"
                           }
                        ]
                     },
                     {
                        "code":"7722",
                        "libelle":"[7722] Location de vidéocassettes et disques vidéo",
                        "children":[
                           {
                              "code":"7722Z",
                              "libelle":"[7722Z] Location de vidéocassettes et disques vidéo"
                           }
                        ]
                     },
                     {
                        "code":"7729",
                        "libelle":"[7729] Location et location-bail d'autres biens personnels et domestiques",
                        "children":[
                           {
                              "code":"7729Z",
                              "libelle":"[7729Z] Location et location-bail d'autres biens personnels et domestiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"773",
                  "libelle":"[773] Location et location-bail d'autres machines, équipements et biens",
                  "children":[
                     {
                        "code":"7731",
                        "libelle":"[7731] Location et location-bail de machines et équipements agricoles",
                        "children":[
                           {
                              "code":"7731Z",
                              "libelle":"[7731Z] Location et location-bail de machines et équipements agricoles"
                           }
                        ]
                     },
                     {
                        "code":"7732",
                        "libelle":"[7732] Location et location-bail de machines et équipements pour la construction",
                        "children":[
                           {
                              "code":"7732Z",
                              "libelle":"[7732Z] Location et location-bail de machines et équipements pour la construction"
                           }
                        ]
                     },
                     {
                        "code":"7733",
                        "libelle":"[7733] Location et location-bail de machines de bureau et de matériel informatique",
                        "children":[
                           {
                              "code":"7733Z",
                              "libelle":"[7733Z] Location et location-bail de machines de bureau et de matériel informatique"
                           }
                        ]
                     },
                     {
                        "code":"7734",
                        "libelle":"[7734] Location et location-bail de matériels de transport par eau",
                        "children":[
                           {
                              "code":"7734Z",
                              "libelle":"[7734Z] Location et location-bail de matériels de transport par eau"
                           }
                        ]
                     },
                     {
                        "code":"7735",
                        "libelle":"[7735] Location et location-bail de matériels de transport aérien",
                        "children":[
                           {
                              "code":"7735Z",
                              "libelle":"[7735Z] Location et location-bail de matériels de transport aérien"
                           }
                        ]
                     },
                     {
                        "code":"7739",
                        "libelle":"[7739] Location et location-bail d'autres machines, équipements et biens matériels n.c.a.",
                        "children":[
                           {
                              "code":"7739Z",
                              "libelle":"[7739Z] Location et location-bail d'autres machines, équipements et biens matériels n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"774",
                  "libelle":"[774] Location-bail de propriété intellectuelle et de produits similaires, à l'exception des œuvres soumises à copyright",
                  "children":[
                     {
                        "code":"7740",
                        "libelle":"[7740] Location-bail de propriété intellectuelle et de produits similaires, à l'exception des œuvres soumises à copyright",
                        "children":[
                           {
                              "code":"7740Z",
                              "libelle":"[7740Z] Location-bail de propriété intellectuelle et de produits similaires, à l'exception des œuvres soumises à copyright"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"78",
            "code_section":"N",
            "libelle":"[78] Activités liées à l'emploi",
            "children":[
               {
                  "code":"781",
                  "libelle":"[781] Activités des agences de placement de main-d'œuvre",
                  "children":[
                     {
                        "code":"7810",
                        "libelle":"[7810] Activités des agences de placement de main-d'œuvre",
                        "children":[
                           {
                              "code":"7810Z",
                              "libelle":"[7810Z] Activités des agences de placement de main-d'œuvre"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"782",
                  "libelle":"[782] Activités des agences de travail temporaire",
                  "children":[
                     {
                        "code":"7820",
                        "libelle":"[7820] Activités des agences de travail temporaire",
                        "children":[
                           {
                              "code":"7820Z",
                              "libelle":"[7820Z] Activités des agences de travail temporaire"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"783",
                  "libelle":"[783] Autre mise à disposition de ressources humaines",
                  "children":[
                     {
                        "code":"7830",
                        "libelle":"[7830] Autre mise à disposition de ressources humaines",
                        "children":[
                           {
                              "code":"7830Z",
                              "libelle":"[7830Z] Autre mise à disposition de ressources humaines"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"79",
            "code_section":"N",
            "libelle":"[79] Activités des agences de voyage, voyagistes, services de réservation et activités connexes",
            "children":[
               {
                  "code":"791",
                  "libelle":"[791] Activités des agences de voyage et voyagistes",
                  "children":[
                     {
                        "code":"7911",
                        "libelle":"[7911] Activités des agences de voyage",
                        "children":[
                           {
                              "code":"7911Z",
                              "libelle":"[7911Z] Activités des agences de voyage"
                           }
                        ]
                     },
                     {
                        "code":"7912",
                        "libelle":"[7912] Activités des voyagistes",
                        "children":[
                           {
                              "code":"7912Z",
                              "libelle":"[7912Z] Activités des voyagistes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"799",
                  "libelle":"[799] Autres services de réservation et activités connexes",
                  "children":[
                     {
                        "code":"7990",
                        "libelle":"[7990] Autres services de réservation et activités connexes",
                        "children":[
                           {
                              "code":"7990Z",
                              "libelle":"[7990Z] Autres services de réservation et activités connexes"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"80",
            "code_section":"N",
            "libelle":"[80] Enquêtes et sécurité",
            "children":[
               {
                  "code":"801",
                  "libelle":"[801] Activités de sécurité privée",
                  "children":[
                     {
                        "code":"8010",
                        "libelle":"[8010] Activités de sécurité privée",
                        "children":[
                           {
                              "code":"8010Z",
                              "libelle":"[8010Z] Activités de sécurité privée"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"802",
                  "libelle":"[802] Activités liées aux systèmes de sécurité",
                  "children":[
                     {
                        "code":"8020",
                        "libelle":"[8020] Activités liées aux systèmes de sécurité",
                        "children":[
                           {
                              "code":"8020Z",
                              "libelle":"[8020Z] Activités liées aux systèmes de sécurité"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"803",
                  "libelle":"[803] Activités d'enquête",
                  "children":[
                     {
                        "code":"8030",
                        "libelle":"[8030] Activités d'enquête",
                        "children":[
                           {
                              "code":"8030Z",
                              "libelle":"[8030Z] Activités d'enquête"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"81",
            "code_section":"N",
            "libelle":"[81] Services relatifs aux bâtiments et aménagement paysager",
            "children":[
               {
                  "code":"811",
                  "libelle":"[811] Activités combinées de soutien lié aux bâtiments",
                  "children":[
                     {
                        "code":"8110",
                        "libelle":"[8110] Activités combinées de soutien lié aux bâtiments",
                        "children":[
                           {
                              "code":"8110Z",
                              "libelle":"[8110Z] Activités combinées de soutien lié aux bâtiments"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"812",
                  "libelle":"[812] Activités de nettoyage",
                  "children":[
                     {
                        "code":"8121",
                        "libelle":"[8121] Nettoyage courant des bâtiments",
                        "children":[
                           {
                              "code":"8121Z",
                              "libelle":"[8121Z] Nettoyage courant des bâtiments"
                           }
                        ]
                     },
                     {
                        "code":"8122",
                        "libelle":"[8122] Autres activités de nettoyage des bâtiments et nettoyage industriel",
                        "children":[
                           {
                              "code":"8122Z",
                              "libelle":"[8122Z] Autres activités de nettoyage des bâtiments et nettoyage industriel"
                           }
                        ]
                     },
                     {
                        "code":"8129",
                        "libelle":"[8129] Autres activités de nettoyage",
                        "children":[
                           {
                              "code":"8129A",
                              "libelle":"[8129A] Désinfection, désinsectisation, dératisation"
                           },
                           {
                              "code":"8129B",
                              "libelle":"[8129B] Autres activités de nettoyage n.c.a."
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"813",
                  "libelle":"[813] Services d'aménagement paysager",
                  "children":[
                     {
                        "code":"8130",
                        "libelle":"[8130] Services d'aménagement paysager",
                        "children":[
                           {
                              "code":"8130Z",
                              "libelle":"[8130Z] Services d'aménagement paysager"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"82",
            "code_section":"N",
            "libelle":"[82] Activités administratives et autres activités de soutien aux entreprises",
            "children":[
               {
                  "code":"821",
                  "libelle":"[821] Activités administratives",
                  "children":[
                     {
                        "code":"8211",
                        "libelle":"[8211] Services administratifs combinés de bureau",
                        "children":[
                           {
                              "code":"8211Z",
                              "libelle":"[8211Z] Services administratifs combinés de bureau"
                           }
                        ]
                     },
                     {
                        "code":"8219",
                        "libelle":"[8219] Photocopie, préparation de documents et autres activités spécialisées de soutien de bureau",
                        "children":[
                           {
                              "code":"8219Z",
                              "libelle":"[8219Z] Photocopie, préparation de documents et autres activités spécialisées de soutien de bureau"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"822",
                  "libelle":"[822] Activités de centres d'appels",
                  "children":[
                     {
                        "code":"8220",
                        "libelle":"[8220] Activités de centres d'appels",
                        "children":[
                           {
                              "code":"8220Z",
                              "libelle":"[8220Z] Activités de centres d'appels"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"823",
                  "libelle":"[823] Organisation de salons professionnels et congrès",
                  "children":[
                     {
                        "code":"8230",
                        "libelle":"[8230] Organisation de salons professionnels et congrès",
                        "children":[
                           {
                              "code":"8230Z",
                              "libelle":"[8230Z] Organisation de foires, salons professionnels et congrès"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"829",
                  "libelle":"[829] Activités de soutien aux entreprises n.c.a.",
                  "children":[
                     {
                        "code":"8291",
                        "libelle":"[8291] Activités des agences de recouvrement de factures et des sociétés d'information financière sur la clientèle",
                        "children":[
                           {
                              "code":"8291Z",
                              "libelle":"[8291Z] Activités des agences de recouvrement de factures et des sociétés d'information financière sur la clientèle"
                           }
                        ]
                     },
                     {
                        "code":"8292",
                        "libelle":"[8292] Activités de conditionnement",
                        "children":[
                           {
                              "code":"8292Z",
                              "libelle":"[8292Z] Activités de conditionnement"
                           }
                        ]
                     },
                     {
                        "code":"8299",
                        "libelle":"[8299] Autres activités de soutien aux entreprises n.c.a.",
                        "children":[
                           {
                              "code":"8299Z",
                              "libelle":"[8299Z] Autres activités de soutien aux entreprises n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"O",
      "libelle":"[O] Administration publique",
      "children":[
         {
            "code":"84",
            "code_section":"O",
            "libelle":"[84] Administration publique et défense ; sécurité sociale obligatoire",
            "children":[
               {
                  "code":"841",
                  "libelle":"[841] Administration générale, économique et sociale",
                  "children":[
                     {
                        "code":"8411",
                        "libelle":"[8411] Administration publique générale",
                        "children":[
                           {
                              "code":"8411Z",
                              "libelle":"[8411Z] Administration publique générale"
                           }
                        ]
                     },
                     {
                        "code":"8412",
                        "libelle":"[8412] Administration publique (tutelle) de la santé, de la formation, de la culture et des services sociaux, autre que sécurité sociale",
                        "children":[
                           {
                              "code":"8412Z",
                              "libelle":"[8412Z] Administration publique (tutelle) de la santé, de la formation, de la culture et des services sociaux, autre que sécurité sociale"
                           }
                        ]
                     },
                     {
                        "code":"8413",
                        "libelle":"[8413] Administration publique (tutelle) des activités économiques",
                        "children":[
                           {
                              "code":"8413Z",
                              "libelle":"[8413Z] Administration publique (tutelle) des activités économiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"842",
                  "libelle":"[842] Services de prérogative publique",
                  "children":[
                     {
                        "code":"8421",
                        "libelle":"[8421] Affaires étrangères",
                        "children":[
                           {
                              "code":"8421Z",
                              "libelle":"[8421Z] Affaires étrangères"
                           }
                        ]
                     },
                     {
                        "code":"8422",
                        "libelle":"[8422] Défense",
                        "children":[
                           {
                              "code":"8422Z",
                              "libelle":"[8422Z] Défense"
                           }
                        ]
                     },
                     {
                        "code":"8423",
                        "libelle":"[8423] Justice",
                        "children":[
                           {
                              "code":"8423Z",
                              "libelle":"[8423Z] Justice"
                           }
                        ]
                     },
                     {
                        "code":"8424",
                        "libelle":"[8424] Activités d'ordre public et de sécurité",
                        "children":[
                           {
                              "code":"8424Z",
                              "libelle":"[8424Z] Activités d'ordre public et de sécurité"
                           }
                        ]
                     },
                     {
                        "code":"8425",
                        "libelle":"[8425] Services du feu et de secours",
                        "children":[
                           {
                              "code":"8425Z",
                              "libelle":"[8425Z] Services du feu et de secours"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"843",
                  "libelle":"[843] Sécurité sociale obligatoire",
                  "children":[
                     {
                        "code":"8430",
                        "libelle":"[8430] Sécurité sociale obligatoire",
                        "children":[
                           {
                              "code":"8430A",
                              "libelle":"[8430A] Activités générales de sécurité sociale"
                           },
                           {
                              "code":"8430B",
                              "libelle":"[8430B] Gestion des retraites complémentaires"
                           },
                           {
                              "code":"8430C",
                              "libelle":"[8430C] Distribution sociale de revenus"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"P",
      "libelle":"[P] Enseignement",
      "children":[
         {
            "code":"85",
            "code_section":"P",
            "libelle":"[85] Enseignement",
            "children":[
               {
                  "code":"851",
                  "libelle":"[851] Enseignement pré-primaire",
                  "children":[
                     {
                        "code":"8510",
                        "libelle":"[8510] Enseignement pré-primaire",
                        "children":[
                           {
                              "code":"8510Z",
                              "libelle":"[8510Z] Enseignement pré-primaire"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"852",
                  "libelle":"[852] Enseignement primaire",
                  "children":[
                     {
                        "code":"8520",
                        "libelle":"[8520] Enseignement primaire",
                        "children":[
                           {
                              "code":"8520Z",
                              "libelle":"[8520Z] Enseignement primaire"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"853",
                  "libelle":"[853] Enseignement secondaire",
                  "children":[
                     {
                        "code":"8531",
                        "libelle":"[8531] Enseignement secondaire général",
                        "children":[
                           {
                              "code":"8531Z",
                              "libelle":"[8531Z] Enseignement secondaire général"
                           }
                        ]
                     },
                     {
                        "code":"8532",
                        "libelle":"[8532] Enseignement secondaire technique ou professionnel",
                        "children":[
                           {
                              "code":"8532Z",
                              "libelle":"[8532Z] Enseignement secondaire technique ou professionnel"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"854",
                  "libelle":"[854] Enseignement supérieur et post-secondaire non supérieur",
                  "children":[
                     {
                        "code":"8541",
                        "libelle":"[8541] Enseignement post-secondaire non supérieur",
                        "children":[
                           {
                              "code":"8541Z",
                              "libelle":"[8541Z] Enseignement post-secondaire non supérieur"
                           }
                        ]
                     },
                     {
                        "code":"8542",
                        "libelle":"[8542] Enseignement supérieur",
                        "children":[
                           {
                              "code":"8542Z",
                              "libelle":"[8542Z] Enseignement supérieur"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"855",
                  "libelle":"[855] Autres activités d'enseignement",
                  "children":[
                     {
                        "code":"8551",
                        "libelle":"[8551] Enseignement de disciplines sportives et d'activités de loisirs",
                        "children":[
                           {
                              "code":"8551Z",
                              "libelle":"[8551Z] Enseignement de disciplines sportives et d'activités de loisirs"
                           }
                        ]
                     },
                     {
                        "code":"8552",
                        "libelle":"[8552] Enseignement culturel",
                        "children":[
                           {
                              "code":"8552Z",
                              "libelle":"[8552Z] Enseignement culturel"
                           }
                        ]
                     },
                     {
                        "code":"8553",
                        "libelle":"[8553] Enseignement de la conduite",
                        "children":[
                           {
                              "code":"8553Z",
                              "libelle":"[8553Z] Enseignement de la conduite"
                           }
                        ]
                     },
                     {
                        "code":"8559",
                        "libelle":"[8559] Enseignements divers",
                        "children":[
                           {
                              "code":"8559A",
                              "libelle":"[8559A] Formation continue d'adultes"
                           },
                           {
                              "code":"8559B",
                              "libelle":"[8559B] Autres enseignements"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"856",
                  "libelle":"[856] Activités de soutien à l'enseignement",
                  "children":[
                     {
                        "code":"8560",
                        "libelle":"[8560] Activités de soutien à l'enseignement",
                        "children":[
                           {
                              "code":"8560Z",
                              "libelle":"[8560Z] Activités de soutien à l'enseignement"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"Q",
      "libelle":"[Q] Santé humaine et action sociale",
      "children":[
         {
            "code":"86",
            "code_section":"Q",
            "libelle":"[86] Activités pour la santé humaine",
            "children":[
               {
                  "code":"861",
                  "libelle":"[861] Activités hospitalières",
                  "children":[
                     {
                        "code":"8610",
                        "libelle":"[8610] Activités hospitalières",
                        "children":[
                           {
                              "code":"8610Z",
                              "libelle":"[8610Z] Activités hospitalières"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"862",
                  "libelle":"[862] Activité des médecins et des dentistes",
                  "children":[
                     {
                        "code":"8621",
                        "libelle":"[8621] Activité des médecins généralistes",
                        "children":[
                           {
                              "code":"8621Z",
                              "libelle":"[8621Z] Activité des médecins généralistes"
                           }
                        ]
                     },
                     {
                        "code":"8622",
                        "libelle":"[8622] Activité des médecins spécialistes",
                        "children":[
                           {
                              "code":"8622A",
                              "libelle":"[8622A] Activités de radiodiagnostic et de radiothérapie"
                           },
                           {
                              "code":"8622B",
                              "libelle":"[8622B] Activités chirurgicales"
                           },
                           {
                              "code":"8622C",
                              "libelle":"[8622C] Autres activités des médecins spécialistes"
                           }
                        ]
                     },
                     {
                        "code":"8623",
                        "libelle":"[8623] Pratique dentaire",
                        "children":[
                           {
                              "code":"8623Z",
                              "libelle":"[8623Z] Pratique dentaire"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"869",
                  "libelle":"[869] Autres activités pour la santé humaine",
                  "children":[
                     {
                        "code":"8690",
                        "libelle":"[8690] Autres activités pour la santé humaine",
                        "children":[
                           {
                              "code":"8690A",
                              "libelle":"[8690A] Ambulances"
                           },
                           {
                              "code":"8690B",
                              "libelle":"[8690B] Laboratoires d'analyses médicales"
                           },
                           {
                              "code":"8690C",
                              "libelle":"[8690C] Centres de collecte et banques d'organes"
                           },
                           {
                              "code":"8690D",
                              "libelle":"[8690D] Activités des infirmiers et des sages-femmes"
                           },
                           {
                              "code":"8690E",
                              "libelle":"[8690E] Activités des professionnels de la rééducation, de l'appareillage et des pédicures-podologues"
                           },
                           {
                              "code":"8690F",
                              "libelle":"[8690F] Activités de santé humaine non classées ailleurs"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"87",
            "code_section":"Q",
            "libelle":"[87] Hébergement médico-social et social",
            "children":[
               {
                  "code":"871",
                  "libelle":"[871] Hébergement médicalisé",
                  "children":[
                     {
                        "code":"8710",
                        "libelle":"[8710] Hébergement médicalisé",
                        "children":[
                           {
                              "code":"8710A",
                              "libelle":"[8710A] Hébergement médicalisé pour personnes âgées"
                           },
                           {
                              "code":"8710B",
                              "libelle":"[8710B] Hébergement médicalisé pour enfants handicapés"
                           },
                           {
                              "code":"8710C",
                              "libelle":"[8710C] Hébergement médicalisé pour adultes handicapés et autre hébergement médicalisé"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"872",
                  "libelle":"[872] Hébergement social pour personnes handicapées mentales, malades mentales et toxicomanes",
                  "children":[
                     {
                        "code":"8720",
                        "libelle":"[8720] Hébergement social pour personnes handicapées mentales, malades mentales et toxicomanes",
                        "children":[
                           {
                              "code":"8720A",
                              "libelle":"[8720A] Hébergement social pour handicapés mentaux et malades mentaux"
                           },
                           {
                              "code":"8720B",
                              "libelle":"[8720B] Hébergement social pour toxicomanes"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"873",
                  "libelle":"[873] Hébergement social pour personnes âgées ou handicapées physiques",
                  "children":[
                     {
                        "code":"8730",
                        "libelle":"[8730] Hébergement social pour personnes âgées ou handicapées physiques",
                        "children":[
                           {
                              "code":"8730A",
                              "libelle":"[8730A] Hébergement social pour personnes âgées"
                           },
                           {
                              "code":"8730B",
                              "libelle":"[8730B] Hébergement social pour handicapés physiques"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"879",
                  "libelle":"[879] Autres activités d'hébergement social",
                  "children":[
                     {
                        "code":"8790",
                        "libelle":"[8790] Autres activités d'hébergement social",
                        "children":[
                           {
                              "code":"8790A",
                              "libelle":"[8790A] Hébergement social pour enfants en difficultés"
                           },
                           {
                              "code":"8790B",
                              "libelle":"[8790B] Hébergement social pour adultes et familles en difficultés et autre hébergement social"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"88",
            "code_section":"Q",
            "libelle":"[88] Action sociale sans hébergement",
            "children":[
               {
                  "code":"881",
                  "libelle":"[881] Action sociale sans hébergement pour personnes âgées et pour personnes handicapées",
                  "children":[
                     {
                        "code":"8810",
                        "libelle":"[8810] Action sociale sans hébergement pour personnes âgées et pour personnes handicapées",
                        "children":[
                           {
                              "code":"8810A",
                              "libelle":"[8810A] Aide à domicile"
                           },
                           {
                              "code":"8810B",
                              "libelle":"[8810B] Accueil ou accompagnement sans hébergement d'adultes handicapés ou de personnes âgées"
                           },
                           {
                              "code":"8810C",
                              "libelle":"[8810C] Aide par le travail"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"889",
                  "libelle":"[889] Autre action sociale sans hébergement",
                  "children":[
                     {
                        "code":"8891",
                        "libelle":"[8891] Action sociale sans hébergement pour jeunes enfants",
                        "children":[
                           {
                              "code":"8891A",
                              "libelle":"[8891A] Accueil de jeunes enfants"
                           },
                           {
                              "code":"8891B",
                              "libelle":"[8891B] Accueil ou accompagnement sans hébergement d'enfants handicapés"
                           }
                        ]
                     },
                     {
                        "code":"8899",
                        "libelle":"[8899] Autre action sociale sans hébergement n.c.a.",
                        "children":[
                           {
                              "code":"8899A",
                              "libelle":"[8899A] Autre accueil ou accompagnement sans hébergement d'enfants et d'adolescents"
                           },
                           {
                              "code":"8899B",
                              "libelle":"[8899B] Action sociale sans hébergement n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"R",
      "libelle":"[R] Arts, spectacles et activités récréatives",
      "children":[
         {
            "code":"90",
            "code_section":"R",
            "libelle":"[90] Activités créatives, artistiques et de spectacle",
            "children":[
               {
                  "code":"900",
                  "libelle":"[900] Activités créatives, artistiques et de spectacle",
                  "children":[
                     {
                        "code":"9001",
                        "libelle":"[9001] Arts du spectacle vivant",
                        "children":[
                           {
                              "code":"9001Z",
                              "libelle":"[9001Z] Arts du spectacle vivant"
                           }
                        ]
                     },
                     {
                        "code":"9002",
                        "libelle":"[9002] Activités de soutien au spectacle vivant",
                        "children":[
                           {
                              "code":"9002Z",
                              "libelle":"[9002Z] Activités de soutien au spectacle vivant"
                           }
                        ]
                     },
                     {
                        "code":"9003",
                        "libelle":"[9003] Création artistique",
                        "children":[
                           {
                              "code":"9003A",
                              "libelle":"[9003A] Création artistique relevant des arts plastiques"
                           },
                           {
                              "code":"9003B",
                              "libelle":"[9003B] Autre création artistique"
                           }
                        ]
                     },
                     {
                        "code":"9004",
                        "libelle":"[9004] Gestion de salles de spectacles",
                        "children":[
                           {
                              "code":"9004Z",
                              "libelle":"[9004Z] Gestion de salles de spectacles"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"91",
            "code_section":"R",
            "libelle":"[91] Bibliothèques, archives, musées et autres activités culturelles",
            "children":[
               {
                  "code":"910",
                  "libelle":"[910] Bibliothèques, archives, musées et autres activités culturelles",
                  "children":[
                     {
                        "code":"9101",
                        "libelle":"[9101] Gestion des bibliothèques et des archives",
                        "children":[
                           {
                              "code":"9101Z",
                              "libelle":"[9101Z] Gestion des bibliothèques et des archives"
                           }
                        ]
                     },
                     {
                        "code":"9102",
                        "libelle":"[9102] Gestion des musées",
                        "children":[
                           {
                              "code":"9102Z",
                              "libelle":"[9102Z] Gestion des musées"
                           }
                        ]
                     },
                     {
                        "code":"9103",
                        "libelle":"[9103] Gestion des sites et monuments historiques et des attractions touristiques similaires",
                        "children":[
                           {
                              "code":"9103Z",
                              "libelle":"[9103Z] Gestion des sites et monuments historiques et des attractions touristiques similaires"
                           }
                        ]
                     },
                     {
                        "code":"9104",
                        "libelle":"[9104] Gestion des jardins botaniques et zoologiques et des réserves naturelles",
                        "children":[
                           {
                              "code":"9104Z",
                              "libelle":"[9104Z] Gestion des jardins botaniques et zoologiques et des réserves naturelles"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"92",
            "code_section":"R",
            "libelle":"[92] Organisation de jeux de hasard et d'argent",
            "children":[
               {
                  "code":"920",
                  "libelle":"[920] Organisation de jeux de hasard et d'argent",
                  "children":[
                     {
                        "code":"9200",
                        "libelle":"[9200] Organisation de jeux de hasard et d'argent",
                        "children":[
                           {
                              "code":"9200Z",
                              "libelle":"[9200Z] Organisation de jeux de hasard et d'argent"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"93",
            "code_section":"R",
            "libelle":"[93] Activités sportives, récréatives et de loisirs",
            "children":[
               {
                  "code":"931",
                  "libelle":"[931] Activités liées au sport",
                  "children":[
                     {
                        "code":"9311",
                        "libelle":"[9311] Gestion d'installations sportives",
                        "children":[
                           {
                              "code":"9311Z",
                              "libelle":"[9311Z] Gestion d'installations sportives"
                           }
                        ]
                     },
                     {
                        "code":"9312",
                        "libelle":"[9312] Activités de clubs de sports",
                        "children":[
                           {
                              "code":"9312Z",
                              "libelle":"[9312Z] Activités de clubs de sports"
                           }
                        ]
                     },
                     {
                        "code":"9313",
                        "libelle":"[9313] Activités des centres de culture physique",
                        "children":[
                           {
                              "code":"9313Z",
                              "libelle":"[9313Z] Activités des centres de culture physique"
                           }
                        ]
                     },
                     {
                        "code":"9319",
                        "libelle":"[9319] Autres activités liées au sport",
                        "children":[
                           {
                              "code":"9319Z",
                              "libelle":"[9319Z] Autres activités liées au sport"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"932",
                  "libelle":"[932] Activités récréatives et de loisirs",
                  "children":[
                     {
                        "code":"9321",
                        "libelle":"[9321] Activités des parcs d'attractions et parcs à thèmes",
                        "children":[
                           {
                              "code":"9321Z",
                              "libelle":"[9321Z] Activités des parcs d'attractions et parcs à thèmes"
                           }
                        ]
                     },
                     {
                        "code":"9329",
                        "libelle":"[9329] Autres activités récréatives et de loisirs",
                        "children":[
                           {
                              "code":"9329Z",
                              "libelle":"[9329Z] Autres activités récréatives et de loisirs"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"S",
      "libelle":"[S] Autres activités de services",
      "children":[
         {
            "code":"94",
            "code_section":"S",
            "libelle":"[94] Activités des organisations associatives",
            "children":[
               {
                  "code":"941",
                  "libelle":"[941] Activités des organisations économiques, patronales et professionnelles",
                  "children":[
                     {
                        "code":"9411",
                        "libelle":"[9411] Activités des organisations patronales et consulaires",
                        "children":[
                           {
                              "code":"9411Z",
                              "libelle":"[9411Z] Activités des organisations patronales et consulaires"
                           }
                        ]
                     },
                     {
                        "code":"9412",
                        "libelle":"[9412] Activités des organisations professionnelles",
                        "children":[
                           {
                              "code":"9412Z",
                              "libelle":"[9412Z] Activités des organisations professionnelles"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"942",
                  "libelle":"[942] Activités des syndicats de salariés",
                  "children":[
                     {
                        "code":"9420",
                        "libelle":"[9420] Activités des syndicats de salariés",
                        "children":[
                           {
                              "code":"9420Z",
                              "libelle":"[9420Z] Activités des syndicats de salariés"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"949",
                  "libelle":"[949] Activités des autres organisations associatives",
                  "children":[
                     {
                        "code":"9491",
                        "libelle":"[9491] Activités des organisations religieuses",
                        "children":[
                           {
                              "code":"9491Z",
                              "libelle":"[9491Z] Activités des organisations religieuses"
                           }
                        ]
                     },
                     {
                        "code":"9492",
                        "libelle":"[9492] Activités des organisations politiques",
                        "children":[
                           {
                              "code":"9492Z",
                              "libelle":"[9492Z] Activités des organisations politiques"
                           }
                        ]
                     },
                     {
                        "code":"9499",
                        "libelle":"[9499] Activités des organisations associatives n.c.a.",
                        "children":[
                           {
                              "code":"9499Z",
                              "libelle":"[9499Z] Autres organisations fonctionnant par adhésion volontaire"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"95",
            "code_section":"S",
            "libelle":"[95] Réparation d'ordinateurs et de biens personnels et domestiques",
            "children":[
               {
                  "code":"951",
                  "libelle":"[951] Réparation d'ordinateurs et d'équipements de communication",
                  "children":[
                     {
                        "code":"9511",
                        "libelle":"[9511] Réparation d'ordinateurs et d'équipements périphériques",
                        "children":[
                           {
                              "code":"9511Z",
                              "libelle":"[9511Z] Réparation d'ordinateurs et d'équipements périphériques"
                           }
                        ]
                     },
                     {
                        "code":"9512",
                        "libelle":"[9512] Réparation d'équipements de communication",
                        "children":[
                           {
                              "code":"9512Z",
                              "libelle":"[9512Z] Réparation d'équipements de communication"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"952",
                  "libelle":"[952] Réparation de biens personnels et domestiques",
                  "children":[
                     {
                        "code":"9521",
                        "libelle":"[9521] Réparation de produits électroniques grand public",
                        "children":[
                           {
                              "code":"9521Z",
                              "libelle":"[9521Z] Réparation de produits électroniques grand public"
                           }
                        ]
                     },
                     {
                        "code":"9522",
                        "libelle":"[9522] Réparation d'appareils électroménagers et d'équipements pour la maison et le jardin",
                        "children":[
                           {
                              "code":"9522Z",
                              "libelle":"[9522Z] Réparation d'appareils électroménagers et d'équipements pour la maison et le jardin"
                           }
                        ]
                     },
                     {
                        "code":"9523",
                        "libelle":"[9523] Réparation de chaussures et d'articles en cuir",
                        "children":[
                           {
                              "code":"9523Z",
                              "libelle":"[9523Z] Réparation de chaussures et d'articles en cuir"
                           }
                        ]
                     },
                     {
                        "code":"9524",
                        "libelle":"[9524] Réparation de meubles et d'équipements du foyer",
                        "children":[
                           {
                              "code":"9524Z",
                              "libelle":"[9524Z] Réparation de meubles et d'équipements du foyer"
                           }
                        ]
                     },
                     {
                        "code":"9525",
                        "libelle":"[9525] Réparation d'articles d'horlogerie et de bijouterie",
                        "children":[
                           {
                              "code":"9525Z",
                              "libelle":"[9525Z] Réparation d'articles d'horlogerie et de bijouterie"
                           }
                        ]
                     },
                     {
                        "code":"9529",
                        "libelle":"[9529] Réparation d'autres biens personnels et domestiques",
                        "children":[
                           {
                              "code":"9529Z",
                              "libelle":"[9529Z] Réparation d'autres biens personnels et domestiques"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"96",
            "code_section":"S",
            "libelle":"[96] Autres services personnels",
            "children":[
               {
                  "code":"960",
                  "libelle":"[960] Autres services personnels",
                  "children":[
                     {
                        "code":"9601",
                        "libelle":"[9601] Blanchisserie-teinturerie",
                        "children":[
                           {
                              "code":"9601A",
                              "libelle":"[9601A] Blanchisserie-teinturerie de gros"
                           },
                           {
                              "code":"9601B",
                              "libelle":"[9601B] Blanchisserie-teinturerie de détail"
                           }
                        ]
                     },
                     {
                        "code":"9602",
                        "libelle":"[9602] Coiffure et soins de beauté",
                        "children":[
                           {
                              "code":"9602A",
                              "libelle":"[9602A] Coiffure"
                           },
                           {
                              "code":"9602B",
                              "libelle":"[9602B] Soins de beauté"
                           }
                        ]
                     },
                     {
                        "code":"9603",
                        "libelle":"[9603] Services funéraires",
                        "children":[
                           {
                              "code":"9603Z",
                              "libelle":"[9603Z] Services funéraires"
                           }
                        ]
                     },
                     {
                        "code":"9604",
                        "libelle":"[9604] Entretien corporel",
                        "children":[
                           {
                              "code":"9604Z",
                              "libelle":"[9604Z] Entretien corporel"
                           }
                        ]
                     },
                     {
                        "code":"9609",
                        "libelle":"[9609] Autres services personnels n.c.a.",
                        "children":[
                           {
                              "code":"9609Z",
                              "libelle":"[9609Z] Autres services personnels n.c.a."
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"T",
      "libelle":"[T] Activités des ménages en tant qu'employeurs ; activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre",
      "children":[
         {
            "code":"97",
            "code_section":"T",
            "libelle":"[97] Activités des ménages en tant qu'employeurs de personnel domestique",
            "children":[
               {
                  "code":"970",
                  "libelle":"[970] Activités des ménages en tant qu'employeurs de personnel domestique",
                  "children":[
                     {
                        "code":"9700",
                        "libelle":"[9700] Activités des ménages en tant qu'employeurs de personnel domestique",
                        "children":[
                           {
                              "code":"9700Z",
                              "libelle":"[9700Z] Activités des ménages en tant qu'employeurs de personnel domestique"
                           }
                        ]
                     }
                  ]
               }
            ]
         },
         {
            "code":"98",
            "code_section":"T",
            "libelle":"[98] Activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre",
            "children":[
               {
                  "code":"981",
                  "libelle":"[981] Activités indifférenciées des ménages en tant que producteurs de biens pour usage propre",
                  "children":[
                     {
                        "code":"9810",
                        "libelle":"[9810] Activités indifférenciées des ménages en tant que producteurs de biens pour usage propre",
                        "children":[
                           {
                              "code":"9810Z",
                              "libelle":"[9810Z] Activités indifférenciées des ménages en tant que producteurs de biens pour usage propre"
                           }
                        ]
                     }
                  ]
               },
               {
                  "code":"982",
                  "libelle":"[982] Activités indifférenciées des ménages en tant que producteurs de services pour usage propre",
                  "children":[
                     {
                        "code":"9820",
                        "libelle":"[9820] Activités indifférenciées des ménages en tant que producteurs de services pour usage propre",
                        "children":[
                           {
                              "code":"9820Z",
                              "libelle":"[9820Z] Activités indifférenciées des ménages en tant que producteurs de services pour usage propre"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   },
   {
      "code":"U",
      "libelle":"[U] Activités extra-territoriales",
      "children":[
         {
            "code":"99",
            "code_section":"U",
            "libelle":"[99] Activités des organisations et organismes extraterritoriaux",
            "children":[
               {
                  "code":"990",
                  "libelle":"[990] Activités des organisations et organismes extraterritoriaux",
                  "children":[
                     {
                        "code":"9900",
                        "libelle":"[9900] Activités des organisations et organismes extraterritoriaux",
                        "children":[
                           {
                              "code":"9900Z",
                              "libelle":"[9900Z] Activités des organisations et organismes extraterritoriaux"
                           }
                        ]
                     }
                  ]
               }
            ]
         }
      ]
   }
]

EOT;

$json = json_decode($treenaf_all, true);

return [
    "activity_tree" => $json
];
