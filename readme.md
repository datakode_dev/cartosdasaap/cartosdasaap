/!\ console.log = function () {} à effacer dans resources/assets/js/app.js si preprod / development

## Installation
- `composer install`
- `npm install`
- `cp .env.example .env`
- edit .env file
- `php artisan key:generate`
- Install postgis (`brew install postgis` on macOs )
- Activer postgis avec `CREATE EXTENSION postgis;` dans psql
- Créer une database cartosdasaap  
- `psql cartosdasaap -c "CREATE EXTENSION postgis";`
- Create a new account with the same email as used for the main login, without filling in any other fields. This will then give us access to the account as that user
- Import CSV files to database (details to come)

### Database
- Créer un utilisateur postgresql + grant privilege https://www.cyberciti.biz/faq/howto-add-postgresql-user-account/  
- `php artisan migrate:fresh ` 
- `php artisan db:seed`  

- Si issue lors de la création d'un account (account_2 already created) : DROP SCHEMA account_2 CASCADE;
- SQLSTATE[42704]: Undefined object: 7 ERROR: type "geography" does not exist : Refaire `CREATE EXTENSION postgis;`

### Launch
- Lors de la création du premier Account, si message d'erreur (exemple : Trying to access array offset on value of type int)  
Disable l'ancien et mettre error_reporting = E_ERROR & ~E_NOTICE & ~E_DEPRECATED dans `/etc/php/7.version/cli/php.ini`  
Et dans laravel : https://stackoverflow.com/a/59635409/210824  
- Lier l'utilisateur avec le nouveau compte (Vérifier les ids dans les tables users / accounts, puis insert dans members)
INSERT INTO members (user_id, account_id, active, role_id) VALUES (...);
### Init geoapi
```
geoapi
  geoapi:get_commune      Get commune from data gouv
  geoapi:get_departement  Get departement from data gouv
  geoapi:get_region       Get region from data gouv
  ```
`php artisan geoapi:get_departement 31`

## key_types table
- 1 : Login/Pass
- 2 : Referer 

## Permission
- 1 : members_all -> Gérer les utilisateurs
- 2 : roles_all -> 	Gérer les rôles
- 3 : not_disabled -> Ne peut être désactivé
- 4 : can_edit -> Peut créer ou éditer des éléments 

## Queues

There are two queues for each account. `default_{account_id}` and `high_{account_id}`
The supervisor job configuration files are created automatically when a new account is created.
To install supervisor on your server please see [the supervisor documentation](http://supervisord.org/installing.html)
If you already have accounts created when you install supervisor, you can run the following command to create the supervisor config files for them : `php artisan jobs:update`
You will need to symlink your supervisor configuration file to the folder where they are stored, this can be for example `ln -s /etc/supervisor/conf.d /var/www/carto/storage/app/.supervisord` where `/var/www/carto/` is your install directory. You may have to remove the `/etc/supervisor/conf.d` folder before creating the symlink.
Add the following variable to your `.env` file :
`SUPERVISOR_USER=www-data` where `www-data` is the user your web server is running as.

`php artisan queue:work --queue=high_[account-id] && php artisan queue:work --queue=default_[account-id]`
