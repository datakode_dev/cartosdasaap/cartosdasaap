<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'slug' => "members_all",
            'name' => "Gérer les utilisateurs",
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('permissions')->insert([
            'slug' => "roles_all",
            'name' => "Gérer les rôles",
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('permissions')->insert([
            'slug' => "can_edit",
            'name' => "Peut créer ou éditer des éléments",
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('permissions')->insert([
            'slug' => "not_disabled",
            'name' => "Ne peut être désactivé",
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }
}
