<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(IgnKeyTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(AccountsSeeder::class);
        $this->call(IcoSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(IsoTypesTableSeeder::class);
        $this->call(MoveTypesTableSeeder::class);
        $this->call(ZonesTableSeeder::class);
    }
}
