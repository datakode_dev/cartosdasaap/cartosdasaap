<?php

use Illuminate\Database\Seeder;

class IcoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = $account = \App\Models\Account::where("name", "account 1")->first();

        $ico1 = \App\Models\Ico::firstOrCreate([
            'name' => 'fa fa-heart',
        ]);
        $ico2 = \App\Models\Ico::firstOrCreate([
            'name' => 'fa fa-car',
        ]);

        $color = \App\Models\Color::firstOrCreate([
            'name' => '#ff0000',
        ]);

        \App\Models\IcoColor::firstOrCreate([
            'ico_id' => $ico1->id,
            'color_id' => $color->id,
            'account_id' => $account->id,
            'active' => true,
        ]);

        \App\Models\IcoColor::firstOrCreate([
            'ico_id' => $ico2->id,
            'color_id' => $color->id,
            'account_id' => $account->id,
            'active' => true,
        ]);
    }
}
