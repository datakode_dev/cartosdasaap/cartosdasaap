<?php

use Illuminate\Database\Seeder;

class IgnKeyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('key_types')->insert([
            'name' => "Login/Pass",
            'created_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('key_types')->insert([
            'name' => "Referer",
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }
}
