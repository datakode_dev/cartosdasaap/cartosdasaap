<?php

use Illuminate\Database\Seeder;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('geo_data.zones')->insert([
            'name' => "Commune",
        ]);
        DB::table('geo_data.zones')->insert([
            'name' => "EPCI",
        ]);
        DB::table('geo_data.zones')->insert([
            'name' => "Département",
        ]);
        DB::table('geo_data.zones')->insert([
            'name' => "Région",
        ]);
    }
}
