<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => "Team",
            'lastname' => "Team",
            'email' => 'team@datakode.fr',
            'is_superadmin' => 1,
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'firstname' => "admin",
            'lastname' => "admin",
            'email' => 'admin@datakode.fr',
            'is_superadmin' => 1,
            'password' => bcrypt('secret'),
        ]);
    }
}
