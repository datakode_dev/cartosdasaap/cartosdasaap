<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = DB::table('users')->where('email', 'admin@datakode.fr')->first();

        $account = \App\Models\Account::firstOrCreate([
            "name" => "account 1",
            "active" => true,
        ]);

        $ignKey = \App\Models\IgnKey::firstOrCreate([
            'name' => 'ignKey',
            'key' => "choisirgeoportail",
            'account_id' => $account->id,
            'active' => true,
            'max_count' => 300000,
            'key_types_id' => 2,
            'local_key' => true,
            'start_date' => \Carbon\Carbon::now(),
            'end_date' => \Carbon\Carbon::now(),
        ]);

        $role = \App\Models\Role::firstOrCreate([
            "name" => "Admin",
            "active" => true,
            "account_id" => $account->id,
        ]);

        $member = \App\Models\Member::firstOrCreate([
            'user_id' => $admin->id,
            'account_id' => $account->id,
            'active' => true,
            'role_id' => $role->id,
        ]);
    }
}
