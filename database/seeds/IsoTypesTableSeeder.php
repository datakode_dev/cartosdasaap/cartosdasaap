<?php

use Illuminate\Database\Seeder;

class IsoTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('iso_types')->insert([
            'name' => "Isochrone",
        ]);
        DB::table('iso_types')->insert([
            'name' => "Isodistance",
        ]);
    }
}
