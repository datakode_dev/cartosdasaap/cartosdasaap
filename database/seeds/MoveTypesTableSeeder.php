<?php

use Illuminate\Database\Seeder;

class MoveTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('move_types')->insert([
            'name' => "Voiture",
        ]);
        DB::table('move_types')->insert([
            'name' => "Piéton",
        ]);
    }
}
