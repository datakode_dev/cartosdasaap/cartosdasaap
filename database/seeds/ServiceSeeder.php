<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = $account = \App\Models\Account::where("name", "account 1")->first();

        $services = \App\Models\ServicePei::firstOrCreate([
            'name' => 'test service',
            'path' => '',
            'account_id' => $account->id,
            'ico_color_id' => \App\Models\IcoColor::first()->id,
            'active' => true,
            'day_between_refresh' => 1,
            'source_url' => route('API.geo.convert', [
                'limit' => '81005.1',
                'buffer' => '5',
                'nodes%5B0%5D' => '0322',
            ]),
        ]);
    }
}
