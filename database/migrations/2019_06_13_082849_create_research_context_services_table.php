<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchContextServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_context_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('research_context_id');
            $table->foreign('research_context_id')->references('id')->on('research_contexts');
            $table->unsignedInteger('service_pei_id')->nullable();
            $table->foreign('service_pei_id')->references('id')->on('service_peis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_context_services');
    }
}
