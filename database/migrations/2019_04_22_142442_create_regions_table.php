<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $name = "geo_data";
        $sql1 = 'CREATE SCHEMA ' . $name;
        $pdo = DB::connection()->getPdo(); // get pdo from laravel database
        $query = $pdo->prepare($sql1); // prepare raw sql
        $succ = $query->execute(); // execute raw sql

        Schema::create($name . '.regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('insee');
            $table->string('name');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.regions ADD "geom" geography NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $name = "geo_data";
        $sql1 = 'DROP SCHEMA ' . $name . ' CASCADE';
        $pdo = DB::connection()->getPdo(); // get pdo from laravel database
        $query = $pdo->prepare($sql1); // prepare raw sql
        $succ = $query->execute(); // execute raw sql

        Schema::dropIfExists('geo_data.regions');
    }
}
