<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsoContextServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_context_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('iso_context_id');
            $table->foreign('iso_context_id')->references('id')->on('iso_contexts');
            $table->unsignedInteger('service_pei_id');
            $table->foreign('service_pei_id')->references('id')->on('service_peis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_context_services');
    }
}
