<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchContextRadiusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_context_radius', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('value');
            $table->unsignedInteger('research_context_id');
            $table->foreign('research_context_id')->references('id')->on('research_contexts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_context_radius');
    }
}
