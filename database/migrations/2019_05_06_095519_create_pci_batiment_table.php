<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePciBatimentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_data.pci_batiment', function (Blueprint $table) {
            $table->string('type');
            $table->string('nom')->nullable();
            $table->date('created');
            $table->date('updated');
        });
        DB::statement('ALTER TABLE geo_data.pci_batiment ADD "geom" geometry(Geometry,4326) NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data.pci_batiment');
    }
}
