<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $name = "geo_data";
        Schema::create($name . '.communes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('insee');
            $table->string('insee_departement');
            $table->string('name');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.communes ADD "geom" geography NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data.communes');
    }
}
