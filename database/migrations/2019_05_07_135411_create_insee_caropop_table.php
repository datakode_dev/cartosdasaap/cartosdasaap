<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInseeCaropopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_data.insee_caropop', function (Blueprint $table) {
            $table->string('id');
            $table->string('idinspire');
            $table->string('nbcar_car');
            $table->string('idk');
            $table->decimal('ind_c', 16, 4);
            $table->string('nbcar_rec');
            $table->decimal('men', 16, 4);
            $table->decimal('men_surf', 16, 4);
            $table->decimal('men_occ5', 16, 4);
            $table->decimal('men_coll', 16, 4);
            $table->decimal('men_5ind', 16, 4);
            $table->decimal('men_1ind', 16, 4);
            $table->string('i_1ind');
            $table->decimal('men_prop', 16, 4);
            $table->string('i_prop');
            $table->decimal('men_basr', 16, 4);
            $table->string('i_basr');
            $table->decimal('ind_r', 16, 4);
            $table->decimal('ind_age1', 16, 4);
            $table->decimal('ind_age2', 16, 4);
            $table->decimal('ind_age3', 16, 4);
            $table->decimal('ind_age4', 16, 4);
            $table->decimal('ind_age5', 16, 4);
            $table->decimal('ind_age6', 16, 4);
            $table->decimal('ind_age7', 16, 4);
            $table->string('i_age7');
            $table->decimal('ind_age8', 16, 4);
            $table->string('i_age8');
            $table->decimal('ind_srf', 16, 4);
        });
        DB::statement('ALTER TABLE geo_data.insee_caropop ADD "geom" geometry(Geometry,4326) NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data.insee_caropop');
    }
}
