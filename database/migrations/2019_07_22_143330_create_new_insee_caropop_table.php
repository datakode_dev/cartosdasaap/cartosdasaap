<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewInseeCaropopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_data.new_insee_caropop', function (Blueprint $table) {
            $table->string('idinspire');
            $table->string('id_carr1km');
            $table->integer('i_est_cr');
            $table->string('id_carr_n');
            $table->integer('groupe');
            $table->string('depcom');
            $table->integer('i_pauv');
            $table->string('id_car2010');
            $table->decimal('ind', 16, 4);
            $table->decimal('men', 16, 4);
            $table->decimal('men_pauv', 16, 4);
            $table->decimal('men_1ind', 16, 4);
            $table->decimal('men_5ind', 16, 4);
            $table->decimal('men_prop', 16, 4);
            $table->decimal('men_fmp', 16, 4);
            $table->decimal('ind_snv', 16, 4);
            $table->decimal('men_surf', 16, 4);
            $table->decimal('men_coll', 16, 4);
            $table->decimal('men_mais', 16, 4);
            $table->decimal('log_av45', 16, 4);
            $table->decimal('log_45_70', 16, 4);
            $table->decimal('log_70_90', 16, 4);
            $table->decimal('log_ap90', 16, 4);
            $table->decimal('log_inc', 16, 4);
            $table->decimal('log_soc', 16, 4);
            $table->decimal('ind_0_3', 16, 4);
            $table->decimal('ind_4_5', 16, 4);
            $table->decimal('ind_6_10', 16, 4);
            $table->decimal('ind_11_17', 16, 4);
            $table->decimal('ind_18_24', 16, 4);
            $table->decimal('ind_25_39', 16, 4);
            $table->decimal('ind_40_54', 16, 4);
            $table->decimal('ind_55_64', 16, 4);
            $table->decimal('ind_65_79', 16, 4);
            $table->decimal('ind_80p', 16, 4);
            $table->decimal('ind_inc', 16, 4);
            $table->integer('i_est_1km');
        });
        DB::statement('ALTER TABLE geo_data.new_insee_caropop ADD "geom" geometry(Geometry,4326) NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_insee_caropop');
    }
}
