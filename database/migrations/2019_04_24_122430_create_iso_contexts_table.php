<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsoContextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_contexts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('is_simulation');
            $table->unsignedInteger('iso_type_id');
            $table->foreign('iso_type_id')->references('id')->on('iso_types');
            $table->string('insee_limit');
            $table->string('zone_id');
            $table->unsignedInteger('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('iso_contexts');
            $table->json('indicators');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_contexts');
    }
}
