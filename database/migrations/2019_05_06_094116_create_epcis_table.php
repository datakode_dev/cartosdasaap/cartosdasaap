<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpcisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_data.epcis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dep_epci');
            $table->string('siren_epci');
            $table->string('nom_complet');
        });
        DB::statement('ALTER TABLE geo_data.epcis ADD "geom" geography NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data.epcis');
    }
}
