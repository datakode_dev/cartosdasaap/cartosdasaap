<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $name = "geo_data";
        Schema::create($name . '.departements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('insee');
            $table->string('name');
            $table->string('insee_region');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE ' . $name . '.departements ADD "geom" geography NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data.departements');
    }
}
