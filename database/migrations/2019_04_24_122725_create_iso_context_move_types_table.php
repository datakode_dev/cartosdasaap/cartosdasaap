<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsoContextMoveTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_context_move_types', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('move_type_id');
            $table->foreign('move_type_id')->references('id')->on('move_types');

            $table->unsignedInteger('iso_context_service_id');
            $table->foreign('iso_context_service_id')->references('id')->on('iso_context_services');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_context_move_types');
    }
}
