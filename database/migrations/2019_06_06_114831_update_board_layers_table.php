<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateBoardLayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('board_layers', function (Blueprint $table) {
            $table->dropColumn(['service_pei_id']);
            $table->unsignedInteger('layer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('board_layers', function (Blueprint $table) {
            $table->unsignedInteger('service_pei_id');
            $table->foreign('service_pei_id')->references('id')->on('service_peis');
            $table->dropColumn('layer_id');
        });
    }
}
