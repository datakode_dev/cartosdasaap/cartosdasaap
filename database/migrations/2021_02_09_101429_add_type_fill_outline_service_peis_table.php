<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFillOutlineServicePeisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_peis', function (Blueprint $table) {
            $table->string('geometry')->nullable();
            $table->string('outline')->nullable();
            $table->string('fill')->nullable();
            $table->integer('ico_color_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_peis', function (Blueprint $table) {
            $table->dropColumn('geometry');
            $table->dropColumn('outline');
            $table->dropColumn('fill');
        });
    }
}
