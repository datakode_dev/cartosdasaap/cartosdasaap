<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateIgnKeysColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ign_keys', function (Blueprint $table) {
            $table->renameColumn('ens_date', 'end_date');
            $table->string('login')->nullable()->after('key');
            $table->string('password')->nullable()->after('login');
            $table->integer('count')->default(0)->change();
            $table->integer('key_types_id');
            $table->foreign('key_types_id')->references('id')->on('key_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ign_keys', function (Blueprint $table) {
            $table->dropColumn(['key_types_id']);
        });
    }
}
