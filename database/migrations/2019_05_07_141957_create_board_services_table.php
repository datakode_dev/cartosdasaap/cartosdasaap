<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('board_id');
            $table->foreign('board_id')->references('id')->on('boards');
            $table->unsignedInteger('service_pei_id');
            $table->foreign('service_pei_id')->references('id')->on('service_peis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_services');
    }
}
