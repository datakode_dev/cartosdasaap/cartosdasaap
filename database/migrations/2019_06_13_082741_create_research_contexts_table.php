<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchContextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('research_contexts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('insee_limit');
            $table->unsignedInteger('zone_id');
            $table->unsignedInteger('ign_key_id');
            $table->foreign('ign_key_id')->references('id')->on('ign_keys');

            $table->unsignedInteger('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->boolean('active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('research_contexts');
    }
}
