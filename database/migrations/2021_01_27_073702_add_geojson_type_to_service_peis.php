<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGeojsonTypeToServicePeis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_peis', function (Blueprint $table) {
            $table->boolean('geojson_type')->default(false);
        });

        \Illuminate\Support\Facades\DB::table('service_peis')
            ->where('source_url', 'like' , '%http%')
            ->update(['geojson_type' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_peis', function (Blueprint $table) {
            $table->dropColumn('geojson_type');
        });
    }
}
