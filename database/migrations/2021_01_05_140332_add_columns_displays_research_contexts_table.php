<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDisplaysResearchContextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('research_contexts', function (Blueprint $table) {
            $table->boolean('display_city')->default(false);
            $table->boolean('display_route')->default(false);
            $table->boolean('display_selection')->default(false);
            $table->boolean('display_legend')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('research_contexts', function (Blueprint $table) {
        });
    }
}
