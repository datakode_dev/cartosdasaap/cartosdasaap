<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicePeisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_peis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('info')->nullable();
            $table->string('path');
            $table->string('source_url')->nullable();
            $table->boolean('is_simulation')->default(0);
            $table->integer('day_between_refresh')->nullable();
            $table->unsignedInteger('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->unsignedInteger('ico_color_id');
            $table->foreign('ico_color_id')->references('id')->on('icos_colors');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_peis');
    }
}
