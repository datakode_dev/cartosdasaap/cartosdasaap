<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsoContextMoveStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iso_context_move_step', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('iso_context_move_type_id');

            $table->foreign('iso_context_move_type_id')->references('id')->on('iso_context_move_types');
            $table->integer('value');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iso_context_move_step');
    }
}
